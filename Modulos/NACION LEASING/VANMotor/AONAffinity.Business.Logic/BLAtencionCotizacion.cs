﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Motor.Resources;
using AONAffinity.Library.Resources;
using AONAffinity.Library.DataAccess;
using AONAffinity.Library.DataAccess.BDMotor;
using AONAffinity.Library.BusinessEntity.BDMotor; 

namespace AONAffinity.Motor.BusinessLogic
{
    /// <summary>
    /// Clase de lógica del negocio de la tabla AtencionCotizacion.
    /// </summary>
    public class BLAtencionCotizacion
    {
        #region NoTransaccional
        /// <summary>
        /// Permite obtener una cotización para ser atendida por el informador.
        /// </summary>
        /// <param name="pcIdInformador">Código de informador.</param>
        /// <returns>Cotización a atender.</returns>
        public BEAtencionCotizacion Generar(String pcIdInformador)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            BEAtencionCotizacion objBEAtencionCotizacion = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAAtencionCotizacion objDAAtencionCotizacion = new DAAtencionCotizacion();
                SqlDataReader sqlDr = objDAAtencionCotizacion.Generar(pcIdInformador, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {                        
                        Int32 nIndex1 = sqlDr.GetOrdinal("idPeriodo");
                        Int32 nIndex2 = sqlDr.GetOrdinal("nroCotizacion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("tipo");

                        objBEAtencionCotizacion = new BEAtencionCotizacion();

                        if (sqlDr.Read())
                        {
                            objBEAtencionCotizacion.IdPeriodo = sqlDr.GetInt32(nIndex1);
                            objBEAtencionCotizacion.NroCotizacion = sqlDr.GetDecimal(nIndex2);
                            objBEAtencionCotizacion.Tipo = sqlDr.GetInt32(nIndex3); 
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return objBEAtencionCotizacion;
        }
        #endregion
    }
}
