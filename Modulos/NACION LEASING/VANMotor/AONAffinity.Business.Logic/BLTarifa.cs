﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity; 

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLTarifa
    {
        public BETarifa Obtener(Int32 pnIdProducto, Int32 pnIdModelo, Int32 pnAnioFab, Int16 pnNroAsientos)
        {
            BETarifa objBETarifa = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DATarifa objDATarifa = new DATarifa();
                SqlDataReader sqlDr = objDATarifa.Obtener(pnIdProducto, pnIdModelo, pnAnioFab, pnNroAsientos, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idTarifa");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex3 = sqlDr.GetOrdinal("idCategoria");
                        Int32 nIndex4 = sqlDr.GetOrdinal("idModelo");
                        Int32 nIndex5 = sqlDr.GetOrdinal("nroAsientos");
                        Int32 nIndex6 = sqlDr.GetOrdinal("anioFab");
                        Int32 nIndex7 = sqlDr.GetOrdinal("valorVehiculo");
                        Int32 nIndex8 = sqlDr.GetOrdinal("fecIniVigencia");
                        Int32 nIndex9 = sqlDr.GetOrdinal("fecFinVigencia");
                        Int32 nIndex10 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex11 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex12 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex13 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex14 = sqlDr.GetOrdinal("stsRegistro");

                        if (sqlDr.Read()) 
                        {
                            objBETarifa = new BETarifa();
                            objBETarifa.IdTarifa = sqlDr.GetInt32(nIndex1);
                            objBETarifa.IdProducto = sqlDr.GetInt32(nIndex2);
                            objBETarifa.IdCategoria = sqlDr.GetInt32(nIndex3);
                            objBETarifa.IdModelo = sqlDr.GetInt32(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBETarifa.NroAsientos = NullTypes.ShortNull; } else { objBETarifa.NroAsientos = sqlDr.GetInt16(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBETarifa.AnioFab = NullTypes.IntegerNull; } else { objBETarifa.AnioFab = sqlDr.GetInt32(nIndex6); }
                            if (sqlDr.IsDBNull(nIndex7)) { objBETarifa.ValorVehiculo = NullTypes.DecimalNull; } else { objBETarifa.ValorVehiculo = sqlDr.GetDecimal(nIndex7); }
                            if (sqlDr.IsDBNull(nIndex8)) { objBETarifa.FecIniVigencia = NullTypes.FechaNull; } else { objBETarifa.FecIniVigencia = sqlDr.GetDateTime(nIndex8); }
                            if (sqlDr.IsDBNull(nIndex9)) { objBETarifa.FecFinVigencia = NullTypes.FechaNull; } else { objBETarifa.FecFinVigencia = sqlDr.GetDateTime(nIndex9); }
                            objBETarifa.FechaCreacion = sqlDr.GetDateTime(nIndex10);
                            objBETarifa.UsuarioCreacion = sqlDr.GetString(nIndex11);
                            if (sqlDr.IsDBNull(nIndex12)) { objBETarifa.FechaModificacion = NullTypes.FechaNull; } else { objBETarifa.FechaModificacion = sqlDr.GetDateTime(nIndex12); }
                            if (sqlDr.IsDBNull(nIndex13)) { objBETarifa.UsuarioModificacion = NullTypes.CadenaNull; } else { objBETarifa.UsuarioModificacion = sqlDr.GetString(nIndex13); }
                            objBETarifa.EstadoRegistro = sqlDr.GetBoolean(nIndex14);
                        }

                        sqlDr.Close();                                               
                    }
                }
            }

            return objBETarifa;
        }

        #region Antiguo
        //public BETarifa Obtener(Int32 pnIdModelo, Int32 pnIdAnio)
        //{
        //    BETarifa objBETarifa = null;

        //    using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
        //    {
        //        sqlCn.Open();

        //        DATarifa objDATarifa = new DATarifa();
        //        SqlDataReader sqlDr = objDATarifa.Obtener(pnIdModelo, pnIdAnio, sqlCn);

        //        if (sqlDr != null)
        //        {
        //            if (sqlDr.HasRows)
        //            {
        //                Int32 nIndex1 = sqlDr.GetOrdinal("idModelo");
        //                Int32 nIndex2 = sqlDr.GetOrdinal("idAnio");
        //                Int32 nIndex3 = sqlDr.GetOrdinal("valorVehiculo");
        //                Int32 nIndex4 = sqlDr.GetOrdinal("fecVigencia");
        //                Int32 nIndex5 = sqlDr.GetOrdinal("usrCreacion");
        //                Int32 nIndex6 = sqlDr.GetOrdinal("fecCreacion");
        //                Int32 nIndex7 = sqlDr.GetOrdinal("usrModificacion");
        //                Int32 nIndex8 = sqlDr.GetOrdinal("fecModificacion");
        //                Int32 nIndex9 = sqlDr.GetOrdinal("stsRegistro");

        //                objBETarifa = new BETarifa();

        //                if (sqlDr.Read())
        //                {
        //                    objBETarifa.IdModelo = sqlDr.GetInt32(nIndex1);
        //                    objBETarifa.IdAnio = sqlDr.GetInt32(nIndex2);
        //                    objBETarifa.ValorVehiculo = sqlDr.GetDecimal(nIndex3);
        //                    if (sqlDr.IsDBNull(nIndex4)) { objBETarifa.FecVigencia = NullTypes.FechaNull; } else { objBETarifa.FecVigencia = sqlDr.GetDateTime(nIndex4); }
        //                    objBETarifa.UsuarioCreacion = sqlDr.GetString(nIndex5);
        //                    objBETarifa.FechaCreacion = sqlDr.GetDateTime(nIndex6);
        //                    if (sqlDr.IsDBNull(nIndex7)) { objBETarifa.UsuarioModificacion = NullTypes.CadenaNull; } else { objBETarifa.UsuarioModificacion = sqlDr.GetString(nIndex7); }
        //                    if (sqlDr.IsDBNull(nIndex8)) { objBETarifa.FechaModificacion = NullTypes.FechaNull; } else { objBETarifa.FechaModificacion = sqlDr.GetDateTime(nIndex8); }
        //                    objBETarifa.EstadoRegistro = sqlDr.GetBoolean(nIndex9);
        //                }

        //                sqlDr.Close();
        //            }
        //        }

        //    }

        //    return objBETarifa;
        //}
        #endregion       
    }
}
