﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;      

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLEstructuraArchivo
    {
        #region NoTransaccional
        public List<BEEstructuraArchivo> Listar(Int32 pnIdTipoProceso, Int32 pnIdTipoArchivo, Nullable<Boolean> pbStsRegistro)
        {
            List<BEEstructuraArchivo> lstBEEstructuraArchivo = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAEstructuraArchivo objDAEstructuraArchivo = new DAEstructuraArchivo();
                SqlDataReader sqlDr = objDAEstructuraArchivo.Listar(pnIdTipoProceso, pnIdTipoArchivo, pbStsRegistro, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex0 = sqlDr.GetOrdinal("idTipoProceso");
                        Int32 nIndex1 = sqlDr.GetOrdinal("orden");
                        Int32 nIndex2 = sqlDr.GetOrdinal("tipoDato");
                        Int32 nIndex3 = sqlDr.GetOrdinal("valorFijo");
                        Int32 nIndex4 = sqlDr.GetOrdinal("nombre");
                        Int32 nIndex5 = sqlDr.GetOrdinal("inicio");
                        Int32 nIndex6 = sqlDr.GetOrdinal("longitud");
                        Int32 nIndex7 = sqlDr.GetOrdinal("Obligatorio");
                        Int32 nIndex8 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex9 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex10 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex11 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex12 = sqlDr.GetOrdinal("stsRegistro");

                        lstBEEstructuraArchivo = new List<BEEstructuraArchivo>();

                        while (sqlDr.Read())
                        {
                            BEEstructuraArchivo objBEEstructuraArchivo = new BEEstructuraArchivo();
                            objBEEstructuraArchivo.IdTipoProceso = sqlDr.GetInt32(nIndex0);
                            objBEEstructuraArchivo.Orden = sqlDr.GetInt32(nIndex1);
                            objBEEstructuraArchivo.TipoDato = sqlDr.GetString(nIndex2);
                            if (sqlDr.IsDBNull(nIndex3)) { objBEEstructuraArchivo.ValorFijo = NullTypes.CadenaNull; } else { objBEEstructuraArchivo.ValorFijo = sqlDr.GetString(nIndex3); }
                            objBEEstructuraArchivo.Nombre = sqlDr.GetString(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBEEstructuraArchivo.Inicio = NullTypes.IntegerNull; } else { objBEEstructuraArchivo.Inicio = sqlDr.GetInt32(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBEEstructuraArchivo.Longitud = NullTypes.IntegerNull; } else { objBEEstructuraArchivo.Longitud = sqlDr.GetInt32(nIndex6); }
                            objBEEstructuraArchivo.Obligatorio = sqlDr.GetBoolean(nIndex7);
                            objBEEstructuraArchivo.UsuarioCreacion = sqlDr.GetString(nIndex8);
                            objBEEstructuraArchivo.FechaCreacion = sqlDr.GetDateTime(nIndex9);
                            if (sqlDr.IsDBNull(nIndex10)) { objBEEstructuraArchivo.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEEstructuraArchivo.UsuarioModificacion = sqlDr.GetString(nIndex10); }
                            if (sqlDr.IsDBNull(nIndex11)) { objBEEstructuraArchivo.FechaModificacion = NullTypes.FechaNull; } else { objBEEstructuraArchivo.FechaModificacion = sqlDr.GetDateTime(nIndex11); }
                            objBEEstructuraArchivo.EstadoRegistro = sqlDr.GetBoolean(nIndex12);

                            lstBEEstructuraArchivo.Add(objBEEstructuraArchivo);
                        }

                        sqlDr.Close();
                    }
                }
            }


            return lstBEEstructuraArchivo;
        }
        #endregion        

        #region Transaccional
        public Int32 Actualizar(BEEstructuraArchivo pObjBEEstructuraArchivo) 
        {
            Int32 nIndex = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAEstructuraArchivo objDAEstructuraArchivo = new DAEstructuraArchivo();
                nIndex = objDAEstructuraArchivo.Actualizar(pObjBEEstructuraArchivo, sqlCn);                 
            }

            return nIndex;
        }
        #endregion
    }
}
