﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Resources;
using AONAffinity.Business.Entity.BDMotor;
using AONAffinity.Data.DataAccess;
using AONAffinity.Data.DataAccess.BDMotor;

namespace AONAffinity.Business.Logic
{
    /// <summary>
    /// Clase de lógica del negocio de la tabla Carroceria.
    /// </summary>
    public class BLCarroceria
    {
        #region NoTransaccional
        /// <summary>
        /// Permite listar las carrocerias de vehículos.
        /// </summary>
        /// <param name="pbStsRegistro">Estado del registro.</param>
        /// <returns>Objeto de List de tipo BECarroceria.</returns>
        public List<BECarroceria> Listar(Nullable<Boolean> pbStsRegistro) 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-31
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            List<BECarroceria> lstBECarroceria = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DACarroceria objDACarroceria = new DACarroceria();
                SqlDataReader sqlDr = objDACarroceria.Listar(pbStsRegistro, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idCarroceria");
                        Int32 nIndex2 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex4 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex5 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex6 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("stsRegistro");

                        lstBECarroceria = new List<BECarroceria>();

                        while (sqlDr.Read()) 
                        {
                            BECarroceria objBECarroceria = new BECarroceria();

                            objBECarroceria.IdCarroceria = sqlDr.GetInt32(nIndex1);
                            objBECarroceria.Descripcion = sqlDr.GetString(nIndex2);
                            objBECarroceria.UsuarioCreacion = sqlDr.GetString(nIndex3);
                            objBECarroceria.FechaCreacion = sqlDr.GetDateTime(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBECarroceria.UsuarioModificacion = NullTypes.CadenaNull; } else { objBECarroceria.UsuarioModificacion = sqlDr.GetString(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBECarroceria.FechaModificacion = NullTypes.FechaNull; } else { objBECarroceria.FechaModificacion = sqlDr.GetDateTime(nIndex6); }
                            objBECarroceria.EstadoRegistro = sqlDr.GetBoolean(nIndex7);

                            lstBECarroceria.Add(objBECarroceria);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBECarroceria;
        }  
        #endregion
    }
}
