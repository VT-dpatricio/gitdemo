﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO; 
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;
using AONAffinity.Motor.BusinessLogic.Cotizacion;   
  
namespace AONAffinity.Motor.BusinessLogic
{
    /// <summary>
    /// Clase de lógica del negocio de la tabla Cotizacion.
    /// </summary>
    public class BLCotizacion
    {
        #region Métodos
        public Int32 Generar(Int32 pnIdProceso, String pcUsuario) 
        {
            Int32 nResult = 0;
            BLProceso objBLProceso = new BLProceso();
            BEProceso objBEProceso = objBLProceso.Obtener(pnIdProceso);

            if (objBEProceso == null) 
            {
                return nResult;
            }

            BLCliente objBLCliente = new BLCliente();
            List<BECliente> lstBECliente = objBLCliente.ListarxProceso(pnIdProceso);

            if (lstBECliente == null) 
            {
                return nResult;
            }

            //Si clientes son de BBVA Generar
            if (objBEProceso.IdProducto == Producto.SegVehicularBBVA_5251) 
            {
                this.GenerarCotizacionBBVA_5251(lstBECliente, pcUsuario, ref nResult);
            }

            //Si clientes son de IBK Generar
            if (objBEProceso.IdProducto == Producto.SegVehicularIBK_5250)
            {
                this.GenerarCotizacionIBK_5250(lstBECliente, pcUsuario, ref nResult);
            }

            return nResult;
        }
        #endregion

        #region Cotizacion BBVA
        private void GenerarCotizacionBBVA_5251(List<BECliente> pLstBECliente, String pcUsuario, ref Int32 pnNroCotizacion) 
        {
            BLCotizacion5251 objBLCotizacion5251 = new BLCotizacion5251();
            objBLCotizacion5251.CotizarClienteBBVA_5251(pLstBECliente, pcUsuario, ref pnNroCotizacion);
        }
        #endregion

        #region Cotizacion IBK
        private void GenerarCotizacionIBK_5250(List<BECliente> pLstBECliente, String pcUsario, ref Int32 pnNroCotizacion)
        {
            BLCotizacion5250 objBLCotizacion5250 = new BLCotizacion5250();
            objBLCotizacion5250.CotizarClienteIBK_5250(pLstBECliente, pcUsario, ref pnNroCotizacion);            
        }
        #endregion




        #region Transaccional
        /// <summary>
        /// Permite generar las cotizaciones masivas.
        /// </summary>
        /// <param name="pcUsuario">Código de usuario que ejecuta el proceso.</param>
        public Int32 Generar(List<BECliente> pLstBECliente, String pcUsuario)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            BLCliente objBLCliente = new BLCliente();
            List<BECliente> lstBECliente = pLstBECliente;
            Int32 nResult = 0;

            if (pLstBECliente != null) 
            {
                this.GenerarCotizacionBBVA(lstBECliente, pcUsuario, ref nResult);
            }

            

            ////Si existe clienete por cotizar, se obtiene ineformacion de cada uno.
            //if (lstBECliente != null)
            //{
            //    //Validar si se obtenedra valor del vehículo de la tabla APESEG.
            //    if (!this.UsarAPESEG())
            //    {
            //        this.GenCotSinAPESEG(lstBECliente, pcUsuario, ref nResult);
            //    }
            //    else
            //    {
            //        this.GenCotConAPESEG(lstBECliente, pcUsuario, ref nResult);
            //    }
            //}

            return nResult;
        }

        /// <summary>
        /// Permite insertar una cotización.
        /// </summary>
        /// <param name="pObjBECotizacion">Objeto que contiene la información de la cotización.</param>
        /// <returns>Nro. de filas afectadas.</returns>
        public Int32 Insertar(BECotizacion pObjBECotizacion)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DACotizacion objDACotizacion = new DACotizacion();
                nResult = objDACotizacion.Insertar(pObjBECotizacion, sqlCn);
            }

            return nResult;
        }

        /// <summary>
        /// Permite actualizar el valor del vehículo.
        /// </summary>
        /// <param name="pObjBECotizacion">Objeto que contiene la información de la cotización</param>
        /// <returns>Nro. de filas afectadas.</returns>
        public Int32 ActualizarValVeh(BECotizacion pObjBECotizacion) 
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DACotizacion objDACotizacion = new DACotizacion();
                nResult = objDACotizacion.ActualizarValVeh(pObjBECotizacion, sqlCn);  
            }

            return nResult;
        }

        /// <summary>
        /// Permite actualizar la cotización al estado de rechazo.
        /// </summary>
        /// <param name="pObjBECotizacion">Objeto que contien la información de los rechazos.</param>
        /// <returns>Nro. de filas afectadas.</returns>
        public Int32 ActualizarRechazo(BECotizacion pObjBECotizacion) 
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DACotizacion objDACotizacion = new DACotizacion();

                nResult = objDACotizacion.ActualizarRechazo(pObjBECotizacion, sqlCn);
            }

            return nResult;
        }

        /// <summary>
        /// Permite revertir el cambio del valor del vehiculo.
        /// </summary>
        /// <param name="pObjBECotizacion">Objeto que contien la información de la cotización.</param>
        /// <returns>Nro. de filas afectadas.</returns>
        public Int32 RevertirValVeh(BECotizacion pObjBECotizacion) 
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DACotizacion objDACotizacion = new DACotizacion();
                nResult = objDACotizacion.RevertirValVeh(pObjBECotizacion, sqlCn);  
            }

            return nResult;
        }

        public Int32 ActualizarCliente(BECotizacion pObjBECotizacion) 
        {
            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DACotizacion objDACotizacion = new DACotizacion();
                nResult = objDACotizacion.ActualizarCliente(pObjBECotizacion, sqlCn); 
            }

            return nResult;
        }

        public Int32 ActualizarVehiculo(BECotizacion pObjBECotizacion) 
        {
            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DACotizacion objDACotizacion = new DACotizacion();
                nResult = objDACotizacion.ActualizarVehiculo(pObjBECotizacion, sqlCn); 
            }

            return nResult;
        }

        public Int32 Actualizar(BECotizacion pObjBECotizacion) 
        {
            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DACotizacion objDACotizacion = new DACotizacion();
                nResult = objDACotizacion.Actualizar(pObjBECotizacion, sqlCn);
            }

            return nResult;
        }

        public Int32 Expedir(BECotizacion pObjBECotizacion) 
        {
            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DACotizacion objDACotizacion = new DACotizacion();
                nResult = objDACotizacion.Expedir(pObjBECotizacion, sqlCn);
            }

            return nResult;
        }

        public Decimal Regitrar(BECotizacion pObjBECotizacion, List<BECotizacionDetalle> pLstBECotizacionDetalle) 
        {
            Decimal nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                SqlTransaction sqlTrs = sqlCn.BeginTransaction();

                Decimal nNroCotizacion = 0;
                DACotizacion objDACotizacion = new DACotizacion();
                DACotizacionDetalle objDACotizacionDetalle = new DACotizacionDetalle();
                Int32 nConta = 0;

                if (objDACotizacion.Registrar(pObjBECotizacion, sqlCn, sqlTrs, ref nNroCotizacion) > 0) 
                {
                    foreach (BECotizacionDetalle objBECotizacionDetalle in pLstBECotizacionDetalle) 
                    {
                        objBECotizacionDetalle.NroCotizacion = nNroCotizacion;

                        if (objDACotizacionDetalle.Insertar(objBECotizacionDetalle, sqlCn, sqlTrs) > 0) 
                        {
                            nConta++;
                        }
                    }
                }

                if (nConta == pLstBECotizacionDetalle.Count())
                {
                    sqlTrs.Commit();
                    nResult = nNroCotizacion;
                }
                else
                {
                    sqlTrs.Rollback();
                }
            }

            return nResult;
        }
        #endregion

        #region NoTransaccional
        //new2013
        public List<BECotizacion> Buscar(Int32 pnIdTipo, String pcNroDocumento, String pcApePaterno, String pcApeMaterno, String pcPriNombre, String pcSegNombre, String pcNroPlaca, String pcUsuario) 
        {
            List<BECotizacion> lstBECotizacion = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DACotizacion objDACotizacion = new DACotizacion();
                SqlDataReader sqlDr = objDACotizacion.Buscar(pnIdTipo, pcNroDocumento, pcApePaterno, pcApeMaterno, pcPriNombre, pcSegNombre, pcNroPlaca, pcUsuario, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idPeriodo");
                        Int32 nIndex2 = sqlDr.GetOrdinal("nroCotizacion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("nroDocumento");
                        Int32 nIndex4 = sqlDr.GetOrdinal("apePaterno");
                        Int32 nIndex5 = sqlDr.GetOrdinal("apeMaterno");
                        Int32 nIndex6 = sqlDr.GetOrdinal("priNombre");
                        Int32 nIndex7 = sqlDr.GetOrdinal("segNombre");
                        Int32 nIndex8 = sqlDr.GetOrdinal("telDomicilio1");
                        Int32 nIndex9 = sqlDr.GetOrdinal("telDomicilio2");
                        Int32 nIndex10 = sqlDr.GetOrdinal("telDomicilio3");
                        Int32 nIndex11 = sqlDr.GetOrdinal("telMovil1");
                        Int32 nIndex12 = sqlDr.GetOrdinal("telMovil2");
                        Int32 nIndex13 = sqlDr.GetOrdinal("telMovil3");
                        Int32 nIndex14 = sqlDr.GetOrdinal("telOficina1");
                        Int32 nIndex15 = sqlDr.GetOrdinal("telOficina2");
                        Int32 nIndex16 = sqlDr.GetOrdinal("telOficina3");
                        Int32 nIndex17 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex18 = sqlDr.GetOrdinal("desProducto");
                        Int32 nIndex19 = sqlDr.GetOrdinal("nroPlaca");
                        Int32 nIndex20 = sqlDr.GetOrdinal("descMarca");
                        Int32 nIndex21 = sqlDr.GetOrdinal("descModelo");
                        Int32 nIndex22 = sqlDr.GetOrdinal("valOriVehiculo");
                        Int32 nIndex23 = sqlDr.GetOrdinal("anioFab");

                        lstBECotizacion = new List<BECotizacion>();

                        while(sqlDr.Read())
                        {
                            BECotizacion objBECotizacion = new BECotizacion();
                            objBECotizacion.IdPeriodo = sqlDr.GetInt32(nIndex1);
                            objBECotizacion.NroCotizacion = sqlDr.GetDecimal(nIndex2);
                            if (sqlDr.IsDBNull(nIndex3)) { objBECotizacion.NroDocumento = NullTypes.CadenaNull; } else { objBECotizacion.NroDocumento = sqlDr.GetString(nIndex3); }
                            objBECotizacion.ApePaterno = sqlDr.GetString(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBECotizacion.ApeMaterno = NullTypes.CadenaNull; } else { objBECotizacion.ApeMaterno = sqlDr.GetString(nIndex5); }
                            objBECotizacion.PriNombre = sqlDr.GetString(nIndex6);
                            if (sqlDr.IsDBNull(nIndex7)) { objBECotizacion.SegNombre = NullTypes.CadenaNull; } else { objBECotizacion.SegNombre = sqlDr.GetString(nIndex7); }
                            objBECotizacion.TelDomicilio1 = sqlDr.GetString(nIndex8);
                            if (sqlDr.IsDBNull(nIndex9)) { objBECotizacion.TelDomicilio2 = NullTypes.CadenaNull; } else { objBECotizacion.TelDomicilio2 = sqlDr.GetString(nIndex9); }
                            if (sqlDr.IsDBNull(nIndex10)) { objBECotizacion.TelDomicilio3 = NullTypes.CadenaNull; } else { objBECotizacion.TelDomicilio3 = sqlDr.GetString(nIndex10); }
                            if (sqlDr.IsDBNull(nIndex11)) { objBECotizacion.TelMovil1 = NullTypes.CadenaNull; } else { objBECotizacion.TelMovil1 = sqlDr.GetString(nIndex11); }
                            if (sqlDr.IsDBNull(nIndex12)) { objBECotizacion.TelMovil2 = NullTypes.CadenaNull; } else { objBECotizacion.TelMovil2 = sqlDr.GetString(nIndex12); }
                            if (sqlDr.IsDBNull(nIndex13)) { objBECotizacion.TelMovil3 = NullTypes.CadenaNull; } else { objBECotizacion.TelMovil3 = sqlDr.GetString(nIndex13); }
                            if (sqlDr.IsDBNull(nIndex14)) { objBECotizacion.TelOficina1 = NullTypes.CadenaNull; } else { objBECotizacion.TelOficina1 = sqlDr.GetString(nIndex14); }
                            if (sqlDr.IsDBNull(nIndex15)) { objBECotizacion.TelOficina2 = NullTypes.CadenaNull; } else { objBECotizacion.TelOficina2 = sqlDr.GetString(nIndex15); }
                            if (sqlDr.IsDBNull(nIndex16)) { objBECotizacion.TelOficina3 = NullTypes.CadenaNull; } else { objBECotizacion.TelOficina3 = sqlDr.GetString(nIndex16); }
                            if (sqlDr.IsDBNull(nIndex17)) { objBECotizacion.IdProducto = NullTypes.IntegerNull; } else { objBECotizacion.IdProducto = sqlDr.GetInt32(nIndex17); }
                            if (sqlDr.IsDBNull(nIndex18)) { objBECotizacion.DesProducto = NullTypes.CadenaNull; } else { objBECotizacion.DesProducto = sqlDr.GetString(nIndex18); }
                            objBECotizacion.NroPlaca = sqlDr.GetString(nIndex19);
                            objBECotizacion.DesMarca = sqlDr.GetString(nIndex20);
                            objBECotizacion.DesModelo = sqlDr.GetString(nIndex21);
                            objBECotizacion.ValOriVehiculo = Decimal.Round(sqlDr.GetDecimal(nIndex22), 2);
                            objBECotizacion.AnioFab = sqlDr.GetInt32(nIndex23);

                            lstBECotizacion.Add(objBECotizacion);
                        }

                        
                     }
                }
            }

            return lstBECotizacion;
        }

        public List<BERespuesta> ValidarVehiculo(Int32 pnIdProducto, Int32 pnIdModelo, Boolean pbTimonCambiado, Decimal pnValorVehiculo, Int32 pnAnioFab, Int16 pnNroAsientos, Int32 pnidTipoVehiculo, Int32 pnIdUsoVehiculo, Boolean pbGPS, Boolean pbGas, Int32 pnIdCiudad, Int32 pnIdCiudadInsp) 
        {
            List<BERespuesta> lstBERespuesta = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DACotizacion objDACotizacion = new DACotizacion();
                SqlDataReader sqlDr = objDACotizacion.ValidarVehiculo(pnIdProducto, pnIdModelo, pbTimonCambiado, pnValorVehiculo, pnAnioFab, pnNroAsientos, pnidTipoVehiculo, pnIdUsoVehiculo, pbGPS, pbGas, pnIdCiudad, pnIdCiudadInsp, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idRpta");
                        Int32 nIndex2 = sqlDr.GetOrdinal("descripcion");

                        lstBERespuesta = new List<BERespuesta>();

                        while (sqlDr.Read()) 
                        {
                            BERespuesta objBERespuesta = new BERespuesta();
                            objBERespuesta.IdRespuesta = sqlDr.GetInt32(nIndex1);
                            objBERespuesta.Descripcion = sqlDr.GetString(nIndex2);
                            lstBERespuesta.Add(objBERespuesta);
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return lstBERespuesta;
        }  

        public List<String> GenerarLog(Int32 pnIdProceso) 
        {
            List<String> lstcLog = new List<String>();

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DACotizacion objDACotizacion = new DACotizacion();
                SqlDataReader sqlDr = objDACotizacion.GenerarLog(pnIdProceso, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("trama");

                        while (sqlDr.Read()) 
                        {
                            String cTrama = sqlDr.GetString(nIndex1);
                            lstcLog.Add(cTrama);
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return lstcLog;
        }
        public List<BECotizacion> ListarxNroDocumento(String pcNroDocumento) 
        {
            List<BECotizacion> lstBECotizacion = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DACotizacion objDACotizacion = new DACotizacion();
                SqlDataReader sqlDr = objDACotizacion.ListarxNroDocumento(pcNroDocumento, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idPeriodo");
                        Int32 nIndex2 = sqlDr.GetOrdinal("nroCotizacion");
		                Int32 nIndex3 = sqlDr.GetOrdinal("nroDocumento"); 	
		                Int32 nIndex4 = sqlDr.GetOrdinal("apePaterno");
                        Int32 nIndex5 = sqlDr.GetOrdinal("apeMaterno");
                        Int32 nIndex6 = sqlDr.GetOrdinal("priNombre");
                        Int32 nIndex7 = sqlDr.GetOrdinal("segNombre");
		                Int32 nIndex8 = sqlDr.GetOrdinal("telDomicilio1");
                        Int32 nIndex9 = sqlDr.GetOrdinal("telDomicilio2");
                        Int32 nIndex10 = sqlDr.GetOrdinal("telDomicilio3");
		                Int32 nIndex11 = sqlDr.GetOrdinal("telMovil1"); 
                        Int32 nIndex12 = sqlDr.GetOrdinal("telMovil2");
                        Int32 nIndex13 = sqlDr.GetOrdinal("telMovil3");
		                Int32 nIndex14 = sqlDr.GetOrdinal("telOficina1");
                        Int32 nIndex15 = sqlDr.GetOrdinal("telOficina2");
                        Int32 nIndex16 = sqlDr.GetOrdinal("telOficina3");
                        Int32 nIndex17 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex18 = sqlDr.GetOrdinal("desProducto");

                        lstBECotizacion = new List<BECotizacion>();

                        while (sqlDr.Read()) 
                        {
                            BECotizacion objBECotizacion = new BECotizacion();
                            objBECotizacion.IdPeriodo = sqlDr.GetInt32(nIndex1);
                            objBECotizacion.NroCotizacion = sqlDr.GetDecimal(nIndex2);
                            if (sqlDr.IsDBNull(nIndex3)) { objBECotizacion.NroDocumento = NullTypes.CadenaNull; } else { objBECotizacion.NroDocumento = sqlDr.GetString(nIndex3); }
                            objBECotizacion.ApePaterno = sqlDr.GetString(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBECotizacion.ApeMaterno = NullTypes.CadenaNull; } else { objBECotizacion.ApeMaterno = sqlDr.GetString(nIndex5); }
                            objBECotizacion.PriNombre = sqlDr.GetString(nIndex6);
                            if (sqlDr.IsDBNull(nIndex7)) { objBECotizacion.SegNombre = NullTypes.CadenaNull; } else { objBECotizacion.SegNombre = sqlDr.GetString(nIndex7); }
                            objBECotizacion.TelDomicilio1 = sqlDr.GetString(nIndex8);
                            if (sqlDr.IsDBNull(nIndex9)) { objBECotizacion.TelDomicilio2 = NullTypes.CadenaNull; } else { objBECotizacion.TelDomicilio2 = sqlDr.GetString(nIndex9); }
                            if (sqlDr.IsDBNull(nIndex10)) { objBECotizacion.TelDomicilio3 = NullTypes.CadenaNull; } else { objBECotizacion.TelDomicilio3 = sqlDr.GetString(nIndex10); }
                            if (sqlDr.IsDBNull(nIndex11)) { objBECotizacion.TelMovil1 = NullTypes.CadenaNull; } else { objBECotizacion.TelMovil1 = sqlDr.GetString(nIndex11); }
                            if (sqlDr.IsDBNull(nIndex12)) { objBECotizacion.TelMovil2 = NullTypes.CadenaNull; } else { objBECotizacion.TelMovil2 = sqlDr.GetString(nIndex12); }
                            if (sqlDr.IsDBNull(nIndex13)) { objBECotizacion.TelMovil3 = NullTypes.CadenaNull; } else { objBECotizacion.TelMovil3 = sqlDr.GetString(nIndex13); }
                            if (sqlDr.IsDBNull(nIndex14)) { objBECotizacion.TelOficina1 = NullTypes.CadenaNull; } else { objBECotizacion.TelOficina1 = sqlDr.GetString(nIndex14); }
                            if (sqlDr.IsDBNull(nIndex15)) { objBECotizacion.TelOficina2 = NullTypes.CadenaNull; } else { objBECotizacion.TelOficina2 = sqlDr.GetString(nIndex15); }
                            if (sqlDr.IsDBNull(nIndex16)) { objBECotizacion.TelOficina3 = NullTypes.CadenaNull; } else { objBECotizacion.TelOficina3 = sqlDr.GetString(nIndex16); }
                            if (sqlDr.IsDBNull(nIndex17)) { objBECotizacion.IdProducto = NullTypes.IntegerNull; } else { objBECotizacion.IdProducto = sqlDr.GetInt32(nIndex17); }
                            if (sqlDr.IsDBNull(nIndex18)) { objBECotizacion.DesProducto = NullTypes.CadenaNull; } else { objBECotizacion.DesProducto = sqlDr.GetString(nIndex18); }

                            lstBECotizacion.Add(objBECotizacion);  
                        }

                        sqlDr.Close(); 
                    }
                }
            } 

            return lstBECotizacion;
        }

        public List<BECotizacion> ListarxNomApe(String pcApePaterno, String pcApeMaterno, String pcPriNonbre, String pcSegNonbre)
        {
            List<BECotizacion> lstBECotizacion = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DACotizacion objDACotizacion = new DACotizacion();
                SqlDataReader sqlDr = objDACotizacion.ListarxNomApe(pcApePaterno, pcApeMaterno, pcPriNonbre, pcSegNonbre, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idPeriodo");
                        Int32 nIndex2 = sqlDr.GetOrdinal("nroCotizacion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("nroDocumento");
                        Int32 nIndex4 = sqlDr.GetOrdinal("apePaterno");
                        Int32 nIndex5 = sqlDr.GetOrdinal("apeMaterno");
                        Int32 nIndex6 = sqlDr.GetOrdinal("priNombre");
                        Int32 nIndex7 = sqlDr.GetOrdinal("segNombre");
                        Int32 nIndex8 = sqlDr.GetOrdinal("telDomicilio1");
                        Int32 nIndex9 = sqlDr.GetOrdinal("telDomicilio2");
                        Int32 nIndex10 = sqlDr.GetOrdinal("telDomicilio3");
                        Int32 nIndex11 = sqlDr.GetOrdinal("telMovil1");
                        Int32 nIndex12 = sqlDr.GetOrdinal("telMovil2");
                        Int32 nIndex13 = sqlDr.GetOrdinal("telMovil3");
                        Int32 nIndex14 = sqlDr.GetOrdinal("telOficina1");
                        Int32 nIndex15 = sqlDr.GetOrdinal("telOficina2");
                        Int32 nIndex16 = sqlDr.GetOrdinal("telOficina3");
                        Int32 nIndex17 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex18 = sqlDr.GetOrdinal("desProducto");

                        lstBECotizacion = new List<BECotizacion>();

                        while (sqlDr.Read())
                        {
                            BECotizacion objBECotizacion = new BECotizacion();
                            objBECotizacion.IdPeriodo = sqlDr.GetInt32(nIndex1);
                            objBECotizacion.NroCotizacion = sqlDr.GetDecimal(nIndex2);
                            if (sqlDr.IsDBNull(nIndex3)) { objBECotizacion.NroDocumento = NullTypes.CadenaNull; } else { objBECotizacion.NroDocumento = sqlDr.GetString(nIndex3); }
                            objBECotizacion.ApePaterno = sqlDr.GetString(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBECotizacion.ApeMaterno = NullTypes.CadenaNull; } else { objBECotizacion.ApeMaterno = sqlDr.GetString(nIndex5); }
                            objBECotizacion.PriNombre = sqlDr.GetString(nIndex6);
                            if (sqlDr.IsDBNull(nIndex7)) { objBECotizacion.SegNombre = NullTypes.CadenaNull; } else { objBECotizacion.SegNombre = sqlDr.GetString(nIndex7); }
                            objBECotizacion.TelDomicilio1 = sqlDr.GetString(nIndex8);
                            if (sqlDr.IsDBNull(nIndex9)) { objBECotizacion.TelDomicilio2 = NullTypes.CadenaNull; } else { objBECotizacion.TelDomicilio2 = sqlDr.GetString(nIndex9); }
                            if (sqlDr.IsDBNull(nIndex10)) { objBECotizacion.TelDomicilio3 = NullTypes.CadenaNull; } else { objBECotizacion.TelDomicilio3 = sqlDr.GetString(nIndex10); }
                            if (sqlDr.IsDBNull(nIndex11)) { objBECotizacion.TelMovil1 = NullTypes.CadenaNull; } else { objBECotizacion.TelMovil1 = sqlDr.GetString(nIndex11); }
                            if (sqlDr.IsDBNull(nIndex12)) { objBECotizacion.TelMovil2 = NullTypes.CadenaNull; } else { objBECotizacion.TelMovil2 = sqlDr.GetString(nIndex12); }
                            if (sqlDr.IsDBNull(nIndex13)) { objBECotizacion.TelMovil3 = NullTypes.CadenaNull; } else { objBECotizacion.TelMovil3 = sqlDr.GetString(nIndex13); }
                            if (sqlDr.IsDBNull(nIndex14)) { objBECotizacion.TelOficina1 = NullTypes.CadenaNull; } else { objBECotizacion.TelOficina1 = sqlDr.GetString(nIndex14); }
                            if (sqlDr.IsDBNull(nIndex15)) { objBECotizacion.TelOficina2 = NullTypes.CadenaNull; } else { objBECotizacion.TelOficina2 = sqlDr.GetString(nIndex15); }
                            if (sqlDr.IsDBNull(nIndex16)) { objBECotizacion.TelOficina3 = NullTypes.CadenaNull; } else { objBECotizacion.TelOficina3 = sqlDr.GetString(nIndex16); }
                            if (sqlDr.IsDBNull(nIndex17)) { objBECotizacion.IdProducto = NullTypes.IntegerNull; } else { objBECotizacion.IdProducto = sqlDr.GetInt32(nIndex17); }
                            if (sqlDr.IsDBNull(nIndex18)) { objBECotizacion.DesProducto = NullTypes.CadenaNull; } else { objBECotizacion.DesProducto = sqlDr.GetString(nIndex18); }

                            lstBECotizacion.Add(objBECotizacion);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBECotizacion;
        }

        public BECotizacion Obtener(Int32 pnIdPeriodo, Decimal pnNroCotizacion)
        {
            BECotizacion objBECotizacion = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DACotizacion objDACotizacion = new DACotizacion();
                SqlDataReader sqlDr = objDACotizacion.Obtener(pnIdPeriodo, pnNroCotizacion, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idPeriodo");
                        Int32 nIndex2 = sqlDr.GetOrdinal("nroCotizacion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("apePaterno");
                        Int32 nIndex4 = sqlDr.GetOrdinal("apeMaterno");
                        Int32 nIndex5 = sqlDr.GetOrdinal("priNombre");
                        Int32 nIndex6 = sqlDr.GetOrdinal("segNombre");
                        Int32 nIndex7 = sqlDr.GetOrdinal("idTipoDocumento");
                        Int32 nIndex8 = sqlDr.GetOrdinal("nroDocumento");
                        Int32 nIndex9 = sqlDr.GetOrdinal("sexo");
                        Int32 nIndex10 = sqlDr.GetOrdinal("fecNacimiento");
                        Int32 nIndex11 = sqlDr.GetOrdinal("idCiudad");
                        Int32 nIndex12 = sqlDr.GetOrdinal("distrito");
                        Int32 nIndex13 = sqlDr.GetOrdinal("provincia");
                        Int32 nIndex14 = sqlDr.GetOrdinal("departamento");
                        Int32 nIndex15 = sqlDr.GetOrdinal("direccion");
                        Int32 nIndex16 = sqlDr.GetOrdinal("telDomicilio1");
                        Int32 nIndex17 = sqlDr.GetOrdinal("telDomicilio2");
                        Int32 nIndex18 = sqlDr.GetOrdinal("telDomicilio3");
                        Int32 nIndex19 = sqlDr.GetOrdinal("telMovil1");
                        Int32 nIndex20 = sqlDr.GetOrdinal("telMovil2");
                        Int32 nIndex21 = sqlDr.GetOrdinal("telMovil3");
                        Int32 nIndex22 = sqlDr.GetOrdinal("telOficina1");
                        Int32 nIndex23 = sqlDr.GetOrdinal("telOficina2");
                        Int32 nIndex24 = sqlDr.GetOrdinal("telOficina3");
                        Int32 nIndex25 = sqlDr.GetOrdinal("email1");
                        Int32 nIndex26 = sqlDr.GetOrdinal("email2");
                        Int32 nIndex27 = sqlDr.GetOrdinal("fecIniVigPoliza");
                        Int32 nIndex28 = sqlDr.GetOrdinal("fecFinVigPoliza");
                        Int32 nIndex29 = sqlDr.GetOrdinal("idMarca");
                        Int32 nIndex30 = sqlDr.GetOrdinal("desMarca");
                        Int32 nIndex31 = sqlDr.GetOrdinal("idModelo");
                        Int32 nIndex32 = sqlDr.GetOrdinal("desModelo");
                        Int32 nIndex33 = sqlDr.GetOrdinal("idClase");
                        Int32 nIndex34 = sqlDr.GetOrdinal("desClase");
                        Int32 nIndex35 = sqlDr.GetOrdinal("nroPlaca");
                        Int32 nIndex36 = sqlDr.GetOrdinal("anioFab");
                        Int32 nIndex37 = sqlDr.GetOrdinal("nroMotor");
                        Int32 nIndex38 = sqlDr.GetOrdinal("nroSerie");
                        Int32 nIndex39 = sqlDr.GetOrdinal("color");
                        Int32 nIndex40 = sqlDr.GetOrdinal("idUsoVehiculo");
                        Int32 nIndex41 = sqlDr.GetOrdinal("desUsoVehiculo");
                        Int32 nIndex42 = sqlDr.GetOrdinal("nroAsientos");
                        Int32 nIndex43 = sqlDr.GetOrdinal("esTimonCambiado");
                        Int32 nIndex44 = sqlDr.GetOrdinal("reqGPS");
                        Int32 nIndex45 = sqlDr.GetOrdinal("valOriVehiculo");
                        Int32 nIndex46 = sqlDr.GetOrdinal("valVehiculo");
                        Int32 nIndex47 = sqlDr.GetOrdinal("camValVehiculo");
                        Int32 nIndex48 = sqlDr.GetOrdinal("camPorcentaje");
                        Int32 nIndex49 = sqlDr.GetOrdinal("primaAnual");
                        Int32 nIndex50 = sqlDr.GetOrdinal("tasaAnual");
                        Int32 nIndex51 = sqlDr.GetOrdinal("primaBianual");
                        Int32 nIndex52 = sqlDr.GetOrdinal("tasaBianual");
                        Int32 nIndex53 = sqlDr.GetOrdinal("condicion");
                        Int32 nIndex54 = sqlDr.GetOrdinal("porcentajeC");
                        Int32 nIndex55 = sqlDr.GetOrdinal("primaAnualC");
                        Int32 nIndex56 = sqlDr.GetOrdinal("tasaAnualC");
                        Int32 nIndex57 = sqlDr.GetOrdinal("primaBianualC");
                        Int32 nIndex58 = sqlDr.GetOrdinal("tasaBianualC");
                        Int32 nIndex59 = sqlDr.GetOrdinal("idEstado");
                        Int32 nIndex60 = sqlDr.GetOrdinal("desEstado");
                        Int32 nIndex61 = sqlDr.GetOrdinal("reqInspeccion");
                        Int32 nIndex62 = sqlDr.GetOrdinal("dirInspeccion");
                        Int32 nIndex63 = sqlDr.GetOrdinal("fecHorInspeccion");
                        Int32 nIndex64 = sqlDr.GetOrdinal("fecValidez");
                        Int32 nIndex65 = sqlDr.GetOrdinal("canSiniestro1Anio");
                        Int32 nIndex66 = sqlDr.GetOrdinal("monSiniestro1Anio");
                        Int32 nIndex67 = sqlDr.GetOrdinal("canSiniestro2Anio");
                        Int32 nIndex68 = sqlDr.GetOrdinal("monSiniestro2Anio");
                        Int32 nIndex69 = sqlDr.GetOrdinal("canSiniestro3Anio");
                        Int32 nIndex70 = sqlDr.GetOrdinal("monSiniestro3Anio");
                        Int32 nIndex71 = sqlDr.GetOrdinal("canSiniestro4Anio");
                        Int32 nIndex72 = sqlDr.GetOrdinal("monSiniestro4Anio");
                        Int32 nIndex73 = sqlDr.GetOrdinal("canSiniestro5Anio");
                        Int32 nIndex74 = sqlDr.GetOrdinal("monSiniestro5Anio");                        
                        Int32 nIndex75 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex76 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex77 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex78 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex79 = sqlDr.GetOrdinal("stsRegistro");
                        Int32 nIndex80 = sqlDr.GetOrdinal("idTipoVehiculo");
                        Int32 nIndex81 = sqlDr.GetOrdinal("destipovehiculo");
                        Int32 nIndex82 = sqlDr.GetOrdinal("idCategoria");
                        Int32 nIndex83 = sqlDr.GetOrdinal("primaAnualMensual");
                        Int32 nIndex84 = sqlDr.GetOrdinal("primaBianualMensual");

                        objBECotizacion = new BECotizacion();

                        if (sqlDr.Read())
                        {
                            objBECotizacion.IdPeriodo = sqlDr.GetInt32(nIndex1);
                            objBECotizacion.NroCotizacion = sqlDr.GetDecimal(nIndex2);
                            objBECotizacion.ApePaterno = sqlDr.GetString(nIndex3);
                            objBECotizacion.ApeMaterno = sqlDr.GetString(nIndex4);
                            objBECotizacion.PriNombre = sqlDr.GetString(nIndex5);
                            if (sqlDr.IsDBNull(nIndex6)) { objBECotizacion.SegNombre = NullTypes.CadenaNull; } else { objBECotizacion.SegNombre = sqlDr.GetString(nIndex6); }
                            objBECotizacion.IdTipoDocumento = sqlDr.GetString(nIndex7);
                            objBECotizacion.NroDocumento = sqlDr.GetString(nIndex8);
                            if (sqlDr.IsDBNull(nIndex9)) { objBECotizacion.Sexo = NullTypes.CadenaNull; } else { objBECotizacion.Sexo = sqlDr.GetString(nIndex9); }
                            if (sqlDr.IsDBNull(nIndex10)) { objBECotizacion.FecNacimiento = NullTypes.FechaNull; } else { objBECotizacion.FecNacimiento = sqlDr.GetDateTime(nIndex10); }
                            objBECotizacion.IdCiudad = sqlDr.GetInt32(nIndex11);
                            if (sqlDr.IsDBNull(nIndex12)) { objBECotizacion.Distrito = NullTypes.CadenaNull; } else { objBECotizacion.Distrito = sqlDr.GetString(nIndex12); }
                            if (sqlDr.IsDBNull(nIndex13)) { objBECotizacion.Provincia = NullTypes.CadenaNull; } else { objBECotizacion.Provincia = sqlDr.GetString(nIndex13); }
                            if (sqlDr.IsDBNull(nIndex14)) { objBECotizacion.Departamento = NullTypes.CadenaNull; } else { objBECotizacion.Departamento = sqlDr.GetString(nIndex14); }
                            objBECotizacion.Direccion = sqlDr.GetString(nIndex15);
                            objBECotizacion.TelDomicilio1 = sqlDr.GetString(nIndex16);
                            if (sqlDr.IsDBNull(nIndex17)) { objBECotizacion.TelDomicilio2 = NullTypes.CadenaNull; } else { objBECotizacion.TelDomicilio2 = sqlDr.GetString(nIndex17); }
                            if (sqlDr.IsDBNull(nIndex18)) { objBECotizacion.TelDomicilio3 = NullTypes.CadenaNull; } else { objBECotizacion.TelDomicilio3 = sqlDr.GetString(nIndex18); }
                            if (sqlDr.IsDBNull(nIndex19)) { objBECotizacion.TelMovil1 = NullTypes.CadenaNull; } else { objBECotizacion.TelMovil1 = sqlDr.GetString(nIndex19); }
                            if (sqlDr.IsDBNull(nIndex20)) { objBECotizacion.TelMovil2 = NullTypes.CadenaNull; } else { objBECotizacion.TelMovil2 = sqlDr.GetString(nIndex20); }
                            if (sqlDr.IsDBNull(nIndex21)) { objBECotizacion.TelMovil3 = NullTypes.CadenaNull; } else { objBECotizacion.TelMovil3 = sqlDr.GetString(nIndex21); }
                            if (sqlDr.IsDBNull(nIndex22)) { objBECotizacion.TelOficina1 = NullTypes.CadenaNull; } else { objBECotizacion.TelOficina1 = sqlDr.GetString(nIndex22); }
                            if (sqlDr.IsDBNull(nIndex23)) { objBECotizacion.TelOficina2 = NullTypes.CadenaNull; } else { objBECotizacion.TelOficina2 = sqlDr.GetString(nIndex23); }
                            if (sqlDr.IsDBNull(nIndex24)) { objBECotizacion.TelOficina3 = NullTypes.CadenaNull; } else { objBECotizacion.TelOficina3 = sqlDr.GetString(nIndex24); }
                            if (sqlDr.IsDBNull(nIndex25)) { objBECotizacion.Email1 = NullTypes.CadenaNull; } else { objBECotizacion.Email1 = sqlDr.GetString(nIndex25); }
                            if (sqlDr.IsDBNull(nIndex26)) { objBECotizacion.Email2 = NullTypes.CadenaNull; } else { objBECotizacion.Email2 = sqlDr.GetString(nIndex26); }
                            if (sqlDr.IsDBNull(nIndex27)) { objBECotizacion.FecIniVigPoliza = NullTypes.FechaNull; } else { objBECotizacion.FecIniVigPoliza = sqlDr.GetDateTime(nIndex27); }
                            if (sqlDr.IsDBNull(nIndex28)) { objBECotizacion.FecFinVigPoliza = NullTypes.FechaNull; } else { objBECotizacion.FecFinVigPoliza = sqlDr.GetDateTime(nIndex28); }
                            objBECotizacion.IdMarca = sqlDr.GetInt32(nIndex29);
                            if (sqlDr.IsDBNull(nIndex30)) { objBECotizacion.DesMarca = NullTypes.CadenaNull; } else { objBECotizacion.DesMarca = sqlDr.GetString(nIndex30); }
                            objBECotizacion.IdModelo = sqlDr.GetInt32(nIndex31);
                            if (sqlDr.IsDBNull(nIndex32)) { objBECotizacion.DesModelo = NullTypes.CadenaNull; } else { objBECotizacion.DesModelo = sqlDr.GetString(nIndex32); }
                            objBECotizacion.IdClase = sqlDr.GetInt32(nIndex33);
                            if (sqlDr.IsDBNull(nIndex34)) { objBECotizacion.DesClase = NullTypes.CadenaNull; } else { objBECotizacion.DesClase = sqlDr.GetString(nIndex34); }
                            if (sqlDr.IsDBNull(nIndex35)) { objBECotizacion.NroPlaca = NullTypes.CadenaNull; } else { objBECotizacion.NroPlaca = sqlDr.GetString(nIndex35); }
                            objBECotizacion.AnioFab = sqlDr.GetInt32(nIndex36);
                            objBECotizacion.NroMotor = sqlDr.GetString(nIndex37);
                            if (sqlDr.IsDBNull(nIndex38)) { objBECotizacion.NroSerie = NullTypes.CadenaNull; } else { objBECotizacion.NroSerie = sqlDr.GetString(nIndex38); }
                            if (sqlDr.IsDBNull(nIndex39)) { objBECotizacion.Color = NullTypes.CadenaNull; } else { objBECotizacion.Color = sqlDr.GetString(nIndex39); }
                            if (sqlDr.IsDBNull(nIndex40)) { objBECotizacion.IdUsoVehiculo = NullTypes.IntegerNull; } else { objBECotizacion.IdUsoVehiculo = sqlDr.GetInt32(nIndex40); }
                            if (sqlDr.IsDBNull(nIndex41)) { objBECotizacion.DesUsoVehiculo = NullTypes.CadenaNull; } else { objBECotizacion.DesUsoVehiculo = sqlDr.GetString(nIndex41); }
                            if (sqlDr.IsDBNull(nIndex42)) { objBECotizacion.NroAsientos = NullTypes.IntegerNull; } else { objBECotizacion.NroAsientos = sqlDr.GetInt32(nIndex42); }
                            if (sqlDr.IsDBNull(nIndex43)) { objBECotizacion.EsTimonCambiado = false; } else { objBECotizacion.EsTimonCambiado = sqlDr.GetBoolean(nIndex43); }
                            if (sqlDr.IsDBNull(nIndex44)) { objBECotizacion.ReqGPS = false; } else { objBECotizacion.ReqGPS = sqlDr.GetBoolean(nIndex44); }
                            objBECotizacion.ValOriVehiculo = sqlDr.GetDecimal(nIndex45);
                            objBECotizacion.ValVehiculo = sqlDr.GetDecimal(nIndex46);
                            if (sqlDr.IsDBNull(nIndex47)) { objBECotizacion.CamValVehiculo = false; } else { objBECotizacion.CamValVehiculo = sqlDr.GetBoolean(nIndex47); }
                            if (sqlDr.IsDBNull(nIndex48)) { objBECotizacion.CamPorcentaje = NullTypes.DecimalNull; } else { objBECotizacion.CamPorcentaje = sqlDr.GetDecimal(nIndex48); }
                            if (sqlDr.IsDBNull(nIndex49)) { objBECotizacion.PrimaAnual = NullTypes.DecimalNull; } else { objBECotizacion.PrimaAnual = Decimal.Round(sqlDr.GetDecimal(nIndex49), 2); }
                            if (sqlDr.IsDBNull(nIndex50)) { objBECotizacion.TasaAnual = NullTypes.DecimalNull; } else { objBECotizacion.TasaAnual = Decimal.Round(sqlDr.GetDecimal(nIndex50), 2); }
                            if (sqlDr.IsDBNull(nIndex51)) { objBECotizacion.PrimaBianual = NullTypes.DecimalNull; } else { objBECotizacion.PrimaBianual = Decimal.Round(sqlDr.GetDecimal(nIndex51), 2); }
                            if (sqlDr.IsDBNull(nIndex52)) { objBECotizacion.TasaBianual = NullTypes.DecimalNull; } else { objBECotizacion.TasaBianual = sqlDr.GetDecimal(nIndex52); }
                            objBECotizacion.Condicion = sqlDr.GetInt32(nIndex53);
                            if (sqlDr.IsDBNull(nIndex54)) { objBECotizacion.PorcentajeC = NullTypes.DecimalNull; } else { objBECotizacion.PorcentajeC = sqlDr.GetDecimal(nIndex54); }
                            if (sqlDr.IsDBNull(nIndex55)) { objBECotizacion.PrimaAnualC = NullTypes.DecimalNull; } else { objBECotizacion.PrimaAnualC = sqlDr.GetDecimal(nIndex55); }
                            if (sqlDr.IsDBNull(nIndex56)) { objBECotizacion.TasaAnualC = NullTypes.DecimalNull; } else { objBECotizacion.TasaAnualC = Decimal.Round(sqlDr.GetDecimal(nIndex56), 2); }
                            if (sqlDr.IsDBNull(nIndex57)) { objBECotizacion.PrimaBianualC = NullTypes.DecimalNull; } else { objBECotizacion.PrimaBianualC = sqlDr.GetDecimal(nIndex57); }
                            if (sqlDr.IsDBNull(nIndex58)) { objBECotizacion.TasaBianualC = NullTypes.DecimalNull; } else { objBECotizacion.TasaBianualC = sqlDr.GetDecimal(nIndex58); }
                            objBECotizacion.IdEstado = sqlDr.GetInt32(nIndex59);
                            if (sqlDr.IsDBNull(nIndex60)) { objBECotizacion.DesEstado = NullTypes.CadenaNull; } else { objBECotizacion.DesEstado = sqlDr.GetString(nIndex60); }
                            objBECotizacion.ReqInspeccion = sqlDr.GetBoolean(nIndex61);
                            if (sqlDr.IsDBNull(nIndex62)) { objBECotizacion.DirInspeccion = NullTypes.CadenaNull; } else { objBECotizacion.DirInspeccion = sqlDr.GetString(nIndex62); }
                            if (sqlDr.IsDBNull(nIndex63)) { objBECotizacion.FecHorInspeccion = NullTypes.CadenaNull; } else { objBECotizacion.FecHorInspeccion = sqlDr.GetString(nIndex63); }
                            if (sqlDr.IsDBNull(nIndex64)) { objBECotizacion.FecValidez = NullTypes.FechaNull; } else { objBECotizacion.FecValidez = sqlDr.GetDateTime(nIndex64); }
                            if (sqlDr.IsDBNull(nIndex65)) { objBECotizacion.CanSiniestro1Anio = NullTypes.IntegerNull; } else { objBECotizacion.CanSiniestro1Anio = sqlDr.GetInt32(nIndex65); }
                            if (sqlDr.IsDBNull(nIndex66)) { objBECotizacion.MonSiniestro1Anio = NullTypes.DecimalNull; } else { objBECotizacion.MonSiniestro1Anio = sqlDr.GetDecimal(nIndex66); }
                            if (sqlDr.IsDBNull(nIndex67)) { objBECotizacion.CanSiniestro2Anio = NullTypes.IntegerNull; } else { objBECotizacion.CanSiniestro2Anio = sqlDr.GetInt32(nIndex67); }
                            if (sqlDr.IsDBNull(nIndex68)) { objBECotizacion.MonSiniestro2Anio = NullTypes.DecimalNull; } else { objBECotizacion.MonSiniestro2Anio = sqlDr.GetDecimal(nIndex68); }
                            if (sqlDr.IsDBNull(nIndex69)) { objBECotizacion.CanSiniestro3Anio = NullTypes.IntegerNull; } else { objBECotizacion.CanSiniestro3Anio = sqlDr.GetInt32(nIndex69); }
                            if (sqlDr.IsDBNull(nIndex70)) { objBECotizacion.MonSiniestro3Anio = NullTypes.DecimalNull; } else { objBECotizacion.MonSiniestro3Anio = sqlDr.GetDecimal(nIndex70); }
                            if (sqlDr.IsDBNull(nIndex71)) { objBECotizacion.CanSiniestro4Anio = NullTypes.IntegerNull; } else { objBECotizacion.CanSiniestro4Anio = sqlDr.GetInt32(nIndex71); }
                            if (sqlDr.IsDBNull(nIndex72)) { objBECotizacion.MonSiniestro4Anio = NullTypes.DecimalNull; } else { objBECotizacion.MonSiniestro4Anio = sqlDr.GetDecimal(nIndex72); }
                            if (sqlDr.IsDBNull(nIndex73)) { objBECotizacion.CanSiniestro5Anio = NullTypes.IntegerNull; } else { objBECotizacion.CanSiniestro5Anio = sqlDr.GetInt32(nIndex73); }
                            if (sqlDr.IsDBNull(nIndex74)) { objBECotizacion.MonSiniestro5Anio = NullTypes.DecimalNull; } else { objBECotizacion.MonSiniestro5Anio = sqlDr.GetDecimal(nIndex74); }
                            objBECotizacion.UsuarioCreacion = sqlDr.GetString(nIndex75);
                            objBECotizacion.FechaCreacion = sqlDr.GetDateTime(nIndex76);
                            if (sqlDr.IsDBNull(nIndex77)) { objBECotizacion.UsuarioModificacion = NullTypes.CadenaNull; } else { objBECotizacion.UsuarioModificacion = sqlDr.GetString(nIndex77); }
                            if (sqlDr.IsDBNull(nIndex78)) { objBECotizacion.FechaModificacion = NullTypes.FechaNull; } else { objBECotizacion.FechaModificacion = sqlDr.GetDateTime(nIndex78); }
                            objBECotizacion.EstadoRegistro = sqlDr.GetBoolean(nIndex79);
                            if (sqlDr.IsDBNull(nIndex80)) { objBECotizacion.IdTipoVehiculo = NullTypes.IntegerNull; } else { objBECotizacion.IdTipoVehiculo = sqlDr.GetInt32(nIndex80); };
                            objBECotizacion.DesTipoVehiculo = sqlDr.GetString(nIndex81);
                            objBECotizacion.IdCategoria = sqlDr.GetInt32(nIndex82);
                            objBECotizacion.PrimaAnualMensual = sqlDr.GetDecimal(nIndex83);
                            if (sqlDr.IsDBNull(nIndex84)) { objBECotizacion.PrimaBianualMensual = NullTypes.DecimalNull; } else { objBECotizacion.PrimaBianualMensual = sqlDr.GetDecimal(nIndex84); }  
                            
                        }

                        sqlDr.Close();
                    }
                }
            }

            return objBECotizacion;
        }

        public BECotizacion ObtenerSpeech(Int32 pnIdPeriodo, Decimal pnNroCotizacion, String pcSaludo, String pcCondicion, String pcConfirmacion, String pcValVehiculo)
        {
            BECotizacion objBECotizacion = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DACotizacion objDACotizacion = new DACotizacion();
                SqlDataReader sqlDr = objDACotizacion.Obtener(pnIdPeriodo, pnNroCotizacion, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idPeriodo");
                        Int32 nIndex2 = sqlDr.GetOrdinal("nroCotizacion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("apePaterno");
                        Int32 nIndex4 = sqlDr.GetOrdinal("apeMaterno");
                        Int32 nIndex5 = sqlDr.GetOrdinal("priNombre");
                        Int32 nIndex6 = sqlDr.GetOrdinal("segNombre");
                        Int32 nIndex7 = sqlDr.GetOrdinal("idTipoDocumento");
                        Int32 nIndex8 = sqlDr.GetOrdinal("nroDocumento");
                        Int32 nIndex9 = sqlDr.GetOrdinal("sexo");
                        Int32 nIndex10 = sqlDr.GetOrdinal("fecNacimiento");
                        Int32 nIndex11 = sqlDr.GetOrdinal("idCiudad");
                        Int32 nIndex12 = sqlDr.GetOrdinal("distrito");
                        Int32 nIndex13 = sqlDr.GetOrdinal("provincia");
                        Int32 nIndex14 = sqlDr.GetOrdinal("departamento");
                        Int32 nIndex15 = sqlDr.GetOrdinal("direccion");
                        Int32 nIndex16 = sqlDr.GetOrdinal("telDomicilio1");
                        Int32 nIndex17 = sqlDr.GetOrdinal("telDomicilio2");
                        Int32 nIndex18 = sqlDr.GetOrdinal("telDomicilio3");
                        Int32 nIndex19 = sqlDr.GetOrdinal("telMovil1");
                        Int32 nIndex20 = sqlDr.GetOrdinal("telMovil2");
                        Int32 nIndex21 = sqlDr.GetOrdinal("telMovil3");
                        Int32 nIndex22 = sqlDr.GetOrdinal("telOficina1");
                        Int32 nIndex23 = sqlDr.GetOrdinal("telOficina2");
                        Int32 nIndex24 = sqlDr.GetOrdinal("telOficina3");
                        Int32 nIndex25 = sqlDr.GetOrdinal("email1");
                        Int32 nIndex26 = sqlDr.GetOrdinal("email2");
                        Int32 nIndex27 = sqlDr.GetOrdinal("fecIniVigPoliza");
                        Int32 nIndex28 = sqlDr.GetOrdinal("fecFinVigPoliza");
                        Int32 nIndex29 = sqlDr.GetOrdinal("idMarca");
                        Int32 nIndex30 = sqlDr.GetOrdinal("desMarca");
                        Int32 nIndex31 = sqlDr.GetOrdinal("idModelo");
                        Int32 nIndex32 = sqlDr.GetOrdinal("desModelo");
                        Int32 nIndex33 = sqlDr.GetOrdinal("idClase");
                        Int32 nIndex34 = sqlDr.GetOrdinal("desClase");
                        Int32 nIndex35 = sqlDr.GetOrdinal("nroPlaca");
                        Int32 nIndex36 = sqlDr.GetOrdinal("anioFab");
                        Int32 nIndex37 = sqlDr.GetOrdinal("nroMotor");
                        Int32 nIndex38 = sqlDr.GetOrdinal("nroSerie");
                        Int32 nIndex39 = sqlDr.GetOrdinal("color");
                        Int32 nIndex40 = sqlDr.GetOrdinal("idUsoVehiculo");
                        Int32 nIndex41 = sqlDr.GetOrdinal("desUsoVehiculo");
                        Int32 nIndex42 = sqlDr.GetOrdinal("nroAsientos");
                        Int32 nIndex43 = sqlDr.GetOrdinal("esTimonCambiado");
                        Int32 nIndex44 = sqlDr.GetOrdinal("reqGPS");
                        Int32 nIndex45 = sqlDr.GetOrdinal("valOriVehiculo");
                        Int32 nIndex46 = sqlDr.GetOrdinal("valVehiculo");
                        Int32 nIndex47 = sqlDr.GetOrdinal("camValVehiculo");
                        Int32 nIndex48 = sqlDr.GetOrdinal("camPorcentaje");
                        Int32 nIndex49 = sqlDr.GetOrdinal("primaAnual");
                        Int32 nIndex50 = sqlDr.GetOrdinal("tasaAnual");
                        Int32 nIndex51 = sqlDr.GetOrdinal("primaBianual");
                        Int32 nIndex52 = sqlDr.GetOrdinal("tasaBianual");
                        Int32 nIndex53 = sqlDr.GetOrdinal("condicion");
                        Int32 nIndex54 = sqlDr.GetOrdinal("porcentajeC");
                        Int32 nIndex55 = sqlDr.GetOrdinal("primaAnualC");
                        Int32 nIndex56 = sqlDr.GetOrdinal("tasaAnualC");
                        Int32 nIndex57 = sqlDr.GetOrdinal("primaBianualC");
                        Int32 nIndex58 = sqlDr.GetOrdinal("tasaBianualC");
                        Int32 nIndex59 = sqlDr.GetOrdinal("idEstado");
                        Int32 nIndex60 = sqlDr.GetOrdinal("desEstado");
                        Int32 nIndex61 = sqlDr.GetOrdinal("reqInspeccion");
                        Int32 nIndex62 = sqlDr.GetOrdinal("dirInspeccion");
                        Int32 nIndex63 = sqlDr.GetOrdinal("fecHorInspeccion");
                        Int32 nIndex64 = sqlDr.GetOrdinal("fecValidez");
                        Int32 nIndex65 = sqlDr.GetOrdinal("canSiniestro1Anio");
                        Int32 nIndex66 = sqlDr.GetOrdinal("monSiniestro1Anio");
                        Int32 nIndex67 = sqlDr.GetOrdinal("canSiniestro2Anio");
                        Int32 nIndex68 = sqlDr.GetOrdinal("monSiniestro2Anio");
                        Int32 nIndex69 = sqlDr.GetOrdinal("canSiniestro3Anio");
                        Int32 nIndex70 = sqlDr.GetOrdinal("monSiniestro3Anio");
                        Int32 nIndex71 = sqlDr.GetOrdinal("canSiniestro4Anio");
                        Int32 nIndex72 = sqlDr.GetOrdinal("monSiniestro4Anio");
                        Int32 nIndex73 = sqlDr.GetOrdinal("canSiniestro5Anio");
                        Int32 nIndex74 = sqlDr.GetOrdinal("monSiniestro5Anio");
                        Int32 nIndex75 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex76 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex77 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex78 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex79 = sqlDr.GetOrdinal("stsRegistro");
                        Int32 nIndex80 = sqlDr.GetOrdinal("idTipoVehiculo");
                        Int32 nIndex81 = sqlDr.GetOrdinal("destipovehiculo");
                        Int32 nIndex82 = sqlDr.GetOrdinal("idCategoria");
                        Int32 nIndex83 = sqlDr.GetOrdinal("primaAnualMensual");
                        Int32 nIndex84 = sqlDr.GetOrdinal("primaBianualMensual");

                        objBECotizacion = new BECotizacion();

                        if (sqlDr.Read())
                        {
                            objBECotizacion.IdPeriodo = sqlDr.GetInt32(nIndex1);
                            objBECotizacion.NroCotizacion = sqlDr.GetDecimal(nIndex2);
                            objBECotizacion.ApePaterno = sqlDr.GetString(nIndex3);
                            objBECotizacion.ApeMaterno = sqlDr.GetString(nIndex4);
                            objBECotizacion.PriNombre = sqlDr.GetString(nIndex5);
                            if (sqlDr.IsDBNull(nIndex6)) { objBECotizacion.SegNombre = NullTypes.CadenaNull; } else { objBECotizacion.SegNombre = sqlDr.GetString(nIndex6); }
                            objBECotizacion.IdTipoDocumento = sqlDr.GetString(nIndex7);
                            objBECotizacion.NroDocumento = sqlDr.GetString(nIndex8);
                            if (sqlDr.IsDBNull(nIndex9)) { objBECotizacion.Sexo = NullTypes.CadenaNull; } else { objBECotizacion.Sexo = sqlDr.GetString(nIndex9); }
                            if (sqlDr.IsDBNull(nIndex10)) { objBECotizacion.FecNacimiento = NullTypes.FechaNull; } else { objBECotizacion.FecNacimiento = sqlDr.GetDateTime(nIndex10); }
                            objBECotizacion.IdCiudad = sqlDr.GetInt32(nIndex11);
                            if (sqlDr.IsDBNull(nIndex12)) { objBECotizacion.Distrito = NullTypes.CadenaNull; } else { objBECotizacion.Distrito = sqlDr.GetString(nIndex12); }
                            if (sqlDr.IsDBNull(nIndex13)) { objBECotizacion.Provincia = NullTypes.CadenaNull; } else { objBECotizacion.Provincia = sqlDr.GetString(nIndex13); }
                            if (sqlDr.IsDBNull(nIndex14)) { objBECotizacion.Departamento = NullTypes.CadenaNull; } else { objBECotizacion.Departamento = sqlDr.GetString(nIndex14); }
                            objBECotizacion.Direccion = sqlDr.GetString(nIndex15);
                            objBECotizacion.TelDomicilio1 = sqlDr.GetString(nIndex16);
                            if (sqlDr.IsDBNull(nIndex17)) { objBECotizacion.TelDomicilio2 = NullTypes.CadenaNull; } else { objBECotizacion.TelDomicilio2 = sqlDr.GetString(nIndex17); }
                            if (sqlDr.IsDBNull(nIndex18)) { objBECotizacion.TelDomicilio3 = NullTypes.CadenaNull; } else { objBECotizacion.TelDomicilio3 = sqlDr.GetString(nIndex18); }
                            if (sqlDr.IsDBNull(nIndex19)) { objBECotizacion.TelMovil1 = NullTypes.CadenaNull; } else { objBECotizacion.TelMovil1 = sqlDr.GetString(nIndex19); }
                            if (sqlDr.IsDBNull(nIndex20)) { objBECotizacion.TelMovil2 = NullTypes.CadenaNull; } else { objBECotizacion.TelMovil2 = sqlDr.GetString(nIndex20); }
                            if (sqlDr.IsDBNull(nIndex21)) { objBECotizacion.TelMovil3 = NullTypes.CadenaNull; } else { objBECotizacion.TelMovil3 = sqlDr.GetString(nIndex21); }
                            if (sqlDr.IsDBNull(nIndex22)) { objBECotizacion.TelOficina1 = NullTypes.CadenaNull; } else { objBECotizacion.TelOficina1 = sqlDr.GetString(nIndex22); }
                            if (sqlDr.IsDBNull(nIndex23)) { objBECotizacion.TelOficina2 = NullTypes.CadenaNull; } else { objBECotizacion.TelOficina2 = sqlDr.GetString(nIndex23); }
                            if (sqlDr.IsDBNull(nIndex24)) { objBECotizacion.TelOficina3 = NullTypes.CadenaNull; } else { objBECotizacion.TelOficina3 = sqlDr.GetString(nIndex24); }
                            if (sqlDr.IsDBNull(nIndex25)) { objBECotizacion.Email1 = NullTypes.CadenaNull; } else { objBECotizacion.Email1 = sqlDr.GetString(nIndex25); }
                            if (sqlDr.IsDBNull(nIndex26)) { objBECotizacion.Email2 = NullTypes.CadenaNull; } else { objBECotizacion.Email2 = sqlDr.GetString(nIndex26); }
                            if (sqlDr.IsDBNull(nIndex27)) { objBECotizacion.FecIniVigPoliza = NullTypes.FechaNull; } else { objBECotizacion.FecIniVigPoliza = sqlDr.GetDateTime(nIndex27); }
                            if (sqlDr.IsDBNull(nIndex28)) { objBECotizacion.FecFinVigPoliza = NullTypes.FechaNull; } else { objBECotizacion.FecFinVigPoliza = sqlDr.GetDateTime(nIndex28); }
                            objBECotizacion.IdMarca = sqlDr.GetInt32(nIndex29);
                            if (sqlDr.IsDBNull(nIndex30)) { objBECotizacion.DesMarca = NullTypes.CadenaNull; } else { objBECotizacion.DesMarca = sqlDr.GetString(nIndex30); }
                            objBECotizacion.IdModelo = sqlDr.GetInt32(nIndex31);
                            if (sqlDr.IsDBNull(nIndex32)) { objBECotizacion.DesModelo = NullTypes.CadenaNull; } else { objBECotizacion.DesModelo = sqlDr.GetString(nIndex32); }
                            objBECotizacion.IdClase = sqlDr.GetInt32(nIndex33);
                            if (sqlDr.IsDBNull(nIndex34)) { objBECotizacion.DesClase = NullTypes.CadenaNull; } else { objBECotizacion.DesClase = sqlDr.GetString(nIndex34); }
                            if (sqlDr.IsDBNull(nIndex35)) { objBECotizacion.NroPlaca = NullTypes.CadenaNull; } else { objBECotizacion.NroPlaca = sqlDr.GetString(nIndex35); }
                            objBECotizacion.AnioFab = sqlDr.GetInt32(nIndex36);
                            objBECotizacion.NroMotor = sqlDr.GetString(nIndex37);
                            if (sqlDr.IsDBNull(nIndex38)) { objBECotizacion.NroSerie = NullTypes.CadenaNull; } else { objBECotizacion.NroSerie = sqlDr.GetString(nIndex38); }
                            if (sqlDr.IsDBNull(nIndex39)) { objBECotizacion.Color = NullTypes.CadenaNull; } else { objBECotizacion.Color = sqlDr.GetString(nIndex39); }
                            if (sqlDr.IsDBNull(nIndex40)) { objBECotizacion.IdUsoVehiculo = NullTypes.IntegerNull; } else { objBECotizacion.IdUsoVehiculo = sqlDr.GetInt32(nIndex40); }
                            if (sqlDr.IsDBNull(nIndex41)) { objBECotizacion.DesUsoVehiculo = NullTypes.CadenaNull; } else { objBECotizacion.DesUsoVehiculo = sqlDr.GetString(nIndex41); }
                            if (sqlDr.IsDBNull(nIndex42)) { objBECotizacion.NroAsientos = NullTypes.IntegerNull; } else { objBECotizacion.NroAsientos = sqlDr.GetInt32(nIndex42); }
                            if (sqlDr.IsDBNull(nIndex43)) { objBECotizacion.EsTimonCambiado = false; } else { objBECotizacion.EsTimonCambiado = sqlDr.GetBoolean(nIndex43); }
                            if (sqlDr.IsDBNull(nIndex44)) { objBECotizacion.ReqGPS = false; } else { objBECotizacion.ReqGPS = sqlDr.GetBoolean(nIndex44); }
                            objBECotizacion.ValOriVehiculo = sqlDr.GetDecimal(nIndex45);
                            objBECotizacion.ValVehiculo = sqlDr.GetDecimal(nIndex46);
                            if (sqlDr.IsDBNull(nIndex47)) { objBECotizacion.CamValVehiculo = false; } else { objBECotizacion.CamValVehiculo = sqlDr.GetBoolean(nIndex47); }
                            if (sqlDr.IsDBNull(nIndex48)) { objBECotizacion.CamPorcentaje = NullTypes.DecimalNull; } else { objBECotizacion.CamPorcentaje = sqlDr.GetDecimal(nIndex48); }
                            if (sqlDr.IsDBNull(nIndex49)) { objBECotizacion.PrimaAnual = NullTypes.DecimalNull; } else { objBECotizacion.PrimaAnual = sqlDr.GetDecimal(nIndex49); }
                            if (sqlDr.IsDBNull(nIndex50)) { objBECotizacion.TasaAnual = NullTypes.DecimalNull; } else { objBECotizacion.TasaAnual = sqlDr.GetDecimal(nIndex50); }
                            if (sqlDr.IsDBNull(nIndex51)) { objBECotizacion.PrimaBianual = NullTypes.DecimalNull; } else { objBECotizacion.PrimaBianual = sqlDr.GetDecimal(nIndex51); }
                            if (sqlDr.IsDBNull(nIndex52)) { objBECotizacion.TasaBianual = NullTypes.DecimalNull; } else { objBECotizacion.TasaBianual = sqlDr.GetDecimal(nIndex52); }
                            objBECotizacion.Condicion = sqlDr.GetInt32(nIndex53);
                            if (sqlDr.IsDBNull(nIndex54)) { objBECotizacion.PorcentajeC = NullTypes.DecimalNull; } else { objBECotizacion.PorcentajeC = sqlDr.GetDecimal(nIndex54); }
                            if (sqlDr.IsDBNull(nIndex55)) { objBECotizacion.PrimaAnualC = NullTypes.DecimalNull; } else { objBECotizacion.PrimaAnualC = sqlDr.GetDecimal(nIndex55); }
                            if (sqlDr.IsDBNull(nIndex56)) { objBECotizacion.TasaAnualC = NullTypes.DecimalNull; } else { objBECotizacion.TasaAnualC = sqlDr.GetDecimal(nIndex56); }
                            if (sqlDr.IsDBNull(nIndex57)) { objBECotizacion.PrimaBianualC = NullTypes.DecimalNull; } else { objBECotizacion.PrimaBianualC = sqlDr.GetDecimal(nIndex57); }
                            if (sqlDr.IsDBNull(nIndex58)) { objBECotizacion.TasaBianualC = NullTypes.DecimalNull; } else { objBECotizacion.TasaBianualC = sqlDr.GetDecimal(nIndex58); }
                            objBECotizacion.IdEstado = sqlDr.GetInt32(nIndex59);
                            if (sqlDr.IsDBNull(nIndex60)) { objBECotizacion.DesEstado = NullTypes.CadenaNull; } else { objBECotizacion.DesEstado = sqlDr.GetString(nIndex60); }
                            objBECotizacion.ReqInspeccion = sqlDr.GetBoolean(nIndex61);
                            if (sqlDr.IsDBNull(nIndex62)) { objBECotizacion.DirInspeccion = NullTypes.CadenaNull; } else { objBECotizacion.DirInspeccion = sqlDr.GetString(nIndex62); }
                            if (sqlDr.IsDBNull(nIndex63)) { objBECotizacion.FecHorInspeccion = NullTypes.CadenaNull; } else { objBECotizacion.FecHorInspeccion = sqlDr.GetString(nIndex63); }
                            if (sqlDr.IsDBNull(nIndex64)) { objBECotizacion.FecValidez = NullTypes.FechaNull; } else { objBECotizacion.FecValidez = sqlDr.GetDateTime(nIndex64); }
                            if (sqlDr.IsDBNull(nIndex65)) { objBECotizacion.CanSiniestro1Anio = NullTypes.IntegerNull; } else { objBECotizacion.CanSiniestro1Anio = sqlDr.GetInt32(nIndex65); }
                            if (sqlDr.IsDBNull(nIndex66)) { objBECotizacion.MonSiniestro1Anio = NullTypes.DecimalNull; } else { objBECotizacion.MonSiniestro1Anio = sqlDr.GetDecimal(nIndex66); }
                            if (sqlDr.IsDBNull(nIndex67)) { objBECotizacion.CanSiniestro2Anio = NullTypes.IntegerNull; } else { objBECotizacion.CanSiniestro2Anio = sqlDr.GetInt32(nIndex67); }
                            if (sqlDr.IsDBNull(nIndex68)) { objBECotizacion.MonSiniestro2Anio = NullTypes.DecimalNull; } else { objBECotizacion.MonSiniestro2Anio = sqlDr.GetDecimal(nIndex68); }
                            if (sqlDr.IsDBNull(nIndex69)) { objBECotizacion.CanSiniestro3Anio = NullTypes.IntegerNull; } else { objBECotizacion.CanSiniestro3Anio = sqlDr.GetInt32(nIndex69); }
                            if (sqlDr.IsDBNull(nIndex70)) { objBECotizacion.MonSiniestro3Anio = NullTypes.DecimalNull; } else { objBECotizacion.MonSiniestro3Anio = sqlDr.GetDecimal(nIndex70); }
                            if (sqlDr.IsDBNull(nIndex71)) { objBECotizacion.CanSiniestro4Anio = NullTypes.IntegerNull; } else { objBECotizacion.CanSiniestro4Anio = sqlDr.GetInt32(nIndex71); }
                            if (sqlDr.IsDBNull(nIndex72)) { objBECotizacion.MonSiniestro4Anio = NullTypes.DecimalNull; } else { objBECotizacion.MonSiniestro4Anio = sqlDr.GetDecimal(nIndex72); }
                            if (sqlDr.IsDBNull(nIndex73)) { objBECotizacion.CanSiniestro5Anio = NullTypes.IntegerNull; } else { objBECotizacion.CanSiniestro5Anio = sqlDr.GetInt32(nIndex73); }
                            if (sqlDr.IsDBNull(nIndex74)) { objBECotizacion.MonSiniestro5Anio = NullTypes.DecimalNull; } else { objBECotizacion.MonSiniestro5Anio = sqlDr.GetDecimal(nIndex74); }
                            objBECotizacion.UsuarioCreacion = sqlDr.GetString(nIndex75);
                            objBECotizacion.FechaCreacion = sqlDr.GetDateTime(nIndex76);
                            if (sqlDr.IsDBNull(nIndex77)) { objBECotizacion.UsuarioModificacion = NullTypes.CadenaNull; } else { objBECotizacion.UsuarioModificacion = sqlDr.GetString(nIndex77); }
                            if (sqlDr.IsDBNull(nIndex78)) { objBECotizacion.FechaModificacion = NullTypes.FechaNull; } else { objBECotizacion.FechaModificacion = sqlDr.GetDateTime(nIndex78); }
                            objBECotizacion.EstadoRegistro = sqlDr.GetBoolean(nIndex79);
                            if (sqlDr.IsDBNull(nIndex80)) { objBECotizacion.IdTipoVehiculo = NullTypes.IntegerNull; } else { objBECotizacion.IdTipoVehiculo = sqlDr.GetInt32(nIndex80); };
                            objBECotizacion.DesTipoVehiculo = sqlDr.GetString(nIndex81);
                            objBECotizacion.IdCategoria = sqlDr.GetInt32(nIndex82);
                            objBECotizacion.PrimaAnualMensual = sqlDr.GetDecimal(nIndex83);
                            if (sqlDr.IsDBNull(nIndex84)) { objBECotizacion.PrimaBianualMensual = NullTypes.DecimalNull; } else { objBECotizacion.PrimaBianualMensual = sqlDr.GetDecimal(nIndex84); }  
                            //objBECotizacion.PrimaBianualMensual = sqlDr.GetDecimal(nIndex84);

                            objBECotizacion.ValorString1 = pcSaludo;
                            objBECotizacion.ValorString2 = pcCondicion;
                            objBECotizacion.ValorString3 = pcConfirmacion;
                            objBECotizacion.ValorString4 = pcValVehiculo;  
                        }

                        sqlDr.Close();
                    }
                }
            }

            return objBECotizacion;
        }

        public List<BECotizacion> Cotizar(Int32 pnIdProducto, Int32 pnIdModelo, Int32 pnAnioFab, Decimal pnValVehiculo, Int32 pnIdMarca, String pcMarca, String pcModelo, Boolean pbTimon, String pcScoring, Decimal pnPorc, Decimal pnPrimaMin) 
        {

            List<BECotizacion> lstBECotizacion = new List<BECotizacion>();

            if (pnIdModelo != 0 && pnAnioFab != 0 && pnValVehiculo != 0 && pnIdMarca != 0) 
            {
                using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
                {
                    sqlCn.Open();

                    Decimal nTasaA = NullTypes.DecimalNull;
                    Decimal nTasaB = NullTypes.DecimalNull;

                    BLTasa obBLTasa = new BLTasa();
                    BETasa objBETasaA = obBLTasa.Obtener(pnIdModelo, 1, pnAnioFab);

                    if (objBETasaA != null)
                    {
                        nTasaA = objBETasaA.ValorTasa;
                    }

                    BETasa objBETasaB = obBLTasa.Obtener(pnIdModelo, 2, pnAnioFab);

                    if (objBETasaB != null)
                    {
                        nTasaB = objBETasaB.ValorTasa;
                    }

                    DACotizacion objDACotizacion = new DACotizacion();
                    SqlDataReader sqlDr = objDACotizacion.Cotizar(pnIdProducto, pnIdModelo, pnAnioFab, pnValVehiculo, nTasaA, nTasaB, pbTimon, pcScoring, pnPorc, pnPrimaMin, sqlCn);

                    if (sqlDr != null)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idModelo");
                        Int32 nIndex3 = sqlDr.GetOrdinal("valorVeh");
                        Int32 nIndex4 = sqlDr.GetOrdinal("anioFab");
                        Int32 nIndex5 = sqlDr.GetOrdinal("tasaAnual");
                        Int32 nIndex6 = sqlDr.GetOrdinal("primaAnual");
                        Int32 nIndex7 = sqlDr.GetOrdinal("tasaBinual");
                        Int32 nIndex8 = sqlDr.GetOrdinal("primaBianual");

                        lstBECotizacion = new List<BECotizacion>();

                        while (sqlDr.Read())
                        {
                            BECotizacion objBECotizacion = new BECotizacion();
                            objBECotizacion.IdProducto = sqlDr.GetInt32(nIndex1);
                            objBECotizacion.IdModelo = sqlDr.GetInt32(nIndex2);
                            objBECotizacion.ValVehiculo = sqlDr.GetDecimal(nIndex3);
                            objBECotizacion.AnioFab = sqlDr.GetInt32(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBECotizacion.TasaAnual = NullTypes.DecimalNull; } else { objBECotizacion.TasaAnual = sqlDr.GetDecimal(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBECotizacion.PrimaAnual = NullTypes.DecimalNull; } else { objBECotizacion.PrimaAnual = sqlDr.GetDecimal(nIndex6); }
                            if (sqlDr.IsDBNull(nIndex7)) { objBECotizacion.TasaBianual = NullTypes.DecimalNull; } else { objBECotizacion.TasaBianual = sqlDr.GetDecimal(nIndex7); }
                            if (sqlDr.IsDBNull(nIndex8)) { objBECotizacion.TasaBianual = NullTypes.DecimalNull; } else { objBECotizacion.PrimaBianual = sqlDr.GetDecimal(nIndex8); } 
                            objBECotizacion.IdMarca = pnIdMarca;
                            objBECotizacion.DesMarca = pcMarca;
                            objBECotizacion.DesModelo = pcModelo; 

                            lstBECotizacion.Add(objBECotizacion); 
                        }

                        sqlDr.Close();
                    }
                }
            }                        
            return lstBECotizacion;
        }

        public BECotizacion Cotizar(Int32 pnIdProducto, Int32 pnIdModelo, Int32 pnAnioFab, Decimal pnValVehiculo, Boolean pbTimon, String pcScoring, Decimal pnPorc, Decimal pnPrimaMinima)
        {

            BECotizacion objBECotizacion = null;
            
                using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
                {
                    sqlCn.Open();

                    Decimal nTasaA = NullTypes.DecimalNull;
                    Decimal nTasaB = NullTypes.DecimalNull;

                    BLTasa obBLTasa = new BLTasa();
                    BETasa objBETasaA = obBLTasa.Obtener(pnIdModelo, 1, pnAnioFab);

                    if (objBETasaA != null)
                    {
                        nTasaA = objBETasaA.ValorTasa;
                    }

                    BETasa objBETasaB = obBLTasa.Obtener(pnIdModelo, 2, pnAnioFab);

                    if (objBETasaB != null)
                    {
                        nTasaB = objBETasaB.ValorTasa;
                    }

                    DACotizacion objDACotizacion = new DACotizacion();
                    SqlDataReader sqlDr = objDACotizacion.Cotizar(pnIdProducto, pnIdModelo, pnAnioFab, pnValVehiculo, nTasaA, nTasaB, pbTimon, pcScoring, pnPorc, pnPrimaMinima, sqlCn);

                    if (sqlDr != null)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idModelo");
                        Int32 nIndex3 = sqlDr.GetOrdinal("valorVeh");
                        Int32 nIndex4 = sqlDr.GetOrdinal("anioFab");
                        Int32 nIndex5 = sqlDr.GetOrdinal("tasaAnual");
                        Int32 nIndex6 = sqlDr.GetOrdinal("primaAnual");
                        Int32 nIndex7 = sqlDr.GetOrdinal("tasaBinual");
                        Int32 nIndex8 = sqlDr.GetOrdinal("primaBianual");
                       
                        if (sqlDr.Read())
                        {
                            objBECotizacion = new BECotizacion();
                            objBECotizacion.IdProducto = sqlDr.GetInt32(nIndex1);
                            objBECotizacion.IdModelo = sqlDr.GetInt32(nIndex2);
                            objBECotizacion.ValVehiculo = sqlDr.GetDecimal(nIndex3);
                            objBECotizacion.AnioFab = sqlDr.GetInt32(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBECotizacion.TasaAnual = NullTypes.DecimalNull; } else { objBECotizacion.TasaAnual = sqlDr.GetDecimal(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBECotizacion.PrimaAnual = NullTypes.DecimalNull; } else { objBECotizacion.PrimaAnual = sqlDr.GetDecimal(nIndex6); }
                            if (sqlDr.IsDBNull(nIndex7)) { objBECotizacion.TasaBianual = NullTypes.DecimalNull; } else { objBECotizacion.TasaBianual = sqlDr.GetDecimal(nIndex7); }
                            if (sqlDr.IsDBNull(nIndex8)) { objBECotizacion.TasaBianual = NullTypes.DecimalNull; } else { objBECotizacion.PrimaBianual = sqlDr.GetDecimal(nIndex8); }                            
                        }
                        sqlDr.Close();
                    }
                }
            
            return objBECotizacion;
        }

        public List<BECotizacion> Cotizar(Int32 pnIdProducto, Int32 pnIdModelo, Int32 pnAnioFab, Decimal pnValorVeh, Boolean pbTimon)
        {
            List<BECotizacion> lstBECotizacion = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus())) 
            {
                sqlCn.Open();

                DACotizacion objDACotizacion = new DACotizacion();
                SqlDataReader sqlDr = objDACotizacion.Cotizar(pnIdProducto, pnIdModelo, pnAnioFab, pnValorVeh, pbTimon, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idModelo");
                        Int32 nIndex3 = sqlDr.GetOrdinal("valorVeh");
                        Int32 nIndex4 = sqlDr.GetOrdinal("anioFab");
                        Int32 nIndex5 = sqlDr.GetOrdinal("idTasa");
                        Int32 nIndex6 = sqlDr.GetOrdinal("valorTasa");
                        Int32 nIndex7 = sqlDr.GetOrdinal("primaTotal");
                        Int32 nIndex8 = sqlDr.GetOrdinal("primaMensual");
                        Int32 nIndex9 = sqlDr.GetOrdinal("opcion");

                        lstBECotizacion = new List<BECotizacion>();

                        while (sqlDr.Read()) 
                        {
                            BECotizacion objBECotizacion = new BECotizacion();
                            objBECotizacion.IdProducto = sqlDr.GetInt32(nIndex1);
                            objBECotizacion.IdModelo = sqlDr.GetInt32(nIndex2);
                            objBECotizacion.ValOriVehiculo = sqlDr.GetInt32(nIndex3);
                            objBECotizacion.AnioFab = sqlDr.GetInt32(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBECotizacion.IdTasa = NullTypes.IntegerNull; } else { objBECotizacion.IdTasa = sqlDr.GetInt32(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex5)) { objBECotizacion.IdTasa = NullTypes.IntegerNull; } else { objBECotizacion.IdTasa = sqlDr.GetInt32(nIndex5); } 
                        }
                    }
                }
            }

            return lstBECotizacion;
        }  

        public List<BECotizacion> Listar() 
        {
            List<BECotizacion> lstBECotizacion = new List<BECotizacion>();
            return lstBECotizacion;
        }

        public List<BECotizacion> ListarTipo() 
        {
            List<BECotizacion> lstBECotizacion = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DACotizacion objDACotizacion = new DACotizacion();
                SqlDataReader sqlDr = objDACotizacion.ListarTipo(sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows) 
                    {
                        lstBECotizacion = new List<BECotizacion>();

                        Int32 nIndex1 = sqlDr.GetOrdinal("idTipo");
                        Int32 nIndex2 = sqlDr.GetOrdinal("descripcion");

                        while(sqlDr.Read()) 
                        {
                            BECotizacion objBECotizacion = new BECotizacion();
                            objBECotizacion.IdTipo = sqlDr.GetInt32(nIndex1);
                            objBECotizacion.DesTipo = sqlDr.GetString(nIndex2);
                            lstBECotizacion.Add(objBECotizacion);
                        }
                    }
                }
            }

            return lstBECotizacion;
        }

        //new2013
        public List<BECotizacion> GenerarCotizacion(Int32 pnTipoCot, Int32 pnIdProducto, Int32 pnIdSponsor, Int32 pnIdGrupProd,
                                                    Decimal pnValorVehiculo, Int32 pnIdMarca, Int32 pnIdModelo, Int32 pnAnioFab, Boolean pbTimonCamb, Boolean pbGPS,
                                                    Boolean pbScoring, Decimal pnPorcentValVeh, Int32 pnIdCiudad, 
                                                    Int32 pnIdDescuento, Int32 pnIdRecargo, Int32 pnIdAdicional, String pcIdAseguradores,
                                                    String pcParam1, String pcParam2, String pcParam3, String pcParam4, String pcParam5, 
                                                    String pcParam6, String pcParam7, String pcParam8, String pcParam9, String pcParam10)
        {
            List<BECotizacion> lstBECotizacion = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DACotizacion objDACotizacion = new DACotizacion();
                SqlDataReader sqlDr = objDACotizacion.GenerarCotizacion(pnTipoCot, pnIdProducto, pnIdSponsor, pnIdGrupProd,                                                                        pnValorVehiculo, pnIdMarca, pnIdModelo, pnAnioFab, pbTimonCamb, pbGPS,
                                                                        pbScoring, pnPorcentValVeh, pnIdCiudad,
                                                                        pnIdDescuento, pnIdRecargo, pnIdAdicional, pcIdAseguradores,
                                                                        pcParam1, pcParam2, pcParam3, pcParam4, pcParam5,
                                                                        pcParam6, pcParam7, pcParam8, pcParam9, pcParam10, 
                                                                        sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        lstBECotizacion = new List<BECotizacion>();
                        
                        Int32 nIndex = sqlDr.GetOrdinal("idCorrelativo");
                        Int32 nIndex0 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex1 = sqlDr.GetOrdinal("producto");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idAsegurador");
                        Int32 nIndex3 = sqlDr.GetOrdinal("asegurador");
                        Int32 nIndex4 = sqlDr.GetOrdinal("idCategoria");
                        Int32 nIndex5 = sqlDr.GetOrdinal("desCategoria");
                        Int32 nIndex6 = sqlDr.GetOrdinal("seguroPlan");
                        Int32 nIndex7 = sqlDr.GetOrdinal("idTasa");
                        Int32 nIndex8 = sqlDr.GetOrdinal("capital");
                        Int32 nIndex9 = sqlDr.GetOrdinal("valorTasa");
                        Int32 nIndex10 = sqlDr.GetOrdinal("tasaPorc");
                        Int32 nIndex11 = sqlDr.GetOrdinal("costoAnual");
                        Int32 nIndex12 = sqlDr.GetOrdinal("porcRecargo");
                        Int32 nIndex13 = sqlDr.GetOrdinal("importeRecargo");
                        Int32 nIndex14 = sqlDr.GetOrdinal("porcDescuento");
                        Int32 nIndex15 = sqlDr.GetOrdinal("importeDescuento");
                        Int32 nIndex16 = sqlDr.GetOrdinal("primaAnualFinal");
                        Int32 nIndex17 = sqlDr.GetOrdinal("primaAnualMensualFinal");
                        Int32 nIndex18 = sqlDr.GetOrdinal("rutaLogoAseg");
                        Int32 nIndex19 = sqlDr.GetOrdinal("idPromocion");
                        Int32 nIndex20 = sqlDr.GetOrdinal("desPromocion");

                        Int32 nIndex21 = sqlDr.GetOrdinal("IDDescuento");
                        Int32 nIndex22 = sqlDr.GetOrdinal("IDRecargo");
                        Int32 nIndex23 = sqlDr.GetOrdinal("IDAdicional");
                        Int32 nIndex24 = sqlDr.GetOrdinal("MontoAdicional");
                        Int32 nIndex25 = sqlDr.GetOrdinal("PagoContado");

                       
             

                        while (sqlDr.Read())
                        {
                            BECotizacion objBECotizacion = new BECotizacion();
                            objBECotizacion.Correlativo = sqlDr.GetInt32(nIndex);
                            objBECotizacion.IdProducto = sqlDr.GetInt32(nIndex0);
                            objBECotizacion.DesProducto = sqlDr.GetString(nIndex1);
                            objBECotizacion.IdAsegurador = sqlDr.GetInt32(nIndex2);
                            objBECotizacion.DesAsegurador = sqlDr.GetString(nIndex3);
                            objBECotizacion.IdCategoria = sqlDr.GetInt32(nIndex4);
                            objBECotizacion.DesCategoria = sqlDr.GetString(nIndex5);
                            objBECotizacion.SeguroPlan = sqlDr.GetString(nIndex6);
                            objBECotizacion.IdTasa = sqlDr.GetInt32(nIndex7);
                            objBECotizacion.ValVehiculo = sqlDr.GetDecimal(nIndex8);
                            objBECotizacion.Tasa = sqlDr.GetDecimal(nIndex9);
                            objBECotizacion.TasaPorc = sqlDr.GetDecimal(nIndex10);
                            objBECotizacion.CostoAnual = sqlDr.GetDecimal(nIndex11);
                            objBECotizacion.PorcRecargo = sqlDr.GetDecimal(nIndex12);
                            objBECotizacion.ImporteRecargo = sqlDr.GetDecimal(nIndex13);
                            objBECotizacion.PorcDescuento = sqlDr.GetDecimal(nIndex14);
                            objBECotizacion.ImporteDescuento = sqlDr.GetDecimal(nIndex15);
                            objBECotizacion.PrimaAnual = sqlDr.GetDecimal(nIndex16);
                            objBECotizacion.PrimaAnualMensual = sqlDr.GetDecimal(nIndex17);
                            objBECotizacion.RutaLogoAseg = sqlDr.GetString(nIndex18);
                            if (sqlDr.IsDBNull(nIndex19)) { objBECotizacion.IdPromocion = NullTypes.IntegerNull; } else { objBECotizacion.IdPromocion = sqlDr.GetInt32(nIndex19); }
                            if (sqlDr.IsDBNull(nIndex20)) { objBECotizacion.DesPromocion = NullTypes.CadenaNull; } else { objBECotizacion.DesPromocion = sqlDr.GetString(nIndex20); }

                            if (sqlDr.IsDBNull(nIndex21)) { objBECotizacion.IdDescuento = NullTypes.IntegerNull; } else { objBECotizacion.IdDescuento = sqlDr.GetInt32(nIndex21); }
                            if (sqlDr.IsDBNull(nIndex22)) { objBECotizacion.IdRecargo = NullTypes.IntegerNull; } else { objBECotizacion.IdRecargo = sqlDr.GetInt32(nIndex22); }
                            if (sqlDr.IsDBNull(nIndex23)) { objBECotizacion.IdAdicional = NullTypes.IntegerNull; } else { objBECotizacion.IdAdicional = sqlDr.GetInt32(nIndex23); }
                            if (sqlDr.IsDBNull(nIndex24)) { objBECotizacion.MontoAdicional = NullTypes.DecimalNull; } else { objBECotizacion.MontoAdicional = sqlDr.GetInt32(nIndex23); }
                            objBECotizacion.PagoContado = sqlDr.GetBoolean(nIndex25);

                            lstBECotizacion.Add(objBECotizacion);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBECotizacion;
        }

        //new2013
        public List<BECotizacionDetalle> ObtenerDetalle(Decimal pnNroCotizacion)
        {
            List<BECotizacionDetalle> lstBECotizacionDetalle = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DACotizacion objDACotizacion = new DACotizacion();
                SqlDataReader sqlDr = objDACotizacion.ObtenerDetalle(pnNroCotizacion, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nindex1 = sqlDr.GetOrdinal("nroCotizacion");
                        Int32 nindex2 = sqlDr.GetOrdinal("desProducto");
                        Int32 nindex3 = sqlDr.GetOrdinal("desAsegurador");
                        Int32 nindex4 = sqlDr.GetOrdinal("rutaLogo");
                        Int32 nindex5 = sqlDr.GetOrdinal("priNombre");
                        Int32 nindex6 = sqlDr.GetOrdinal("segNombre");
                        Int32 nindex7 = sqlDr.GetOrdinal("apePaterno");
                        Int32 nindex8 = sqlDr.GetOrdinal("apeMaterno");
                        Int32 nindex9 = sqlDr.GetOrdinal("idTipoDocumento");
                        Int32 nindex10 = sqlDr.GetOrdinal("nroDocumento");
	                    Int32 nindex11 = sqlDr.GetOrdinal("idMarca");
                        Int32 nindex12= sqlDr.GetOrdinal("desMarca");
                        Int32 nindex13 = sqlDr.GetOrdinal("idModelo");
                        Int32 nindex14 = sqlDr.GetOrdinal("desModelo");
                        Int32 nindex15 = sqlDr.GetOrdinal("anioFab");
                        Int32 nindex16 = sqlDr.GetOrdinal("valOriVehiculo");
	                    Int32 nindex17 = sqlDr.GetOrdinal("correlativo");
                        Int32 nindex18 = sqlDr.GetOrdinal("primaMensual");
                        Int32 nindex19 = sqlDr.GetOrdinal("primaTotal");
                        Int32 nindex20 = sqlDr.GetOrdinal("porcDescuento");
                        Int32 nindex21 = sqlDr.GetOrdinal("impDescuento");
                        Int32 nIndex22 = sqlDr.GetOrdinal("porcRecargo");
                        Int32 nindex23 = sqlDr.GetOrdinal("impRecargo");
                        Int32 nindex24 = sqlDr.GetOrdinal("opcionPlan");
	                    Int32 nindex25 = sqlDr.GetOrdinal("idTasa");
                        Int32 nindex26 = sqlDr.GetOrdinal("porcTasa");
                        Int32 nIndex27 = sqlDr.GetOrdinal("elegido");
                        Int32 nIndex28 = sqlDr.GetOrdinal("nroPlaca");

                        lstBECotizacionDetalle = new List<BECotizacionDetalle>();

                        while (sqlDr.Read()) 
                        {
                            BECotizacionDetalle objBECotizacionDetalle = new BECotizacionDetalle();
                            objBECotizacionDetalle.NroCotizacion = sqlDr.GetDecimal(nindex1);
                            objBECotizacionDetalle.DesProducto = sqlDr.GetString(nindex2);
                            objBECotizacionDetalle.DesAsegurador = sqlDr.GetString(nindex3);
                            objBECotizacionDetalle.RutaLogo = sqlDr.GetString(nindex4);
                            objBECotizacionDetalle.PriNombre = sqlDr.GetString(nindex5);
                            if (sqlDr.IsDBNull(nindex6)) { objBECotizacionDetalle.SegNombre = NullTypes.CadenaNull; } else { objBECotizacionDetalle.SegNombre = sqlDr.GetString(nindex6); }
                            objBECotizacionDetalle.ApePaterno = sqlDr.GetString(nindex7);
                            if (sqlDr.IsDBNull(nindex8)) { objBECotizacionDetalle.ApeMaterno = NullTypes.CadenaNull; } else { objBECotizacionDetalle.ApeMaterno = sqlDr.GetString(nindex8); }
                            objBECotizacionDetalle.IdTipoDocumento = sqlDr.GetString(nindex9);
                            objBECotizacionDetalle.NroDocumento = sqlDr.GetString(nindex10);
                            objBECotizacionDetalle.IdMarca = sqlDr.GetInt32(nindex11);
                            objBECotizacionDetalle.DesMarca = sqlDr.GetString(nindex12);
                            objBECotizacionDetalle.IdModelo = sqlDr.GetInt32(nindex13);
                            objBECotizacionDetalle.DesModelo = sqlDr.GetString(nindex14);
                            objBECotizacionDetalle.AnioFab = sqlDr.GetInt32(nindex15);
                            objBECotizacionDetalle.ValorVehiculo = sqlDr.GetDecimal(nindex16);
                            objBECotizacionDetalle.Correlativo = sqlDr.GetInt32(nindex17);
                            objBECotizacionDetalle.PrimaMensual = sqlDr.GetDecimal(nindex18);
                            objBECotizacionDetalle.PrimaTotal = sqlDr.GetDecimal(nindex19);
                            objBECotizacionDetalle.PorcDescuento = sqlDr.GetDecimal(nindex20);
                            objBECotizacionDetalle.ImpDescuento = sqlDr.GetDecimal(nindex21);
                            objBECotizacionDetalle.PorcRecargo = sqlDr.GetDecimal(nIndex22);
                            objBECotizacionDetalle.ImpRecargo = sqlDr.GetDecimal(nindex23);
                            objBECotizacionDetalle.OpcionPlan = sqlDr.GetString(nindex24);
                            objBECotizacionDetalle.IdTasa = sqlDr.GetInt32(nindex25);
                            objBECotizacionDetalle.PorcTasa = sqlDr.GetDecimal(nindex26);
                            objBECotizacionDetalle.Elegido = sqlDr.GetBoolean(nIndex27);
                            objBECotizacionDetalle.NroPlaca = sqlDr.GetString(nIndex28);

                            lstBECotizacionDetalle.Add(objBECotizacionDetalle);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBECotizacionDetalle;
        }
        //new2013
        public Int32 ActualizarElegido(Decimal pnNroCotizacion, Int32 pnCorrelativo, Boolean pbElegido, String pcUsuario)
        {
            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DACotizacion objDACotizacion = new DACotizacion();

                nResult = objDACotizacion.ActualizarElegido(pnNroCotizacion, pnCorrelativo, pbElegido, pcUsuario, sqlCn);
            }

            return nResult;
        }
        #endregion       

        #region Funciones Privadas
        /// <summary>
        /// Permite definir si se obtendra el valor del vehículo de la tabla APESEG.
        /// </summary>
        /// <returns>True Si usar APESEG | False No usar APESEG</returns>
        private Boolean UsarAPESEG()
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.:2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.:____-__-__
            ---------------------------------------------------------------------------*/

            Boolean bResult = false;
            Int32 nCodParAPSEG = 7;

            BLParametro objBLParametro = new BLParametro();
            BEParametro objBEParametro = objBLParametro.Obtener(nCodParAPSEG);

            if (objBEParametro == null)
            {
                return bResult;
            }

            if (objBEParametro.ValorBool == NullTypes.BoolNull)
            {
                return bResult;
            }

            if (objBEParametro.ValorBool == true)
            {
                bResult = true;
            }

            return bResult;
        }

        /// <summary>
        /// Permite obtener si el indicador si cliente requiere inspección.
        /// </summary>
        /// <param name="pnIdAsegurador">Código de asegurador.</param>
        /// <returns>True requiere inspección. | False no requiere inspección.</returns>
        private Boolean RequiereInspeccion(Int32 pnIdAsegurador)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Boolean bResult = false;

            if (pnIdAsegurador != Convert.ToInt32(ValorConstante.CodigoAsegurador.RIMAC))
            {
                bResult = true;
            }

            return bResult;
        }

        /// <summary>
        /// Permite validar la siniestralidad del cliente.
        /// </summary>
        /// <param name="pObjBEVigenciaSiniestro"></param>
        /// <returns>True existe siniestro | False no existe siniestro.</returns>
        private Boolean ExisteSiniestro(BEVigenciaSiniestro pObjBEVigenciaSiniestro)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Boolean bResult = false;

            //Si la cantidad de vigencias es 1.
            if (pObjBEVigenciaSiniestro.CantVigencia == 1)
            {
                if (pObjBEVigenciaSiniestro.CantSiniestro1Anio > 0)
                {
                    bResult = true;
                }
            }
            //Si la cantidad de vigencias es 2.
            if (pObjBEVigenciaSiniestro.CantVigencia == 2)
            {
                if (pObjBEVigenciaSiniestro.CantSiniestro1Anio > 0)
                {
                    bResult = true;
                }

                if (pObjBEVigenciaSiniestro.CantSiniestro2Anio > 0)
                {
                    bResult = true;
                }
            }
            //Si la cantidad de vigencias es 3.
            if (pObjBEVigenciaSiniestro.CantVigencia == 3)
            {
                if (pObjBEVigenciaSiniestro.CantSiniestro1Anio > 0)
                {
                    bResult = true;
                }

                if (pObjBEVigenciaSiniestro.CantSiniestro2Anio > 0)
                {
                    bResult = true;
                }

                if (pObjBEVigenciaSiniestro.CantSiniestro3Anio > 0)
                {
                    bResult = true;
                }
            }
            //Si la cantidad de vigencias es 4.
            if (pObjBEVigenciaSiniestro.CantVigencia == 4)
            {
                if (pObjBEVigenciaSiniestro.CantSiniestro1Anio > 0)
                {
                    bResult = true;
                }

                if (pObjBEVigenciaSiniestro.CantSiniestro2Anio > 0)
                {
                    bResult = true;
                }

                if (pObjBEVigenciaSiniestro.CantSiniestro3Anio > 0)
                {
                    bResult = true;
                }

                if (pObjBEVigenciaSiniestro.CantSiniestro4Anio > 0)
                {
                    bResult = true;
                }
            }
            //Si la cantidad de vigencias es 5 a más.
            if (pObjBEVigenciaSiniestro.CantVigencia >= 5)
            {
                if (pObjBEVigenciaSiniestro.CantSiniestro1Anio > 0)
                {
                    bResult = true;
                }

                if (pObjBEVigenciaSiniestro.CantSiniestro2Anio > 0)
                {
                    bResult = true;
                }

                if (pObjBEVigenciaSiniestro.CantSiniestro3Anio > 0)
                {
                    bResult = true;
                }

                if (pObjBEVigenciaSiniestro.CantSiniestro4Anio > 0)
                {
                    bResult = true;
                }

                if (pObjBEVigenciaSiniestro.CantSiniestro5Anio > 0)
                {
                    bResult = true;
                }
            }

            return bResult;
        }        

        /// <summary>
        /// Permite calcular la prima.
        /// </summary>
        /// <param name="pObjBETasa">Información de tasa.</param>
        /// <param name="pnValorVehiculo">Valor del vehículo.</param>
        /// <returns>Prima calculada</returns>
        private Decimal CalcularPrima(BETasa pObjBETasa, Decimal pnValorVehiculo)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Decimal nResult = NullTypes.DecimalNull;
            Decimal nEmision = Convert.ToDecimal("1.2154"); 

            if (pObjBETasa != null)
            {
                nResult = pnValorVehiculo * (pObjBETasa.ValorTasa / 100) * nEmision;
            }

            return nResult;
        }

        /// <summary>
        /// Permite retornar el valor de la tasa.
        /// </summary>
        /// <param name="pObjBETasa">Objeto que contiene la infromación de la tasa.</param>
        /// <returns>Valor de la tasa.</returns>
        private Decimal RetornarTasa(BETasa pObjBETasa)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Decimal nResult = NullTypes.DecimalNull;

            if (pObjBETasa != null)
            {
                nResult = pObjBETasa.ValorTasa;
            }
            return nResult;
        }

        /// <summary>
        /// Parmite calcular la tasa por condición.
        /// </summary>
        /// <param name="bSiniestro">Indicador de siniestro.</param>
        /// <param name="pObjBECliente">Objeto que contiene la información del cliente.</param>
        /// <param name="pObjBEVigenciaSiniestro">Objeto que contiene la información de la vigencia y siniestro.</param>
        /// <param name="pnValorTasa">Valor de la tasa.</param>
        /// <param name="pnValorVehiculo">Valor del vehiculo</param>
        /// <param name="pnPorcentaje">Porcentaje de condición.</param>
        /// <returns>Tasa aplicada la condición.</returns>
        private Decimal CalcularTasaCondicion(Boolean bSiniestro, BECliente pObjBECliente, BEVigenciaSiniestro pObjBEVigenciaSiniestro, Decimal pnValorTasa, ref Decimal pnPorcentaje)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Decimal nTasa = 0;

            //Si el cliente ralizará la renocacion del seguro es cliente de RIMAC,
            //se calcula los mosntos de descuento en tasa.
            if (pObjBECliente.CodAsegurador == 1)
            {
                BLDescuentoRecargo objBLDescuentoRecargo = new BLDescuentoRecargo();
                BEDescuentoRecargo objBEDescuentoRecargo = null;

                //Si no pose siniestros en vigencias anteriores,
                //se procede a calcular el descuento en la tasa.
                if (bSiniestro == false)
                {
                    objBEDescuentoRecargo = objBLDescuentoRecargo.Obtener(ValorConstante.TipoDescuento, pObjBEVigenciaSiniestro.CantVigencia);

                    //Si existe porcentaje de descuento registrado, 
                    //se calcula el valor de la nueva tasa y prima.                        
                    if (objBEDescuentoRecargo != null)
                    {
                        pnPorcentaje = objBEDescuentoRecargo.Porcentaje;
                        nTasa = pnValorTasa - (pnValorTasa * (objBEDescuentoRecargo.Porcentaje / 100));
                        //nTasa = pnValorVehiculo * (nNuevaTasa / 100);
                    }
                    else
                    {
                        nTasa = pnValorTasa;
                    }
                }
                else
                {
                    objBEDescuentoRecargo = objBLDescuentoRecargo.Obtener(ValorConstante.TipoRecargo, pObjBEVigenciaSiniestro.CantSiniestro1Anio);

                    //Si existe porcentaje de recargo registrado, 
                    //se calcula el valor de la nueva tasa y prima. 
                    if (objBEDescuentoRecargo != null)
                    {
                        pnPorcentaje = objBEDescuentoRecargo.Porcentaje;
                        nTasa = pnValorTasa + (pnValorTasa * (objBEDescuentoRecargo.Porcentaje / 100));
                        //nTasa = pnValorVehiculo * (nNuevaTasa / 100);
                    }
                    else
                    {
                        nTasa = pnValorTasa;
                    }
                }
            }

            return nTasa;
        }

        /// <summary>
        /// Permite obtener el porcentaje de depreciación segun la antigüedad del vehículo.
        /// </summary>
        /// <param name="pnAntiguedad">antigüedad del vehículo.</param>
        /// <param name="pObjBEParametro_Men1">Parámetro de porcentaje de depreciación menor iguial a 1 año de antigüedad.</param>
        /// <param name="pObjBEParametro_May1">Parámetro de porcentaje de depreciación mayor a 1 año de antigúedad.</param>
        /// <returns>Tasa de depreciación.</returns>
        private Decimal ObtenerPorcDep(Int32 pnAntiguedad, BEParametro pObjBEParametro_Men1, BEParametro pObjBEParametro_May1)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Decimal nPorcentaje = 0;

            if (pnAntiguedad <= 1)
            {
                nPorcentaje = pObjBEParametro_Men1.ValorNumeroDec;
            }
            else
            {
                nPorcentaje = pObjBEParametro_May1.ValorNumeroDec;
            }

            return nPorcentaje;
        }

        /// <summary>
        /// Permite validar si registro de porcentaje de depreciación es activo, segun la antigüedad del vehículo.
        /// </summary>
        /// <param name="nAntiguedad">Antigüedad del vehículo.</param>
        /// <param name="pObjBEParametro_Men1">Parámetro de porcentaje de depreciación menor iguial a 1 año de antigüedad.</param>
        /// <param name="pObjBEParametro_May1">Parámetro de porcentaje de depreciación mayor a 1 año de antigüedad.</param>
        /// <returns>True porcentaje activo. | False porcentaje no activo.</returns>
        private Boolean ValidarTasDepActiva(Int32 nAntiguedad, BEParametro pObjBEParametro_Men1, BEParametro pObjBEParametro_May1)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Boolean bResult = true;

            if (nAntiguedad <= 1)
            {
                if (pObjBEParametro_Men1.EstadoRegistro == false)
                {
                    bResult = false;
                }
            }
            else
            {
                if (pObjBEParametro_May1.EstadoRegistro == false)
                {
                    bResult = false;
                }
            }
            return bResult;
        }

        /// <summary>
        /// Permite obtener la antigüedad del vehículo según el año de fabricación.
        /// </summary>
        /// <param name="nAnioFab">Año de fabricación.</param>
        /// <returns>Antigüedad del vehículo.</returns>
        private Int32 ObtenerAntiguedad(Int32 nAnioFab)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Int32 nAntiguedad = DateTime.Now.Year - nAnioFab;
            return nAntiguedad;
        }

        /// <summary>
        /// Permite obtener la prima
        /// </summary>
        /// <param name="nTasa">Valor de tasa.</param>
        /// <param name="pnValorVehiculo">Valor del vehículo.</param>
        /// <param name="pnPrimaMinima">Prima mínima.</param>
        /// <returns>El valor de la prima.</returns>
        private Decimal ObtenerPrima(Decimal pnTasa, Decimal pnValorVehiculo, Decimal pnPrimaMinima)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Decimal nResult;
            Decimal nPorcEmision = Decimal.Round(Convert.ToDecimal("1.2154"), 4);
            nResult = pnValorVehiculo * (pnTasa / 100) * (nPorcEmision);

            //Si no existe prima mínima. retornar la prima calculada.
            if (pnPrimaMinima == NullTypes.DecimalNull)
            {
                return nResult;
            }

            //Si prima calculada es menor que la prima mínima. la prima sera la prima mínima
            if (nResult < pnPrimaMinima)
            {
                nResult = pnPrimaMinima;
            }

            return nResult;
        }

        /// <summary>
        /// Permite obtener la categoría segun las tasas encontradas.
        /// </summary>
        /// <param name="pObjBETasa_Anual">Objeto que contiene la información de la tasa anual.</param>
        /// <param name="pObjBETasa_Bianual">Objeto que contiene la información de la tasa bianual.</param>
        /// <returns>El código de categoría.</returns>
        private Int32 ObtenerCategoria(BETasa pObjBETasa_Anual, BETasa pObjBETasa_Bianual)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Int32 nResult = 0;

            if (pObjBETasa_Anual != null)
            {
                nResult = pObjBETasa_Anual.IdCategoria;
            }
            else
            {
                nResult = pObjBETasa_Bianual.IdCategoria;
            }

            return nResult;
        }

        /// <summary>
        /// Permite cargar los datos de la cotización para ser registrada.
        /// </summary>
        /// <param name="pObjBECotizacion">Objeto que contiene la información de la cotización.</param>
        /// <param name="pObjBECliente">Objeto que contiene la información del cliente.</param>
        /// <returns>El nro. de filas afectadas.</returns>
        private Int32 CargarCotizacion(BECotizacion pObjBECotizacion, BECliente pObjBECliente)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Int32 nResult = 0;
            String cLog = String.Empty;
            pObjBECotizacion.ApePaterno = pObjBECliente.ApePaterno;
            pObjBECotizacion.ApeMaterno = pObjBECliente.ApeMaterno;
            pObjBECotizacion.PriNombre = pObjBECliente.PriNombre;
            pObjBECotizacion.SegNombre = pObjBECliente.SegNombre;
            pObjBECotizacion.IdTipoDocumento = pObjBECliente.IdTipoDocumento;
            pObjBECotizacion.NroDocumento = pObjBECliente.NroDocumento;
            pObjBECotizacion.Sexo = pObjBECliente.Sexo;
            pObjBECotizacion.FecNacimiento = pObjBECliente.FecNacimiento;
            pObjBECotizacion.IdCiudad = pObjBECliente.idCiudad;
            pObjBECotizacion.Direccion = pObjBECliente.Direccion;
            pObjBECotizacion.TelDomicilio1 = pObjBECliente.TelDomicilio1;
            pObjBECotizacion.TelDomicilio2 = pObjBECliente.TelDomicilio2;
            pObjBECotizacion.TelDomicilio3 = pObjBECliente.TelDomicilio3;
            pObjBECotizacion.TelMovil1 = pObjBECliente.TelMovil1;
            pObjBECotizacion.TelMovil2 = pObjBECliente.TelMovil2;
            pObjBECotizacion.TelMovil3 = pObjBECliente.TelMovil2;
            pObjBECotizacion.TelOficina1 = pObjBECliente.TelOficina1;
            pObjBECotizacion.TelOficina2 = pObjBECliente.TelOficina2;
            pObjBECotizacion.TelOficina3 = pObjBECliente.TelOficina3;
            pObjBECotizacion.Email1 = pObjBECliente.Email1;
            pObjBECotizacion.Email2 = pObjBECliente.Email2;
            //pObjBECotizacion.FecIniVigPoliza = pObjBECliente.FecVigencia;
            //pObjBECotizacion.FecIniVigPoliza = pObjBECliente.FecVigencia;
            //pObjBECotizacion.FecFinVigPoliza = pObjBECliente.FecFinVigencia;
            pObjBECotizacion.IdMarca = pObjBECliente.IdMarca;
            pObjBECotizacion.IdModelo = pObjBECliente.IdModelo;
            pObjBECotizacion.IdClase = pObjBECliente.IdClase;
            pObjBECotizacion.NroPlaca = pObjBECliente.NroPlaca;
            pObjBECotizacion.AnioFab = pObjBECliente.AnioFab;
            pObjBECotizacion.NroMotor = pObjBECliente.NroMotor;
            pObjBECotizacion.NroSerie = pObjBECliente.NroSerie;
            pObjBECotizacion.Color = pObjBECliente.Color;
            pObjBECotizacion.IdUsoVehiculo = pObjBECliente.IdUsoVehiculo;
            pObjBECotizacion.NroAsientos = pObjBECliente.NroAsientos;
            pObjBECotizacion.EsTimonCambiado = pObjBECliente.EsTimonCambiado;
            pObjBECotizacion.ReqGPS = pObjBECliente.ReqGPS;
            pObjBECotizacion.ValOriVehiculo = pObjBECotizacion.ValVehiculo;
            //pObjBECotizacion.ValVehiculo = pObjBECliente.ValorVehiculo; X3
            pObjBECotizacion.IdEstado = Convert.ToInt32(ValorConstante.EstadoCotizacion.Creada);
            pObjBECotizacion.ReqInspeccion = this.RequiereInspeccion(pObjBECliente.CodAsegurador);
            pObjBECotizacion.DirInspeccion = NullTypes.CadenaNull;
            pObjBECotizacion.FecHorInspeccion = NullTypes.CadenaNull;
            pObjBECotizacion.FecValidez = NullTypes.FechaNull;            

            //Si se registró la cotizació del cliente, 
            //se cambia al estado cotizado.
            if (this.Insertar(pObjBECotizacion) > 0)
            {
                //Si se registro la cotización, se procede a actializar el estado del cliente.
                BLCliente objBLCliente = new BLCliente();
                if (objBLCliente.Atualizar(pObjBECliente.NroMotor, Convert.ToInt32(ValorConstante.EstadoCliente.COTIZADO), NombreSession.Automatico) > 0)
                {
                    nResult = 1;
                }
            }

            return nResult;
        }

        public BECotizacion Retornar(BECotizacion pObjBECotizacion) 
        {
            if (pObjBECotizacion == null)
            {
                pObjBECotizacion = new BECotizacion();

            }

            return pObjBECotizacion;
        }
        #endregion

        #region NoTransaccional
        public Int32 ObtenerVigencias(Int32 pnIdProducto, String pcNroMotor) 
        {
            Int32 nCanVigencias = NullTypes.IntegerNull;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DACotizacion objDACotizacion = new DACotizacion();
                SqlDataReader sqlDr = objDACotizacion.ObtenerVigencias(pnIdProducto, pcNroMotor, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex = sqlDr.GetOrdinal("canVigencia");

                        if (sqlDr.Read()) 
                        {
                            if (sqlDr.IsDBNull(nIndex)) { nCanVigencias = NullTypes.IntegerNull; } else { nCanVigencias = sqlDr.GetInt32(nIndex); }
                        }
                    }
                }
            }

            return nCanVigencias;
        }

        public Decimal ObtenerValorDepresiado(Int32 pnAnioFab, Decimal pnValorVehiculo, Int32 pnIdProducto) 
        {
            Decimal nResult = NullTypes.DecimalNull;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DACotizacion  objDACotizacion = new DACotizacion();
                SqlDataReader sqlDr = objDACotizacion.ObtenerValorDepresiado(pnAnioFab, pnValorVehiculo, pnIdProducto, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex = sqlDr.GetOrdinal("valordepresiado");

                        if (sqlDr.Read())
                        {
                            if (sqlDr.IsDBNull(nIndex)) { nResult = NullTypes.DecimalNull; } else { nResult = sqlDr.GetDecimal(nIndex); }
                        }

                        sqlDr.Close(); 
                    }                    
                }
            }
            return nResult;
        }

        public List<BECotizacion> RetornarLista(Int32 pnAnioFab, Decimal pnValorVeh, Int32 pnIdMarca, String pcDesMarca, Int32 pnIdModelo, String pcDesModelo)
        {
            List<BECotizacion> lstBECotizacion = new List<BECotizacion>();
            BECotizacion objBECotizacion = new BECotizacion();
            BLTasa objBLTasa = new BLTasa();

            BETasa objBETasa_A = objBLTasa.Obtener(pnIdModelo, 1, pnAnioFab);
            BETasa objBETasa_B = objBLTasa.Obtener(pnIdModelo, 2, pnAnioFab);

            objBECotizacion.AnioFab = pnAnioFab;
            objBECotizacion.ValVehiculo = pnValorVeh;
            objBECotizacion.IdMarca = pnIdMarca;
            objBECotizacion.DesMarca = pcDesMarca;
            objBECotizacion.IdModelo = pnIdModelo;
            objBECotizacion.DesModelo = pcDesModelo;

            if (objBETasa_A != null)
            {
                objBECotizacion.TasaAnual = objBETasa_A.ValorTasa;
                objBECotizacion.PrimaAnual = objBECotizacion.ValVehiculo * (objBECotizacion.TasaAnual / 100);
            }

            if (objBETasa_B != null)
            {
                objBECotizacion.TasaBianual = objBETasa_B.ValorTasa;
                objBECotizacion.PrimaBianual = objBECotizacion.ValVehiculo * (objBECotizacion.TasaBianual / 100);
            }

            objBECotizacion.IdProducto = 5250;
            lstBECotizacion.Add(objBECotizacion);

            //
            objBECotizacion = new BECotizacion();
            objBECotizacion.AnioFab = pnAnioFab;
            objBECotizacion.ValVehiculo = pnValorVeh;
            objBECotizacion.IdMarca = pnIdMarca;
            objBECotizacion.DesMarca = pcDesMarca;
            objBECotizacion.IdModelo = pnIdModelo;
            objBECotizacion.DesModelo = pcDesModelo;

            if (objBETasa_A != null)
            {
                objBECotizacion.TasaAnual = objBETasa_A.ValorTasa - (objBETasa_A.ValorTasa * (10 / 100));
                objBECotizacion.PrimaAnual = objBECotizacion.ValVehiculo * (objBECotizacion.TasaBianual / 100);
            }

            if (objBETasa_B != null)
            {
                objBECotizacion.TasaBianual = objBETasa_B.ValorTasa - (objBETasa_B.ValorTasa * (10 / 100));
                objBECotizacion.PrimaBianual = objBECotizacion.ValVehiculo * (objBECotizacion.TasaBianual / 100);
            }

            objBECotizacion.IdProducto = 5251;
            lstBECotizacion.Add(objBECotizacion);

            return lstBECotizacion;
        }
        #endregion

        #region Métodos Privados
        /// <summary>
        /// Permite generar cotizaciones, obteniendo el valor del vehículo
        /// de la tabla APESEG.
        /// </summary>
        /// <param name="pLstBECliente">Lista de clientes por cotizar.</param>
        private void GenCotConAPESEG(List<BECliente> pLstBECliente, String pcUsuario, ref Int32 pnNroCot)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.:2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.:____-__-__
            ---------------------------------------------------------------------------*/

            BLCliente objBLCliente = new BLCliente();

            foreach (BECliente objBECliente in pLstBECliente)
            {
                BLApeseg objBLApeseg = new BLApeseg();
                BEApeseg objBEApeseg = new BEApeseg();
                objBEApeseg = objBLApeseg.ObtenerDisponible(objBECliente.IdModelo, objBECliente.AnioFab);

                BLParametro objBLParametro = new BLParametro();
                Int32 nCodParMaxSin = 10;

                BEParametro objBEParametro_MaxSin = objBLParametro.Obtener(nCodParMaxSin);

                //Si el vehículo existe en la tabla APESEG, se buscará la
                //tasa que le corresponde, de lo contrario se actualizará el
                //registro del cliente informando que no exiete valor en APESEG.
                if (objBEApeseg == null)
                {
                    pnNroCot = 0;
                    Int32 nResult = objBLCliente.Atualizar(objBECliente.NroMotor, Convert.ToInt32(ValorConstante.EstadoCliente.NoExisteValorAPESEG), NombreSession.Automatico);
                }
                else
                {
                    //Obtener Porcentaje de depreciación y valor del vehículo.                    
                    Decimal nValorVehiculo = objBEApeseg.ValorVehiculo;  //objBECliente.ValorVehiculo - (objBECliente.ValorVehiculo * (nPorcDep / 100));
                    this.CotizarCliente(objBECliente, nValorVehiculo, objBEParametro_MaxSin.ValorNumeroEnt, pcUsuario, ref pnNroCot);
                }
            }
        }

        /// <summary>
        /// Permite generar cotizaciones, sin obtener el valor del vehículo
        /// de la tabla APESEG.
        /// </summary>
        /// <param name="pLstBECliente">Lista de clientes por cotizar.</param>
        private void GenerarCotizacionBBVA(List<BECliente> pLstBECliente, String pcUsuario, ref Int32 pnNroCot)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            BLCliente objBLCliente = new BLCliente();
            BLParametro objBLParametro = new BLParametro();

            Int32 nCodParMen1 = 8;
            BEParametro objBEParametro_Men1 = objBLParametro.Obtener(nCodParMen1);

            Int32 nCodParMay1 = 9;
            BEParametro objBEParametro_May1 = objBLParametro.Obtener(nCodParMay1);

            Int32 nCodParMaxSin = 10;
            BEParametro objBEParametro_MaxSin = objBLParametro.Obtener(nCodParMaxSin);

            this.ValidarTasaDep(objBEParametro_Men1, objBEParametro_May1);

            foreach (BECliente objBECliente in pLstBECliente)
            {
                Int32 nAntiguedad = this.ObtenerAntiguedad(objBECliente.AnioFab);

                //Si no existe tasa de depreciación activa para el cliente, 
                //se actualiza estado del cliente.
                if (!ValidarTasDepActiva(nAntiguedad, objBEParametro_Men1, objBEParametro_May1))
                {
                    Int32 nResult = objBLCliente.Atualizar(objBECliente.NroMotor, Convert.ToInt32(ValorConstante.EstadoCliente.NO_EXISTE_TASADEPRECIACION), pcUsuario);
                }
                else
                {
                    //Obtener Porcentaje de depreciación y valor del vehículo. 0_0
                    //Decimal nPorcDep = this.ObtenerPorcDep(nAntiguedad, objBEParametro_Men1, objBEParametro_May1); X3
                    //Decimal nValorVehiculo = objBECliente.ValorVehiculo - (objBECliente.ValorVehiculo * (nPorcDep / 100)); X3
                    Decimal nValorVehiculo = this.ObtenerValorDepresiado(objBECliente.AnioFab, objBECliente.ValorVehiculo, 5251);
                    this.CotizarCliente(objBECliente, nValorVehiculo, objBEParametro_MaxSin.ValorNumeroEnt, pcUsuario, ref pnNroCot);
                }
            }
        }

        /// <summary>
        /// Permite cotizar un cliente.
        /// </summary>
        /// <param name="pObjBECliente">Objeto qie contiene la información del cliente.</param>
        /// <param name="pnValorVehiculo">Valor actual del vahiculo.</param>
        /// <param name="nMaxCantSiniestro">Maxima cantidad de siniestros permitidos.</param>        
        /// <param name="pcUsuario">Código de usuario que ejecuta proceso.</param>
        /// <param name="pnNroCot">Nro de cototizaciones generadas.</param>
        private void CotizarCliente(BECliente pObjBECliente, Decimal pnValorVehiculo, Int32 nMaxCantSiniestro, String pcUsuario, ref Int32 pnNroCot)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            String cLog = String.Empty;
            //Variables para almacenar las tasas por condición.
            Decimal nTasaAnualCond = NullTypes.DecimalNull;
            Decimal nTasaBianualCond = NullTypes.DecimalNull;
            //Variables para almacenar las primas por condición.
            Decimal nPrimAnualCond = NullTypes.DecimalNull;
            Decimal nPrimBianualCond = NullTypes.DecimalNull;
            //Variables para almacenar las primas mensuales por condición.
            Decimal nPrimAnualMenCond = NullTypes.DecimalNull;
            Decimal nPrimBianualMenCond = NullTypes.DecimalNull;
            //Variable para almacenar el porcentaje de recargo o descuento
            Decimal nPorcentaje = NullTypes.DecimalNull;
            //Variable para almacenar la condicion de la cotizacion. 
            Int32 nCondicion = Convert.ToInt32(ValorConstante.CondicionCotizacion.NO_APLICA);
            Int32 nIdCategoria = 0;

            //Obtener tasa para planes anuales y bianuales.
            BLTasa objBLTasa = new BLTasa();
            BETasa objBETasa_Anual = objBLTasa.Obtener(pObjBECliente.IdModelo, Convert.ToInt32(ValorConstante.TipoTasa.Anual), pObjBECliente.AnioFab);
            BETasa objBETasa_Bianual = objBLTasa.Obtener(pObjBECliente.IdModelo, Convert.ToInt32(ValorConstante.TipoTasa.Bianual), pObjBECliente.AnioFab);

            BLCliente objBLCliente = new BLCliente();

            if (objBETasa_Anual == null && objBETasa_Bianual == null)
            {
                //Actualizar estado cliente que no existe valor de tasa.
                Int32 nresult = objBLCliente.Atualizar(pObjBECliente.NroMotor, Convert.ToInt32(ValorConstante.EstadoCliente.NoExisteValorTasa), pcUsuario);
                return;
            }
            if (objBETasa_Anual.ValorTasa == 0 && objBETasa_Bianual.ValorTasa == 0) 
            {
                //Actualizar estado cliente que no existe valor de tasa.
                Int32 nresult = objBLCliente.Atualizar(pObjBECliente.NroMotor, Convert.ToInt32(ValorConstante.EstadoCliente.NoExisteValorTasa), pcUsuario);
                return;
            }

            nIdCategoria = objBETasa_Anual.IdCategoria;

            //Si cliente es de RIMAC, se obtendra los siniestros, para realizar descuento o recargo.
            if (pObjBECliente.CodAsegurador == 5)
            {                
                BLVigenciaSiniestro objBLVigenciaSiniestro = new BLVigenciaSiniestro();
                BEVigenciaSiniestro objBEVigenciaSiniestro = objBLVigenciaSiniestro.Obtener(pObjBECliente.NroMotor);

                //Si cliente no tiene siniestros registrados, 
                //se actualiza el estado del cliente.
                if (objBEVigenciaSiniestro == null)
                {  
                    Int32 nresult = objBLCliente.Atualizar(pObjBECliente.NroMotor, Convert.ToInt32(ValorConstante.EstadoCliente.NO_EXISTE_SINIESTROREG), pcUsuario);
                    return;
                }
                else
                {                    
                    //Si la cantidad de siniestros es mayor a lo permitodo, no se puede renovar seguro.
                    if (objBEVigenciaSiniestro.CantSiniestro1Anio > nMaxCantSiniestro && objBEVigenciaSiniestro.MontoSiniestro1Anio >= 100)
                    {
                        Int32 nResult = objBLCliente.Atualizar(pObjBECliente.NroMotor, Convert.ToInt32(ValorConstante.EstadoCliente.NO_RENO_MAXCANTSINIESTRO), pcUsuario);
                    }
                    else
                    {
                        Boolean bSiniestro = false;

                        if (objBEVigenciaSiniestro.CantSiniestro1Anio == 1 || objBEVigenciaSiniestro.CantSiniestro1Anio == 2 ) 
                        {
                            if (objBEVigenciaSiniestro.MontoSiniestro1Anio >= 100) 
                            {
                                nCondicion = 0;
                                bSiniestro = true;
                            }                            
                        }
                        if (objBEVigenciaSiniestro.CantSiniestro1Anio == 3 && objBEVigenciaSiniestro.MontoSiniestro1Anio >= 100)
                        {
                            nCondicion = 2;
                            bSiniestro = true;
                            nPorcentaje = 15;
                        }
                        if (objBEVigenciaSiniestro.CantSiniestro1Anio == 4 && objBEVigenciaSiniestro.MontoSiniestro1Anio >= 100)
                        {
                            nCondicion = 2;
                            bSiniestro = true;
                            nPorcentaje = 25;
                        }

                        //ExisteSiniestro DESCUENTO
                        if (bSiniestro == false) 
                        {
                            Int32 nCanVigencia = this.ObtenerVigencias(5251, pObjBECliente.NroMotor);
                            //Si tiene 1 vigencia, existe siniestros o no proceder a validar montos.
                            if (nCanVigencia == 1 && objBEVigenciaSiniestro.CantSiniestro1Anio >= 0)
                            {
                                //Si monto es menor a $100
                                if (objBEVigenciaSiniestro.MontoSiniestro1Anio < 100)
                                {
                                    nCondicion = 1;
                                    nPorcentaje = 10;
                                }
                            }
                            //Si tiene 2 vigencias, existe siniestros o no proceder a validar montos.
                            if (nCanVigencia == 2 && objBEVigenciaSiniestro.CantSiniestro1Anio >= 0 && objBEVigenciaSiniestro.CantSiniestro2Anio >= 0)
                            {
                                //Si monto es menor a $100
                                if (objBEVigenciaSiniestro.MontoSiniestro1Anio < 100)
                                {
                                    nCondicion = 1;
                                    nPorcentaje = 10;
                                }

                                //Si los montos son menores a $100.
                                if (objBEVigenciaSiniestro.MontoSiniestro1Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro2Anio < 100)
                                {
                                    nCondicion = 1;
                                    nPorcentaje = 15;
                                }

                                /*
                                //Si los montos son menores a $100.
                                if (objBEVigenciaSiniestro.MontoSiniestro1Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro2Anio < 100) 
                                {
                                    nCondicion = 1;
                                    nPorcentaje = 15;
                                }
                                */
                            }
                            //Si tiene 3 vigencias, existe siniestros o no proceder a validar montos.
                            if (nCanVigencia == 3 && objBEVigenciaSiniestro.CantSiniestro1Anio >= 0 && objBEVigenciaSiniestro.CantSiniestro2Anio >= 0 && objBEVigenciaSiniestro.CantSiniestro3Anio >= 0)
                            {
                                //Si monto es menor a $100
                                if (objBEVigenciaSiniestro.MontoSiniestro1Anio < 100)
                                {
                                    nCondicion = 1;
                                    nPorcentaje = 10;
                                }

                                //Si los montos son menores a $100.
                                if (objBEVigenciaSiniestro.MontoSiniestro1Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro2Anio < 100)
                                {
                                    nCondicion = 1;
                                    nPorcentaje = 15;
                                }

                                //Si los montos son menores a $100.
                                if (objBEVigenciaSiniestro.MontoSiniestro1Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro2Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro3Anio < 100)
                                {
                                    nCondicion = 1;
                                    nPorcentaje = 20;
                                }

                                /*
                                //Si los montos son menores a $100.
                                if (objBEVigenciaSiniestro.MontoSiniestro1Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro2Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro3Anio < 100) 
                                {
                                    nCondicion = 1;
                                    nPorcentaje = 20;
                                }
                                */
                            }
                            if (nCanVigencia == 4 && objBEVigenciaSiniestro.CantSiniestro1Anio >= 0 && objBEVigenciaSiniestro.CantSiniestro2Anio >= 0 && objBEVigenciaSiniestro.CantSiniestro3Anio >= 0 && objBEVigenciaSiniestro.CantSiniestro4Anio >= 0)
                            {
                                //Si monto es menor a $100
                                if (objBEVigenciaSiniestro.MontoSiniestro1Anio < 100)
                                {
                                    nCondicion = 1;
                                    nPorcentaje = 10;
                                }

                                //Si los montos son menores a $100.
                                if (objBEVigenciaSiniestro.MontoSiniestro1Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro2Anio < 100)
                                {
                                    nCondicion = 1;
                                    nPorcentaje = 15;
                                }

                                //Si los montos son menores a $100.
                                if (objBEVigenciaSiniestro.MontoSiniestro1Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro2Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro3Anio < 100)
                                {
                                    nCondicion = 1;
                                    nPorcentaje = 20;
                                }

                                //Si los montos son menores a $100.
                                if (objBEVigenciaSiniestro.MontoSiniestro1Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro2Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro3Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro4Anio < 100)
                                {
                                    nCondicion = 1;
                                    nPorcentaje = 25;
                                }

                                /*
                                //Si los montos son menores a $100.                                
                                if (objBEVigenciaSiniestro.MontoSiniestro1Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro2Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro3Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro4Anio < 100)
                                {
                                    nCondicion = 1;
                                    nPorcentaje = 25;
                                }*/
                            }
                            if (nCanVigencia == 5 && objBEVigenciaSiniestro.CantSiniestro1Anio >= 0 && objBEVigenciaSiniestro.CantSiniestro2Anio >= 0 && objBEVigenciaSiniestro.CantSiniestro3Anio >= 0 && objBEVigenciaSiniestro.CantSiniestro4Anio >= 0 && objBEVigenciaSiniestro.CantSiniestro5Anio >= 0)
                            {
                                //Si monto es menor a $100
                                if (objBEVigenciaSiniestro.MontoSiniestro1Anio < 100)
                                {
                                    nCondicion = 1;
                                    nPorcentaje = 10;
                                }

                                //Si los montos son menores a $100.
                                if (objBEVigenciaSiniestro.MontoSiniestro1Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro2Anio < 100)
                                {
                                    nCondicion = 1;
                                    nPorcentaje = 15;
                                }

                                //Si los montos son menores a $100.
                                if (objBEVigenciaSiniestro.MontoSiniestro1Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro2Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro3Anio < 100)
                                {
                                    nCondicion = 1;
                                    nPorcentaje = 20;
                                }

                                //Si los montos son menores a $100.
                                if (objBEVigenciaSiniestro.MontoSiniestro1Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro2Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro3Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro4Anio < 100)
                                {
                                    nCondicion = 1;
                                    nPorcentaje = 25;
                                }

                                //Si los montos son menores a $100.
                                if (objBEVigenciaSiniestro.MontoSiniestro1Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro2Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro3Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro4Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro5Anio < 100)
                                {
                                    nCondicion = 1;
                                    nPorcentaje = 30;
                                }
                            }
                            //Int32 nCanVigencia = this.ObtenerVigencias(5251, pObjBECliente.NroMotor);

                            ////Si tiene 1 vigencia, existe siniestros o no proceder a validar montos.
                            //if (nCanVigencia == 1 && objBEVigenciaSiniestro.CantSiniestro1Anio >= 0)
                            //{
                            //    //Si monto es menor a $100
                            //    if (objBEVigenciaSiniestro.MontoSiniestro1Anio < 100) 
                            //    {
                            //        nCondicion = 1;
                            //        nPorcentaje = 10;
                            //    }                                
                            //}
                            ////Si tiene 2 vigencias, existe siniestros o no proceder a validar montos.
                            //if (nCanVigencia == 2 && objBEVigenciaSiniestro.CantSiniestro1Anio >= 0 && objBEVigenciaSiniestro.CantSiniestro2Anio >= 0)
                            //{
                            //    //Si los montos son menores a $100.
                            //    if (objBEVigenciaSiniestro.MontoSiniestro1Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro2Anio < 100) 
                            //    {
                            //        nCondicion = 1;
                            //        nPorcentaje = 15;
                            //    }                                
                            //}
                            ////Si tiene 3 vigencias, existe siniestros o no proceder a validar montos.
                            //if (nCanVigencia == 3 && objBEVigenciaSiniestro.CantSiniestro1Anio >= 0 && objBEVigenciaSiniestro.CantSiniestro2Anio >= 0 && objBEVigenciaSiniestro.CantSiniestro3Anio >= 0)
                            //{
                            //    //Si los montos son menores a $100.
                            //    if (objBEVigenciaSiniestro.MontoSiniestro1Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro2Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro3Anio < 100) 
                            //    {
                            //        nCondicion = 1;
                            //        nPorcentaje = 20;
                            //    }                                
                            //}
                            //if (nCanVigencia == 4 && objBEVigenciaSiniestro.CantSiniestro1Anio >= 0 && objBEVigenciaSiniestro.CantSiniestro2Anio >= 0 && objBEVigenciaSiniestro.CantSiniestro3Anio >= 0 && objBEVigenciaSiniestro.CantSiniestro4Anio >= 0)
                            //{
                            //    //Si los montos son menores a $100.
                            //    if (objBEVigenciaSiniestro.MontoSiniestro1Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro2Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro3Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro4Anio < 100)
                            //    {
                            //        nCondicion = 1;
                            //        nPorcentaje = 25;
                            //    }
                            //}
                            //if (nCanVigencia == 5 && objBEVigenciaSiniestro.CantSiniestro1Anio >= 0 && objBEVigenciaSiniestro.CantSiniestro2Anio >= 0 && objBEVigenciaSiniestro.CantSiniestro3Anio >= 0 && objBEVigenciaSiniestro.CantSiniestro4Anio >= 0 && objBEVigenciaSiniestro.CantSiniestro5Anio >= 0)
                            //{
                            //    //Si los montos son menores a $100.
                            //    if (objBEVigenciaSiniestro.MontoSiniestro1Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro2Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro3Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro4Anio < 100 && objBEVigenciaSiniestro.MontoSiniestro5Anio < 100)
                            //    {
                            //        nCondicion = 1;
                            //        nPorcentaje = 30;
                            //    }
                            //}
                        }                        
                                                
                        BECotizacion objBECotizacion = new BECotizacion();
                        objBECotizacion.PrimaAnual = this.CalcularPrima(objBETasa_Anual, pnValorVehiculo);
                        objBECotizacion.TasaAnual = this.RetornarTasa(objBETasa_Anual);
                        objBECotizacion.PrimaBianual = this.CalcularPrima(objBETasa_Bianual, pnValorVehiculo);
                        objBECotizacion.TasaBianual = this.RetornarTasa(objBETasa_Bianual);
                        objBECotizacion.Condicion = nCondicion;
                        objBECotizacion.PorcentajeC = nPorcentaje;
                        objBECotizacion.PrimaAnualC = nPrimAnualCond;
                        objBECotizacion.TasaAnualC = nTasaAnualCond;
                        objBECotizacion.PrimaBianualC = nPrimBianualCond;
                        objBECotizacion.TasaBianualC = nTasaBianualCond;
                        objBECotizacion.IdCategoria = this.ObtenerCategoria(objBETasa_Anual, objBETasa_Bianual);
                        objBECotizacion.UsuarioCreacion = pcUsuario;
                        objBECotizacion.ValVehiculo = pnValorVehiculo;
                        objBECotizacion.FecIniVigPoliza = objBEVigenciaSiniestro.FecIniVigencia;
                        objBECotizacion.FecFinVigPoliza = objBEVigenciaSiniestro.FecFinVigencia;

                        if (pObjBECliente.IdSponsor == Sponsor.BBVA)
                        {
                            objBECotizacion.IdProducto = 5251;
                        }
                        else
                        {
                            objBECotizacion.IdProducto = 5250;
                        }

                        if (this.CargarCotizacion(objBECotizacion, pObjBECliente) > 0)
                        {
                            pnNroCot = pnNroCot + 1;
                        }
                    }
                }
            }
            else
            {
                BECotizacion objBECotizacion = new BECotizacion();
                objBECotizacion.PrimaAnual = this.CalcularPrima(objBETasa_Anual, pnValorVehiculo);
                objBECotizacion.TasaAnual = this.RetornarTasa(objBETasa_Anual);
                objBECotizacion.PrimaBianual = this.CalcularPrima(objBETasa_Bianual, pnValorVehiculo);
                objBECotizacion.TasaBianual = this.RetornarTasa(objBETasa_Bianual);
                objBECotizacion.Condicion = nCondicion;
                objBECotizacion.PorcentajeC = NullTypes.DecimalNull;
                objBECotizacion.PrimaAnualC = NullTypes.DecimalNull;
                objBECotizacion.TasaAnualC = NullTypes.DecimalNull;
                objBECotizacion.PrimaBianualC = NullTypes.DecimalNull;
                objBECotizacion.TasaBianualC = NullTypes.DecimalNull;
                objBECotizacion.UsuarioCreacion = pcUsuario;
                objBECotizacion.IdCategoria = this.ObtenerCategoria(objBETasa_Anual, objBETasa_Bianual);

                if (this.CargarCotizacion(objBECotizacion, pObjBECliente) > 0)
                {
                    pnNroCot = +1;
                }
            }
        }

        /// <summary>
        /// Permite validar los porcentaje de depreciación.
        /// </summary>
        /// <param name="pObjBEParametro_Men1">Parámetro de porcentaje de depreciación menor iguial a 1 año de antigüedad.</param>
        /// <param name="pObjBEParametro_May1">Parámetro de porcentaje de depreciación mayor a 1 año de antigúedad.</param>
        private void ValidarTasaDep(BEParametro pObjBEParametro_Men1, BEParametro pObjBEParametro_May1)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            if (pObjBEParametro_Men1 == null || pObjBEParametro_May1 == null)
            {
                throw new Exception("No está configurado el parametro de porcentaje de depreciación");
            }

            if (pObjBEParametro_Men1.EstadoRegistro == true && pObjBEParametro_Men1.ValorNumeroDec == NullTypes.DecimalNull)
            {
                throw new Exception("El porcentaje de depreciación para vehículos hasta 1 año de antiguedad es incorrecto.");
            }

            if (pObjBEParametro_May1.EstadoRegistro == true && pObjBEParametro_May1.ValorNumeroDec == NullTypes.DecimalNull)
            {
                throw new Exception("El porcentaje de depreciación para vehículos mayores a 1 año es incorrecto.");
            }
        }

        public void EscribirErrores(List<String> pLstLog, String pcRuta, String pcNombreArchivo)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            StreamWriter swLog = new StreamWriter(pcRuta + pcNombreArchivo);

            if ((pLstLog != null) && (pLstLog.Count != 0))
            {
                foreach (String cError in pLstLog)
                {
                    swLog.WriteLine(cError);
                }
            }
            else
                swLog.WriteLine("No se presentaron inconsistencias");

            swLog.Close();
        }
        #endregion
    }
}
