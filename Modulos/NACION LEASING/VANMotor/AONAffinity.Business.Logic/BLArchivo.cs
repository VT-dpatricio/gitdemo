﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Data.Common;
using AONAffinity.Motor.Resources;
using AONAffinity.Library.Resources;
using AONAffinity.Library.DataAccess;
using AONAffinity.Library.DataAccess.BDMotor;
using AONAffinity.Library.BusinessEntity.BDMotor;  

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLArchivo
    {
        public Boolean ValidarLectura(String pcRuta, String pcArchivo)
        {
            Boolean bResult = true;
            OleDbConnection oleCn = null;
            try
            {
                String cConexion = String.Format(AppSettings.ProviderExcel, pcRuta + pcArchivo);
                oleCn = new OleDbConnection(cConexion);
                oleCn.Open();

                OleDbCommand oleCmd = new OleDbCommand("SELECT * FROM [" + "Hoja1$" + "]", oleCn);
                oleCmd.CommandType = CommandType.Text;                
                Int32 nResult = oleCmd.ExecuteNonQuery();                 
            }
            catch(Exception ex)
            {                
                bResult = false;
                throw ex;
            }
            finally
            {
                if (oleCn != null) 
                {
                    if (oleCn.State == ConnectionState.Open)
                    {
                        oleCn.Close();
                    }
                }                
            }

            return bResult;
        }         
    
        public OleDbDataReader LeerExcel(OleDbConnection pOleCn) 
        {
            OleDbDataReader oleDr = null;
            try
            {
                
                OleDbCommand oleCmd = new OleDbCommand("SELECT * FROM [" + "Hoja1$" + "]", pOleCn);
                oleCmd.CommandType = CommandType.Text;
                oleDr = oleCmd.ExecuteReader();                
            }
            catch (Exception ex) 
            {
                throw new Exception("Error al leer el archivo, asegúrese que el nombre de la hoja sea: Hoja1. " + ex.Message);
            }
            return oleDr;
        }
    }
}
