﻿using System;
using System.Collections.Generic;
using AONAffinity.Motor.DataAccess.DLNT;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLUsuario
    {
        public BEUsuarioBW Seleccionar(string pIDUsuario, Int32 pIDSistema)
        {
            DLNTUsuario oDL = new DLNTUsuario();
            BEUsuarioBW oBE = new BEUsuarioBW();
            return oDL.SeleccionarE(pIDUsuario, pIDSistema);
        }
    }
}
