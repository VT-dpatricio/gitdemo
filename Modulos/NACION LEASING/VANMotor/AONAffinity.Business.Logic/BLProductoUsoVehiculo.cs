﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;
using AONAffinity.Library.Resources;

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLProductoUsoVehiculo
    {
        #region NoTransaccional
        public List<BEProductoUsoVehiculo> ListarxProducto(Int32 pnIdproducto, Boolean? pbStsRegistro)
        {
            List<BEProductoUsoVehiculo> lstBEProductoUsoVehiculo = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAProductoUsoVehiculo objDAProductoUsoVehiculo = new DAProductoUsoVehiculo();
                SqlDataReader sqlDr = objDAProductoUsoVehiculo.ListarxProducto(pnIdproducto, pbStsRegistro, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idUsoVehiculo");
                        Int32 nIndex3 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex4 = sqlDr.GetOrdinal("codExterno");

                        lstBEProductoUsoVehiculo = new List<BEProductoUsoVehiculo>();

                        while (sqlDr.Read())
                        {
                            BEProductoUsoVehiculo objBEProductoUsoVehiculo = new BEProductoUsoVehiculo();
                            objBEProductoUsoVehiculo.IdProducto = sqlDr.GetInt32(nIndex1);
                            objBEProductoUsoVehiculo.IdUsoVehiculo = sqlDr.GetInt32(nIndex2);
                            objBEProductoUsoVehiculo.Descripcion = sqlDr.GetString(nIndex3);
                            if (sqlDr.IsDBNull(nIndex4)) { objBEProductoUsoVehiculo.CodExterno = NullTypes.CadenaNull; } else { objBEProductoUsoVehiculo.CodExterno = sqlDr.GetString(nIndex4); }

                            lstBEProductoUsoVehiculo.Add(objBEProductoUsoVehiculo);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBEProductoUsoVehiculo;
        }
        #endregion
    }
}
