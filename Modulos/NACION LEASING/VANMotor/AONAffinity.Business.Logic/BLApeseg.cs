﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;  
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;
 
namespace AONAffinity.Motor.BusinessLogic
{
    public class BLApeseg
    {
        public BEApeseg ObtenerDisponible(Int32 pnIdModelo, Int32 pnIdAnio)        
        {
            BEApeseg objBEApeseg = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAApeseg objDAApeseg = new DAApeseg();
                SqlDataReader sqlDr = objDAApeseg.ObtenerDisponible(pnIdModelo, pnIdAnio, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idModelo");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idAnio");
                        Int32 nIndex3 = sqlDr.GetOrdinal("idPeriodo");
                        Int32 nIndex4 = sqlDr.GetOrdinal("valorVehiculo");

                        objBEApeseg = new BEApeseg();

                        if (sqlDr.Read()) 
                        {
                            objBEApeseg.IdModelo = sqlDr.GetInt32(nIndex1);
                            objBEApeseg.IdAnio = sqlDr.GetInt32(nIndex2);
                            objBEApeseg.IdPeriodo = sqlDr.GetInt32(nIndex3);
                            objBEApeseg.ValorVehiculo = sqlDr.GetDecimal(nIndex4);   
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return objBEApeseg;
        }
    }
}
