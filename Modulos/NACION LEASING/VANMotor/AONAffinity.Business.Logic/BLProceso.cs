﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Data.Common;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;
using AONAffinity.Motor.BusinessLogic.Import;

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLProceso
    {
        #region Transaccional
        //ok
        public Int32 Insertar(BEProceso pObjBEProceso)
        {
            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAProceso objDAProceso = new DAProceso();
                nResult = objDAProceso.Insertar(pObjBEProceso, sqlCn);
            }

            return nResult;
        }

        //ok
        public Int32 Actualizar(BEProceso pObjBEProceso)
        {
            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAProceso objDAProceso = new DAProceso();
                nResult = objDAProceso.Actualizar(pObjBEProceso, sqlCn);
            }

            return nResult;
        }

        //ok
        public Int32 ActualizarLog(BEProceso pObjBEProceso)
        {
            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAProceso objDAProceso = new DAProceso();
                nResult = objDAProceso.ActualizarLog(pObjBEProceso, sqlCn);
            }

            return nResult;
        }

        //ok
        public Int32 ActualizarEstado(BEProceso pObjBEProceso)
        {
            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAProceso objDAProceso = new DAProceso();
                nResult = objDAProceso.ActualizarEstado(pObjBEProceso, sqlCn);
            }

            return nResult;
        }

        public Int32 ActualizarCotizacion(BEProceso pObjBEProceso)
        {
            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAProceso objDAProceso = new DAProceso();
                nResult = objDAProceso.ActualizarCotizacion(pObjBEProceso, sqlCn);
            }

            return nResult;
        }

        //ok        

        #endregion

        #region Métodos
        public void Ejecutar(BETipoProceso pObjBETipoProceso, BETipoArchivo pObjBETipoArchivo, String pcUsuario)
        {
            List<BEProceso> lstBEProceso = this.ListarDisponible(pObjBETipoProceso.IdTipoProceso);

            if (lstBEProceso != null)
            {
                foreach (BEProceso objBEProceso in lstBEProceso)
                {
                    //Import cliente BBVA
                    if (objBEProceso.IdTipProceso == TipoProceso.ImportClienteBBVA)
                    {
                        this.ImportClienteBBVA(pObjBETipoProceso, objBEProceso, pObjBETipoArchivo, pcUsuario);
                    }

                    //Import Siniestro BBVA
                    if (objBEProceso.IdTipProceso == TipoProceso.ImportSiniestroBBVA)
                    {
                        this.ImportSiniestroBBVA(pObjBETipoProceso, objBEProceso, pObjBETipoArchivo, pcUsuario);
                    }

                    //Import Cliente IBK
                    if (objBEProceso.IdTipProceso == TipoProceso.ImportClienteIBK) 
                    {
                        this.ImportClienteIBK(pObjBETipoProceso, objBEProceso, pObjBETipoArchivo, pcUsuario);
                    }

                    //Import Cliente IBK - 5292 - Seg. Auto Facil.
                    if (objBEProceso.IdTipProceso == TipoProceso.ImportClienteIBK_5292) 
                    {
                        this.ImportClienteIBK_5292(pObjBETipoProceso, objBEProceso, pObjBETipoArchivo, pcUsuario);   
                    }
                }
            }
        }
        #endregion

        #region Import BBVA
        public void ImportClienteBBVA(BETipoProceso pObjBETipoProceso, BEProceso pObjBEProceso, BETipoArchivo pObjBETipoArchivo, String pcUsuario) 
        {
            BLImport5251 objBLImport5251 = new BLImport5251();

            if (pObjBETipoArchivo.IdTipoArchivo == TipoArchivo.Texto)
            {
                objBLImport5251.CargarClientesTxtBBVA(pObjBETipoProceso, pObjBEProceso, pObjBETipoArchivo, pcUsuario);
            }
            else
            {
                objBLImport5251.CargarClientesXlsBBVA(pObjBETipoProceso, pObjBEProceso, pObjBETipoArchivo, pcUsuario);
            }
        }

        public void ImportSiniestroBBVA(BETipoProceso pObjBETipoProceso, BEProceso pObjBEProceso, BETipoArchivo pObjBETipoArchivo, String pcUsuario) 
        {
            if (pObjBETipoArchivo.IdTipoArchivo == TipoArchivo.Excel) 
            {
                BLImport5251 objBLImport5251 = new BLImport5251();
                objBLImport5251.CargarSiniestrosBBVA(pObjBETipoProceso, pObjBEProceso, pObjBETipoArchivo, pcUsuario);
            }            
        }
        #endregion

        #region ImportIBK
        private void ImportClienteIBK(BETipoProceso pObjBETipoProceso, BEProceso pObjBEProceso, BETipoArchivo pObjBETipoArchivo, String pcUsuario) 
        {
            BLImport5250 objBLImport5250 = new BLImport5250();

            if (pObjBETipoArchivo.IdTipoArchivo == TipoArchivo.Excel) 
            {
                objBLImport5250.CargarClientesXlsIBK(pObjBETipoProceso, pObjBEProceso, pObjBETipoArchivo, pcUsuario); 
            }
        }
        #endregion

        #region Import IBK Auto Facil
        private void ImportClienteIBK_5292(BETipoProceso pObjBETipoProceso, BEProceso pObjBEProceso, BETipoArchivo pObjBETipoArchivo, String pcUsuario)
        {
            BLImport5292 objBLImport5292 = new BLImport5292();

            if (pObjBETipoArchivo.IdTipoArchivo == TipoArchivo.Excel)
            {
                objBLImport5292.CargarClientesXlsIBK(pObjBETipoProceso, pObjBEProceso, pObjBETipoArchivo, pcUsuario);
            }
        }
        #endregion








        #region Proceso BBVA - AON
        //public void ProcesoImportBBVACliente(BETipoProceso pObjBETipoProceso, BEProceso pObjBEProceso, String pcUsuario)
        //{
        //    Int32 nObservaciones = 0;
        //    String cLog = String.Empty;
        //    List<String> lstCLog = new List<String>();
        //    Boolean bEstructura = true;
        //    String cArchivoLog = "Log_" + pObjBEProceso.Archivo;

        //    if (pObjBETipoProceso.LongitudTrama == NullTypes.IntegerNull)
        //    {
        //        bEstructura = false;
        //        cLog = "No está configurada la longitud de la trama.";
        //        lstCLog.Add(cLog);
        //        nObservaciones++;
        //    }

        //    BLEstructuraArchivo objBLEstructuraArchivo = new BLEstructuraArchivo();
        //    List<BEEstructuraArchivo> lstBEEstructuraArchivo = objBLEstructuraArchivo.Listar(pObjBETipoProceso.IdTipoProceso, true);

        //    if (lstBEEstructuraArchivo == null)
        //    {
        //        bEstructura = false;
        //        cLog = "No existe ninguna estructura configurada para el archivo.";
        //        lstCLog.Add(cLog);
        //        nObservaciones++;
        //    }

        //    FileStream fsArchivo = new FileStream(pObjBETipoProceso.RutaArchivo + pObjBEProceso.Archivo, FileMode.Open);
        //    Int32 nFila = 0;

        //    //Si estructura no es válida.
        //    if (!bEstructura) 
        //    {                
        //        this.EscribirErrores(lstCLog, pObjBETipoProceso.RutaArchivoLog, cArchivoLog);

        //        pObjBEProceso.Procesado = true;
        //        pObjBEProceso.UsuarioModificacion = pcUsuario;
        //        pObjBEProceso.ArchivoLog = cArchivoLog;
        //        pObjBEProceso.RegTotal = 0;
        //        pObjBEProceso.RegTotalVal = 0;
        //        pObjBEProceso.RegTotalErr = 0;
        //        pObjBEProceso.NroObservaciones = nObservaciones;
        //        this.ActualizarLog(pObjBEProceso);

        //        return;
        //    }

        //    BLCliente objBLCliente = new BLCliente();
        //    Boolean bValido = true;
        //    Int32 nValidos = 0;

        //    using (StreamReader stream = new StreamReader(fsArchivo, Funciones.GetFileEncoding(pObjBETipoProceso.RutaArchivo + pObjBEProceso.Archivo, fsArchivo)))
        //    {
        //        String cTrama;
        //        while (((cTrama = stream.ReadLine()) != null))
        //        {
        //            bValido = true;

        //            //Validar sio longitud de trama es válida.
        //            if (cTrama.Length != pObjBETipoProceso.LongitudTrama)
        //            {
        //                bValido = false;
        //                cLog = "Fila nro. " + nFila + " - el tamaño de la trama es diferente a la configurada.";
        //                lstCLog.Add(cLog);
        //            }

        //            //Si filtro es válido
        //            if (bValido == true) 
        //            {
        //                List<String> lstCCampos = this.TramaSplit(cTrama, lstBEEstructuraArchivo);

        //                if (!this.ValidarCampoObligatorio(lstBEEstructuraArchivo, nFila, lstCCampos, ref lstCLog))
        //                {
        //                    bValido = false;
        //                }

        //                if (!ValidarCampoTipoDato(lstBEEstructuraArchivo, nFila, lstCCampos, ref  lstCLog))
        //                {
        //                    bValido = false;
        //                }

        //                if (bValido == true)
        //                {
        //                    String cNroMotor = String.Empty;
        //                    String cCodExtModelo = String.Empty;
        //                    String cCodExtMarca = String.Empty;
        //                    String cCodDep = String.Empty;
        //                    String cCodProv = String.Empty;
        //                    String cCodDist = String.Empty;
        //                    String cCodTipoReg = String.Empty;
        //                    DateTime dFecInicio = NullTypes.FechaNull;
        //                    DateTime dFecFin = NullTypes.FechaNull;
        //                    Int32 nAnioFab = NullTypes.IntegerNull;
        //                    String cTelefono1 = String.Empty;

        //                    foreach (BEEstructuraArchivo objBEEstructuraArchivo in lstBEEstructuraArchivo)
        //                    {
        //                        if (objBEEstructuraArchivo.Nombre.ToUpper() == ("NumeroMotor").ToUpper()) 
        //                        {
        //                            cNroMotor = lstCCampos[objBEEstructuraArchivo.Orden].ToString(); 
        //                        }
        //                        if (objBEEstructuraArchivo.Nombre.ToUpper() == ("TipoRegistro").ToUpper())
        //                        {
        //                            cCodTipoReg = lstCCampos[objBEEstructuraArchivo.Orden].ToString().Trim();
        //                        }
        //                        if (objBEEstructuraArchivo.Nombre.ToUpper() == ("Marca").ToUpper())
        //                        {
        //                            cCodExtMarca = lstCCampos[objBEEstructuraArchivo.Orden].ToString().Trim();
        //                        }
        //                        if (objBEEstructuraArchivo.Nombre.ToUpper() == ("Modelo").ToUpper())
        //                        {
        //                            cCodExtModelo = lstCCampos[objBEEstructuraArchivo.Orden].ToString().Trim();
        //                        }
        //                        if (objBEEstructuraArchivo.Nombre.ToUpper() == ("CodDepartamento").ToUpper())
        //                        {
        //                            cCodDep = lstCCampos[objBEEstructuraArchivo.Orden].ToString().Trim();
        //                        }
        //                        if (objBEEstructuraArchivo.Nombre.ToUpper() == ("CodProvincia").ToUpper())
        //                        {
        //                            cCodProv = lstCCampos[objBEEstructuraArchivo.Orden].ToString().Trim();
        //                        }
        //                        if (objBEEstructuraArchivo.Nombre.ToUpper() == ("CodDistrito").ToUpper())
        //                        {
        //                            cCodDist = lstCCampos[objBEEstructuraArchivo.Orden].ToString().Trim();
        //                        }
        //                        if (objBEEstructuraArchivo.Nombre.ToUpper() == ("FechaInicioSeguroCredito").ToUpper())
        //                        {
        //                            String cYY = lstCCampos[objBEEstructuraArchivo.Orden].Substring(0, 4);
        //                            String cMM = lstCCampos[objBEEstructuraArchivo.Orden].Substring(4, 2);
        //                            String cDD = lstCCampos[objBEEstructuraArchivo.Orden].Substring(6, 2);
        //                            dFecInicio = Convert.ToDateTime(cDD + "/" + cMM + "/" + cYY);
        //                        }
        //                        if (objBEEstructuraArchivo.Nombre.ToUpper() == ("FechaFinSeguroCredito").ToUpper())
        //                        {
        //                            String cYY = lstCCampos[objBEEstructuraArchivo.Orden].Substring(0, 4);
        //                            String cMM = lstCCampos[objBEEstructuraArchivo.Orden].Substring(4, 2);
        //                            String cDD = lstCCampos[objBEEstructuraArchivo.Orden].Substring(6, 2);
        //                            dFecFin = Convert.ToDateTime(cDD + "/" + cMM + "/" + cYY);
        //                        }
        //                        if (objBEEstructuraArchivo.Nombre.ToUpper() == ("AnioFabricacion").ToUpper())
        //                        {
        //                            nAnioFab = Convert.ToInt32(lstCCampos[objBEEstructuraArchivo.Orden]);
        //                        }
        //                        if (objBEEstructuraArchivo.Nombre.ToUpper().Trim() == ("NumeroTelefono").ToUpper().Trim())
        //                        {
        //                            cTelefono1 = lstCCampos[objBEEstructuraArchivo.Orden].ToString(); 
        //                        }
        //                    }

        //                    BLModelo objBLModelo = new BLModelo();
        //                    BEModelo objBEModelo = objBLModelo.ObtenerPorCodExterno(cCodExtModelo, cCodExtMarca, true, true, NullTypes.CadenaNull);

        //                    if (objBEModelo == null)
        //                    {
        //                        bValido = false;
        //                        nObservaciones++;
        //                        cLog = "Fila nro. " + nFila + " - No está registrado el modelo del vehículo con código " + cCodExtModelo + " para el cliente con nro. de motor: " + cNroMotor;
        //                        lstCLog.Add(cLog);
        //                    }
        //                    if (objBEModelo != null) 
        //                    {
        //                        if (!objBEModelo.Asegurable)
        //                        {
        //                            bValido = false;
        //                            nObservaciones++;
        //                            cLog = "Fila nro. " + nFila + " - El modelo del vehículo con código " + cCodExtModelo + " no es asegurable, para el cliente con nro. de motor: " + cNroMotor;
        //                            lstCLog.Add(cLog);
        //                        }
        //                    }                            
        //                    if (dFecInicio.Year < nAnioFab)
        //                    {
        //                        bValido = false;
        //                        nObservaciones++;
        //                        cLog = "Fila nro. " + nFila + " - La fecha de inicio de vigencia es menor que el año de fabricación del vehículo, para el cliente con nro. de motor: " + cNroMotor;
        //                        lstCLog.Add(cLog);
        //                    }

        //                    if (dFecInicio.Year > dFecFin.Year)
        //                    {
        //                        bValido = false;
        //                        nObservaciones++;
        //                        cLog = "Fila nro. " + nFila + " - La fecha de inicio de vigencia es mayor a la fecha de fin de vigencia, para el cliente con nro. de motor: " + cNroMotor;
        //                        lstCLog.Add(cLog);
        //                    }

        //                    if (cTelefono1 == "0000000000" || cTelefono1.Trim() == "0" || cTelefono1 == String.Empty ) 
        //                    {
        //                        bValido = false;
        //                        nObservaciones++;
        //                        cLog = "Fila nro. " + nFila + " - El telefóno es incorrecto, para el cliente con nro. de motor: " + cNroMotor;
        //                        lstCLog.Add(cLog);
        //                    }

        //                    Int32 nIdCidad;
        //                    if (cCodDep.Trim() + cCodProv.Trim() + cCodDist.Trim() == String.Empty)
        //                    {
        //                        nIdCidad = 511501001;                                                                                                
        //                    }
        //                    else
        //                    {
        //                        nIdCidad = Convert.ToInt32("51" + cCodDep + cCodProv + cCodDist);
        //                    }

        //                    if (bValido == true)
        //                    {
        //                        BECliente objBECliente = new BECliente();
        //                        foreach (BEEstructuraArchivo objBEEstructuraArchivo in lstBEEstructuraArchivo)
        //                        {
        //                            if (objBEEstructuraArchivo.Nombre.ToUpper() == ("ApellidoPaterno").ToUpper()) { objBECliente.ApePaterno = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper(); }
        //                            if (objBEEstructuraArchivo.Nombre.ToUpper() == ("ApellidoMaterno").ToUpper()) { objBECliente.ApeMaterno = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper(); }
        //                            if (objBEEstructuraArchivo.Nombre.ToUpper() == ("Nombres").ToUpper()) { objBECliente.PriNombre = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper(); }
        //                            if (objBEEstructuraArchivo.Nombre.ToUpper() == ("DireccionDomiciliaria").ToUpper()) { objBECliente.Direccion = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper(); }
        //                            if (objBEEstructuraArchivo.Nombre.ToUpper() == ("MzLteNro").ToUpper()) { objBECliente.Direccion = lstCCampos[objBEEstructuraArchivo.Orden] + " " + objBECliente.Direccion.Trim().ToUpper(); }
        //                            if (objBEEstructuraArchivo.Nombre.ToUpper() == ("NumeroTelefono").ToUpper()) 
        //                            { 
        //                                objBECliente.TelDomicilio1 = lstCCampos[objBEEstructuraArchivo.Orden].Trim(); 
        //                            }
        //                            if (objBEEstructuraArchivo.Nombre.ToUpper() == ("NumeroTelefono2").ToUpper()) { objBECliente.TelMovil1 = lstCCampos[objBEEstructuraArchivo.Orden].Trim(); }
        //                            if (objBEEstructuraArchivo.Nombre.ToUpper() == ("NumeroTelefono3").ToUpper()) { objBECliente.TelOficina1 = lstCCampos[objBEEstructuraArchivo.Orden].Trim(); }
        //                            if (objBEEstructuraArchivo.Nombre.ToUpper() == ("CorreoElectronico").ToUpper()) { objBECliente.Email1 = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToLower(); }
        //                            if (objBEEstructuraArchivo.Nombre.ToUpper() == ("Color").ToUpper()) { objBECliente.Color = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper(); }
        //                            if (objBEEstructuraArchivo.Nombre.ToUpper() == ("NumeroChasis").ToUpper()) { objBECliente.NroSerie = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper(); }
        //                            if (objBEEstructuraArchivo.Nombre.ToUpper() == ("NumeroMotor").ToUpper()) { objBECliente.NroMotor = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper(); }
        //                            if (objBEEstructuraArchivo.Nombre.ToUpper() == ("NumeroMatriculaPlaca").ToUpper()) { objBECliente.NroPlaca = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper(); }
        //                            if (objBEEstructuraArchivo.Nombre.ToUpper() == ("IndicadorTimonCambiado").ToUpper())
        //                            {
        //                                if (lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper() == "S") { objBECliente.EsTimonCambiado = true; } else { objBECliente.EsTimonCambiado = false; }
        //                            }
        //                            if (objBEEstructuraArchivo.Nombre.ToUpper() == ("ValorVehiculo").ToUpper())
        //                            {
        //                                String cEntero = lstCCampos[objBEEstructuraArchivo.Orden].Substring(0, 13);
        //                                String cDecimal = lstCCampos[objBEEstructuraArchivo.Orden].Substring(13, 2);
        //                                Decimal nValorVehiculo = Convert.ToDecimal((cEntero + "." + cDecimal).Trim());

        //                                objBECliente.ValorVehiculo = Convert.ToDecimal(nValorVehiculo);
        //                            }
        //                        }

        //                        objBECliente.IdModelo = objBEModelo.IdModelo;
        //                        objBECliente.IdMarca = objBEModelo.IdMarca;
        //                        objBECliente.NroCertificado = NullTypes.CadenaNull;
        //                        objBECliente.CodAsegurador = 5;
        //                        objBECliente.SegNombre = NullTypes.CadenaNull;
        //                        if (cCodTipoReg == "01") { objBECliente.IdTipoDocumento = "L"; } else { objBECliente.IdTipoDocumento = "R"; }
        //                        objBECliente.NroDocumento = String.Empty;
        //                        objBECliente.Sexo = NullTypes.CadenaNull;
        //                        objBECliente.FecNacimiento = NullTypes.FechaNull;
        //                        objBECliente.idCiudad = nIdCidad;
        //                        objBECliente.FecVigencia = dFecInicio;
        //                        objBECliente.FecFinVigencia = dFecFin;
        //                        objBECliente.TelDomicilio2 = String.Empty;
        //                        objBECliente.TelDomicilio3 = String.Empty;
        //                        objBECliente.TelOficina2 = String.Empty;
        //                        objBECliente.TelOficina3 = String.Empty;
        //                        objBECliente.TelMovil2 = String.Empty;
        //                        objBECliente.TelMovil3 = String.Empty;
        //                        objBECliente.Email1 = String.Empty;
        //                        objBECliente.Email2 = String.Empty;
        //                        objBECliente.AnioFab = nAnioFab;
        //                        objBECliente.IdUsoVehiculo = 1;
        //                        objBECliente.IdClase = objBEModelo.IdClase;
        //                        objBECliente.NroAsientos = NullTypes.IntegerNull;
        //                        objBECliente.EsBlindado = false;
        //                        objBECliente.ReqGPS = false;
        //                        objBECliente.IdSponsor = 1; //BBVA 
        //                        objBECliente.IdProceso = pObjBEProceso.IdProceso;
        //                        objBECliente.IdEstadoCliente = 5;
        //                        objBECliente.UsuarioCreacion = pcUsuario;
        //                        objBECliente.EstadoRegistro = true;

        //                        BECliente objBECliente_Temp = objBLCliente.Obtener(objBECliente.NroMotor);

        //                        if (objBECliente_Temp != null)
        //                        {
        //                            cLog = "Fila nro. " + nFila.ToString() + " - Ya se encuentra registrado el cliente con nro. de motor: " + objBECliente.NroMotor;
        //                            lstCLog.Add(cLog);
        //                            nObservaciones++;
        //                        }
        //                        else
        //                        {
        //                            if (objBLCliente.Insertar(objBECliente) > 0)
        //                            {
        //                                nValidos++;
        //                                cLog = "Fila nro. " + nFila + " - Se registró el cliente con nro. de motor: " + objBECliente.NroMotor;
        //                                lstCLog.Add(cLog);
        //                            }
        //                            else
        //                            {
        //                                nObservaciones++;
        //                                cLog = "Fila nro. " + nFila + " - No se pudo registrar el cliente con nro. de motor: " + objBECliente.NroMotor;
        //                                lstCLog.Add(cLog);
        //                            }
        //                        }
        //                    }
        //                } 
        //            }

        //            nFila++;
        //            fsArchivo.Flush();
        //        }

        //        fsArchivo.Close();
        //    }

        //    //Actualizar proceso            
        //    this.EscribirErrores(lstCLog, pObjBETipoProceso.RutaArchivoLog, cArchivoLog);

        //    pObjBEProceso.Procesado = true;
        //    pObjBEProceso.UsuarioModificacion = pcUsuario;
        //    pObjBEProceso.ArchivoLog = cArchivoLog;
        //    pObjBEProceso.RegTotal = nFila;
        //    pObjBEProceso.RegTotalVal = nValidos;
        //    pObjBEProceso.RegTotalErr = nFila - nValidos;
        //    pObjBEProceso.NroObservaciones = nObservaciones;

        //    this.ActualizarLog(pObjBEProceso);
        //}
        #endregion

        #region Proceso RIMAC - AON Clientes BBVA
        //private void ProcesoImportRIMACSiniestroBBVA(BETipoProceso pObjBETipoProceso, BEProceso pObjBEProceso, String pcUsuario)
        //{
        //    String cArchivoLog = pObjBEProceso.Archivo;
        //    cArchivoLog = pObjBEProceso.Archivo.Replace(".xls", ".txt");
        //    cArchivoLog = "Log_" + cArchivoLog;

        //    Int32 nObservaciones = 0;
        //    String cLog = String.Empty;
        //    List<String> lstCLog = new List<String>();
        //    Boolean bEstructura = true;

        //    BLEstructuraArchivo objBLEstructuraArchivo = new BLEstructuraArchivo();
        //    List<BEEstructuraArchivo> lstBEEstructuraArchivo = objBLEstructuraArchivo.Listar(pObjBETipoProceso.IdTipoProceso, true);

        //    if (lstBEEstructuraArchivo == null)
        //    {
        //        bEstructura = false;
        //        nObservaciones++;
        //        cLog = "No existe ninguna estructura configurada para el archivo.";
        //        lstCLog.Add(cLog);  
        //    }

        //    if (!bEstructura) 
        //    {
        //        //Actualizar proceso            
        //        this.EscribirErrores(lstCLog, pObjBETipoProceso.RutaArchivoLog, cArchivoLog);
        //        pObjBEProceso.Procesado = true;
        //        pObjBEProceso.UsuarioModificacion = pcUsuario;
        //        pObjBEProceso.ArchivoLog = cArchivoLog;
        //        pObjBEProceso.RegTotal = 0;
        //        pObjBEProceso.RegTotalVal = 0;
        //        pObjBEProceso.RegTotalErr = 0;
        //        pObjBEProceso.NroObservaciones = nObservaciones;
        //        this.ActualizarLog(pObjBEProceso);
        //        return;
        //    }

        //    BLArchivo objBLArchivo = new BLArchivo();
        //    String cConexion = String.Format(AppSettings.ProviderExcel, pObjBETipoProceso.RutaArchivo + pObjBEProceso.Archivo);
        //    OleDbConnection oleCn = new OleDbConnection(cConexion);
        //    oleCn.Open();
        //    OleDbDataReader oleDr = objBLArchivo.LeerExcel(oleCn);                        
        //    Int32 nFila = 0;
        //    Boolean bValido = true;
        //    Int32 nValidos = 0;            

        //    if (oleDr != null)
        //    {
        //        if (oleDr.HasRows)
        //        {
        //            while (oleDr.Read())
        //            {
        //                bValido = true;

        //                if (!this.ValidarOrdenCampos(lstBEEstructuraArchivo, oleDr, ref lstCLog))
        //                {                            
        //                    bValido = false;
        //                }
        //                if (!this.ValidarCampoObligatorio(lstBEEstructuraArchivo, nFila, oleDr, ref lstCLog))
        //                {
        //                    bValido = false;
        //                }

        //                if (!this.ValidarCampoTipoDato(lstBEEstructuraArchivo, nFila, oleDr, ref lstCLog))
        //                {
        //                    bValido = false;
        //                }

        //                if (bValido == true) 
        //                {
        //                    if (!ValidarCampoValores(nFila, oleDr, ref lstCLog)) 
        //                    {
        //                        bValido = false;
        //                    }
        //                }

        //                //Insertar el registro
        //                if (bValido == true)
        //                {
        //                    Int32 nIndex0 = oleDr.GetOrdinal("CANAL");
        //                    Int32 nIndex1 = oleDr.GetOrdinal("POLIZA");
        //                    Int32 nIndex2 = oleDr.GetOrdinal("PLACA");
        //                    Int32 nIndex3 = oleDr.GetOrdinal("NRO_MOTOR");
        //                    Int32 nIndex4 = oleDr.GetOrdinal("NRO_CERTIFICADO");
        //                    Int32 nIndex5 = oleDr.GetOrdinal("CONTRATANTE");
        //                    Int32 nIndex6 = oleDr.GetOrdinal("ID_CONTRATANTE");
        //                    Int32 nIndex7 = oleDr.GetOrdinal("ID_ASEGURADO");
        //                    Int32 nIndex8 = oleDr.GetOrdinal("ASEGURADO");
        //                    Int32 nIndex9 = oleDr.GetOrdinal("MARCA_VEHICULOS");
        //                    Int32 nIndex10 = oleDr.GetOrdinal("MODELO_VEHICULOS");
        //                    Int32 nIndex11 = oleDr.GetOrdinal("ANO_FABRICACION");
        //                    Int32 nIndex12 = oleDr.GetOrdinal("INICIO_VIGENCIA");
        //                    Int32 nIndex13 = oleDr.GetOrdinal("FIN_VIGENCIA");
        //                    Int32 nIndex14 = oleDr.GetOrdinal("SINIESTROS_ANIO1");
        //                    Int32 nIndex15 = oleDr.GetOrdinal("NRO_SINIESTROS_ANIO1");
        //                    Int32 nIndex16 = oleDr.GetOrdinal("SINIESTROS_ANIO2");
        //                    Int32 nIndex17 = oleDr.GetOrdinal("NRO_SINIESTROS_ANIO2");
        //                    Int32 nIndex18 = oleDr.GetOrdinal("SINIESTROS_ANIO3");
        //                    Int32 nIndex19 = oleDr.GetOrdinal("NRO_SINIESTROS_ANIO3");
        //                    Int32 nIndex20 = oleDr.GetOrdinal("SINIESTROS_ANIO4");
        //                    Int32 nIndex21 = oleDr.GetOrdinal("NRO_SINIESTROS_ANIO4");
        //                    Int32 nIndex22 = oleDr.GetOrdinal("SINIESTROS_ANIO5");
        //                    Int32 nIndex23 = oleDr.GetOrdinal("NRO_SINIESTROS_ANIO5");
        //                    Int32 nIndex24 = oleDr.GetOrdinal("IND_CLIENTE");

        //                    BLCliente objBLCliente = new BLCliente();
        //                    BECliente objBECliente = objBLCliente.Obtener(oleDr.GetValue(nIndex3).ToString());

        //                    //Si no se encuentra registrado el nro motor del cliente, agregar a log el detalle.
        //                    if (objBECliente == null)
        //                    {                                
        //                        cLog = "Fila nro. " + nFila.ToString() + " - No se encuentra registrado el cliente con nro de motor: " + oleDr.GetValue(nIndex3).ToString();
        //                        lstCLog.Add(cLog);
        //                        nObservaciones++;
        //                    }
        //                    else
        //                    {
        //                        //Si cliente no es valido para filtro de RIMAC, actualizar el estado del clie
        //                        if (oleDr.GetValue(nIndex24).ToString() == "N")
        //                        {
        //                            if (objBLCliente.Atualizar(oleDr.GetValue(nIndex0).ToString(), 15, pcUsuario) > 0)
        //                            {
        //                                cLog = "Fila nro. " + nFila.ToString() + " - Se actualizó el estado del cliente como no cotizable por aseguradora, nro de motor: " + oleDr.GetValue(nIndex3).ToString();
        //                                lstCLog.Add(cLog);
        //                            }
        //                            else
        //                            {
        //                                cLog = "Fila nro. " + nFila.ToString() + " - No se pudo actualizar el estado del cliente como no cotizable por aseguradora, nro de motor: " + oleDr.GetValue(nIndex3).ToString();
        //                                lstCLog.Add(cLog);
        //                                nObservaciones++;
        //                            }
        //                        }

        //                        BLVigenciaSiniestro objBLVigenciaSiniestro = new BLVigenciaSiniestro();
        //                        BEVigenciaSiniestro objBEVigenciaSiniestro = objBLVigenciaSiniestro.Obtener(oleDr.GetValue(nIndex3).ToString());

        //                        //Si ya existe siniestro registrado, agregar a log el detalle. 
        //                        if (objBEVigenciaSiniestro != null)
        //                        {                                    
        //                            cLog = "Fila nro. " + nFila.ToString() + " - La información del siniestro ya se encuentra registrada, nro : " + oleDr.GetValue(nIndex3).ToString();
        //                            lstCLog.Add(cLog);
        //                            nObservaciones++;
        //                        }
        //                        else
        //                        {
        //                            objBEVigenciaSiniestro = new BEVigenciaSiniestro();

        //                            objBEVigenciaSiniestro.NroMotor = oleDr.GetString(nIndex3);
        //                            objBEVigenciaSiniestro.CantSiniestro1Anio = Convert.ToInt32(oleDr.GetValue(nIndex15).ToString());
        //                            if (oleDr.IsDBNull(nIndex17)) { objBEVigenciaSiniestro.CantSiniestro2Anio = NullTypes.IntegerNull; } else { objBEVigenciaSiniestro.CantSiniestro2Anio = Convert.ToInt32(oleDr.GetValue(nIndex17).ToString()); }
        //                            if (oleDr.IsDBNull(nIndex19)) { objBEVigenciaSiniestro.CantSiniestro3Anio = NullTypes.IntegerNull; } else { objBEVigenciaSiniestro.CantSiniestro3Anio = Convert.ToInt32(oleDr.GetValue(nIndex19).ToString()); }
        //                            if (oleDr.IsDBNull(nIndex21)) { objBEVigenciaSiniestro.CantSiniestro4Anio = NullTypes.IntegerNull; } else { objBEVigenciaSiniestro.CantSiniestro4Anio = Convert.ToInt32(oleDr.GetValue(nIndex21).ToString()); }
        //                            if (oleDr.IsDBNull(nIndex23)) { objBEVigenciaSiniestro.CantSiniestro5Anio = NullTypes.IntegerNull; } else { objBEVigenciaSiniestro.CantSiniestro5Anio = Convert.ToInt32(oleDr.GetValue(nIndex23).ToString()); }
        //                            objBEVigenciaSiniestro.MontoSiniestro1Anio = Convert.ToDecimal(oleDr.GetValue(nIndex14));
        //                            if (oleDr.IsDBNull(nIndex16)) { objBEVigenciaSiniestro.MontoSiniestro2Anio = NullTypes.DecimalNull; } else { objBEVigenciaSiniestro.MontoSiniestro2Anio = Convert.ToDecimal(oleDr.GetValue(nIndex16).ToString()); }
        //                            if (oleDr.IsDBNull(nIndex18)) { objBEVigenciaSiniestro.MontoSiniestro3Anio = NullTypes.DecimalNull; } else { objBEVigenciaSiniestro.MontoSiniestro3Anio = Convert.ToDecimal(oleDr.GetValue(nIndex18).ToString()); }
        //                            if (oleDr.IsDBNull(nIndex20)) { objBEVigenciaSiniestro.MontoSiniestro4Anio = NullTypes.DecimalNull; } else { objBEVigenciaSiniestro.MontoSiniestro4Anio = Convert.ToDecimal(oleDr.GetValue(nIndex20).ToString()); }
        //                            if (oleDr.IsDBNull(nIndex22)) { objBEVigenciaSiniestro.MontoSiniestro5Anio = NullTypes.DecimalNull; } else { objBEVigenciaSiniestro.MontoSiniestro5Anio = Convert.ToDecimal(oleDr.GetValue(nIndex22).ToString()); }
        //                            objBEVigenciaSiniestro.FecIniVigencia = Convert.ToDateTime(oleDr.GetValue(nIndex12).ToString());
        //                            objBEVigenciaSiniestro.FecFinVigencia = Convert.ToDateTime(oleDr.GetValue(nIndex13).ToString());
        //                            objBEVigenciaSiniestro.UsuarioCreacion = pcUsuario;

        //                            if (objBLVigenciaSiniestro.Insertar(objBEVigenciaSiniestro) > 0)
        //                            {
        //                                cLog = "Fila nro. " + nFila.ToString() + " - Se registró la infomación del siniestro, nro : " + oleDr.GetValue(nIndex3).ToString();
        //                                lstCLog.Add(cLog);
        //                                nValidos++;
        //                            }
        //                            else
        //                            {
        //                                cLog = "Fila nro. " + nFila.ToString() + " - No se pudo registrar la infomación del siniestro, nro : " + oleDr.GetValue(nIndex3).ToString();
        //                                lstCLog.Add(cLog);
        //                                nObservaciones++;
        //                            }
        //                        }
        //                    }
        //                }
        //                nFila++;
        //            }
        //        }
        //    }

        //    oleCn.Close();

        //    //Actualizar proceso            
        //    this.EscribirErrores(lstCLog, pObjBETipoProceso.RutaArchivoLog, cArchivoLog);
        //    pObjBEProceso.Procesado = true;
        //    pObjBEProceso.UsuarioModificacion = pcUsuario;
        //    pObjBEProceso.ArchivoLog = cArchivoLog;
        //    pObjBEProceso.RegTotal = nFila;
        //    pObjBEProceso.RegTotalVal = nValidos;
        //    pObjBEProceso.RegTotalErr = nFila - nValidos;
        //    pObjBEProceso.NroObservaciones = nObservaciones;
        //    this.ActualizarLog(pObjBEProceso);
        //}        
        #endregion

        #region Funciones Privadas
        //ok
        private Boolean ValidarOrdenCampos(List<BEEstructuraArchivo> lstBEEstructuraArchivo, DbDataReader pDr, ref List<String> pLstError)
        {
            Boolean bResult = true;
            String cError = String.Empty;

            foreach (BEEstructuraArchivo objBEEstructuraArchivo in lstBEEstructuraArchivo)
            {
                if (pDr.GetName(objBEEstructuraArchivo.Orden) != objBEEstructuraArchivo.Nombre)
                {
                    bResult = false;
                    cError = "El nombre de la columna " + objBEEstructuraArchivo.Orden + " debe ser: " + objBEEstructuraArchivo.Nombre;
                    pLstError.Add(cError);
                }
            }
            return bResult;
        }

        //ok
        private Boolean ValidarCampoObligatorio(List<BEEstructuraArchivo> lstBEEstructuraArchivo, Int32 pnFila, DbDataReader pDr, ref List<String> pLstError)
        {
            Boolean bResult = true;
            String cError = String.Empty;

            foreach (BEEstructuraArchivo objBEEstructuraArchivo in lstBEEstructuraArchivo)
            {
                if (objBEEstructuraArchivo.Obligatorio == true)
                {
                    if (pDr.GetValue(objBEEstructuraArchivo.Orden) == DBNull.Value)
                    {
                        bResult = false;
                        cError = "Fila nro. " + pnFila.ToString() + " - El campo " + objBEEstructuraArchivo.Nombre + " es obligatorio.";
                        pLstError.Add(cError);
                    }
                }
            }

            return bResult;
        }

        //ok
        private Boolean ValidarCampoObligatorio(List<BEEstructuraArchivo> lstBEEstructuraArchivo, Int32 pnFila, List<String> pLstCFilaTrama, ref List<String> pLstError)
        {
            Boolean bResult = true;
            String cError = String.Empty;

            foreach (BEEstructuraArchivo objBEEstructuraArchivo in lstBEEstructuraArchivo)
            {
                if (objBEEstructuraArchivo.Obligatorio == true)
                {
                    if (pLstCFilaTrama[objBEEstructuraArchivo.Orden].Trim() == String.Empty)
                    {
                        bResult = false;
                        cError = "Fila nro. " + pnFila.ToString() + " - El campo " + objBEEstructuraArchivo.Nombre + " es obligatorio.";
                        pLstError.Add(cError);
                    }
                }
            }

            return bResult;
        }

        //ok
        private Boolean ValidarCampoTipoDato(List<BEEstructuraArchivo> lstBEEstructuraArchivo, Int32 pnFila, DbDataReader pDr, ref List<String> pLstError)
        {
            Boolean bResult = true;
            String cError = String.Empty;

            foreach (BEEstructuraArchivo objBEEstructuraArchivo in lstBEEstructuraArchivo)
            {
                //Si campo es obligatorio
                if (objBEEstructuraArchivo.Obligatorio == true)
                {
                    //Si tipo de dato es Number
                    if (objBEEstructuraArchivo.TipoDato == "N")
                    {
                        Decimal nValor;

                        if (Decimal.TryParse(pDr.GetValue(objBEEstructuraArchivo.Orden).ToString().Trim(), out nValor) != true)
                        {
                            bResult = false;
                            cError = "Fila nro. " + pnFila.ToString() + " - El campo " + objBEEstructuraArchivo.Nombre + " debe ser de tipo numérico.";
                            pLstError.Add(cError);
                        }
                    }

                    //Si tipo de dato es Date
                    if (objBEEstructuraArchivo.TipoDato == "D")
                    {
                        DateTime dValor;

                        if (DateTime.TryParse(pDr.GetValue(objBEEstructuraArchivo.Orden).ToString().Trim(), out dValor) != true)
                        {
                            bResult = false;
                            cError = "Fila nro. " + pnFila.ToString() + " - El campo " + objBEEstructuraArchivo.Nombre + " debe ser de tipo fecha.";
                            pLstError.Add(cError);
                        }
                    }

                    //Si tipo de dato es String
                    if (objBEEstructuraArchivo.TipoDato == "S")
                    {
                        if (objBEEstructuraArchivo.ValorFijo != NullTypes.CadenaNull)
                        {
                            if (this.ObtenerNroValoresFijos(objBEEstructuraArchivo.ValorFijo) > 0)
                            {
                                String[] arrcValores = objBEEstructuraArchivo.ValorFijo.Split(',');

                                if (!ValidarValorFijo(arrcValores, pDr.GetValue(objBEEstructuraArchivo.Orden).ToString()))
                                {
                                    bResult = false;
                                    cError = "Fila nro. " + pnFila.ToString() + " - El campo " + objBEEstructuraArchivo.Nombre + " debe contener el siguientes valores: " + objBEEstructuraArchivo.ValorFijo;
                                    pLstError.Add(cError);
                                }
                            }
                            else
                            {
                                if (pDr.GetValue(objBEEstructuraArchivo.Orden).ToString().Trim().ToUpper() != objBEEstructuraArchivo.ValorFijo.Trim().ToUpper())
                                {
                                    bResult = false;
                                    cError = "Fila nro. " + pnFila.ToString() + " - El campo " + objBEEstructuraArchivo.Nombre + " debe contener el siguiente valor: " + objBEEstructuraArchivo.ValorFijo;
                                    pLstError.Add(cError);
                                }
                            }
                        }
                    }
                }
            }

            return bResult;
        }

        //ok
        private Boolean ValidarCampoTipoDato(List<BEEstructuraArchivo> lstBEEstructuraArchivo, Int32 pnFila, List<String> pLstCFilaTrama, ref List<String> pLstError)
        {
            Boolean bResult = true;
            String cError = String.Empty;

            foreach (BEEstructuraArchivo objBEEstructuraArchivo in lstBEEstructuraArchivo)
            {
                if (objBEEstructuraArchivo.Obligatorio == true)
                {
                    //Si tipo de dato es Number
                    if (objBEEstructuraArchivo.TipoDato == "N")
                    {
                        Decimal nValor;

                        if (Decimal.TryParse(pLstCFilaTrama[objBEEstructuraArchivo.Orden].ToString().Trim(), out nValor) != true)
                        {
                            bResult = false;
                            cError = "Fila nro. " + pnFila.ToString() + " - El campo " + objBEEstructuraArchivo.Nombre + " debe ser de tipo numérico.";
                            pLstError.Add(cError);
                        }
                    }

                    //Si tipo de dato es Date
                    if (objBEEstructuraArchivo.TipoDato == "D")
                    {
                        String cValor = pLstCFilaTrama[objBEEstructuraArchivo.Orden].ToString().Trim();

                        if (cValor.Length != 8)
                        {
                            bResult = false;
                            cError = "Fila nro. " + pnFila.ToString() + " - El campo " + objBEEstructuraArchivo.Nombre + " debe ser de formato AAAAMMDD.";
                            pLstError.Add(cError);
                        }
                        else
                        {
                            DateTime dValor;
                            String cYY = cValor.Substring(0, 4);
                            String cMM = cValor.Substring(4, 2);
                            String cDD = cValor.Substring(6, 2);

                            if (DateTime.TryParse(cDD + "/" + cMM + "/" + cDD, out dValor) != true)
                            {
                                bResult = false;
                                cError = "Fila nro. " + pnFila.ToString() + " - El campo " + objBEEstructuraArchivo.Nombre + " debe ser de tipo fecha.";
                                pLstError.Add(cError);
                            }
                        }
                    }

                    //Si tipo de dato es String
                    if (objBEEstructuraArchivo.TipoDato == "S")
                    {
                        if (objBEEstructuraArchivo.ValorFijo != NullTypes.CadenaNull)
                        {
                            if (this.ObtenerNroValoresFijos(objBEEstructuraArchivo.ValorFijo) > 0)
                            {
                                String[] arrcValores = objBEEstructuraArchivo.ValorFijo.Split(',');

                                if (!ValidarValorFijo(arrcValores, pLstCFilaTrama[objBEEstructuraArchivo.Orden].ToString().Trim()))
                                {
                                    bResult = false;
                                    cError = "Fila nro. " + pnFila.ToString() + " - El campo " + objBEEstructuraArchivo.Nombre + " debe contener el siguientes valores: " + objBEEstructuraArchivo.ValorFijo;
                                    pLstError.Add(cError);
                                }
                            }
                            else
                            {
                                if (pLstCFilaTrama[objBEEstructuraArchivo.Orden].ToString().Trim().ToUpper() != objBEEstructuraArchivo.ValorFijo.Trim().ToUpper())
                                {
                                    bResult = false;
                                    cError = "Fila nro. " + pnFila.ToString() + " - El campo " + objBEEstructuraArchivo.Nombre + " debe contener el siguiente valor: " + objBEEstructuraArchivo.ValorFijo;
                                    pLstError.Add(cError);
                                }
                            }
                        }
                    }
                }
            }

            return bResult;
        }

        //
        private Boolean ValidarCampoValores(Int32 pnFila, DbDataReader pDr, ref List<String> pLstError)
        {
            Boolean bResult = true;
            String cError = String.Empty;

            Int32 nNroMotor = pDr.GetOrdinal("NRO_MOTOR");
            String cNroMotor = pDr.GetValue(nNroMotor).ToString();

            Int32 nMontSin1Anio = pDr.GetOrdinal("SINIESTROS_ANIO1");
            Int32 nMontSin2Anio = pDr.GetOrdinal("SINIESTROS_ANIO2");
            Int32 nMontSin3Anio = pDr.GetOrdinal("SINIESTROS_ANIO3");
            Int32 nMontSin4Anio = pDr.GetOrdinal("SINIESTROS_ANIO4");
            Int32 nMontSin5Anio = pDr.GetOrdinal("SINIESTROS_ANIO5");

            Int32 nCantSin1Anio = pDr.GetOrdinal("NRO_SINIESTROS_ANIO1");
            Int32 nCantSin2Anio = pDr.GetOrdinal("NRO_SINIESTROS_ANIO2");
            Int32 nCantSin3Anio = pDr.GetOrdinal("NRO_SINIESTROS_ANIO3");
            Int32 nCantSin4Anio = pDr.GetOrdinal("NRO_SINIESTROS_ANIO4");
            Int32 nCantSin5Anio = pDr.GetOrdinal("NRO_SINIESTROS_ANIO5");

            //Año 1
            if (Convert.ToDecimal(pDr.GetValue(nCantSin1Anio)) > 0 && Convert.ToDecimal(pDr.GetValue(nMontSin1Anio)) == 0)
            {
                cError = "Fila nro. " + pnFila + " - Error en monto de siniestro de año 1 nro: " + cNroMotor;
                pLstError.Add(cError);
                bResult = false;
            }
            if (Convert.ToDecimal(pDr.GetValue(nCantSin1Anio)) == 0 && Convert.ToDecimal(pDr.GetValue(nMontSin1Anio)) > 0)
            {
                cError = "Fila nro. " + pnFila + " - Error en canidad de siniestro de año 1 nro: " + cNroMotor;
                pLstError.Add(cError);
                bResult = false;
            }

            //Año 2
            if (Convert.ToDecimal(pDr.GetValue(nCantSin2Anio)) > 0 && Convert.ToDecimal(pDr.GetValue(nMontSin2Anio)) == 0)
            {
                cError = "Fila nro. " + pnFila + " - Error en monto de siniestro del año 2 nro: " + cNroMotor;
                pLstError.Add(cError);
                bResult = false;
            }
            if (Convert.ToDecimal(pDr.GetValue(nCantSin2Anio)) == 0 && Convert.ToDecimal(pDr.GetValue(nMontSin2Anio)) > 0)
            {
                cError = "Fila nro. " + pnFila + " - Error en canidad de siniestro del año 2 nro: " + cNroMotor;
                pLstError.Add(cError);
                bResult = false;
            }

            //Año 3
            if (Convert.ToDecimal(pDr.GetValue(nCantSin3Anio)) > 0 && Convert.ToDecimal(pDr.GetValue(nMontSin3Anio)) == 0)
            {
                cError = "Fila nro. " + pnFila + " - Error en monto de siniestro del año 3 nro: " + cNroMotor;
                pLstError.Add(cError);
                bResult = false;
            }
            if (Convert.ToDecimal(pDr.GetValue(nCantSin3Anio)) == 0 && Convert.ToDecimal(pDr.GetValue(nMontSin3Anio)) > 0)
            {
                cError = "Fila nro. " + pnFila + " - Error en canidad de siniestro del año 3 nro: " + cNroMotor;
                pLstError.Add(cError);
                bResult = false;
            }

            //Año 4
            if (Convert.ToDecimal(pDr.GetValue(nCantSin4Anio)) > 0 && Convert.ToDecimal(pDr.GetValue(nMontSin4Anio)) == 0)
            {
                cError = "Fila nro. " + pnFila + " - Error en monto de siniestro del año 4 nro: " + cNroMotor;
                pLstError.Add(cError);
                bResult = false;
            }
            if (Convert.ToDecimal(pDr.GetValue(nCantSin4Anio)) == 0 && Convert.ToDecimal(pDr.GetValue(nMontSin4Anio)) > 0)
            {
                cError = "Fila nro. " + pnFila + " - Error en canidad de siniestro del año 4 nro: " + cNroMotor;
                pLstError.Add(cError);
                bResult = false;
            }

            //Año 5
            if (Convert.ToDecimal(pDr.GetValue(nCantSin5Anio)) > 0 && Convert.ToDecimal(pDr.GetValue(nMontSin5Anio)) == 0)
            {
                cError = "Fila nro. " + pnFila + " - Error en monto de siniestro del año 5 nro: " + cNroMotor;
                pLstError.Add(cError);
                bResult = false;
            }
            if (Convert.ToDecimal(pDr.GetValue(nCantSin5Anio)) == 0 && Convert.ToDecimal(pDr.GetValue(nMontSin5Anio)) > 0)
            {
                cError = "Fila nro. " + pnFila + " - Error en canidad de siniestro del año 5 nro: " + cNroMotor;
                pLstError.Add(cError);
                bResult = false;
            }

            return bResult;
        }

        //ok
        private Int32 ObtenerNroValoresFijos(String pcCadena)
        {
            Int32 nResult = 0;

            if (pcCadena.Length > 1)
            {
                Int32 nIndex = 0;

                while (nIndex <= pcCadena.Length - 1)
                {
                    if (pcCadena[nIndex] == ',')
                    {
                        nResult++;
                    }

                    nIndex++;
                }
            }

            return nResult;
        }

        //ok
        private Boolean ValidarValorFijo(String[] pcArrValores, String pcValor)
        {
            Boolean bResult = false;
            Int32 nIndex = 0;

            while (nIndex < pcArrValores.Length)
            {
                if (bResult != true)
                {
                    if (pcArrValores[nIndex].Trim().ToUpper() == pcValor.Trim().ToUpper())
                    {
                        bResult = true;
                    }
                }

                nIndex++;
            }

            return bResult;
        }

        //ok
        private List<String> TramaSplit(String pcTrama, List<BEEstructuraArchivo> lstBEEstructuraArchivo)
        {
            List<String> lstCampos = new List<String>();
            String cTrama = String.Empty;
            Int32 nColumna = 0;
            Int32 nPosicion = 0;

            //cTrama = new String[lstBEEstructuraArchivo.Count];

            foreach (BEEstructuraArchivo objBEEstructuraArchivo in lstBEEstructuraArchivo)
            {
                cTrama = pcTrama.Substring(nPosicion, objBEEstructuraArchivo.Longitud);
                lstCampos.Add(cTrama);
                nPosicion = nPosicion + objBEEstructuraArchivo.Longitud;
                nColumna++;
            }

            return lstCampos;
        }
        #endregion

        #region NoTransaccional
        //ok
        public List<BEProceso> ListarDisponible()
        {
            List<BEProceso> lstBEProceso = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAProceso objDAProceso = new DAProceso();
                SqlDataReader sqlDr = objDAProceso.ListarDisponible(sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idProceso");
                        Int32 nIndex2 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("archivo");
                        Int32 nIndex4 = sqlDr.GetOrdinal("procesado");
                        Int32 nIndex5 = sqlDr.GetOrdinal("idTipProceso");
                        Int32 nIndex6 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex8 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex9 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex10 = sqlDr.GetOrdinal("stsRegistro");

                        lstBEProceso = new List<BEProceso>();

                        while (sqlDr.Read())
                        {
                            BEProceso objBEProceso = new BEProceso();

                            objBEProceso.IdProceso = sqlDr.GetInt32(nIndex1);
                            objBEProceso.Descripcion = sqlDr.GetString(nIndex2);
                            objBEProceso.Archivo = sqlDr.GetString(nIndex3);
                            objBEProceso.Procesado = sqlDr.GetBoolean(nIndex4);
                            objBEProceso.IdTipProceso = sqlDr.GetInt32(nIndex5);
                            objBEProceso.UsuarioCreacion = sqlDr.GetString(nIndex6);
                            objBEProceso.FechaCreacion = sqlDr.GetDateTime(nIndex7);
                            if (sqlDr.IsDBNull(nIndex8)) { objBEProceso.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEProceso.UsuarioModificacion = sqlDr.GetString(nIndex8); }
                            if (sqlDr.IsDBNull(nIndex9)) { objBEProceso.FechaModificacion = NullTypes.FechaNull; } else { objBEProceso.FechaModificacion = sqlDr.GetDateTime(nIndex9); }
                            objBEProceso.EstadoRegistro = sqlDr.GetBoolean(nIndex10);

                            lstBEProceso.Add(objBEProceso);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBEProceso;
        }

        //ok
        public List<BEProceso> ListarDisponible(Int32 pnIdTipProceso)
        {
            List<BEProceso> lstBEProceso = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAProceso objDAProceso = new DAProceso();
                SqlDataReader sqlDr = objDAProceso.ListarDisponible(pnIdTipProceso, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idProceso");
                        Int32 nIndex2 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("archivo");
                        Int32 nIndex4 = sqlDr.GetOrdinal("procesado");
                        Int32 nIndex5 = sqlDr.GetOrdinal("idTipProceso");
                        Int32 nIndex6 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex8 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex9 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex10 = sqlDr.GetOrdinal("stsRegistro");

                        lstBEProceso = new List<BEProceso>();


                        while (sqlDr.Read())
                        {
                            BEProceso objBEProceso = new BEProceso();

                            objBEProceso.IdProceso = sqlDr.GetInt32(nIndex1);
                            objBEProceso.Descripcion = sqlDr.GetString(nIndex2);
                            objBEProceso.Archivo = sqlDr.GetString(nIndex3);
                            objBEProceso.Procesado = sqlDr.GetBoolean(nIndex4);
                            objBEProceso.IdTipProceso = sqlDr.GetInt32(nIndex5);
                            objBEProceso.UsuarioCreacion = sqlDr.GetString(nIndex6);
                            objBEProceso.FechaCreacion = sqlDr.GetDateTime(nIndex7);
                            if (sqlDr.IsDBNull(nIndex8)) { objBEProceso.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEProceso.UsuarioModificacion = sqlDr.GetString(nIndex8); }
                            if (sqlDr.IsDBNull(nIndex9)) { objBEProceso.FechaModificacion = NullTypes.FechaNull; } else { objBEProceso.FechaModificacion = sqlDr.GetDateTime(nIndex9); }
                            objBEProceso.EstadoRegistro = sqlDr.GetBoolean(nIndex10);

                            lstBEProceso.Add(objBEProceso);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBEProceso;
        }

        //ok
        public List<BEProceso> Listar(Nullable<Boolean> pbStsRegistro)
        {
            List<BEProceso> lstBEProceso = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAProceso objDAProceso = new DAProceso();
                SqlDataReader sqlDr = objDAProceso.Listar(pbStsRegistro, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idProceso");
                        Int32 nIndex2 = sqlDr.GetOrdinal("desTipProceso");
                        Int32 nIndex3 = sqlDr.GetOrdinal("archivo");
                        Int32 nIndex4 = sqlDr.GetOrdinal("procesado");
                        Int32 nIndex5 = sqlDr.GetOrdinal("archivoLog");
                        Int32 nIndex6 = sqlDr.GetOrdinal("fecProceso");
                        Int32 nIndex7 = sqlDr.GetOrdinal("idTipProceso");
                        Int32 nIndex8 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex9 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex10 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex11 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex12 = sqlDr.GetOrdinal("stsRegistro");

                        lstBEProceso = new List<BEProceso>();

                        while (sqlDr.Read())
                        {
                            BEProceso objBEProceso = new BEProceso();

                            objBEProceso.IdProceso = sqlDr.GetInt32(nIndex1);
                            objBEProceso.DesTipProceso = sqlDr.GetString(nIndex2);
                            objBEProceso.Archivo = sqlDr.GetString(nIndex3);
                            objBEProceso.Procesado = sqlDr.GetBoolean(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBEProceso.ArchivoLog = NullTypes.CadenaNull; } else { objBEProceso.ArchivoLog = sqlDr.GetString(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBEProceso.FecProceso = NullTypes.FechaNull; } else { objBEProceso.FecProceso = sqlDr.GetDateTime(nIndex6); }
                            objBEProceso.IdTipProceso = sqlDr.GetInt32(nIndex7);
                            objBEProceso.UsuarioCreacion = sqlDr.GetString(nIndex8);
                            objBEProceso.FechaCreacion = sqlDr.GetDateTime(nIndex9);
                            if (sqlDr.IsDBNull(nIndex10)) { objBEProceso.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEProceso.UsuarioModificacion = sqlDr.GetString(nIndex10); }
                            if (sqlDr.IsDBNull(nIndex11)) { objBEProceso.FechaModificacion = NullTypes.FechaNull; } else { objBEProceso.FechaModificacion = sqlDr.GetDateTime(nIndex11); }
                            objBEProceso.EstadoRegistro = sqlDr.GetBoolean(nIndex12);

                            lstBEProceso.Add(objBEProceso);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBEProceso;
        }

        //ok
        public List<BEProceso> Listar(Int32 pnIdTipProceso, Nullable<Boolean> pbStsRegistro)
        {
            List<BEProceso> lstBEProceso = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAProceso objDAProceso = new DAProceso();
                SqlDataReader sqlDr = objDAProceso.Listar(pnIdTipProceso, pbStsRegistro, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idProceso");
                        Int32 nIndex2 = sqlDr.GetOrdinal("desTipProceso");
                        Int32 nIndex3 = sqlDr.GetOrdinal("archivo");
                        Int32 nIndex4 = sqlDr.GetOrdinal("procesado");
                        Int32 nIndex5 = sqlDr.GetOrdinal("archivoLog");
                        Int32 nIndex6 = sqlDr.GetOrdinal("fecProceso");
                        Int32 nIndex7 = sqlDr.GetOrdinal("idTipProceso");
                        Int32 nIndex8 = sqlDr.GetOrdinal("regTotal");
                        Int32 nIndex9 = sqlDr.GetOrdinal("regTotalVal");
                        Int32 nIndex10 = sqlDr.GetOrdinal("regTotalErr");
                        Int32 nIndex11 = sqlDr.GetOrdinal("nroObservaciones");
                        Int32 nIndex12 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex13 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex14 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex15 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex16 = sqlDr.GetOrdinal("stsRegistro");
                        Int32 nIndex17 = sqlDr.GetOrdinal("rutaArchivo");
                        Int32 nIndex18 = sqlDr.GetOrdinal("rutaArchivoLog");
                        Int32 nIndex19 = sqlDr.GetOrdinal("generaExport");
                        Int32 nIndex20 = sqlDr.GetOrdinal("spExport");
                        Int32 nIndex21 = sqlDr.GetOrdinal("rutaArchivoExport");
                        Int32 nIndex22 = sqlDr.GetOrdinal("rutaArchivoLogCotizacion");

                        lstBEProceso = new List<BEProceso>();

                        while (sqlDr.Read())
                        {
                            BEProceso objBEProceso = new BEProceso();

                            objBEProceso.IdProceso = sqlDr.GetInt32(nIndex1);
                            objBEProceso.DesTipProceso = sqlDr.GetString(nIndex2);
                            objBEProceso.Archivo = sqlDr.GetString(nIndex3);
                            objBEProceso.Procesado = sqlDr.GetBoolean(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBEProceso.ArchivoLog = NullTypes.CadenaNull; } else { objBEProceso.ArchivoLog = sqlDr.GetString(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBEProceso.FecProceso = NullTypes.FechaNull; } else { objBEProceso.FecProceso = sqlDr.GetDateTime(nIndex6); }
                            objBEProceso.IdTipProceso = sqlDr.GetInt32(nIndex7);
                            if (sqlDr.IsDBNull(nIndex8)) { objBEProceso.RegTotal = NullTypes.IntegerNull; } else { objBEProceso.RegTotal = sqlDr.GetInt32(nIndex8); }
                            if (sqlDr.IsDBNull(nIndex9)) { objBEProceso.RegTotalVal = NullTypes.IntegerNull; } else { objBEProceso.RegTotalVal = sqlDr.GetInt32(nIndex9); }
                            if (sqlDr.IsDBNull(nIndex10)) { objBEProceso.RegTotalErr = NullTypes.IntegerNull; } else { objBEProceso.RegTotalErr = sqlDr.GetInt32(nIndex10); }
                            if (sqlDr.IsDBNull(nIndex11)) { objBEProceso.NroObservaciones = NullTypes.IntegerNull; } else { objBEProceso.NroObservaciones = sqlDr.GetInt32(nIndex11); }
                            objBEProceso.UsuarioCreacion = sqlDr.GetString(nIndex12);
                            objBEProceso.FechaCreacion = sqlDr.GetDateTime(nIndex13);
                            if (sqlDr.IsDBNull(nIndex14)) { objBEProceso.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEProceso.UsuarioModificacion = sqlDr.GetString(nIndex14); }
                            if (sqlDr.IsDBNull(nIndex15)) { objBEProceso.FechaModificacion = NullTypes.FechaNull; } else { objBEProceso.FechaModificacion = sqlDr.GetDateTime(nIndex15); }
                            objBEProceso.EstadoRegistro = sqlDr.GetBoolean(nIndex16);
                            if (sqlDr.IsDBNull(nIndex17)) { objBEProceso.RutaArchivo = NullTypes.CadenaNull; } else { objBEProceso.RutaArchivo = sqlDr.GetString(nIndex17); }
                            if (sqlDr.IsDBNull(nIndex18)) { objBEProceso.RutaArchivoLog = NullTypes.CadenaNull; } else { objBEProceso.RutaArchivoLog = sqlDr.GetString(nIndex18); }
                            if (sqlDr.IsDBNull(nIndex19)) { objBEProceso.GeneraExport = false; } else { objBEProceso.GeneraExport = sqlDr.GetBoolean(nIndex19); }
                            if (sqlDr.IsDBNull(nIndex20)) { objBEProceso.SpExport = NullTypes.CadenaNull; } else { objBEProceso.SpExport = sqlDr.GetString(nIndex20); }
                            if (sqlDr.IsDBNull(nIndex21)) { objBEProceso.RutaArchivoExport = NullTypes.CadenaNull; } else { objBEProceso.RutaArchivoExport = sqlDr.GetString(nIndex21); }
                            if (sqlDr.IsDBNull(nIndex22)) { objBEProceso.RutaArchivoLogCotizacion = NullTypes.CadenaNull; } else { objBEProceso.RutaArchivoLogCotizacion = sqlDr.GetString(nIndex22); }
                            lstBEProceso.Add(objBEProceso);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBEProceso;
        }

        //ok
        public List<String> GenerarTramaExport(Int32 pnIdProceso, String pcProcedure)
        {
            List<String> lstCTrama = new List<String>();

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAProceso objDAProceso = new DAProceso();
                SqlDataReader sqlDr = objDAProceso.GenerarTramaExport(pnIdProceso, pcProcedure, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex = sqlDr.GetOrdinal("Trama");

                        while (sqlDr.Read())
                        {
                            String cTrama = sqlDr.GetString(nIndex);
                            lstCTrama.Add(cTrama);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstCTrama;
        }

        public BEProceso Obtener(Int32 pnIdProceso) 
        {
            BEProceso objBEProceso = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAProceso objDAProceso = new DAProceso();
                SqlDataReader sqlDr = objDAProceso.Obtener(pnIdProceso, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idProceso");
                        Int32 nIndex2 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("archivo");
                        Int32 nIndex4 = sqlDr.GetOrdinal("procesado");
                        Int32 nIndex5 = sqlDr.GetOrdinal("archivoLog");
                        Int32 nIndex6 = sqlDr.GetOrdinal("fecProceso");
                        Int32 nIndex7 = sqlDr.GetOrdinal("idTipProceso");
                        Int32 nIndex8 = sqlDr.GetOrdinal("idProducto"); 

                        objBEProceso = new BEProceso();

                        if (sqlDr.Read()) 
                        {
                            objBEProceso.IdProceso = sqlDr.GetInt32(nIndex1);
                            objBEProceso.Descripcion = sqlDr.GetString(nIndex2);
                            objBEProceso.Archivo = sqlDr.GetString(nIndex3);
                            if (sqlDr.IsDBNull(nIndex4)) { objBEProceso.Procesado = false; } else { objBEProceso.Procesado = sqlDr.GetBoolean(nIndex4); }
                            if (sqlDr.IsDBNull(nIndex5)) { objBEProceso.ArchivoLog = NullTypes.CadenaNull; } else { objBEProceso.ArchivoLog = sqlDr.GetString(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBEProceso.FecProceso = NullTypes.FechaNull; } else { objBEProceso.FecProceso = sqlDr.GetDateTime(nIndex6); }
                            if (sqlDr.IsDBNull(nIndex7)) { objBEProceso.IdTipProceso = NullTypes.IntegerNull; } else { objBEProceso.IdTipProceso = sqlDr.GetInt32(nIndex7); }
                            if (sqlDr.IsDBNull(nIndex8)) { objBEProceso.IdProducto = NullTypes.IntegerNull; } else { objBEProceso.IdProducto = sqlDr.GetInt32(nIndex8); }
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return objBEProceso;
        }
        #endregion

        #region Util
        //ok
        private void EscribirErrores(List<String> pLstLog, String pcRuta, String pcNombreArchivo)
        {
            StreamWriter swLog = new StreamWriter(pcRuta + pcNombreArchivo);

            if ((pLstLog != null) && (pLstLog.Count != 0))
            {
                foreach (String cError in pLstLog)
                {
                    swLog.WriteLine(cError);
                }
            }
            else
                swLog.WriteLine("No se presentaron inconsistencias");

            swLog.Close();
        }

        //ok
        public void CrearArchivoExport(Int32 pnIdProceso, String pcProcedure, String pcRuta, String pcArchivo)
        {
            List<String> lstCTrama = this.GenerarTramaExport(pnIdProceso, pcProcedure);

            if (lstCTrama != null)
            {
                StreamWriter sw = new StreamWriter(pcRuta + @"/" + pcArchivo);
                StringBuilder cadena = null;

                foreach (String cTrama in lstCTrama)
                {
                    cadena = new StringBuilder();
                    cadena.Append(cTrama);
                    sw.WriteLine(cadena.ToString());
                }
                sw.Close();
            }
        }
        #endregion
    }
}
