﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Resources;
using AONAffinity.Business.Entity.BDMotor;
using AONAffinity.Data.DataAccess; 
using AONAffinity.Data.DataAccess.BDMotor;   

namespace AONAffinity.Business.Logic
{
    public class BLAnio
    {
        #region Transaccional
        #endregion

        #region NoTransaccional
        /// <summary>
        /// Permite listar los años de los vehículos.
        /// </summary>
        /// <param name="pbStsRegistro"></param>
        /// <returns></returns>
        public List<BEAnio> Listar(Nullable<Boolean> pbStsRegistro) 
        {
            List<BEAnio> lstBEAnio = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAAnio objDAAnio = new DAAnio();
                SqlDataReader sqlDr = objDAAnio.Listar(pbStsRegistro, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idAnio");
                        Int32 nIndex2 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex4 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex5 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex6 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("stsRegistro");

                        lstBEAnio = new List<BEAnio>();

                        while (sqlDr.Read()) 
                        {
                            BEAnio objBEAnio = new BEAnio();

                            objBEAnio.IdAnio = sqlDr.GetInt32(nIndex1);
                            objBEAnio.Descripcion = sqlDr.GetString(nIndex2);
                            objBEAnio.UsuarioCreacion = sqlDr.GetString(nIndex3);
                            objBEAnio.FechaCreacion = sqlDr.GetDateTime(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBEAnio.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEAnio.UsuarioModificacion = sqlDr.GetString(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBEAnio.FechaModificacion = NullTypes.FechaNull; } else { objBEAnio.FechaModificacion = sqlDr.GetDateTime(nIndex6); }
                            objBEAnio.EstadoRegistro = sqlDr.GetBoolean(nIndex7);

                            lstBEAnio.Add(objBEAnio);  
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return lstBEAnio;
        }
      
        #endregion
    }
}
