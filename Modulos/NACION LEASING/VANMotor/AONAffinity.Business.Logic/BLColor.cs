﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Resources;
using AONAffinity.Business.Entity.BDMotor;
using AONAffinity.Data.DataAccess;
using AONAffinity.Data.DataAccess.BDMotor;  

namespace AONAffinity.Business.Logic
{
    /// <summary>
    /// Clase de lógica del negocio de la tabla Color.
    /// </summary>
    public class BLColor
    {
        #region NoTransaccional
        /// <summary>
        /// Permite listar las clases de color.
        /// </summary>
        /// <param name="pbStsRegistro">Estado del registro.</param>        
        /// <returns>Objeto List de tipo BEColor.</returns>
        public List<BEColor> Listar(Nullable<Boolean> pbStsRegistro) 
        {
            /*
            * CREADO POR:              Gary Porras Paraguay
            * FECHA DE CREACION:       2011-06-01
            * MODIFICADO POR:          
            * FECHA DE MODIFICACION:   
            */

            List<BEColor> lstBEColor = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAColor objDAColor = new DAColor();
                SqlDataReader sqlDr = objDAColor.Listar(pbStsRegistro, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idColor");
                        Int32 nIndex2 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex4 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex5 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex6 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("stsRegistro");

                        lstBEColor = new List<BEColor>();

                        while (sqlDr.Read()) 
                        {
                            BEColor objBEColor = new BEColor();

                            objBEColor.IdColor = sqlDr.GetInt32(nIndex1);
                            objBEColor.Descripcion = sqlDr.GetString(nIndex2);
                            objBEColor.UsuarioCreacion = sqlDr.GetString(nIndex3);
                            objBEColor.FechaCreacion = sqlDr.GetDateTime(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBEColor.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEColor.UsuarioModificacion = sqlDr.GetString(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBEColor.FechaModificacion = NullTypes.FechaNull; } else { objBEColor.FechaModificacion = sqlDr.GetDateTime(nIndex6); }
                            objBEColor.EstadoRegistro = sqlDr.GetBoolean(nIndex7);

                            lstBEColor.Add(objBEColor);   
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return lstBEColor;
        }  
        #endregion
    }
}
