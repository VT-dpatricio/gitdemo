﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Motor.Resources;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;  

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLAsegurador
    {
        public List<BEAsegurador> Listar(Boolean pbEstado)
        {
            List<BEAsegurador> lstBEAsegurador = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAAsegurador objDAAsegurador = new DAAsegurador();
                SqlDataReader sqlDr = objDAAsegurador.Listar(pbEstado, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("IdAsegurador");
                        Int32 nIndex2 = sqlDr.GetOrdinal("Descripcion");

                        lstBEAsegurador = new List<BEAsegurador>();

                        while (sqlDr.Read()) 
                        {
                            BEAsegurador objBEAsegurador = new BEAsegurador();
                            objBEAsegurador.IdAsegurador = sqlDr.GetInt32(nIndex1);
                            objBEAsegurador.Descripcion = sqlDr.GetString(nIndex2);

                            lstBEAsegurador.Add(objBEAsegurador);  
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return lstBEAsegurador;
        }

        public List<BEAsegurador> ListarxGrupoProducto(Int32 pnIdGrupo, Boolean pbEstado) 
        {
            List<BEAsegurador> lstBEAsegurador = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAAsegurador objDAAsegurador = new DAAsegurador();
                SqlDataReader sqlDr = objDAAsegurador.ListarxGrupoProducto(pnIdGrupo, pbEstado, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("IdAsegurador");
                        Int32 nIndex2 = sqlDr.GetOrdinal("Descripcion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("rutaLogo");

                        lstBEAsegurador = new List<BEAsegurador>();

                        while (sqlDr.Read()) 
                        {
                            BEAsegurador objBEAsegurador = new BEAsegurador();
                            objBEAsegurador.IdAsegurador = sqlDr.GetInt32(nIndex1);
                            objBEAsegurador.Descripcion = sqlDr.GetString(nIndex2);
                            objBEAsegurador.RutaLogo = sqlDr.GetString(nIndex3);
                            lstBEAsegurador.Add(objBEAsegurador);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBEAsegurador;
        }
    }
}
