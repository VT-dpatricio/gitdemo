﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLProducto
    {
        #region NoTransaccional
        public BEProducto Obtener(Int32 pnIdProducto) 
        {
            BEProducto objBEProducto = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAProducto objDAProducto = new DAProducto();
                SqlDataReader sqlDr = objDAProducto.Obtener(pnIdProducto, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idproducto");
                        Int32 nIndex2 = sqlDr.GetOrdinal("Descripcion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("IdAsegurador");
                        Int32 nIndex4 = sqlDr.GetOrdinal("IdSponsor");
                        Int32 nIndex5 = sqlDr.GetOrdinal("maxAntiguedad");
                        Int32 nIndex6 = sqlDr.GetOrdinal("maxvalorvehiculo");
                        Int32 nIndex7 = sqlDr.GetOrdinal("siniestro");
                        Int32 nIndex8 = sqlDr.GetOrdinal("montoSiniestro");
                        Int32 nIndex9 = sqlDr.GetOrdinal("maxCantSiniestro");
                        Int32 nIndex10 = sqlDr.GetOrdinal("timonCambiado");
                        Int32 nIndex11 = sqlDr.GetOrdinal("porcEmision");
                        Int32 nIndex12 = sqlDr.GetOrdinal("cambioValVehiculo");
                        Int32 nIndex13 = sqlDr.GetOrdinal("maxPorcMenos");
                        Int32 nIndex14 = sqlDr.GetOrdinal("maxPorcMas");
                        Int32 nIndex15 = sqlDr.GetOrdinal("minAntiguedad");
                        Int32 nIndex16 = sqlDr.GetOrdinal("maxNroAsientos");

                        if (sqlDr.Read()) 
                        {                           
                            objBEProducto = new BEProducto();
                            objBEProducto.IdProducto = sqlDr.GetInt32(nIndex1);
                            objBEProducto.Descripcion= sqlDr.GetString(nIndex2);
                            objBEProducto.IdAsegurador = sqlDr.GetInt32(nIndex3);
                            objBEProducto.IdSponsor = sqlDr.GetInt32(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBEProducto.MaxAntiguedad = NullTypes.IntegerNull; } else { objBEProducto.MaxAntiguedad = sqlDr.GetInt32(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBEProducto.MaxValorVehiculo = NullTypes.DecimalNull; } else { objBEProducto.MaxValorVehiculo = sqlDr.GetDecimal(nIndex6); }
                            if (sqlDr.IsDBNull(nIndex7)) { objBEProducto.Siniestro = false; } else { objBEProducto.Siniestro = sqlDr.GetBoolean(nIndex7); }
                            if (sqlDr.IsDBNull(nIndex8)) { objBEProducto.MontoSiniestro = NullTypes.DecimalNull; } else { objBEProducto.MontoSiniestro = sqlDr.GetDecimal(nIndex8); }
                            if (sqlDr.IsDBNull(nIndex9)) { objBEProducto.MaxCantSiniestro = NullTypes.IntegerNull; } else { objBEProducto.MaxCantSiniestro = sqlDr.GetInt32(nIndex9); }
                            if (sqlDr.IsDBNull(nIndex10)) { objBEProducto.TimonCambiado = null; } else { objBEProducto.TimonCambiado = sqlDr.GetBoolean(nIndex10); }
                            if (sqlDr.IsDBNull(nIndex11)) { objBEProducto.PorcEmision = NullTypes.DecimalNull; } else { objBEProducto.PorcEmision = sqlDr.GetDecimal(nIndex11); }
                            if (sqlDr.IsDBNull(nIndex12)) { objBEProducto.CambioValVehiculo = false; } else { objBEProducto.CambioValVehiculo = sqlDr.GetBoolean(nIndex12); }
                            if (sqlDr.IsDBNull(nIndex13)) { objBEProducto.MaxPorcMenos = NullTypes.ShortNull; } else { objBEProducto.MaxPorcMenos = sqlDr.GetInt16(nIndex13); }
                            if (sqlDr.IsDBNull(nIndex14)) { objBEProducto.MaxPorcMas = NullTypes.ShortNull; } else { objBEProducto.MaxPorcMas = sqlDr.GetInt16(nIndex14); }
                            if (sqlDr.IsDBNull(nIndex15)) { objBEProducto.MinAntiguedad = NullTypes.IntegerNull; } else { objBEProducto.MinAntiguedad = sqlDr.GetInt32(nIndex15); }
                            if (sqlDr.IsDBNull(nIndex16)) { objBEProducto.MaxNroAsientos = NullTypes.ShortNull; } else { objBEProducto.MaxNroAsientos = sqlDr.GetInt16(nIndex16); }
                        }
                        sqlDr.Close(); 
                    }
                }
            }

            return objBEProducto;
        }

        public List<BEProducto> Listar(Nullable<Boolean> pbEstado) 
        {
            List<BEProducto> lstBEProducto = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAProducto objDAProducto = new DAProducto();
                SqlDataReader sqlDr = objDAProducto.Listar(pbEstado, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex2 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("idAsegurador");
                        Int32 nIndex4 = sqlDr.GetOrdinal("idSponsor");
                        Int32 nIndex5 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex6 = sqlDr.GetOrdinal("UsrCreacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("FecModificacion");
                        Int32 nIndex8 = sqlDr.GetOrdinal("UsrModificacion");
                        Int32 nIndex9 = sqlDr.GetOrdinal("StsRegistro");
                        Int32 nIndex10 = sqlDr.GetOrdinal("maxAntiguedad");
                        Int32 nIndex11 = sqlDr.GetOrdinal("maxValorVehiculo");
                        Int32 nIndex12 = sqlDr.GetOrdinal("siniestro");
                        Int32 nIndex13 = sqlDr.GetOrdinal("montoSiniestro");
                        Int32 nIndex14 = sqlDr.GetOrdinal("maxCantSiniestro");

                        lstBEProducto = new List<BEProducto>();

                        if (sqlDr.Read()) 
                        {
                            BEProducto objBEProducto = new BEProducto();
                            objBEProducto.IdProducto = sqlDr.GetInt32(nIndex1);
                            objBEProducto.Descripcion = sqlDr.GetString(nIndex2);
                            objBEProducto.IdAsegurador = sqlDr.GetInt32(nIndex3);
                            objBEProducto.IdSponsor = sqlDr.GetInt32(nIndex4);
                            objBEProducto.UsuarioCreacion = sqlDr.GetString(nIndex5);
                            objBEProducto.FechaCreacion = sqlDr.GetDateTime(nIndex6);
                            if (sqlDr.IsDBNull(nIndex7)) { objBEProducto.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEProducto.UsuarioModificacion = sqlDr.GetString(nIndex7); }
                            if (sqlDr.IsDBNull(nIndex8)) { objBEProducto.FechaCreacion = NullTypes.FechaNull; } else { objBEProducto.FechaModificacion = sqlDr.GetDateTime(nIndex8); }
                            objBEProducto.EstadoRegistro = sqlDr.GetBoolean(nIndex9);
                            if (sqlDr.IsDBNull(nIndex10)) { objBEProducto.MaxAntiguedad = NullTypes.IntegerNull; } else { objBEProducto.MaxAntiguedad = sqlDr.GetInt32(nIndex10); }
                            if (sqlDr.IsDBNull(nIndex11)) { objBEProducto.MaxValorVehiculo = NullTypes.DecimalNull; } else { objBEProducto.MaxValorVehiculo = sqlDr.GetDecimal(nIndex11); }
                            if (sqlDr.IsDBNull(nIndex12)) { objBEProducto.Siniestro = false; } else { objBEProducto.Siniestro = sqlDr.GetBoolean(nIndex12); }
                            if (sqlDr.IsDBNull(nIndex13)) { objBEProducto.MontoSiniestro = NullTypes.DecimalNull; } else { objBEProducto.MontoSiniestro = sqlDr.GetDecimal(nIndex13); }
                            if (sqlDr.IsDBNull(nIndex14)) { objBEProducto.MaxCantSiniestro = NullTypes.IntegerNull; } else { objBEProducto.MaxCantSiniestro = sqlDr.GetInt32(nIndex14); }

                            lstBEProducto.Add(objBEProducto);  
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return lstBEProducto;
        }

        public List<String> ListarAnios(Int32 pnIdProducto)
        {
            List<String> lstAnios = new List<String>();

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAProducto objDAProducto = new DAProducto();
                SqlDataReader sqlDr = objDAProducto.ListarAnios(pnIdProducto, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("anio");

                        while (sqlDr.Read()) 
                        {
                            Int32 nAnio = sqlDr.GetInt32(nIndex1);
                            lstAnios.Add(nAnio.ToString());                               
                        }

                        sqlDr.Read();  
                    }
                }
            }

            return lstAnios;
        }

        public List<String> ListarNroAsientos(Int16 pnMaxNroAsientos) 
        {
            List<String> lstNroAsientos = null;

            if (pnMaxNroAsientos != NullTypes.ShortNull)
            {
                lstNroAsientos = new List<String>();
                Int16 nIndex = 1;

                while (nIndex <= pnMaxNroAsientos) 
                {                                                                                
                    lstNroAsientos.Add(nIndex.ToString());
                    nIndex++;
                }
            }

            return lstNroAsientos;
        }
        #endregion
    }
}
