﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
//using AONAffinity.Motor.Resources;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.DataAccess; 
using AONAffinity.Motor.BusinessEntity; 

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLProductoEmail
    {
        #region NoTransaccional
        #endregion
        public BEProductoEmail Obtener(Int32 pnIdProducto)
        {
            BEProductoEmail objBEProductoEmail = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAProductoEmail objDAProductoEmail = new DAProductoEmail();
                SqlDataReader sqlDr = objDAProductoEmail.Obtener(pnIdProducto, sqlCn);

                if (sqlDr != null)                                
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex2 = sqlDr.GetOrdinal("emailFrom");
                        Int32 nIndex3 = sqlDr.GetOrdinal("emailCC");
                        Int32 nIndex4 = sqlDr.GetOrdinal("emailCCO");
                        Int32 nIndex5 = sqlDr.GetOrdinal("emailSubject");
                        Int32 nIndex6 = sqlDr.GetOrdinal("emailBody");
                        Int32 nIndex7 = sqlDr.GetOrdinal("emailPws");
                        Int32 nIndex8 = sqlDr.GetOrdinal("emailSMTP");
                        Int32 nIndex9 = sqlDr.GetOrdinal("emailPort");
                        Int32 nIndex10 = sqlDr.GetOrdinal("rutaArchivo");
                        Int32 nIndex11 = sqlDr.GetOrdinal("rutaImgAsegurador");
                        Int32 nIndex12 = sqlDr.GetOrdinal("rutaImgSponsor");
                        Int32 nIndex13 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex14 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex15 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex16 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex17 = sqlDr.GetOrdinal("stsRegistro");

                        objBEProductoEmail = new BEProductoEmail();

                        if (sqlDr.Read()) 
                        {
                            objBEProductoEmail.IdProducto = sqlDr.GetInt32(nIndex1);
                            objBEProductoEmail.EmailFrom = sqlDr.GetString(nIndex2);
                            if (sqlDr.IsDBNull(nIndex3)) { objBEProductoEmail.EmailCC = NullTypes.CadenaNull; } else { objBEProductoEmail.EmailCC = sqlDr.GetString(nIndex3); }
                            if (sqlDr.IsDBNull(nIndex4)) { objBEProductoEmail.EmailCCO = NullTypes.CadenaNull; } else { objBEProductoEmail.EmailCCO = sqlDr.GetString(nIndex4); }
                            objBEProductoEmail.EmailSubject = sqlDr.GetString(nIndex5);
                            objBEProductoEmail.EmailBody = sqlDr.GetString(nIndex6);
                            if (sqlDr.IsDBNull(nIndex7)) { objBEProductoEmail.EmailPws = NullTypes.CadenaNull; } else { objBEProductoEmail.EmailPws = sqlDr.GetString(nIndex7); }                            
                            if (sqlDr.IsDBNull(nIndex8)) { objBEProductoEmail.EmailSMTP = NullTypes.CadenaNull; } else { objBEProductoEmail.EmailSMTP = sqlDr.GetString(nIndex8); } 
                            if (sqlDr.IsDBNull(nIndex9)) { objBEProductoEmail.EmailPort = NullTypes.IntegerNull; } else { objBEProductoEmail.EmailPort = sqlDr.GetInt32(nIndex9); } 
                            if (sqlDr.IsDBNull(nIndex10)) { objBEProductoEmail.RutaArchivo = NullTypes.CadenaNull; } else { objBEProductoEmail.RutaArchivo = sqlDr.GetString(nIndex10); }
                            if (sqlDr.IsDBNull(nIndex11)) { objBEProductoEmail.RutaImgAsegurador = NullTypes.CadenaNull; } else { objBEProductoEmail.RutaImgAsegurador = sqlDr.GetString(nIndex11); }
                            if (sqlDr.IsDBNull(nIndex12)) { objBEProductoEmail.RutaImgSponsor = NullTypes.CadenaNull; } else { objBEProductoEmail.RutaImgSponsor = sqlDr.GetString(nIndex12); }
                            objBEProductoEmail.FechaCreacion = sqlDr.GetDateTime(nIndex13);
                            objBEProductoEmail.UsuarioCreacion = sqlDr.GetString(nIndex14);
                            if (sqlDr.IsDBNull(nIndex15)) { objBEProductoEmail.FechaModificacion = NullTypes.FechaNull; } else { objBEProductoEmail.FechaModificacion = sqlDr.GetDateTime(nIndex15); }
                            if (sqlDr.IsDBNull(nIndex16)) { objBEProductoEmail.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEProductoEmail.UsuarioModificacion = sqlDr.GetString(nIndex16); }
                            objBEProductoEmail.EstadoRegistro = sqlDr.GetBoolean(nIndex17);    
                        }
                        sqlDr.Close();  
                    }
                }
            }

            return objBEProductoEmail;
        }
    }
}
