﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;  
 
//using AONAffinity.Resources;
//using AONAffinity Business.Entity.BDMotor;
//using AONAffinity.Library.BusinessEntity.BDMotor;  
//using AONAffinity.Data.DataAccess.BDMotor;
//using AONAffinity.Resources;
//using AONAffinity.Motor.Resources;

namespace AONAffinity.Motor.BusinessLogic
{
    /// <summary>
    /// Clase de lógica del negocio para perocesar las tramas del cliente.
    /// </summary>
    public class BLTrama 
    {
        #region Variables
        /// <summary>
        /// Tabla donde se insertará la información.
        /// </summary>
        private static String cTbCliente = "Cliente";
        private static String cTbVigenciaSiniestro = "VigenciaSiniestro";

        private static Int32 nCanError = 0;
        private static Int32 nNroProceso = 0;
        private static Int32 nObservaciones = 0;

        private static Int32 nIdTipoProceso = 0;
        /// <summary>
         /// Objeto lista que contiene la información de los errores.
        /// </summary>
        private static List<String> lstCError = null;

        /// <summary>
        /// Objeto que contiene la información del archivo.
        /// </summary>
        private AONAffinity.Business.Entity.BDIntegration.BEArchivo objBEArchivo = null;

        /// <summary>
        /// Objeto que contiene la información de la estructura de la trama.
        /// </summary>
        private AONAffinity.Business.Entity.BDIntegration.BEEstructura objBEEstructura = null;

        /// <summary>
        /// Objeto lista que contiene el detalle de la estructura de la trama.
        /// </summary>
        private static List<AONAffinity.Business.Entity.BDIntegration.BEEstructuraDetalle> lstBEEstructuraDetalle = null;

        /// <summary>
        /// Objeto dadatable que contiene informacion de equivalencia.
        /// </summary>
        private static DataTable dtEquivalencia = null;

        /// <summary>
        /// Variable para almacenar la cantidad de registros procesados.
        /// </summary>
        private Int32 nCantidad = 0; 
       
        /// <summary>
        /// Data table que representa a la tabla cliente.
        /// </summary>
        private DataTable dtTablaCliente = null;

        private DataTable dtTablaVigenciaSiniestro = null;

        private static String cUsario = "";
        #endregion

        #region Metodo Principal
        /// <summary>
        /// Permite procesar la carga de las tramas del cliente.
        /// </summary>
        /// <param name="pObjBEProceso"></param>
        public BEProceso Procesar(BEProceso pObjBEProceso, Int32 pnIdProdTrama, String pcArchivo, String pcArchivoLog, String pcUsuario)
        {                        
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-07-11
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BEProceso objBEProceso = null;
            nNroProceso = pObjBEProceso.IdProceso;
            nIdTipoProceso = pObjBEProceso.IdTipProceso;
            cUsario = pcUsuario;            
   
            this.CargarObjetos(pnIdProdTrama);

            FileStream fsArchivoCli = new FileStream(pcArchivo, FileMode.Open);
            Int32 nFila = 0;
            using (StreamReader stream = new StreamReader(fsArchivoCli, Funciones.GetFileEncoding(pcArchivo, fsArchivoCli)))
            {
                String cTrama;                
                while (((cTrama = stream.ReadLine()) != null))
                {
                    this.ValidarTrama(cTrama, nFila);
                    nFila++;
                    fsArchivoCli.Flush();                                         
                }

                fsArchivoCli.Close();
            }

            String cResultado = this.CargarData();
            if (cResultado != String.Empty) 
            {
                this.AgregarError(-1, "Error al cargar la data del cliente"); 
            }
            this.EscribirErrores(AppSettings.DirectorioTramaCliLog, pcArchivoLog);

            objBEProceso = pObjBEProceso;
            objBEProceso.RegTotal = nFila;
            objBEProceso.RegTotalErr = nCanError;
            objBEProceso.RegTotalVal = nFila - nCanError;
            objBEProceso.NroObservaciones = nObservaciones; 

            nFila = 0;
            nCanError = 0;

            return objBEProceso;
        }       
        #endregion

        #region Metodos Privados
        /// <summary>
        /// Permite cargar los objetos de la clase.
        /// </summary>
        /// <param name="pnIdProducto">Código de producto.</param>
        private void CargarObjetos(Int32 pnIdProducto)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-07-11
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            AONAffinity.Motor.BusinessLogic.BDIntegration.BLProducto objBLProducto = new AONAffinity.Motor.BusinessLogic.BDIntegration.BLProducto();
            AONAffinity.Business.Entity.BDIntegration.BEProducto objBEProducto = objBLProducto.Obtener(pnIdProducto);

            AONAffinity.Motor.BusinessLogic.BDIntegration.BLArchivo objBLArchivo = new AONAffinity.Motor.BusinessLogic.BDIntegration.BLArchivo();
            objBEArchivo = objBLArchivo.Obtener(objBEProducto.IdProducto, String.Empty + "%");

            AONAffinity.Motor.BusinessLogic.BDIntegration.BLEstructura objBLEstructura = new AONAffinity.Motor.BusinessLogic.BDIntegration.BLEstructura();
            objBEEstructura = objBLEstructura.Obtener(objBEArchivo.IdArchivo, 4);

            AONAffinity.Motor.BusinessLogic.BDIntegration.BLEstructuraDetalle objBLEstructuraDetalle = new AONAffinity.Motor.BusinessLogic.BDIntegration.BLEstructuraDetalle();
            lstBEEstructuraDetalle = objBLEstructuraDetalle.Obtener(objBEEstructura.IdEstructura);

            AONAffinity.Motor.BusinessLogic.BDIntegration.BLEquivalencia objBLEquivalencia = new AONAffinity.Motor.BusinessLogic.BDIntegration.BLEquivalencia();
            dtEquivalencia = objBLEquivalencia.Obtener(objBEEstructura.IdEstructura); 

            lstCError = new List<String>();
        }

        /// <summary>
        /// Permite validar la estructura de la trama.
        /// </summary>
        /// <param name="pcTrama"></param>
        /// <param name="pnNroFila"></param>
        private void ValidarTrama(String pcTrama, Int32 pnNroFila)   
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-07-11
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            String[] arrCfields = TramaSplit(pcTrama);

            if (nIdTipoProceso == Convert.ToInt32(ValorConstante.TipoProceso.CLIENTE_IBK) || nIdTipoProceso == Convert.ToInt32(ValorConstante.TipoProceso.CLIENTE_BBVA)) 
            {
                if (dtTablaCliente == null)
                {
                    dtTablaCliente = DAContructorTablas.dtTablaCliente();
                }
            }
            else
            {
                if (dtTablaVigenciaSiniestro == null) 
                {
                    dtTablaVigenciaSiniestro = DAContructorTablas.dtTablaVigenciaSiniestro(); 
                }
            }            

            Boolean bValEstructura = this.ValidarEstructura(pcTrama, pnNroFila);
            Boolean bValEstructuraDet = this.ValidarEstructuraDetalle(arrCfields, pnNroFila);

            if (bValEstructura && bValEstructuraDet)
            {
                AgregarTrama(pnNroFila, arrCfields);
            }
        }

        /// <summary>
        /// Permite agregar el error a la lista de errores.
        /// </summary>
        /// <param name="pnNroFila">Nro de fila.</param>
        /// <param name="pcError">Detalle error.</param>
        private void AgregarError(Int32 pnNroFila, String pcError)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-07-11
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            if (lstCError == null)
            {
                lstCError = new List<String>();
            }

            lstCError.Add("Fila No. " + pnNroFila + " -- " + pcError);
        }

        /// <summary>
        /// Permite generar el archivo log.
        /// </summary>
        /// <param name="pcRuta">Ruta donde se generará el archivo log.</param>
        /// <param name="pcNombreArchivo">Nombre del archivo.</param>
        private void EscribirErrores(String pcRuta, String pcNombreArchivo)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-07-11
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            StreamWriter swLog = new StreamWriter(pcNombreArchivo);

            if ((lstCError != null) && (lstCError.Count != 0))
            {
                foreach (String cError in lstCError)
                {
                    swLog.WriteLine(cError);
                }
            }
            else
                swLog.WriteLine("No se presentaron inconsistencias");
            swLog.Close();
            lstCError = null;
            lstCError = new List<String>();
        }

        /// <summary>
        /// Permite agregar la trama.
        /// </summary>
        /// <param name="pnNroFila">Nro. de fila.</param>
        /// <param name="trama">Trama a agregar.</param>
        private void AgregarTrama(Int32 pnNroFila, String[] pcArrtrama)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-07-11
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            this.CargarValoresCliente(pnNroFila, pcArrtrama);
            nCantidad++;
        }

        /// <summary>
        /// Permite cargar los valores en el DataTale.
        /// </summary>
        /// <param name="pnNroFila">Nro de fila.</param>
        /// <param name="pcArrTrama">Valores de trama.</param>
        private void CargarValoresCliente(Int32 pnNroFila, String[] pcArrTrama)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-07-11
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            //Carga trama de clientes.
            if (nIdTipoProceso == Convert.ToInt32(ValorConstante.TipoProceso.CLIENTE_IBK) || nIdTipoProceso == Convert.ToInt32(ValorConstante.TipoProceso.CLIENTE_BBVA)) 
            {
                BLCliente objBLCliente = new BLCliente();

                //Validar si el registro esxiste en la BD.
                if (objBLCliente.Obtener(pcArrTrama[32].Trim()) != null)
                {
                    this.AgregarError(pnNroFila, "El registro ya existe en la BD");
                    nObservaciones++;
                    return;
                }
                //Validar si el resgitro existe en el DataTable.
                else if (this.ValidarRepetido(pcArrTrama[32].Trim()) == false)
                {
                    this.AgregarError(pnNroFila, "El registro se repite en el archivo.");
                    nObservaciones++;
                    return;
                }
                //Si el registro no existe, lo agregamos al DataTable.
                else
                {
                    DataRow dtRow = dtTablaCliente.NewRow();

                    dtRow["nroMotor"] = pcArrTrama[32].Trim();  
                    dtRow["nroCertificado"] = pcArrTrama[0].Trim();
                    dtRow["codAsegurador"] = pcArrTrama[1].Trim();
                    dtRow["apePaterno"] = pcArrTrama[2].Trim();
                    dtRow["apeMaterno"] = pcArrTrama[3].Trim();
                    dtRow["priNombre"] = pcArrTrama[4].Trim();
                    dtRow["segNombre"] = pcArrTrama[5].Trim();
                    dtRow["idTipoDocumento"] = pcArrTrama[6].Trim();
                    dtRow["nroDocumento"] = pcArrTrama[7].Trim();
                    dtRow["sexo"] = pcArrTrama[8].Trim();
                    dtRow["fecNacimiento"] = Format.FormateaFecha(pcArrTrama[9].Trim(), "AAAAMMDD");
                    dtRow["idCiudad"] = pcArrTrama[10].Trim();
                    dtRow["direccion"] = pcArrTrama[11].Trim();
                    dtRow["telDomicilio1"] = pcArrTrama[12].Trim();
                    dtRow["telDomicilio2"] = pcArrTrama[13].Trim();
                    dtRow["telDomicilio3"] = pcArrTrama[14].Trim();
                    dtRow["telMovil1"] = pcArrTrama[15].Trim();
                    dtRow["telMovil2"] = pcArrTrama[16].Trim();
                    dtRow["telMovil3"] = pcArrTrama[17].Trim();
                    dtRow["telOficina1"] = pcArrTrama[18].Trim();
                    dtRow["telOficina2"] = pcArrTrama[19].Trim();
                    dtRow["telOficina3"] = pcArrTrama[20].Trim();  
                    dtRow["email1"] = pcArrTrama[21].Trim();
                    dtRow["email2"] = pcArrTrama[22].Trim();
                    dtRow["fecVigencia"] = Format.FormateaFecha(pcArrTrama[23].Trim(), "AAAAMMDD");
                    dtRow["fecFinVigencia"] = Format.FormateaFecha(pcArrTrama[24].Trim(), "AAAAMMDD");                    
                    dtRow["idMarca"] = pcArrTrama[25].Trim();
                    dtRow["idModelo"] = pcArrTrama[26].Trim();
                    dtRow["nroPlaca"] = pcArrTrama[27].Trim();
                    dtRow["anioFab"] = pcArrTrama[28].Trim();
                    dtRow["valorVehiculo"] = pcArrTrama[29].Trim();
                    dtRow["nroSerie"] = pcArrTrama[30].Trim();
                    dtRow["color"] = pcArrTrama[31].Trim();
                    dtRow["idUsoVehiculo"] = pcArrTrama[33].Trim();                                        
                    dtRow["idClase"] = pcArrTrama[34].Trim();
                    dtRow["nroAsientos"] = pcArrTrama[35].Trim();
                    dtRow["esTimonCambiado"] = Funciones.getBool(pcArrTrama[36].Trim());
                    dtRow["esBlindado"] = Funciones.getBool(pcArrTrama[37].Trim());
                    dtRow["reqGPS"] = Funciones.getBool(pcArrTrama[38].Trim());
                    dtRow["idSponsor"] = pcArrTrama[39].Trim();
                    dtRow["esAsegurable"] = DBNull.Value;
                    dtRow["idProceso"] = nNroProceso;
                    dtRow["idEstadoCliente"] = Convert.ToInt32(ValorConstante.EstadoCliente.NoCotizado);
                    dtRow["usrCreacion"] = cUsario;
                    dtRow["fecCreacion"] = DateTime.Now;
                    dtRow["usrModificacion"] = DBNull.Value;
                    dtRow["fecModificacion"] = DBNull.Value;
                    dtRow["stsRegistro"] = true;

                    dtTablaCliente.Rows.Add(dtRow);
                }  
            }

            //Carga trama de siniestro.
            if (nIdTipoProceso == Convert.ToInt32(ValorConstante.TipoProceso.CLIENTE_SINIESTRO)) 
            {
                BLVigenciaSiniestro objBLVigenciaSiniestro = new BLVigenciaSiniestro();

                if (objBLVigenciaSiniestro.Obtener(pcArrTrama[0].Trim()) != null) 
                {
                    this.AgregarError(pnNroFila, "El registro ya existe en la BD");
                    nObservaciones++;
                    return;
                }
                else if (this.ValidarSiniestroRepetido(pcArrTrama[0].Trim()) == false)
                {
                    this.AgregarError(pnNroFila, "El registro se repite en el archivo.");
                    nObservaciones++;
                    return;
                }
                else 
                {
                    DataRow dtRow = dtTablaVigenciaSiniestro.NewRow();

                    dtRow["nroMotor"] = pcArrTrama[0].Trim();                    
                    if (pcArrTrama[4].Trim() == String.Empty) { dtRow["cantSiniestro1Anio"] = DBNull.Value; } else { dtRow["cantSiniestro1Anio"] = pcArrTrama[4].Trim(); }
                    if (pcArrTrama[5].Trim() == String.Empty) { dtRow["cantSiniestro2Anio"] = DBNull.Value; } else { dtRow["cantSiniestro2Anio"] = pcArrTrama[5].Trim(); }
                    if (pcArrTrama[6].Trim() == String.Empty) { dtRow["cantSiniestro3Anio"] = DBNull.Value; } else { dtRow["cantSiniestro3Anio"] = pcArrTrama[6].Trim(); }
                    if (pcArrTrama[7].Trim() == String.Empty) { dtRow["cantSiniestro4Anio"] = DBNull.Value; } else { dtRow["cantSiniestro4Anio"] = pcArrTrama[7].Trim(); }
                    if (pcArrTrama[8].Trim() == String.Empty) { dtRow["cantSiniestro5Anio"] = DBNull.Value; } else { dtRow["cantSiniestro5Anio"] = pcArrTrama[8].Trim(); }
                    if (pcArrTrama[9].Trim() == String.Empty) { dtRow["montoSiniestro1Anio"] = DBNull.Value; } else { dtRow["montoSiniestro1Anio"] = pcArrTrama[9].Trim(); }
                    if (pcArrTrama[10].Trim() == String.Empty) { dtRow["montoSiniestro2Anio"] = DBNull.Value; } else { dtRow["montoSiniestro2Anio"] = pcArrTrama[10].Trim(); }
                    if (pcArrTrama[11].Trim() == String.Empty) { dtRow["montoSiniestro3Anio"] = DBNull.Value; } else { dtRow["montoSiniestro3Anio"] = pcArrTrama[11].Trim(); }
                    if (pcArrTrama[12].Trim() == String.Empty) { dtRow["montoSiniestro4Anio"] = DBNull.Value; } else { dtRow["montoSiniestro4Anio"] = pcArrTrama[12].Trim(); }
                    if (pcArrTrama[13].Trim() == String.Empty) { dtRow["montoSiniestro5Anio"] = DBNull.Value; } else { dtRow["montoSiniestro5Anio"] = pcArrTrama[13].Trim(); }
                    dtRow["fecIniVigencia"] = Funciones.getDate(pcArrTrama[2].Trim(), "AAAAMMDD");
                    dtRow["fecFinVigencia"] = Funciones.getDate(pcArrTrama[3].Trim(), "AAAAMMDD");
                    dtRow["usrCreacion"] = cUsario;
                    dtRow["fecCreacion"] = DateTime.Now;
                    dtRow["usrModificacion"] = DBNull.Value;
                    dtRow["fecModificacion"] = DBNull.Value;
                    dtRow["stsRegistro"] = true;

                    dtTablaVigenciaSiniestro.Rows.Add(dtRow);  
                }
            }
        }

        private Boolean ValidaClienteRepetidos(DataTable pDtCliente) 
        {
            Boolean bResult = true;

            if (pDtCliente != null) 
            {

            }

            return bResult;
        }

        /// <summary>
        /// Permite cargar la información del cliente mediante el SqlBulkCopy.       
        /// </summary>
        /// <param name="pSqlCn">Objeto conexion a la BD.</param>
        /// <param name="pSqlTrs">Objeto transacción</param>
        private void CargarDataCliente(SqlConnection pSqlCn, SqlTransaction pSqlTrs)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-07-11
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            using (SqlBulkCopy sqlBC = new SqlBulkCopy(pSqlCn, SqlBulkCopyOptions.TableLock, pSqlTrs))
            {
                if (nIdTipoProceso == Convert.ToInt32(ValorConstante.TipoProceso.CLIENTE_IBK) || nIdTipoProceso == Convert.ToInt32(ValorConstante.TipoProceso.CLIENTE_BBVA))
                {
                    sqlBC.ColumnMappings.Add(0, 0);
                    sqlBC.ColumnMappings.Add(1, 1);
                    sqlBC.ColumnMappings.Add(2, 2);
                    sqlBC.ColumnMappings.Add(3, 3);
                    sqlBC.ColumnMappings.Add(4, 4);
                    sqlBC.ColumnMappings.Add(5, 5);
                    sqlBC.ColumnMappings.Add(6, 6);
                    sqlBC.ColumnMappings.Add(7, 7);
                    sqlBC.ColumnMappings.Add(8, 8);
                    sqlBC.ColumnMappings.Add(9, 9);
                    sqlBC.ColumnMappings.Add(10, 10);
                    sqlBC.ColumnMappings.Add(11, 11);
                    sqlBC.ColumnMappings.Add(12, 12);
                    sqlBC.ColumnMappings.Add(13, 13);
                    sqlBC.ColumnMappings.Add(14, 14);
                    sqlBC.ColumnMappings.Add(15, 15);
                    sqlBC.ColumnMappings.Add(16, 16);
                    sqlBC.ColumnMappings.Add(17, 17);
                    sqlBC.ColumnMappings.Add(18, 18);
                    sqlBC.ColumnMappings.Add(19, 19);
                    sqlBC.ColumnMappings.Add(20, 20);
                    sqlBC.ColumnMappings.Add(21, 21);
                    sqlBC.ColumnMappings.Add(22, 22);
                    sqlBC.ColumnMappings.Add(23, 23);
                    sqlBC.ColumnMappings.Add(24, 24);
                    sqlBC.ColumnMappings.Add(25, 25);
                    sqlBC.ColumnMappings.Add(26, 26);
                    sqlBC.ColumnMappings.Add(27, 27);
                    sqlBC.ColumnMappings.Add(28, 28);
                    sqlBC.ColumnMappings.Add(29, 29);
                    sqlBC.ColumnMappings.Add(30, 30);
                    sqlBC.ColumnMappings.Add(31, 31);
                    sqlBC.ColumnMappings.Add(32, 32);
                    sqlBC.ColumnMappings.Add(33, 33);
                    sqlBC.ColumnMappings.Add(34, 34);
                    sqlBC.ColumnMappings.Add(35, 35);
                    sqlBC.ColumnMappings.Add(36, 36);
                    sqlBC.ColumnMappings.Add(37, 37);
                    sqlBC.ColumnMappings.Add(38, 38);
                    sqlBC.ColumnMappings.Add(39, 39);
                    sqlBC.ColumnMappings.Add(40, 40);
                    sqlBC.ColumnMappings.Add(41, 41);
                    sqlBC.ColumnMappings.Add(42, 42);
                    sqlBC.ColumnMappings.Add(43, 43);
                    sqlBC.ColumnMappings.Add(44, 44);
                    sqlBC.ColumnMappings.Add(45, 45);
                    sqlBC.ColumnMappings.Add(46, 46);
                    sqlBC.ColumnMappings.Add(47, 47);

                    sqlBC.DestinationTableName = cTbCliente;
                    sqlBC.BulkCopyTimeout = 120;
                    sqlBC.WriteToServer(dtTablaCliente);
                }
                else 
                {
                    sqlBC.ColumnMappings.Add(0, 0);
                    sqlBC.ColumnMappings.Add(1, 1);
                    sqlBC.ColumnMappings.Add(2, 2);
                    sqlBC.ColumnMappings.Add(3, 3);
                    sqlBC.ColumnMappings.Add(4, 4);
                    sqlBC.ColumnMappings.Add(5, 5);
                    sqlBC.ColumnMappings.Add(6, 6);
                    sqlBC.ColumnMappings.Add(7, 7);
                    sqlBC.ColumnMappings.Add(8, 8);
                    sqlBC.ColumnMappings.Add(9, 9);
                    sqlBC.ColumnMappings.Add(10, 10);
                    sqlBC.ColumnMappings.Add(11, 11);
                    sqlBC.ColumnMappings.Add(12, 12);
                    sqlBC.ColumnMappings.Add(13, 13);
                    sqlBC.ColumnMappings.Add(14, 14);
                    sqlBC.ColumnMappings.Add(15, 15);
                    sqlBC.ColumnMappings.Add(16, 16);
                    sqlBC.ColumnMappings.Add(17, 17);

                    sqlBC.DestinationTableName = cTbVigenciaSiniestro;
                    sqlBC.BulkCopyTimeout = 60;
                    sqlBC.WriteToServer(dtTablaVigenciaSiniestro);
                }
            }
        }
        #endregion

        #region Funciones Privadas
        /// <summary>
        /// Permite validar la estructura de la trama.
        /// </summary>
        /// <param name="pcTrama">Trama a validar.</param>
        /// <param name="pnFila">Nro de fila.</param>
        /// <returns>Objeto de tipo Boolean.</returns>
        private Boolean ValidarEstructura(String pcTrama, Int32 pnFila)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-07-11
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            Boolean bResult = true;
            
            if (objBEEstructura.Longitud != pcTrama.Length)
            {
                this.AgregarError(pnFila, "La longitud de la trama es diferente.");                
                bResult = false;
                nCanError = nCanError + 1;
            }
            return bResult;
        }

        /// <summary>
        /// Permite validar el detalle de la estructura de la trama.
        /// </summary>
        /// <param name="pcArrTrama">Trama a validar.</param>
        /// <param name="pnFila">Nro de fila.</param>
        /// <returns>Objeto de tipo Boolean.</returns>
        private Boolean ValidarEstructuraDetalle(String[] pcArrTrama, Int32 pnFila)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-07-11
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            Boolean bResult = true; 
            Int32 nColumna = 0;
            Int32 nColumnaReal = 1;
            
            if (nColumna < lstBEEstructuraDetalle.Count)
            {
                foreach (AONAffinity.Business.Entity.BDIntegration.BEEstructuraDetalle objBEEstructuraDetalle in lstBEEstructuraDetalle)
                {
                    String campo = pcArrTrama[nColumna];
                    if (objBEEstructuraDetalle.Longitud < campo.Trim().Length)
                    {
                        this.AgregarError(pnFila, "La longitud de la columna[" + nColumnaReal.ToString() + "] es diferente.");                                                
                        bResult = false;
                    }
                    if (!campo.Trim().Equals(""))
                    {
                        if (!ValidarFormato(objBEEstructuraDetalle, campo, pnFila, nColumnaReal))
                            bResult = false;
                    }
                    else
                    {
                        if (objBEEstructuraDetalle.Obligatorio == 1)
                        {
                            this.AgregarError(pnFila, "El dato de la columna[" + nColumnaReal.ToString() + "] es obligatorio.");                                                        
                            bResult = false;
                        }
                    }                   
                    nColumna++;
                    nColumnaReal++;
                }
            }
            else
            {
                this.AgregarError(pnFila, "La Cantidad de columnas es diferente de las configuradas en el sistema.");                
                bResult = false;
            }

            if (!bResult)
            {
                nCanError = nCanError + 1;
            }

            return bResult;
        }

        /// <summary>
        /// Permite validar el formato del valor del campo.
        /// </summary>
        /// <param name="pObjBEEstructuraDetalle">Objeto de tipo BEEstructuraDetalle.</param>
        /// <param name="pcCampo">Campo a validar.</param>
        /// <param name="pnFila">Nro. de fila.</param>
        /// <param name="pnColumna">Columna a validar.</param>
        /// <returns>Objeto de tipo Boolean.</returns>
        private Boolean ValidarFormato(AONAffinity.Business.Entity.BDIntegration.BEEstructuraDetalle pObjBEEstructuraDetalle, String pcCampo, Int32 pnFila, Int32 pnColumna)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-07-11
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            Boolean bResult = true;
            
            if (pObjBEEstructuraDetalle.Nombretd.Equals("INT"))
            {
                Int32 nIntValTrama = 0;

                if (int.TryParse(pcCampo, out nIntValTrama))
                {
                    if ((pObjBEEstructuraDetalle.ValMax != -1) && (pObjBEEstructuraDetalle.ValMin != -1))
                    {
                        if (!((pObjBEEstructuraDetalle.ValMax >= nIntValTrama) && (nIntValTrama >= pObjBEEstructuraDetalle.ValMin)))
                        {
                            this.AgregarError(pnFila, "El dato de la columna[" + pnColumna.ToString() + "] no coincide con los máximos y mínimos permitidos.");                           
                            bResult = false;
                        }
                        else
                        {
                            if (pObjBEEstructuraDetalle.Equivalencia == 1)
                            {
                                if (!this.ValidarEquivalencia(pObjBEEstructuraDetalle.IdEstructuraDetalle, pcCampo, pnFila, pnColumna))
                                    bResult = false;
                            }
                        }
                    }
                    else
                    {
                        if (pObjBEEstructuraDetalle.Equivalencia == 1)
                        {
                            if (!this.ValidarEquivalencia(pObjBEEstructuraDetalle.IdEstructuraDetalle, pcCampo, pnFila, pnColumna))
                                bResult = false;
                        }
                    }
                }
                else
                {
                    this.AgregarError(pnFila, "El tipo de dato de la columna[" + pnColumna.ToString() + "] es incorrecto.");                                        
                    bResult = false;
                }
            }
            else if (pObjBEEstructuraDetalle.Nombretd.Equals("DECIMAL"))
            {
                float nFloatValTrama = 0;

                if (!float.TryParse(pcCampo, out nFloatValTrama))
                {
                    this.AgregarError(pnFila, "El tipo de dato de la columna[" + pnColumna.ToString() + "] es incorrecto.");                                        
                    bResult = false;
                }
                else
                {
                    if (pObjBEEstructuraDetalle.Equivalencia == 1)
                    {
                        if (!this.ValidarEquivalencia(pObjBEEstructuraDetalle.IdEstructuraDetalle, pcCampo, pnFila, pnColumna))
                            bResult = false;
                    }
                }
            }
            else if (pObjBEEstructuraDetalle.Nombretd.Equals("DATETIME"))
            {
                Int32 nDateValTrama = 0;

                if (int.TryParse(pcCampo, out nDateValTrama))
                {
                    if (!((pObjBEEstructuraDetalle.ValMax >= nDateValTrama) && (nDateValTrama >= pObjBEEstructuraDetalle.ValMin)))
                    {
                        this.AgregarError(pnFila, "El dato de la columna[" + pnColumna.ToString() + "] no coincide con los máximos y mínimos permitidos.");                        
                        bResult = false;
                    }
                    else
                    {
                        DateTime dToday = DateTime.Now;
                        DateTime dValida = Funciones.getDate(pcCampo, pObjBEEstructuraDetalle.Formato);
                        if (pObjBEEstructuraDetalle.ValoresPermitidos.Equals("<="))
                        {
                            if (!(dValida <= dToday))
                            {
                                this.AgregarError(pnFila, "El dato de la columna[" + pnColumna.ToString() + "] no coincide con el valor permitido.");                               
                                bResult = false;
                            }
                            else
                            {
                                if (pObjBEEstructuraDetalle.Equivalencia == 1)
                                {
                                    if (!this.ValidarEquivalencia(pObjBEEstructuraDetalle.IdEstructuraDetalle, pcCampo, pnFila, pnColumna))
                                        bResult = false;
                                }
                            }
                        }
                        else if (pObjBEEstructuraDetalle.ValoresPermitidos.Equals("<"))
                        {
                            if (!(dValida < dToday))
                            {
                                this.AgregarError(pnFila, "El dato de la columna[" + pnColumna.ToString() + "] no coincide con el valor permitido.");                                
                                bResult = false;
                            }
                            else
                            {
                                if (pObjBEEstructuraDetalle.Equivalencia == 1)
                                {
                                    if (!ValidarEquivalencia(pObjBEEstructuraDetalle.IdEstructuraDetalle, pcCampo, pnFila, pnColumna))
                                        bResult = false;
                                }
                            }
                        }
                        else if (pObjBEEstructuraDetalle.ValoresPermitidos.Equals("="))
                        {
                            if (!(dValida == dToday))
                            {
                                this.AgregarError(pnFila, "El dato de la columna[" + pnColumna.ToString() + "] no coincide con el valor permitido.");                                
                                bResult = false;
                            }
                            else
                            {
                                if (pObjBEEstructuraDetalle.Equivalencia == 1)
                                {
                                    if (!ValidarEquivalencia(pObjBEEstructuraDetalle.IdEstructuraDetalle, pcCampo, pnFila, pnColumna))
                                        bResult = false;
                                }
                            }
                        }
                        else if (pObjBEEstructuraDetalle.ValoresPermitidos.Equals(">="))
                        {
                            if (!(dValida >= dToday))
                            {
                                this.AgregarError(pnFila, "El dato de la columna[" + pnColumna.ToString() + "] no coincide con el valor permitido.");                                
                                bResult = false;
                            }
                            else
                            {
                                if (pObjBEEstructuraDetalle.Equivalencia == 1)
                                {
                                    if (!ValidarEquivalencia(pObjBEEstructuraDetalle.IdEstructuraDetalle, pcCampo, pnFila, pnColumna))
                                        bResult = false;
                                }
                            }
                        }
                        else if (pObjBEEstructuraDetalle.ValoresPermitidos.Equals(">"))
                        {
                            if (!(dValida > dToday))
                            {
                                this.AgregarError(pnFila, "El dato de la columna[" + pnColumna.ToString() + "] no coincide con el valor permitido.");                                
                                bResult = false;
                            }
                            else
                            {
                                if (pObjBEEstructuraDetalle.Equivalencia == 1)
                                {
                                    if (!ValidarEquivalencia(pObjBEEstructuraDetalle.IdEstructuraDetalle, pcCampo, pnFila, pnColumna))
                                        bResult = false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    this.AgregarError(pnFila, "El tipo de dato de la columna[" + pnColumna.ToString() + "] es incorrecto.");                    
                    bResult = false;
                }
            }
            else if (pObjBEEstructuraDetalle.Nombretd.Equals("STRING"))
            {
                if ((Regex.IsMatch(pcCampo, pObjBEEstructuraDetalle.ValoresPermitidos, RegexOptions.ExplicitCapture)) && !(pObjBEEstructuraDetalle.ValoresPermitidos.Equals("")))
                {
                    this.AgregarError(pnFila, "El tipo de dato de la columna[" + pnColumna.ToString() + "] no coincide con el valor permitido.");                    
                    bResult = false;
                }
                else
                {
                    if (pObjBEEstructuraDetalle.Equivalencia == 1)
                    {
                        if (!ValidarEquivalencia(pObjBEEstructuraDetalle.IdEstructuraDetalle, pcCampo, pnFila, pnColumna))
                            bResult = false;
                    }
                }
            }

            return bResult;
        }

        /// <summary>
        /// Permite validar la equivalencia.
        /// </summary>
        /// <param name="pnId">Código</param>
        /// <param name="pcCampo">Campo</param>
        /// <param name="pnFila">Nro. fila.</param>
        /// <param name="pnColumna">Columna.</param>
        /// <returns>Objeto de tipo Boolean.</returns>
        private Boolean ValidarEquivalencia(Int32 pnId, String pcCampo, Int32 pnFila, Int32 pnColumna)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-07-11
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            Boolean bResult = false;
            Int32 nValCampo = -1;
            String cCampo = pcCampo;

            if (int.TryParse(pcCampo, out nValCampo))
            {
                cCampo = nValCampo.ToString();
            }

            IEnumerable<DataRow> query =
                from equiv in dtEquivalencia.AsEnumerable()
                where equiv.Field<int>("PosicionInicial") == pnColumna &&
                    equiv.Field<String>("ValorTrama") == cCampo
                select equiv;

            if (query.Count() > 0)
            {
                bResult = true;
            }

            if (!bResult)
            {
                this.AgregarError(pnFila, "El dato de la columna[" + pnColumna.ToString() + "] no coincide con ninguna equivalencia.");
                bResult = false;
            }
            return bResult;
        }

        /// <summary>
        /// Permite validar si existe campo repetido en trama.
        /// </summary>
        /// <param name="pcNroCertificado">nroCertificado.</param>
        /// <param name="pcTrama">Valor de trama.</param>
        /// <returns>Objeto de tipo Boolean.</returns>
        private Boolean ValidarRepetido(String pcNroCertificado, Int32 pcTrama)
        {
            Boolean bResult = true;

            if (dtTablaCliente != null)
            {
                foreach (DataRow dtRow in dtTablaCliente.Rows)
                {
                    if (dtRow["nroCertificado"].ToString() == pcNroCertificado && dtRow["codAsegurador"].ToString() == pcTrama.ToString())
                    {
                        bResult = false;
                        return bResult;
                    }
                }
            }

            return bResult;
        }


        private Boolean ValidarRepetido(String pcNroMotor)
        {            
            Boolean bResult = true;

            if (dtTablaCliente != null)
            {
                foreach (DataRow dtRow in dtTablaCliente.Rows)
                {
                    if (dtRow["nroMotor"].ToString() == pcNroMotor)
                    {
                        return false;
                    }
                }
            }

            return bResult;
        }

        private Boolean ValidarSiniestroRepetido(String pcNroMotor) 
        {
            Boolean bResult = true;

            if (dtTablaVigenciaSiniestro != null)
            {
                foreach (DataRow dtRow in dtTablaVigenciaSiniestro.Rows) 
                {
                    if (dtRow["nroMotor"].ToString().Trim() == pcNroMotor.Trim()) 
                    {
                        bResult = false;
                        return bResult;
                    }
                }
            }

            return bResult;
        }

        /// <summary>
        /// Permite cargar la data de las tramas en la tabla Cliente.
        /// </summary>
        /// <returns>Objeto de tipo String.</returns>
        private String CargarData()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-07-11
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            String cError = String.Empty;

            using (SqlConnection sqlCn = new SqlConnection(AONAffinity.Data.DataAccess.DAConexion.ConexionBDMotor()))
            {
                SqlTransaction sqlTrs = null;
                try
                {
                    sqlCn.Open();
                    sqlTrs = sqlCn.BeginTransaction();

                    this.CargarDataCliente(sqlCn, sqlTrs);
                    sqlTrs.Commit();
                }
                catch (Exception ex)
                {
                    cError = ex.Message;

                    if (sqlTrs != null)
                        sqlTrs.Rollback();
                }
            }
            return cError;
        }

        /// <summary>
        /// Permite generar split.
        /// </summary>
        /// <param name="pcTrama">Trana ajecutar.</param>
        /// <returns>Objeto de tipo String</returns>
        private String[] TramaSplit(String pcTrama)
        {
            String[] cTrama = null;
            Int32 nColumna = 0;
            Int32 nPosicion = 0;
            AONAffinity.Motor.BusinessLogic.BDIntegration.BLEstructuraDetalle objBLEstructuraDetalle = new AONAffinity.Motor.BusinessLogic.BDIntegration.BLEstructuraDetalle();

            //foreach (AONAffinity.Business.Entity.BDIntegration.BEEstructura objBEEstructura in lstBEEstructura)
            //{
                List<AONAffinity.Business.Entity.BDIntegration.BEEstructuraDetalle> lstBEEstructuraDetalle = objBLEstructuraDetalle.Obtener(objBEEstructura.IdEstructura);    // AdmTrama.BLL.BLEstructuraDetalle.ObtenerEstructuraDetallePorEstructura(objBEEstructura.IdEstructura); //O_O
                
                if (nColumna < lstBEEstructuraDetalle.Count)
                {
                    cTrama = new String[lstBEEstructuraDetalle.Count];

                    foreach (AONAffinity.Business.Entity.BDIntegration.BEEstructuraDetalle objBEEstructuraDetalle in lstBEEstructuraDetalle)
                    {
                        cTrama[nColumna] = pcTrama.Substring(nPosicion, objBEEstructuraDetalle.Longitud);
                        nPosicion = nPosicion + objBEEstructuraDetalle.Longitud;
                        nColumna++;
                    }
                }
            //}

            return cTrama;
        }
        #endregion
    }
}