﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;      

//using AONAffinity.Library.DataAccess; 
//using AONAffinity.Library.DataAccess.BDMotor;
//using AONAffinity.Library.BusinessEntity.BDMotor;

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLMenu
    {
        public List<BEMenu> Listar(Int32 pIDSistema,Int32 pIDEntidad, String pcIdUsuario) 
        {
            List<BEMenu> lstBEMenu = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus())) 
            {
                sqlCn.Open();

                DAMenu objDAMenu = new DAMenu();
                SqlDataReader sqlDr = objDAMenu.Listar(pIDSistema,pIDEntidad,pcIdUsuario, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("IDMenu");
                        Int32 nIndex2 = sqlDr.GetOrdinal("Descripcion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("IDMenuPadre");
                        Int32 nIndex4 = sqlDr.GetOrdinal("Url");

                        lstBEMenu = new List<BEMenu>();

                        while (sqlDr.Read()) 
                        {
                            BEMenu objBEMenu = new BEMenu();
                            objBEMenu.IdMenu = sqlDr.GetInt32(nIndex1);
                            objBEMenu.Nombre = sqlDr.GetString(nIndex2);
                            if (sqlDr.IsDBNull(nIndex3)) { objBEMenu.NodPadre = NullTypes.IntegerNull; } else { objBEMenu.NodPadre = sqlDr.GetInt32(nIndex3); }
                            if (sqlDr.IsDBNull(nIndex4)) { objBEMenu.Ruta = NullTypes.CadenaNull; } else { objBEMenu.Ruta = sqlDr.GetString(nIndex4); }
                            
                            lstBEMenu.Add(objBEMenu); 
                        }
                    } 
                }                
            }

            return lstBEMenu;
        }
    }
}
