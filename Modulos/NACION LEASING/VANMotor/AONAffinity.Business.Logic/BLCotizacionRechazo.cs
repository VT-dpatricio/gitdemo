﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Resources;
using AONAffinity.Business.Entity.BDMotor;
using AONAffinity.Data.DataAccess.BDMotor;   

namespace AONAffinity.Business.Logic
{
    public class BLCotizacionRechazo
    {
        #region Transaccional
        public Int32 Insertar(BECotizacionRechazo pObjBECotizacionRechazo) 
        {
            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(AONAffinity.Data.DataAccess.DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DACotizacionRechazo objDACotizacionRechazo = new DACotizacionRechazo();
                nResult = objDACotizacionRechazo.Insertar(pObjBECotizacionRechazo, sqlCn);
            }

            return nResult;
        }
        #endregion
    }
}
