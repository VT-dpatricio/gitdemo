﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;
using AONAffinity.Library.Resources;

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLProductoTipoVehiculo
    {
        #region NoTransaccional
        public List<BEProductoTipoVehiculo> ListarxProducto(Int32 pnIdproducto, Boolean? pbStsRegistro)
        {
            List<BEProductoTipoVehiculo> lstBEProductoTipoVehiculo = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAProductoTipoVehiculo objDAProductoTipoVehiculo = new DAProductoTipoVehiculo();
                SqlDataReader sqlDr = objDAProductoTipoVehiculo.ListarxProducto(pnIdproducto, pbStsRegistro, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idTipoVehiculo");
                        Int32 nIndex3 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex4 = sqlDr.GetOrdinal("codExterno");

                        lstBEProductoTipoVehiculo = new List<BEProductoTipoVehiculo>();

                        while (sqlDr.Read())
                        {
                            BEProductoTipoVehiculo objBEProductoTipoVehiculo = new BEProductoTipoVehiculo();
                            objBEProductoTipoVehiculo.IdProducto = sqlDr.GetInt32(nIndex1);
                            objBEProductoTipoVehiculo.IdTipoVehiculo = sqlDr.GetInt32(nIndex2);
                            objBEProductoTipoVehiculo.Descripcion = sqlDr.GetString(nIndex3);
                            if (sqlDr.IsDBNull(nIndex4)) { objBEProductoTipoVehiculo.CodExterno = NullTypes.CadenaNull; } else { objBEProductoTipoVehiculo.CodExterno = sqlDr.GetString(nIndex4); }

                            lstBEProductoTipoVehiculo.Add(objBEProductoTipoVehiculo);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBEProductoTipoVehiculo;
        }
        #endregion
    }
}
