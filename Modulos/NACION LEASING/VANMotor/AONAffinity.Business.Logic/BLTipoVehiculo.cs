﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLTipoVehiculo
    {
        #region NoTransaccional
        /// <summary>
        /// Permite listar las clases de vehículos.
        /// </summary>
        /// <param name="pbStsRegistro">Estado del registro.</param>
        /// <returns>Objeto List de tipo BEClase.</returns>
        public List<BETipoVehiculo> Listar(Nullable<Boolean> pbStsRegistro)
        {
            List<BETipoVehiculo> lstBETipoVehiculo = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DATipoVehiculo objDATipoVehiculo = new DATipoVehiculo();
                SqlDataReader sqlDr = objDATipoVehiculo.Listar(pbStsRegistro, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idTipoVehiculo");
                        Int32 nIndex2 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex4 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex5 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex6 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("stsRegistro");

                        lstBETipoVehiculo = new List<BETipoVehiculo>();

                        while (sqlDr.Read())
                        {
                            BETipoVehiculo objBETipoVehiculo = new BETipoVehiculo();

                            objBETipoVehiculo.IdTipoVehiculo = sqlDr.GetInt32(nIndex1);
                            objBETipoVehiculo.Descripcion = sqlDr.GetString(nIndex2);
                            objBETipoVehiculo.UsuarioCreacion = sqlDr.GetString(nIndex3);
                            objBETipoVehiculo.FechaCreacion = sqlDr.GetDateTime(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBETipoVehiculo.UsuarioModificacion = NullTypes.CadenaNull; } else { objBETipoVehiculo.UsuarioModificacion = sqlDr.GetString(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBETipoVehiculo.FechaModificacion = NullTypes.FechaNull; } else { objBETipoVehiculo.FechaModificacion = sqlDr.GetDateTime(nIndex6); }
                            objBETipoVehiculo.EstadoRegistro = sqlDr.GetBoolean(nIndex7);

                            lstBETipoVehiculo.Add(objBETipoVehiculo);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBETipoVehiculo;
        }
        #endregion
    }
}
