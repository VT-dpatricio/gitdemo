﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.BusinessLogic
{
    /// <summary>
    /// Clase de lógica del negocio a la tabla TipoProceso.
    /// </summary>
    public class BLTipoProceso
    {
        #region NoTransaccional
        /// <summary>
        /// Permite listar los tipos de procesos por estado.
        /// </summary>
        /// <param name="pbEstado">Estado de registro.</param>
        /// <returns>Objeto lista generica de tipo BETipoProceso.</returns>
        public List<BETipoProceso> Listar(Nullable<Boolean> pbEstado)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2012-05-03
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            List<BETipoProceso> lstBETipoProceso = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DATipoProceso objDATipoProceso = new DATipoProceso();
                SqlDataReader sqlDr = objDATipoProceso.Listar(pbEstado, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idTipoProceso");
                        Int32 nIndex2 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("nombreArchivo");
                        Int32 nIndex4 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex5 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex6 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex8 = sqlDr.GetOrdinal("stsRegistro");

                        lstBETipoProceso = new List<BETipoProceso>();

                        while (sqlDr.Read())
                        {
                            BETipoProceso objBETipoProceso = new BETipoProceso();

                            objBETipoProceso.IdTipoProceso = sqlDr.GetInt32(nIndex1);
                            objBETipoProceso.Descripcion = sqlDr.GetString(nIndex2);
                            objBETipoProceso.NombreArchivo = sqlDr.GetString(nIndex3);
                            objBETipoProceso.UsuarioCreacion = sqlDr.GetString(nIndex4);
                            objBETipoProceso.FechaCreacion = sqlDr.GetDateTime(nIndex5);
                            if (sqlDr.IsDBNull(nIndex6)) { objBETipoProceso.UsuarioModificacion = NullTypes.CadenaNull; } else { objBETipoProceso.UsuarioModificacion = sqlDr.GetString(nIndex6); }
                            if (sqlDr.IsDBNull(nIndex7)) { objBETipoProceso.FechaModificacion = NullTypes.FechaNull; } else { objBETipoProceso.FechaModificacion = sqlDr.GetDateTime(nIndex7); }
                            objBETipoProceso.EstadoRegistro = sqlDr.GetBoolean(nIndex8);

                            lstBETipoProceso.Add(objBETipoProceso);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBETipoProceso;
        }

        /// <summary>
        /// Listar proceso a cotizar.
        /// </summary>
        /// <returns>Objeto lista generica de tipo BETipoProceso.</returns>
        public List<BETipoProceso> ListarProcesoCotizar() 
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2012-05-03
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            List<BETipoProceso> lstBETipoProceso = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DATipoProceso objDATipoProceso = new DATipoProceso();
                SqlDataReader sqlDr = objDATipoProceso.ListarProcesoCotizar(sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idTipoProceso");
                        Int32 nIndex2 = sqlDr.GetOrdinal("descripcion");

                        lstBETipoProceso = new List<BETipoProceso>();

                        while (sqlDr.Read()) 
                        {
                            BETipoProceso objBETipoProceso = new BETipoProceso();

                            objBETipoProceso.IdTipoProceso = sqlDr.GetInt32(nIndex1);
                            objBETipoProceso.Descripcion = sqlDr.GetString(nIndex2);

                            lstBETipoProceso.Add(objBETipoProceso);  
                        }

                        sqlDr.Close();
                    }
                }

                return lstBETipoProceso;
            }
        }

        public BETipoProceso Obtener(Int32 pnIdTipoProceso)
        {
            BETipoProceso objBETipoProceso = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DATipoProceso objDATipoProceso = new DATipoProceso();
                SqlDataReader sqlDr = objDATipoProceso.Obtener(pnIdTipoProceso, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idTipoProceso");
                        Int32 nIndex2 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("nombreArchivo");
                        Int32 nIndex4 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex5 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex6 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex8 = sqlDr.GetOrdinal("stsRegistro");
                        Int32 nIndex9 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex10 = sqlDr.GetOrdinal("rutaArchivo");
                        Int32 nIndex11 = sqlDr.GetOrdinal("rutaArchivoLog");
                        Int32 nIndex12 = sqlDr.GetOrdinal("longitudTrama");

                        objBETipoProceso = new BETipoProceso();

                        if (sqlDr.Read())
                        {
                            objBETipoProceso.IdTipoProceso = sqlDr.GetInt32(nIndex1);
                            objBETipoProceso.Descripcion = sqlDr.GetString(nIndex2);
                            objBETipoProceso.NombreArchivo = sqlDr.GetString(nIndex3);
                            objBETipoProceso.UsuarioCreacion = sqlDr.GetString(nIndex4);
                            objBETipoProceso.FechaCreacion = sqlDr.GetDateTime(nIndex5);
                            if (sqlDr.IsDBNull(nIndex6)) { objBETipoProceso.UsuarioModificacion = NullTypes.CadenaNull; } else { objBETipoProceso.UsuarioModificacion = sqlDr.GetString(nIndex6); }
                            if (sqlDr.IsDBNull(nIndex7)) { objBETipoProceso.FechaModificacion = NullTypes.FechaNull; } else { objBETipoProceso.FechaModificacion = sqlDr.GetDateTime(nIndex7); }
                            objBETipoProceso.EstadoRegistro = sqlDr.GetBoolean(nIndex8);
                            if (sqlDr.IsDBNull(nIndex9)) { objBETipoProceso.IdProducto = NullTypes.IntegerNull; } else { objBETipoProceso.IdProducto = sqlDr.GetInt32(nIndex9); }
                            if (sqlDr.IsDBNull(nIndex10)) { objBETipoProceso.RutaArchivo = NullTypes.CadenaNull; } else { objBETipoProceso.RutaArchivo = sqlDr.GetString(nIndex10); }
                            if (sqlDr.IsDBNull(nIndex11)) { objBETipoProceso.RutaArchivoLog = NullTypes.CadenaNull; } else { objBETipoProceso.RutaArchivoLog = sqlDr.GetString(nIndex11); }
                            if (sqlDr.IsDBNull(nIndex12)) { objBETipoProceso.LongitudTrama = NullTypes.IntegerNull; } else { objBETipoProceso.LongitudTrama = sqlDr.GetInt32(nIndex12); }
                        }

                        sqlDr.Close();
                    }
                }
            }

            return objBETipoProceso;
        }
        #endregion        
    }
}
