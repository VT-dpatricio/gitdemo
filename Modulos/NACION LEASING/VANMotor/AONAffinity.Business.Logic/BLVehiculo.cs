﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLVehiculo
    {
        public List<BEVehiculo> ObtenerxCertificado(String pcIdCertificado) 
        {
            List<BEVehiculo> lstBEVehiculo = new List<BEVehiculo>();

            AONAffinity.Motor.BusinessLogic.Bais.BLInfoAseguradoC objBLInfoAseguradoC = new AONAffinity.Motor.BusinessLogic.Bais.BLInfoAseguradoC();
            List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC> lstBEInfoAseguradoC = objBLInfoAseguradoC.ListarxCertificado(pcIdCertificado);

            if (lstBEInfoAseguradoC != null) 
            {
                BEVehiculo objBEVehiculo = new BEVehiculo();

                foreach (AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC objBEInfoAseguradoC in lstBEInfoAseguradoC)
                {
                    if (objBEInfoAseguradoC.Nombre == "MARCA" ) { objBEVehiculo.Marca = objBEInfoAseguradoC.ValorString; }
                    if (objBEInfoAseguradoC.Nombre == "MODELO" ) { objBEVehiculo.Modelo = objBEInfoAseguradoC.ValorString; }
                    if (objBEInfoAseguradoC.Nombre == "COLOR" ) { objBEVehiculo.Color = objBEInfoAseguradoC.ValorString; }
                    if (objBEInfoAseguradoC.Nombre == "AÑOFABRICACIÓN" ) { objBEVehiculo.AnioFab = Convert.ToInt16(objBEInfoAseguradoC.ValorNum); }
                    if (objBEInfoAseguradoC.Nombre == "NUMERODECHASIS" ) { objBEVehiculo.NroChasis = objBEInfoAseguradoC.ValorString; }
                    if (objBEInfoAseguradoC.Nombre == "NUMERODEMOTOR" ) { objBEVehiculo.NroMotor = objBEInfoAseguradoC.ValorString; }
                    if (objBEInfoAseguradoC.Nombre == "NUMERODEMATRICULAPLACA") { objBEVehiculo.Placa = objBEInfoAseguradoC.ValorString; }
                    if (objBEInfoAseguradoC.Nombre == "CLASEDEVEHÍCULOS") { objBEVehiculo.Clase = objBEInfoAseguradoC.ValorString; }
                    if (objBEInfoAseguradoC.Nombre == "USODEVEHÍCULOS") { objBEVehiculo.Uso = objBEInfoAseguradoC.ValorString; }
                    if (objBEInfoAseguradoC.Nombre == "TIPODEVEHICULO") { objBEVehiculo.Tipo = objBEInfoAseguradoC.ValorString; }                                        
                }

                lstBEVehiculo.Add(objBEVehiculo); 
            }            

            return lstBEVehiculo;
        }  
    }
}
