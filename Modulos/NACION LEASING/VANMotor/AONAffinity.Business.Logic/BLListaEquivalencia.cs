﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using AONAffinity.Motor.BusinessEntity;
using AONAffinity.Motor.DataAccess.DLNT;
namespace AONAffinity.Motor.BusinessLogic
{
    public class BLListaEquivalencia
    {
        public IList Seleccionar(int pIDProducto, string pTabla)
        {
            DLNTListaEquivalencia oDL = new DLNTListaEquivalencia();
            BEListaEquivalencia oBE = new BEListaEquivalencia();
            oBE.IdProducto = pIDProducto;
            oBE.Tabla = pTabla;
            return oDL.Seleccionar(oBE);
        }

        public BEListaEquivalencia getValorAON(IList pLista, string pValoEquivalente)
        {
            BEListaEquivalencia oBE = new BEListaEquivalencia();
            foreach (BEListaEquivalencia oBEEQ in pLista)
            {
                if (oBEEQ.ValorEquivalencia == pValoEquivalente)
                {
                    oBE = oBEEQ;
                    break; // TODO: might not be correct. Was : Exit For
                }
            }
            return oBE;
        }

        public BEListaEquivalencia getValorAON(int pIDProducto, string pTabla, string pValoEquivalente)
        {
            BEListaEquivalencia oBE = new BEListaEquivalencia();
            IList pLista = Seleccionar(pIDProducto, pTabla);
            return getValorAON(pLista, pValoEquivalente);
        }


        public BEListaEquivalencia getValorEquivalencia(IList pLista, string pValoAON)
        {
            BEListaEquivalencia oBE = new BEListaEquivalencia();
            foreach (BEListaEquivalencia oBEEQ in pLista)
            {
                if (oBEEQ.ValorAON == pValoAON)
                {
                    oBE = oBEEQ;
                    break; // TODO: might not be correct. Was : Exit For
                }
            }
            return oBE;
        }

        public BEListaEquivalencia getValorEquivalencia(int pIDProducto, string pTabla, string pValoAON)
        {
            BEListaEquivalencia oBE = new BEListaEquivalencia();
            IList pLista = Seleccionar(pIDProducto, pTabla);
            return getValorEquivalencia(pLista, pValoAON);
        }
    }
}
