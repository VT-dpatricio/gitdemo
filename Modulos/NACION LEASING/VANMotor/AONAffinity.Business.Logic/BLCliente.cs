﻿using System;
using System.IO; 
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources; 
using AONAffinity.Motor.DataAccess; 
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.BusinessLogic
{
    /// <summary>
    /// Clase que contiene la lógica del negocio de los clientes.
    /// </summary>
    public class BLCliente
    {
        #region NoTransaccional
        /// <summary>
        /// Permite listar los clientes no cotizados.
        /// </summary>
        /// <returns>Objeto lista de tipo BECliente.</returns>
        public List<BECliente> ListarxCotizar()
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.:2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.:____-__-__
            ---------------------------------------------------------------------------*/

            List<BECliente> lstBECliente = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DACliente objDACliente = new DACliente();
                SqlDataReader sqlDr = objDACliente.ListarxCotizar(sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("nroCertificado");
                        Int32 nIndex2 = sqlDr.GetOrdinal("codAsegurador");
                        Int32 nIndex3 = sqlDr.GetOrdinal("apePaterno");
                        Int32 nIndex4 = sqlDr.GetOrdinal("apeMaterno");
                        Int32 nIndex5 = sqlDr.GetOrdinal("priNombre");
                        Int32 nIndex6 = sqlDr.GetOrdinal("segNombre");
                        Int32 nIndex7 = sqlDr.GetOrdinal("idTipoDocumento");
                        Int32 nIndex8 = sqlDr.GetOrdinal("nroDocumento");
                        Int32 nIndex9 = sqlDr.GetOrdinal("sexo");
                        Int32 nIndex10 = sqlDr.GetOrdinal("fecNacimiento");
                        Int32 nIndex11 = sqlDr.GetOrdinal("idCiudad");
                        Int32 nIndex12 = sqlDr.GetOrdinal("direccion");
                        Int32 nIndex13 = sqlDr.GetOrdinal("telDomicilio1");
                        Int32 nIndex14 = sqlDr.GetOrdinal("telDomicilio2");
                        Int32 nIndex15 = sqlDr.GetOrdinal("telDomicilio3");
                        Int32 nIndex16 = sqlDr.GetOrdinal("telMovil1");
                        Int32 nIndex17 = sqlDr.GetOrdinal("telMovil2");
                        Int32 nIndex18 = sqlDr.GetOrdinal("telMovil3");
                        Int32 nIndex19 = sqlDr.GetOrdinal("telOficina1");
                        Int32 nIndex20 = sqlDr.GetOrdinal("telOficina2");
                        Int32 nIndex21 = sqlDr.GetOrdinal("telOficina3");
                        Int32 nIndex22 = sqlDr.GetOrdinal("email1");
                        Int32 nIndex23 = sqlDr.GetOrdinal("email2");                                               
                        Int32 nIndex24 = sqlDr.GetOrdinal("fecVigencia");
                        Int32 nIndex25 = sqlDr.GetOrdinal("fecFinVigencia");
                        Int32 nIndex26 = sqlDr.GetOrdinal("idMarca");   
                        Int32 nIndex27 = sqlDr.GetOrdinal("idModelo");
                        Int32 nIndex28 = sqlDr.GetOrdinal("nroPlaca");
                        Int32 nIndex29 = sqlDr.GetOrdinal("anioFab");
                        Int32 nIndex30 = sqlDr.GetOrdinal("valorVehiculo");                        
                        Int32 nIndex31 = sqlDr.GetOrdinal("nroSerie");
                        Int32 nIndex32 = sqlDr.GetOrdinal("color");
                        Int32 nIndex33 = sqlDr.GetOrdinal("idUsoVehiculo");
                        Int32 nIndex34 = sqlDr.GetOrdinal("nroMotor");
                        Int32 nIndex35 = sqlDr.GetOrdinal("idClase");
                        Int32 nIndex36 = sqlDr.GetOrdinal("esTimonCambiado");
                        Int32 nIndex37 = sqlDr.GetOrdinal("reqGPS");
                        Int32 nIndex38 = sqlDr.GetOrdinal("esBlindado");
                        Int32 nIndex39 = sqlDr.GetOrdinal("idSponsor");
                         
                        lstBECliente = new List<BECliente>();

                        while (sqlDr.Read()) 
                        {
                            BECliente objBECliente = new BECliente();
                            if (sqlDr.IsDBNull(nIndex1)) { objBECliente.NroCertificado = NullTypes.CadenaNull; } else { objBECliente.NroCertificado = sqlDr.GetString(nIndex1); }
                            objBECliente.CodAsegurador = sqlDr.GetInt32(nIndex2);
                            objBECliente.ApePaterno = sqlDr.GetString(nIndex3);
                            objBECliente.ApeMaterno = sqlDr.GetString(nIndex4);
                            objBECliente.PriNombre = sqlDr.GetString(nIndex5);
                            if (sqlDr.IsDBNull(nIndex6)) { objBECliente.SegNombre = NullTypes.CadenaNull; } else { objBECliente.SegNombre = sqlDr.GetString(nIndex6); }
                            objBECliente.IdTipoDocumento = sqlDr.GetString(nIndex7);
                            objBECliente.NroDocumento = sqlDr.GetString(nIndex8);
                            if (sqlDr.IsDBNull(nIndex9)) { objBECliente.Sexo = NullTypes.CadenaNull; } else { objBECliente.Sexo = sqlDr.GetString(nIndex9); }
                            if (sqlDr.IsDBNull(nIndex10)) { objBECliente.FecNacimiento = NullTypes.FechaNull; } else { objBECliente.FecNacimiento = sqlDr.GetDateTime(nIndex10); }
                            objBECliente.idCiudad = sqlDr.GetInt32(nIndex11);
                            objBECliente.Direccion = sqlDr.GetString(nIndex12);
                            objBECliente.TelDomicilio1 = sqlDr.GetString(nIndex13);
                            if (sqlDr.IsDBNull(nIndex14)) { objBECliente.TelDomicilio2 = NullTypes.CadenaNull; } else { objBECliente.TelDomicilio2 = sqlDr.GetString(nIndex14); }
                            if (sqlDr.IsDBNull(nIndex15)) { objBECliente.TelDomicilio3 = NullTypes.CadenaNull; } else { objBECliente.TelDomicilio3 = sqlDr.GetString(nIndex15); }
                            if (sqlDr.IsDBNull(nIndex16)) { objBECliente.TelMovil1 = NullTypes.CadenaNull; } else { objBECliente.TelMovil1 = sqlDr.GetString(nIndex16); }
                            if (sqlDr.IsDBNull(nIndex17)) { objBECliente.TelMovil2 = NullTypes.CadenaNull; } else { objBECliente.TelMovil2 = sqlDr.GetString(nIndex17); }
                            if (sqlDr.IsDBNull(nIndex18)) { objBECliente.TelMovil3 = NullTypes.CadenaNull; } else { objBECliente.TelMovil3 = sqlDr.GetString(nIndex18); }
                            if (sqlDr.IsDBNull(nIndex19)) { objBECliente.TelOficina1 = NullTypes.CadenaNull; } else { objBECliente.TelOficina1 = sqlDr.GetString(nIndex19); }
                            if (sqlDr.IsDBNull(nIndex20)) { objBECliente.TelOficina2 = NullTypes.CadenaNull; } else { objBECliente.TelOficina2 = sqlDr.GetString(nIndex20); }
                            if (sqlDr.IsDBNull(nIndex21)) { objBECliente.TelOficina3 = NullTypes.CadenaNull; } else { objBECliente.TelOficina3 = sqlDr.GetString(nIndex21); }
                            if (sqlDr.IsDBNull(nIndex22)) { objBECliente.Email1 = NullTypes.CadenaNull; } else { objBECliente.Email1 = sqlDr.GetString(nIndex22); }
                            if (sqlDr.IsDBNull(nIndex23)) { objBECliente.Email2 = NullTypes.CadenaNull; } else { objBECliente.Email2 = sqlDr.GetString(nIndex23); }
                            if (sqlDr.IsDBNull(nIndex24)) { objBECliente.FecVigencia = NullTypes.FechaNull; } else { objBECliente.FecVigencia = sqlDr.GetDateTime(nIndex24); }
                            if (sqlDr.IsDBNull(nIndex25)) { objBECliente.FecFinVigencia = NullTypes.FechaNull; } else { objBECliente.FecFinVigencia = sqlDr.GetDateTime(nIndex25); }
                            objBECliente.IdMarca = sqlDr.GetInt32(nIndex26);
                            objBECliente.IdModelo = sqlDr.GetInt32(nIndex27);
                            if (sqlDr.IsDBNull(nIndex28)) { objBECliente.NroPlaca = NullTypes.CadenaNull; } else { objBECliente.NroPlaca = sqlDr.GetString(nIndex28); }                            
                            objBECliente.AnioFab = sqlDr.GetInt32(nIndex29);
                            objBECliente.ValorVehiculo = sqlDr.GetDecimal(nIndex30);
                            if (sqlDr.IsDBNull(nIndex31)) { objBECliente.NroSerie = NullTypes.CadenaNull; } else { objBECliente.NroSerie = sqlDr.GetString(nIndex31); }
                            objBECliente.Color = sqlDr.GetString(nIndex32);
                            objBECliente.IdUsoVehiculo = sqlDr.GetInt32(nIndex33);
                            objBECliente.NroMotor = sqlDr.GetString(nIndex34);
                            objBECliente.IdClase = sqlDr.GetInt32(nIndex35);                            
                            objBECliente.EsTimonCambiado = sqlDr.GetBoolean(nIndex36);
                            objBECliente.ReqGPS = sqlDr.GetBoolean(nIndex37);
                            objBECliente.EsBlindado = sqlDr.GetBoolean(nIndex38);
                            if (sqlDr.IsDBNull(nIndex39)) { objBECliente.IdSponsor = NullTypes.IntegerNull; } else { objBECliente.IdSponsor = sqlDr.GetInt32(nIndex39); }

                            lstBECliente.Add(objBECliente);
                        }

                        sqlDr.Close(); 
                    } 
                }
            }

            return lstBECliente;
        }

        public List<BECliente> ListarxProceso(Int32 pnIdProceso) 
        {
            List<BECliente> lstBECliente = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DACliente objDACliente = new DACliente();
                SqlDataReader sqlDr = objDACliente.ListarxProceso(pnIdProceso, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("nroCertificado");
                        Int32 nIndex2 = sqlDr.GetOrdinal("codAsegurador");
                        Int32 nIndex3 = sqlDr.GetOrdinal("apePaterno");
                        Int32 nIndex4 = sqlDr.GetOrdinal("apeMaterno");
                        Int32 nIndex5 = sqlDr.GetOrdinal("priNombre");
                        Int32 nIndex6 = sqlDr.GetOrdinal("segNombre");
                        Int32 nIndex7 = sqlDr.GetOrdinal("idTipoDocumento");
                        Int32 nIndex8 = sqlDr.GetOrdinal("nroDocumento");
                        Int32 nIndex9 = sqlDr.GetOrdinal("sexo");
                        Int32 nIndex10 = sqlDr.GetOrdinal("fecNacimiento");
                        Int32 nIndex11 = sqlDr.GetOrdinal("idCiudad");
                        Int32 nIndex12 = sqlDr.GetOrdinal("direccion");
                        Int32 nIndex13 = sqlDr.GetOrdinal("telDomicilio1");
                        Int32 nIndex14 = sqlDr.GetOrdinal("telDomicilio2");
                        Int32 nIndex15 = sqlDr.GetOrdinal("telDomicilio3");
                        Int32 nIndex16 = sqlDr.GetOrdinal("telMovil1");
                        Int32 nIndex17 = sqlDr.GetOrdinal("telMovil2");
                        Int32 nIndex18 = sqlDr.GetOrdinal("telMovil3");
                        Int32 nIndex19 = sqlDr.GetOrdinal("telOficina1");
                        Int32 nIndex20 = sqlDr.GetOrdinal("telOficina2");
                        Int32 nIndex21 = sqlDr.GetOrdinal("telOficina3");
                        Int32 nIndex22 = sqlDr.GetOrdinal("email1");
                        Int32 nIndex23 = sqlDr.GetOrdinal("email2");
                        Int32 nIndex24 = sqlDr.GetOrdinal("fecVigencia");
                        Int32 nIndex25 = sqlDr.GetOrdinal("fecFinVigencia");
                        Int32 nIndex26 = sqlDr.GetOrdinal("idMarca");
                        Int32 nIndex27 = sqlDr.GetOrdinal("idModelo");
                        Int32 nIndex28 = sqlDr.GetOrdinal("nroPlaca");
                        Int32 nIndex29 = sqlDr.GetOrdinal("anioFab");
                        Int32 nIndex30 = sqlDr.GetOrdinal("valorVehiculo");
                        Int32 nIndex31 = sqlDr.GetOrdinal("nroSerie");
                        Int32 nIndex32 = sqlDr.GetOrdinal("color");
                        Int32 nIndex33 = sqlDr.GetOrdinal("idUsoVehiculo");
                        Int32 nIndex34 = sqlDr.GetOrdinal("nroMotor");
                        Int32 nIndex35 = sqlDr.GetOrdinal("idClase");
                        Int32 nIndex36 = sqlDr.GetOrdinal("esTimonCambiado");
                        Int32 nIndex37 = sqlDr.GetOrdinal("reqGPS");
                        Int32 nIndex38 = sqlDr.GetOrdinal("esBlindado");
                        Int32 nIndex39 = sqlDr.GetOrdinal("idSponsor");

                        lstBECliente = new List<BECliente>();

                        while (sqlDr.Read())
                        {
                            BECliente objBECliente = new BECliente();
                            if (sqlDr.IsDBNull(nIndex1)) { objBECliente.NroCertificado = NullTypes.CadenaNull; } else { objBECliente.NroCertificado = sqlDr.GetString(nIndex1); }
                            objBECliente.CodAsegurador = sqlDr.GetInt32(nIndex2);
                            objBECliente.ApePaterno = sqlDr.GetString(nIndex3);
                            objBECliente.ApeMaterno = sqlDr.GetString(nIndex4);
                            objBECliente.PriNombre = sqlDr.GetString(nIndex5);
                            if (sqlDr.IsDBNull(nIndex6)) { objBECliente.SegNombre = NullTypes.CadenaNull; } else { objBECliente.SegNombre = sqlDr.GetString(nIndex6); }
                            objBECliente.IdTipoDocumento = sqlDr.GetString(nIndex7);
                            objBECliente.NroDocumento = sqlDr.GetString(nIndex8);
                            if (sqlDr.IsDBNull(nIndex9)) { objBECliente.Sexo = NullTypes.CadenaNull; } else { objBECliente.Sexo = sqlDr.GetString(nIndex9); }
                            if (sqlDr.IsDBNull(nIndex10)) { objBECliente.FecNacimiento = NullTypes.FechaNull; } else { objBECliente.FecNacimiento = sqlDr.GetDateTime(nIndex10); }
                            objBECliente.idCiudad = sqlDr.GetInt32(nIndex11);
                            objBECliente.Direccion = sqlDr.GetString(nIndex12);
                            objBECliente.TelDomicilio1 = sqlDr.GetString(nIndex13);
                            if (sqlDr.IsDBNull(nIndex14)) { objBECliente.TelDomicilio2 = NullTypes.CadenaNull; } else { objBECliente.TelDomicilio2 = sqlDr.GetString(nIndex14); }
                            if (sqlDr.IsDBNull(nIndex15)) { objBECliente.TelDomicilio3 = NullTypes.CadenaNull; } else { objBECliente.TelDomicilio3 = sqlDr.GetString(nIndex15); }
                            if (sqlDr.IsDBNull(nIndex16)) { objBECliente.TelMovil1 = NullTypes.CadenaNull; } else { objBECliente.TelMovil1 = sqlDr.GetString(nIndex16); }
                            if (sqlDr.IsDBNull(nIndex17)) { objBECliente.TelMovil2 = NullTypes.CadenaNull; } else { objBECliente.TelMovil2 = sqlDr.GetString(nIndex17); }
                            if (sqlDr.IsDBNull(nIndex18)) { objBECliente.TelMovil3 = NullTypes.CadenaNull; } else { objBECliente.TelMovil3 = sqlDr.GetString(nIndex18); }
                            if (sqlDr.IsDBNull(nIndex19)) { objBECliente.TelOficina1 = NullTypes.CadenaNull; } else { objBECliente.TelOficina1 = sqlDr.GetString(nIndex19); }
                            if (sqlDr.IsDBNull(nIndex20)) { objBECliente.TelOficina2 = NullTypes.CadenaNull; } else { objBECliente.TelOficina2 = sqlDr.GetString(nIndex20); }
                            if (sqlDr.IsDBNull(nIndex21)) { objBECliente.TelOficina3 = NullTypes.CadenaNull; } else { objBECliente.TelOficina3 = sqlDr.GetString(nIndex21); }
                            if (sqlDr.IsDBNull(nIndex22)) { objBECliente.Email1 = NullTypes.CadenaNull; } else { objBECliente.Email1 = sqlDr.GetString(nIndex22); }
                            if (sqlDr.IsDBNull(nIndex23)) { objBECliente.Email2 = NullTypes.CadenaNull; } else { objBECliente.Email2 = sqlDr.GetString(nIndex23); }
                            if (sqlDr.IsDBNull(nIndex24)) { objBECliente.FecVigencia = NullTypes.FechaNull; } else { objBECliente.FecVigencia = sqlDr.GetDateTime(nIndex24); }
                            if (sqlDr.IsDBNull(nIndex25)) { objBECliente.FecFinVigencia = NullTypes.FechaNull; } else { objBECliente.FecFinVigencia = sqlDr.GetDateTime(nIndex25); }
                            objBECliente.IdMarca = sqlDr.GetInt32(nIndex26);
                            objBECliente.IdModelo = sqlDr.GetInt32(nIndex27);
                            if (sqlDr.IsDBNull(nIndex28)) { objBECliente.NroPlaca = NullTypes.CadenaNull; } else { objBECliente.NroPlaca = sqlDr.GetString(nIndex28); }
                            objBECliente.AnioFab = sqlDr.GetInt32(nIndex29);
                            objBECliente.ValorVehiculo = sqlDr.GetDecimal(nIndex30);
                            if (sqlDr.IsDBNull(nIndex31)) { objBECliente.NroSerie = NullTypes.CadenaNull; } else { objBECliente.NroSerie = sqlDr.GetString(nIndex31); }
                            objBECliente.Color = sqlDr.GetString(nIndex32);
                            objBECliente.IdUsoVehiculo = sqlDr.GetInt32(nIndex33);
                            objBECliente.NroMotor = sqlDr.GetString(nIndex34);
                            objBECliente.IdClase = sqlDr.GetInt32(nIndex35);
                            objBECliente.EsTimonCambiado = sqlDr.GetBoolean(nIndex36);
                            objBECliente.ReqGPS = sqlDr.GetBoolean(nIndex37);
                            objBECliente.EsBlindado = sqlDr.GetBoolean(nIndex38);
                            if (sqlDr.IsDBNull(nIndex39)) { objBECliente.IdSponsor = NullTypes.IntegerNull; } else { objBECliente.IdSponsor = sqlDr.GetInt32(nIndex39); }

                            lstBECliente.Add(objBECliente);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBECliente;
        }

        public BECliente Obtener(String pcNroMotor)
        {
            BECliente objBECliente = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DACliente objDACliente = new DACliente();
                SqlDataReader sqlDr = objDACliente.Obtener(pcNroMotor, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("nroMotor");
                        Int32 nIndex2 = sqlDr.GetOrdinal("apePaterno");
                        Int32 nIndex3 = sqlDr.GetOrdinal("apeMaterno");
                        Int32 nIndex4 = sqlDr.GetOrdinal("priNombre");
                        Int32 nIndex5 = sqlDr.GetOrdinal("segNombre");
                        Int32 nIndex6 = sqlDr.GetOrdinal("idTipoDocumento");
                        Int32 nIndex7 = sqlDr.GetOrdinal("nroDocumento");                        

                        objBECliente = new BECliente();

                        if (sqlDr.Read())
                        {
                            objBECliente.NroMotor = sqlDr.GetString(nIndex1);                            
                            objBECliente.ApePaterno = sqlDr.GetString(nIndex2);
                            objBECliente.ApeMaterno = sqlDr.GetString(nIndex3);
                            objBECliente.PriNombre = sqlDr.GetString(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBECliente.SegNombre = NullTypes.CadenaNull; } else { objBECliente.SegNombre = sqlDr.GetString(nIndex5); }
                            objBECliente.IdTipoDocumento = sqlDr.GetString(nIndex6);
                            objBECliente.NroDocumento = sqlDr.GetString(nIndex7);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return objBECliente;
        }

        public List<BECliente> ObtenerxCertificado(String pcIdCertificado) 
        {
            List<BECliente> lstBECliente = new List<BECliente>();

            AONAffinity.Motor.BusinessLogic.Bais.BLInfoProductoC objBLInfoProductoC = new AONAffinity.Motor.BusinessLogic.Bais.BLInfoProductoC();
            List<AONAffinity.Library.BusinessEntity.BDJanus.BEInfoProductoC> lstBEInfoProductoC = objBLInfoProductoC.ListarxCertificado(pcIdCertificado);

            if (lstBEInfoProductoC != null)             
            {
                BECliente objBECliente = new BECliente();

                foreach (AONAffinity.Library.BusinessEntity.BDJanus.BEInfoProductoC objBEInfoProductoC in lstBEInfoProductoC) 
                {
                    if (objBEInfoProductoC.Nombre == "SEXO") { objBECliente.Sexo = objBEInfoProductoC.ValorString; }
                    //if (objBEInfoProductoC.Nombre == "ESTADOCIVIL" ) { objBECliente.Estad = objBEInfoProductoC.ValorDate; }
                    if (objBEInfoProductoC.Nombre == "FECNACIMIENTO") { objBECliente.FecNacimiento = objBEInfoProductoC.ValorDate; }
                    if (objBEInfoProductoC.Nombre == "NROPOLIZA") { objBECliente.NroPoliza = objBEInfoProductoC.ValorString; }
                }

                lstBECliente.Add(objBECliente);
            }

            return lstBECliente;
        }

        public BECliente ObtenerxPlaca(String pcPlaca)
        {
            BECliente objBECliente = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DACliente objDACliente = new DACliente();
                SqlDataReader sqlDr = objDACliente.ObtenerxPlaca(pcPlaca, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("nroMotor");
                        Int32 nIndex2 = sqlDr.GetOrdinal("apePaterno");
                        Int32 nIndex3 = sqlDr.GetOrdinal("apeMaterno");
                        Int32 nIndex4 = sqlDr.GetOrdinal("priNombre");
                        Int32 nIndex5 = sqlDr.GetOrdinal("segNombre");
                        Int32 nIndex6 = sqlDr.GetOrdinal("idTipoDocumento");
                        Int32 nIndex7 = sqlDr.GetOrdinal("nroDocumento");

                        objBECliente = new BECliente();

                        if (sqlDr.Read())
                        {
                            objBECliente.NroMotor = sqlDr.GetString(nIndex1);
                            objBECliente.ApePaterno = sqlDr.GetString(nIndex2);
                            objBECliente.ApeMaterno = sqlDr.GetString(nIndex3);
                            objBECliente.PriNombre = sqlDr.GetString(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBECliente.SegNombre = NullTypes.CadenaNull; } else { objBECliente.SegNombre = sqlDr.GetString(nIndex5); }
                            objBECliente.IdTipoDocumento = sqlDr.GetString(nIndex6);
                            objBECliente.NroDocumento = sqlDr.GetString(nIndex7);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return objBECliente;
        }

        public BECliente ObtenerxSerie(String pcSerie) 
        {
            BECliente objBECliente = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DACliente objDACliente = new DACliente();
                SqlDataReader sqlDr = objDACliente.ObtenerxSerie(pcSerie, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {                        
                        Int32 nIndex1 = sqlDr.GetOrdinal("nroMotor");
                        Int32 nIndex2 = sqlDr.GetOrdinal("apePaterno");
                        Int32 nIndex3 = sqlDr.GetOrdinal("apeMaterno");
                        Int32 nIndex4 = sqlDr.GetOrdinal("priNombre");
                        Int32 nIndex5 = sqlDr.GetOrdinal("segNombre");
                        Int32 nIndex6 = sqlDr.GetOrdinal("idTipoDocumento");
                        Int32 nIndex7 = sqlDr.GetOrdinal("nroDocumento");

                        objBECliente = new BECliente();

                        if (sqlDr.Read())
                        {
                            objBECliente.NroMotor = sqlDr.GetString(nIndex1);
                            objBECliente.ApePaterno = sqlDr.GetString(nIndex2);
                            objBECliente.ApeMaterno = sqlDr.GetString(nIndex3);
                            objBECliente.PriNombre = sqlDr.GetString(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBECliente.SegNombre = NullTypes.CadenaNull; } else { objBECliente.SegNombre = sqlDr.GetString(nIndex5); }
                            objBECliente.IdTipoDocumento = sqlDr.GetString(nIndex6);
                            objBECliente.NroDocumento = sqlDr.GetString(nIndex7);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return objBECliente;
        }
        #endregion

        #region Transaccional
        public Int32 Insertar(BECliente pObjBECliente) 
        {
            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DACliente objDACliente = new DACliente();
                nResult = objDACliente.Insertar(pObjBECliente, sqlCn);
            }

            return nResult;
        }

        /// <summary>
        /// Permite actualizar el estado del cliente.
        /// </summary>
        /// <param name="pcNroMotor">Nro de motor.</param>
        /// <param name="pnEstadoCliente">Valor del nuevo estado del cliente.</param>
        /// <param name="pcUsuario">Código de usuario de actualización.</param>
        /// <returns>0 No Actualozó | 1 Si Actualizó</returns>
        public Int32 Atualizar(String pcNroMotor, Int32 pnEstadoCliente, String pcUsuario) 
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.:2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.:____-__-__
            ---------------------------------------------------------------------------*/

            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DACliente objDACliente = new DACliente();
                nResult = objDACliente.Atualizar(pcNroMotor, pnEstadoCliente, pcUsuario, sqlCn); 
            }

            return nResult;
        }
        #endregion                          
    }
}
