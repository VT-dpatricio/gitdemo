﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLTasa
    {
        #region NoTransaccional
        public BETasa Obtener(Int32 pnIdModelo, Int32 pnIdTipo, Int32 pnAnioAntiguedad) 
        {
            BETasa objBETasa = null;

            using(SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DATasa objDATasa = new DATasa();
                SqlDataReader sqlDr = objDATasa.Obtener(pnIdModelo, pnIdTipo, pnAnioAntiguedad, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idModelo");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idCategoria");
                        Int32 nIndex3 = sqlDr.GetOrdinal("desCategoria");
                        Int32 nIndex4 = sqlDr.GetOrdinal("montoPrima");
                        Int32 nIndex5 = sqlDr.GetOrdinal("idMoneda");
                        Int32 nIndex6 = sqlDr.GetOrdinal("idPeriodo");
                        Int32 nIndex7 = sqlDr.GetOrdinal("idTipo");
                        Int32 nIndex8 = sqlDr.GetOrdinal("anioAntiguedad");
                        Int32 nIndex9 = sqlDr.GetOrdinal("valorTasa");

                        objBETasa = new BETasa();

                        if (sqlDr.Read()) 
                        {
                            objBETasa.IdModelo = sqlDr.GetInt32(nIndex1);
                            objBETasa.IdCategoria = sqlDr.GetInt32(nIndex2);
                            objBETasa.DesCategoria = sqlDr.GetString(nIndex3);
                            if (sqlDr.IsDBNull(nIndex4)) { objBETasa.MontoPrima = NullTypes.DecimalNull; } else { objBETasa.MontoPrima = sqlDr.GetDecimal(nIndex4); }
                            if (sqlDr.IsDBNull(nIndex5)) { objBETasa.IdMoneda = NullTypes.CadenaNull; } else { objBETasa.IdMoneda = sqlDr.GetString(nIndex5); }
                            objBETasa.IdPeriodo = sqlDr.GetInt32(nIndex6);                    
                            objBETasa.IdTipo = sqlDr.GetInt32(nIndex7);
                            objBETasa.AnioAntiguedad = sqlDr.GetInt32(nIndex8);
                            objBETasa.ValorTasa = sqlDr.GetDecimal(nIndex9);          
                        }

                        sqlDr.Close();
                    }
                }
            }

            return objBETasa;
        }
        #endregion
    }
}
