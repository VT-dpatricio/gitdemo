﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;      

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLDescuentoRecargo
    {
        #region NoTransaccional
        public BEDescuentoRecargo Obtener(String pcTipo, Int32 pnValor) 
        {
            BEDescuentoRecargo objBEDescuentoRecargo = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DADescuentoRecargo objDADescuentoRecargo = new DADescuentoRecargo();
                SqlDataReader sqlDr = objDADescuentoRecargo.Obtener(pcTipo, pnValor, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idDsctoRcrgo");
                        Int32 nIndex2 = sqlDr.GetOrdinal("porcentaje");
                        Int32 nIndex3 = sqlDr.GetOrdinal("tipo");
                        Int32 nIndex4 = sqlDr.GetOrdinal("minValor");
                        Int32 nIndex5 = sqlDr.GetOrdinal("maxValor");
                        Int32 nIndex6 = sqlDr.GetOrdinal("condicion");

                        objBEDescuentoRecargo = new BEDescuentoRecargo();

                        if (sqlDr.Read()) 
                        {
                            objBEDescuentoRecargo.IdDsctoRcrgo = sqlDr.GetInt32(nIndex1);
                            objBEDescuentoRecargo.Porcentaje = sqlDr.GetDecimal(nIndex2);
                            objBEDescuentoRecargo.Tipo = sqlDr.GetString(nIndex3);
                            objBEDescuentoRecargo.MinValor = sqlDr.GetInt32(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBEDescuentoRecargo.MaxValor = NullTypes.IntegerNull; } else { objBEDescuentoRecargo.MaxValor = sqlDr.GetInt32(nIndex5); }
                             
                            objBEDescuentoRecargo.Condicion = sqlDr.GetString(nIndex6);
                        }

                        sqlDr.Read(); 
                    }
                }
            }

            return objBEDescuentoRecargo;
        }

        public List<BEDescuentoRecargo> ListarxGrupoProducto(Int32 pnIdGrupo, String pcTipo, Boolean? pbStsRegistro) 
        {
            List<BEDescuentoRecargo> lstBEDescuentoRecargo = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DADescuentoRecargo objDADescuentoRecargo = new DADescuentoRecargo();
                SqlDataReader sqlDr = objDADescuentoRecargo.ListarxGrupoProducto(pnIdGrupo, pcTipo, pbStsRegistro, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("IdGrupoOpcion");
                        Int32 nIndex2 = sqlDr.GetOrdinal("condicion");

                        lstBEDescuentoRecargo = new List<BEDescuentoRecargo>();

                        while (sqlDr.Read()) 
                        {
                            BEDescuentoRecargo objBEDescuentoRecargo = new BEDescuentoRecargo();
                            objBEDescuentoRecargo.IdGrupoOpcion = sqlDr.GetInt32(nIndex1);
                            objBEDescuentoRecargo.Condicion = sqlDr.GetString(nIndex2);

                            lstBEDescuentoRecargo.Add(objBEDescuentoRecargo);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBEDescuentoRecargo;
        }
        #endregion
    }
}
