﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources; 
using AONAffinity.Motor.DataAccess;  
using AONAffinity.Motor.BusinessEntity;

//using AONAffinity.Library.DataAccess;
//using AONAffinity.Library.DataAccess.BDMotor;
//using AONAffinity.Library.BusinessEntity.BDMotor;

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLCategoria
    {
        #region NoTransaccional
        public BECategoria Obtener(Int32 pnIdCategoria)
        {
            BECategoria objBECategoria = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DACategoria objDACategoria = new DACategoria();
                SqlDataReader sqlDr = objDACategoria.Obtener(pnIdCategoria, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idCategoria");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idCategoriaPad");
                        Int32 nIndex3 = sqlDr.GetOrdinal("desCategoria");
                        Int32 nIndex4 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex5 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex6 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex8 = sqlDr.GetOrdinal("stsRegistro");
                        Int32 nIndex9 = sqlDr.GetOrdinal("montoPrima");
                        Int32 nIndex10 = sqlDr.GetOrdinal("idMoneda");

                        objBECategoria = new BECategoria();

                        if (sqlDr.Read())
                        {
                            objBECategoria.IdCategoria = sqlDr.GetInt32(nIndex1);
                            if (sqlDr.IsDBNull(nIndex2)) { objBECategoria.IdCategoriaPad = NullTypes.IntegerNull; } else { objBECategoria.IdCategoriaPad = sqlDr.GetInt32(nIndex2); }
                            objBECategoria.DesCategoria = sqlDr.GetString(nIndex3);
                            objBECategoria.UsuarioCreacion = sqlDr.GetString(nIndex4);
                            objBECategoria.FechaCreacion = sqlDr.GetDateTime(nIndex5);
                            if (sqlDr.IsDBNull(nIndex6)) { objBECategoria.UsuarioModificacion = NullTypes.CadenaNull; } else { objBECategoria.UsuarioModificacion = sqlDr.GetString(nIndex6); }
                            if (sqlDr.IsDBNull(nIndex7)) { objBECategoria.FechaModificacion = NullTypes.FechaNull; } else { objBECategoria.FechaModificacion = sqlDr.GetDateTime(nIndex7); }
                            objBECategoria.EstadoRegistro = sqlDr.GetBoolean(nIndex8);
                            if (sqlDr.IsDBNull(nIndex9)) { objBECategoria.MontoPrima = NullTypes.DecimalNull; } else { objBECategoria.MontoPrima = sqlDr.GetDecimal(nIndex9); }
                            if (sqlDr.IsDBNull(nIndex10)) { objBECategoria.IdMoneda = NullTypes.CadenaNull; } else { objBECategoria.IdMoneda = sqlDr.GetString(nIndex10); }
                        }

                        sqlDr.Close();
                    }
                }
            }

            return objBECategoria;
        }

        public BECategoria ObtenerxModelo(Int32 pnIdModelo)
        {
            BECategoria objBECategoria = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DACategoria objDACategoria = new DACategoria();
                SqlDataReader sqlDr = objDACategoria.ObtenerxModelo(pnIdModelo, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idCategoria");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idCategoriaPad");
                        Int32 nIndex3 = sqlDr.GetOrdinal("desCategoria");
                        Int32 nIndex4 = sqlDr.GetOrdinal("montoPrima");
                        Int32 nIndex5 = sqlDr.GetOrdinal("minAntiguedad");
                        Int32 nIndex6 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex7 = sqlDr.GetOrdinal("codExterno");

                        if (sqlDr.Read()) 
                        {
                            objBECategoria = new BECategoria();
                            objBECategoria.IdCategoria = sqlDr.GetInt32(nIndex1);
                            if (sqlDr.IsDBNull(nIndex2)) { objBECategoria.IdCategoriaPad = NullTypes.IntegerNull; } else { objBECategoria.IdCategoriaPad = sqlDr.GetInt32(nIndex2); }
                            objBECategoria.DesCategoria = sqlDr.GetString(nIndex3);
                            if (sqlDr.IsDBNull(nIndex4)) { objBECategoria.MontoPrima = NullTypes.IntegerNull; } else { objBECategoria.MontoPrima = sqlDr.GetDecimal(nIndex4); }
                            if (sqlDr.IsDBNull(nIndex5)) { objBECategoria.MinAntiguedad = NullTypes.IntegerNull; } else { objBECategoria.MinAntiguedad = sqlDr.GetInt32(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBECategoria.IdProducto = NullTypes.IntegerNull; } else { objBECategoria.IdProducto = sqlDr.GetInt32(nIndex6); }
                            if (sqlDr.IsDBNull(nIndex7)) { objBECategoria.CodExterno = NullTypes.CadenaNull; } else { objBECategoria.CodExterno = sqlDr.GetString(nIndex7); }
                        }

                        sqlDr.Close();
                    }
                }
            }

            return objBECategoria;
        }
        #endregion        
    }
}
