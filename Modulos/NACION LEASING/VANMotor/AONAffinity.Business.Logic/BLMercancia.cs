﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Resources;
using AONAffinity.Business.Entity.BDMotor;
using AONAffinity.Data.DataAccess;
using AONAffinity.Data.DataAccess.BDMotor;

namespace AONAffinity.Business.Logic
{
    /// <summary>
    /// Clase de lógica del negocio de la tabla Mercancia.
    /// </summary>
    public class BLMercancia
    {
        #region NoTransaccional
        /// <summary>
        /// Permite listar los tipos de mercancias.
        /// </summary>
        /// <param name="pbStsRegistro">Estado del registro.</param>
        /// <returns>Objeto de List de tipo BEMercancia.</returns>
        public List<BEMercancia> Listar(Nullable<Boolean> pbStsRegistro) 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-31
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            List<BEMercancia> lstBEMercancia = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAMercancia objDAMercancia = new DAMercancia();
                SqlDataReader sqlDr = objDAMercancia.Listar(pbStsRegistro, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idMercancia");
                        Int32 nIndex2 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex4 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex5 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex6 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("stsRegistro");

                        lstBEMercancia = new List<BEMercancia>();

                        while (sqlDr.Read()) 
                        {
                            BEMercancia objBEMercancia = new BEMercancia();

                            objBEMercancia.IdMercancia = sqlDr.GetInt32(nIndex1);
                            objBEMercancia.Descripcion = sqlDr.GetString(nIndex2);
                            objBEMercancia.UsuarioCreacion = sqlDr.GetString(nIndex3);
                            objBEMercancia.FechaCreacion = sqlDr.GetDateTime(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBEMercancia.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEMercancia.UsuarioModificacion = sqlDr.GetString(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBEMercancia.FechaModificacion = NullTypes.FechaNull; } else { objBEMercancia.FechaModificacion = sqlDr.GetDateTime(nIndex6); }
                            objBEMercancia.EstadoRegistro = sqlDr.GetBoolean(nIndex7);

                            lstBEMercancia.Add(objBEMercancia);  
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return lstBEMercancia;
        }
        #endregion
    }
}
