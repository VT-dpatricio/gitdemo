﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources; 
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;

//using AONAffinity.Library.DataAccess;
//using AONAffinity.Library.DataAccess.BDMotor;  
//using AONAffinity.Library.BusinessEntity.BDMotor; 

namespace AONAffinity.Motor.BusinessLogic.Import
{
    /// <summary>
    /// Clase de lógica del negocio para el proceso de import.
    /// </summary>
    public class BLImport
    {
        #region Métodos
        /// <summary>
        /// Permite escribir los errores en archivo log del proceso import.
        /// </summary>
        /// <param name="pLstLog">Lista de errores.</param>
        /// <param name="pcRuta">Ruta donde se guardará el archivo log.</param>
        /// <param name="pcNombreArchivo">Nombre del archivo log.</param>
        public void EscribirErrores(List<String> pLstLog, String pcRuta, String pcNombreArchivo)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            StreamWriter swLog = new StreamWriter(pcRuta + pcNombreArchivo);

            if ((pLstLog != null) && (pLstLog.Count != 0))
            {
                foreach (String cError in pLstLog)
                {
                    swLog.WriteLine(cError);
                }
            }
            else
                swLog.WriteLine("No se presentaron inconsistencias");

            swLog.Close();
        }
        #endregion

        #region Funciones
        public Encoding GetFileEncoding(String srcFile, FileStream fs)
        {
            byte[] buffer = new byte[5];
            fs.Read(buffer, 0, 5);
            fs.Position = 0;
            Encoding enc = Encoding.Default;
            if (buffer[0] == 0xef && buffer[1] == 0xbb && buffer[2] == 0xbf)
                enc = Encoding.UTF8;
            else if (buffer[0] == 0xfe && buffer[1] == 0xff)
                enc = Encoding.Unicode;
            else if (buffer[0] == 0 && buffer[1] == 0 && buffer[2] == 0xfe && buffer[3] == 0xff)
                enc = Encoding.UTF32;
            else if (buffer[0] == 0x2b && buffer[1] == 0x2f && buffer[2] == 0x76)
                enc = Encoding.UTF7;
            return enc;
        }

        /// <summary>
        /// PErmite generar el SPLIT en la trama a cargar.
        /// </summary>
        /// <param name="pcTrama">Valor de trama.</param>
        /// <param name="lstBEEstructuraArchivo">Estrucura de trama.</param>
        /// <returns>Los valores de la trama en una lista genérica de tipo String.</returns>
        public List<String> TramaSplit(String pcTrama, List<BEEstructuraArchivo> lstBEEstructuraArchivo)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            List<String> lstCampos = new List<String>();
            String cTrama = String.Empty;
            Int32 nColumna = 0;
            Int32 nPosicion = 0;

            foreach (BEEstructuraArchivo objBEEstructuraArchivo in lstBEEstructuraArchivo)
            {
                cTrama = pcTrama.Substring(nPosicion, objBEEstructuraArchivo.Longitud);
                lstCampos.Add(cTrama);
                nPosicion = nPosicion + objBEEstructuraArchivo.Longitud;
                nColumna++;
            }

            return lstCampos;
        }

        private Boolean ValidarValorFijo(String[] pcArrValores, String pcValor)
        {
            Boolean bResult = false;
            Int32 nIndex = 0;

            while (nIndex < pcArrValores.Length)
            {
                if (bResult != true)
                {
                    if (pcArrValores[nIndex].Trim().ToUpper() == pcValor.Trim().ToUpper())
                    {
                        bResult = true;
                    }
                }

                nIndex++;
            }

            return bResult;
        }

        public Boolean ValidarCampoObligatorio(List<BEEstructuraArchivo> lstBEEstructuraArchivo, Int32 pnFila, List<String> pLstCFilaTrama, ref List<String> pLstError)
        {
            Boolean bResult = true;
            String cError = String.Empty;

            foreach (BEEstructuraArchivo objBEEstructuraArchivo in lstBEEstructuraArchivo)
            {
                if (objBEEstructuraArchivo.Obligatorio == true)
                {
                    if (pLstCFilaTrama[objBEEstructuraArchivo.Orden].Trim() == String.Empty)
                    {
                        bResult = false;
                        cError = "Fila nro. " + pnFila.ToString() + " - El campo " + objBEEstructuraArchivo.Nombre + " es obligatorio.";
                        pLstError.Add(cError);
                    }
                }
            }

            return bResult;
        }

        public Boolean ValidarCampoTipoDato(List<BEEstructuraArchivo> lstBEEstructuraArchivo, Int32 pnFila, DbDataReader pDr, ref List<String> pLstError)
        {
            Boolean bResult = true;
            String cError = String.Empty;

            foreach (BEEstructuraArchivo objBEEstructuraArchivo in lstBEEstructuraArchivo)
            {
                //Si campo es obligatorio
                if (objBEEstructuraArchivo.Obligatorio == true)
                {
                    //Si tipo de dato es Number
                    if (objBEEstructuraArchivo.TipoDato == "N")
                    {
                        Decimal nValor;

                        if (Decimal.TryParse(pDr.GetValue(objBEEstructuraArchivo.Orden).ToString().Trim(), out nValor) != true)
                        {
                            bResult = false;
                            cError = "Fila nro. " + (pnFila + 1).ToString() + " - El campo " + objBEEstructuraArchivo.Nombre + " debe ser de tipo numérico.";
                            pLstError.Add(cError);
                        }
                    }

                    //Si tipo de dato es Date
                    if (objBEEstructuraArchivo.TipoDato == "D")
                    {
                        DateTime dValor;

                        if (DateTime.TryParse(pDr.GetValue(objBEEstructuraArchivo.Orden).ToString().Trim(), out dValor) != true)
                        {
                            bResult = false;
                            cError = "Fila nro. " + (pnFila + 1).ToString() + " - El campo " + objBEEstructuraArchivo.Nombre + " debe ser de tipo fecha.";
                            pLstError.Add(cError);
                        }
                    }

                    //Si tipo de dato es String
                    if (objBEEstructuraArchivo.TipoDato == "S")
                    {
                        if (objBEEstructuraArchivo.ValorFijo != NullTypes.CadenaNull)
                        {
                            if (this.ObtenerNroValoresFijos(objBEEstructuraArchivo.ValorFijo) > 0)
                            {
                                String[] arrcValores = objBEEstructuraArchivo.ValorFijo.Split(',');

                                if (!ValidarValorFijo(arrcValores, pDr.GetValue(objBEEstructuraArchivo.Orden).ToString()))
                                {
                                    bResult = false;
                                    cError = "Fila nro. " + (pnFila + 1).ToString() + " - El campo " + objBEEstructuraArchivo.Nombre + " debe contener el siguientes valores: " + objBEEstructuraArchivo.ValorFijo;
                                    pLstError.Add(cError);
                                }
                            }
                            else
                            {
                                if (pDr.GetValue(objBEEstructuraArchivo.Orden).ToString().Trim().ToUpper() != objBEEstructuraArchivo.ValorFijo.Trim().ToUpper())
                                {
                                    bResult = false;
                                    cError = "Fila nro. " + (pnFila + 1).ToString() + " - El campo " + objBEEstructuraArchivo.Nombre + " debe contener el siguiente valor: " + objBEEstructuraArchivo.ValorFijo;
                                    pLstError.Add(cError);
                                }
                            }
                        }
                    }
                }
            }

            return bResult;
        }

        public Boolean ValidarCampoTipoDato(List<BEEstructuraArchivo> lstBEEstructuraArchivo, Int32 pnFila, List<String> pLstCFilaTrama, ref List<String> pLstError)
        {
            Boolean bResult = true;
            String cError = String.Empty;

            foreach (BEEstructuraArchivo objBEEstructuraArchivo in lstBEEstructuraArchivo)
            {
                if (objBEEstructuraArchivo.Obligatorio == true)
                {
                    //Si tipo de dato es Number
                    if (objBEEstructuraArchivo.TipoDato == "N")
                    {
                        Decimal nValor;

                        if (Decimal.TryParse(pLstCFilaTrama[objBEEstructuraArchivo.Orden].ToString().Trim(), out nValor) != true)
                        {
                            bResult = false;
                            cError = "Fila nro. " + pnFila.ToString() + " - El campo " + objBEEstructuraArchivo.Nombre + " debe ser de tipo numérico.";
                            pLstError.Add(cError);
                        }
                    }

                    //Si tipo de dato es Date
                    if (objBEEstructuraArchivo.TipoDato == "D")
                    {
                        String cValor = pLstCFilaTrama[objBEEstructuraArchivo.Orden].ToString().Trim();

                        if (cValor.Length != 8)
                        {
                            bResult = false;
                            cError = "Fila nro. " + pnFila.ToString() + " - El campo " + objBEEstructuraArchivo.Nombre + " debe ser de formato AAAAMMDD.";
                            pLstError.Add(cError);
                        }
                        else
                        {
                            DateTime dValor;
                            String cYY = cValor.Substring(0, 4);
                            String cMM = cValor.Substring(4, 2);
                            String cDD = cValor.Substring(6, 2);

                            if (DateTime.TryParse(cDD + "/" + cMM + "/" + cDD, out dValor) != true)
                            {
                                bResult = false;
                                cError = "Fila nro. " + pnFila.ToString() + " - El campo " + objBEEstructuraArchivo.Nombre + " debe ser de tipo fecha.";
                                pLstError.Add(cError);
                            }
                        }
                    }

                    //Si tipo de dato es String
                    if (objBEEstructuraArchivo.TipoDato == "S")
                    {
                        if (objBEEstructuraArchivo.ValorFijo != NullTypes.CadenaNull)
                        {
                            if (this.ObtenerNroValoresFijos(objBEEstructuraArchivo.ValorFijo) > 0)
                            {
                                String[] arrcValores = objBEEstructuraArchivo.ValorFijo.Split(',');

                                if (!ValidarValorFijo(arrcValores, pLstCFilaTrama[objBEEstructuraArchivo.Orden].ToString().Trim()))
                                {
                                    bResult = false;
                                    cError = "Fila nro. " + pnFila.ToString() + " - El campo " + objBEEstructuraArchivo.Nombre + " debe contener el siguientes valores: " + objBEEstructuraArchivo.ValorFijo;
                                    pLstError.Add(cError);
                                }
                            }
                            else
                            {
                                if (pLstCFilaTrama[objBEEstructuraArchivo.Orden].ToString().Trim().ToUpper() != objBEEstructuraArchivo.ValorFijo.Trim().ToUpper())
                                {
                                    bResult = false;
                                    cError = "Fila nro. " + pnFila.ToString() + " - El campo " + objBEEstructuraArchivo.Nombre + " debe contener el siguiente valor: " + objBEEstructuraArchivo.ValorFijo;
                                    pLstError.Add(cError);
                                }
                            }
                        }
                    }
                }
            }

            return bResult;
        }

        public Int32 ObtenerNroValoresFijos(String pcCadena)
        {
            Int32 nResult = 0;

            if (pcCadena.Length > 1)
            {
                Int32 nIndex = 0;

                while (nIndex <= pcCadena.Length - 1)
                {
                    if (pcCadena[nIndex] == ',')
                    {
                        nResult++;
                    }

                    nIndex++;
                }
            }

            return nResult;
        }

        public Boolean ValidarOrdenCampos(List<BEEstructuraArchivo> lstBEEstructuraArchivo, DbDataReader pDr, ref List<String> pLstError)
        {
            Boolean bResult = true;
            String cError = String.Empty;

            foreach (BEEstructuraArchivo objBEEstructuraArchivo in lstBEEstructuraArchivo)
            {
                if (LImpiarCaracter(pDr.GetName(objBEEstructuraArchivo.Orden).ToString().Trim().ToUpper()) != LImpiarCaracter(objBEEstructuraArchivo.Nombre.Trim().ToUpper()))
                {
                    bResult = false;
                    cError = "El nombre de la columna " + (objBEEstructuraArchivo.Orden + 1).ToString()  + " debe ser: " + objBEEstructuraArchivo.Nombre;
                    pLstError.Add(cError);
                }
            }
            return bResult;
        }

        public Boolean ValidarCampoObligatorio(List<BEEstructuraArchivo> lstBEEstructuraArchivo, Int32 pnFila, DbDataReader pDr, ref List<String> pLstError)
        {
            Boolean bResult = true;
            String cError = String.Empty;

            foreach (BEEstructuraArchivo objBEEstructuraArchivo in lstBEEstructuraArchivo)
            {
                if (objBEEstructuraArchivo.Obligatorio == true)
                {
                    if (pDr.GetValue(objBEEstructuraArchivo.Orden) == DBNull.Value)
                    {
                        bResult = false;
                        cError = "Fila nro. " + (pnFila + 1).ToString() + " - El campo " + objBEEstructuraArchivo.Nombre + " es obligatorio.";
                        pLstError.Add(cError);
                    }
                }
            }

            return bResult;
        }       

        public String LImpiarCaracter(String pcValor) 
        {
            String cCadena = String.Empty;
            cCadena = pcValor.Replace("#", "");
            cCadena = cCadena.Replace(".", "");
            cCadena = cCadena.Replace("/", "");
            return cCadena; 
        }

        public Boolean ValidarCampoValores(Int32 pnFila, DbDataReader pDr, ref List<String> pLstError)
        {
            Boolean bResult = true;
            String cError = String.Empty;

            Int32 nNroMotor = pDr.GetOrdinal("NRO_MOTOR");
            String cNroMotor = pDr.GetValue(nNroMotor).ToString();

            Int32 nMontSin1Anio = pDr.GetOrdinal("SINIESTROS_ANIO1");
            Int32 nMontSin2Anio = pDr.GetOrdinal("SINIESTROS_ANIO2");
            Int32 nMontSin3Anio = pDr.GetOrdinal("SINIESTROS_ANIO3");
            Int32 nMontSin4Anio = pDr.GetOrdinal("SINIESTROS_ANIO4");
            Int32 nMontSin5Anio = pDr.GetOrdinal("SINIESTROS_ANIO5");

            Int32 nCantSin1Anio = pDr.GetOrdinal("NRO_SINIESTROS_ANIO1");
            Int32 nCantSin2Anio = pDr.GetOrdinal("NRO_SINIESTROS_ANIO2");
            Int32 nCantSin3Anio = pDr.GetOrdinal("NRO_SINIESTROS_ANIO3");
            Int32 nCantSin4Anio = pDr.GetOrdinal("NRO_SINIESTROS_ANIO4");
            Int32 nCantSin5Anio = pDr.GetOrdinal("NRO_SINIESTROS_ANIO5");

            //Año 1
            if (Convert.ToDecimal(pDr.GetValue(nCantSin1Anio)) > 0 && Convert.ToDecimal(pDr.GetValue(nMontSin1Anio)) == 0)
            {
                cError = "Fila nro. " + pnFila + " - Error en monto de siniestro de año 1 nro: " + cNroMotor;
                pLstError.Add(cError);
                bResult = false;
            }
            if (Convert.ToDecimal(pDr.GetValue(nCantSin1Anio)) == 0 && Convert.ToDecimal(pDr.GetValue(nMontSin1Anio)) > 0)
            {
                cError = "Fila nro. " + pnFila + " - Error en canidad de siniestro de año 1 nro: " + cNroMotor;
                pLstError.Add(cError);
                bResult = false;
            }

            //Año 2
            if (Convert.ToDecimal(pDr.GetValue(nCantSin2Anio)) > 0 && Convert.ToDecimal(pDr.GetValue(nMontSin2Anio)) == 0)
            {
                cError = "Fila nro. " + pnFila + " - Error en monto de siniestro del año 2 nro: " + cNroMotor;
                pLstError.Add(cError);
                bResult = false;
            }
            if (Convert.ToDecimal(pDr.GetValue(nCantSin2Anio)) == 0 && Convert.ToDecimal(pDr.GetValue(nMontSin2Anio)) > 0)
            {
                cError = "Fila nro. " + pnFila + " - Error en canidad de siniestro del año 2 nro: " + cNroMotor;
                pLstError.Add(cError);
                bResult = false;
            }

            //Año 3
            if (Convert.ToDecimal(pDr.GetValue(nCantSin3Anio)) > 0 && Convert.ToDecimal(pDr.GetValue(nMontSin3Anio)) == 0)
            {
                cError = "Fila nro. " + pnFila + " - Error en monto de siniestro del año 3 nro: " + cNroMotor;
                pLstError.Add(cError);
                bResult = false;
            }
            if (Convert.ToDecimal(pDr.GetValue(nCantSin3Anio)) == 0 && Convert.ToDecimal(pDr.GetValue(nMontSin3Anio)) > 0)
            {
                cError = "Fila nro. " + pnFila + " - Error en canidad de siniestro del año 3 nro: " + cNroMotor;
                pLstError.Add(cError);
                bResult = false;
            }

            //Año 4
            if (Convert.ToDecimal(pDr.GetValue(nCantSin4Anio)) > 0 && Convert.ToDecimal(pDr.GetValue(nMontSin4Anio)) == 0)
            {
                cError = "Fila nro. " + pnFila + " - Error en monto de siniestro del año 4 nro: " + cNroMotor;
                pLstError.Add(cError);
                bResult = false;
            }
            if (Convert.ToDecimal(pDr.GetValue(nCantSin4Anio)) == 0 && Convert.ToDecimal(pDr.GetValue(nMontSin4Anio)) > 0)
            {
                cError = "Fila nro. " + pnFila + " - Error en canidad de siniestro del año 4 nro: " + cNroMotor;
                pLstError.Add(cError);
                bResult = false;
            }

            //Año 5
            if (Convert.ToDecimal(pDr.GetValue(nCantSin5Anio)) > 0 && Convert.ToDecimal(pDr.GetValue(nMontSin5Anio)) == 0)
            {
                cError = "Fila nro. " + pnFila + " - Error en monto de siniestro del año 5 nro: " + cNroMotor;
                pLstError.Add(cError);
                bResult = false;
            }
            if (Convert.ToDecimal(pDr.GetValue(nCantSin5Anio)) == 0 && Convert.ToDecimal(pDr.GetValue(nMontSin5Anio)) > 0)
            {
                cError = "Fila nro. " + pnFila + " - Error en canidad de siniestro del año 5 nro: " + cNroMotor;
                pLstError.Add(cError);
                bResult = false;
            }

            return bResult;
        }

        public String ObtenerEquivalencia(String pcTipDato, Int32 pnIdTipoProceso, Int32 pnIdTipoArchivo, Int32 pnOrden, String pcValor) 
        {
            String cValor = String.Empty;

            BLValorEquivalencia objBLValorEquivalencia = new BLValorEquivalencia();
            BEValorEquivalencia objBEValorEquivalencia = objBLValorEquivalencia.Obtener(pnIdTipoProceso, pnIdTipoArchivo, pnOrden, pcValor);

            if (objBEValorEquivalencia != null)
            {
                cValor = objBEValorEquivalencia.Equivalencia; 
            }
            else 
            {
                if (pcTipDato == TipoDato.Numero)
                {
                    cValor = NullTypes.DecimalNull.ToString();
                }
                if (pcTipDato == TipoDato.Cadena)
                {
                    cValor = NullTypes.CadenaNull;
                }
                if (pcTipDato == TipoDato.Fecha)
                {
                    cValor = NullTypes.FechaNull.ToString();
                }
            }

            return cValor;
        }
        #endregion

        #region Transaccional
        public Int32 ActualizarLog(BEProceso pObjBEProceso)
        {
            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAProceso objDAProceso = new DAProceso();
                nResult = objDAProceso.ActualizarLog(pObjBEProceso, sqlCn);
            }

            return nResult;
        }
        #endregion
    }
}
