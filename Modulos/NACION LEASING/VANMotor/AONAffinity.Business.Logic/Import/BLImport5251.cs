﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.BusinessLogic.Import
{
    public class BLImport5251 : BLImport 
    {
        private Int32 nIdProducto = 5251;

        #region Import Clientes BBVA .txt 
        public void CargarClientesTxtBBVA(BETipoProceso pObjBETipoProceso, BEProceso pObjBEProceso, BETipoArchivo pObjBETipoArchivo,  String pcUsuario)
        {
            Int32 nObservaciones = 0;
            String cLog = String.Empty;
            List<String> lstCLog = new List<String>();
            Boolean bEstructura = true;
            String cArchivoLog = "Log_" + pObjBEProceso.Archivo;

            if (pObjBETipoProceso.LongitudTrama == NullTypes.IntegerNull)
            {
                bEstructura = false;
                cLog = "No está configurada la longitud de la trama.";
                lstCLog.Add(cLog);
                nObservaciones++;
            }

            BLEstructuraArchivo objBLEstructuraArchivo = new BLEstructuraArchivo();
            List<BEEstructuraArchivo> lstBEEstructuraArchivo = objBLEstructuraArchivo.Listar(pObjBETipoProceso.IdTipoProceso, pObjBETipoArchivo.IdTipoArchivo, true);

            BLProducto objBLProducto = new BLProducto();
            BEProducto objBEProducto = objBLProducto.Obtener(this.nIdProducto);

            if (lstBEEstructuraArchivo == null)
            {
                bEstructura = false;
                cLog = "No existe ninguna estructura configurada para el archivo.";
                lstCLog.Add(cLog);
                nObservaciones++;
            }

            FileStream fsArchivo = new FileStream(pObjBETipoProceso.RutaArchivo + pObjBEProceso.Archivo, FileMode.Open);
            Int32 nFila = 0;

            //Si estructura no es válida.
            if (!bEstructura)
            {
                this.EscribirErrores(lstCLog, pObjBETipoProceso.RutaArchivoLog, cArchivoLog);

                pObjBEProceso.Procesado = true;
                pObjBEProceso.UsuarioModificacion = pcUsuario;
                pObjBEProceso.ArchivoLog = cArchivoLog;
                pObjBEProceso.RegTotal = 0;
                pObjBEProceso.RegTotalVal = 0;
                pObjBEProceso.RegTotalErr = 0;
                pObjBEProceso.NroObservaciones = nObservaciones;
                this.ActualizarLog(pObjBEProceso);

                return;
            }

            BLCliente objBLCliente = new BLCliente();
            Boolean bValido = true;
            Int32 nValidos = 0;

            using (StreamReader stream = new StreamReader(fsArchivo, GetFileEncoding(pObjBETipoProceso.RutaArchivo + pObjBEProceso.Archivo, fsArchivo)))
            {
                String cTrama;
                while (((cTrama = stream.ReadLine()) != null))
                {
                    bValido = true;

                    //Validar sio longitud de trama es válida.
                    if (cTrama.Length != pObjBETipoProceso.LongitudTrama)
                    {
                        bValido = false;
                        cLog = "Fila nro. " + nFila + " - el tamaño de la trama es diferente a la configurada.";
                        lstCLog.Add(cLog);
                    }

                    //Si filtro es válido
                    if (bValido == true)
                    {
                        List<String> lstCCampos = this.TramaSplit(cTrama, lstBEEstructuraArchivo);

                        if (!this.ValidarCampoObligatorio(lstBEEstructuraArchivo, nFila, lstCCampos, ref lstCLog))
                        {
                            bValido = false;
                        }

                        if (!ValidarCampoTipoDato(lstBEEstructuraArchivo, nFila, lstCCampos, ref  lstCLog))
                        {
                            bValido = false;
                        }

                        if (bValido == true)
                        {
                            String cNroMotor = String.Empty;
                            String cCodExtModelo = String.Empty;
                            String cCodExtMarca = String.Empty;
                            String cCodDep = String.Empty;
                            String cCodProv = String.Empty;
                            String cCodDist = String.Empty;
                            String cCodTipoReg = String.Empty;
                            DateTime dFecInicio = NullTypes.FechaNull;
                            DateTime dFecFin = NullTypes.FechaNull;
                            Int32 nAnioFab = NullTypes.IntegerNull;
                            String cTelefono1 = String.Empty;
                            Decimal nValorVehiculo = NullTypes.DecimalNull; 

                            foreach (BEEstructuraArchivo objBEEstructuraArchivo in lstBEEstructuraArchivo)
                            {
                                if (objBEEstructuraArchivo.Nombre.ToUpper() == ("NumeroMotor").ToUpper())
                                {
                                    cNroMotor = lstCCampos[objBEEstructuraArchivo.Orden].ToString();
                                }
                                if (objBEEstructuraArchivo.Nombre.ToUpper() == ("TipoRegistro").ToUpper())
                                {
                                    cCodTipoReg = lstCCampos[objBEEstructuraArchivo.Orden].ToString().Trim();
                                }
                                if (objBEEstructuraArchivo.Nombre.ToUpper() == ("Marca").ToUpper())
                                {
                                    cCodExtMarca = lstCCampos[objBEEstructuraArchivo.Orden].ToString().Trim();
                                }
                                if (objBEEstructuraArchivo.Nombre.ToUpper() == ("Modelo").ToUpper())
                                {
                                    cCodExtModelo = lstCCampos[objBEEstructuraArchivo.Orden].ToString().Trim();
                                }
                                if (objBEEstructuraArchivo.Nombre.ToUpper() == ("CodDepartamento").ToUpper())
                                {
                                    cCodDep = lstCCampos[objBEEstructuraArchivo.Orden].ToString().Trim();
                                }
                                if (objBEEstructuraArchivo.Nombre.ToUpper() == ("CodProvincia").ToUpper())
                                {
                                    cCodProv = lstCCampos[objBEEstructuraArchivo.Orden].ToString().Trim();
                                }
                                if (objBEEstructuraArchivo.Nombre.ToUpper() == ("CodDistrito").ToUpper())
                                {
                                    cCodDist = lstCCampos[objBEEstructuraArchivo.Orden].ToString().Trim();
                                }
                                if (objBEEstructuraArchivo.Nombre.ToUpper() == ("FechaInicioSeguroCredito").ToUpper())
                                {
                                    String cYY = lstCCampos[objBEEstructuraArchivo.Orden].Substring(0, 4);
                                    String cMM = lstCCampos[objBEEstructuraArchivo.Orden].Substring(4, 2);
                                    String cDD = lstCCampos[objBEEstructuraArchivo.Orden].Substring(6, 2);
                                    dFecInicio = Convert.ToDateTime(cDD + "/" + cMM + "/" + cYY);
                                }
                                if (objBEEstructuraArchivo.Nombre.ToUpper() == ("FechaFinSeguroCredito").ToUpper())
                                {
                                    String cYY = lstCCampos[objBEEstructuraArchivo.Orden].Substring(0, 4);
                                    String cMM = lstCCampos[objBEEstructuraArchivo.Orden].Substring(4, 2);
                                    String cDD = lstCCampos[objBEEstructuraArchivo.Orden].Substring(6, 2);
                                    dFecFin = Convert.ToDateTime(cDD + "/" + cMM + "/" + cYY);
                                }
                                if (objBEEstructuraArchivo.Nombre.ToUpper() == ("AnioFabricacion").ToUpper())
                                {
                                    nAnioFab = Convert.ToInt32(lstCCampos[objBEEstructuraArchivo.Orden]);
                                }
                                if (objBEEstructuraArchivo.Nombre.ToUpper().Trim() == ("NumeroTelefono").ToUpper().Trim())
                                {
                                    cTelefono1 = lstCCampos[objBEEstructuraArchivo.Orden].ToString();
                                }

                                if (objBEEstructuraArchivo.Nombre.ToUpper() == ("ValorVehiculo").ToUpper())
                                {
                                    String cEntero = lstCCampos[objBEEstructuraArchivo.Orden].Substring(0, 13);
                                    String cDecimal = lstCCampos[objBEEstructuraArchivo.Orden].Substring(13, 2);
                                    nValorVehiculo = Convert.ToDecimal((cEntero + "." + cDecimal).Trim());
                                }
                            }

                            BLModelo objBLModelo = new BLModelo();
                            BEModelo objBEModelo = objBLModelo.ObtenerPorCodExterno(cCodExtModelo, cCodExtMarca, true, true, NullTypes.CadenaNull);

                            if (objBEModelo == null)
                            {
                                bValido = false;
                                nObservaciones++;
                                cLog = "Fila nro. " + nFila + " - No está registrado el modelo del vehículo con código " + cCodExtModelo + " para el cliente con nro. de motor: " + cNroMotor;                                
                                lstCLog.Add(cLog);
                            }
                            if (objBEModelo != null)
                            {
                                if (!objBEModelo.Asegurable)
                                {
                                    bValido = false;
                                    nObservaciones++;
                                    cLog = "Fila nro. " + nFila + " - El modelo del vehículo con código " + cCodExtModelo + " no es asegurable, para el cliente con nro. de motor: " + cNroMotor;
                                    lstCLog.Add(cLog);
                                }
                            }
                            if (dFecInicio.Year < nAnioFab)
                            {
                                bValido = false;
                                nObservaciones++;
                                cLog = "Fila nro. " + nFila + " - La fecha de inicio de vigencia es menor que el año de fabricación del vehículo, para el cliente con nro. de motor: " + cNroMotor;
                                lstCLog.Add(cLog);
                            }

                            if (dFecInicio.Year > dFecFin.Year)
                            {
                                bValido = false;
                                nObservaciones++;
                                cLog = "Fila nro. " + nFila + " - La fecha de inicio de vigencia es mayor a la fecha de fin de vigencia, para el cliente con nro. de motor: " + cNroMotor;
                                lstCLog.Add(cLog);
                            }

                            if (cTelefono1 == "0000000000" || cTelefono1.Trim() == "0" || cTelefono1 == String.Empty)
                            {
                                bValido = false;
                                nObservaciones++;
                                cLog = "Fila nro. " + nFila + " - El telefóno es incorrecto, para el cliente con nro. de motor: " + cNroMotor;
                                lstCLog.Add(cLog);
                            }

                            if (nValorVehiculo <= 0) 
                            {
                                bValido = false;
                                nObservaciones++;
                                cLog = "Fila nro. " + nFila + " - El valor del vehículo es incorrecto, es menor igual a 0 para el cliente con nro. de motor: " + cNroMotor;
                                lstCLog.Add(cLog);
                            }

                            Int32 nIdCidad;
                            if (cCodDep.Trim() + cCodProv.Trim() + cCodDist.Trim() == String.Empty)
                            {
                                nIdCidad = 511501001;
                            }
                            else
                            {
                                nIdCidad = Convert.ToInt32("51" + cCodDep + cCodProv + cCodDist);
                            }

                            if (bValido == true)
                            {
                                BECliente objBECliente = new BECliente();
                                foreach (BEEstructuraArchivo objBEEstructuraArchivo in lstBEEstructuraArchivo)
                                {
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("ApellidoPaterno").ToUpper()) { objBECliente.ApePaterno = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper(); }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("ApellidoMaterno").ToUpper()) { objBECliente.ApeMaterno = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper(); }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("Nombres").ToUpper()) { objBECliente.PriNombre = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper(); }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("DireccionDomiciliaria").ToUpper()) { objBECliente.Direccion = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper(); }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("MzLteNro").ToUpper()) { objBECliente.Direccion = lstCCampos[objBEEstructuraArchivo.Orden] + " " + objBECliente.Direccion.Trim().ToUpper(); }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("NumeroTelefono").ToUpper())
                                    {
                                        objBECliente.TelDomicilio1 = lstCCampos[objBEEstructuraArchivo.Orden].Trim();
                                    }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("NumeroTelefono2").ToUpper()) { objBECliente.TelMovil1 = lstCCampos[objBEEstructuraArchivo.Orden].Trim(); }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("NumeroTelefono3").ToUpper()) { objBECliente.TelOficina1 = lstCCampos[objBEEstructuraArchivo.Orden].Trim(); }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("CorreoElectronico").ToUpper()) { objBECliente.Email1 = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToLower(); }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("Color").ToUpper()) { objBECliente.Color = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper(); }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("NumeroChasis").ToUpper()) { objBECliente.NroSerie = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper(); }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("NumeroMotor").ToUpper()) { objBECliente.NroMotor = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper(); }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("NumeroMatriculaPlaca").ToUpper()) { objBECliente.NroPlaca = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper(); }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("IndicadorTimonCambiado").ToUpper())
                                    {
                                        if (lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper() == "S") { objBECliente.EsTimonCambiado = true; } else { objBECliente.EsTimonCambiado = false; }
                                    }
                                }

                                objBECliente.ValorVehiculo = nValorVehiculo;
                                objBECliente.IdModelo = objBEModelo.IdModelo;
                                objBECliente.IdMarca = objBEModelo.IdMarca;
                                objBECliente.NroCertificado = NullTypes.CadenaNull;
                                objBECliente.CodAsegurador = 5;
                                objBECliente.SegNombre = NullTypes.CadenaNull;
                                if (cCodTipoReg == "01") { objBECliente.IdTipoDocumento = "L"; } else { objBECliente.IdTipoDocumento = "R"; }
                                objBECliente.NroDocumento = String.Empty;
                                objBECliente.Sexo = NullTypes.CadenaNull;
                                objBECliente.FecNacimiento = NullTypes.FechaNull;
                                objBECliente.idCiudad = nIdCidad;
                                objBECliente.FecVigencia = dFecInicio;
                                objBECliente.FecFinVigencia = dFecFin;
                                objBECliente.TelDomicilio2 = String.Empty;
                                objBECliente.TelDomicilio3 = String.Empty;
                                objBECliente.TelOficina2 = String.Empty;
                                objBECliente.TelOficina3 = String.Empty;
                                objBECliente.TelMovil2 = String.Empty;
                                objBECliente.TelMovil3 = String.Empty;
                                objBECliente.Email1 = String.Empty;
                                objBECliente.Email2 = String.Empty;
                                objBECliente.AnioFab = nAnioFab;
                                objBECliente.IdUsoVehiculo = 1;
                                objBECliente.IdClase = objBEModelo.IdClase;
                                objBECliente.NroAsientos = NullTypes.IntegerNull;
                                objBECliente.EsBlindado = false;
                                objBECliente.ReqGPS = false;
                                objBECliente.IdSponsor = 1; //BBVA 
                                objBECliente.IdProceso = pObjBEProceso.IdProceso;
                                objBECliente.IdEstadoCliente = 5;
                                objBECliente.UsuarioCreacion = pcUsuario;
                                objBECliente.EstadoRegistro = true;

                                if (ValidarCliente(objBEProducto, objBECliente, nFila, ref lstCLog, ref nObservaciones))
                                {
                                    if (objBLCliente.Insertar(objBECliente) > 0)
                                    {
                                        nValidos++;
                                        cLog = "Fila nro. " + nFila + " - Se registró el cliente con nro. de motor: " + objBECliente.NroMotor;
                                        lstCLog.Add(cLog);
                                    }
                                    else
                                    {
                                        nObservaciones++;
                                        cLog = "Fila nro. " + nFila + " - No se pudo registrar el cliente con nro. de motor: " + objBECliente.NroMotor;
                                        lstCLog.Add(cLog);
                                    }
                                }
                            }
                        }
                    }

                    nFila++;
                    fsArchivo.Flush();
                }

                fsArchivo.Close();
            }

            //Actualizar proceso            
            this.EscribirErrores(lstCLog, pObjBETipoProceso.RutaArchivoLog, cArchivoLog);

            pObjBEProceso.Procesado = true;
            pObjBEProceso.UsuarioModificacion = pcUsuario;
            pObjBEProceso.ArchivoLog = cArchivoLog;
            pObjBEProceso.RegTotal = nFila;
            pObjBEProceso.RegTotalVal = nValidos;
            pObjBEProceso.RegTotalErr = nFila - nValidos;
            pObjBEProceso.NroObservaciones = nObservaciones;

            this.ActualizarLog(pObjBEProceso);
        }
        #endregion 

        #region Import Clientes BBVA .xls
        /// <summary>
        /// Permite cargar la información de los clientes desde un archivo XLSX.
        /// </summary>
        /// <param name="pObjBETipoProceso">Objeto que contiene la información del tipo de proceso.</param>
        /// <param name="pObjBEProceso">Objeto que contiene la información del proceso.</param>
        /// <param name="pObjBETipoArchivo">Objeto que contiene la información del tipo de archivo.</param>
        /// <param name="pcUsuario">Código de ussuario quien ejecuta el proceso.</param>
        public void CargarClientesXlsBBVA(BETipoProceso pObjBETipoProceso, BEProceso pObjBEProceso, BETipoArchivo pObjBETipoArchivo,  String pcUsuario) 
        {
            String cArchivoLog = pObjBEProceso.Archivo;
            String[] cArrArchivoLog = cArchivoLog.Split('.');
            cArchivoLog = cArrArchivoLog[0] + "_.txt";
            cArchivoLog = "Log_" + cArchivoLog;

            Int32 nObservaciones = 0;
            String cLog = String.Empty;
            List<String> lstCLog = new List<String>();
            Boolean bEstructura = true;

            BLEstructuraArchivo objBLEstructuraArchivo = new BLEstructuraArchivo();
            List<BEEstructuraArchivo> lstBEEstructuraArchivo = objBLEstructuraArchivo.Listar(pObjBETipoProceso.IdTipoProceso, pObjBETipoArchivo.IdTipoArchivo, true);
            BLProducto objBLProducto = new BLProducto();
            BEProducto objBEProducto = objBLProducto.Obtener(this.nIdProducto);

            if (lstBEEstructuraArchivo == null)
            {
                bEstructura = false;
                nObservaciones++;
                cLog = "No existe ninguna estructura configurada para el archivo.";
                lstCLog.Add(cLog);
            }

            if (!bEstructura)
            {
                //Actualizar proceso            
                this.EscribirErrores(lstCLog, pObjBETipoProceso.RutaArchivoLog, cArchivoLog);
                pObjBEProceso.Procesado = true;
                pObjBEProceso.UsuarioModificacion = pcUsuario;
                pObjBEProceso.ArchivoLog = cArchivoLog;
                pObjBEProceso.RegTotal = 0;
                pObjBEProceso.RegTotalVal = 0;
                pObjBEProceso.RegTotalErr = 0;
                pObjBEProceso.NroObservaciones = nObservaciones;
                this.ActualizarLog(pObjBEProceso);
                return;
            }

            BLCliente objBLCliente = new BLCliente();

            BLArchivo objBLArchivo = new BLArchivo();
            String cConexion = String.Format(AppSettings.ProviderExcel, pObjBETipoProceso.RutaArchivo + pObjBEProceso.Archivo);
            OleDbConnection oleCn = new OleDbConnection(cConexion);
            oleCn.Open();
            OleDbDataReader oleDr = null;
                        
            oleDr = objBLArchivo.LeerExcel(oleCn);            

            Int32 nFila = 0;
            Boolean bValido = true;
            Int32 nValidos = 0;

            if (oleDr != null)
            {
                while (oleDr.Read())
                {
                    bValido = true;

                    if (!this.ValidarOrdenCampos(lstBEEstructuraArchivo, oleDr, ref lstCLog))
                    {
                        bValido = false;
                    }
                    if (!this.ValidarCampoObligatorio(lstBEEstructuraArchivo, nFila, oleDr, ref lstCLog))
                    {
                        bValido = false;
                    }

                    if (!this.ValidarCampoTipoDato(lstBEEstructuraArchivo, nFila, oleDr, ref lstCLog))
                    {
                        bValido = false;
                    }                    

                    //Insertar el registro
                    if (bValido == true) 
                    {
                        Int32[] nIndex = new Int32[lstBEEstructuraArchivo.Count];

                        foreach (BEEstructuraArchivo objBEEstructuraArchivo in lstBEEstructuraArchivo) 
                        {
                            nIndex[objBEEstructuraArchivo.Orden] = objBEEstructuraArchivo.Orden;
                        }
                         
                        BLModelo objBLModelo = new BLModelo();                        
                        BEModelo objBEModelo = objBLModelo.ObtenerPorCodExterno(oleDr.GetValue(nIndex[12]).ToString(), oleDr.GetValue(nIndex[10]).ToString(), true, true, NullTypes.CadenaNull);

                        if (objBEModelo == null)
                        {
                            bValido = false;
                            nObservaciones++;
                            cLog = "Fila nro. " + (nFila + 1).ToString() + " - No está registrado o el modelo no es asegurable, código modelo: " + oleDr.GetValue(nIndex[12]).ToString() + " para el cliente con nro. de motor: " + oleDr.GetValue(nIndex[17]).ToString();                            
                            lstCLog.Add(cLog);
                        }

                        if (objBEModelo != null)
                        {
                            //Validar si vehiculo es segurable.
                            if (!objBEModelo.Asegurable)
                            {
                                bValido = false;
                                nObservaciones++;
                                cLog = "Fila nro. " + (nFila + 1).ToString() + " - El modelo del vehículo con código " + oleDr.GetValue(nIndex[12]).ToString() + " no es asegurable, para el cliente con nro. de motor: " + oleDr.GetValue(nIndex[17]).ToString();
                                lstCLog.Add(cLog);
                            }

                            //Validar si la descripción de la marca del archivo es distinta a la configurada. 
                            if (objBEModelo.DesMarca != oleDr.GetValue(nIndex[11]).ToString())
                            {
                                nObservaciones++;
                                cLog = "Fila nro. " + (nFila + 1).ToString() + " - La descripción de la marca enviada en el archivo: " + oleDr.GetValue(nIndex[11]).ToString() + " es diferente al que está configurada en el sistema: " + objBEModelo.DesMarca + " para el cliente con nro. de motor: " + oleDr.GetValue(nIndex[17]).ToString();
                                lstCLog.Add(cLog);
                            }

                            //Validar si la descripción del modelo del archivo es distinta a la configurada. 
                            if (objBEModelo.Descripcion != oleDr.GetValue(nIndex[13]).ToString())
                            {
                                nObservaciones++;
                                cLog = "Fila nro. " + (nFila + 1).ToString() + " - La descripción del modelo enviado en el archivo: " + oleDr.GetValue(nIndex[13]).ToString() + " es diferente al que está configurado en el sistema: " + objBEModelo.Descripcion + " para el cliente con nro. de motor: " + oleDr.GetValue(nIndex[17]).ToString();
                                lstCLog.Add(cLog);
                            }
                        }

                        if (Convert.ToDateTime(oleDr.GetValue(nIndex[8])).Year < Convert.ToInt32(oleDr.GetValue(nIndex[15])))
                        {
                            bValido = false;
                            nObservaciones++;
                            cLog = "Fila nro. " + (nFila + 1).ToString() + " - La fecha de inicio de vigencia es menor que el año de fabricación del vehículo, para el cliente con nro. de motor: " + oleDr.GetValue(nIndex[17]).ToString();
                            lstCLog.Add(cLog);
                        }

                        if (Convert.ToDateTime(oleDr.GetValue(nIndex[8])).Year > Convert.ToDateTime(oleDr.GetValue(nIndex[9])).Year)
                        {
                            bValido = false;
                            nObservaciones++;
                            cLog = "Fila nro. " + (nFila + 1).ToString() + " - La fecha de inicio de vigencia es mayor a la fecha de fin de vigencia, para el cliente con nro. de motor: " + oleDr.GetValue(nIndex[17]).ToString();
                            lstCLog.Add(cLog);
                        }

                        if (oleDr.GetValue(nIndex[5]).ToString() == "0000000000" || oleDr.GetValue(nIndex[5]).ToString() == "0" || oleDr.GetValue(nIndex[5]).ToString() == String.Empty)
                        {
                            bValido = false;
                            nObservaciones++;
                            cLog = "Fila nro. " + (nFila + 1).ToString() + " - El telefóno es incorrecto, para el cliente con nro. de motor: " + oleDr.GetValue(nIndex[17]).ToString();
                            lstCLog.Add(cLog);
                        }

                        if (Convert.ToDecimal(oleDr.GetValue(nIndex[21])) <= 0) 
                        {
                            bValido = false;
                            nObservaciones++;
                            cLog = "Fila nro. " + (nFila + 1).ToString() + " - El valor del vehículo es incorrecto, es menor igual a 0 para el cliente con nro. de motor: " + oleDr.GetValue(nIndex[17]).ToString();
                            lstCLog.Add(cLog);
                        }

                        Int32 nIdCidad = 511501001;

                        if (bValido == true) 
                        {
                            BECliente objBECliente = new BECliente();

                            objBECliente.ApePaterno = oleDr.GetValue(nIndex[1]).ToString();  
                            objBECliente.ApeMaterno = oleDr.GetValue(nIndex[2]).ToString();  
                            objBECliente.PriNombre = oleDr.GetValue(nIndex[3]).ToString();
                            objBECliente.Direccion = String.Empty;
                            objBECliente.TelDomicilio1 = oleDr.GetValue(nIndex[5]).ToString();
                            objBECliente.TelMovil1 = oleDr.GetValue(nIndex[6]).ToString();
                            objBECliente.TelOficina1 = oleDr.GetValue(nIndex[7]).ToString();
                            objBECliente.Email1 = String.Empty;
                            objBECliente.Color = oleDr.GetValue(nIndex[14]).ToString();
                            objBECliente.NroSerie = oleDr.GetValue(nIndex[16]).ToString();   
                            objBECliente.NroMotor = oleDr.GetValue(nIndex[17]).ToString();
                            objBECliente.NroPlaca = oleDr.GetValue(nIndex[18]).ToString();
                            if (oleDr.GetValue(nIndex[20]).ToString() == "S") { objBECliente.EsTimonCambiado = true; } else { objBECliente.EsTimonCambiado = false; }
                            objBECliente.ValorVehiculo = Convert.ToDecimal(oleDr.GetValue(nIndex[21]));
                            objBECliente.IdModelo = objBEModelo.IdModelo;
                            objBECliente.IdMarca = objBEModelo.IdMarca;
                            objBECliente.NroCertificado = NullTypes.CadenaNull;
                            objBECliente.CodAsegurador = Asegurador.Rimac;
                            objBECliente.SegNombre = NullTypes.CadenaNull;
                            objBECliente.IdTipoDocumento = "L";
                            objBECliente.NroDocumento = String.Empty;
                            objBECliente.Sexo = NullTypes.CadenaNull;
                            objBECliente.FecNacimiento = NullTypes.FechaNull;
                            objBECliente.idCiudad = nIdCidad;
                            objBECliente.FecVigencia = Convert.ToDateTime(oleDr.GetValue(nIndex[8]));
                            objBECliente.FecFinVigencia = Convert.ToDateTime(oleDr.GetValue(nIndex[9]));
                            objBECliente.TelDomicilio2 = String.Empty;
                            objBECliente.TelDomicilio3 = String.Empty;
                            objBECliente.TelOficina2 = String.Empty;
                            objBECliente.TelOficina3 = String.Empty;
                            objBECliente.TelMovil2 = String.Empty;
                            objBECliente.TelMovil3 = String.Empty;
                            objBECliente.Email1 = String.Empty;
                            objBECliente.Email2 = String.Empty;
                            objBECliente.AnioFab = Convert.ToInt32(oleDr.GetValue(nIndex[15]));
                            objBECliente.IdUsoVehiculo = 1;
                            objBECliente.IdClase = objBEModelo.IdClase;
                            objBECliente.NroAsientos = NullTypes.IntegerNull;
                            objBECliente.EsBlindado = false;
                            objBECliente.ReqGPS = false;
                            objBECliente.IdSponsor = Convert.ToInt32(this.ObtenerEquivalencia(TipoDato.Numero, TipoProceso.ImportClienteBBVA, TipoArchivo.Excel, 0, oleDr.GetValue(nIndex[0]).ToString()));
                            objBECliente.IdProceso = pObjBEProceso.IdProceso;
                            objBECliente.IdEstadoCliente = EstadoCliente.NoCotizado;
                            objBECliente.UsuarioCreacion = pcUsuario;
                            objBECliente.EstadoRegistro = true;

                            if (ValidarCliente(objBEProducto, objBECliente, nFila, ref lstCLog, ref nObservaciones)) 
                            {
                                if (objBLCliente.Insertar(objBECliente) > 0)
                                {
                                    nValidos++;
                                    cLog = "Fila nro. " + (nFila + 1).ToString() + " - Se registró el cliente con nro. de motor: " + objBECliente.NroMotor;
                                    lstCLog.Add(cLog);
                                }
                                else
                                {
                                    nObservaciones++;
                                    cLog = "Fila nro. " + (nFila + 1).ToString()  + " - No se pudo registrar el cliente con nro. de motor: " + objBECliente.NroMotor;
                                    lstCLog.Add(cLog);
                                }
                            }
                        }
                        nFila++;
                    }
                }
            }

            oleCn.Close();

            //Actualizar proceso            
            this.EscribirErrores(lstCLog, pObjBETipoProceso.RutaArchivoLog, cArchivoLog);
            pObjBEProceso.Procesado = true;
            pObjBEProceso.UsuarioModificacion = pcUsuario;
            pObjBEProceso.ArchivoLog = cArchivoLog;
            pObjBEProceso.RegTotal = nFila;
            pObjBEProceso.RegTotalVal = nValidos;
            pObjBEProceso.RegTotalErr = nFila - nValidos;
            pObjBEProceso.NroObservaciones = nObservaciones;
            this.ActualizarLog(pObjBEProceso);
        }
        #endregion

        #region Validacion Cliente
        /// <summary>
        /// Permite validar la información del cliente.
        /// </summary>
        /// <param name="pObjBEProducto">Objeto que contiene la información del producto.</param>
        /// <param name="pObBEjCliente">Objeto que contien la información del cliente</param>
        /// <param name="pnFila">Nro. de fila de registro en el archivo.</param>
        /// <param name="pLstCLog">Lista log.</param>
        /// <param name="pnObservaciones">nro de observaciones.</param>
        /// <returns>True Información correcta | False información no correcta.</returns>
        public Boolean ValidarCliente(BEProducto pObjBEProducto, BECliente pObBEjCliente, Int32 pnFila, ref List<String> pLstCLog, ref Int32 pnObservaciones)
        {
            /*---------------------------------------------------------------------------
            * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
            * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Boolean bResult = true;
            BECliente objBECliente = new BECliente();
            String cLog = String.Empty;

            BLCliente objBLCliente = new BLCliente();
            BECliente objBECliente_Temp = new BECliente();

            //Obtener cliente por nro de motor.
            objBECliente = objBLCliente.Obtener(pObBEjCliente.NroMotor);

            //Si cliente ya esta registrado por nro de motor, escribir log.
            if (objBECliente != null)
            {
                cLog = "Fila nro. " + (pnFila + 1).ToString() + " - Ya se encuentra registrado el cliente con nro. de motor: " + pObBEjCliente.NroMotor;
                pLstCLog.Add(cLog);
                pnObservaciones++;
                bResult = false;
            }

            //Obtener cliente por nro de placa.
            objBECliente = objBLCliente.ObtenerxPlaca(pObBEjCliente.NroPlaca);

            //Si cliente ya está registrado por placa, escribir log.
            if (objBECliente != null)
            {
                cLog = "Fila nro. " + (pnFila + 1).ToString() + " - Ya se encuentra registrado el cliente con nro. de placa: " + pObBEjCliente.NroPlaca + " con nro. de motor: " + pObBEjCliente.NroMotor;
                pLstCLog.Add(cLog);
                pnObservaciones++;
                bResult = false;
            }

            //Obtener clientes por nro serie.
            objBECliente = objBLCliente.ObtenerxSerie(pObBEjCliente.NroSerie);
            if (objBECliente != null)
            {
                cLog = "Fila nro. " + (pnFila + 1).ToString() + " - Ya se encuentra registrado el cliente con nro. de serie: " + pObBEjCliente.NroSerie + " con nro. de motor: " + pObBEjCliente.NroMotor;
                pLstCLog.Add(cLog);
                pnObservaciones++;
                bResult = false;
            }

            //Validar si año de fab. es mayor al año actual.
            if (pObBEjCliente.AnioFab > DateTime.Now.Year)
            {
                cLog = "Fila nro. " + (pnFila + 1).ToString() + " - El año de fabricación del vehículo es incorrecto, es mayor al año actual, para el cliente con nro. de motor: " + pObBEjCliente.NroMotor;
                pLstCLog.Add(cLog);
                pnObservaciones++;
                bResult = false;
            }

            //Si existe información del producto.
            if (pObjBEProducto != null)
            {
                //Si existe maxima antiguedad.
                if (pObjBEProducto.MaxAntiguedad != NullTypes.IntegerNull)
                {
                    Int32 nAnioAct = DateTime.Now.Year;
                    Int32 nAntiguedad = nAnioAct - pObBEjCliente.AnioFab;

                    //Si la antiguedad del vehículo es mayor a la permitida.
                    if (nAntiguedad > pObjBEProducto.MaxAntiguedad)
                    {
                        cLog = "Fila nro. " + (pnFila + 1).ToString() + " - El vehículo no es asegurale porque antigüedad supera la máxima permitida. nro. de motor: " + pObBEjCliente.NroMotor;
                        pLstCLog.Add(cLog);
                        pnObservaciones++;
                        bResult = false;
                    }
                }

                //Si existe maximo valor de vehículo.
                if (pObjBEProducto.MaxValorVehiculo != NullTypes.DecimalNull)
                {
                    BLCotizacion objBLCotizacion = new BLCotizacion();
                    Decimal nValorVehiculoDep = objBLCotizacion.ObtenerValorDepresiado(pObBEjCliente.AnioFab, pObBEjCliente.ValorVehiculo, this.nIdProducto);

                    //Validar si el valor es mayor al maximo permitido.
                    if (nValorVehiculoDep > pObjBEProducto.MaxValorVehiculo)
                    {
                        cLog = "Fila nro. " + (pnFila + 1).ToString() + " - El vehículo no es asegurale porque valor del vehículo depresiado: " + Decimal.Round(nValorVehiculoDep, 2).ToString() + " supera al máximo permitido: " + Decimal.Round(pObjBEProducto.MaxValorVehiculo, 2).ToString() + " nro. de motor: " + pObBEjCliente.NroMotor;
                        pLstCLog.Add(cLog);
                        pnObservaciones++;
                        bResult = false;
                    }
                }

                BLCategoriaModelo objBLCategoriaModelo = new BLCategoriaModelo();
                String cRespuesta = objBLCategoriaModelo.ValidarVehiculo(this.nIdProducto, pObBEjCliente.IdModelo, pObBEjCliente.AnioFab);

                if (cRespuesta != String.Empty) 
                {
                    cLog = "Fila nro. " + (pnFila + 1).ToString() + " " + cRespuesta + " nro. de motor: " + pObBEjCliente.NroMotor;
                    pLstCLog.Add(cLog);
                    pnObservaciones++;
                    bResult = false;
                }
            }

            return bResult;
        }        
        #endregion

        #region Import Siniestros BBVA
        public void CargarSiniestrosBBVA(BETipoProceso pObjBETipoProceso, BEProceso pObjBEProceso, BETipoArchivo pObjBETipoArchivo,  String pcUsuario)
        {
            String cArchivoLog = pObjBEProceso.Archivo;
            //cArchivoLog = pObjBEProceso.Archivo.Replace(".xls", ".txt");
            //cArchivoLog = "Log_" + cArchivoLog;
            String[] cArrArchivoLog = cArchivoLog.Split('.');
            cArchivoLog = cArrArchivoLog[0] + ".txtx";
            cArchivoLog = "Log_" + cArchivoLog;

            Int32 nObservaciones = 0;
            String cLog = String.Empty;
            List<String> lstCLog = new List<String>();
            Boolean bEstructura = true;

            BLEstructuraArchivo objBLEstructuraArchivo = new BLEstructuraArchivo();
            List<BEEstructuraArchivo> lstBEEstructuraArchivo = objBLEstructuraArchivo.Listar(pObjBETipoProceso.IdTipoProceso,  pObjBETipoArchivo.IdTipoArchivo, true);

            if (lstBEEstructuraArchivo == null)
            {
                bEstructura = false;
                nObservaciones++;
                cLog = "No existe ninguna estructura configurada para el archivo.";
                lstCLog.Add(cLog);
            }

            if (!bEstructura)
            {
                //Actualizar proceso            
                this.EscribirErrores(lstCLog, pObjBETipoProceso.RutaArchivoLog, cArchivoLog);
                pObjBEProceso.Procesado = true;
                pObjBEProceso.UsuarioModificacion = pcUsuario;
                pObjBEProceso.ArchivoLog = cArchivoLog;
                pObjBEProceso.RegTotal = 0;
                pObjBEProceso.RegTotalVal = 0;
                pObjBEProceso.RegTotalErr = 0;
                pObjBEProceso.NroObservaciones = nObservaciones;
                this.ActualizarLog(pObjBEProceso);
                return;
            }

            BLArchivo objBLArchivo = new BLArchivo();
            String cConexion = String.Format(AppSettings.ProviderExcel, pObjBETipoProceso.RutaArchivo + pObjBEProceso.Archivo);
            OleDbConnection oleCn = new OleDbConnection(cConexion);
            oleCn.Open();
            OleDbDataReader oleDr = objBLArchivo.LeerExcel(oleCn);
            Int32 nFila = 0;
            Boolean bValido = true;
            Int32 nValidos = 0;

            if (oleDr != null)
            {
                if (oleDr.HasRows)
                {
                    while (oleDr.Read())
                    {
                        bValido = true;

                        if (!this.ValidarOrdenCampos(lstBEEstructuraArchivo, oleDr, ref lstCLog))
                        {
                            bValido = false;
                        }
                        if (!this.ValidarCampoObligatorio(lstBEEstructuraArchivo, nFila, oleDr, ref lstCLog))
                        {
                            bValido = false;
                        }

                        if (!this.ValidarCampoTipoDato(lstBEEstructuraArchivo, nFila, oleDr, ref lstCLog))
                        {
                            bValido = false;
                        }

                        //if (bValido == true) NO va
                        //{
                        //    if (!ValidarCampoValores(nFila, oleDr, ref lstCLog))
                        //    {
                        //        bValido = false;
                        //    }
                        //}

                        //Insertar el registro
                        if (bValido == true)
                        {
                            Int32[] nIndex = new Int32[lstBEEstructuraArchivo.Count];

                            foreach (BEEstructuraArchivo objBEEstructuraArchivo in lstBEEstructuraArchivo)
                            {
                                nIndex[objBEEstructuraArchivo.Orden] = objBEEstructuraArchivo.Orden;
                            }

                            //Int32 nIndex0 = oleDr.GetOrdinal("CANAL");
                            //Int32 nIndex1 = oleDr.GetOrdinal("POLIZA");
                            //Int32 nIndex2 = oleDr.GetOrdinal("PLACA");
                            //Int32 nIndex3 = oleDr.GetOrdinal("NRO_MOTOR");
                            //Int32 nIndex4 = oleDr.GetOrdinal("NRO_CERTIFICADO");
                            //Int32 nIndex5 = oleDr.GetOrdinal("CONTRATANTE");
                            //Int32 nIndex6 = oleDr.GetOrdinal("ID_CONTRATANTE");
                            //Int32 nIndex7 = oleDr.GetOrdinal("ID_ASEGURADO");
                            //Int32 nIndex8 = oleDr.GetOrdinal("ASEGURADO");
                            //Int32 nIndex9 = oleDr.GetOrdinal("MARCA_VEHICULOS");
                            //Int32 nIndex10 = oleDr.GetOrdinal("MODELO_VEHICULOS");
                            //Int32 nIndex11 = oleDr.GetOrdinal("ANO_FABRICACION");
                            //Int32 nIndex12 = oleDr.GetOrdinal("INICIO_VIGENCIA");
                            //Int32 nIndex13 = oleDr.GetOrdinal("FIN_VIGENCIA");
                            //Int32 nIndex14 = oleDr.GetOrdinal("SINIESTROS_ANIO1");
                            //Int32 nIndex15 = oleDr.GetOrdinal("NRO_SINIESTROS_ANIO1");
                            //Int32 nIndex16 = oleDr.GetOrdinal("SINIESTROS_ANIO2");
                            //Int32 nIndex17 = oleDr.GetOrdinal("NRO_SINIESTROS_ANIO2");
                            //Int32 nIndex18 = oleDr.GetOrdinal("SINIESTROS_ANIO3");
                            //Int32 nIndex19 = oleDr.GetOrdinal("NRO_SINIESTROS_ANIO3");
                            //Int32 nIndex20 = oleDr.GetOrdinal("SINIESTROS_ANIO4");
                            //Int32 nIndex21 = oleDr.GetOrdinal("NRO_SINIESTROS_ANIO4");
                            //Int32 nIndex22 = oleDr.GetOrdinal("SINIESTROS_ANIO5");
                            //Int32 nIndex23 = oleDr.GetOrdinal("NRO_SINIESTROS_ANIO5");
                            //Int32 nIndex24 = oleDr.GetOrdinal("IND_CLIENTE");

                            BLCliente objBLCliente = new BLCliente();
                            BECliente objBECliente = objBLCliente.Obtener(oleDr.GetValue(nIndex[3]).ToString());

                            //Si no se encuentra registrado el nro motor del cliente, agregar a log el detalle.
                            if (objBECliente == null)
                            {
                                cLog = "Fila nro. " + (nFila + 1).ToString() + " - No se encuentra registrado el cliente con nro de motor: " + oleDr.GetValue(nIndex[3]).ToString();
                                lstCLog.Add(cLog);
                                nObservaciones++;
                            }
                            else
                            {
                                //Si cliente no es valido para filtro de RIMAC, actualizar el estado del clie
                                if (oleDr.GetValue(nIndex[24]).ToString() == "N")
                                {
                                    if (objBLCliente.Atualizar(oleDr.GetValue(nIndex[0]).ToString(), 15, pcUsuario) > 0)
                                    {
                                        cLog = "Fila nro. " + (nFila + 1).ToString() + " - Se actualizó el estado del cliente como no cotizable por aseguradora, nro de motor: " + oleDr.GetValue(nIndex[3]).ToString();
                                        lstCLog.Add(cLog);
                                    }
                                    else
                                    {
                                        cLog = "Fila nro. " + (nFila + 1).ToString() + " - No se pudo actualizar el estado del cliente como no cotizable por aseguradora, nro de motor: " + oleDr.GetValue(nIndex[3]).ToString();
                                        lstCLog.Add(cLog);
                                        nObservaciones++;
                                    }
                                }

                                BLVigenciaSiniestro objBLVigenciaSiniestro = new BLVigenciaSiniestro();
                                BEVigenciaSiniestro objBEVigenciaSiniestro = objBLVigenciaSiniestro.Obtener(oleDr.GetValue(nIndex[3]).ToString());

                                //Si ya existe siniestro registrado, agregar a log el detalle. 
                                if (objBEVigenciaSiniestro != null)
                                {
                                    cLog = "Fila nro. " + (nFila + 1).ToString() + " - La información del siniestro ya se encuentra registrada, nro : " + oleDr.GetValue(nIndex[3]).ToString();
                                    lstCLog.Add(cLog);
                                    nObservaciones++;
                                }
                                else
                                {
                                    objBEVigenciaSiniestro = new BEVigenciaSiniestro();

                                    objBEVigenciaSiniestro.NroMotor = oleDr.GetString(nIndex[3]);
                                    objBEVigenciaSiniestro.CantSiniestro1Anio = Convert.ToInt32(oleDr.GetValue(nIndex[15]));
                                    if (oleDr.IsDBNull(nIndex[17])) { objBEVigenciaSiniestro.CantSiniestro2Anio = NullTypes.IntegerNull; } else { objBEVigenciaSiniestro.CantSiniestro2Anio = Convert.ToInt32(oleDr.GetValue(nIndex[17])); }
                                    if (oleDr.IsDBNull(nIndex[19])) { objBEVigenciaSiniestro.CantSiniestro3Anio = NullTypes.IntegerNull; } else { objBEVigenciaSiniestro.CantSiniestro3Anio = Convert.ToInt32(oleDr.GetValue(nIndex[19])); }
                                    if (oleDr.IsDBNull(nIndex[21])) { objBEVigenciaSiniestro.CantSiniestro4Anio = NullTypes.IntegerNull; } else { objBEVigenciaSiniestro.CantSiniestro4Anio = Convert.ToInt32(oleDr.GetValue(nIndex[21])); }
                                    if (oleDr.IsDBNull(nIndex[23])) { objBEVigenciaSiniestro.CantSiniestro5Anio = NullTypes.IntegerNull; } else { objBEVigenciaSiniestro.CantSiniestro5Anio = Convert.ToInt32(oleDr.GetValue(nIndex[23])); }
                                    objBEVigenciaSiniestro.MontoSiniestro1Anio = Convert.ToDecimal(oleDr.GetValue(nIndex[14]));
                                    if (oleDr.IsDBNull(nIndex[16])) { objBEVigenciaSiniestro.MontoSiniestro2Anio = NullTypes.DecimalNull; } else { objBEVigenciaSiniestro.MontoSiniestro2Anio = Convert.ToDecimal(oleDr.GetValue(nIndex[16])); }
                                    if (oleDr.IsDBNull(nIndex[18])) { objBEVigenciaSiniestro.MontoSiniestro3Anio = NullTypes.DecimalNull; } else { objBEVigenciaSiniestro.MontoSiniestro3Anio = Convert.ToDecimal(oleDr.GetValue(nIndex[18])); }
                                    if (oleDr.IsDBNull(nIndex[20])) { objBEVigenciaSiniestro.MontoSiniestro4Anio = NullTypes.DecimalNull; } else { objBEVigenciaSiniestro.MontoSiniestro4Anio = Convert.ToDecimal(oleDr.GetValue(nIndex[20])); }
                                    if (oleDr.IsDBNull(nIndex[22])) { objBEVigenciaSiniestro.MontoSiniestro5Anio = NullTypes.DecimalNull; } else { objBEVigenciaSiniestro.MontoSiniestro5Anio = Convert.ToDecimal(oleDr.GetValue(nIndex[22])); }
                                    objBEVigenciaSiniestro.FecIniVigencia = Convert.ToDateTime(oleDr.GetValue(nIndex[12]).ToString());
                                    objBEVigenciaSiniestro.FecFinVigencia = Convert.ToDateTime(oleDr.GetValue(nIndex[13]).ToString());
                                    objBEVigenciaSiniestro.UsuarioCreacion = pcUsuario;

                                    if (this.ValidarSiniestro(objBEVigenciaSiniestro, nFila, ref lstCLog) == true) 
                                    {
                                        if (objBLVigenciaSiniestro.Insertar(objBEVigenciaSiniestro) > 0)
                                        {
                                            cLog = "Fila nro. " + (nFila + 1).ToString() + " - Se registró la infomación del siniestro, nro. motor: " + oleDr.GetValue(nIndex[3]).ToString();
                                            lstCLog.Add(cLog);
                                            nValidos++;
                                        }
                                        else
                                        {
                                            cLog = "Fila nro. " + (nFila + 1).ToString() + " - No se pudo registrar la infomación del siniestro, nro motor: " + oleDr.GetValue(nIndex[3]).ToString();
                                            lstCLog.Add(cLog);
                                            nObservaciones++;
                                        }
                                    }                                    
                                }
                            }                            
                        }
                        nFila++;
                    }
                }
            }

            oleCn.Close();

            //Actualizar proceso            
            this.EscribirErrores(lstCLog, pObjBETipoProceso.RutaArchivoLog, cArchivoLog);
            pObjBEProceso.Procesado = true;
            pObjBEProceso.UsuarioModificacion = pcUsuario;
            pObjBEProceso.ArchivoLog = cArchivoLog;
            pObjBEProceso.RegTotal = nFila;
            pObjBEProceso.RegTotalVal = nValidos;
            pObjBEProceso.RegTotalErr = nFila - nValidos;
            pObjBEProceso.NroObservaciones = nObservaciones;
            this.ActualizarLog(pObjBEProceso);
        }

        public Boolean ValidarSiniestro(BEVigenciaSiniestro objBEVigenciaSiniestro, Int32 pnFila, ref List<String> pLstCLog) 
        {
            Boolean bResult = true;
            String cLog = String.Empty;

            if (objBEVigenciaSiniestro.CantSiniestro1Anio > objBEVigenciaSiniestro.MontoSiniestro1Anio) 
            {
                bResult = false;
                cLog = "Fila nro. " + (pnFila + 1).ToString() + " - La cantidad de siniestros es incorrecto par el 1er año para el cliente con nro de motor: " + objBEVigenciaSiniestro.NroMotor;
                pLstCLog.Add(cLog);
            }
            
            if (objBEVigenciaSiniestro.CantSiniestro2Anio > objBEVigenciaSiniestro.MontoSiniestro2Anio)
            {
                bResult = false;
                cLog = "Fila nro. " + (pnFila + 1).ToString() + " - La cantidad de siniestros es incorrecto par el 2do año para el cliente con nro de motor: " + objBEVigenciaSiniestro.NroMotor;
                pLstCLog.Add(cLog);
            }

            if (objBEVigenciaSiniestro.CantSiniestro3Anio > objBEVigenciaSiniestro.MontoSiniestro3Anio)
            {
                bResult = false;
                cLog = "Fila nro. " + (pnFila + 1).ToString() + " - La cantidad de siniestros es incorrecto par el 3er año para el cliente con nro de motor: " + objBEVigenciaSiniestro.NroMotor;
                pLstCLog.Add(cLog);
            }

            if (objBEVigenciaSiniestro.CantSiniestro4Anio > objBEVigenciaSiniestro.MontoSiniestro4Anio)
            {
                bResult = false;
                cLog = "Fila nro. " + (pnFila + 1).ToString() + " - La cantidad de siniestros es incorrecto par el 4to año para el cliente con nro de motor: " + objBEVigenciaSiniestro.NroMotor;
                pLstCLog.Add(cLog);
            }

            if (objBEVigenciaSiniestro.CantSiniestro5Anio > objBEVigenciaSiniestro.MontoSiniestro5Anio)
            {
                bResult = false;
                cLog = "Fila nro. " + (pnFila + 1).ToString() + " - La cantidad de siniestros es incorrecto par el 5to año para el cliente con nro de motor: " + objBEVigenciaSiniestro.NroMotor;
                pLstCLog.Add(cLog);
            }
            
            return bResult;
        }
        #endregion        
    }
}