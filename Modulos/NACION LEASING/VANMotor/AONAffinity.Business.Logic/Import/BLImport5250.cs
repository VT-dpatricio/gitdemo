﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.BusinessLogic.Import
{
    public class BLImport5250 : BLImport
    {
        private Int32 nIdProducto = 5250;

        #region Import Clientes IBK .xls
        /// <summary>
        /// Permite cargar la información de los clientes desde un archivo XLSX.
        /// </summary>
        /// <param name="pObjBETipoProceso">Objeto que contiene la información del tipo de proceso.</param>
        /// <param name="pObjBEProceso">Objeto que contiene la información del proceso.</param>
        /// <param name="pObjBETipoArchivo">Objeto que contiene la información del tipo de archivo.</param>
        /// <param name="pcUsuario">Código de ussuario quien ejecuta el proceso.</param>
        public void CargarClientesXlsIBK(BETipoProceso pObjBETipoProceso, BEProceso pObjBEProceso, BETipoArchivo pObjBETipoArchivo, String pcUsuario)
        {
            String cArchivoLog = pObjBEProceso.Archivo;
            String[] cArrArchivoLog = cArchivoLog.Split('.');
            cArchivoLog = cArrArchivoLog[0] + "_.txt";
            cArchivoLog = "Log_" + cArchivoLog;

            Int32 nObservaciones = 0;
            String cLog = String.Empty;
            List<String> lstCLog = new List<String>();
            Boolean bEstructura = true;

            BLEstructuraArchivo objBLEstructuraArchivo = new BLEstructuraArchivo();
            List<BEEstructuraArchivo> lstBEEstructuraArchivo = objBLEstructuraArchivo.Listar(pObjBETipoProceso.IdTipoProceso, pObjBETipoArchivo.IdTipoArchivo, true);
            BLProducto objBLProducto = new BLProducto();
            BEProducto objBEProducto = objBLProducto.Obtener(this.nIdProducto);

            if (lstBEEstructuraArchivo == null)
            {
                bEstructura = false;
                nObservaciones++;
                cLog = "No existe ninguna estructura configurada para el archivo.";
                lstCLog.Add(cLog);
            }

            if (!bEstructura)
            {
                //Actualizar proceso            
                this.EscribirErrores(lstCLog, pObjBETipoProceso.RutaArchivoLog, cArchivoLog);
                pObjBEProceso.Procesado = true;
                pObjBEProceso.UsuarioModificacion = pcUsuario;
                pObjBEProceso.ArchivoLog = cArchivoLog;
                pObjBEProceso.RegTotal = 0;
                pObjBEProceso.RegTotalVal = 0;
                pObjBEProceso.RegTotalErr = 0;
                pObjBEProceso.NroObservaciones = nObservaciones;
                this.ActualizarLog(pObjBEProceso);
                return;
            }

            BLCliente objBLCliente = new BLCliente();

            BLArchivo objBLArchivo = new BLArchivo();
            String cConexion = String.Format(AppSettings.ProviderExcel, pObjBETipoProceso.RutaArchivo + pObjBEProceso.Archivo);
            OleDbConnection oleCn = new OleDbConnection(cConexion);
            oleCn.Open();
            OleDbDataReader oleDr = null;

            oleDr = objBLArchivo.LeerExcel(oleCn);

            Int32 nFila = 0;
            Boolean bValido = true;
            Int32 nValidos = 0;

            if (oleDr != null)
            {
                while (oleDr.Read())
                {
                    bValido = true;

                    if (!this.ValidarOrdenCampos(lstBEEstructuraArchivo, oleDr, ref lstCLog))
                    {
                        bValido = false;
                    }
                    if (!this.ValidarCampoObligatorio(lstBEEstructuraArchivo, nFila, oleDr, ref lstCLog))
                    {
                        bValido = false;
                    }

                    if (!this.ValidarCampoTipoDato(lstBEEstructuraArchivo, nFila, oleDr, ref lstCLog))
                    {
                        bValido = false;
                    }                    

                    //Insertar el registro
                    if (bValido == true)
                    {
                        Int32[] nIndex = new Int32[lstBEEstructuraArchivo.Count];

                        foreach (BEEstructuraArchivo objBEEstructuraArchivo in lstBEEstructuraArchivo)
                        {
                            nIndex[objBEEstructuraArchivo.Orden] = objBEEstructuraArchivo.Orden;
                        }

                        BLModelo objBLModelo = new BLModelo();
                        BEModelo objBEModelo = objBLModelo.ObtenerPorCodExterno(oleDr.GetValue(nIndex[12]).ToString(), oleDr.GetValue(nIndex[10]).ToString(), true, true, NullTypes.CadenaNull);

                        if (objBEModelo == null)
                        {
                            bValido = false;
                            nObservaciones++;
                            cLog = "Fila nro. " + (nFila + 1).ToString() + " - No está registrado o el modelo no es asegurable, código modelo: " + oleDr.GetValue(nIndex[12]).ToString() + " para el cliente con nro. de motor: " + oleDr.GetValue(nIndex[17]).ToString();
                            lstCLog.Add(cLog);
                        }

                        if (objBEModelo != null)
                        {
                            //Validar si vehiculo es segurable.
                            if (!objBEModelo.Asegurable)
                            {
                                bValido = false;
                                nObservaciones++;
                                cLog = "Fila nro. " + (nFila + 1).ToString() + " - El modelo del vehículo con código " + oleDr.GetValue(nIndex[12]).ToString() + " no es asegurable, para el cliente con nro. de motor: " + oleDr.GetValue(nIndex[17]).ToString();
                                lstCLog.Add(cLog);
                            }

                            //Validar si la descripción de la marca del archivo es distinta a la configurada. 
                            if (objBEModelo.DesMarca != oleDr.GetValue(nIndex[11]).ToString())
                            {
                                nObservaciones++;
                                cLog = "Fila nro. " + (nFila + 1).ToString() + " - La descripción de la marca enviada en el archivo: " + oleDr.GetValue(nIndex[11]).ToString() + " es diferente al que está configurada en el sistema: " + objBEModelo.DesMarca + " para el cliente con nro. de motor: " + oleDr.GetValue(nIndex[17]).ToString();
                                lstCLog.Add(cLog);
                            }

                            //Validar si la descripción del modelo del archivo es distinta a la configurada. 
                            if (objBEModelo.Descripcion != oleDr.GetValue(nIndex[13]).ToString())
                            {
                                nObservaciones++;
                                cLog = "Fila nro. " + (nFila + 1).ToString() + " - La descripción del modelo enviado en el archivo: " + oleDr.GetValue(nIndex[13]).ToString() + " es diferente al que está configurado en el sistema: " + objBEModelo.Descripcion + " para el cliente con nro. de motor: " + oleDr.GetValue(nIndex[17]).ToString();
                                lstCLog.Add(cLog);
                            }
                        }

                        if (Convert.ToDateTime(oleDr.GetValue(nIndex[8])).Year < Convert.ToInt32(oleDr.GetValue(nIndex[15])))
                        {
                            bValido = false;
                            nObservaciones++;
                            cLog = "Fila nro. " + (nFila + 1).ToString() + " - La fecha de inicio de vigencia es menor que el año de fabricación del vehículo, para el cliente con nro. de motor: " + oleDr.GetValue(nIndex[17]).ToString();
                            lstCLog.Add(cLog);
                        }

                        if (Convert.ToDateTime(oleDr.GetValue(nIndex[8])).Year > Convert.ToDateTime(oleDr.GetValue(nIndex[9])).Year)
                        {
                            bValido = false;
                            nObservaciones++;
                            cLog = "Fila nro. " + (nFila + 1).ToString() + " - La fecha de inicio de vigencia es mayor a la fecha de fin de vigencia, para el cliente con nro. de motor: " + oleDr.GetValue(nIndex[17]).ToString();
                            lstCLog.Add(cLog);
                        }

                        if (oleDr.GetValue(nIndex[5]).ToString() == "0000000000" || oleDr.GetValue(nIndex[5]).ToString() == "0" || oleDr.GetValue(nIndex[5]).ToString() == String.Empty)
                        {
                            bValido = false;
                            nObservaciones++;
                            cLog = "Fila nro. " + (nFila + 1).ToString() + " - El telefóno es incorrecto, para el cliente con nro. de motor: " + oleDr.GetValue(nIndex[17]).ToString();
                            lstCLog.Add(cLog);
                        }

                        if (Convert.ToDecimal(oleDr.GetValue(nIndex[21])) <= 0)
                        {
                            bValido = false;
                            nObservaciones++;
                            cLog = "Fila nro. " + (nFila + 1).ToString() + " - El valor del vehículo es incorrecto, es menor igual a 0 para el cliente con nro. de motor: " + oleDr.GetValue(nIndex[17]).ToString();
                            lstCLog.Add(cLog);
                        }

                        Int32 nIdCidad = 511501001;

                        if (bValido == true)
                        {
                            BECliente objBECliente = new BECliente();

                            objBECliente.ApePaterno = oleDr.GetValue(nIndex[1]).ToString();
                            objBECliente.ApeMaterno = oleDr.GetValue(nIndex[2]).ToString();
                            objBECliente.PriNombre = oleDr.GetValue(nIndex[3]).ToString();
                            objBECliente.Direccion = String.Empty;
                            objBECliente.TelDomicilio1 = oleDr.GetValue(nIndex[5]).ToString();
                            objBECliente.TelMovil1 = oleDr.GetValue(nIndex[6]).ToString();
                            objBECliente.TelOficina1 = oleDr.GetValue(nIndex[7]).ToString();
                            objBECliente.Email1 = String.Empty;
                            objBECliente.Color = oleDr.GetValue(nIndex[14]).ToString();
                            objBECliente.NroSerie = oleDr.GetValue(nIndex[16]).ToString();
                            objBECliente.NroMotor = oleDr.GetValue(nIndex[17]).ToString();
                            objBECliente.NroPlaca = oleDr.GetValue(nIndex[18]).ToString();
                            if (oleDr.GetValue(nIndex[20]).ToString() == "S") { objBECliente.EsTimonCambiado = true; } else { objBECliente.EsTimonCambiado = false; }
                            objBECliente.ValorVehiculo = Convert.ToDecimal(oleDr.GetValue(nIndex[21]));
                            objBECliente.IdModelo = objBEModelo.IdModelo;
                            objBECliente.IdMarca = objBEModelo.IdMarca;
                            objBECliente.NroCertificado = NullTypes.CadenaNull;
                            objBECliente.CodAsegurador = Asegurador.Rimac;
                            objBECliente.SegNombre = NullTypes.CadenaNull;
                            objBECliente.IdTipoDocumento = "L";
                            objBECliente.NroDocumento = String.Empty;
                            objBECliente.Sexo = NullTypes.CadenaNull;
                            objBECliente.FecNacimiento = NullTypes.FechaNull;
                            objBECliente.idCiudad = nIdCidad;
                            objBECliente.FecVigencia = Convert.ToDateTime(oleDr.GetValue(nIndex[8]));
                            objBECliente.FecFinVigencia = Convert.ToDateTime(oleDr.GetValue(nIndex[9]));
                            objBECliente.TelDomicilio2 = String.Empty;
                            objBECliente.TelDomicilio3 = String.Empty;
                            objBECliente.TelOficina2 = String.Empty;
                            objBECliente.TelOficina3 = String.Empty;
                            objBECliente.TelMovil2 = String.Empty;
                            objBECliente.TelMovil3 = String.Empty;
                            objBECliente.Email1 = String.Empty;
                            objBECliente.Email2 = String.Empty;
                            objBECliente.AnioFab = Convert.ToInt32(oleDr.GetValue(nIndex[15]));
                            objBECliente.IdUsoVehiculo = 1;
                            objBECliente.IdClase = objBEModelo.IdClase;
                            objBECliente.NroAsientos = NullTypes.IntegerNull;
                            objBECliente.EsBlindado = false;
                            objBECliente.ReqGPS = false;
                            objBECliente.IdSponsor = Convert.ToInt32(this.ObtenerEquivalencia(TipoDato.Numero, TipoProceso.ImportClienteIBK, TipoArchivo.Excel, 0, oleDr.GetValue(nIndex[0]).ToString()));
                            objBECliente.IdProceso = pObjBEProceso.IdProceso;
                            objBECliente.IdEstadoCliente = EstadoCliente.NoCotizado;
                            objBECliente.UsuarioCreacion = pcUsuario;
                            objBECliente.EstadoRegistro = true;

                            if (ValidarCliente(objBEProducto, objBECliente, nFila, ref lstCLog, ref nObservaciones))
                            {
                                if (objBLCliente.Insertar(objBECliente) > 0)
                                {
                                    nValidos++;
                                    cLog = "Fila nro. " + (nFila + 1).ToString() + " - Se registró el cliente con nro. de motor: " + objBECliente.NroMotor;
                                    lstCLog.Add(cLog);
                                }
                                else
                                {
                                    nObservaciones++;
                                    cLog = "Fila nro. " + (nFila + 1).ToString() + " - No se pudo registrar el cliente con nro. de motor: " + objBECliente.NroMotor;
                                    lstCLog.Add(cLog);
                                }
                            }
                        }
                        nFila++;
                    }
                }
            }

            oleCn.Close();

            //Actualizar proceso            
            this.EscribirErrores(lstCLog, pObjBETipoProceso.RutaArchivoLog, cArchivoLog);
            pObjBEProceso.Procesado = true;
            pObjBEProceso.UsuarioModificacion = pcUsuario;
            pObjBEProceso.ArchivoLog = cArchivoLog;
            pObjBEProceso.RegTotal = nFila;
            pObjBEProceso.RegTotalVal = nValidos;
            pObjBEProceso.RegTotalErr = nFila - nValidos;
            pObjBEProceso.NroObservaciones = nObservaciones;
            this.ActualizarLog(pObjBEProceso);
        }
        #endregion

        #region Validacion Cliente
        /// <summary>
        /// Permite validar la información del cliente.
        /// </summary>
        /// <param name="pObjBEProducto">Objeto que contiene la información del producto.</param>
        /// <param name="pObBEjCliente">Objeto que contien la información del cliente</param>
        /// <param name="pnFila">Nro. de fila de registro en el archivo.</param>
        /// <param name="pLstCLog">Lista log.</param>
        /// <param name="pnObservaciones">nro de observaciones.</param>
        /// <returns>True Información correcta | False información no correcta.</returns>
        public Boolean ValidarCliente(BEProducto pObjBEProducto, BECliente pObBEjCliente, Int32 pnFila, ref List<String> pLstCLog, ref Int32 pnObservaciones)
        {
            /*---------------------------------------------------------------------------
            * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
            * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Boolean bResult = true;
            BECliente objBECliente = new BECliente();
            String cLog = String.Empty;

            BLCliente objBLCliente = new BLCliente();
            BECliente objBECliente_Temp = new BECliente();

            //Obtener cliente por nro de motor.
            objBECliente = objBLCliente.Obtener(pObBEjCliente.NroMotor);

            //Si cliente ya esta registrado por nro de motor, escribir log.
            if (objBECliente != null)
            {
                cLog = "Fila nro. " + (pnFila + 1).ToString() + " - Ya se encuentra registrado el cliente con nro. de motor: " + pObBEjCliente.NroMotor;
                pLstCLog.Add(cLog);
                pnObservaciones++;
                bResult = false;
            }

            //Obtener cliente por nro de placa.
            objBECliente = objBLCliente.ObtenerxPlaca(pObBEjCliente.NroPlaca);

            //Si cliente ya está registrado por placa, escribir log.
            if (objBECliente != null)
            {
                cLog = "Fila nro. " + (pnFila + 1).ToString() + " - Ya se encuentra registrado el cliente con nro. de placa: " + pObBEjCliente.NroPlaca + " con nro. de motor: " + pObBEjCliente.NroMotor;
                pLstCLog.Add(cLog);
                pnObservaciones++;
                bResult = false;
            }

            //Obtener clientes por nro serie.
            objBECliente = objBLCliente.ObtenerxSerie(pObBEjCliente.NroSerie);
            if (objBECliente != null)
            {
                cLog = "Fila nro. " + (pnFila + 1).ToString() + " - Ya se encuentra registrado el cliente con nro. de serie: " + pObBEjCliente.NroSerie + " con nro. de motor: " + pObBEjCliente.NroMotor;
                pLstCLog.Add(cLog);
                pnObservaciones++;
                bResult = false;
            }

            //Validar si año de fab. es mayor al año actual.
            if (pObBEjCliente.AnioFab > DateTime.Now.Year)
            {
                cLog = "Fila nro. " + (pnFila + 1).ToString() + " - El año de fabricación del vehículo es incorrecto, es mayor al año actual, para el cliente con nro. de motor: " + pObBEjCliente.NroMotor;
                pLstCLog.Add(cLog);
                pnObservaciones++;
                bResult = false;
            }

            //Si existe información del producto.
            if (pObjBEProducto != null)
            {
                //Si existe maxima antiguedad.
                if (pObjBEProducto.MaxAntiguedad != NullTypes.IntegerNull)
                {
                    Int32 nAnioAct = DateTime.Now.Year;
                    Int32 nAntiguedad = nAnioAct - pObBEjCliente.AnioFab;

                    //Si la antiguedad del vehículo es mayor a la permitida.
                    if (nAntiguedad > pObjBEProducto.MaxAntiguedad)
                    {
                        cLog = "Fila nro. " + (pnFila + 1).ToString() + " - El vehículo no es asegurale porque antigüedad supera la máxima permitida. nro. de motor: " + pObBEjCliente.NroMotor;
                        pLstCLog.Add(cLog);
                        pnObservaciones++;
                        bResult = false;
                    }
                }

                //Si existe maximo valor de vehículo.
                if (pObjBEProducto.MaxValorVehiculo != NullTypes.DecimalNull)
                {
                    BLCotizacion objBLCotizacion = new BLCotizacion();
                    Decimal nValorVehiculoDep = objBLCotizacion.ObtenerValorDepresiado(pObBEjCliente.AnioFab, pObBEjCliente.ValorVehiculo, this.nIdProducto);

                    //Validar si el valor es mayor al maximo permitido.
                    if (nValorVehiculoDep > pObjBEProducto.MaxValorVehiculo)
                    {
                        cLog = "Fila nro. " + (pnFila + 1).ToString() + " - El vehículo no es asegurale porque valor del vehículo depresiado: " + Decimal.Round(nValorVehiculoDep, 2).ToString() + " supera al máximo permitido: " + Decimal.Round(pObjBEProducto.MaxValorVehiculo, 2).ToString() + " nro. de motor: " + pObBEjCliente.NroMotor;
                        pLstCLog.Add(cLog);
                        pnObservaciones++;
                        bResult = false;
                    }
                }

                BLCategoriaModelo objBLCategoriaModelo = new BLCategoriaModelo();
                String cRespuesta = objBLCategoriaModelo.ValidarVehiculo(this.nIdProducto, pObBEjCliente.IdModelo, pObBEjCliente.AnioFab);

                if (cRespuesta != String.Empty)
                {
                    cLog = "Fila nro. " + (pnFila + 1).ToString() + " " + cRespuesta + " nro. de motor: " + pObBEjCliente.NroMotor;
                    pLstCLog.Add(cLog);
                    pnObservaciones++;
                    bResult = false;
                }
            }

            return bResult;
        }
        #endregion
    }
}
