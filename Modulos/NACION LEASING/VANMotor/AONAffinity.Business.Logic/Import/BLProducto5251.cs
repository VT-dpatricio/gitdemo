﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AONAffinity.Motor.Resources; 
using AONAffinity.Library.Resources;
using AONAffinity.Library.BusinessEntity.BDMotor;  

namespace AONAffinity.Motor.BusinessLogic.Import
{
    public class BLProducto5251 : BLImport
    {
        #region Proceso BBVA - AON
        private void ProcesoImportBBVACliente(BETipoProceso pObjBETipoProceso, BEProceso pObjBEProceso, String pcUsuario)
        {
            Int32 nObservaciones = 0;
            String cLog = String.Empty;
            List<String> lstCLog = new List<String>();
            Boolean bEstructura = true;
            String cArchivoLog = "Log_" + pObjBEProceso.Archivo;
            BLProceso objBLProceso = new BLProceso();

            if (pObjBETipoProceso.LongitudTrama == NullTypes.IntegerNull)
            {
                bEstructura = false;
                cLog = "No está configurada la longitud de la trama.";
                lstCLog.Add(cLog);
                nObservaciones++;
            }

            BLEstructuraArchivo objBLEstructuraArchivo = new BLEstructuraArchivo();
            List<BEEstructuraArchivo> lstBEEstructuraArchivo = objBLEstructuraArchivo.Listar(pObjBETipoProceso.IdTipoProceso, true);

            if (lstBEEstructuraArchivo == null)
            {
                bEstructura = false;
                cLog = "No existe ninguna estructura configurada para el archivo.";
                lstCLog.Add(cLog);
                nObservaciones++;
            }

            FileStream fsArchivo = new FileStream(pObjBETipoProceso.RutaArchivo + pObjBEProceso.Archivo, FileMode.Open);
            Int32 nFila = 0;

            //Si estructura no es válida.
            if (!bEstructura)
            {
                this.EscribirErrores(lstCLog, pObjBETipoProceso.RutaArchivoLog, cArchivoLog);

                pObjBEProceso.Procesado = true;
                pObjBEProceso.UsuarioModificacion = pcUsuario;
                pObjBEProceso.ArchivoLog = cArchivoLog;
                pObjBEProceso.RegTotal = 0;
                pObjBEProceso.RegTotalVal = 0;
                pObjBEProceso.RegTotalErr = 0;
                pObjBEProceso.NroObservaciones = nObservaciones;
                objBLProceso.ActualizarLog(pObjBEProceso);

                return;
            }

            BLCliente objBLCliente = new BLCliente();
            Boolean bValido = true;
            Int32 nValidos = 0;

            using (StreamReader stream = new StreamReader(fsArchivo, Funciones.GetFileEncoding(pObjBETipoProceso.RutaArchivo + pObjBEProceso.Archivo, fsArchivo)))
            {
                String cTrama;
                while (((cTrama = stream.ReadLine()) != null))
                {
                    bValido = true;

                    //Validar sio longitud de trama es válida.
                    if (cTrama.Length != pObjBETipoProceso.LongitudTrama)
                    {
                        bValido = false;
                        cLog = "Fila nro. " + nFila + " - el tamaño de la trama es diferente a la configurada.";
                        lstCLog.Add(cLog);
                    }

                    //Si filtro es válido
                    if (bValido == true)
                    {
                        List<String> lstCCampos = this.TramaSplit(cTrama, lstBEEstructuraArchivo);

                        if (!this.ValidarCampoObligatorio(lstBEEstructuraArchivo, nFila, lstCCampos, ref lstCLog))
                        {
                            bValido = false;
                        }

                        if (!ValidarCampoTipoDato(lstBEEstructuraArchivo, nFila, lstCCampos, ref  lstCLog))
                        {
                            bValido = false;
                        }

                        if (bValido == true)
                        {
                            String cCodExtModelo = String.Empty;
                            String cCodExtMarca = String.Empty;
                            String cCodDep = String.Empty;
                            String cCodProv = String.Empty;
                            String cCodDist = String.Empty;
                            String cCodTipoReg = String.Empty;
                            DateTime dFecInicio = NullTypes.FechaNull;
                            DateTime dFecFin = NullTypes.FechaNull;
                            Int32 nAnioFab = NullTypes.IntegerNull;
                            String cTelefono1 = String.Empty;

                            foreach (BEEstructuraArchivo objBEEstructuraArchivo in lstBEEstructuraArchivo)
                            {
                                if (objBEEstructuraArchivo.Nombre.ToUpper() == ("TipoRegistro").ToUpper())
                                {
                                    cCodTipoReg = lstCCampos[objBEEstructuraArchivo.Orden].ToString().Trim();
                                }
                                if (objBEEstructuraArchivo.Nombre.ToUpper() == ("Marca").ToUpper())
                                {
                                    cCodExtMarca = lstCCampos[objBEEstructuraArchivo.Orden].ToString().Trim();
                                }
                                if (objBEEstructuraArchivo.Nombre.ToUpper() == ("Modelo").ToUpper())
                                {
                                    cCodExtModelo = lstCCampos[objBEEstructuraArchivo.Orden].ToString().Trim();
                                }
                                if (objBEEstructuraArchivo.Nombre.ToUpper() == ("CodDepartamento").ToUpper())
                                {
                                    cCodDep = lstCCampos[objBEEstructuraArchivo.Orden].ToString().Trim();
                                }
                                if (objBEEstructuraArchivo.Nombre.ToUpper() == ("CodProvincia").ToUpper())
                                {
                                    cCodProv = lstCCampos[objBEEstructuraArchivo.Orden].ToString().Trim();
                                }
                                if (objBEEstructuraArchivo.Nombre.ToUpper() == ("CodDistrito").ToUpper())
                                {
                                    cCodDist = lstCCampos[objBEEstructuraArchivo.Orden].ToString().Trim();
                                }
                                if (objBEEstructuraArchivo.Nombre.ToUpper() == ("FechaInicioSeguroCredito").ToUpper())
                                {
                                    String cYY = lstCCampos[objBEEstructuraArchivo.Orden].Substring(0, 4);
                                    String cMM = lstCCampos[objBEEstructuraArchivo.Orden].Substring(4, 2);
                                    String cDD = lstCCampos[objBEEstructuraArchivo.Orden].Substring(6, 2);
                                    dFecInicio = Convert.ToDateTime(cDD + "/" + cMM + "/" + cYY);
                                }
                                if (objBEEstructuraArchivo.Nombre.ToUpper() == ("FechaFinSeguroCredito").ToUpper())
                                {
                                    String cYY = lstCCampos[objBEEstructuraArchivo.Orden].Substring(0, 4);
                                    String cMM = lstCCampos[objBEEstructuraArchivo.Orden].Substring(4, 2);
                                    String cDD = lstCCampos[objBEEstructuraArchivo.Orden].Substring(6, 2);
                                    dFecFin = Convert.ToDateTime(cDD + "/" + cMM + "/" + cYY);
                                }
                                if (objBEEstructuraArchivo.Nombre.ToUpper() == ("AnioFabricacion").ToUpper())
                                {
                                    nAnioFab = Convert.ToInt32(lstCCampos[objBEEstructuraArchivo.Orden]);
                                }
                                if (objBEEstructuraArchivo.Nombre.ToUpper().Trim() == ("NumeroTelefono").ToUpper().Trim())
                                {
                                    cTelefono1 = lstCCampos[objBEEstructuraArchivo.Orden].ToString();
                                }
                            }

                            BLModelo objBLModelo = new BLModelo();
                            BEModelo objBEModelo = objBLModelo.ObtenerPorCodExterno(cCodExtModelo, cCodExtMarca, true, true, NullTypes.CadenaNull);

                            if (objBEModelo == null)
                            {
                                bValido = false;
                                nObservaciones++;
                                cLog = "Fila nro. " + nFila + " - No está registrado el modelo con código externo: " + cCodExtModelo;
                                lstCLog.Add(cLog);
                            }
                            if (objBEModelo != null)
                            {
                                if (!objBEModelo.Asegurable)
                                {
                                    bValido = false;
                                    nObservaciones++;
                                    cLog = "Fila nro. " + nFila + " - El modelo del vehículo no es asegurable: " + cCodExtModelo;
                                    lstCLog.Add(cLog);
                                }
                            }
                            if (dFecInicio.Year < nAnioFab)
                            {
                                bValido = false;
                                nObservaciones++;
                                cLog = "Fila nro. " + nFila + " - La fecha de inicio de vigencia es menor que el año de fabricación del vehículo: " + cCodExtModelo;
                                lstCLog.Add(cLog);
                            }

                            if (dFecInicio.Year > dFecFin.Year)
                            {
                                bValido = false;
                                nObservaciones++;
                                cLog = "Fila nro. " + nFila + " - La fecha de inicio de vigencia es mayor a la fecha de fin de vigencia";
                                lstCLog.Add(cLog);
                            }

                            if (cTelefono1 == "0000000000" || cTelefono1.Trim() == "0" || cTelefono1 == String.Empty)
                            {
                                bValido = false;
                                nObservaciones++;
                                cLog = "Fila nro. " + nFila + " - El telefóno es incorrecto, cliente con nro e motor: ";
                                lstCLog.Add(cLog);
                            }

                            Int32 nIdCidad;
                            if (cCodDep.Trim() + cCodProv.Trim() + cCodDist.Trim() == String.Empty)
                            {
                                nIdCidad = 511501001;
                            }
                            else
                            {
                                nIdCidad = Convert.ToInt32("51" + cCodDep + cCodProv + cCodDist);
                            }

                            if (bValido == true)
                            {
                                BECliente objBECliente = new BECliente();
                                foreach (BEEstructuraArchivo objBEEstructuraArchivo in lstBEEstructuraArchivo)
                                {
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("ApellidoPaterno").ToUpper()) { objBECliente.ApePaterno = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper(); }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("ApellidoMaterno").ToUpper()) { objBECliente.ApeMaterno = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper(); }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("Nombres").ToUpper()) { objBECliente.PriNombre = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper(); }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("DireccionDomiciliaria").ToUpper()) { objBECliente.Direccion = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper(); }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("MzLteNro").ToUpper()) { objBECliente.Direccion = lstCCampos[objBEEstructuraArchivo.Orden] + " " + objBECliente.Direccion.Trim().ToUpper(); }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("NumeroTelefono").ToUpper())
                                    {
                                        objBECliente.TelDomicilio1 = lstCCampos[objBEEstructuraArchivo.Orden].Trim();
                                    }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("NumeroTelefono2").ToUpper()) { objBECliente.TelMovil1 = lstCCampos[objBEEstructuraArchivo.Orden].Trim(); }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("NumeroTelefono3").ToUpper()) { objBECliente.TelOficina1 = lstCCampos[objBEEstructuraArchivo.Orden].Trim(); }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("CorreoElectronico").ToUpper()) { objBECliente.Email1 = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToLower(); }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("Color").ToUpper()) { objBECliente.Color = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper(); }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("NumeroChasis").ToUpper()) { objBECliente.NroSerie = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper(); }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("NumeroMotor").ToUpper()) { objBECliente.NroMotor = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper(); }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("NumeroMatriculaPlaca").ToUpper()) { objBECliente.NroPlaca = lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper(); }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("IndicadorTimonCambiado").ToUpper())
                                    {
                                        if (lstCCampos[objBEEstructuraArchivo.Orden].Trim().ToUpper() == "S") { objBECliente.EsTimonCambiado = true; } else { objBECliente.EsTimonCambiado = false; }
                                    }
                                    if (objBEEstructuraArchivo.Nombre.ToUpper() == ("ValorVehiculo").ToUpper())
                                    {
                                        String cEntero = lstCCampos[objBEEstructuraArchivo.Orden].Substring(0, 13);
                                        String cDecimal = lstCCampos[objBEEstructuraArchivo.Orden].Substring(13, 2);
                                        Decimal nValorVehiculo = Convert.ToDecimal((cEntero + "." + cDecimal).Trim());

                                        objBECliente.ValorVehiculo = Convert.ToDecimal(nValorVehiculo);
                                    }
                                }

                                objBECliente.IdModelo = objBEModelo.IdModelo;
                                objBECliente.IdMarca = objBEModelo.IdMarca;
                                objBECliente.NroCertificado = NullTypes.CadenaNull;
                                objBECliente.CodAsegurador = 5;
                                objBECliente.SegNombre = NullTypes.CadenaNull;
                                if (cCodTipoReg == "01") { objBECliente.IdTipoDocumento = "L"; } else { objBECliente.IdTipoDocumento = "R"; }
                                objBECliente.NroDocumento = String.Empty;
                                objBECliente.Sexo = NullTypes.CadenaNull;
                                objBECliente.FecNacimiento = NullTypes.FechaNull;
                                objBECliente.idCiudad = nIdCidad;
                                objBECliente.FecVigencia = dFecInicio;
                                objBECliente.FecFinVigencia = dFecFin;
                                objBECliente.TelDomicilio2 = String.Empty;
                                objBECliente.TelDomicilio3 = String.Empty;
                                objBECliente.TelOficina2 = String.Empty;
                                objBECliente.TelOficina3 = String.Empty;
                                objBECliente.TelMovil2 = String.Empty;
                                objBECliente.TelMovil3 = String.Empty;
                                objBECliente.Email1 = String.Empty;
                                objBECliente.Email2 = String.Empty;
                                objBECliente.AnioFab = nAnioFab;
                                objBECliente.IdUsoVehiculo = 1;
                                objBECliente.IdClase = objBEModelo.IdClase;
                                objBECliente.NroAsientos = NullTypes.IntegerNull;
                                objBECliente.EsBlindado = false;
                                objBECliente.ReqGPS = false;
                                objBECliente.IdSponsor = 1; //BBVA 
                                objBECliente.IdProceso = pObjBEProceso.IdProceso;
                                objBECliente.IdEstadoCliente = 5;
                                objBECliente.UsuarioCreacion = pcUsuario;
                                objBECliente.EstadoRegistro = true;

                                BECliente objBECliente_Temp = objBLCliente.Obtener(objBECliente.NroMotor);

                                if (objBECliente_Temp != null)
                                {
                                    cLog = "Fila nro. " + nFila.ToString() + " - Ya se encuentra registrado el cliente con nro de motor: " + objBECliente.NroMotor;
                                    lstCLog.Add(cLog);
                                    nObservaciones++;
                                }
                                else
                                {
                                    if (objBLCliente.Insertar(objBECliente) > 0)
                                    {
                                        nValidos++;
                                        cLog = "Fila nro. " + nFila + " - Se registró el cliente con nro. de motor: " + objBECliente.NroMotor;
                                        lstCLog.Add(cLog);
                                    }
                                    else
                                    {
                                        nObservaciones++;
                                        cLog = "Fila nro. " + nFila + " - No se pudo registrar el cliente con nro. de motor: " + objBECliente.NroMotor;
                                        lstCLog.Add(cLog);
                                    }
                                }
                            }
                        }
                    }

                    nFila++;
                    fsArchivo.Flush();
                }

                fsArchivo.Close();
            }

            //Actualizar proceso            
            this.EscribirErrores(lstCLog, pObjBETipoProceso.RutaArchivoLog, cArchivoLog);

            pObjBEProceso.Procesado = true;
            pObjBEProceso.UsuarioModificacion = pcUsuario;
            pObjBEProceso.ArchivoLog = cArchivoLog;
            pObjBEProceso.RegTotal = nFila;
            pObjBEProceso.RegTotalVal = nValidos;
            pObjBEProceso.RegTotalErr = nFila - nValidos;
            pObjBEProceso.NroObservaciones = nObservaciones;

            objBLProceso.ActualizarLog(pObjBEProceso);
        }
        #endregion

        #region Util
        

        //ok
        public void CrearArchivoExport(Int32 pnIdProceso, String pcProcedure, String pcRuta, String pcArchivo)
        {
            List<String> lstCTrama = this.GenerarTramaExport(pnIdProceso, pcProcedure);

            if (lstCTrama != null)
            {
                StreamWriter sw = new StreamWriter(pcRuta + @"/" + pcArchivo);
                StringBuilder cadena = null;

                foreach (String cTrama in lstCTrama)
                {
                    cadena = new StringBuilder();
                    cadena.Append(cTrama);
                    sw.WriteLine(cadena.ToString());
                }
                sw.Close();
            }
        }
        #endregion
        


    }
}
