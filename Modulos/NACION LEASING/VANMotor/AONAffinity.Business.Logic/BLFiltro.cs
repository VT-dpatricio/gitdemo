﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AONAffinity.Library.Resources; 
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;  

//using AONAffinity.Library.Resources;
//using AONAffinity.Library.DataAccess;
//using AONAffinity.Library.DataAccess.BDMotor;   
//using AONAffinity.Library.BusinessEntity.BDMotor;   

namespace AONAffinity.Motor.BusinessLogic
{
    /// <summary>
    /// lase que contiene la lógica del negocio de los filtros de información.
    /// </summary>
    public class BLFiltro
    {
        #region Transaccional
        /// <summary>
        /// Permite validar la asegurabilidad de los vehículos, segun la nota técnica.
        /// </summary>
        /// <param name="pLstBECliente">Lista de clientes no cotizados.</param>
        /// <param name="pcUsuario">Código de usuario que realiza proceso.</param>
        public void ValidarVehiculoAsegurables(List<BECliente> pLstBECliente, String pcUsuario)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.:2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.:____-__-__
            ---------------------------------------------------------------------------*/

            if (pLstBECliente != null)
            {
                String cLog = String.Empty;

                //Obtener la lista de modelos no asegurables.
                BLModelo objBLModelo = new BLModelo();
                List<BEModelo> lstBEModelo = objBLModelo.ListarNoAseg();

                BLParametro objBLParametro = new BLParametro();

                Int32 nIdParMaxAntiguedad = 2;
                BEParametro objBEParametro_Ant = objBLParametro.Obtener(nIdParMaxAntiguedad);

                Int32 nIdParTimonCamb = 3;
                BEParametro objBEParametro_Tim = objBLParametro.Obtener(nIdParTimonCamb);

                Int32 nIdParMinAntPickUp = 4;
                BEParametro objBEParametro_AntPU = objBLParametro.Obtener(nIdParMinAntPickUp);

                Int32 nIdParBlindado = 5;
                BEParametro objBEParametro_Bli = objBLParametro.Obtener(nIdParBlindado);

                Int32 nIdParMaxValVeh = 6;
                BEParametro objBEParametro_MaxValVeh = objBLParametro.Obtener(nIdParMaxValVeh);

                BLCliente objBLCliente = new BLCliente();

                foreach (BECliente objBECliente in pLstBECliente)
                {
                    Boolean bActualizo = false;

                    //Si modelo no es asegurable, se actualiza el estado del cliente.
                    if (this.NoAsegurablexModelo(objBECliente, lstBEModelo))
                    {
                        objBLCliente.Atualizar(objBECliente.NroMotor, Convert.ToInt32(ValorConstante.EstadoCliente.NO_ASEG_MODELO), pcUsuario);
                        bActualizo = true;
                    }

                    //Si vehículo no es asgurable por año de fabricación, y áún no se realizó actualización
                    //se actualiza el estado del cliente.
                    if (this.NoAsegurablexMaxAnt(objBEParametro_Ant, objBECliente) && bActualizo == false)
                    {
                        objBLCliente.Atualizar(objBECliente.NroMotor, Convert.ToInt32(ValorConstante.EstadoCliente.NO_ASEG_ANTIGUEDAD), pcUsuario);
                        bActualizo = true;
                    }

                    //Si Vehículo no es asegurable  por timón cambiado y áún no se realizó actualización.
                    //se actualiza el estado del cliente.
                    if (this.NoAsegurablexTimonCamb(objBEParametro_Tim, objBECliente) && bActualizo == false)
                    {
                        objBLCliente.Atualizar(objBECliente.NroMotor, Convert.ToInt32(ValorConstante.EstadoCliente.NO_ASEG_TIMONCAMB), pcUsuario);
                        bActualizo = true;
                    }

                    //Si vehículo es Pick Up, menor a la antigüead permitida y aún no se realizo actualización.
                    //se actualiza el estado del cliente.
                    if (this.NoAsegurablexMinAntPickUp(objBEParametro_AntPU, objBECliente) && bActualizo == false)
                    {
                        objBLCliente.Atualizar(objBECliente.NroMotor, Convert.ToInt32(ValorConstante.EstadoCliente.NO_ASEG_MINANTPICKUP), pcUsuario);
                        bActualizo = true;
                    }

                    //Si vehículo es blindado y aún no se realizo actualización.
                    //se actualiza el estado del cliente.
                    if (this.NoAsegurablexBlindado(objBEParametro_Bli, objBECliente) && bActualizo == false)
                    {
                        objBLCliente.Atualizar(objBECliente.NroMotor, Convert.ToInt32(ValorConstante.EstadoCliente.NO_ASEG_BLINDADO), pcUsuario);
                        bActualizo = true;
                    }

                    //Si vehículo no es aseguirable por máximo valor de vehículo.
                    //se actualiza el estado del cliente.
                    if (this.NoAsegurablexMaxValVehiculo(objBEParametro_MaxValVeh, objBECliente) && bActualizo == false)
                    {
                        objBLCliente.Atualizar(objBECliente.NroMotor, Convert.ToInt32(ValorConstante.EstadoCliente.NO_ASEG_MAXVALORVEH), pcUsuario);
                        bActualizo = true;
                    }
                }
            }
        }
        #endregion        

        #region Funciones Privadas
        /// <summary>
        /// Permite validar si el modelo es asegurable.
        /// </summary>
        /// <param name="objBECliente">Información del cliente.</param>
        /// <param name="pLstBEModelo">Lista de modelos no asegurables.</param>
        /// <returns>True no asegurable | False asegurable.</returns>
        private Boolean NoAsegurablexModelo(BECliente objBECliente, List<BEModelo> pLstBEModelo)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.:2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.:____-__-__
            ---------------------------------------------------------------------------*/

            Boolean nResult = false;

            if (pLstBEModelo != null)
            {
                foreach (BEModelo objBEModelo in pLstBEModelo)
                {
                    if (objBEModelo.IdModelo == objBECliente.IdModelo)
                    {
                        BLCliente objBLCliente = new BLCliente();

                        nResult = true;
                    }
                }
            }

            return nResult;
        }

        /// <summary>
        /// Permite validar si vehículo es asegurable por antigüedad máxima permitida.
        /// </summary>
        /// <param name="pObjBEParametro">Información del parámetro.</param>
        /// <param name="pObjBECliente">Información del cliente.</param>
        /// <returns>True no asegurable | False asegurable.</returns>
        private Boolean NoAsegurablexMaxAnt(BEParametro pObjBEParametro, BECliente pObjBECliente)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.:2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.:____-__-__
            ---------------------------------------------------------------------------*/

            Boolean nResult = false;

            //Si parametro el null, retornar False.
            if (pObjBEParametro == null) 
            {
                return nResult;
            }

            //Si estado de resgistro es no activo, retornar False.
            if (!pObjBEParametro.EstadoRegistro)
            {
                return nResult;
            }

            //Si el maximio año de antiguedad es IntegerNull, retornar False.
            if (pObjBEParametro.ValorNumeroEnt == NullTypes.IntegerNull) 
            {
                return nResult;
            }
            
            //Validar el maximo año de antiguedad.
            Int32 nAnio = DateTime.Now.Year;

            //Si el año de antiguedad del vehículo es mayor que la maxima permitida.
            //valor a retornar será True.
            if ((nAnio - pObjBECliente.AnioFab) > pObjBEParametro.ValorNumeroEnt)
            {
                nResult = true;
            }
            
            return nResult;
        }

        /// <summary>
        /// Permite validar si vehículo es asegurable por timón cambiado
        /// </summary>
        /// <param name="pObjBEParametro">Información del parámetro.</param>
        /// <param name="pObjBECliente">Información del cliente.</param>
        /// <returns>True no asegurable | False asegurable.</returns>
        private Boolean NoAsegurablexTimonCamb(BEParametro pObjBEParametro, BECliente pObjBECliente)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.:2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.:____-__-__
            ---------------------------------------------------------------------------*/

            Boolean bResult = false;

            //Si parametro es null, retornar False.
            if (pObjBEParametro == null) 
            {
                return bResult;
            }

            //Si estado de resgistro es no activo, retornar False.
            if (!pObjBEParametro.EstadoRegistro) 
            {
                return bResult;
            }

            //Si no existe estado para el registro, retornar False.
            if (pObjBEParametro.ValorBool == NullTypes.BoolNull) 
            {
                return bResult;
            }

            //Si valor es activo. validar vehículo.
            if (pObjBEParametro.ValorBool == true)             
            {
                //Si vehículo es timón cambiado, valor a retornar será True.
                if (pObjBECliente.EsTimonCambiado == true) 
                {
                    bResult = true;
                }
            }

            return bResult;
        }

        /// <summary>
        /// Permite validar mínima antiguedad para vehículos Pickup.
        /// </summary>
        /// <param name="pObjBEParametro">Información del parámetro.</param>
        /// <param name="pObjBECliente">Información del cliente.</param>
        /// <returns>True no asegurable | False asegurable.</returns>
        private Boolean NoAsegurablexMinAntPickUp(BEParametro pObjBEParametro, BECliente pObjBECliente)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.:2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.:____-__-__
            ---------------------------------------------------------------------------*/

            Boolean bResult = false;

            //Si la clase del vehiculo no es PickUp, no aplica validación, retornar false.
            if (pObjBECliente.IdClase != Convert.ToInt32(ValorConstante.Clase.PICK_UP)) 
            {
                return bResult;
            }

            //Si parametro es null, retornar False.
            if (pObjBEParametro == null) 
            {
                return bResult;
            }

            //Si estado de resgistro es no activo, retornar False.
            if (!pObjBEParametro.EstadoRegistro)
            {
                return bResult;
            }

            //Si el mínimo año de antiguedad es IntegerNull, retornar False.
            if (pObjBEParametro.ValorNumeroEnt == NullTypes.IntegerNull) 
            {
                return bResult;
            }

            ///Validar el mínimo anio de antiguedad.
            Int32 nAnio = DateTime.Now.Year;

            //Si el año de antiguedad del vehículo es menor que la mínima permitida.
            //valor a retornar será True.
            if ((nAnio - pObjBECliente.AnioFab) < pObjBEParametro.ValorNumeroEnt) 
            {
                bResult = true;
            }

            return bResult;
        }

        /// <summary>
        /// Permite validar si vehiculo es asugurable si es blindado.
        /// </summary>
        /// <param name="pObjBEParametro">Información del parámetro.</param>
        /// <param name="pObjBECliente">Información del cliente.</param>
        /// <returns>True no asegurable | False asegurable.</returns>
        private Boolean NoAsegurablexBlindado(BEParametro pObjBEParametro, BECliente pObjBECliente) 
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.:2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.:____-__-__
            ---------------------------------------------------------------------------*/

            Boolean bResult = false;

            //Si parametro es null, retornar False.
            if (pObjBEParametro == null)
            {
                return bResult;
            }

            //Si estado de resgistro es no activo, retornar False.
            if (!pObjBEParametro.EstadoRegistro)
            {
                return bResult;
            }

            //Si no existe estado para el registro, retornar False.
            if (pObjBEParametro.ValorBool == NullTypes.BoolNull)
            {
                return bResult;
            }

            //Si valor es activo. validar vehículo.
            if (pObjBEParametro.ValorBool == true) 
            {
                //Si vehículo es timón cambiado, valor a retornar será True.
                if (pObjBECliente.EsBlindado == true)
                {
                    bResult = true;
                }
            }                    

            return bResult;
        }

        /// <summary>
        /// Permite validar si vehículo es asegurable segun el máximo valor de vehículo permitido.
        /// </summary>
        /// <param name="pObjBEParametro">Información del parámetro.</param>
        /// <param name="pObjBECliente">Información del cliente.</param>
        /// <returns>True no asegurable | False asegurable.</returns>
        private Boolean NoAsegurablexMaxValVehiculo(BEParametro pObjBEParametro, BECliente pObjBECliente) 
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.:2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.:____-__-__
            ---------------------------------------------------------------------------*/

            Boolean bResult = false;

            //Si parametro es null, retornar False.
            if (pObjBEParametro == null)
            {
                return bResult;
            }

            //Si estado de resgistro es no activo, retornar False.
            if (!pObjBEParametro.EstadoRegistro)
            {
                return bResult;
            }

            //Si no existe estado para el registro, retornar False.
            if (pObjBEParametro.ValorNumeroDec == NullTypes.DecimalNull)
            {
                return bResult;
            }

            //Si valor del vehículo es mayor a al maximo permitido, valor a retornar será True.
            if (pObjBECliente.ValorVehiculo > pObjBEParametro.ValorNumeroDec) 
            {
                bResult = true;
            }

            return bResult;
        } 
        #endregion               
    }
}
