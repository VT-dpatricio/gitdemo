﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources; 
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLTipoArchivo
    {
        #region NoTransasscional
        public List<BETipoArchivo> ListarxTipoProceso(Int32 pnIdTipoProceso)
        {
            List<BETipoArchivo> lstBETipoArchivo = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DATipoArchivo objDATipoArchivo = new DATipoArchivo();
                SqlDataReader sqlDr = objDATipoArchivo.ListarxTipoProceso(pnIdTipoProceso, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idTipoArchivo");
                        Int32 nindex2 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("extencion");
                        Int32 nIndex4 = sqlDr.GetOrdinal("idTipoProceso");
                        Int32 nIndex5 = sqlDr.GetOrdinal("longitudTrama");
                        Int32 nIndex6 = sqlDr.GetOrdinal("nombreArchivo");
                        Int32 nIndex7 = sqlDr.GetOrdinal("descProceso");

                        lstBETipoArchivo = new List<BETipoArchivo>();

                        while (sqlDr.Read())
                        {
                            BETipoArchivo objBETipoArchivo = new BETipoArchivo();
                            objBETipoArchivo.IdTipoArchivo = sqlDr.GetInt32(nIndex1);
                            objBETipoArchivo.Descripcion = sqlDr.GetString(nindex2);
                            objBETipoArchivo.Extencion = sqlDr.GetString(nIndex3);
                            objBETipoArchivo.IdTipoProceso = sqlDr.GetInt32(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBETipoArchivo.Longitud = NullTypes.IntegerNull; } else { objBETipoArchivo.Longitud = sqlDr.GetInt32(nIndex5); }
                            objBETipoArchivo.NombreArchivo = sqlDr.GetString(nIndex6);
                            objBETipoArchivo.DescProceso = sqlDr.GetString(nIndex7);

                            lstBETipoArchivo.Add(objBETipoArchivo);
                        }

                        sqlDr.Close();
                    }
                }

            }

            return lstBETipoArchivo;
        }

        
        #endregion        
    }
}
