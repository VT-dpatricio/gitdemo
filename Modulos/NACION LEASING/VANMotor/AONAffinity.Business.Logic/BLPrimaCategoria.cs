﻿//OK
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;

//using AONAffinity.Resources;
//using AONAffinity.Business.Entity.BDMotor;
//using AONAffinity.Data.DataAccess.BDMotor;

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLPrimaCategoria
    {
        #region NoTransaccional
        public BEPrimaCategoria Obtener(Int32 pnIdCategoria) 
        {
            BEPrimaCategoria objBEPrimaCategoria = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAPrimaCategoria objDAPrimaCategoria = new DAPrimaCategoria();
                SqlDataReader sqlDr = objDAPrimaCategoria.Obtener(pnIdCategoria, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idCategoria");
                        Int32 nIndex2 = sqlDr.GetOrdinal("consecutivo");
                        Int32 nIndex3 = sqlDr.GetOrdinal("montoPrima");
                        Int32 nIndex4 = sqlDr.GetOrdinal("fecIniVigencia");
                        Int32 nIndex5 = sqlDr.GetOrdinal("fecFinVigencia");
                        Int32 nIndex6 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex8 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex9 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex10 = sqlDr.GetOrdinal("stsRegistro");

                        objBEPrimaCategoria = new BEPrimaCategoria();
                             
                        if (sqlDr.Read()) 
                        {
                            objBEPrimaCategoria.IdCategoria = sqlDr.GetInt32(nIndex1);
                            objBEPrimaCategoria.Consecutivo = sqlDr.GetInt32(nIndex2);
                            objBEPrimaCategoria.MontoPrima = sqlDr.GetDecimal(nIndex3);
                            objBEPrimaCategoria.FecIniVigencia = sqlDr.GetDateTime(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBEPrimaCategoria.FecFinVigencia = NullTypes.FechaNull; } else { objBEPrimaCategoria.FecFinVigencia = sqlDr.GetDateTime(nIndex5); }
                            objBEPrimaCategoria.UsuarioCreacion = sqlDr.GetString(nIndex6);
                            objBEPrimaCategoria.FechaCreacion = sqlDr.GetDateTime(nIndex7);
                            if (sqlDr.IsDBNull(nIndex8)) { objBEPrimaCategoria.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEPrimaCategoria.UsuarioModificacion = sqlDr.GetString(nIndex8); }
                            if (sqlDr.IsDBNull(nIndex9)) { objBEPrimaCategoria.FechaModificacion = NullTypes.FechaNull; } else { objBEPrimaCategoria.FechaModificacion = sqlDr.GetDateTime(nIndex9); }
                            objBEPrimaCategoria.EstadoRegistro = sqlDr.GetBoolean(nIndex10);     
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return objBEPrimaCategoria;
        }
        #endregion
    }
}
