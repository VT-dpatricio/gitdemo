﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.BusinessLogic
{
    /// <summary>
    /// Clase de lógica del negocio de la tabla Modelo.
    /// </summary>
    public class BLModelo
    {
        #region Transaccional
        public Int32 Insertar(BEModelo pObjBEModelo)
        {
            Int32 nResult = 0;
            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();
                DAModelo objDAModelo = new DAModelo();
                nResult = objDAModelo.Insertar(pObjBEModelo, sqlCn);
            }
            return nResult;
        }

        public Int32 Actualizar(BEModelo pObjBEModelo)
        {
            Int32 nResult = 0;
            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();
                DAModelo objDAModelo = new DAModelo();
                nResult = objDAModelo.Actualizar(pObjBEModelo, sqlCn);
            }
            return nResult;
        }


        #endregion

        #region NoTransaccional
        public BEModelo Obtener(Int32 pnIdModelo)
        {
            BEModelo objBEModelo = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAModelo objDAModelo = new DAModelo();
                SqlDataReader sqlDr = objDAModelo.Obtener(pnIdModelo, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idModelo");
                        Int32 nIndex2 = sqlDr.GetOrdinal("desModelo");
                        Int32 nIndex3 = sqlDr.GetOrdinal("idClase");
                        Int32 nIndex4 = sqlDr.GetOrdinal("codExtModelo");
                        Int32 nIndex5 = sqlDr.GetOrdinal("descClase");

                        if (sqlDr.Read())
                        {
                            objBEModelo = new BEModelo();
                            objBEModelo.IdModelo = sqlDr.GetInt32(nIndex1);
                            objBEModelo.Descripcion = sqlDr.GetString(nIndex2);
                            objBEModelo.IdClase = sqlDr.GetInt32(nIndex3);
                            objBEModelo.CodExterno = sqlDr.GetString(nIndex4);
                            objBEModelo.DesClase = sqlDr.GetString(nIndex5); 
                        }

                        sqlDr.Close();
                    }
                }
            }

            return objBEModelo;
        }

        public List<BEModelo> ListarNoAseg() 
        {
            List<BEModelo> lstBEModelo = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAModelo objDAModelo = new DAModelo();
                SqlDataReader sqlDr = objDAModelo.ListarNoAseg(sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idModelo");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idMarca");
                        Int32 nIndex3 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex4 = sqlDr.GetOrdinal("idClase");
                        Int32 nIndex5 = sqlDr.GetOrdinal("codExterno");
                        Int32 nIndex6 = sqlDr.GetOrdinal("asegurable");
                        Int32 nIndex7 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex8 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex9 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex10 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex11 = sqlDr.GetOrdinal("stsRegistro");

                        lstBEModelo = new List<BEModelo>();

                        while (sqlDr.Read()) 
                        {
                            BEModelo objBEModelo = new BEModelo();

                            objBEModelo.IdModelo = sqlDr.GetInt32(nIndex1);
                            objBEModelo.IdMarca = sqlDr.GetInt32(nIndex2);
                            objBEModelo.Descripcion = sqlDr.GetString(nIndex3);
                            objBEModelo.IdClase = sqlDr.GetInt32(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBEModelo.CodExterno = NullTypes.CadenaNull; } else { objBEModelo.CodExterno = sqlDr.GetString(nIndex5); }
                            objBEModelo.Asegurable = sqlDr.GetBoolean(nIndex6);
                            objBEModelo.UsuarioCreacion = sqlDr.GetString(nIndex7);
                            objBEModelo.FechaCreacion = sqlDr.GetDateTime(nIndex8);
                            if (sqlDr.IsDBNull(nIndex9)) { objBEModelo.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEModelo.UsuarioModificacion = sqlDr.GetString(nIndex9); }
                            if (sqlDr.IsDBNull(nIndex10)) { objBEModelo.FechaModificacion = NullTypes.FechaNull; } else { objBEModelo.FechaModificacion = sqlDr.GetDateTime(nIndex10); }
                            objBEModelo.EstadoRegistro = sqlDr.GetBoolean(nIndex11);

                            lstBEModelo.Add(objBEModelo); 
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return lstBEModelo;
        }

        public List<BEModelo> ListarxMarcaDesc(Int32 pnIdMarca, String pcDescripcion, Nullable<Boolean> pbStsRegistro) 
        {
            List<BEModelo> lstBEModelo = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAModelo objDAModelo = new DAModelo();
                SqlDataReader sqlDr = objDAModelo.ListarxMarcaDesc(pnIdMarca, pcDescripcion, pbStsRegistro, sqlCn);
                lstBEModelo = new List<BEModelo>();
                BEModelo objBEModelo = new BEModelo();
                objBEModelo.EstadoRegistro = true;
                objBEModelo.Asegurable = true;
                lstBEModelo.Add(objBEModelo); 
                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idModelo");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idMarca");
                        Int32 nIndex3 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex4 = sqlDr.GetOrdinal("idClase");
                        Int32 nIndex5 = sqlDr.GetOrdinal("descClase");  
                        Int32 nIndex6 = sqlDr.GetOrdinal("codExterno");
                        Int32 nIndex7 = sqlDr.GetOrdinal("asegurable");
                        Int32 nIndex8 = sqlDr.GetOrdinal("idTipoVehiculo");
                        Int32 nIndex9 = sqlDr.GetOrdinal("descTipoVehiculo");
                        Int32 nIndex10 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex11 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex12 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex13 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex14 = sqlDr.GetOrdinal("stsRegistro");

                
                        while (sqlDr.Read()) 
                        {
                            objBEModelo = new BEModelo();

                            objBEModelo.IdModelo = sqlDr.GetInt32(nIndex1);
                            objBEModelo.IdMarca = sqlDr.GetInt32(nIndex2);
                            objBEModelo.Descripcion = sqlDr.GetString(nIndex3);
                            objBEModelo.IdClase = sqlDr.GetInt32(nIndex4);
                            objBEModelo.DesClase = sqlDr.GetString(nIndex5);    
                            if (sqlDr.IsDBNull(nIndex6)) { objBEModelo.CodExterno = NullTypes.CadenaNull; } else { objBEModelo.CodExterno = sqlDr.GetString(nIndex6); }
                            objBEModelo.Asegurable = sqlDr.GetBoolean(nIndex7);
                            objBEModelo.IdTipoVehiculo = sqlDr.GetInt32(nIndex8);
                            objBEModelo.DesTipoVehiculo = sqlDr.GetString(nIndex9);   
                            objBEModelo.UsuarioCreacion = sqlDr.GetString(nIndex10);
                            objBEModelo.FechaCreacion = sqlDr.GetDateTime(nIndex11);
                            if (sqlDr.IsDBNull(nIndex12)) { objBEModelo.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEModelo.UsuarioModificacion = sqlDr.GetString(nIndex12); }
                            if (sqlDr.IsDBNull(nIndex13)) { objBEModelo.FechaModificacion = NullTypes.FechaNull; } else { objBEModelo.FechaModificacion = sqlDr.GetDateTime(nIndex13); }
                            objBEModelo.EstadoRegistro = sqlDr.GetBoolean(nIndex14);

                            lstBEModelo.Add(objBEModelo); 
                        }
                    }
                }
            }

            return lstBEModelo;
        }

        public BEModelo ObtenerPorCodExterno(String pcCodExternoMod, String pcCodExternoMar, Nullable<Boolean> pbAsegurable, Nullable<Boolean> pbStsRegistro, String pcCodCIA) 
        {
            BEModelo objBEModelo = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAModelo objDAModelo = new DAModelo();
                SqlDataReader sqlDr = objDAModelo.ObtenerPorCodExterno(pcCodExternoMod, pcCodExternoMar, pbAsegurable, pbStsRegistro, pcCodCIA, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idModelo");
                        Int32 nIndex2 = sqlDr.GetOrdinal("desModelo");
                        Int32 nIndex3 = sqlDr.GetOrdinal("idClase");
                        Int32 nIndex4 = sqlDr.GetOrdinal("codExtModelo");
                        Int32 nIndex5 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex6 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex8 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex9 = sqlDr.GetOrdinal("stsRegistro");
                        Int32 nIndex10 = sqlDr.GetOrdinal("asegurable");
                        Int32 nIndex11 = sqlDr.GetOrdinal("idTipoVehiculo");
                        Int32 nIndex12 = sqlDr.GetOrdinal("idMarca");
                        Int32 nIndex13 = sqlDr.GetOrdinal("desMarca");
                        Int32 nIndex14 = sqlDr.GetOrdinal("codExtMarca");

                        if (sqlDr.Read()) 
                        {
                            objBEModelo = new BEModelo();
                            objBEModelo.IdModelo = sqlDr.GetInt32(nIndex1);
                            objBEModelo.Descripcion = sqlDr.GetString(nIndex2);
                            objBEModelo.IdClase = sqlDr.GetInt32(nIndex3);
                            objBEModelo.CodExterno = sqlDr.GetString(nIndex4);
                            objBEModelo.UsuarioCreacion = sqlDr.GetString(nIndex5);
                            objBEModelo.FechaCreacion = sqlDr.GetDateTime(nIndex6);
                            if (sqlDr.IsDBNull(nIndex7)) { objBEModelo.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEModelo.UsuarioModificacion = sqlDr.GetString(nIndex7); }
                            if (sqlDr.IsDBNull(nIndex8)) { objBEModelo.FechaModificacion = NullTypes.FechaNull; } else { objBEModelo.FechaModificacion = sqlDr.GetDateTime(nIndex8); }
                            objBEModelo.EstadoRegistro = sqlDr.GetBoolean(nIndex9);
                            objBEModelo.Asegurable = sqlDr.GetBoolean(nIndex10);
                            objBEModelo.IdTipoVehiculo = sqlDr.GetInt32(nIndex11);
                            objBEModelo.IdMarca = sqlDr.GetInt32(nIndex12);
                            objBEModelo.DesMarca = sqlDr.GetString(nIndex13);
                            objBEModelo.CodExtMarca = sqlDr.GetString(nIndex14);   
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return objBEModelo;
        }

        public List<BEModelo> ListarxMarcaClaseTipo(Int32 pnIdMarca, Int32 pnIdClase, Int32 pnIdTipoVehiculo, Nullable<Boolean> pbStsRegistro)
        {
            List<BEModelo> lstBEModelo = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAModelo objDAModelo = new DAModelo();
                SqlDataReader sqlDr = objDAModelo.ListarxMarcaClaseTipo(pnIdMarca, pnIdClase, pnIdTipoVehiculo, pbStsRegistro, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idModelo");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idMarca");
                        Int32 nIndex3 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex4 = sqlDr.GetOrdinal("idClase");
                        Int32 nIndex5 = sqlDr.GetOrdinal("descClase");
                        Int32 nIndex6 = sqlDr.GetOrdinal("codExterno");
                        Int32 nIndex7 = sqlDr.GetOrdinal("asegurable");
                        Int32 nIndex8 = sqlDr.GetOrdinal("idTipoVehiculo");
                        Int32 nIndex9 = sqlDr.GetOrdinal("descTipoVehiculo");
                        Int32 nIndex10 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex11 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex12 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex13 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex14 = sqlDr.GetOrdinal("stsRegistro");

                        lstBEModelo = new List<BEModelo>();

                        while (sqlDr.Read())
                        {
                            BEModelo objBEModelo = new BEModelo();

                            objBEModelo.IdModelo = sqlDr.GetInt32(nIndex1);
                            objBEModelo.IdMarca = sqlDr.GetInt32(nIndex2);
                            objBEModelo.Descripcion = sqlDr.GetString(nIndex3);
                            objBEModelo.IdClase = sqlDr.GetInt32(nIndex4);
                            //objBEModelo.DesClase = sqlDr.GetString(nIndex5);
                            if (sqlDr.IsDBNull(nIndex6)) { objBEModelo.CodExterno = NullTypes.CadenaNull; } else { objBEModelo.CodExterno = sqlDr.GetString(nIndex6); }
                            objBEModelo.Asegurable = sqlDr.GetBoolean(nIndex7);
                            objBEModelo.IdTipoVehiculo = sqlDr.GetInt32(nIndex8);
                            //objBEModelo.DesTipoVehiculo = sqlDr.GetString(nIndex9);
                            objBEModelo.UsuarioCreacion = sqlDr.GetString(nIndex10);
                            objBEModelo.FechaCreacion = sqlDr.GetDateTime(nIndex11);
                            if (sqlDr.IsDBNull(nIndex12)) { objBEModelo.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEModelo.UsuarioModificacion = sqlDr.GetString(nIndex12); }
                            if (sqlDr.IsDBNull(nIndex13)) { objBEModelo.FechaModificacion = NullTypes.FechaNull; } else { objBEModelo.FechaModificacion = sqlDr.GetDateTime(nIndex13); }
                            objBEModelo.EstadoRegistro = sqlDr.GetBoolean(nIndex14);

                            lstBEModelo.Add(objBEModelo);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBEModelo;
        }

        public List<BEModelo> ListarxMarcaTipo(Int32 pnIdProducto, Int32 pnIdMarca, Int32 pnIdTipoVehiculo, Boolean? pbStsRegistro) 
        {
            List<BEModelo> lstBEModelo = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAModelo objDAModelo = new DAModelo();
                SqlDataReader sqlDr = objDAModelo.ListarxMarcaTipo(pnIdProducto, pnIdMarca, pnIdTipoVehiculo, pbStsRegistro, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idModelo");
                        Int32 nIndex2 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex4 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex5 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex6 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("stsRegistro");  

                        lstBEModelo = new List<BEModelo>();

                        while (sqlDr.Read()) 
                        {
                            BEModelo objBEModelo = new BEModelo();
                            objBEModelo.IdModelo = sqlDr.GetInt32(nIndex1);
                            objBEModelo.Descripcion = sqlDr.GetString(nIndex2);
                            objBEModelo.UsuarioCreacion = sqlDr.GetString(nIndex3);
                            objBEModelo.FechaCreacion = sqlDr.GetDateTime(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBEModelo.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEModelo.UsuarioModificacion = sqlDr.GetString(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBEModelo.FechaModificacion = NullTypes.FechaNull; } else { objBEModelo.FechaCreacion = sqlDr.GetDateTime(nIndex6); }
                            objBEModelo.EstadoRegistro = sqlDr.GetBoolean(nIndex7);

                            lstBEModelo.Add(objBEModelo);  
                        }

                        sqlDr.Close(); 
                    }
                }

            }

            return lstBEModelo;
        }  

            /*
             public SqlDataReader ListarxMarcaTipo(Int32 pnIdProducto, Int32 pnIdMarca, Int32 pnIdTipoVehiculo, Boolean? pbStsRegistro, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Modelo_ObtenerxMarcaTipo]", pSqlCn)) 
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idMarca", SqlDbType.Int);
                sqlParam2.Value = pnIdMarca;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@idTipoVehiculo", SqlDbType.Int);
                sqlParam3.Value = pnIdTipoVehiculo;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@stsRegistro", SqlDbType.Bit);
                if (pbStsRegistro == null) { sqlParam4.Value = DBNull.Value; } else { sqlParam4.Value = pnIdTipoVehiculo; }

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }
             */
        #endregion

        #region NoUso
        /// <summary>
        /// Permite listar los modelos de automoviles.
        /// </summary>
        /// <param name="pbStsRegistro">Estado del registro.</param>
        /// <returns>Objeto List de tipo BEModelo.</returns>
        public List<BEModelo> Listar(Nullable<Boolean> pbStsRegistro)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-24
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            List<BEModelo> lstBEModelo = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAModelo objDAModelo = new DAModelo();
                SqlDataReader sqlDr = objDAModelo.Listar(pbStsRegistro, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idModelo");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idMarca");
                        Int32 nIndex3 = sqlDr.GetOrdinal("desMarca");
                        Int32 nIndex4 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex5 = sqlDr.GetOrdinal("idClase");
                        Int32 nIndex6 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex8 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex9 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex10 = sqlDr.GetOrdinal("stsRegistro");

                        lstBEModelo = new List<BEModelo>();

                        while (sqlDr.Read())
                        {
                            BEModelo objBEModelo = new BEModelo();

                            objBEModelo.IdModelo = sqlDr.GetInt32(nIndex1);
                            objBEModelo.IdMarca = sqlDr.GetInt32(nIndex2);
                            objBEModelo.DesMarca = sqlDr.GetString(nIndex3);
                            objBEModelo.Descripcion = sqlDr.GetString(nIndex4);
                            objBEModelo.IdClase = sqlDr.GetInt32(nIndex5);
                            objBEModelo.UsuarioCreacion = sqlDr.GetString(nIndex6);
                            objBEModelo.FechaCreacion = sqlDr.GetDateTime(nIndex7);
                            if (sqlDr.IsDBNull(nIndex8)) { objBEModelo.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEModelo.UsuarioModificacion = sqlDr.GetString(nIndex8); }
                            if (sqlDr.IsDBNull(nIndex9)) { objBEModelo.FechaModificacion = NullTypes.FechaNull; } else { objBEModelo.FechaModificacion = sqlDr.GetDateTime(nIndex9); }
                            objBEModelo.EstadoRegistro = sqlDr.GetBoolean(nIndex10);

                            lstBEModelo.Add(objBEModelo);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBEModelo;
        }

        /// <summary>
        /// Permite obtener los modelos por marca.
        /// </summary>
        /// <param name="pLstBEModelo">Lista de modelos.</param>
        /// <param name="pnIdMarca">Código de marca.</param>
        /// <returns>Objeto List de tipo BEModelo.</returns>
        public List<BEModelo> ListarxMarca(List<BEModelo> pLstBEModelo, Int32 pnIdMarca, Int32 pnIdClase)
        {
            List<BEModelo> lstBEModelo = null;

            if (pLstBEModelo != null)
            {
                lstBEModelo = new List<BEModelo>();
                BEModelo objBEModelo_tmp = new BEModelo();
                objBEModelo_tmp.IdModelo = 0;
                objBEModelo_tmp.Descripcion = "-Seleccione-";

                lstBEModelo.Insert(0, objBEModelo_tmp);

                foreach (BEModelo objBEModelo in pLstBEModelo)
                {
                    if (objBEModelo.IdMarca == pnIdMarca && objBEModelo.IdClase == pnIdClase)
                    {
                        lstBEModelo.Add(objBEModelo);
                    }
                }
            }

            return lstBEModelo;
        }


        /// <summary>
        /// Permite obtener los modelos por marca.
        /// </summary>
        /// <param name="pLstBEModelo">Lista de modelos.</param>
        /// <param name="pnIdMarca">Código de marca.</param>
        /// <returns>Objeto List de tipo BEModelo.</returns>
        public List<BEModelo> ListarxMarca(List<BEModelo> pLstBEModelo, Int32 pnIdMarca)
        {
            List<BEModelo> lstBEModelo = null;

            if (pLstBEModelo != null)
            {
                lstBEModelo = new List<BEModelo>();
                //BEModelo objBEModelo_tmp = new BEModelo();
                //objBEModelo_tmp.IdModelo = 0;
                //objBEModelo_tmp.Descripcion = "-Seleccione-";

                //lstBEModelo.Insert(0, objBEModelo_tmp);

                foreach (BEModelo objBEModelo in pLstBEModelo)
                {
                    if (objBEModelo.IdMarca == pnIdMarca)
                    {
                        lstBEModelo.Add(objBEModelo);
                    }
                }
            }

            return lstBEModelo;
        } 
        #endregion
    }
}
