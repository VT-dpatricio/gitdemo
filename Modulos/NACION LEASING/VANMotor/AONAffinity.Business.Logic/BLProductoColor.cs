﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;
using AONAffinity.Library.Resources;

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLProductoColor
    {
        #region NoTransaccional
        public List<BEProductoColor> ListarxProducto(Int32 pnIdproducto, Boolean? pbStsRegistro)
        {
            List<BEProductoColor> lstBEProductoColor = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAProductoColor objDAProductoColor = new DAProductoColor();
                SqlDataReader sqlDr = objDAProductoColor.ListarxProducto(pnIdproducto, pbStsRegistro, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idColor");
                        Int32 nIndex3 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex4 = sqlDr.GetOrdinal("codExterno");

                        lstBEProductoColor = new List<BEProductoColor>();

                        while (sqlDr.Read()) 
                        {
                            BEProductoColor objBEProductoColor = new BEProductoColor();
                            objBEProductoColor.IdProducto = sqlDr.GetInt32(nIndex1);
                            objBEProductoColor.IdColor = sqlDr.GetInt32(nIndex2);
                            objBEProductoColor.Descripcion = sqlDr.GetString(nIndex3);
                            if (sqlDr.IsDBNull(nIndex4)) { objBEProductoColor.CodExterno = NullTypes.CadenaNull; } else { objBEProductoColor.CodExterno = sqlDr.GetString(nIndex4); }

                            lstBEProductoColor.Add(objBEProductoColor);  
                        }

                        sqlDr.Close(); 
                    }
                }                
            }

            return lstBEProductoColor;
        }
        #endregion
    }
}
