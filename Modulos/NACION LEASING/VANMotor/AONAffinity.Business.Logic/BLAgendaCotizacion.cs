﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Motor.Resources;
using AONAffinity.Library.Resources;
using AONAffinity.Library.DataAccess;
using AONAffinity.Library.DataAccess.BDMotor;
using AONAffinity.Library.BusinessEntity.BDMotor; 

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLAgendaCotizacion
    {
        public Int32 Insertar(BEAgendaCotizacion pObjBEAgendaCotizacion) 
        {
            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAAgendaCotizacion objDAAgendaCotizacion = new DAAgendaCotizacion();
                nResult = objDAAgendaCotizacion.Insertar(pObjBEAgendaCotizacion, sqlCn);
            }

            return nResult;
        }
    }
}
