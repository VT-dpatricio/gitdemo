﻿using System;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.BusinessEntity;
using AONAffinity.Motor.DataAccess;  
//using AONAffinity.Library.DataAccess;     

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLCategoriaModelo
    {
        #region NoTransaccional
        public String ValidarVehiculo(Int32 pnIdProducto, Int32 pnIdModelo, Int32 pnAnioFab)
        {
            String cRespuesta = String.Empty;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DACategoriaModelo objDACategoriaModelo = new DACategoriaModelo();
                SqlDataReader sqlDr = objDACategoriaModelo.ValidarVehiculo(pnIdProducto, pnIdModelo, pnAnioFab, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex = sqlDr.GetOrdinal("Respuesta");

                        if (sqlDr.Read())
                        {
                            cRespuesta = sqlDr.GetString(nIndex);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return cRespuesta;
        }

        public BECategoriaModelo Obtener(Int32 pnIdProducto, Int32 pnIdModelo)
        {
            BECategoriaModelo objBECategoriaModelo = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DACategoriaModelo objDACategoriaModelo = new DACategoriaModelo();
                SqlDataReader sqlDr = objDACategoriaModelo.Obtener(pnIdProducto, pnIdModelo, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idCategoria");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idModelo");
                        Int32 nIndex3 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex4 = sqlDr.GetOrdinal("codExterno");
                        Int32 nIndex5 = sqlDr.GetOrdinal("codSubModelo");
                        Int32 nIndex6 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex8 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex9 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex10 = sqlDr.GetOrdinal("stsRegistro");
                        Int32 nIndex11 = sqlDr.GetOrdinal("TimonCambiado");
                        Int32 nIndex12 = sqlDr.GetOrdinal("descModelo");
                        Int32 nIndex13 = sqlDr.GetOrdinal("descSubModelo"); 

                        objBECategoriaModelo = new BECategoriaModelo();

                        if (sqlDr.Read()) 
                        {
                            objBECategoriaModelo.IdCategoria = sqlDr.GetInt32(nIndex1);
                            objBECategoriaModelo.IdModelo = sqlDr.GetInt32(nIndex2);
                            objBECategoriaModelo.IdIdproducto = sqlDr.GetInt32(nIndex3);
                            if (sqlDr.IsDBNull(nIndex4)) { objBECategoriaModelo.CodExterno = NullTypes.CadenaNull; } else { objBECategoriaModelo.CodExterno = sqlDr.GetString(nIndex4); }
                            if (sqlDr.IsDBNull(nIndex5)) { objBECategoriaModelo.CodSubModelo = NullTypes.CadenaNull; } else { objBECategoriaModelo.CodSubModelo = sqlDr.GetString(nIndex5); }
                            objBECategoriaModelo.UsuarioCreacion = sqlDr.GetString(nIndex6);
                            objBECategoriaModelo.FechaCreacion = sqlDr.GetDateTime(nIndex7);
                            if (sqlDr.IsDBNull(nIndex8)) { objBECategoriaModelo.UsuarioModificacion = NullTypes.CadenaNull; } else { objBECategoriaModelo.UsuarioModificacion = sqlDr.GetString(nIndex8); }
                            if (sqlDr.IsDBNull(nIndex9)) { objBECategoriaModelo.FechaModificacion = NullTypes.FechaNull; } else { objBECategoriaModelo.FechaModificacion = sqlDr.GetDateTime(nIndex9); }
                            objBECategoriaModelo.EstadoRegistro = sqlDr.GetBoolean(nIndex10);
                            if (sqlDr.IsDBNull(nIndex11)) { objBECategoriaModelo.TimonCambiado = null; } else { objBECategoriaModelo.TimonCambiado = sqlDr.GetBoolean(nIndex11); }
                            if (sqlDr.IsDBNull(nIndex12)) { objBECategoriaModelo.DescModelo = NullTypes.CadenaNull; } else { objBECategoriaModelo.DescModelo = sqlDr.GetString(nIndex12); }
                            if (sqlDr.IsDBNull(nIndex13)) { objBECategoriaModelo.DescSubModelo = NullTypes.CadenaNull; } else { objBECategoriaModelo.DescSubModelo = sqlDr.GetString(nIndex13); }
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return objBECategoriaModelo;
        }
        #endregion         
    }
}
