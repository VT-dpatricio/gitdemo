﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.DataAccess.Bais;
using AONAffinity.Motor.BusinessEntity.Bais;

namespace AONAffinity.Motor.BusinessLogic.Bais
{
    public class BLCiudad
    {
        #region NoTransaccional
        /// <summary>
        /// Permite listar las ciudades por país.
        /// </summary>
        /// <param name="pnIdPais">Código de país.</param>
        /// <returns>La lista de ciudades por país.</returns>
        public List<BECiudad> Listar(Int32 pnIdPais)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-09-14
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            List<BECiudad> lstBECiudad = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus()))
            {
                sqlCn.Open();

                DACiudad objDACiudad = new DACiudad();                 
                SqlDataReader sqlDr = objDACiudad.Listar(pnIdPais, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idCiudad");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idDepartamento");
                        Int32 nIndex3 = sqlDr.GetOrdinal("nombre");
                        Int32 nIndex4 = sqlDr.GetOrdinal("idProvincia");  
                        Int32 nIndex5 = sqlDr.GetOrdinal("Provincia");

                        lstBECiudad = new List<BECiudad>();

                        while (sqlDr.Read())
                        {
                            BECiudad objBECiudad = new BECiudad();

                            objBECiudad.IdCiudad = sqlDr.GetInt32(nIndex1);
                            objBECiudad.IdDepartamento = sqlDr.GetInt32(nIndex2);
                            objBECiudad.Nombre = sqlDr.GetString(nIndex3);
                            objBECiudad.IdProvincia = sqlDr.GetString(nIndex4);
                            objBECiudad.Provincia = sqlDr.GetString(nIndex5);

                            lstBECiudad.Add(objBECiudad);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBECiudad;
        }

        public List<BECiudad> ListarProvincia(Int32 pnIdDepartamento, List<BECiudad> pLstBECiudad) 
        {
            List<BECiudad> lstBECiudad = null;

            if (pLstBECiudad != null) 
            {
                lstBECiudad = new List<BECiudad>();

                foreach (BECiudad objBECiudad in pLstBECiudad)
                {
                    if (objBECiudad.IdDepartamento == pnIdDepartamento) 
                    {
                        if (!ExisteProvincia(objBECiudad, lstBECiudad))
                        {
                            lstBECiudad.Add(objBECiudad); 
                        }
                    }                    
                }
            }           

            return lstBECiudad;
        }

        public List<BECiudad> ListarDistrito(String pcIdProvincia, List<BECiudad> pLstBECiudad) 
        {
            List<BECiudad> lstBECiudad = null;

            if (pLstBECiudad != null)             
            {
                lstBECiudad = new List<BECiudad>();

                foreach (BECiudad objBECiudad in pLstBECiudad) 
                {
                    if (objBECiudad.IdProvincia == pcIdProvincia) 
                    {
                        lstBECiudad.Add(objBECiudad); 
                    }
                }
            }

            return lstBECiudad;
        }

        private Boolean ExisteProvincia(BECiudad pObjBECiudad, List<BECiudad> pLstBECiudad)
        {
            Boolean bResult = false;

            foreach (BECiudad objBECiudad in pLstBECiudad) 
            {
                if (pObjBECiudad.IdProvincia == objBECiudad.IdProvincia) 
                {
                    return true;
                }
            }

            return bResult;
        }

        public String ObtenerIdDepartamento(String pcIdCiudad) 
        {   //P  D  P  D
            //51 01 06 005
            String cIdDepartamento = "";

            if (pcIdCiudad.Length >= 4)
            {
                cIdDepartamento = pcIdCiudad.Substring(0, 4);  
            }

            return cIdDepartamento;
        }

        public String ObtenerIdProvincia(String pnIdCiudad)
        {
            String cIdProvincia = "";

            if (pnIdCiudad.ToString().Length >= 6)
            {                
                cIdProvincia = pnIdCiudad.ToString().Substring(0,6);
            }

            return cIdProvincia;
        }

        public BECiudad Obtener(Int32 pnIdciudad) 
        {
            BECiudad objBECiudad = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus())) 
            {
                sqlCn.Open();

                DACiudad objDACiudad = new DACiudad();
                SqlDataReader sqlDr = objDACiudad.Obtener(pnIdciudad, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idCiudad");
                        Int32 nIndex2 = sqlDr.GetOrdinal("nombre");

                        if (sqlDr.Read()) 
                        {
                            objBECiudad = new BECiudad();
                            objBECiudad.IdCiudad = sqlDr.GetInt32(nIndex1);
                            objBECiudad.Nombre = sqlDr.GetString(nIndex2);  
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return objBECiudad;
        }
        #endregion                       
    }
}
