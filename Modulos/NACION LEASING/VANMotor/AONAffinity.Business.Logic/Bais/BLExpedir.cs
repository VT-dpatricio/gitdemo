﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using AONAffinity.Business.Entity.BDJanus;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Data.DataAccess.BDJanus;


namespace AONAffinity.Motor.BusinessLogic.Bais
{
    public class BLExpedir
    {
        public List<BEExpedir> BLExpedirCertificado(BECertificado pObjBECertificado, List<BEAsegurado> pLstBEAsegurado, List<BEBeneficiario> pLstBEBeneficiario, List<BEInfoProductoC> pLstInfoProductoC, List<BEInfoAseguradoC> pLstBEInfoAseguradoC, String pNumCertFisico)
        {
            List<BEExpedir> lstValidaCertificado;
            List<BEExpedir> lstValidaAsegurado;
            List<BEExpedir> lstBEExpedir = new List<BEExpedir>();
            BEExpedir objBEExpedir;
            Boolean exito = false;
            SqlTransaction sqlTranx;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus()))
            {
                sqlCn.Open();

                /*****************VALIDAR LA INFORMACION DEL CERTIFICADO****************/
                DAExpedir objDAExpedir = new DAExpedir();
                lstValidaCertificado = this.ValidaExpedirCertificado(pObjBECertificado.IdProducto, pNumCertFisico, pObjBECertificado.CcCliente, pObjBECertificado.NumeroCuenta, sqlCn);

                if (lstValidaCertificado != null)
                {
                    if (lstValidaCertificado.Count > 0)
                    {
                        return lstValidaCertificado;
                    }
                }

                /*****************VALIDAR LA INFORMACION DEL ASEGURADO******************/
                foreach (BEAsegurado objBEAseg in pLstBEAsegurado)
                {
                    lstValidaAsegurado = this.ValidaExpedirAsegurado(pObjBECertificado.IdProducto, objBEAseg.Ccaseg, sqlCn);

                    if (lstValidaAsegurado != null)
                    {
                        if (lstValidaAsegurado.Count > 0)
                        {
                            return lstValidaAsegurado;
                        }
                    }
                }

                sqlTranx = sqlCn.BeginTransaction();

                /**********INSERCION DEL CERTIFICADO****************/
                DACertificado objDACertificado = new DACertificado();
                Int32 rpta = objDACertificado.Insertar(pObjBECertificado, sqlCn, sqlTranx);

                if (rpta != -1)
                {
                    exito = true;
                    //DAInfoProductoC objDAInfoProductoC = new DAInfoProductoC();
                    //foreach (BEInfoProductoC objInfPc in pLstInfoProductoC)
                    //{
                    //    /**********INSERCION DE LOS INFOPRODUCTOS****************/
                    //    rpta = objDAInfoProductoC.DAInsertaInfoProductoC(objInfPc, sqlCon, sqlTranx);
                    //    if (rpta == -1)
                    //    {
                    //        exito = false;
                    //        break;
                    //    }
                    //    else
                    //    {
                    //        exito = true;
                    //    }
                    //}

                    if (exito)
                    {
                        DAAsegurado objDAAsegurado = new DAAsegurado();
                        foreach (BEAsegurado objAseg in pLstBEAsegurado)
                        {
                            /**********INSERCION DE LOS ASEGURADOS****************/
                            rpta = objDAAsegurado.Insertar(objAseg, sqlCn, sqlTranx);
                            if (rpta == -1)
                            {
                                exito = false;
                                break;
                            }
                            else
                            {
                                exito = true;
                            }
                        }
                        if (exito)
                        {
                            //DAInfoAseguradoC objDAInfAsegC = new DAInfoAseguradoC();
                            //foreach (BEInfoAseguradoC objInfAsegC in pLstBEInfoAseguradoC)
                            //{
                            //    /**********INSERCION DE LOS INFOASEGURADOS****************/
                            //    rpta = objDAInfAsegC.DAInsertaInfoAseguradoC(objInfAsegC, pObjBECertificado.IdProducto,
                            //        sqlCon, sqlTranx);
                            //    if (rpta == -1)
                            //    {
                            //        exito = false;
                            //        break;
                            //    }
                            //    else
                            //    {
                            //        exito = true;
                            //    }
                            //}
                            //if (exito)
                            //{
                            //    DABeneficiario objDABenef = new DABeneficiario();
                            //    foreach (BEBeneficiario objBenef in pLstBEBeneficiario)
                            //    {
                            //        /**********INSERCION DE LOS BENEFICIARIOS****************/
                            //        rpta = objDABenef.DAInsertarBeneficiario(objBenef, sqlCon, sqlTranx);

                            //        if (rpta == -1)
                            //        {
                            //            exito = false;
                            //            break;
                            //        }
                            //        else
                            //        {
                            //            exito = true;
                            //        }
                            //    }

                                //if (exito)
                                //{
                                    sqlTranx.Commit();
                                    objBEExpedir = new BEExpedir();
                                    objBEExpedir.Descripcion = "OK";
                                    objBEExpedir.Codigo = 0;
                                    lstBEExpedir.Add(objBEExpedir);
                                    return lstBEExpedir;
                                //}
                                //else
                                //{
                                //    sqlTranx.Rollback();
                                //    objBEExpedir = new BEExpedir();
                                //    objBEExpedir.Descripcion = "ER";
                                //    objBEExpedir.Codigo = 0;
                                //    lstBEExpedir.Add(objBEExpedir);
                                //    return lstBEExpedir;
                                //}
                            }
                            else
                            {
                                sqlTranx.Rollback();
                                objBEExpedir = new BEExpedir();
                                objBEExpedir.Descripcion = "ER";
                                objBEExpedir.Codigo = 0;
                                lstBEExpedir.Add(objBEExpedir);
                                return lstBEExpedir;
                            }
                        }//Retorna en caso de que falle la insercion de algun asegurado
                        else
                        {
                            sqlTranx.Rollback();
                            objBEExpedir = new BEExpedir();
                            objBEExpedir.Descripcion = "ER";
                            objBEExpedir.Codigo = 0;
                            lstBEExpedir.Add(objBEExpedir);
                            return lstBEExpedir;
                        }
                    }//Retorna en caso de que falle la insercion de algun infoproducto
                    else
                    {
                        sqlTranx.Rollback();
                        objBEExpedir = new BEExpedir();
                        objBEExpedir.Descripcion = "ER";
                        objBEExpedir.Codigo = 0;
                        lstBEExpedir.Add(objBEExpedir);
                        return lstBEExpedir;
                    }
                //}//Retorna en caso de que falle la insercion del certificado
                //else
                //{
                //    sqlTranx.Rollback();
                //    objBEExpedir = new BEExpedir();
                //    objBEExpedir.Descripcion = "ER";
                //    objBEExpedir.Codigo = 0;
                //    lstBEExpedir.Add(objBEExpedir);
                //    return lstBEExpedir;
                //}
            }
        }

        /*ObtieneResultadoExpValideCert*/
        private List<BEExpedir> ValidaExpedirCertificado(Int32 pnIdProducto, String pcNumCertFisico, String pcCcliente, String pcNumeroCuenta, SqlConnection pSqlCn)
        {
            List<BEExpedir> lstBEExpedir = null;

            DAExpedir objDAExpedir = new DAExpedir();

            using (SqlDataReader sqlDr = objDAExpedir.ValidaExpedirCertificado(pnIdProducto, pcNumCertFisico, pcCcliente, pcNumeroCuenta, pSqlCn))
            {
                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex2 = sqlDr.GetOrdinal("error");

                        lstBEExpedir = new List<BEExpedir>();
                        
                        while (sqlDr.Read())
                        {
                            BEExpedir objBEExpedir = new  BEExpedir();
                            objBEExpedir.Descripcion = sqlDr.GetString(nIndex1);
                            objBEExpedir.Codigo = sqlDr.GetInt32(nIndex2);
                            
                            lstBEExpedir.Add(objBEExpedir);
                        }                        
                    }
                }
            }
            return lstBEExpedir;
        }

        /*ObtieneResultadoExpValideAseg*/
        private List<BEExpedir> ValidaExpedirAsegurado(Int32 pnIdProducto, String pcCcaseg, SqlConnection pSqlCon)
        {
            List<BEExpedir> lstBEExpedir = null;

            DAExpedir objDAExpedir = new DAExpedir();

            using (SqlDataReader sqlDr = objDAExpedir.ValidaExpedirAsegurado(pnIdProducto, pcCcaseg, pSqlCon))
            {
                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex2 = sqlDr.GetOrdinal("error");

                        lstBEExpedir = new List<BEExpedir>();
                        
                        while (sqlDr.Read())
                        {
                            BEExpedir objBEExpedir = new BEExpedir();

                            objBEExpedir.Descripcion = sqlDr.GetString(nIndex1);
                            objBEExpedir.Codigo = sqlDr.GetInt32(nIndex2);

                            lstBEExpedir.Add(objBEExpedir);
                        }
                    }
                }
            }
            return lstBEExpedir;
        }
    }
}
