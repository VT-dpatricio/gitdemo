﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.DataAccess.Bais;
using AONAffinity.Motor.BusinessEntity.Bais;

namespace AONAffinity.Motor.BusinessLogic.Bais
{
    /// <summary>
    /// Clase de lógica del negocio de la tabla ProductoMedioPago.
    /// </summary>
    public class BLProductoMedioPago
    {
        #region NoTransaccional
        /// <summary>
        /// Permite listar los medios de pago por producto.
        /// </summary>
        /// <param name="pnIdProducto">Código de producto.</param>
        /// <returns>Objeto List de tipo BEProductoMedioPago.</returns>
        public List<BEProductoMedioPago> Listar(Int32 pnIdProducto)
        {
            /*---------------------------------------------------------------------------
            * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
            * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            List<BEProductoMedioPago> lstBEProductoMedioPago = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus()))
            {
                sqlCn.Open();

                DAProductoMedioPago objDAProductoMedioPago = new DAProductoMedioPago();
                SqlDataReader sqlDr = objDAProductoMedioPago.Listar(pnIdProducto, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idMedioPago");
                        Int32 nIndex3 = sqlDr.GetOrdinal("idMonedaCobro");
                        Int32 nIndex4 = sqlDr.GetOrdinal("desMonedacobro");
                        Int32 nIndex5 = sqlDr.GetOrdinal("desMedioPago");

                        lstBEProductoMedioPago = new List<BEProductoMedioPago>();

                        while (sqlDr.Read())
                        {
                            BEProductoMedioPago objBEProductoMedioPago = new BEProductoMedioPago();

                            objBEProductoMedioPago.IdProducto = sqlDr.GetInt32(nIndex1);
                            objBEProductoMedioPago.IdMedioPago = sqlDr.GetString(nIndex2);
                            objBEProductoMedioPago.IdMonedaCobro = sqlDr.GetString(nIndex3);   
                            objBEProductoMedioPago.DesMonedacobro = sqlDr.GetString(nIndex4).ToUpper();
                            objBEProductoMedioPago.DesMedioPago = sqlDr.GetString(nIndex5).ToUpper();     

                            lstBEProductoMedioPago.Add(objBEProductoMedioPago);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBEProductoMedioPago;
        }
        #endregion        
    }
}
