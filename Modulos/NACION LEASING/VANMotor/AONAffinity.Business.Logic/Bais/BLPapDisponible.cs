﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.DataAccess.Bais;
using AONAffinity.Motor.BusinessEntity.Bais;

namespace AONAffinity.Motor.BusinessLogic.Bais
{
    /// <summary>
    /// Clase de lógica del negocio de la tabla PapDisponible.
    /// </summary>
    public class BLPapDisponible
    {
        #region NoTransaccional
        /// <summary>
        /// Permite obtener el nro de certificado disponible.
        /// </summary>
        /// <param name="pnIdOficina">Código de oficina</param>
        /// <param name="pnIdProducto">Código de producto.</param>
        /// <returns>Nro de certificado.</returns>
        public Decimal ObtenerDisponible(Int32 pnIdOficina, Int32 pnIdProducto)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Decimal nResult = NullTypes.DecimalNull;

            using(SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus())) 
            {
                sqlCn.Open();

                DAPapDisponible objDAPapDisponible = new DAPapDisponible();
                SqlDataReader sqlDr = objDAPapDisponible.ObtenerDisponible(pnIdOficina, pnIdProducto, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("numero");

                        if (sqlDr.Read())
                        {
                            if (!sqlDr.IsDBNull(nIndex1)) { nResult = sqlDr.GetDecimal(nIndex1); }
                        }

                        sqlDr.Close();
                    }
                }
            }
                    
            return nResult;
        }
        #endregion        
    }
}
