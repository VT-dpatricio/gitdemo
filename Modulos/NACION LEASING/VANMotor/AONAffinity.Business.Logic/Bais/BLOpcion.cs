﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.DataAccess.Bais;
using AONAffinity.Motor.BusinessEntity.Bais;

namespace AONAffinity.Motor.BusinessLogic.Bais
{
    public class BLOpcion
    {
        #region NoTransaccional
        /// <summary>
        /// Permite listar las opciones de planes por producto.
        /// </summary>
        /// <param name="pnIdProducto">Código de producto.</param>
        /// <returns></returns>
        public List<BEOpcion> Listar(Int32 pnIdProducto)
        {
            /*---------------------------------------------------------------------------
            * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
            * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            List<BEOpcion> lstBEOpcion = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus())) 
            {
                sqlCn.Open();

                DAOpcion objDAOpcion = new DAOpcion();
                SqlDataReader sqlDr = objDAOpcion.Listar(pnIdProducto, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("IDPRODUCTO");
                        Int32 nIndex2 = sqlDr.GetOrdinal("OPCION");
                        Int32 nIndex3 = sqlDr.GetOrdinal("ACTIVO");
                        Int32 nIndex4 = sqlDr.GetOrdinal("PUNTOS");
                        Int32 nIndex5 = sqlDr.GetOrdinal("PUNTOSVALOR");

                        lstBEOpcion = new List<BEOpcion>();

                        while (sqlDr.Read()) 
                        {
                            BEOpcion objBEOpcion = new BEOpcion();

                            objBEOpcion.IdProducto = sqlDr.GetInt32(nIndex1);
                            objBEOpcion.Opcion = sqlDr.GetString(nIndex2).ToUpper();
                            objBEOpcion.Activo = sqlDr.GetBoolean(nIndex3);
                            objBEOpcion.Puntos = sqlDr.GetInt32(nIndex4);
                            objBEOpcion.PuntosValor = sqlDr.GetDecimal(nIndex5);

                            lstBEOpcion.Add(objBEOpcion);
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return lstBEOpcion;
        }           
        #endregion
    }
}
