﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using AONAffinity.Library.DataAccess;
using AONAffinity.Library.DataAccess.BDJanus;
using AONAffinity.Library.BusinessEntity.BDJanus;

namespace AONAffinity.Motor.BusinessLogic.Bais
{
    /// <summary>
    /// Clase de lógica del negocio a la tabla EstadoCivil.
    /// </summary>
    public class BLEstadoCivil
    {
        #region NoTransaccional
        /// <summary>
        /// Permite listar los estados civiles.
        /// </summary>
        /// <returns>Lista de tipo BEEstadoCivil.</returns>
        public List<BEEstadoCivil> Listar()
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            List<BEEstadoCivil> lstBEEstadoCivil = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus()))
            {
                sqlCn.Open();

                DAEstadoCivil objDAEstadoCivil = new DAEstadoCivil();
                SqlDataReader sqlDr = objDAEstadoCivil.Listar(sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idEstadoCivil");
                        Int32 nIndex2 = sqlDr.GetOrdinal("nombre");

                        lstBEEstadoCivil = new List<BEEstadoCivil>();

                        while (sqlDr.Read())
                        {
                            BEEstadoCivil objBEEstadoCivil = new BEEstadoCivil();
                            objBEEstadoCivil.IdEstadoCivil = sqlDr.GetString(nIndex1);
                            objBEEstadoCivil.Nombre = sqlDr.GetString(nIndex2).ToUpper();
                            lstBEEstadoCivil.Add(objBEEstadoCivil);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBEEstadoCivil;
        }
        #endregion        
    }
}
