﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.DataAccess.Bais;
using AONAffinity.Motor.BusinessEntity.Bais;

namespace AONAffinity.Motor.BusinessLogic.Bais
{
    public class BLFrecuencia
    {
        #region NoTransaccional
        /// <summary>
        /// Permite listar las frecuencias por producto.
        /// </summary>
        /// <param name="pnIdProducto">Código de producto.</param>
        /// <returns>Objeto List de tipo BEFrecuencia.</returns>
        public List<BEFrecuencia> Listar(Int32 pnIdProducto)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
             ---------------------------------------------------------------------------*/

            List<BEFrecuencia> lstBEFrecuencia = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus())) 
            {
                sqlCn.Open();

                DAFrecuencia objDAFrecuencia = new DAFrecuencia();
                SqlDataReader sqlDr = objDAFrecuencia.Listar(pnIdProducto, sqlCn);

                if(sqlDr != null)
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idFrecuencia");
                        Int32 nIndex2 = sqlDr.GetOrdinal("nombre");

                        lstBEFrecuencia = new List<BEFrecuencia>();

                        while(sqlDr.Read()) 
                        {
                            BEFrecuencia objBEFrecuencia = new BEFrecuencia();

                            objBEFrecuencia.IdFrecuencia = sqlDr.GetInt32(nIndex1);
                            objBEFrecuencia.Nombre = sqlDr.GetString(nIndex2).ToUpper();

                            lstBEFrecuencia.Add(objBEFrecuencia);
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return lstBEFrecuencia;
        }    
        #endregion
    }
}
