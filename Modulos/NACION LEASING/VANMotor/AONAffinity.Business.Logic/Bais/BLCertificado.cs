﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;  
using AONAffinity.Motor.DataAccess.Bais;
using AONAffinity.Motor.BusinessEntity;
using AONAffinity.Motor.BusinessEntity.Bais;

namespace AONAffinity.Motor.BusinessLogic.Bais
{
    public class BLCertificado
    {
        #region Transaccional
        /// <summary>
        /// Permite insertar un certificado.
        /// </summary>
        /// <param name="pObjBECertificado">Objeto que contiene la información del certificado.</param>
        /// <returns>La cantidad de registros insertados.</returns>
        public Int32 Insertar(BECertificado pObjBECertificado)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-02
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus()))
            {
                sqlCn.Open();

                DACertificado objDACertificado = new DACertificado();
                nResult = objDACertificado.Insertar(pObjBECertificado, sqlCn);
            }

            return nResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pObjBECertificado"></param>
        /// <param name="pLstBEAsegurado"></param>
        /// <param name="pLstBEBeneficiario"></param>
        /// <param name="pLstBEInfoProductoC"></param>
        /// <param name="pLstBEInfoAseguradoC"></param>
        /// <param name="pNumCertFisico"></param>
        /// <returns></returns>
        public List<BEExpedir> Expedir(BECertificado pObjBECertificado, List<BEAsegurado> pLstBEAsegurado, List<BEBeneficiario> pLstBEBeneficiario, List<BEInfoProductoC> pLstBEInfoProductoC, List<BEInfoAseguradoC> pLstBEInfoAseguradoC, String pNumCertFisico)
        {
            List<BEExpedir> lstValidaCertificado;
            List<BEExpedir> lstValidaAsegurado;
            List<BEExpedir> lstBEValidaVehiculoAsegurado;
            List<BEExpedir> lstBEExpedir = new List<BEExpedir>();
            BEExpedir objBEExpedir;
            Boolean exito = false;
            SqlTransaction sqlTrs;

            if (pObjBECertificado.PrimaCobrar == 0)
            {
                throw new Exception("El monto de la prima debe ser mayor a 0.00");
            }

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus()))
            {
                sqlCn.Open();

                //Validar informacion del certificado.
                DAExpedir objDAExpedir = new DAExpedir();
                lstValidaCertificado = this.ValidaExpedirCertificado(pObjBECertificado.IdProducto, pNumCertFisico, pObjBECertificado.CcCliente, pObjBECertificado.NumeroCuenta, sqlCn);

                if (lstValidaCertificado != null)
                {
                    if (lstValidaCertificado.Count > 0)
                    {
                        return lstValidaCertificado;
                    }
                }

                //Validar información del asegurado.
                foreach (BEAsegurado objBEAseg in pLstBEAsegurado)
                {
                    lstValidaAsegurado = this.ValidaExpedirAsegurado(pObjBECertificado.IdProducto, objBEAseg.Ccaseg, sqlCn);

                    if (lstValidaAsegurado != null)
                    {
                        if (lstValidaAsegurado.Count > 0)
                        {
                            return lstValidaAsegurado;
                        }
                    }
                }

                ////Validar vehiculo a asegurar.
                //BusinessEntity.BEVehiculo objBEVehiculo = new BusinessEntity.BEVehiculo();
                
                //foreach (BEInfoAseguradoC objBEInfoAseguradoC in pLstBEInfoAseguradoC) 
                //{
                //    //Obtener nro de placa
                //    if (objBEInfoAseguradoC.Nombre == "NUMERODEMATRICULAPLACA")
                //    {
                //        objBEVehiculo.IdInfasegPlaca = objBEInfoAseguradoC.IdInfoAsegurado;
                //        objBEVehiculo.Placa = objBEInfoAseguradoC.ValorString;
                //    }

                //    //Obtener nro de motor.
                //    if (objBEInfoAseguradoC.Nombre == "NUMERODEMOTOR") 
                //    {
                //        objBEVehiculo.IdInfoAsegMotor = objBEInfoAseguradoC.IdInfoAsegurado;
                //        objBEVehiculo.NroMotor = objBEInfoAseguradoC.ValorString;
                //    }

                //    //Obtener nro de serie.
                //    if (objBEInfoAseguradoC.Nombre == "NUMERODECHASIS") 
                //    {
                //        objBEVehiculo.IdInfAsegSerie = objBEInfoAseguradoC.IdInfoAsegurado;
                //        objBEVehiculo.NroChasis = objBEInfoAseguradoC.ValorString;
                //    }
                //}

                //lstBEValidaVehiculoAsegurado = this.ValidaExpedirVehiculoAsegurado(pObjBECertificado.IdProducto, objBEVehiculo, sqlCn);

                ////Validar resultados.
                //if (lstBEValidaVehiculoAsegurado != null) 
                //{
                //    if (lstBEValidaVehiculoAsegurado.Count > 0) 
                //    {
                //        return lstBEValidaVehiculoAsegurado;
                //    }
                //}

                Decimal nNumCertificado = this.ObtenerNroCertDisp(pObjBECertificado.IdProducto, sqlCn);

                sqlTrs = sqlCn.BeginTransaction();
                DACertificado objDACertificado = new DACertificado();

                pObjBECertificado.IdCertificado = pObjBECertificado.IdProducto + "-" + nNumCertificado;
                pObjBECertificado.NumCertificado = nNumCertificado;

                Int32 rpta = objDACertificado.Insertar(pObjBECertificado, sqlCn, sqlTrs);

                if (rpta != -1)
                {
                    exito = true;
                    DAInfoProductoC objDAInfoProductoC = new DAInfoProductoC();

                    //Insertar los infoproductos.
                    foreach (BEInfoProductoC objBEInfoProductoC in pLstBEInfoProductoC)
                    {
                        objBEInfoProductoC.IdCertificado = pObjBECertificado.IdCertificado;
                        rpta = objDAInfoProductoC.Insertar(objBEInfoProductoC, sqlCn, sqlTrs);

                        if (rpta == -1)
                        {
                            exito = false;
                            break;
                        }
                        else
                        {
                            exito = true;
                        }
                    }

                    if (exito)
                    {
                        DAAsegurado objDAAsegurado = new DAAsegurado();
                        foreach (BEAsegurado objBEAsegurado in pLstBEAsegurado)
                        {
                            objBEAsegurado.IdCertificado = pObjBECertificado.IdCertificado;
                            rpta = objDAAsegurado.Insertar(objBEAsegurado, sqlCn, sqlTrs);

                            if (rpta == -1)
                            {
                                exito = false;
                                break;
                            }
                            else
                            {
                                exito = true;
                            }
                        }
                        if (exito)
                        {
                            DAInfoAseguradoC objDAInfoAseguradoC = new DAInfoAseguradoC();

                            //Insertar los infoasegurados.
                            foreach (BEInfoAseguradoC objBEInfoAseguradoC in pLstBEInfoAseguradoC)
                            {
                                objBEInfoAseguradoC.IdCertificado = pObjBECertificado.IdCertificado;
                                rpta = objDAInfoAseguradoC.Insertar(objBEInfoAseguradoC, pObjBECertificado.IdProducto, sqlCn, sqlTrs);

                                if (rpta == -1)
                                {
                                    exito = false;
                                    break;
                                }
                                else
                                {
                                    exito = true;
                                }
                            }
                            //if (exito)
                            //{
                            //    DABeneficiario objDABenef = new DABeneficiario();
                            //    foreach (BEBeneficiario objBenef in pLstBEBeneficiario)
                            //    {
                            //        /**********INSERCION DE LOS BENEFICIARIOS****************/
                            //        rpta = objDABenef.DAInsertarBeneficiario(objBenef, sqlCon, sqlTranx);

                            //        if (rpta == -1)
                            //        {
                            //            exito = false;
                            //            break;
                            //        }
                            //        else
                            //        {
                            //            exito = true;
                            //        }
                            //}

                            if (exito)
                            {
                                sqlTrs.Commit();
                                objBEExpedir = new BEExpedir();
                                objBEExpedir.Descripcion = pObjBECertificado.IdCertificado;
                                objBEExpedir.Codigo = 1;
                                lstBEExpedir.Add(objBEExpedir);
                                return lstBEExpedir;
                            }
                            else
                            {
                                sqlTrs.Rollback();
                                objBEExpedir = new BEExpedir();
                                objBEExpedir.Descripcion = "ER";
                                objBEExpedir.Codigo = 0;
                                lstBEExpedir.Add(objBEExpedir);
                                return lstBEExpedir;
                            }
                        }
                        else
                        {
                            sqlTrs.Rollback();
                            objBEExpedir = new BEExpedir();
                            objBEExpedir.Descripcion = "ER";
                            objBEExpedir.Codigo = 0;
                            lstBEExpedir.Add(objBEExpedir);
                            return lstBEExpedir;
                        }
                    }//Retorna en caso de que falle la insercion de algun asegurado
                    else
                    {
                        sqlTrs.Rollback();
                        objBEExpedir = new BEExpedir();
                        objBEExpedir.Descripcion = "ER";
                        objBEExpedir.Codigo = 0;
                        lstBEExpedir.Add(objBEExpedir);
                        return lstBEExpedir;
                    }
                }//Retorna en caso de que falle la insercion de algun infoproducto
                else
                {
                    sqlTrs.Rollback();
                    objBEExpedir = new BEExpedir();
                    objBEExpedir.Descripcion = "ER";
                    objBEExpedir.Codigo = 0;
                    lstBEExpedir.Add(objBEExpedir);
                    return lstBEExpedir;
                }
                //}//Retorna en caso de que falle la insercion del certificado
                //else
                //{
                //    sqlTranx.Rollback();
                //    objBEExpedir = new BEExpedir();
                //    objBEExpedir.Descripcion = "ER";
                //    objBEExpedir.Codigo = 0;
                //    lstBEExpedir.Add(objBEExpedir);
                //    return lstBEExpedir;
                //}
            }
        }

        /// <summary>
        /// Permite actualizar la informacion del certificado.
        /// </summary>
        /// <param name="pObjBECertificado">Objeto que contiene la información del certificado.</param>
        /// <returns>0 No actualizó | 1 Si actualizó.</returns>
        public Int32 Actualizar(BECertificado pObjBECertificado, String pcObservacion) 
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-12-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus())) 
            {
                sqlCn.Open();
 
                DACertificado objDACertificado = new DACertificado();
                nResult = objDACertificado.Actualizar(pObjBECertificado, String.Empty, sqlCn);   
            }

            return nResult;
        }
        #endregion                     

        #region NoTransaccional
        public List<BECertificado> Listar()
        {
            List<BECertificado> lstBECertificado = new List<BECertificado>();
            return lstBECertificado;
        }

        public List<BECertificado> Consultar(Int32 pnTipo, String pcCcCliente, Decimal pnNumCertificado, String pcApellido1, String pcApellido2, String pcNombre1, String pcNombre2, String pcIdUsuario) 
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-11-29
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            List<BECertificado> lstBECertificado = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DACertificado objDACertificado = new DACertificado();
                SqlDataReader sqlDr = objDACertificado.Consultar(pnTipo, pcCcCliente, pnNumCertificado, pcApellido1, pcApellido2, pcNombre1, pcNombre2, pcIdUsuario, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idCertificado");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex3 = sqlDr.GetOrdinal("numCertificado");
                        Int32 nIndex4 = sqlDr.GetOrdinal("descProducto");
                        Int32 nIndex5 = sqlDr.GetOrdinal("nombre1");
                        Int32 nIndex6 = sqlDr.GetOrdinal("nombre2");
                        Int32 nIndex7 = sqlDr.GetOrdinal("apellido1");
                        Int32 nIndex8 = sqlDr.GetOrdinal("apellido2");
                        Int32 nIndex9 = sqlDr.GetOrdinal("idTipoDocumento");
                        Int32 nIndex10 = sqlDr.GetOrdinal("descTipoDoc");
                        Int32 nIndex11 = sqlDr.GetOrdinal("ccCliente");
                        Int32 nIndex12 = sqlDr.GetOrdinal("idEstadoCertificado");
                        Int32 nIndex13 = sqlDr.GetOrdinal("descEstadoCer");                                                                                                 

                        lstBECertificado = new List<BECertificado>();

                        while (sqlDr.Read()) 
                        {
                            BECertificado objBECertificado = new BECertificado();

                            objBECertificado.IdCertificado = sqlDr.GetString(nIndex1);
                            objBECertificado.IdProducto = sqlDr.GetInt32(nIndex2);
                            objBECertificado.NumCertificado = sqlDr.GetDecimal(nIndex3);
                            objBECertificado.DesProducto = sqlDr.GetString(nIndex4);
                            objBECertificado.Nombre1 = sqlDr.GetString(nIndex5);
                            if (sqlDr.IsDBNull(nIndex6)) { objBECertificado.Nombre2 = NullTypes.CadenaNull; } else { objBECertificado.Nombre2 = sqlDr.GetString(nIndex6); }
                            objBECertificado.Apellido1 = sqlDr.GetString(nIndex7);
                            if (sqlDr.IsDBNull(nIndex8)) { objBECertificado.Apellido2 = NullTypes.CadenaNull; } else { objBECertificado.Apellido2 = sqlDr.GetString(nIndex8); }
                            objBECertificado.IdTipoDocumento = sqlDr.GetString(nIndex9);
                            objBECertificado.DesTipoDocumento = sqlDr.GetString(nIndex10);
                            objBECertificado.CcCliente = sqlDr.GetString(nIndex11);
                            objBECertificado.IdEstadoCertificado = sqlDr.GetInt32(nIndex12);
                            objBECertificado.DesEstadoCertificado = sqlDr.GetString(nIndex13);
                                                                                                                                      
                            lstBECertificado.Add(objBECertificado);
                        }
                        sqlDr.Close(); 
                    }
                }                
            }

            return lstBECertificado;
        }

        /// <summary>
        /// Permite obtener los datos de un certificado.
        /// </summary>
        /// <param name="pcIdCertificado">Código de certificado.</param>
        /// <returns>Objeto de tipo BECertificado con los datos del certificado.</returns>
        public BECertificado Obtener(String pcIdCertificado) 
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-11-12
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            BECertificado objBECertificado = null;

            if (pcIdCertificado != "0") 
            {
                using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus()))
                {
                    sqlCn.Open();

                    DACertificado objDACertificado = new DACertificado();
                    SqlDataReader sqlDr = objDACertificado.Obtener(pcIdCertificado, sqlCn);

                    if (sqlDr != null)
                    {
                        if (sqlDr.HasRows)
                        {
                            Int32 nIndex0 = sqlDr.GetOrdinal("numCertificado");
                            Int32 nIndex1 = sqlDr.GetOrdinal("desProducto");
                            Int32 nIndex2 = sqlDr.GetOrdinal("idInformador");
                            Int32 nIndex3 = sqlDr.GetOrdinal("nomInformador");
                            Int32 nIndex4 = sqlDr.GetOrdinal("codOficina");
                            Int32 nIndex5 = sqlDr.GetOrdinal("idOficina");
                            Int32 nIndex6 = sqlDr.GetOrdinal("nomOficina");
                            Int32 nIndex7 = sqlDr.GetOrdinal("desMedioPago");
                            Int32 nIndex8 = sqlDr.GetOrdinal("idMedioPago");
                            Int32 nIndex9 = sqlDr.GetOrdinal("idFrecuencia");
                            Int32 nIndex10 = sqlDr.GetOrdinal("desFrecuencia");
                            Int32 nIndex11 = sqlDr.GetOrdinal("desTipoDocumento");
                            Int32 nIndex12 = sqlDr.GetOrdinal("idTipoDocumento");
                            Int32 nIndex13 = sqlDr.GetOrdinal("nomCiudad");
                            Int32 nIndex14 = sqlDr.GetOrdinal("nomDepartamento");
                            Int32 nIndex15 = sqlDr.GetOrdinal("desEstadoCertificado");
                            Int32 nIndex16 = sqlDr.GetOrdinal("idEstadoCertificado");
                            Int32 nIndex17 = sqlDr.GetOrdinal("desMotivoAnulacion");
                            Int32 nIndex18 = sqlDr.GetOrdinal("idMotivoAnulacion");
                            Int32 nIndex19 = sqlDr.GetOrdinal("vigencia");
                            Int32 nIndex20 = sqlDr.GetOrdinal("finVigencia");
                            Int32 nIndex21 = sqlDr.GetOrdinal("digitacion");
                            Int32 nIndex22 = sqlDr.GetOrdinal("venta");
                            Int32 nIndex23 = sqlDr.GetOrdinal("opcion");
                            Int32 nIndex24 = sqlDr.GetOrdinal("montoAsegurado");
                            Int32 nIndex25 = sqlDr.GetOrdinal("primaBruta");
                            Int32 nIndex26 = sqlDr.GetOrdinal("iva");
                            Int32 nIndex27 = sqlDr.GetOrdinal("primaCobrar");
                            Int32 nIndex28 = sqlDr.GetOrdinal("primaTotal");
                            Int32 nIndex29 = sqlDr.GetOrdinal("numeroCuenta");
                            Int32 nIndex30 = sqlDr.GetOrdinal("vencimiento");
                            Int32 nIndex31 = sqlDr.GetOrdinal("nombre1");
                            Int32 nIndex32 = sqlDr.GetOrdinal("nombre2");
                            Int32 nIndex33 = sqlDr.GetOrdinal("apellido1");
                            Int32 nIndex34 = sqlDr.GetOrdinal("apellido2");
                            Int32 nIndex35 = sqlDr.GetOrdinal("ccCliente");
                            Int32 nIndex36 = sqlDr.GetOrdinal("telefono");
                            Int32 nIndex37 = sqlDr.GetOrdinal("direccion");
                            Int32 nIndex38 = sqlDr.GetOrdinal("solicitudAnulacion");
                            Int32 nIndex39 = sqlDr.GetOrdinal("digitacionAnulacion");
                            Int32 nIndex40 = sqlDr.GetOrdinal("efectuarAnulacion");
                            Int32 nIndex41 = sqlDr.GetOrdinal("consistente");
                            Int32 nIndex42 = sqlDr.GetOrdinal("simboloMoneda");
                            Int32 nIndex43 = sqlDr.GetOrdinal("desMonedaPrima");
                            Int32 nIndex44 = sqlDr.GetOrdinal("desMonedaCobro");
                            Int32 nIndex45 = sqlDr.GetOrdinal("idMonedaCobro");
                            Int32 nIndex46 = sqlDr.GetOrdinal("idCiudad");
                            Int32 nIndex47 = sqlDr.GetOrdinal("usuarioModificacion");
                            Int32 nIndex48 = sqlDr.GetOrdinal("Provincia");
                            Int32 nIndex49 = sqlDr.GetOrdinal("idMonedaPrima");

                            objBECertificado = new BECertificado();

                            if (sqlDr.Read())
                            {
                                objBECertificado.NumCertificado = sqlDr.GetDecimal(nIndex0);
                                objBECertificado.DesProducto = sqlDr.GetString(nIndex1).ToUpper();
                                objBECertificado.IdInformador = sqlDr.GetString(nIndex2);
                                objBECertificado.NomInformador = sqlDr.GetString(nIndex3);
                                if (sqlDr.IsDBNull(nIndex4)) { objBECertificado.CodOficina = NullTypes.IntegerNull; } else { objBECertificado.CodOficina = sqlDr.GetInt32(nIndex4); }
                                objBECertificado.IdOficina = sqlDr.GetInt32(nIndex5);
                                objBECertificado.NomOficina = sqlDr.GetString(nIndex6);
                                objBECertificado.DesMedioPago = sqlDr.GetString(nIndex7);
                                objBECertificado.IdMedioPago = sqlDr.GetString(nIndex8);
                                objBECertificado.IdFrecuencia = sqlDr.GetInt32(nIndex9);
                                objBECertificado.DesFrecuencia = sqlDr.GetString(nIndex10);
                                objBECertificado.DesTipoDocumento = sqlDr.GetString(nIndex11);
                                objBECertificado.IdTipoDocumento = sqlDr.GetString(nIndex12);
                                objBECertificado.NomCiudad = sqlDr.GetString(nIndex13);
                                objBECertificado.NomDepartamento = sqlDr.GetString(nIndex14);
                                objBECertificado.DesEstadoCertificado = sqlDr.GetString(nIndex15).ToUpper();
                                objBECertificado.IdEstadoCertificado = sqlDr.GetInt32(nIndex16);
                                objBECertificado.DesMotivoAnulacion = sqlDr.GetString(nIndex17);
                                objBECertificado.IdMotivoAnulacion = sqlDr.GetInt32(nIndex18);
                                if (sqlDr.IsDBNull(nIndex19)) { objBECertificado.Vigencia = NullTypes.FechaNull; } else { objBECertificado.Vigencia = sqlDr.GetDateTime(nIndex19); }
                                if (sqlDr.IsDBNull(nIndex20)) { objBECertificado.FinVigencia = NullTypes.FechaNull; } else { objBECertificado.FinVigencia = sqlDr.GetDateTime(nIndex20); }
                                objBECertificado.Digitacion = sqlDr.GetDateTime(nIndex21);
                                objBECertificado.Venta = sqlDr.GetDateTime(nIndex22);
                                objBECertificado.Opcion = sqlDr.GetString(nIndex23);
                                objBECertificado.MontoAsegurado = sqlDr.GetDecimal(nIndex24);
                                objBECertificado.PrimaBruta = sqlDr.GetDecimal(nIndex25);
                                objBECertificado.Iva = sqlDr.GetDecimal(nIndex26);
                                objBECertificado.PrimaCobrar = sqlDr.GetDecimal(nIndex27);
                                objBECertificado.PrimaTotal = sqlDr.GetDecimal(nIndex28);
                                objBECertificado.NumeroCuenta = sqlDr.GetString(nIndex29);
                                if (sqlDr.IsDBNull(nIndex30)) { objBECertificado.Vencimiento = NullTypes.FechaNull; } else { objBECertificado.Vencimiento = sqlDr.GetDateTime(nIndex30); }
                                objBECertificado.Nombre1 = sqlDr.GetString(nIndex31);
                                objBECertificado.Nombre2 = sqlDr.GetString(nIndex32);
                                objBECertificado.Apellido1 = sqlDr.GetString(nIndex33);
                                objBECertificado.Apellido2 = sqlDr.GetString(nIndex34);
                                objBECertificado.CcCliente = sqlDr.GetString(nIndex35);
                                objBECertificado.Telefono = sqlDr.GetString(nIndex36);
                                objBECertificado.Direccion = sqlDr.GetString(nIndex37);
                                if (sqlDr.IsDBNull(nIndex38)) { objBECertificado.SolicitudAnulacion = NullTypes.FechaNull; } else { objBECertificado.SolicitudAnulacion = sqlDr.GetDateTime(nIndex38); }
                                if (sqlDr.IsDBNull(nIndex39)) { objBECertificado.DigitacionAnulacion = NullTypes.FechaNull; } else { objBECertificado.DigitacionAnulacion = sqlDr.GetDateTime(nIndex39); }
                                if (sqlDr.IsDBNull(nIndex40)) { objBECertificado.EfectuarAnulacion = NullTypes.FechaNull; } else { objBECertificado.EfectuarAnulacion = sqlDr.GetDateTime(nIndex40); }
                                objBECertificado.Consistente = sqlDr.GetBoolean(nIndex41);
                                objBECertificado.SimboloMoneda = sqlDr.GetString(nIndex42);
                                objBECertificado.DesMonedaPrima = sqlDr.GetString(nIndex43);
                                objBECertificado.DesMonedaCobro = sqlDr.GetString(nIndex44);
                                objBECertificado.IdMonedaCobro = sqlDr.GetString(nIndex45);
                                objBECertificado.IdCiudad = sqlDr.GetInt32(nIndex46);
                                objBECertificado.UsuarioModificacion = sqlDr.GetString(nIndex47);
                                objBECertificado.NomProvincia = sqlDr.GetString(nIndex48);
                                objBECertificado.IdMonedaPrima = sqlDr.GetString(nIndex49);
                            }
                            sqlDr.Close();
                        }
                    }
                }
            }
           
            return objBECertificado;
        }

        /// <summary>
        /// Permite obtener la moneda de cobro.
        /// </summary>
        /// <param name="pcIdMedioPago">Código de medio de pago.</param>
        /// <param name="pcIdMonedaPrima">Código de moneda prima.</param>
        /// <param name="pnIdProducto">Código de producto.</param>
        /// <returns>El código de la moneda de cobro.</returns>
        public String ObtenerMonedaCobro(String pcIdMedioPago, String pcIdMonedaPrima, Int32 pnIdProducto) 
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-12-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            String cIdMonedaCobro = String.Empty;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus())) 
            {
                sqlCn.Open();

                DACertificado objDACertificado = new DACertificado();
                SqlDataReader sqlDr = objDACertificado.ObtenerMonedaCobro(pcIdMedioPago, pcIdMonedaPrima, pnIdProducto, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idMonedaCobro");  

                        if (sqlDr.Read()) 
                        {
                            cIdMonedaCobro = sqlDr.GetString(nIndex1);
                        }

                        sqlDr.Close();  
                    }
                }
            }

            return cIdMonedaCobro;
        }

        /// <summary>
        /// Permite calcular el inicio de vigencia.
        /// </summary>
        /// <param name="pnIdProducto">Código de producto.</param>
        /// <param name="pdFechaVenta">Fecha de venta.</param>        
        /// <returns>Fecha de inicio de vigencia.</returns>
        public DateTime ObtenerInicioVigencia(Int32 pnIdProducto, DateTime pdFechaVenta)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2012-06-04
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            DateTime dFecInicioVig = NullTypes.FechaNull; 

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus()))             
            {
                sqlCn.Open();

                DACertificado objDACertificado = new DACertificado();
                SqlDataReader sqlDr = objDACertificado.ObtenerInicioVigencia(pnIdProducto, pdFechaVenta, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex = sqlDr.GetOrdinal("inicioVigencia");

                        if (sqlDr.Read()) 
                        {
                            dFecInicioVig = sqlDr.GetDateTime(nIndex);  
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return dFecInicioVig;            
        }

        /// <summary>
        /// Permite calcular el fin de vigencia, segun el plan del producto.
        /// </summary>
        /// <param name="pnIdProducto">Código de producto.</param>
        /// <param name="pdFechaInicioVig">Fecah inicio vigencia.</param>
        /// <param name="pcOpcion">Plan del producto.</param>        
        /// <returns>Objeto de tipo SqlDataReader.</returns>
        public DateTime ObtenerFinVigenciaxPlan(Int32 pnIdProducto, DateTime pdFechaInicioVig, String pcOpcion) 
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2012-06-04
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            DateTime dFecFinVig = NullTypes.FechaNull;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus())) 
            {
                sqlCn.Open();

                DACertificado objDACertificado = new DACertificado();
                SqlDataReader sqlDr = objDACertificado.ObtenerFinVigenciaxPlan(pnIdProducto, pdFechaInicioVig, pcOpcion, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex = sqlDr.GetOrdinal("finVigencia");

                        if (sqlDr.Read())
                        {
                            dFecFinVig = sqlDr.GetDateTime(nIndex);
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return dFecFinVig;
        }

        public List<BERespuesta> ValidarNumCertificado(Int32 pnIdProducto, Int32 pnIdCanal, Decimal pnNumCertificado) 
        {
            List<BERespuesta> lstBERespuesta = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DACertificado objDACertificado = new DACertificado();
                SqlDataReader sqlDr = objDACertificado.ValidarNumCertificado(pnIdProducto, pnIdCanal, pnNumCertificado, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idRespuesta");
                        Int32 nIndex2 = sqlDr.GetOrdinal("descripcion");

                        lstBERespuesta = new List<BERespuesta>();

                        while(sqlDr.Read()) 
                        {
                            BERespuesta objBERespuesta = new BERespuesta();
                            objBERespuesta.IdRespuesta = sqlDr.GetInt32(nIndex1);
                            objBERespuesta.Descripcion = sqlDr.GetString(nIndex2);
                            lstBERespuesta.Add(objBERespuesta);     
                        }
                        sqlDr.Close();
                    }
                }
            }

            return lstBERespuesta;
        }
        #endregion

        #region Privados
        private List<BEExpedir> ValidaExpedirCertificado(Int32 pnIdProducto, String pcNumCertFisico, String pcCcliente, String pcNumeroCuenta, SqlConnection pSqlCn)
        {
            List<BEExpedir> lstBEExpedir = null;

            DAExpedir objDAExpedir = new DAExpedir();

            using (SqlDataReader sqlDr = objDAExpedir.ValidaExpedirCertificado(pnIdProducto, pcNumCertFisico, pcCcliente, pcNumeroCuenta, pSqlCn))
            {
                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex2 = sqlDr.GetOrdinal("error");

                        lstBEExpedir = new List<BEExpedir>();

                        while (sqlDr.Read())
                        {
                            BEExpedir objBEExpedir = new BEExpedir();
                            objBEExpedir.Descripcion = sqlDr.GetString(nIndex1);
                            objBEExpedir.Codigo = sqlDr.GetInt32(nIndex2);

                            lstBEExpedir.Add(objBEExpedir);
                        }
                    }
                }
            }
            return lstBEExpedir;
        }

        private List<BEExpedir> ValidaExpedirAsegurado(Int32 pnIdProducto, String pcCcaseg, SqlConnection pSqlCon)
        {
            List<BEExpedir> lstBEExpedir = null;

            DAExpedir objDAExpedir = new DAExpedir();

            using (SqlDataReader sqlDr = objDAExpedir.ValidaExpedirAsegurado(pnIdProducto, pcCcaseg, pSqlCon))
            {
                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex2 = sqlDr.GetOrdinal("error");

                        lstBEExpedir = new List<BEExpedir>();

                        while (sqlDr.Read())
                        {
                            BEExpedir objBEExpedir = new BEExpedir();

                            objBEExpedir.Descripcion = sqlDr.GetString(nIndex1);
                            objBEExpedir.Codigo = sqlDr.GetInt32(nIndex2);

                            lstBEExpedir.Add(objBEExpedir);
                        }
                    }
                }
            }
            return lstBEExpedir;
        }

        public List<BEExpedir> ValidaExpedirVehiculoAsegurado(Int32 pnIdProducto, AONAffinity.Motor.BusinessEntity.BEVehiculo pObjBEVehiculo, SqlConnection pSqlCn)
        {
            List<BEExpedir> lstBEExpedir = null;

            DAExpedir objDAExpedir = new DAExpedir();

            using (SqlDataReader sqlDr = objDAExpedir.ValidaExpedirVahiculoAsegurado(pnIdProducto, pObjBEVehiculo, pSqlCn))
            {
                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex2 = sqlDr.GetOrdinal("error");

                        lstBEExpedir = new List<BEExpedir>();

                        while (sqlDr.Read())
                        {
                            BEExpedir objBEExpedir = new BEExpedir();

                            objBEExpedir.Descripcion = sqlDr.GetString(nIndex1);
                            objBEExpedir.Codigo = sqlDr.GetInt32(nIndex2);

                            lstBEExpedir.Add(objBEExpedir);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBEExpedir;
        }

        /// <summary>
        /// Permite Obtener el nro de certificado disponible.
        /// </summary>
        /// <param name="pnIdProducto">Código de producto.</param>
        /// <param name="sqlCn">Objeto que permite la conección a la BD.</param>
        /// <returns>Nro de certificado disponible.</returns>
        private Decimal ObtenerNroCertDisp(Int32 pnIdProducto, SqlConnection sqlCn)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Decimal nResult = 0;
            DACertificado objDACertificado = new DACertificado();

            using (SqlDataReader sqlDr = objDACertificado.ObtenerNroCertDisp(pnIdProducto, sqlCn))
            {
                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("numCertificado");

                        if (sqlDr.Read())
                        {
                            nResult = sqlDr.GetDecimal(nIndex1);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return nResult;
        }
        #endregion
    }
}
