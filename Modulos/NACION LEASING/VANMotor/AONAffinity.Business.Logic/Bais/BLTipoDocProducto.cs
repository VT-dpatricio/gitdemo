﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.DataAccess.Bais;
using AONAffinity.Motor.BusinessEntity.Bais;

namespace AONAffinity.Motor.BusinessLogic.Bais
{
    /// <summary>
    /// Clase de acceso de lógica del negocio de la tabla TipoDocProducto.
    /// </summary>
    public class BLTipoDocProducto
    {
        #region NoTransaccional
        /// <summary>
        /// Permite listar los tipos de documentos por producto.
        /// </summary>
        /// <param name="pnIdProducto">Código de producto.</param>
        /// <returns>Lista generica de tipo BETipoDocProducto.</returns>
        public List<BETipoDocProducto> ListarxProducto(Int32 pnIdProducto)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-11-29
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            List<BETipoDocProducto> lstBETipoDocProducto = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus()))
            {
                sqlCn.Open();

                DATipoDocProducto objDATipoDocProducto = new DATipoDocProducto();
                SqlDataReader sqlDr = objDATipoDocProducto.ListarxProducto(pnIdProducto, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        lstBETipoDocProducto = new List<BETipoDocProducto>();

                        Int32 nIndex1 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idTipoDocumento");
                        Int32 nIndex3 = sqlDr.GetOrdinal("nombre");

                        while (sqlDr.Read())
                        {
                            BETipoDocProducto objBETipoDocProducto = new BETipoDocProducto();
                            objBETipoDocProducto.IdProducto = sqlDr.GetInt32(nIndex1);
                            objBETipoDocProducto.IdTipoDocumento = sqlDr.GetString(nIndex2);
                            objBETipoDocProducto.Nombre = sqlDr.GetString(nIndex3).ToUpper();

                            lstBETipoDocProducto.Add(objBETipoDocProducto);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBETipoDocProducto;
        }
        #endregion        
    }
}
