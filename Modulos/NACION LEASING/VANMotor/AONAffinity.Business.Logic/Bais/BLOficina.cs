﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.DataAccess.Bais;
using AONAffinity.Motor.BusinessEntity.Bais;

namespace AONAffinity.Motor.BusinessLogic.Bais
{
    public class BLOficina
    {
        public List<BEOficina> ListarxSponsor(Int32 pnIdSponsor)
        {
            List<BEOficina> lstBEOficina = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAOficina objDAOficina = new DAOficina();
                SqlDataReader sqlDr = objDAOficina.ListarxSponsor(pnIdSponsor, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idoficina");
                        Int32 nIndex2 = sqlDr.GetOrdinal("nombre");                        

                        lstBEOficina = new List<BEOficina>();

                        while (sqlDr.Read())
                        {
                            BEOficina objBEOficina = new BEOficina();

                            objBEOficina.IdOficina = sqlDr.GetInt32(nIndex1);
                            objBEOficina.Nombre = sqlDr.GetString(nIndex2);                            

                            lstBEOficina.Add(objBEOficina);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBEOficina;
        } 

        public List<BEOficina> Listar(Int32 pnIdEntidad, String pcDescripcion) 
        {
            List<BEOficina> lstBEOficina = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus()))
            {
                sqlCn.Open();

                DAOficina objDAOficina = new DAOficina();
                SqlDataReader sqlDr = objDAOficina.Listar(pnIdEntidad, pcDescripcion, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idoficina");
                        Int32 nIndex2 = sqlDr.GetOrdinal("nombre");
                        Int32 nIndex3 = sqlDr.GetOrdinal("zona");

                        lstBEOficina = new List<BEOficina>();

                        while (sqlDr.Read())
                        {
                            BEOficina objBEOficina = new BEOficina();

                            objBEOficina.IdOficina = sqlDr.GetInt32(nIndex1);
                            objBEOficina.Nombre = sqlDr.GetString(nIndex2);
                            objBEOficina.Zona = sqlDr.GetString(nIndex3);

                            lstBEOficina.Add(objBEOficina);
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return lstBEOficina;
        } 
    }
}
