﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.DataAccess.Bais;
using AONAffinity.Motor.BusinessEntity.Bais;

namespace AONAffinity.Motor.BusinessLogic.Bais
{
    public class BLOpcionPoliza
    {
        public BEOpcionPoliza ObtenerxTipoDoc(Int32 pnIdProducto, String pcOpcion, Int32 pnIdFrecuencia, String pcIdMonedaPrima, String pcIdTipoDocumento, Boolean pbEstado) 
        {
            BEOpcionPoliza objBEOpcionPoliza = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus())) 
            {
                sqlCn.Open();

                DAOpcionPoliza objDAOpcionPoliza = new DAOpcionPoliza();
                SqlDataReader sqlDr = objDAOpcionPoliza.ObtenerxTipoDoc(pnIdProducto, pcOpcion, pnIdFrecuencia, pcIdMonedaPrima, pcIdTipoDocumento, pbEstado, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex2 = sqlDr.GetOrdinal("opcion");  
                        Int32 nIndex3 = sqlDr.GetOrdinal("idFrecuencia");
                        Int32 nIndex4 = sqlDr.GetOrdinal("idMonedaPrima");
                        Int32 nIndex5 = sqlDr.GetOrdinal("idTipoDocumento");
                        Int32 nIndex6 = sqlDr.GetOrdinal("nroPoliza");

                        if (sqlDr.Read()) 
                        {
                            objBEOpcionPoliza = new BEOpcionPoliza();
                            objBEOpcionPoliza.IdProducto = sqlDr.GetInt32(nIndex1);
                            objBEOpcionPoliza.Opcion = sqlDr.GetString(nIndex2);
                            objBEOpcionPoliza.IdFrecuencia = sqlDr.GetInt32(nIndex3);
                            objBEOpcionPoliza.IdMonedaPrima = sqlDr.GetString(nIndex4);
                            objBEOpcionPoliza.IdTipoDocumento = sqlDr.GetString(nIndex5);
                            objBEOpcionPoliza.NroPoliza = sqlDr.GetString(nIndex6);
                        }
                        sqlDr.Close();                        
                    }
                }
            }

            return objBEOpcionPoliza;
        }
    }
}
