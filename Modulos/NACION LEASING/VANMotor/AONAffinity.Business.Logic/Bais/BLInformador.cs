﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.DataAccess;  
using AONAffinity.Motor.DataAccess.Bais;
using AONAffinity.Motor.BusinessEntity.Bais;

namespace AONAffinity.Motor.BusinessLogic.Bais
{
    public class BLInformador
    {
        #region NoTransaccional
        public List<BEInformador> ListarxSponsor(Int32 pnIdSponsor)
        {
            List<BEInformador> lstBEInformador = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAInformador objDAInformador = new DAInformador();
                SqlDataReader sqlDr = objDAInformador.ListarxSponsor(pnIdSponsor, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idInformador");
                        Int32 nIndex2 = sqlDr.GetOrdinal("nombre");

                        lstBEInformador = new List<BEInformador>();

                        while (sqlDr.Read())
                        {
                            BEInformador objBEInformador = new BEInformador();

                            objBEInformador.IdInformador = sqlDr.GetString(nIndex1);
                            objBEInformador.Nombre = sqlDr.GetString(nIndex2);

                            lstBEInformador.Add(objBEInformador);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBEInformador;
        }
        public BEInformador Obtener(String pcIdInformador) 
        {
            BEInformador objBEInformador = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus())) 
            {
                sqlCn.Open();
                
                DAInformador objDAInformador = new DAInformador();
                SqlDataReader sqlDr = objDAInformador.Obtener(pcIdInformador, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idInformador");
                        Int32 nIndex2 = sqlDr.GetOrdinal("cc");
                        Int32 nIndex3 = sqlDr.GetOrdinal("nombre");
                        Int32 nIndex4 = sqlDr.GetOrdinal("idOficina");
                        Int32 nIndex5 = sqlDr.GetOrdinal("nomOficina");
                        Int32 nIndex6 = sqlDr.GetOrdinal("idCargo");
                        Int32 nIndex7 = sqlDr.GetOrdinal("activo");
                        Int32 nIndex8 = sqlDr.GetOrdinal("idCanal");

                        if (sqlDr.Read()) 
                        {
                            objBEInformador = new BEInformador();

                            objBEInformador.IdInformador = sqlDr.GetString(nIndex1);
                            if (sqlDr.IsDBNull(nIndex2)) { objBEInformador.CC = NullTypes.CadenaNull; } else { objBEInformador.CC = sqlDr.GetString(nIndex2); }
                            if (sqlDr.IsDBNull(nIndex3)) { objBEInformador.Nombre = NullTypes.CadenaNull; } else { objBEInformador.Nombre = sqlDr.GetString(nIndex3); }
                            if (sqlDr.IsDBNull(nIndex4)) { objBEInformador.IdOficina = NullTypes.IntegerNull; } else { objBEInformador.IdOficina = sqlDr.GetInt32(nIndex4); }
                            if (sqlDr.IsDBNull(nIndex5)) { objBEInformador.NomOficina = NullTypes.CadenaNull; } else { objBEInformador.NomOficina = sqlDr.GetString(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBEInformador.IdCargo = NullTypes.IntegerNull; } else { objBEInformador.IdCargo = sqlDr.GetInt32(nIndex6); }
                            objBEInformador.Activo = sqlDr.GetBoolean(nIndex7);
                            if (sqlDr.IsDBNull(nIndex8)) { objBEInformador.IdCanal = NullTypes.IntegerNull; } else { objBEInformador.IdCanal = sqlDr.GetInt32(nIndex8); }
                        }

                        sqlDr.Close();  
                    }
                }                
            }

            return objBEInformador;
        }
        #endregion
    }
}
