﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;  
using AONAffinity.Motor.DataAccess.Bais;
using AONAffinity.Motor.BusinessEntity.Bais;

namespace AONAffinity.Motor.BusinessLogic.Bais
{
    /// <summary>
    /// Clase que contiene la lógica para la tabla Departamento;
    /// </summary>
    public class BLDepartamento
    {
        #region NoTransaccional
        /// <summary>
        /// Permite listar los departamentos por país.
        /// </summary>
        /// <param name="pnIdPais">Código de país.</param>
        /// <returns>Objeto List de tipo BEDepartamento.</returns>
        public List<BEDepartamento> Listar(Int32 pnIdPais)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-09-14
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            List<BEDepartamento> lstBEDepartamento = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus()))
            {
                sqlCn.Open();

                DADepartamento objDADepartamento = new DADepartamento();
                SqlDataReader sqlDr = objDADepartamento.Listar(pnIdPais, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idDepartamento");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idPais");
                        Int32 nIndex3 = sqlDr.GetOrdinal("nombre");

                        lstBEDepartamento = new List<BEDepartamento>();

                        while (sqlDr.Read())
                        {
                            BEDepartamento objBEDepartamento = new BEDepartamento();

                            objBEDepartamento.IdDepartamento = sqlDr.GetInt32(nIndex1);
                            objBEDepartamento.IdPais = sqlDr.GetInt32(nIndex2);
                            objBEDepartamento.Nombre = sqlDr.GetString(nIndex3);

                            lstBEDepartamento.Add(objBEDepartamento);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBEDepartamento;
        }
        #endregion             
    }
}
