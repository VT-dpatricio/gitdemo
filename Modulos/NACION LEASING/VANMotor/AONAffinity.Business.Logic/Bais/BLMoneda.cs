﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.DataAccess.Bais;
using AONAffinity.Motor.BusinessEntity.Bais;

namespace AONAffinity.Motor.BusinessLogic.Bais
{
    /// <summary>
    /// Clase de lógica del negocio de la tabla Moneda.
    /// </summary>
    public class BLMoneda
    {
        /// <summary>
        /// Permite listar la moneda prima por producto.
        /// </summary>
        /// <param name="pnIdProducto">Código de producto.</param>
        /// <returns>Objeto List de tipo BEMoneda.</returns>
        public List<BEMoneda> Listar(Int32 pnIdProducto)
        {
            /*---------------------------------------------------------------------------
            * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
            * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            List<BEMoneda> lstBEMoneda = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus())) 
            {
                sqlCn.Open();

                DAMoneda objDAMoneda = new DAMoneda();
                SqlDataReader sqlDr = objDAMoneda.Listar(pnIdProducto, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idMonedaPrima");
                        Int32 nIndex2 = sqlDr.GetOrdinal("nombre");

                        lstBEMoneda = new List<BEMoneda>();

                        while (sqlDr.Read()) 
                        {
                            BEMoneda objBEMoneda = new BEMoneda();

                            objBEMoneda.IdMoneda = sqlDr.GetString(nIndex1);
                            objBEMoneda.Nombre = sqlDr.GetString(nIndex2).ToUpper();

                            lstBEMoneda.Add(objBEMoneda);  
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return lstBEMoneda;
        }
    }
}
