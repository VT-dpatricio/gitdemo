﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.DataAccess.Bais;
using AONAffinity.Motor.BusinessEntity.Bais;
    
namespace AONAffinity.Motor.BusinessLogic.Bais
{
    public class BLInfoAsegurado
    {
        public List<BEInfoAsegurado> ObtenerxProducto(Int32 pnIdProducto)
        {
            List<BEInfoAsegurado> lstBEInfoAsegurado = new List<BEInfoAsegurado>();

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus()))
            {
                sqlCn.Open();

                DAInfoAsegurado objDAInfoAsegurado = new DAInfoAsegurado();
                SqlDataReader sqlDr = objDAInfoAsegurado.ObtenerxProducto(pnIdProducto, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idInfoAsegurado");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex3 = sqlDr.GetOrdinal("nombre");
                        Int32 nIndex4 = sqlDr.GetOrdinal("descripcion");

                        while (sqlDr.Read())
                        {
                            BEInfoAsegurado objBEInfoAsegurado = new BEInfoAsegurado();
                            objBEInfoAsegurado.IdInfoAsegurado = sqlDr.GetInt32(nIndex1);
                            objBEInfoAsegurado.IdProducto = sqlDr.GetInt32(nIndex2);
                            objBEInfoAsegurado.Nombre = sqlDr.GetString(nIndex3);
                            objBEInfoAsegurado.Descripcion = sqlDr.GetString(nIndex4);

                            lstBEInfoAsegurado.Add(objBEInfoAsegurado);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBEInfoAsegurado;
        }
    }
}
