﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.DataAccess.Bais;
using AONAffinity.Motor.BusinessEntity.Bais;

namespace AONAffinity.Motor.BusinessLogic.Bais
{
    /// <summary>
    /// Clase de lógica del negocio de la tabla InfoAseguradoC.
    /// </summary>
    public class BLInfoAseguradoC
    {
        #region NoTransaccional
        /// <summary>
        /// Permite listar los info asegurados por certificado.
        /// </summary>
        /// <param name="pcIdCertificado">Id de certificado.</param>
        /// <returns>Lista de tipo BEInfoAseguradoC.</returns>
        public List<BEInfoAseguradoC> ListarxCertificado(String pcIdCertificado)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-12-20
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            List<BEInfoAseguradoC> lstBEInfoAseguradoC = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus()))
            {
                sqlCn.Open();

                DAInfoAseguradoC objDAInfoAseguradoC = new DAInfoAseguradoC();
                SqlDataReader sqlDr = objDAInfoAseguradoC.ListarxCertificado(pcIdCertificado, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idInfoasegurado");
                        Int32 nIndex2 = sqlDr.GetOrdinal("consecutivo");
                        Int32 nIndex3 = sqlDr.GetOrdinal("nombre");
                        Int32 nIndex4 = sqlDr.GetOrdinal("idcertificado");
                        Int32 nIndex5 = sqlDr.GetOrdinal("valorDate");
                        Int32 nIndex6 = sqlDr.GetOrdinal("valorNum");
                        Int32 nIndex7 = sqlDr.GetOrdinal("valorString");                                                                      

                        lstBEInfoAseguradoC = new List<BEInfoAseguradoC>();

                        while (sqlDr.Read())
                        {
                            BEInfoAseguradoC objBEInfoAseguradoC = new BEInfoAseguradoC();
                            objBEInfoAseguradoC.IdInfoAsegurado = sqlDr.GetInt32(nIndex1);
                            objBEInfoAseguradoC.Consecutivo = sqlDr.GetInt16(nIndex2);
                            objBEInfoAseguradoC.Nombre = sqlDr.GetString(nIndex3);
                            objBEInfoAseguradoC.IdCertificado = sqlDr.GetString(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull; } else { objBEInfoAseguradoC.ValorDate = sqlDr.GetDateTime(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull; } else { objBEInfoAseguradoC.ValorNum = sqlDr.GetDecimal(nIndex6); }                            
                            if (sqlDr.IsDBNull(nIndex7)) { objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull; } else { objBEInfoAseguradoC.ValorString = sqlDr.GetString(nIndex7); }                            

                            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBEInfoAseguradoC;
        }
        #endregion       
    }
}
