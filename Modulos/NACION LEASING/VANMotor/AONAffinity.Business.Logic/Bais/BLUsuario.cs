﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;   
using AONAffinity.Motor.DataAccess.Bais;
using AONAffinity.Motor.BusinessEntity.Bais;

namespace AONAffinity.Motor.BusinessLogic.Bais
{
    public class BLUsuario
    {
        #region NoTransaccional
        public BEUsuario Obtener(String pcIdUsuario) 
        {
            BEUsuario objBEUsuario = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus())) 
            {
                sqlCn.Open();

                DAUsuario objDAUsuario = new DAUsuario();
                SqlDataReader sqlDr = objDAUsuario.Obtener(pcIdUsuario, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idUsuario"); 
                        Int32 nIndex2 = sqlDr.GetOrdinal("apellido1");
                        Int32 nIndex3 = sqlDr.GetOrdinal("apellido2");
                        Int32 nIndex4 = sqlDr.GetOrdinal("nombre1");
                        Int32 nIndex5 = sqlDr.GetOrdinal("nombre2");
                        Int32 nIndex6 = sqlDr.GetOrdinal("idInformador"); 
                      
                        objBEUsuario = new BEUsuario();

                        if (sqlDr.Read()) 
                        {
                            objBEUsuario.IdUsuario = sqlDr.GetString(nIndex1);
                            objBEUsuario.Apellido1 = sqlDr.GetString(nIndex2);
                            if (sqlDr.IsDBNull(nIndex3)) { objBEUsuario.Apellido2 = NullTypes.CadenaNull; } else { objBEUsuario.Apellido2 = sqlDr.GetString(nIndex3); }
                            objBEUsuario.Nombre1 = sqlDr.GetString(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBEUsuario.Nombre2 = NullTypes.CadenaNull; } else { objBEUsuario.Nombre2 = sqlDr.GetString(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBEUsuario.IdInformador = NullTypes.CadenaNull; } else { objBEUsuario.IdInformador = sqlDr.GetString(nIndex6); }
                        }

                        sqlDr.Close();  
                    }
                }
            }

            return objBEUsuario;
        }      
        #endregion
    }
}
