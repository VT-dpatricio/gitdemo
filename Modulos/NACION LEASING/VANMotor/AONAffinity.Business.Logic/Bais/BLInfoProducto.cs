﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using AONAffinity.Motor.Resources;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.DataAccess.Bais;
using AONAffinity.Motor.BusinessEntity.Bais;

namespace AONAffinity.Motor.BusinessLogic.Bais
{
    public class BLInfoProducto
    {
        public List<BEInfoProducto> ObtenerxProducto(Int32 pnIdProducto) 
        {
            List<BEInfoProducto> lstBEInfoProducto = new List<BEInfoProducto>();

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus())) 
            {
                sqlCn.Open();

                DAInfoProducto objDAInfoProducto = new DAInfoProducto();
                SqlDataReader sqlDr = objDAInfoProducto.ObtenerxProducto(pnIdProducto, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idInfoProducto");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex3 = sqlDr.GetOrdinal("nombre");
                        Int32 nIndex4 = sqlDr.GetOrdinal("descripcion");

                        while (sqlDr.Read())
                        {
                            BEInfoProducto objBEInfoProducto = new BEInfoProducto();
                            objBEInfoProducto.IdInfoProducto = sqlDr.GetInt32(nIndex1);
                            objBEInfoProducto.IdProducto = sqlDr.GetInt32(nIndex2);
                            objBEInfoProducto.Nombre = sqlDr.GetString(nIndex3);
                            objBEInfoProducto.Descripcion = sqlDr.GetString(nIndex4);

                            lstBEInfoProducto.Add(objBEInfoProducto);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBEInfoProducto;
        }
    }
}
