﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Motor.Resources;
using AONAffinity.Library.Resources;
using AONAffinity.Library.DataAccess;
using AONAffinity.Library.DataAccess.BDJanus;
using AONAffinity.Library.BusinessEntity.BDJanus;

namespace AONAffinity.Motor.BusinessLogic.Bais
{
    /// <summary>
    /// Clase de lógica del negocio de la tabla InfoProductoC.
    /// </summary>
    public class BLInfoProductoC
    {
        #region NoTransaccional
        /// <summary>
        /// Permite listar los info productos por certificado.
        /// </summary>
        /// <param name="pcIdCertificado">Id de certificado.</param>
        /// <returns>Lista de tipo BEInfoProductoC.</returns>
        public List<BEInfoProductoC> ListarxCertificado(String pcIdCertificado)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-12-20
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            List<BEInfoProductoC> lstBEInfoProductoC = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus()))
            {
                sqlCn.Open();

                DAInfoProductoC objDAInfoProductoC = new DAInfoProductoC();
                SqlDataReader sqlDr = objDAInfoProductoC.ListarxCertificado(pcIdCertificado, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idInfoProducto");
                        Int32 nIndex2 = sqlDr.GetOrdinal("nombre");
                        Int32 nIndex3 = sqlDr.GetOrdinal("idCertificado");
                        Int32 nIndex4 = sqlDr.GetOrdinal("valorDate");
                        Int32 nIndex5 = sqlDr.GetOrdinal("valorNum");
                        Int32 nIndex6 = sqlDr.GetOrdinal("valorString");

                        lstBEInfoProductoC = new List<BEInfoProductoC>();

                        while (sqlDr.Read())
                        {
                            BEInfoProductoC objBEInfoProductoC = new BEInfoProductoC();
                            objBEInfoProductoC.IdInfoProducto = sqlDr.GetInt32(nIndex1);
                            objBEInfoProductoC.Nombre = sqlDr.GetString(nIndex2);
                            objBEInfoProductoC.IdCertificado = sqlDr.GetString(nIndex3);
                            if (sqlDr.IsDBNull(nIndex4)) { objBEInfoProductoC.ValorDate = NullTypes.FechaNull; } else { objBEInfoProductoC.ValorDate = sqlDr.GetDateTime(nIndex4); }
                            if (sqlDr.IsDBNull(nIndex5)) { objBEInfoProductoC.ValorNum = NullTypes.DecimalNull; } else { objBEInfoProductoC.ValorNum = sqlDr.GetDecimal(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBEInfoProductoC.ValorString = NullTypes.CadenaNull; } else { objBEInfoProductoC.ValorString = sqlDr.GetString(nIndex6); }

                            lstBEInfoProductoC.Add(objBEInfoProductoC);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBEInfoProductoC;
        }
        #endregion        
    }
}
