﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Resources;
using AONAffinity.Business.Entity.BDMotor;
using AONAffinity.Data.DataAccess;
using AONAffinity.Data.DataAccess.BDMotor;

namespace AONAffinity.Business.Logic
{
    /// <summary>
    /// Clase de lógica del negocio de la tabla Conbustible.
    /// </summary>
    public class BLConbustible
    {
        #region NoTransaccional
        /// <summary>
        /// Permite listar los tipos de conbustibles.
        /// </summary>
        /// <param name="pbStsRegistro">Estado del registro.</param>
        /// <returns>Objeto de List de tipo BEConbustible.</returns>
        public List<BECombustible> Listar(Nullable<Boolean> pbStsRegistro) 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-31
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            List<BECombustible> lstBEConbustible = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DACombustible objDACombustible = new DACombustible();
                SqlDataReader sqlDr = objDACombustible.Listar(pbStsRegistro, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idCombustible");
                        Int32 nIndex2 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex4 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex5 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex6 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("stsRegistro");

                        lstBEConbustible = new List<BECombustible>();

                        while (sqlDr.Read()) 
                        {
                            BECombustible objBEConbustible = new BECombustible();

                            objBEConbustible.IdCombustible = sqlDr.GetInt32(nIndex1);
                            objBEConbustible.Descripcion = sqlDr.GetString(nIndex2);
                            objBEConbustible.UsuarioCreacion = sqlDr.GetString(nIndex3);
                            objBEConbustible.FechaCreacion = sqlDr.GetDateTime(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBEConbustible.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEConbustible.UsuarioModificacion = sqlDr.GetString(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBEConbustible.FechaModificacion = NullTypes.FechaNull; } else { objBEConbustible.FechaModificacion = sqlDr.GetDateTime(nIndex6); }
                            objBEConbustible.EstadoRegistro = sqlDr.GetBoolean(nIndex7);

                            lstBEConbustible.Add(objBEConbustible);  
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return lstBEConbustible;
        }  
        #endregion
    }
}
