﻿//OK
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;
using AONAffinity.Motor.BusinessEntity.Bais;

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLGeneraArchivo
    {
        #region PDF Cotizacion
        public String CotizacionPDF(BECotizacion pObjBECotizacion, BEUsuario pObjBEUsuario)
        {
            String cNombrePdf = String.Empty;

            if (pObjBECotizacion != null) 
            {
                String cRuta = AppSettings.DirCotizacionRIMAC;
                String cArchivo = AppSettings.ArchivoCotizacionRIMAC + pObjBECotizacion.IdPeriodo + "-" + pObjBECotizacion.NroCotizacion + ".pdf";
                Document objDocument = new Document(PageSize.A4, 60, 60, 60, 60);
                PdfWriter.GetInstance(objDocument, new FileStream(String.Concat(cRuta, cArchivo), FileMode.Create));
                objDocument.Open();

                if (pObjBECotizacion != null)
                {
                    String cLogoRimac = AppSettings.DirImagenesRIMAC + AppSettings.LogoRimac;                    

                    this.InsertarImagenPDF(objDocument, cLogoRimac);
                    this.InsertarTituloPDF(objDocument, "COTIZACIÓN DE SEGURO VEHICULAR");
                    this.InsertarCabeceraPDF(objDocument, pObjBECotizacion, pObjBEUsuario); 
                    this.InsertarDetallePDF(objDocument, pObjBECotizacion);

                    cNombrePdf = cArchivo;
                }

                objDocument.Close();
            }
           
            return cNombrePdf;
        }

        private void InsertarImagenPDF(Document pObjDocument, String pcImagenIzq)
        {
            Table tbImagen = new Table(2, 1);
            tbImagen.Border = 0;
            tbImagen.Padding = 0;
            tbImagen.Spacing = 0;
            tbImagen.DefaultCellBorder = 0;

            Int32[] nArrTamanio = { 50, 50 };

            tbImagen.SetWidths(nArrTamanio);
            tbImagen.Width = 100;

            if (File.Exists(pcImagenIzq))
            {
                Image imgLogo = Image.GetInstance(pcImagenIzq);
                imgLogo.Alignment = Image.ALIGN_LEFT;
                imgLogo.ScaleAbsolute(80, 35);
                tbImagen.AddCell(new Cell(imgLogo));
                pObjDocument.Add(tbImagen);
            }
        }

        private void InsertarTituloPDF(Document pObjDocument, String pcTitulo)
        {
            pObjDocument.Add(new Paragraph(String.Empty));

            Table tbTitulo = new Table(1, 1);
            tbTitulo.Border = 0;
            tbTitulo.Padding = 0;
            tbTitulo.Spacing = 0;
            tbTitulo.DefaultCellBorder = 0;

            Int32[] nArrTamanio = { 100 };
            tbTitulo.SetWidths(nArrTamanio);
            tbTitulo.Width = 100;
            tbTitulo.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            tbTitulo.DefaultCellBorder = 0;            

            Phrase phrTexto = new Phrase(pcTitulo, new Font(Font.HELVETICA, 12, Font.BOLD));
            tbTitulo.AddCell(phrTexto);            
            pObjDocument.Add(tbTitulo);
        }

        private void InsertarCabeceraPDF(Document pObjDocument, BECotizacion pObjBECotizacion, BEUsuario pObjBEUsuario)
        {
            pObjDocument.Add(new Paragraph(String.Empty));

            Table tbCabecera = new Table(4, 2);
            tbCabecera.Border = 0;
            tbCabecera.Padding = 0;
            tbCabecera.Spacing = 0;
            tbCabecera.DefaultCellBorder = 0;

            Int32[] nArrTamanio = { 20, 45, 10, 25 };
            tbCabecera.SetWidths(nArrTamanio);
            tbCabecera.Width = 100;
            tbCabecera.DefaultCellBorder = 0;

            tbCabecera.AddCell(this.ObtenerEtiqueta("Nro. Cotización: "));
            tbCabecera.AddCell(this.ObtenerEtiqueta(pObjBECotizacion.IdPeriodo + "-" + pObjBECotizacion.NroCotizacion));
            tbCabecera.AddCell(this.ObtenerEtiqueta("Fecha: "));
            tbCabecera.AddCell(this.ObtenerEtiqueta(DateTime.Now.ToShortDateString()));
            tbCabecera.AddCell(this.ObtenerEtiqueta("Cliente: "));
            tbCabecera.AddCell(this.ObtenerEtiqueta(pObjBECotizacion.PriNombre + " " + pObjBECotizacion.SegNombre + " " + pObjBECotizacion.ApePaterno + " " + pObjBECotizacion.ApeMaterno));
            tbCabecera.AddCell(this.ObtenerEtiqueta("Asesor: "));
            tbCabecera.AddCell(this.ObtenerEtiqueta(pObjBEUsuario.Nombre1 + " " + pObjBEUsuario.Apellido1));

            pObjDocument.Add(tbCabecera);
        }

        private void InsertarDetallePDF(Document pObjDocument, BECotizacion pObjBECotizacion) 
        {
            pObjDocument.Add(new Paragraph(String.Empty));
            Table tbVehiculo = this.ObtenerTablaVehiculo(pObjBECotizacion);
            pObjDocument.Add(tbVehiculo);

            //Si cotizacion tiene descuento se muestra cuadro de descuento,
            //caso contrario se muestra la prima corespondiente
            if (pObjBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.DESCUENTO))
            {
                pObjDocument.Add(new Paragraph(String.Empty));
                Table tbDescuento = this.ObtenerTablaDescuento(pObjBECotizacion);
                pObjDocument.Add(tbDescuento);
            }
            else 
            {
                pObjDocument.Add(new Paragraph(String.Empty));
                Table tbMontos = this.ObtenerTablaMontos(pObjBECotizacion);
                pObjDocument.Add(tbMontos); 
            }              
        }

        private Cell ObtenerTituloTabla(String pcTitulo, Int32 pnColspan) 
        {
            Phrase phrTitulo = new Phrase(pcTitulo, new Font(Font.HELVETICA, 10, Font.BOLD));
            Cell clTitulo = new Cell(phrTitulo);
            clTitulo.Colspan = pnColspan;
            clTitulo.HorizontalAlignment = Element.ALIGN_CENTER;
            clTitulo.VerticalAlignment = Element.ALIGN_TOP;
            clTitulo.BackgroundColor = Color.LIGHT_GRAY;

            return clTitulo;
        }

        private Cell ObtenerCeldaDefault() 
        {
            Cell clDefault = new Cell();
            clDefault.BorderWidthBottom = 1;
            clDefault.BorderWidthLeft = 1;
            clDefault.BorderWidthRight = 1;
            clDefault.BorderWidthTop = 1;

            return clDefault;
        }

        private Phrase ObtenerEtiqueta(String pcEtiqueta) 
        {
            Phrase phrEtiqueta = new Phrase(pcEtiqueta, new Font(Font.HELVETICA, 10, Font.HELVETICA));
            return phrEtiqueta;
        }

        private Table ObtenerTablaVehiculo(BECotizacion pObjBECotizacion) 
        {
            Table tbVehiculo = new Table(2, 5);
            Int32[] nArrTamanio = { 25, 25 };
            
            tbVehiculo.DefaultCellBorderColor = Color.BLACK;
            tbVehiculo.DefaultCellBorderWidth = 1;
            tbVehiculo.Alignment = Element.ALIGN_LEFT;
            tbVehiculo.Padding = 1;
            tbVehiculo.Spacing = 1;
            tbVehiculo.DefaultCellBorder = 1;
            tbVehiculo.SetWidths(nArrTamanio);
            tbVehiculo.Width = 50;
            tbVehiculo.DefaultCell = this.ObtenerCeldaDefault();
            tbVehiculo.AddCell(this.ObtenerTituloTabla("Datos del Vehículo", 2));
            tbVehiculo.AddCell(this.ObtenerEtiqueta("Marca: "));
            tbVehiculo.AddCell(this.ObtenerEtiqueta(pObjBECotizacion.DesMarca));
            tbVehiculo.AddCell(this.ObtenerEtiqueta("Modelo: "));
            tbVehiculo.AddCell(this.ObtenerEtiqueta(pObjBECotizacion.DesModelo));
            tbVehiculo.AddCell(this.ObtenerEtiqueta("Año: "));
            tbVehiculo.AddCell(this.ObtenerEtiqueta(pObjBECotizacion.AnioFab.ToString()));
            tbVehiculo.AddCell(this.ObtenerEtiqueta("Valor Comercial: "));
            tbVehiculo.AddCell(this.ObtenerEtiqueta("$" + Decimal.Round(pObjBECotizacion.ValVehiculo, 2).ToString()));

            return tbVehiculo;
        }

        private Table ObtenerTablaDescuento(BECotizacion pObjBECotizacion) 
        {
            Table tbDescuento = new Table(4, 7);
            Int32[] nArrTamanio = { 25, 25, 25, 25 };            

            tbDescuento.DefaultCellBorderColor = Color.BLACK;
            tbDescuento.DefaultCellBorderWidth = 1;
            tbDescuento.Alignment = Element.ALIGN_LEFT;
            tbDescuento.Padding = 1;
            tbDescuento.Spacing = 1;
            tbDescuento.DefaultCellBorder = 1;
            tbDescuento.SetWidths(nArrTamanio);
            tbDescuento.Width = 100;
            tbDescuento.DefaultCell = this.ObtenerCeldaDefault();
            tbDescuento.AddCell(this.ObtenerTituloTabla("Descuento en Tasa " + Decimal.Round(pObjBECotizacion.PorcentajeC, 0).ToString() + "%" , 4));
            tbDescuento.AddCell(this.ObtenerTituloTabla("Anual", 2));
            tbDescuento.AddCell(this.ObtenerTituloTabla("Bianual", 2));
            tbDescuento.AddCell(this.ObtenerEtiqueta("Tasa: "));
            tbDescuento.AddCell(this.ObtenerEtiqueta(pObjBECotizacion.TasaAnual == NullTypes.DecimalNull ? String.Empty : pObjBECotizacion.TasaAnual.ToString()));
            tbDescuento.AddCell(this.ObtenerEtiqueta("Tasa: "));
            tbDescuento.AddCell(this.ObtenerEtiqueta(pObjBECotizacion.TasaBianual == NullTypes.DecimalNull ? String.Empty : pObjBECotizacion.TasaBianual.ToString()));
            tbDescuento.AddCell(this.ObtenerEtiqueta("Prima: "));
            tbDescuento.AddCell(this.ObtenerEtiqueta(pObjBECotizacion.PrimaAnual == NullTypes.DecimalNull ? String.Empty : "$" + Decimal.Round(pObjBECotizacion.PrimaAnual, 2).ToString()));
            tbDescuento.AddCell(this.ObtenerEtiqueta("Prima: "));
            tbDescuento.AddCell(this.ObtenerEtiqueta(pObjBECotizacion.PrimaBianualC == NullTypes.DecimalNull ? String.Empty : "$" + Decimal.Round(pObjBECotizacion.PrimaBianual, 2).ToString()));
            tbDescuento.AddCell(this.ObtenerEtiqueta("Tasa Dscto.: "));
            tbDescuento.AddCell(this.ObtenerEtiqueta(pObjBECotizacion.TasaAnualC == NullTypes.DecimalNull ? String.Empty : pObjBECotizacion.TasaAnualC.ToString()));
            tbDescuento.AddCell(this.ObtenerEtiqueta("Tasa Dscto.: "));
            tbDescuento.AddCell(this.ObtenerEtiqueta(pObjBECotizacion.TasaBianualC == NullTypes.DecimalNull ? String.Empty : pObjBECotizacion.TasaBianualC.ToString()));
            tbDescuento.AddCell(this.ObtenerEtiqueta("Prima Dscto.: "));
            tbDescuento.AddCell(this.ObtenerEtiqueta(pObjBECotizacion.PrimaAnualC == NullTypes.DecimalNull ? String.Empty : "$" + Decimal.Round(pObjBECotizacion.PrimaAnualC, 2).ToString()));
            tbDescuento.AddCell(this.ObtenerEtiqueta("Prima Dscto.: "));
            tbDescuento.AddCell(this.ObtenerEtiqueta(pObjBECotizacion.PrimaBianualC == NullTypes.DecimalNull ? String.Empty : "$" + Decimal.Round(pObjBECotizacion.PrimaBianualC, 2).ToString()));
            tbDescuento.AddCell(this.ObtenerEtiqueta("Prima 12 Meses: "));
            tbDescuento.AddCell(this.ObtenerEtiqueta(this.ObtenerPrimaMensualCondicion(pObjBECotizacion, 1)));
            tbDescuento.AddCell(this.ObtenerEtiqueta("Prima 24 Meses: "));
            tbDescuento.AddCell(this.ObtenerEtiqueta(this.ObtenerPrimaMensualCondicion(pObjBECotizacion, 2)));
 
            return tbDescuento;
        }

        private Table ObtenerTablaMontos(BECotizacion pObjBECotizacion) 
        {
            Table tbMontos = new Table(4, 5);
            Int32[] nArrTamanio = { 25, 25, 25, 25 };            

            tbMontos.DefaultCellBorderColor = Color.BLACK;
            tbMontos.DefaultCellBorderWidth = 1;
            tbMontos.Alignment = Element.ALIGN_LEFT;
            tbMontos.Padding = 1;
            tbMontos.Spacing = 1;
            tbMontos.DefaultCellBorder = 1;
            tbMontos.SetWidths(nArrTamanio);
            tbMontos.Width = 100;
            tbMontos.DefaultCell = this.ObtenerCeldaDefault();
            tbMontos.AddCell(this.ObtenerTituloTabla("Información de Prima", 4));
            tbMontos.AddCell(this.ObtenerTituloTabla("Anual", 2));
            tbMontos.AddCell(this.ObtenerTituloTabla("Bianual", 2));
            tbMontos.AddCell(this.ObtenerEtiqueta("Tasa: "));
            tbMontos.AddCell(this.ObtenerEtiqueta(this.ObtenerTasaCondicion(pObjBECotizacion, 1)));
            tbMontos.AddCell(this.ObtenerEtiqueta("Tasa: "));
            tbMontos.AddCell(this.ObtenerEtiqueta(this.ObtenerTasaCondicion(pObjBECotizacion, 2)));
            tbMontos.AddCell(this.ObtenerEtiqueta("Prima: "));
            tbMontos.AddCell(this.ObtenerEtiqueta(this.ObtenerPrimaCondicion(pObjBECotizacion, 1)));
            tbMontos.AddCell(this.ObtenerEtiqueta("Prima: "));
            tbMontos.AddCell(this.ObtenerEtiqueta(this.ObtenerPrimaCondicion(pObjBECotizacion, 2)));
            tbMontos.AddCell(this.ObtenerEtiqueta("Prima 12 Meses: "));
            tbMontos.AddCell(this.ObtenerEtiqueta(this.ObtenerPrimaMensualCondicion(pObjBECotizacion, 1)));
            tbMontos.AddCell(this.ObtenerEtiqueta("Prima 24 Meses: "));
            tbMontos.AddCell(this.ObtenerEtiqueta(this.ObtenerPrimaMensualCondicion(pObjBECotizacion, 2)));
            

            return tbMontos;
        }

        public String ObtenerTasaCondicion(BECotizacion pObjBECotizacion, Int32 pnPeriodo) 
        {
            String cTasa = String.Empty;

            //Periodo Anual
            if (pnPeriodo == 1)
            {
                //No tiene descuento, se muestra tasa correspondiente, 
                //caso contrario se muestra tasa anual con descuento o recargo.
                if (pObjBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.NO_APLICA))
                {
                    if (pObjBECotizacion.TasaAnual != NullTypes.DecimalNull)
                    {
                        cTasa = pObjBECotizacion.TasaAnual.ToString(); 
                    }                    
                }
                else 
                {
                    if (pObjBECotizacion.TasaAnualC != NullTypes.DecimalNull)
                    {
                        cTasa = pObjBECotizacion.TasaAnualC.ToString();
                    }
                }
            }

            //Periodo Bianual
            if (pnPeriodo == 2) 
            {
                //No tiene descuento, se muestra tasa correspondiente, 
                //caso contrario se muestra tasa bianual con descuento o recargo.
                if (pObjBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.NO_APLICA))
                {
                    if (pObjBECotizacion.TasaBianual != NullTypes.DecimalNull) 
                    {
                        cTasa = pObjBECotizacion.TasaBianual.ToString(); 
                    }     
                }
                else 
                {
                    if (pObjBECotizacion.TasaBianualC != NullTypes.DecimalNull) 
                    {
                        cTasa = pObjBECotizacion.TasaBianualC.ToString();  
                    }
                }
            }

            return cTasa;
        }

        public String ObtenerPrimaCondicion(BECotizacion pObjBECotizacion, Int32 pnPeriodo) 
        {
            String cPrima = String.Empty;

            //Periodo Anual
            if (pnPeriodo == 1) 
            {
                //No tiene descuento, se muestra prima correspondiente, 
                //caso contrario se muestra prima anual con descuento o recargo.
                if (pObjBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.NO_APLICA))
                {
                    if (pObjBECotizacion.PrimaAnual != NullTypes.DecimalNull)
                    {
                        cPrima = "$" + Decimal.Round(pObjBECotizacion.PrimaAnual, 2).ToString();
                    }
                }
                else 
                {
                    if (pObjBECotizacion.PrimaAnualC != NullTypes.DecimalNull) 
                    {
                        cPrima = "$" + Decimal.Round(pObjBECotizacion.PrimaAnualC, 2).ToString();
                    }
                }
            }

            //Periodo Bianual
            if (pnPeriodo == 2) 
            {
                //No tiene descuento, se muestra prima correspondiente, 
                //caso contrario se muestra prima bianual con descuento o recargo.
                if (pObjBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.NO_APLICA))
                {
                    if (pObjBECotizacion.PrimaBianual != NullTypes.DecimalNull)
                    {
                        cPrima = "$" + Decimal.Round(pObjBECotizacion.PrimaBianual, 2).ToString();
                    }
                }
                else 
                {
                    if (pObjBECotizacion.PrimaBianualC != NullTypes.DecimalNull)
                    {
                        cPrima = "$" + Decimal.Round(pObjBECotizacion.PrimaBianualC, 2).ToString();
                    }
                }
            }

            return cPrima;
        }

        public String ObtenerPrimaMensualCondicion(BECotizacion pObjBECotizacion, Int32 pnPeriodo) 
        {
            String cPrima = String.Empty;

            //Periodo Anual
            if (pnPeriodo == 1)
            {
                //No tiene descuento, se muestra prima correspondiente, 
                //caso contrario se muestra prima anual con descuento o recargo.
                if (pObjBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.NO_APLICA))
                {
                    if (pObjBECotizacion.PrimaAnual != NullTypes.DecimalNull)
                    {
                        cPrima = "$" + Decimal.Round((pObjBECotizacion.PrimaAnual / 12), 2).ToString();
                    }
                }
                else
                {
                    if (pObjBECotizacion.PrimaAnualC != NullTypes.DecimalNull)
                    {
                        cPrima = "$" + Decimal.Round((pObjBECotizacion.PrimaAnualC / 12), 2).ToString();
                    }
                }
            }

            //Periodo Bianual
            if (pnPeriodo == 2)
            {
                //No tiene descuento, se muestra prima correspondiente, 
                //caso contrario se muestra prima bianual con descuento o recargo.
                if (pObjBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.NO_APLICA))
                {
                    if (pObjBECotizacion.PrimaBianual != NullTypes.DecimalNull)
                    {
                        cPrima = "$" + Decimal.Round((pObjBECotizacion.PrimaBianual / 24), 2).ToString();
                    }
                }
                else
                {
                    if (pObjBECotizacion.PrimaBianualC != NullTypes.DecimalNull)
                    {
                        cPrima = "$" + Decimal.Round((pObjBECotizacion.PrimaBianualC / 24), 2).ToString();
                    }
                }
            }

            return cPrima;
        }
        #endregion
    }
}
