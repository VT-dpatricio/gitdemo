﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Resources;
using AONAffinity.Business.Entity.BDMotor;
using AONAffinity.Data.DataAccess;
using AONAffinity.Data.DataAccess.BDMotor;      

namespace AONAffinity.Business.Logic
{
    /// <summary>
    /// Clase de lógica del negocio de la tabla AccesorioVehiculo.
    /// </summary>
    public class BLAccesorioVehiculo
    {
        #region Transaccional
        /// <summary>
        /// Permite insertar un accesorio del vehículo.
        /// </summary>
        /// <param name="pObjBEAccesorioVehiculo">onjeto que contiene la información del accesorio del vehículo.</param>
        /// <returns>La cantidad de registros insertados.</returns>
        public Int32 Insertar(BEAccesorioVehiculo pObjBEAccesorioVehiculo) 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-02
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAAccesorioVehiculo objDAAccesorioVehiculo = new DAAccesorioVehiculo();
                nResult = objDAAccesorioVehiculo.Insertar(pObjBEAccesorioVehiculo, sqlCn);    
            }

            return nResult;
        }

        /// <summary>
        /// Permite actualizar el estado del accesorio del vehículo.
        /// </summary>
        /// <param name="pObjBEAccesorioVehiculo">Objeto que contiene la información del accesorio del vehículo.</param>
        /// <returns>La cantidad de registros actualizados.</returns>
        public Int32 ActualizarEstado(BEAccesorioVehiculo pObjBEAccesorioVehiculo) 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-14
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAAccesorioVehiculo objDAAccesorioVehiculo = new DAAccesorioVehiculo();
                nResult = objDAAccesorioVehiculo.ActualizarEstado(pObjBEAccesorioVehiculo, sqlCn);
            }

            return nResult;
        }

        /// <summary>
        /// Permite actualizar la información del accesorio del vehículo.
        /// </summary>
        /// <param name="pObjBEAccesorioVehiculo">Objeto que contiene la información del accesorio del vehículo.</param>
        /// <returns>La cantidad de registros actualizados.</returns>
        public Int32 Actualizar(BEAccesorioVehiculo pObjBEAccesorioVehiculo) 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-14
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAAccesorioVehiculo objDAAccesorioVehiculo = new DAAccesorioVehiculo();
                nResult = objDAAccesorioVehiculo.Actualizar(pObjBEAccesorioVehiculo, sqlCn);  
            }

            return nResult;
        }
        #endregion

        #region NoTransaccional
        /// <summary>
        /// Permite listar los accesorios del vehiculo por cotización.
        /// </summary>
        /// <param name="pnIdCotizacion">Código de cotización.</param>
        /// <param name="pbStsRegistro">Estado del registro.</param>
        /// <returns>Objeto List de tipo BEAccesorioVehiculo.</returns>
        public List<BEAccesorioVehiculo> Listar(Decimal pnIdCotizacion, Nullable<Boolean> pbStsRegistro) 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-02
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            List<BEAccesorioVehiculo> lstBEAccesorioVehiculo = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAAccesorioVehiculo objDAAccesorioVehiculo = new DAAccesorioVehiculo();
                SqlDataReader sqlDr = objDAAccesorioVehiculo.Listar(pnIdCotizacion, pbStsRegistro, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idCotizacion");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idAccesorio");
                        Int32 nIndex3 = sqlDr.GetOrdinal("valorAccesorio");
                        Int32 nIndex4 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex5 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex6 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex8 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex9 = sqlDr.GetOrdinal("stsRegistro");
                        Int32 nIndex10 = sqlDr.GetOrdinal("desAccesorio");

                        lstBEAccesorioVehiculo = new List<BEAccesorioVehiculo>();

                        while (sqlDr.Read()) 
                        {
                            BEAccesorioVehiculo objBEAccesorioVehiculo = new BEAccesorioVehiculo();

                            objBEAccesorioVehiculo.IdCotizacion = sqlDr.GetDecimal(nIndex1);
                            objBEAccesorioVehiculo.IdAccesorio = sqlDr.GetInt32(nIndex2);
                            if (sqlDr.IsDBNull(nIndex3)) { objBEAccesorioVehiculo.ValorAccesorio = NullTypes.DecimalNull; } else { objBEAccesorioVehiculo.ValorAccesorio = sqlDr.GetDecimal(nIndex3); }
                            if (sqlDr.IsDBNull(nIndex4)) { objBEAccesorioVehiculo.Descripcion = NullTypes.CadenaNull; } else { objBEAccesorioVehiculo.Descripcion = sqlDr.GetString(nIndex4); }
                            objBEAccesorioVehiculo.UsuarioCreacion = sqlDr.GetString(nIndex5);
                            objBEAccesorioVehiculo.FechaCreacion = sqlDr.GetDateTime(nIndex6);
                            if (sqlDr.IsDBNull(nIndex7)) { objBEAccesorioVehiculo.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEAccesorioVehiculo.UsuarioModificacion = sqlDr.GetString(nIndex7); }
                            if (sqlDr.IsDBNull(nIndex8)) { objBEAccesorioVehiculo.FechaModificacion = NullTypes.FechaNull; } else { objBEAccesorioVehiculo.FechaModificacion = sqlDr.GetDateTime(nIndex8); }
                            objBEAccesorioVehiculo.EstadoRegistro = sqlDr.GetBoolean(nIndex9);
                            objBEAccesorioVehiculo.DesAccesorio = sqlDr.GetString(nIndex10);

                            lstBEAccesorioVehiculo.Add(objBEAccesorioVehiculo);
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return lstBEAccesorioVehiculo;
        }

        /// <summary>
        /// Permite obtener un registro de accesorio del vehiculo de cotización.
        /// </summary>
        /// <param name="pnIdCotizacion">Código de cotizacion.</param>
        /// <param name="pnIdAccesorio">Código de accesorio.</param>
        /// <returns>Objeto de tipo BEAccesorioVehiculo.</returns>
        public BEAccesorioVehiculo Obtener(Decimal pnIdCotizacion, Int32 pnIdAccesorio) 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-11
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BEAccesorioVehiculo objBEAccesorioVehiculo = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAAccesorioVehiculo objDAAccesorioVehiculo = new DAAccesorioVehiculo();
                SqlDataReader sqlDr = objDAAccesorioVehiculo.Obtener(pnIdCotizacion, pnIdAccesorio, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idCotizacion");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idAccesorio");
                        Int32 nIndex3 = sqlDr.GetOrdinal("valorAccesorio");
                        Int32 nIndex4 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex5 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex6 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex8 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex9 = sqlDr.GetOrdinal("stsRegistro");

                        objBEAccesorioVehiculo = new BEAccesorioVehiculo();

                        if (sqlDr.Read())
                        {
                            objBEAccesorioVehiculo.IdCotizacion = sqlDr.GetDecimal(nIndex1);
                            objBEAccesorioVehiculo.IdAccesorio = sqlDr.GetInt32(nIndex2);
                            if (sqlDr.IsDBNull(nIndex3)) { objBEAccesorioVehiculo.ValorAccesorio = NullTypes.DecimalNull; } else { objBEAccesorioVehiculo.ValorAccesorio = sqlDr.GetDecimal(nIndex3); }
                            if (sqlDr.IsDBNull(nIndex4)) { objBEAccesorioVehiculo.Descripcion = NullTypes.CadenaNull; } else { objBEAccesorioVehiculo.Descripcion = sqlDr.GetString(nIndex4); }
                            objBEAccesorioVehiculo.UsuarioCreacion = sqlDr.GetString(nIndex5);
                            objBEAccesorioVehiculo.FechaCreacion = sqlDr.GetDateTime(nIndex6);
                            if (sqlDr.IsDBNull(nIndex7)) { objBEAccesorioVehiculo.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEAccesorioVehiculo.UsuarioModificacion = sqlDr.GetString(nIndex7); }
                            if (sqlDr.IsDBNull(nIndex8)) { objBEAccesorioVehiculo.FechaModificacion = NullTypes.FechaNull; } else { objBEAccesorioVehiculo.FechaModificacion = sqlDr.GetDateTime(nIndex8); }
                            objBEAccesorioVehiculo.EstadoRegistro = sqlDr.GetBoolean(nIndex9);
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return objBEAccesorioVehiculo;
        } 
        #endregion
    }
}
