﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Resources;
using AONAffinity.Business.Entity.BDMotor;
using AONAffinity.Data.DataAccess;
using AONAffinity.Data.DataAccess.BDMotor;

namespace AONAffinity.Business.Logic
{
    /// <summary>
    /// Clase de lógica del negocio de la tabla Accesorio.
    /// </summary>
    public class BLAccesorio
    {
        #region Transaccional
        /// <summary>
        /// Permite listar los accesorios de los vehículos
        /// </summary>
        /// <param name="pbStsRegistro">Estado del registro.</param>
        /// <returns>Objeto lista de tipo BEAccesorio.</returns>
        public List<BEAccesorio> Listar(Nullable<Boolean> pbStsRegistro) 
        {
            /*
            * CREADO POR:              Gary Porras Paraguay
            * FECHA DE CREACION:       2011-06-01
            * MODIFICADO POR:          
            * FECHA DE MODIFICACION:   
            */

            List<BEAccesorio> lstBEAccesorio = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAAccesorio objDAAccesorio = new DAAccesorio();
                SqlDataReader sqlDr = objDAAccesorio.Listar(pbStsRegistro, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idAccesorio");
                        Int32 nIndex2 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex4 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex5 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex6 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("stsRegistro");

                        lstBEAccesorio = new List<BEAccesorio>();

                        while (sqlDr.Read()) 
                        {
                            BEAccesorio objBEAccesorio = new BEAccesorio();

                            objBEAccesorio.IdAccesorio = sqlDr.GetInt32(nIndex1);
                            objBEAccesorio.Descripcion = sqlDr.GetString(nIndex2);
                            objBEAccesorio.UsuarioCreacion = sqlDr.GetString(nIndex3);
                            objBEAccesorio.FechaCreacion = sqlDr.GetDateTime(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBEAccesorio.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEAccesorio.UsuarioModificacion = sqlDr.GetString(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBEAccesorio.FechaModificacion = NullTypes.FechaNull; } else { objBEAccesorio.FechaModificacion = sqlDr.GetDateTime(nIndex6); }
                            objBEAccesorio.EstadoRegistro = sqlDr.GetBoolean(nIndex7);

                            lstBEAccesorio.Add(objBEAccesorio);  
                        }
                    }
                }
            }

            return lstBEAccesorio;
        }
        #endregion
    }
}
