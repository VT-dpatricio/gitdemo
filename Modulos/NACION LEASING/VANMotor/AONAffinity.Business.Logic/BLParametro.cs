﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;

//using AONAffinity.Library.DataAccess;
//using AONAffinity.Library.DataAccess.BDMotor;
//using AONAffinity.Library.BusinessEntity.BDMotor; 

namespace AONAffinity.Motor.BusinessLogic
{
    /// <summary>
    /// Clase de loógica del negócio de la tabla parámetro.
    /// </summary>
    public class BLParametro
    {            
        #region NoTransaccional
        /// <summary>
        /// Permite obtener un registro del parametro por código.
        /// </summary>
        /// <param name="pnIdParametro">Código de parámetro.</param>        
        /// <returns>La información de un parámetro.</returns>
        public BEParametro Obtener(Int32 pnIdParametro)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            BEParametro objBEParametro = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAParametro objDAParametro = new DAParametro();
                SqlDataReader sqlDr = objDAParametro.Obtener(pnIdParametro, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idParametro");
                        Int32 nIndex2 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex4 = sqlDr.GetOrdinal("valorNumeroEnt");
                        Int32 nIndex5 = sqlDr.GetOrdinal("valorNumeroDec");
                        Int32 nIndex6 = sqlDr.GetOrdinal("valorCadena");
                        Int32 nIndex7 = sqlDr.GetOrdinal("valorFecha");
                        Int32 nIndex8 = sqlDr.GetOrdinal("valorBool");
                        Int32 nIndex9 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex10 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex11 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex12 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex13 = sqlDr.GetOrdinal("stsRegistro");

                        objBEParametro = new BEParametro();

                        if (sqlDr.Read())
                        {
                            objBEParametro.IdParametro = sqlDr.GetInt32(nIndex1);
                            objBEParametro.Descripcion = sqlDr.GetString(nIndex2);
                            if (sqlDr.IsDBNull(nIndex3)) { objBEParametro.IdProducto = NullTypes.IntegerNull; } else { objBEParametro.IdProducto = sqlDr.GetInt32(nIndex3); }
                            if (sqlDr.IsDBNull(nIndex4)) { objBEParametro.ValorNumeroEnt = NullTypes.IntegerNull; } else { objBEParametro.ValorNumeroEnt = sqlDr.GetInt32(nIndex4); }
                            if (sqlDr.IsDBNull(nIndex5)) { objBEParametro.ValorNumeroDec = NullTypes.DecimalNull; } else { objBEParametro.ValorNumeroDec = sqlDr.GetDecimal(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBEParametro.ValorCadena = NullTypes.CadenaNull; } else { objBEParametro.ValorCadena = sqlDr.GetString(nIndex6); }
                            if (sqlDr.IsDBNull(nIndex7)) { objBEParametro.ValorFecha = NullTypes.FechaNull; } else { objBEParametro.ValorFecha = sqlDr.GetDateTime(nIndex7); }
                            if (sqlDr.IsDBNull(nIndex8)) { objBEParametro.ValorBool = NullTypes.BoolNull; } else { objBEParametro.ValorBool = sqlDr.GetBoolean(nIndex8); }
                            objBEParametro.UsuarioCreacion = sqlDr.GetString(nIndex9);
                            objBEParametro.FechaCreacion = sqlDr.GetDateTime(nIndex10);
                            if (sqlDr.IsDBNull(nIndex11)) { objBEParametro.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEParametro.UsuarioModificacion = sqlDr.GetString(nIndex11); }
                            if (sqlDr.IsDBNull(nIndex12)) { objBEParametro.FechaModificacion = NullTypes.FechaNull; } else { objBEParametro.FechaModificacion = sqlDr.GetDateTime(nIndex12); }
                            objBEParametro.EstadoRegistro = sqlDr.GetBoolean(nIndex13);
                        }

                        sqlDr.Close();
                    }
                }

            }
            return objBEParametro;
        }

        /// <summary>
        /// Permite obtener el máximo porcenteje de cambio del valor del vehículo.
        /// </summary>
        /// <returns>Lista de porcentaje de cambio.</returns>
        public List<BEParametro> ObtenerPorcMaxCambio() 
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            List<BEParametro> lstBEParametro = null;

            Int32 nCodParMax = 11;            
            BEParametro objBEParametro_Max = this.Obtener(nCodParMax);
            
            if (objBEParametro_Max == null) 
            {
                throw new Exception("No está configurado los porcentajes de cambio.");
            }

            if (!objBEParametro_Max.EstadoRegistro) 
            {
                return lstBEParametro;
            }

            lstBEParametro = new List<BEParametro>();

            Decimal nContaMax = 1;
            while (nContaMax <= objBEParametro_Max.ValorNumeroDec) 
            {
                BEParametro objBEParametro = new BEParametro();
                objBEParametro.ValorNumeroDec = Decimal.Round(nContaMax, 2);

                lstBEParametro.Add(objBEParametro);

                nContaMax++;
            }

            return lstBEParametro; 
        }

        /// <summary>
        /// Permite obtener el mínimo porcenteje de cambio del valor del vehículo.
        /// </summary>
        /// <returns>Lista de porcentaje de cambio.</returns>
        public List<BEParametro> ObtenerPorcMinCambio() 
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            List<BEParametro> lstBEParametro = null;

            Int32 nCodParMin = 12;
            BEParametro objBEParametro_Min = this.Obtener(nCodParMin);

            if (objBEParametro_Min == null)
            {
                throw new Exception("No está configurado los porcentajes de cambio.");
            }

            if (!objBEParametro_Min.EstadoRegistro)
            {
                return lstBEParametro;
            }

            lstBEParametro = new List<BEParametro>();

            Decimal nContaMax = -1;
            while (nContaMax >= objBEParametro_Min.ValorNumeroDec)
            {
                BEParametro objBEParametro = new BEParametro();
                objBEParametro.ValorNumeroDec = Decimal.Round((nContaMax * -1), 2);

                lstBEParametro.Add(objBEParametro);

                nContaMax--;
            }

            return lstBEParametro; 
        }
        #endregion

        #region Transaccional
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pnIdParametro"></param>
        /// <param name="pnValorNumeroEnt"></param>
        /// <returns></returns>
        public Int32 ActualizarValNumEnt(Int32 pnIdParametro, Int32 pnValorNumeroEnt) 
        {
            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAParametro objDAParametro = new DAParametro();
                nResult = objDAParametro.ActualizarValNumEnt(pnIdParametro, pnValorNumeroEnt, sqlCn);
            }

            return nResult;
        }
        #endregion
    }
}
