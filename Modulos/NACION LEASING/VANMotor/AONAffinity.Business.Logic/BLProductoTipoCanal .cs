﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;
using AONAffinity.Library.Resources;

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLProductoTipoCanal
    {
        public BEProductoTipoCanal Obtener(Int32 pnIdProducto, Int32 pnIdTipoCanal)
        {
            BEProductoTipoCanal objBEProductoTipoCanal = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAProductoTipoCanal objDAProductoTipoCanal = new DAProductoTipoCanal();
                SqlDataReader sqlDr = objDAProductoTipoCanal.Obtener(pnIdProducto, pnIdTipoCanal, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idTipoCanal");
                        Int32 nIndex3 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex4 = sqlDr.GetOrdinal("codExterno");

                        objBEProductoTipoCanal = new BEProductoTipoCanal();

                        if (sqlDr.Read()) 
                        {
                            objBEProductoTipoCanal.IdProducto = sqlDr.GetInt32(nIndex1);
                            objBEProductoTipoCanal.IdTipoCanal = sqlDr.GetInt32(nIndex2);
                            objBEProductoTipoCanal.Descripcion = sqlDr.GetString(nIndex3);
                            if (sqlDr.IsDBNull(nIndex4)) { objBEProductoTipoCanal.CodExterno = NullTypes.CadenaNull; } else { objBEProductoTipoCanal.CodExterno = sqlDr.GetString(nIndex4); }
                        }

                        sqlDr.Close(); 
                    }
                }                
            }

            return objBEProductoTipoCanal;
        }
    }
}
