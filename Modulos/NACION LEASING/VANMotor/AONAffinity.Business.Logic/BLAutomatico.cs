﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using AONAffinity.Library.Resources;
using AONAffinity.Library.BusinessEntity.BDMotor; 
using AONAffinity.Motor.Resources;

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLAutomatico
    {
        #region Método Principal
        /// <summary>
        /// Ejecutar proceso automatico
        /// </summary>
        public void Ejecutar()
        {
            //Cargar Clientes
            this.CargarCliente(Convert.ToInt32(ValorConstante.TipoProceso.CLIENTE_IBK));    //AppSettings.TipoProcesoClienteIBK);
            this.CargarCliente(Convert.ToInt32(ValorConstante.TipoProceso.CLIENTE_BBVA));   //AppSettings.TipoProcesoClienteBBVA);

            //Cargar Vigencia y Siniestro
            this.CargarVigenciaSiniestro(Convert.ToInt32(ValorConstante.TipoProceso.CLIENTE_SINIESTRO)); 

            //Generar Cotizaciones Automaticas
            //this.GenerarCotizacion();

            //EjecutarTareaVentas                      
        }

        #endregion

        #region Métodos privados
        /// <summary>
        /// Cargar información cliente.
        /// </summary>
        /// <param name="pnTipoProceso">Código de tipo de proceso</param>
        private void CargarCliente(Int32 pnTipoProceso)
        {
            BLProceso objBLProceso = new BLProceso();
            List<BEProceso> lstBEProceso = objBLProceso.ListarDisponible(pnTipoProceso);

            if (lstBEProceso != null)
            {
                String cArchivo;

                foreach (BEProceso objBEProceso in lstBEProceso)
                {
                    cArchivo = AppSettings.DirectorioTramaCli + objBEProceso.Archivo;

                    BLTrama objBLTramaCliente = new BLTrama();
                    BEProceso objBEProceso_ = null; //objBLTramaCliente.Procesar(objBEProceso);

                    //Actualizar proceso
                    objBEProceso.Procesado = true;
                    objBEProceso.UsuarioModificacion = "appautomatico";
                    objBEProceso.ArchivoLog = "Log_" + objBEProceso.Archivo;
                    objBEProceso.RegTotal = objBEProceso_.RegTotal;
                    objBEProceso.RegTotalVal = objBEProceso_.RegTotalVal;
                    objBEProceso.RegTotalErr = objBEProceso_.RegTotalErr;

                    objBLProceso.ActualizarLog(objBEProceso);
                }
            }
        }

        private void CargarVigenciaSiniestro(Int32 pnTipoProceso) 
        {
            BLProceso objBLProceso = new BLProceso();
            List<BEProceso> lstBEProceso = objBLProceso.ListarDisponible(pnTipoProceso);

            if (lstBEProceso != null)
            {
                String cArchivo;

                foreach (BEProceso objBEProceso in lstBEProceso) 
                {
                    cArchivo = AppSettings.DirectorioTramaCli + objBEProceso.Archivo;

                    BLTrama objBLTramaCliente = new BLTrama();
                    BEProceso objBEProceso_ = null; // objBLTramaCliente.Procesar(objBEProceso);

                    //Actualizar proceso
                    objBEProceso.Procesado = true;
                    objBEProceso.UsuarioModificacion = "appautomatico";
                    objBEProceso.ArchivoLog = "Log_" + objBEProceso.Archivo;
                    objBEProceso.RegTotal = objBEProceso_.RegTotal;
                    objBEProceso.RegTotalVal = objBEProceso_.RegTotalVal;
                    objBEProceso.RegTotalErr = objBEProceso_.RegTotalErr;

                    objBLProceso.ActualizarLog(objBEProceso);
                }
            }
        }

        //private void GenerarCotizacion() 
        //{
        //    BLCotizacion objBLCotizacion = new BLCotizacion();
        //    objBLCotizacion.Generar("");
        //}
        #endregion               
    }
}
