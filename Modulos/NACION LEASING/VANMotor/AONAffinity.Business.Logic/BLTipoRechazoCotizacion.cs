﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLTipoRechazoCotizacion
    {
        #region NoTransaccional
        public List<BETipoRechazoCotizacion> Listar(Nullable<Boolean> pbStsRegistro)
        {
            List<BETipoRechazoCotizacion> lstBETipoRechazoCotizacion = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DATipoRechazoCotizacion objDATipoRechazoCotizacion = new DATipoRechazoCotizacion();
                SqlDataReader sqlDr = objDATipoRechazoCotizacion.Listar(pbStsRegistro, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idTipRechazo");
                        Int32 nIndex2 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex4 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex5 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex6 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("stsRegistro");

                        lstBETipoRechazoCotizacion = new List<BETipoRechazoCotizacion>();

                        while (sqlDr.Read())
                        {
                            BETipoRechazoCotizacion objBETipoRechazoCotizacion = new BETipoRechazoCotizacion();

                            objBETipoRechazoCotizacion.IdTipRechazo = sqlDr.GetInt32(nIndex1);
                            objBETipoRechazoCotizacion.Descripcion = sqlDr.GetString(nIndex2);
                            objBETipoRechazoCotizacion.UsuarioCreacion = sqlDr.GetString(nIndex3);
                            objBETipoRechazoCotizacion.FechaCreacion = sqlDr.GetDateTime(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBETipoRechazoCotizacion.UsuarioModificacion = NullTypes.CadenaNull; } else { objBETipoRechazoCotizacion.UsuarioModificacion = sqlDr.GetString(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBETipoRechazoCotizacion.FechaModificacion = NullTypes.FechaNull; } else { objBETipoRechazoCotizacion.FechaModificacion = sqlDr.GetDateTime(nIndex6); }
                            objBETipoRechazoCotizacion.EstadoRegistro = sqlDr.GetBoolean(nIndex7);

                            lstBETipoRechazoCotizacion.Add(objBETipoRechazoCotizacion);
                        }

                        sqlDr.Close();  
                    }
                }
            }

            return lstBETipoRechazoCotizacion;
        }
        #endregion        
    }
}
