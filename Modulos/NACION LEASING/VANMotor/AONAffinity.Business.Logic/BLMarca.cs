﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.BusinessLogic
{
    /// <summary>
    /// Clase de lógica del negocio de la tabla Marca.
    /// </summary>
    public class BLMarca
    {
        #region Transaccional
        public Int32 Insertar(BEMarca pObjBEMarca)
        {           
            Int32 nResult = 0;
            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();
                DAMarca objDAMarca = new DAMarca();
                nResult = objDAMarca.Insertar(pObjBEMarca, sqlCn);
            }
            return nResult;
        }

        public Int32 Actualizar(BEMarca pObjBEMarca)
        {
            Int32 nResult = 0;
            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();
                DAMarca objDAMarca = new DAMarca();
                nResult = objDAMarca.Actualizar(pObjBEMarca, sqlCn);
            }
            return nResult;
        }


        #endregion

        #region NoTransaccional
        /// <summary>
        /// Permite listar las marcas de automóviles por estado.
        /// </summary>
        /// <param name="pbStsRegistro">Estado del registro.</param>
        /// <returns>Objeto de List de tipo BEMarca.</returns>
        public List<BEMarca> Listar(Nullable<Boolean> pbStsRegistro) 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-24
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            List<BEMarca> lstBEMarca = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAMarca objDAMarca = new DAMarca();
                SqlDataReader sqlDr = objDAMarca.Listar(pbStsRegistro, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idMarca");
                        Int32 nIndex2 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex4 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex5 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex6 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("stsRegistro");

                        lstBEMarca = new List<BEMarca>();

                        while (sqlDr.Read()) 
                        {
                            BEMarca objBEMarca = new BEMarca();

                            objBEMarca.IdMarca = sqlDr.GetInt32(nIndex1);   
                            objBEMarca.Descripcion = sqlDr.GetString(nIndex2);
                            objBEMarca.UsuarioCreacion = sqlDr.GetString(nIndex3);
                            objBEMarca.FechaCreacion = sqlDr.GetDateTime(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBEMarca.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEMarca.UsuarioModificacion = sqlDr.GetString(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBEMarca.FechaModificacion = NullTypes.FechaNull; } else { objBEMarca.FechaModificacion = sqlDr.GetDateTime(nIndex6); }
                            objBEMarca.EstadoRegistro = sqlDr.GetBoolean(nIndex7);

                            lstBEMarca.Add(objBEMarca);   
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return lstBEMarca;
        }

        public List<BEMarca> Buscar(String pcDescripcion,Nullable<Boolean> pbStsRegistro)
        {
           List<BEMarca> lstBEMarca = null;
            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAMarca objDAMarca = new DAMarca();
                SqlDataReader sqlDr = objDAMarca.Buscar(pcDescripcion,pbStsRegistro, sqlCn);
                lstBEMarca = new List<BEMarca>();
                BEMarca objBEMarca = new BEMarca();
                objBEMarca.EstadoRegistro = true;
                lstBEMarca.Add(objBEMarca);
                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idMarca");
                        Int32 nIndex2 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex4 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex5 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex6 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("stsRegistro");
                        Int32 nIndex8 = sqlDr.GetOrdinal("codExterno");

                       
                        while (sqlDr.Read())
                        {
                            objBEMarca = new BEMarca();

                            objBEMarca.IdMarca = sqlDr.GetInt32(nIndex1);
                            objBEMarca.Descripcion = sqlDr.GetString(nIndex2);
                            objBEMarca.UsuarioCreacion = sqlDr.GetString(nIndex3);
                            objBEMarca.FechaCreacion = sqlDr.GetDateTime(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBEMarca.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEMarca.UsuarioModificacion = sqlDr.GetString(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBEMarca.FechaModificacion = NullTypes.FechaNull; } else { objBEMarca.FechaModificacion = sqlDr.GetDateTime(nIndex6); }
                            if (sqlDr.IsDBNull(nIndex8)) { objBEMarca.CodExterno = NullTypes.CadenaNull; } else { objBEMarca.CodExterno = sqlDr.GetString(nIndex8); }
                            objBEMarca.EstadoRegistro = sqlDr.GetBoolean(nIndex7);

                            lstBEMarca.Add(objBEMarca);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBEMarca;
        }        
        
        #endregion
    }
}
