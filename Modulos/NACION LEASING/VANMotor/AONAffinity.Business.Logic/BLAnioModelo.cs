﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Resources;
using AONAffinity.Business.Entity.BDMotor;
using AONAffinity.Data.DataAccess;
using AONAffinity.Data.DataAccess.BDMotor;   

namespace AONAffinity.Business.Logic
{
    public class BLAnioModelo
    {
        public BEAnioModelo Obtener(Int32 pnIdModelo, Int32 pnIdAnio) 
        {
            BEAnioModelo objBEAnioModelo = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAAnioModelo objDAAnioModelo = new DAAnioModelo();
                SqlDataReader sqlDr = objDAAnioModelo.Obtener(pnIdModelo, pnIdAnio, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idModelo");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idAnio");
			            Int32 nIndex3 = sqlDr.GetOrdinal("valorVehiculo");
                        Int32 nIndex4 = sqlDr.GetOrdinal("fecVigencia");
                        Int32 nIndex5 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex6 = sqlDr.GetOrdinal("fecCreacion");
			            Int32 nIndex7 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex8 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex9 = sqlDr.GetOrdinal("stsRegistro");

                        objBEAnioModelo = new BEAnioModelo();

                        if (sqlDr.Read()) 
                        {
                            objBEAnioModelo.IdModelo = sqlDr.GetInt32(nIndex1);
                            objBEAnioModelo.IdAnio = sqlDr.GetInt32(nIndex2);
                            objBEAnioModelo.ValorVehiculo = sqlDr.GetDecimal(nIndex3);
                            if (sqlDr.IsDBNull(nIndex4)) { objBEAnioModelo.FecVigencia = NullTypes.FechaNull; } else { objBEAnioModelo.FecVigencia = sqlDr.GetDateTime(nIndex4); }                             
                        }

                        sqlDr.Close();  
                    }
                }

            }

            return objBEAnioModelo;
        }

    }
}
