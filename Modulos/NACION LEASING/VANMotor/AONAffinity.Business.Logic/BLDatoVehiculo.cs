﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Resources; 
using AONAffinity.Business.Entity.BDMotor;
using AONAffinity.Data.DataAccess;
using AONAffinity.Data.DataAccess.BDMotor;      

namespace AONAffinity.Business.Logic
{
    /// <summary>
    /// Clase de lógica del negocio de la tabla DatoVehiculo.
    /// </summary>
    public class BLDatoVehiculo
    {
        #region Transaccional
        /// <summary>
        /// Permite insertar el dato del vehículo.
        /// </summary>
        /// <param name="pObjBEDatoVehiculo">Objeto que contiene la información del vehículo.</param>
        /// <returns>La cantidad de registros insertados.</returns>
        public Int32 Insertar(BEDatoVehiculo pObjBEDatoVehiculo) 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-02
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DADatoVehiculo objDADatoVehiculo = new DADatoVehiculo();
                nResult = objDADatoVehiculo.Insertar(pObjBEDatoVehiculo, sqlCn);   
            }

            return nResult;
        }

        /// <summary>
        /// Permite actualizar el dato del vehículo.
        /// </summary>
        /// <param name="pObjBEDatoVehiculo">Objeto que contiene la información del vehículo.</param>
        /// <returns>La cantidad de registros actualizados.</returns>
        public Int32 Actualizar(BEDatoVehiculo pObjBEDatoVehiculo) 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DADatoVehiculo objDADatoVehiculo = new DADatoVehiculo();
                nResult = objDADatoVehiculo.Actualizar(pObjBEDatoVehiculo, sqlCn);   
            }

            return nResult;
        }
        #endregion

        #region NoTransaccional
        /// <summary>
        /// Permite obtener un registro del dato del vehículo.
        /// </summary>
        /// <param name="pnIdCotizacion">Código de cotización.</param>
        /// <returns>Objeto de tipo BEDatoVehiculo.</returns>
        public BEDatoVehiculo Obtener(Decimal pnIdCotizacion) 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-15
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BEDatoVehiculo objBEDatoVehiculo = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DADatoVehiculo objDADatoVehiculo = new DADatoVehiculo();
                SqlDataReader sqlDr = objDADatoVehiculo.Obtener(pnIdCotizacion, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idCotizacion");
                        Int32 nIndex2 = sqlDr.GetOrdinal("nroPlaca");
                        Int32 nIndex3 = sqlDr.GetOrdinal("nroMotor");
                        Int32 nIndex4 = sqlDr.GetOrdinal("nroChasis");
                        Int32 nIndex5 = sqlDr.GetOrdinal("idCarroceria");
                        Int32 nIndex6 = sqlDr.GetOrdinal("idCombustible");
                        Int32 nIndex7 = sqlDr.GetOrdinal("idMercancia");                        
                        Int32 nIndex8 = sqlDr.GetOrdinal("idColorOpcion");
                        Int32 nIndex9 = sqlDr.GetOrdinal("nroPasajero");
                        Int32 nIndex10 = sqlDr.GetOrdinal("observacion");
                        Int32 nIndex11 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex12 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex13 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex14 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex15 = sqlDr.GetOrdinal("stsRegistro");
                        Int32 nIndex16 = sqlDr.GetOrdinal("desColorOpcion");

                        if(sqlDr.Read()) 
                        {
                            objBEDatoVehiculo = new BEDatoVehiculo();

                            objBEDatoVehiculo.IdCotizacion = sqlDr.GetDecimal(nIndex1);
                            objBEDatoVehiculo.NroPlaca = sqlDr.GetString(nIndex2);
                            if (sqlDr.IsDBNull(nIndex3)) { objBEDatoVehiculo.NroMotor = NullTypes.CadenaNull; } else { objBEDatoVehiculo.NroMotor = sqlDr.GetString(nIndex3); }
                            if (sqlDr.IsDBNull(nIndex4)) { objBEDatoVehiculo.NroChasis = NullTypes.CadenaNull; } else { objBEDatoVehiculo.NroChasis = sqlDr.GetString(nIndex4); }
                            objBEDatoVehiculo.IdCarroceria = sqlDr.GetInt32(nIndex5);
                            objBEDatoVehiculo.IdCombustible = sqlDr.GetInt32(nIndex6);
                            objBEDatoVehiculo.IdMercancia = sqlDr.GetInt32(nIndex7);
                            objBEDatoVehiculo.IdColorOpcion = sqlDr.GetInt32(nIndex8);
                            objBEDatoVehiculo.NroPasajero = sqlDr.GetInt32(nIndex9);
                            if (sqlDr.IsDBNull(nIndex10)) { objBEDatoVehiculo.Observacion = NullTypes.CadenaNull; } else { objBEDatoVehiculo.Observacion = sqlDr.GetString(nIndex10); } 
                            objBEDatoVehiculo.UsuarioCreacion = sqlDr.GetString(nIndex11);
                            objBEDatoVehiculo.FechaCreacion = sqlDr.GetDateTime(nIndex12);
                            if (sqlDr.IsDBNull(nIndex13)) { objBEDatoVehiculo.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEDatoVehiculo.UsuarioModificacion = sqlDr.GetString(nIndex13); }
                            if (sqlDr.IsDBNull(nIndex14)) { objBEDatoVehiculo.FechaModificacion = NullTypes.FechaNull; } else { objBEDatoVehiculo.FechaModificacion = sqlDr.GetDateTime(nIndex14); }
                            objBEDatoVehiculo.EstadoRegistro = sqlDr.GetBoolean(nIndex15);
                            objBEDatoVehiculo.DesColorOpcion = sqlDr.GetString(nIndex16);  
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return objBEDatoVehiculo;
        }
        #endregion
    }
}
