﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.BusinessLogic.Cotizacion
{
    public class BLCotizacion5251 : BLCotizacionRIMAC 
    {
        private Int32 nIdProducto = 5251;

        public void CotizarClienteBBVA_5251(List<BECliente> pLstBECliente, String pcUsuario, ref Int32 pnConta) 
        {
            pnConta = 0;

            if (pLstBECliente != null) 
            {
                BLProducto objBLProducto = new BLProducto();
                BEProducto objBEProducto = objBLProducto.Obtener(nIdProducto);  

                foreach (BECliente objBECliente in pLstBECliente) 
                {
                    Int32 nAntiguedad = this.ObtenerAntiguedad(objBECliente.AnioFab);
                    Decimal nValorVehiculo = this.ObtenerValorDepresiado(objBECliente.AnioFab, objBECliente.ValorVehiculo, nIdProducto);
                    BLTasa objBLTasa = new BLTasa();
                    BETasa objBETasa_Anual = objBLTasa.Obtener(objBECliente.IdModelo, TipoTasa.Anual, objBECliente.AnioFab);
                    BETasa objBETasa_Bianual = objBLTasa.Obtener(objBECliente.IdModelo,TipoTasa.Bianual, objBECliente.AnioFab);

                    Int32 nIdCategoria = objBETasa_Anual.IdCategoria;

                    BLCliente objBLCliente = new BLCliente();

                    //X10 debe estar en carga cliente.
                    if (objBETasa_Anual == null && objBETasa_Bianual == null)
                    {
                        //Actualizar estado cliente que no existe valor de tasa.
                        Int32 nresult = objBLCliente.Atualizar(objBECliente.NroMotor, Convert.ToInt32(ValorConstante.EstadoCliente.NoExisteValorTasa), pcUsuario);
                        return;
                    }

                    //Si producto aplica sniestro.
                    if (objBEProducto.Siniestro)
                    {
                        Boolean bValido = true;

                        BLVigenciaSiniestro objBLVigenciaSiniestro = new BLVigenciaSiniestro();
                        BEVigenciaSiniestro objBEVigenciaSiniestro = objBLVigenciaSiniestro.Obtener(objBECliente.NroMotor);
                        
                        if (objBEVigenciaSiniestro == null)
                        {
                            Int32 nresult = objBLCliente.Atualizar(objBECliente.NroMotor, Convert.ToInt32(ValorConstante.EstadoCliente.NO_EXISTE_SINIESTROREG), pcUsuario);
                            bValido = false;
                        }

                        if (bValido == true) 
                        {
                            //Si cantidad de siniestro del ultimo año es mayor al maximo permitido y monto es mayor igual a al monto siniestro.
                            if (objBEVigenciaSiniestro.CantSiniestro1Anio > objBEProducto.MaxCantSiniestro && objBEVigenciaSiniestro.MontoSiniestro1Anio >= objBEProducto.MontoSiniestro)
                            {
                                Int32 nResult = objBLCliente.Atualizar(objBECliente.NroMotor, Convert.ToInt32(ValorConstante.EstadoCliente.NO_RENO_MAXCANTSINIESTRO), pcUsuario);
                                bValido = false;
                            }
                        }

                        //Obtener scoring del cliente.
                        if (bValido == true) 
                        {
                            BECotizacion objBECotizacion_Scoring = this.GenerarScoringRIMAC(this.nIdProducto, objBECliente.NroMotor, objBEProducto.MontoSiniestro);
                            BECotizacion objBECotizacion = this.CargarDatosCotizacion(this.nIdProducto, objBECliente, objBEVigenciaSiniestro, objBECotizacion_Scoring, objBETasa_Anual, objBETasa_Bianual, pcUsuario);

                            if (this.InsertarRIMAC(objBECotizacion) > 0) 
                            {                                
                                if (objBLCliente.Atualizar(objBECliente.NroMotor, EstadoCliente.Cotizado, pcUsuario) > 0)
                                {
                                    pnConta++;
                                }                                
                            }
                        }
                    }
                    else //Si producto no aplica siniestro.
                    {
                        BECotizacion objBECotizacion = this.CargarDatosCotizacion(this.nIdProducto, objBECliente, objBETasa_Anual, objBETasa_Bianual, pcUsuario);

                        if (this.InsertarRIMAC(objBECotizacion) > 0)
                        {
                            if (objBLCliente.Atualizar(objBECliente.NroMotor, EstadoCliente.Cotizado, pcUsuario) > 0)
                            {
                                pnConta++;
                            }
                        }
                    }
                }
            }
        }
    }
}
