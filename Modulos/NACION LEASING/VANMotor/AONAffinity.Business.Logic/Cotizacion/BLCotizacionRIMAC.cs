﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;  
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.BusinessLogic.Cotizacion
{
    public class BLCotizacionRIMAC
    {
        /// <summary>
        /// Permite obtener la antigüedad del vehículo según el año de fabricación.
        /// </summary>
        /// <param name="nAnioFab">Año de fabricación.</param>
        /// <returns>Antigüedad del vehículo.</returns>
        public Int32 ObtenerAntiguedad(Int32 nAnioFab)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Int32 nAntiguedad = DateTime.Now.Year - nAnioFab;
            return nAntiguedad;
        }

        public Decimal ObtenerValorDepresiado(Int32 pnAnioFab, Decimal pnValorVehiculo, Int32 pnIdProducto)
        {
            Decimal nResult = NullTypes.DecimalNull;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DACotizacion objDACotizacion = new DACotizacion();
                SqlDataReader sqlDr = objDACotizacion.ObtenerValorDepresiado(pnAnioFab, pnValorVehiculo, pnIdProducto, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex = sqlDr.GetOrdinal("valordepresiado");

                        if (sqlDr.Read())
                        {
                            if (sqlDr.IsDBNull(nIndex)) { nResult = NullTypes.DecimalNull; } else { nResult = sqlDr.GetDecimal(nIndex); }
                        }

                        sqlDr.Close();
                    }
                }
            }
            return nResult;
        }

        public BECotizacion GenerarScoringRIMAC(Int32 pnIdProducto, String pcNroMotor, Decimal pnMontoSiniestro) 
        {
            BECotizacion objBECotizacion = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DACotizacion objDACotizacion = new DACotizacion();
                SqlDataReader sqlDr = objDACotizacion.GenerarScoringRIMAC(pnIdProducto, pcNroMotor, pnMontoSiniestro, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("nroMotor");
                        Int32 nIndex2 = sqlDr.GetOrdinal("cantVigencias");
                        Int32 nIndex3 = sqlDr.GetOrdinal("condicion");
                        Int32 nIndex4 = sqlDr.GetOrdinal("porcentaje");
                        
                        if (sqlDr.Read()) 
                        {
                            objBECotizacion = new BECotizacion();
                            objBECotizacion.NroMotor = sqlDr.GetString(nIndex1);
                            objBECotizacion.CantVigencias = sqlDr.GetInt32(nIndex2);
                            objBECotizacion.Condicion = sqlDr.GetInt32(nIndex3);
                            if (sqlDr.IsDBNull(nIndex4)) { objBECotizacion.PorcentajeC = NullTypes.DecimalNull; } else { objBECotizacion.PorcentajeC = sqlDr.GetDecimal(nIndex4); } 
                        }

                        sqlDr.Close(); 
                    }
                }                
            }

            return objBECotizacion;
        }

        public BECotizacion CargarDatosCotizacion(Int32 pnIdProducto, BECliente pObjBECliente, BEVigenciaSiniestro pObjBEVigenciaSiniestro, BECotizacion pObjBECotizacionScoring, BETasa pObjBETasaAnual, BETasa pObjBETasaBianual, String pcUsuario)
        {
            BECotizacion objBECotizacion = new BECotizacion();                        
            objBECotizacion.TasaAnual = pObjBETasaAnual.ValorTasa;            
            objBECotizacion.TasaBianual = pObjBETasaBianual.ValorTasa;
            objBECotizacion.Condicion = pObjBECotizacionScoring.Condicion;
            objBECotizacion.PorcentajeC = pObjBECotizacionScoring.PorcentajeC;            
            objBECotizacion.IdCategoria = this.ObtenerCategoria(pObjBETasaAnual, pObjBETasaBianual);
            objBECotizacion.UsuarioCreacion = pcUsuario;
            objBECotizacion.ValVehiculo = this.ObtenerValorDepresiado(pObjBECliente.AnioFab, pObjBECliente.ValorVehiculo, pnIdProducto);
            objBECotizacion.FecIniVigPoliza = pObjBEVigenciaSiniestro.FecIniVigencia;
            objBECotizacion.FecFinVigPoliza = pObjBEVigenciaSiniestro.FecFinVigencia;
            objBECotizacion.IdProducto = pnIdProducto;
            objBECotizacion.ApePaterno = pObjBECliente.ApePaterno;
            objBECotizacion.ApeMaterno = pObjBECliente.ApeMaterno;
            objBECotizacion.PriNombre = pObjBECliente.PriNombre;
            objBECotizacion.SegNombre = pObjBECliente.SegNombre;
            objBECotizacion.IdTipoDocumento = pObjBECliente.IdTipoDocumento;
            objBECotizacion.NroDocumento = pObjBECliente.NroDocumento;
            objBECotizacion.Sexo = pObjBECliente.Sexo;
            objBECotizacion.FecNacimiento = pObjBECliente.FecNacimiento;
            objBECotizacion.IdCiudad = pObjBECliente.idCiudad;
            objBECotizacion.Direccion = pObjBECliente.Direccion;
            objBECotizacion.TelDomicilio1 = pObjBECliente.TelDomicilio1;
            objBECotizacion.TelDomicilio2 = pObjBECliente.TelDomicilio2;
            objBECotizacion.TelDomicilio3 = pObjBECliente.TelDomicilio3;
            objBECotizacion.TelMovil1 = pObjBECliente.TelMovil1;
            objBECotizacion.TelMovil2 = pObjBECliente.TelMovil2;
            objBECotizacion.TelMovil3 = pObjBECliente.TelMovil2;
            objBECotizacion.TelOficina1 = pObjBECliente.TelOficina1;
            objBECotizacion.TelOficina2 = pObjBECliente.TelOficina2;
            objBECotizacion.TelOficina3 = pObjBECliente.TelOficina3;
            objBECotizacion.Email1 = pObjBECliente.Email1;
            objBECotizacion.Email2 = pObjBECliente.Email2;
            objBECotizacion.IdMarca = pObjBECliente.IdMarca;
            objBECotizacion.IdModelo = pObjBECliente.IdModelo;
            objBECotizacion.IdClase = pObjBECliente.IdClase;
            objBECotizacion.NroPlaca = pObjBECliente.NroPlaca;
            objBECotizacion.AnioFab = pObjBECliente.AnioFab;
            objBECotizacion.NroMotor = pObjBECliente.NroMotor;
            objBECotizacion.NroSerie = pObjBECliente.NroSerie;
            objBECotizacion.Color = pObjBECliente.Color;
            objBECotizacion.IdUsoVehiculo = pObjBECliente.IdUsoVehiculo;
            objBECotizacion.NroAsientos = pObjBECliente.NroAsientos;
            objBECotizacion.EsTimonCambiado = pObjBECliente.EsTimonCambiado;
            objBECotizacion.ReqGPS = pObjBECliente.ReqGPS;
            objBECotizacion.ValOriVehiculo = objBECotizacion.ValVehiculo;
            objBECotizacion.IdEstado = EstadoCotizacion.Creada;
            objBECotizacion.ReqInspeccion = this.RequiereInspeccion(pObjBECliente.CodAsegurador);
            objBECotizacion.DirInspeccion = NullTypes.CadenaNull;
            objBECotizacion.FecHorInspeccion = NullTypes.CadenaNull;
            objBECotizacion.FecValidez = NullTypes.FechaNull;

            return objBECotizacion; 
        }

        public BECotizacion CargarDatosCotizacion(Int32 pnIdProducto, BECliente pObjBECliente, BETasa pObjBETasaAnual, BETasa pObjBETasaBianual, String pcUsuario) 
        {
            BECotizacion objBECotizacion = new BECotizacion();
            objBECotizacion.TasaAnual = pObjBETasaAnual.ValorTasa;
            if (pObjBETasaBianual == null) { objBECotizacion.TasaBianual = NullTypes.DecimalNull; } else { objBECotizacion.TasaBianual = pObjBETasaBianual.ValorTasa; }            
            objBECotizacion.Condicion = CondicionCotizacion.Ninguno;
            objBECotizacion.PorcentajeC = NullTypes.DecimalNull; 
            objBECotizacion.IdCategoria = this.ObtenerCategoria(pObjBETasaAnual, pObjBETasaBianual);
            objBECotizacion.UsuarioCreacion = pcUsuario;
            objBECotizacion.ValVehiculo = this.ObtenerValorDepresiado(pObjBECliente.AnioFab, pObjBECliente.ValorVehiculo, pnIdProducto);
            objBECotizacion.FecIniVigPoliza = pObjBECliente.FecVigencia;
            objBECotizacion.FecFinVigPoliza = pObjBECliente.FecFinVigencia;
            objBECotizacion.IdProducto = pnIdProducto;
            objBECotizacion.ApePaterno = pObjBECliente.ApePaterno;
            objBECotizacion.ApeMaterno = pObjBECliente.ApeMaterno;
            objBECotizacion.PriNombre = pObjBECliente.PriNombre;
            objBECotizacion.SegNombre = pObjBECliente.SegNombre;
            objBECotizacion.IdTipoDocumento = pObjBECliente.IdTipoDocumento;
            objBECotizacion.NroDocumento = pObjBECliente.NroDocumento;
            objBECotizacion.Sexo = pObjBECliente.Sexo;
            objBECotizacion.FecNacimiento = pObjBECliente.FecNacimiento;
            objBECotizacion.IdCiudad = pObjBECliente.idCiudad;
            objBECotizacion.Direccion = pObjBECliente.Direccion;
            objBECotizacion.TelDomicilio1 = pObjBECliente.TelDomicilio1;
            objBECotizacion.TelDomicilio2 = pObjBECliente.TelDomicilio2;
            objBECotizacion.TelDomicilio3 = pObjBECliente.TelDomicilio3;
            objBECotizacion.TelMovil1 = pObjBECliente.TelMovil1;
            objBECotizacion.TelMovil2 = pObjBECliente.TelMovil2;
            objBECotizacion.TelMovil3 = pObjBECliente.TelMovil2;
            objBECotizacion.TelOficina1 = pObjBECliente.TelOficina1;
            objBECotizacion.TelOficina2 = pObjBECliente.TelOficina2;
            objBECotizacion.TelOficina3 = pObjBECliente.TelOficina3;
            objBECotizacion.Email1 = pObjBECliente.Email1;
            objBECotizacion.Email2 = pObjBECliente.Email2;
            objBECotizacion.IdMarca = pObjBECliente.IdMarca;
            objBECotizacion.IdModelo = pObjBECliente.IdModelo;
            objBECotizacion.IdClase = pObjBECliente.IdClase;
            objBECotizacion.NroPlaca = pObjBECliente.NroPlaca;
            objBECotizacion.AnioFab = pObjBECliente.AnioFab;
            objBECotizacion.NroMotor = pObjBECliente.NroMotor;
            objBECotizacion.NroSerie = pObjBECliente.NroSerie;
            objBECotizacion.Color = pObjBECliente.Color;
            objBECotizacion.IdUsoVehiculo = pObjBECliente.IdUsoVehiculo;
            objBECotizacion.NroAsientos = pObjBECliente.NroAsientos;
            objBECotizacion.EsTimonCambiado = pObjBECliente.EsTimonCambiado;
            objBECotizacion.ReqGPS = pObjBECliente.ReqGPS;
            objBECotizacion.ValOriVehiculo = objBECotizacion.ValVehiculo;
            objBECotizacion.IdEstado = EstadoCotizacion.Creada;
            objBECotizacion.ReqInspeccion = this.RequiereInspeccion(pObjBECliente.CodAsegurador);
            objBECotizacion.DirInspeccion = NullTypes.CadenaNull;
            objBECotizacion.FecHorInspeccion = NullTypes.CadenaNull;
            objBECotizacion.FecValidez = NullTypes.FechaNull;

            return objBECotizacion;
        }

        public Int32 ObtenerCategoria(BETasa pObjBETasaAnual, BETasa pObjBETasaBianual)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Int32 nResult = 0;

            if (pObjBETasaAnual != null)
            {
                nResult = pObjBETasaAnual.IdCategoria;
            }
            else
            {
                nResult = pObjBETasaBianual.IdCategoria;
            }

            return nResult;
        }

        public Boolean RequiereInspeccion(Int32 pnIdAsegurador)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Boolean bResult = false;

            if (pnIdAsegurador != Convert.ToInt32(ValorConstante.CodigoAsegurador.RIMAC))
            {
                bResult = true;
            }

            return bResult;
        }

        public Int32 InsertarRIMAC(BECotizacion pObjBECotizacion) 
        {
            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DACotizacion objDACotizacion = new DACotizacion();
                nResult = objDACotizacion.InsertarRIMAC(pObjBECotizacion, sqlCn);  
            }

            return nResult;
        }
    }
}
