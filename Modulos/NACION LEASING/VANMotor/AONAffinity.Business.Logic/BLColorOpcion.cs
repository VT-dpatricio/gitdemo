﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Resources;
using AONAffinity.Business.Entity.BDMotor;
using AONAffinity.Data.DataAccess;
using AONAffinity.Data.DataAccess.BDMotor;

namespace AONAffinity.Business.Logic
{
    /// <summary>
    /// Clase de lógica del negocio de la tabla ColorOpcion.
    /// </summary>
    public class BLColorOpcion
    {
        /// <summary>
        /// Permite listar las opciones de color por el código de color.
        /// </summary>
        /// <param name="pnIdColor">Código de color.</param>
        /// <param name="pbStsRegistro">Estado del registro.</param>
        /// <returns>Objeto List de tipo BEColorOpcion.</returns>
        public List<BEColorOpcion> Listar(Int32 pnIdColor, Nullable<Boolean> pbStsRegistro) 
        {
            /*
            * CREADO POR:              Gary Porras Paraguay
            * FECHA DE CREACION:       2011-06-01
            * MODIFICADO POR:          
            * FECHA DE MODIFICACION:   
            */

            List<BEColorOpcion> lstBEColorOpcion  = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAColorOpcion objDAColorOpcion = new DAColorOpcion();
                SqlDataReader sqlDr = objDAColorOpcion.Listar(pnIdColor, pbStsRegistro, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idColorOpcion");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idColor");
                        Int32 nIndex3 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex4 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex5 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex6 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex8 = sqlDr.GetOrdinal("stsRegistro");

                        lstBEColorOpcion = new List<BEColorOpcion>();

                        while (sqlDr.Read()) 
                        {
                            BEColorOpcion objBEColorOpcion = new BEColorOpcion();

                            objBEColorOpcion.IdColorOpcion = sqlDr.GetInt32(nIndex1);
                            objBEColorOpcion.IdColor = sqlDr.GetInt32(nIndex2);
                            objBEColorOpcion.Descripcion = sqlDr.GetString(nIndex3);
                            objBEColorOpcion.UsuarioCreacion = sqlDr.GetString(nIndex4);
                            objBEColorOpcion.FechaCreacion = sqlDr.GetDateTime(nIndex5);
                            if (sqlDr.IsDBNull(nIndex6)) { objBEColorOpcion.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEColorOpcion.UsuarioModificacion = sqlDr.GetString(nIndex6); }
                            if (sqlDr.IsDBNull(nIndex7)) { objBEColorOpcion.FechaModificacion = NullTypes.FechaNull; } else { objBEColorOpcion.FechaModificacion = sqlDr.GetDateTime(nIndex7); }
                            objBEColorOpcion.EstadoRegistro = sqlDr.GetBoolean(nIndex8);

                            lstBEColorOpcion.Add(objBEColorOpcion);
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return lstBEColorOpcion;
        }
    }
}
