﻿using System;
using System.Collections.Generic;
using AONAffinity.Motor.DataAccess.DLNT;
using AONAffinity.Motor.DataAccess.DLT;
using AONAffinity.Motor.BusinessEntity;
using System.Collections;
using Microsoft.VisualBasic;

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLSeguridadEvento
    {

        public bool Insertar(BEBase pEntidad)
        {
            DLTSeguridadEvento oDL = new DLTSeguridadEvento();
            return oDL.Insertar(pEntidad);
        }

        public bool PrimerLogin(string pIDUsuario)
        {
            DLNTSeguridadEvento oDL = new DLNTSeguridadEvento();
            BESeguridadEvento oBE = oDL.SeleccionarUltimo(pIDUsuario, 1);
            if ((oBE.IDEvento == 0))
            {
                //Si no existe ningún inicio de sesión
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ClaveCaducada(string pIDUsuario, int pNroDiasCaduca)
        {
            DLNTSeguridadEvento oDL = new DLNTSeguridadEvento();
            BESeguridadEvento oBE = oDL.SeleccionarUltimo(pIDUsuario, 3);
            TimeSpan dias = oBE.FechaRegistro.Subtract(System.DateTime.Now);
            //if ((DateAndTime.DateDiff(("d"), oBE.FechaRegistro, System.DateTime.Now) > pNroDiasCaduca))
            if (dias.Days > pNroDiasCaduca)
            {
                //Si el número de días desde el último cambio de contraseña es mayor a los días en que la contraseña caduca
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ClaveModificadaHoy(string pIDUsuario)
        {
            DLNTSeguridadEvento oDL = new DLNTSeguridadEvento();
            BESeguridadEvento oBE = oDL.SeleccionarUltimo(pIDUsuario, 3);
            bool Retorno = false;
            if ((oBE.IDEvento != 0))
            {
                if (oBE.FechaRegistro.ToString("dd/MM/yyyy") == System.DateTime.Now.ToString("dd/MM/yyyy"))
                {
                    Retorno = true;
                }
            }
            else
            {
                Retorno = false;
            }
            return Retorno;
        }
    }
}
