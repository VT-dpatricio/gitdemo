﻿using System;
using System.Collections.Generic;
using AONAffinity.Motor.DataAccess.DLNT;
using AONAffinity.Motor.DataAccess.DLT;
using AONAffinity.Motor.BusinessEntity;
using System.Collections;

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLSeguridadConfig
    {

        public IList Seleccionar(Int32 pIDEntidad)
        {
            DLNTSeguridadConfig oDL = new DLNTSeguridadConfig();
            return oDL.Seleccionar(pIDEntidad);
        }

        public string Parametro(string pIDParametro, IList pListaP)
        {
            //Dim oBEConfigSeguridad As New BEConfigSeguridad
            string Valor = "";
            foreach (BESeguridadConfig oBEConfigSeguridad in pListaP)
            {
                if ((oBEConfigSeguridad.IDParametro == pIDParametro))
                {
                    Valor = oBEConfigSeguridad.Valor;
                    break; // TODO: might not be correct. Was : Exit For
                }
            }
            return Valor;
        }

    }
}
