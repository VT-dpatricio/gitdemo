﻿//OK
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Business.Entity.BDIntegration;
using AONAffinity.Data.DataAccess.BDIntegration;

namespace AONAffinity.Motor.BusinessLogic.BDIntegration
{
    /// <summary>
    /// Clase de lógica del negocio que referencia a la tabla Estructura de BDIntegration.
    /// </summary>
    public class BLEstructura
    {
        #region NoTransaccional
        /// <summary>
        /// Permite obtener la estructura de un archivo.
        /// </summary>
        /// <param name="pnIdArchivo">Código de archivo.</param>
        /// <param name="pnIdTipo">Código de estructura.</param>
        /// <returns>Objeto de tipo BEEstructura.</returns>
        public BEEstructura Obtener(Int32 pnIdArchivo, Int32 pnIdTipo)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-07-11
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BEEstructura objBEEstructura = null;

            using (SqlConnection sqlCn = new SqlConnection(AONAffinity.Data.DataAccess.DAConexion.ConexionBDAdmTrama()))
            {
                sqlCn.Open();

                DAEstructura objDAEstructura = new DAEstructura();
                SqlDataReader sqlDr = objDAEstructura.ObtenerPorArchivo(pnIdArchivo, pnIdTipo, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idEstructura");
                        Int32 nIndex2 = sqlDr.GetOrdinal("longitud");
                        Int32 nIndex3 = sqlDr.GetOrdinal("idArchivo");
                        Int32 nIndex4 = sqlDr.GetOrdinal("idStoredProcedure");
                        Int32 nIndex5 = sqlDr.GetOrdinal("idTipoEstructura");

                        objBEEstructura = new BEEstructura();

                        if(sqlDr.Read())
                        {                            
                            objBEEstructura.IdEstructura = sqlDr.GetInt32(nIndex1);
                            objBEEstructura.Longitud = sqlDr.GetInt32(nIndex2);
                            objBEEstructura.IdArchivo = sqlDr.GetInt32(nIndex3);
                            objBEEstructura.IdStoredProcedure = sqlDr.GetInt32(nIndex4);
                            objBEEstructura.IdTipoEstructura = sqlDr.GetInt32(nIndex5);                            
                        }

                        sqlDr.Close();
                    }
                }
            }
            return objBEEstructura;
        }
        #endregion           
    }
}
