﻿//OK
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Business.Entity.BDIntegration;
using AONAffinity.Data.DataAccess.BDIntegration;

namespace AONAffinity.Motor.BusinessLogic.BDIntegration
{
    /// <summary>
    /// Clase de lógica del negocio que referencia a la tabla Equivalencia de BDIntegration.
    /// </summary>
    public class BLEquivalencia
    {
        #region NoTransaccional
        /// <summary>
        /// Permite obtener la equivalencia de un campo.
        /// </summary>
        /// <param name="pnIdEstructura">Código de estructura.</param>
        /// <returns>Objeto de tipo DataTable</returns>
        public DataTable Obtener(Int32 pnIdEstructura)
        {
            DataSet dsEquivalencia = null;
            DataTable dtEquivalencia = null;

            using (SqlConnection sqlCn = new SqlConnection(AONAffinity.Data.DataAccess.DAConexion.ConexionBDAdmTrama()))
            {
                sqlCn.Open();

                DAEquivalencia objDAEquivalencia = new DAEquivalencia();
                dsEquivalencia = objDAEquivalencia.ObtenerPorEstructura(pnIdEstructura, sqlCn);
                dtEquivalencia = dsEquivalencia.Tables[0];
            }
            return dtEquivalencia;
        }
        #endregion        
    }
}
