﻿//OK
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Business.Entity.BDIntegration;
using AONAffinity.Data.DataAccess.BDIntegration;

namespace AONAffinity.Motor.BusinessLogic.BDIntegration
{
    /// <summary>
    /// Clase de lógica del negocio que referencia a la tabla Archivo de BDIntegration.
    /// </summary>
    public class BLArchivo
    {
        #region NoTransaccional
        /// <summary>
        /// Permite obtener la información de un archivo.
        /// </summary>
        /// <param name="pnIdProducto">Código de producto.</param>
        /// <param name="pcNombre">Nombre de archivo del producto.</param>
        /// <returns>Objeto de tipo BEArchivo.</returns>
        public BEArchivo Obtener(Int32 pnIdProducto, String pcNombre)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-07-11
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BEArchivo objBEArchivo = null;

            using (SqlConnection sqlCn = new SqlConnection(AONAffinity.Data.DataAccess.DAConexion.ConexionBDAdmTrama()))
            {
                sqlCn.Open();

                DAArchivo objDAArchivo = new DAArchivo();
                SqlDataReader sqlDr = objDAArchivo.ObtenerPorParametros(pnIdProducto, pcNombre, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idArchivo");
                        Int32 nIndex2 = sqlDr.GetOrdinal("Nombre");
                        Int32 nIndex3 = sqlDr.GetOrdinal("formatoNombre");
                        Int32 nIndex4 = sqlDr.GetOrdinal("esActivo");
                        Int32 nIndex5 = sqlDr.GetOrdinal("idProducto");

                        objBEArchivo = new BEArchivo();

                        if (sqlDr.Read())
                        {
                            objBEArchivo.IdArchivo = sqlDr.GetInt32(nIndex1);
                            objBEArchivo.Nombre = sqlDr.GetString(nIndex2);
                            objBEArchivo.FormatoNombre = sqlDr.GetString(nIndex3);
                            /*objBEArchivo.EsActivo = sqlDr.GetInt32(nIndex4);      propiedead debe ser boolean*/
                            objBEArchivo.IdProducto = sqlDr.GetInt32(nIndex5);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return objBEArchivo;
        }
        #endregion        
    }
}
