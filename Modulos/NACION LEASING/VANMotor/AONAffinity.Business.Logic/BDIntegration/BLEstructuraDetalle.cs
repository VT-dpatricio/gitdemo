﻿//OK
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Business.Entity.BDIntegration;
using AONAffinity.Data.DataAccess.BDIntegration;

namespace AONAffinity.Motor.BusinessLogic.BDIntegration
{
    /// <summary>
    /// Clase de lógica del negocio que referencia a la tabla EstructuraDetalle de BDIntegration.
    /// </summary>
    public class BLEstructuraDetalle
    {
        #region NoTransaccional
        /// <summary>
        /// Permite obtener el detalle de la estructura.
        /// </summary>
        /// <param name="pnIdEstructura">Código de estructura.</param>
        /// <returns>Objeto lista de tipo BEEstructuraDetalle.</returns>
        public List<BEEstructuraDetalle> Obtener(Int32 pnIdEstructura)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-07-11
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            List<BEEstructuraDetalle> lstBEEstructuraDetalle = null;

            using (SqlConnection sqlCn = new SqlConnection(AONAffinity.Data.DataAccess.DAConexion.ConexionBDAdmTrama()))
            {
                sqlCn.Open();

                DAEstructuraDetalle objDAEstructuraDetalle = new DAEstructuraDetalle();
                SqlDataReader sqlDr = objDAEstructuraDetalle.ObtenerPorEstructura(pnIdEstructura, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idEstructuraDetalle");
                        Int32 nIndex2 = sqlDr.GetOrdinal("nombre");
                        Int32 nIndex3 = sqlDr.GetOrdinal("posicionInicial");
                        Int32 nIndex4 = sqlDr.GetOrdinal("longitud");
                        Int32 nIndex5 = sqlDr.GetOrdinal("valorMinimo");
                        Int32 nIndex6 = sqlDr.GetOrdinal("valorMaximo");
                        Int32 nIndex7 = sqlDr.GetOrdinal("valoresNoPermitidos");
                        Int32 nIndex8 = sqlDr.GetOrdinal("obligatorio");
                        Int32 nIndex9 = sqlDr.GetOrdinal("equivalencia");
                        Int32 nIndex10 = sqlDr.GetOrdinal("idEstructura");
                        Int32 nIndex11 = sqlDr.GetOrdinal("idTipoDato");
                        Int32 nIndex12 = sqlDr.GetOrdinal("idFormato");
                        Int32 nIndex13 = sqlDr.GetOrdinal("nombreTD");
                        Int32 nIndex14 = sqlDr.GetOrdinal("formato");

                        lstBEEstructuraDetalle = new List<BEEstructuraDetalle>();


                        while (sqlDr.Read())
                        {
                            BEEstructuraDetalle objBEEstructuraDetalle = new BEEstructuraDetalle();

                            objBEEstructuraDetalle.IdEstructuraDetalle = sqlDr.GetInt32(nIndex1);
                            objBEEstructuraDetalle.Nombre = sqlDr.GetString(nIndex2);
                            objBEEstructuraDetalle.PosInicial = sqlDr.GetInt32(nIndex3);
                            objBEEstructuraDetalle.Longitud = sqlDr.GetInt32(nIndex4);
                            objBEEstructuraDetalle.ValMin = sqlDr.GetInt32(nIndex5);
                            objBEEstructuraDetalle.ValMax = sqlDr.GetInt32(nIndex6);
                            objBEEstructuraDetalle.ValoresPermitidos = sqlDr.GetString(nIndex7);
                            objBEEstructuraDetalle.Obligatorio = sqlDr.GetInt32(nIndex8);
                            objBEEstructuraDetalle.Equivalencia = sqlDr.GetInt32(nIndex9);
                            objBEEstructuraDetalle.IdEstructura = sqlDr.GetInt32(nIndex10);
                            objBEEstructuraDetalle.IdTipoDato = sqlDr.GetInt32(nIndex11);
                            objBEEstructuraDetalle.IdFormato = sqlDr.GetInt32(nIndex12);
                            objBEEstructuraDetalle.Nombretd = sqlDr.GetString(nIndex13);
                            objBEEstructuraDetalle.Formato = sqlDr.GetString(nIndex14);

                            lstBEEstructuraDetalle.Add(objBEEstructuraDetalle);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBEEstructuraDetalle;
        }  
        #endregion
        
    }
}
