﻿//OK
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Business.Entity.BDIntegration;   
using AONAffinity.Data.DataAccess.BDIntegration;

namespace AONAffinity.Motor.BusinessLogic.BDIntegration
{
    /// <summary>
    /// Clase de lógica del negocio que referencia a la tabla Producto de BDIntegration.
    /// </summary>
    public class BLProducto
    {
        #region NoTransaccional
        /// <summary>
        /// Permite obtener la información de un Producto.
        /// </summary>
        /// <param name="pnIdProducto">Código de producto.</param>
        /// <returns>Objeto de tipo BEProducto.</returns>
        public BEProducto Obtener(Int32 pnIdProducto)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-07-11
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BEProducto objBEProducto = null;

            using (SqlConnection sqlCn = new SqlConnection(AONAffinity.Data.DataAccess.DAConexion.ConexionBDAdmTrama()))
            {
                sqlCn.Open();

                DAProducto objDAProducto = new DAProducto();
                SqlDataReader sqlDr = objDAProducto.Obtener(pnIdProducto, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex2 = sqlDr.GetOrdinal("nombre");
                        Int32 nIndex3 = sqlDr.GetOrdinal("codProducto");
                        Int32 nIndex4 = sqlDr.GetOrdinal("esActivo");
                        Int32 nIndex5 = sqlDr.GetOrdinal("idAccion");

                        objBEProducto = new BEProducto();

                        if (sqlDr.Read())
                        {
                            objBEProducto.IdProducto = sqlDr.GetInt32(nIndex1);
                            objBEProducto.Nombre = sqlDr.GetString(nIndex2);
                            objBEProducto.CodProducto = sqlDr.GetInt32(nIndex3);
                            /*objBEProducto.EsActivo = sqlDr.GetInt32(nIndex4);     propiedead debe ser boolean*/ 
                            objBEProducto.IdAccion = sqlDr.GetInt32(nIndex5);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return objBEProducto;
        }
        #endregion         
    }
}
