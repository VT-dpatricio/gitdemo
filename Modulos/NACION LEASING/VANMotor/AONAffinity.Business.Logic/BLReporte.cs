﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.DataAccess.Bais;
using AONAffinity.Motor.BusinessEntity;
using AONAffinity.Motor.BusinessEntity.Bais;

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLReporte
    {
        #region NoTransaccional
        public List<BECertificado> ListarPorVencer(Int32 pnNroMeses, Int32 pnIdPproducto)
        {
            List<BECertificado> lstBECertificado = new List<BECertificado>();

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus()))
            {
                sqlCn.Open();

                DACertificado objDACertificado = new DACertificado();
                SqlDataReader sqlDr = objDACertificado.ListarPorVencer(pnNroMeses, pnIdPproducto, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idCertificado");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex3 = sqlDr.GetOrdinal("numCertificado");
                        Int32 nIndex4 = sqlDr.GetOrdinal("opcion");
                        Int32 nIndex5 = sqlDr.GetOrdinal("nombre1");
                        Int32 nIndex6 = sqlDr.GetOrdinal("nombre2");
                        Int32 nIndex7 = sqlDr.GetOrdinal("apellido1");
                        Int32 nIndex8 = sqlDr.GetOrdinal("apellido2");
                        Int32 nIndex9 = sqlDr.GetOrdinal("finVigencia");

                        while (sqlDr.Read())
                        {
                            BECertificado objBECertificado = new BECertificado();

                            objBECertificado.IdCertificado = sqlDr.GetString(nIndex1);
                            objBECertificado.IdProducto = sqlDr.GetInt32(nIndex2);
                            objBECertificado.NumCertificado = sqlDr.GetDecimal(nIndex3);
                            objBECertificado.Opcion = sqlDr.GetString(nIndex4);
                            objBECertificado.Nombre1 = sqlDr.GetString(nIndex5);
                            if (sqlDr.IsDBNull(nIndex6)) { objBECertificado.Nombre2 = NullTypes.CadenaNull; } else { objBECertificado.Nombre2 = sqlDr.GetString(nIndex6); }
                            objBECertificado.Apellido1 = sqlDr.GetString(nIndex7);
                            if (sqlDr.IsDBNull(nIndex8)) { objBECertificado.Apellido2 = NullTypes.CadenaNull; } else { objBECertificado.Apellido2 = sqlDr.GetString(nIndex8); }
                            objBECertificado.FinVigencia = sqlDr.GetDateTime(nIndex9);

                            lstBECertificado.Add(objBECertificado);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBECertificado;
        }

        /// <summary>
        /// Permite obtener un certificado por código.
        /// </summary>
        /// <param name="pcIdCertificado">Código de certificado.</param>
        /// <returns>Objeto de tipo BECertificado.</returns>
        public BECertificado Obtener(String pcIdCertificado)
        {
            BECertificado objBECertificado = new BECertificado();

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDJanus()))
            {
                sqlCn.Open();

                DACertificado objDACertificado = new DACertificado();
                SqlDataReader sqlDr = objDACertificado.Obtener(pcIdCertificado, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex0 = sqlDr.GetOrdinal("numCertificado");
                        Int32 nIndex1 = sqlDr.GetOrdinal("desProducto");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idInformador");
                        Int32 nIndex3 = sqlDr.GetOrdinal("nomInformador");
                        Int32 nIndex4 = sqlDr.GetOrdinal("codOficina");
                        Int32 nIndex5 = sqlDr.GetOrdinal("idOficina");
                        Int32 nIndex6 = sqlDr.GetOrdinal("nomOficina");
                        Int32 nIndex7 = sqlDr.GetOrdinal("desMedioPago");
                        Int32 nIndex8 = sqlDr.GetOrdinal("idMedioPago");
                        Int32 nIndex9 = sqlDr.GetOrdinal("idFrecuencia");
                        Int32 nIndex10 = sqlDr.GetOrdinal("desFrecuencia");
                        Int32 nIndex11 = sqlDr.GetOrdinal("desTipoDocumento");
                        Int32 nIndex12 = sqlDr.GetOrdinal("idTipoDocumento");
                        Int32 nIndex13 = sqlDr.GetOrdinal("nomCiudad");
                        Int32 nIndex14 = sqlDr.GetOrdinal("nomDepartamento");
                        Int32 nIndex15 = sqlDr.GetOrdinal("desEstadoCertificado");
                        Int32 nIndex16 = sqlDr.GetOrdinal("idEstadoCertificado");
                        Int32 nIndex17 = sqlDr.GetOrdinal("desMotivoAnulacion");
                        Int32 nIndex18 = sqlDr.GetOrdinal("idMotivoAnulacion");
                        Int32 nIndex19 = sqlDr.GetOrdinal("vigencia");
                        Int32 nIndex20 = sqlDr.GetOrdinal("finVigencia");
                        Int32 nIndex21 = sqlDr.GetOrdinal("digitacion");
                        Int32 nIndex22 = sqlDr.GetOrdinal("venta");
                        Int32 nIndex23 = sqlDr.GetOrdinal("opcion");
                        Int32 nIndex24 = sqlDr.GetOrdinal("montoAsegurado");
                        Int32 nIndex25 = sqlDr.GetOrdinal("primaBruta");
                        Int32 nIndex26 = sqlDr.GetOrdinal("iva");
                        Int32 nIndex27 = sqlDr.GetOrdinal("primaCobrar");
                        Int32 nIndex28 = sqlDr.GetOrdinal("primaTotal");
                        Int32 nIndex29 = sqlDr.GetOrdinal("numeroCuenta");
                        Int32 nIndex30 = sqlDr.GetOrdinal("vencimiento");
                        Int32 nIndex31 = sqlDr.GetOrdinal("nombre1");
                        Int32 nIndex32 = sqlDr.GetOrdinal("nombre2");
                        Int32 nIndex33 = sqlDr.GetOrdinal("apellido1");
                        Int32 nIndex34 = sqlDr.GetOrdinal("apellido2");
                        Int32 nIndex35 = sqlDr.GetOrdinal("ccCliente");
                        Int32 nIndex36 = sqlDr.GetOrdinal("telefono");
                        Int32 nIndex37 = sqlDr.GetOrdinal("direccion");
                        Int32 nIndex38 = sqlDr.GetOrdinal("solicitudAnulacion");
                        Int32 nIndex39 = sqlDr.GetOrdinal("digitacionAnulacion");
                        Int32 nIndex40 = sqlDr.GetOrdinal("efectuarAnulacion");
                        Int32 nIndex41 = sqlDr.GetOrdinal("consistente");
                        Int32 nIndex42 = sqlDr.GetOrdinal("simboloMoneda");
                        Int32 nIndex43 = sqlDr.GetOrdinal("desMonedaPrima");
                        Int32 nIndex44 = sqlDr.GetOrdinal("desMonedaCobro");
                        Int32 nIndex45 = sqlDr.GetOrdinal("idMonedaCobro");
                        Int32 nIndex46 = sqlDr.GetOrdinal("idCiudad");
                        Int32 nIndex47 = sqlDr.GetOrdinal("usuarioModificacion");
                        Int32 nIndex48 = sqlDr.GetOrdinal("Provincia");

                        if (sqlDr.Read())
                        {
                            objBECertificado.NumCertificado = sqlDr.GetDecimal(nIndex0);
                            objBECertificado.DesProducto = sqlDr.GetString(nIndex1);
                            objBECertificado.IdInformador = sqlDr.GetString(nIndex2);
                            objBECertificado.NomInformador = sqlDr.GetString(nIndex3);
                            if (sqlDr.IsDBNull(nIndex4)) { objBECertificado.CodOficina = NullTypes.IntegerNull; } else { objBECertificado.CodOficina = sqlDr.GetInt32(nIndex4); }
                            objBECertificado.IdOficina = sqlDr.GetInt32(nIndex5);
                            objBECertificado.NomOficina = sqlDr.GetString(nIndex6);
                            objBECertificado.DesMedioPago = sqlDr.GetString(nIndex7);
                            objBECertificado.IdMedioPago = sqlDr.GetString(nIndex8);
                            objBECertificado.IdFrecuencia = sqlDr.GetInt32(nIndex9);
                            objBECertificado.DesFrecuencia = sqlDr.GetString(nIndex10);
                            objBECertificado.DesTipoDocumento = sqlDr.GetString(nIndex11);
                            objBECertificado.IdTipoDocumento = sqlDr.GetString(nIndex12);
                            objBECertificado.NomCiudad = sqlDr.GetString(nIndex13);
                            objBECertificado.NomDepartamento = sqlDr.GetString(nIndex14);
                            objBECertificado.DesEstadoCertificado = sqlDr.GetString(nIndex15);
                            objBECertificado.IdEstadoCertificado = sqlDr.GetInt32(nIndex16);
                            objBECertificado.DesMotivoAnulacion = sqlDr.GetString(nIndex17);
                            objBECertificado.IdMotivoAnulacion = sqlDr.GetInt32(nIndex18);
                            if (sqlDr.IsDBNull(nIndex19)) { objBECertificado.Vigencia = NullTypes.FechaNull; } else { objBECertificado.Vigencia = sqlDr.GetDateTime(nIndex19); }
                            if (sqlDr.IsDBNull(nIndex20)) { objBECertificado.FinVigencia = NullTypes.FechaNull; } else { objBECertificado.FinVigencia = sqlDr.GetDateTime(nIndex20); }
                            objBECertificado.Digitacion = sqlDr.GetDateTime(nIndex21);
                            objBECertificado.Venta = sqlDr.GetDateTime(nIndex22);
                            objBECertificado.Opcion = sqlDr.GetString(nIndex23);
                            objBECertificado.MontoAsegurado = sqlDr.GetDecimal(nIndex24);
                            objBECertificado.PrimaBruta = sqlDr.GetDecimal(nIndex25);
                            objBECertificado.Iva = sqlDr.GetDecimal(nIndex26);
                            objBECertificado.PrimaCobrar = sqlDr.GetDecimal(nIndex27);
                            objBECertificado.PrimaTotal = sqlDr.GetDecimal(nIndex28);
                            objBECertificado.NumeroCuenta = sqlDr.GetString(nIndex29);
                            if (sqlDr.IsDBNull(nIndex30)) { objBECertificado.Vencimiento = NullTypes.FechaNull; } else { objBECertificado.Vencimiento = sqlDr.GetDateTime(nIndex30); }
                            objBECertificado.Nombre1 = sqlDr.GetString(nIndex31);
                            objBECertificado.Nombre2 = sqlDr.GetString(nIndex32);
                            objBECertificado.Apellido1 = sqlDr.GetString(nIndex33);
                            objBECertificado.Apellido2 = sqlDr.GetString(nIndex34);
                            objBECertificado.CcCliente = sqlDr.GetString(nIndex35);
                            objBECertificado.Telefono = sqlDr.GetString(nIndex36);
                            objBECertificado.Direccion = sqlDr.GetString(nIndex37);
                            if (sqlDr.IsDBNull(nIndex38)) { objBECertificado.SolicitudAnulacion = NullTypes.FechaNull; } else { objBECertificado.SolicitudAnulacion = sqlDr.GetDateTime(nIndex38); }
                            if (sqlDr.IsDBNull(nIndex39)) { objBECertificado.DigitacionAnulacion = NullTypes.FechaNull; } else { objBECertificado.DigitacionAnulacion = sqlDr.GetDateTime(nIndex39); }
                            if (sqlDr.IsDBNull(nIndex40)) { objBECertificado.EfectuarAnulacion = NullTypes.FechaNull; } else { objBECertificado.EfectuarAnulacion = sqlDr.GetDateTime(nIndex40); }
                            objBECertificado.Consistente = sqlDr.GetBoolean(nIndex41);
                            objBECertificado.SimboloMoneda = sqlDr.GetString(nIndex42);
                            objBECertificado.DesMonedaPrima = sqlDr.GetString(nIndex43);
                            objBECertificado.DesMonedaCobro = sqlDr.GetString(nIndex44);
                            objBECertificado.IdMonedaCobro = sqlDr.GetString(nIndex45);
                            objBECertificado.IdCiudad = sqlDr.GetInt32(nIndex46);
                            objBECertificado.UsuarioModificacion = sqlDr.GetString(nIndex47);
                            objBECertificado.NomProvincia = sqlDr.GetString(nIndex48);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return objBECertificado;
        }
        #endregion        
    }
}
