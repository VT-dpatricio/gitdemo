﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLProductoPoliza
    {
        public BEProductoPoliza Obtener(Int32 pnIdProducto, String pcOpcionPlan, String pcParametro) 
        {
            BEProductoPoliza objBEProductoPoliza = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAProductoPoliza objDAProductoPoliza = new DAProductoPoliza();
                SqlDataReader sqlDr = objDAProductoPoliza.Obtener(pnIdProducto, pcOpcionPlan, pcParametro, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex2 = sqlDr.GetOrdinal("nroPoliza");
                        Int32 nIndex3 = sqlDr.GetOrdinal("opcionPlan");
                        Int32 nIndex4 = sqlDr.GetOrdinal("parametro");

                        objBEProductoPoliza = new BEProductoPoliza();

                        if (sqlDr.Read()) 
                        {
                            objBEProductoPoliza.IdProducto = sqlDr.GetInt32(nIndex1);
                            objBEProductoPoliza.NroPoliza = sqlDr.GetString(nIndex2);
                            if (sqlDr.IsDBNull(nIndex3)) { objBEProductoPoliza.OpcionPlan = NullTypes.CadenaNull; } else { objBEProductoPoliza.OpcionPlan = sqlDr.GetString(nIndex3); }
                            if (sqlDr.IsDBNull(nIndex4)) { objBEProductoPoliza.Parametro = NullTypes.CadenaNull; } else { objBEProductoPoliza.Parametro = sqlDr.GetString(nIndex4); }
                        }

                        sqlDr.Close();  
                    }
                }
            }

            return objBEProductoPoliza;
        }
    }
}
