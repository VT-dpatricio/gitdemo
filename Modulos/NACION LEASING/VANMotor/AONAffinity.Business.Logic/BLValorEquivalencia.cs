﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources; 
using AONAffinity.Motor.Resources;  
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.BusinessLogic
{
    /// <summary>
    /// Clase de lógica del negocio a la tabla ValorEquivalencia.
    /// </summary>
    public class BLValorEquivalencia
    {
        #region NoTransaccional
        /// <summary>
        /// Permite obtener los valores de equivalencias.
        /// </summary>
        /// <param name="pnIdTipoProceso">Código de tipo de proceso.</param>
        /// <param name="pnIdTipoArchivo">Código de tipo de archivo.</param>
        /// <param name="pnOrden">Nro de orden del campo.</param>
        /// <param name="pcValor">Valor enviado en el campo.</param>
        /// <returns>Objeto de tipo BEValorEquivalencia.</returns>
        public BEValorEquivalencia Obtener(Int32 pnIdTipoProceso, Int32 pnIdTipoArchivo, Int32 pnOrden, String pcValor)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2012-05-03
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            BEValorEquivalencia objBEValorEquivalencia = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAValorEquivalencia objDAValorEquivalencia = new DAValorEquivalencia();
                SqlDataReader sqlDr = objDAValorEquivalencia.Obtener(pnIdTipoProceso, pnIdTipoArchivo, pnOrden, pcValor, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idTipoProceso");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idTipoArchivo");
                        Int32 nIndex3 = sqlDr.GetOrdinal("orden");
                        Int32 nIndex4 = sqlDr.GetOrdinal("valor");
                        Int32 nIndex5 = sqlDr.GetOrdinal("equivalencia");

                        objBEValorEquivalencia = new BEValorEquivalencia();

                        if (sqlDr.Read())
                        {
                            objBEValorEquivalencia.IdTipoProceso = sqlDr.GetInt32(nIndex1);
                            objBEValorEquivalencia.IdTipoArchivo = sqlDr.GetInt32(nIndex2);
                            objBEValorEquivalencia.Orden = sqlDr.GetInt32(nIndex3);
                            objBEValorEquivalencia.Valor = sqlDr.GetString(nIndex4);
                            objBEValorEquivalencia.Equivalencia = sqlDr.GetString(nIndex5);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return objBEValorEquivalencia;
        }
        #endregion       
    }
}
