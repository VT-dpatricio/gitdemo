﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.BusinessLogic
{
    /// <summary>
    /// Clase de lógica del negocio de la tabla Clase.
    /// </summary>
    public class BLClase
    {
        #region NoTransaccional
        /// <summary>
        /// Permite listar las clases de vehículos.
        /// </summary>
        /// <param name="pbStsRegistro">Estado del registro.</param>
        /// <returns>Objeto List de tipo BEClase.</returns>
        public List<BEClase> Listar(Nullable<Boolean> pbStsRegistro) 
        {            
            List<BEClase> lstBEClase = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAClase objDAClase = new DAClase();
                SqlDataReader sqlDr = objDAClase.Listar(pbStsRegistro, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idClase");
                        Int32 nIndex2 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex3 = sqlDr.GetOrdinal("usrCreacion");
                        Int32 nIndex4 = sqlDr.GetOrdinal("fecCreacion");
                        Int32 nIndex5 = sqlDr.GetOrdinal("usrModificacion");
                        Int32 nIndex6 = sqlDr.GetOrdinal("fecModificacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("stsRegistro");

                        lstBEClase = new List<BEClase>();

                        while (sqlDr.Read()) 
                        {
                            BEClase objBEClase = new BEClase();

                            objBEClase.IdClase = sqlDr.GetInt32(nIndex1);
                            objBEClase.Descripcion = sqlDr.GetString(nIndex2);
                            objBEClase.UsuarioCreacion = sqlDr.GetString(nIndex3);
                            objBEClase.FechaCreacion = sqlDr.GetDateTime(nIndex4);
                            if (sqlDr.IsDBNull(nIndex5)) { objBEClase.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEClase.UsuarioModificacion = sqlDr.GetString(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBEClase.FechaModificacion = NullTypes.FechaNull; } else { objBEClase.FechaModificacion = sqlDr.GetDateTime(nIndex6); }
                            objBEClase.EstadoRegistro = sqlDr.GetBoolean(nIndex7);

                            lstBEClase.Add(objBEClase);  
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBEClase;
        }      
        #endregion
    }
}
