﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;
using AONAffinity.Library.Resources;

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLProductoMarca
    {
        #region NoTransaccional
        public List<BEProductoMarca> ListarxProducto(Int32 pnIdproducto, Boolean? pbStsRegistro)
        {
            List<BEProductoMarca> lstBEProductoMarca = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor()))
            {
                sqlCn.Open();

                DAProductoMarca objDAProductoMarca = new DAProductoMarca();
                SqlDataReader sqlDr = objDAProductoMarca.ListarxProducto(pnIdproducto, pbStsRegistro, sqlCn);

                if (sqlDr != null)
                {
                    if (sqlDr.HasRows)
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("idProducto");
                        Int32 nIndex2 = sqlDr.GetOrdinal("idMarca");
                        Int32 nIndex3 = sqlDr.GetOrdinal("descripcion");
                        Int32 nIndex4 = sqlDr.GetOrdinal("codExterno");
                        Int32 nIndex5 = sqlDr.GetOrdinal("FecCreacion");
                        Int32 nIndex6 = sqlDr.GetOrdinal("UsrCreacion");
                        Int32 nIndex7 = sqlDr.GetOrdinal("FecModificacion");
                        Int32 nIndex8 = sqlDr.GetOrdinal("UsrModificacion");
                        Int32 nIndex9 = sqlDr.GetOrdinal("StsRegistro");

                        lstBEProductoMarca = new List<BEProductoMarca>();

                        while (sqlDr.Read())
                        {
                            BEProductoMarca objBEProductoMarca = new BEProductoMarca();
                            objBEProductoMarca.IdProducto = sqlDr.GetInt32(nIndex1);
                            objBEProductoMarca.IdMarca = sqlDr.GetInt32(nIndex2);
                            objBEProductoMarca.Descripcion = sqlDr.GetString(nIndex3);
                            if (sqlDr.IsDBNull(nIndex4)) { objBEProductoMarca.CodExterno = NullTypes.CadenaNull; } else { objBEProductoMarca.CodExterno = sqlDr.GetString(nIndex4); }
                            objBEProductoMarca.FechaCreacion = sqlDr.GetDateTime(nIndex5);
                            objBEProductoMarca.UsuarioCreacion = sqlDr.GetString(nIndex6);
                            if (sqlDr.IsDBNull(nIndex7)) { objBEProductoMarca.FechaModificacion = NullTypes.FechaNull; } else { objBEProductoMarca.FechaModificacion = sqlDr.GetDateTime(nIndex7); }
                            if (sqlDr.IsDBNull(nIndex8)) { objBEProductoMarca.UsuarioModificacion = NullTypes.CadenaNull; } else { objBEProductoMarca.UsuarioModificacion = sqlDr.GetString(nIndex8); }
                            objBEProductoMarca.EstadoRegistro = sqlDr.GetBoolean(nIndex9);  
                            lstBEProductoMarca.Add(objBEProductoMarca);
                        }

                        sqlDr.Close();
                    }
                }
            }

            return lstBEProductoMarca;
        }
        #endregion
    }
}
