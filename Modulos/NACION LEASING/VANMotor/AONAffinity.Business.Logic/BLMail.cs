﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using AONAffinity.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLMail
    {
        public void Enviar(String pcMailTo, String pcMailToCc, String pcMailSubject, String pcBody, String pcArchivo)
        {
            BLParametro objBLParametro = new BLParametro();
            BEParametro objBEParametro_Usr = objBLParametro.Obtener(36);
            BEParametro objBEParametro_Pws = objBLParametro.Obtener(44);

            MailMessage mensaje = new MailMessage();
            mensaje.From = new MailAddress(AppSettings.MailFrom, "Cotización Seguro Vehícular");            
            mensaje.To.Add(AppSettings.MailFrom);     //mensaje.To.Add(pcMailTo);

            //if (pcMailToCc != NullTypes.CadenaNull) 
            //{
            //    mensaje.CC.Add(pcMailToCc);   
            //}

            pcBody = pcBody + "<br /><br /> Correo del cliente: " + pcMailTo + " <br /> Correo Copia: " + pcMailToCc;

            mensaje.Subject = pcMailSubject;
            mensaje.BodyEncoding = Encoding.UTF8;
            mensaje.IsBodyHtml = true;
            mensaje.Body = String.Format(pcBody);
            mensaje.Priority = MailPriority.High; 
            Attachment objAttachment = null;

            if (pcArchivo != NullTypes.CadenaNull) 
            {
                objAttachment = new Attachment(AppSettings.DirCotizacionRIMAC + pcArchivo);
                mensaje.Attachments.Add(objAttachment);                
            }

            SmtpClient smtp = new SmtpClient(AppSettings.SMTPClient, 25);
            //smtp.Credentials = new System.Net.NetworkCredential(AppSettings.MailFrom, AppSettings.MailPswd);
            smtp.Credentials = new System.Net.NetworkCredential(objBEParametro_Usr.ValorCadena, objBEParametro_Pws.ValorCadena);

            smtp.Send(mensaje);

            objAttachment.Dispose();
        }        
    }
}
