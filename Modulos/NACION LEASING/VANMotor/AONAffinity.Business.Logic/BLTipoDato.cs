﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLTipoDato
    {
        public List<BETipoDato> Listar() 
        {
            List<BETipoDato> lstBETipoDato = new List<BETipoDato>();
            BETipoDato objBETipoDato = new BETipoDato();

            objBETipoDato.IdTipoDato = "S";
            objBETipoDato.Descripcion = "CADENA";
            lstBETipoDato.Add(objBETipoDato);

            objBETipoDato = new BETipoDato();
            objBETipoDato.IdTipoDato = "N";
            objBETipoDato.Descripcion = "NUMERO";
            lstBETipoDato.Add(objBETipoDato);

            objBETipoDato = new BETipoDato();
            objBETipoDato.IdTipoDato = "D";
            objBETipoDato.Descripcion = "FECHA";
            lstBETipoDato.Add(objBETipoDato); 

            return lstBETipoDato;
        }  
    }
}
