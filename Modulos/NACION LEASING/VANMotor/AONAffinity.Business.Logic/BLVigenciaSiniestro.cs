﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.BusinessLogic
{
    public class BLVigenciaSiniestro
    {
        #region NoTransaccional
        public BEVigenciaSiniestro Obtener(String pcNroMotor) 
        {
            BEVigenciaSiniestro objBEVigenciaSiniestro = null;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAVigenciaSiniestro objDAVigenciaSiniestro = new DAVigenciaSiniestro();
                SqlDataReader sqlDr = objDAVigenciaSiniestro.Obtener(pcNroMotor, sqlCn);

                if (sqlDr != null) 
                {
                    if (sqlDr.HasRows) 
                    {
                        Int32 nIndex1 = sqlDr.GetOrdinal("nroMotor");
                        Int32 nIndex2 = sqlDr.GetOrdinal("cantSiniestro1Anio");
                        Int32 nIndex3 = sqlDr.GetOrdinal("cantSiniestro2Anio");
                        Int32 nIndex4 = sqlDr.GetOrdinal("cantSiniestro3Anio");
                        Int32 nIndex5 = sqlDr.GetOrdinal("cantSiniestro4Anio");
                        Int32 nIndex6 = sqlDr.GetOrdinal("cantSiniestro5Anio");
                        Int32 nIndex7 = sqlDr.GetOrdinal("cantVigencia");
                        Int32 nIndex8 = sqlDr.GetOrdinal("montoSiniestro1Anio");
                        Int32 nIndex9 = sqlDr.GetOrdinal("montoSiniestro2Anio");
                        Int32 nIndex10 = sqlDr.GetOrdinal("montoSiniestro3Anio");
                        Int32 nIndex11 = sqlDr.GetOrdinal("montoSiniestro4Anio");
                        Int32 nIndex12 = sqlDr.GetOrdinal("montoSiniestro5Anio");
                        Int32 nIndex13 = sqlDr.GetOrdinal("fecIniVigencia");
                        Int32 nIndex14 = sqlDr.GetOrdinal("fecFinVigencia");  
    
                        objBEVigenciaSiniestro = new BEVigenciaSiniestro();

                        if (sqlDr.Read()) 
                        {
                            objBEVigenciaSiniestro.NroMotor = sqlDr.GetString(nIndex1);
                            if (sqlDr.IsDBNull(nIndex2)) { objBEVigenciaSiniestro.CantSiniestro1Anio = 0; } else { objBEVigenciaSiniestro.CantSiniestro1Anio = sqlDr.GetInt32(nIndex2); }
                            if (sqlDr.IsDBNull(nIndex3)) { objBEVigenciaSiniestro.CantSiniestro2Anio = 0; } else { objBEVigenciaSiniestro.CantSiniestro2Anio = sqlDr.GetInt32(nIndex3); }
                            if (sqlDr.IsDBNull(nIndex4)) { objBEVigenciaSiniestro.CantSiniestro3Anio = 0; } else { objBEVigenciaSiniestro.CantSiniestro3Anio = sqlDr.GetInt32(nIndex4); }
                            if (sqlDr.IsDBNull(nIndex5)) { objBEVigenciaSiniestro.CantSiniestro4Anio = 0; } else { objBEVigenciaSiniestro.CantSiniestro4Anio = sqlDr.GetInt32(nIndex5); }
                            if (sqlDr.IsDBNull(nIndex6)) { objBEVigenciaSiniestro.CantSiniestro5Anio = 0; } else { objBEVigenciaSiniestro.CantSiniestro5Anio = sqlDr.GetInt32(nIndex6); }
                            objBEVigenciaSiniestro.CantVigencia = sqlDr.GetInt32(nIndex7);
                            if (sqlDr.IsDBNull(nIndex8)) { objBEVigenciaSiniestro.MontoSiniestro1Anio = 0; } else { objBEVigenciaSiniestro.MontoSiniestro1Anio = sqlDr.GetDecimal(nIndex8); }
                            if (sqlDr.IsDBNull(nIndex9)) { objBEVigenciaSiniestro.MontoSiniestro2Anio = 0; } else { objBEVigenciaSiniestro.MontoSiniestro2Anio = sqlDr.GetDecimal(nIndex9); }
                            if (sqlDr.IsDBNull(nIndex10)) { objBEVigenciaSiniestro.MontoSiniestro3Anio = 0; } else { objBEVigenciaSiniestro.MontoSiniestro3Anio = sqlDr.GetDecimal(nIndex10); }
                            if (sqlDr.IsDBNull(nIndex11)) { objBEVigenciaSiniestro.MontoSiniestro4Anio = 0; } else { objBEVigenciaSiniestro.MontoSiniestro4Anio = sqlDr.GetDecimal(nIndex11); }
                            if (sqlDr.IsDBNull(nIndex12)) { objBEVigenciaSiniestro.MontoSiniestro5Anio = 0; } else { objBEVigenciaSiniestro.MontoSiniestro5Anio = sqlDr.GetDecimal(nIndex12); }
                            if (sqlDr.IsDBNull(nIndex13)) { objBEVigenciaSiniestro.FecIniVigencia = NullTypes.FechaNull; } else { objBEVigenciaSiniestro.FecIniVigencia = sqlDr.GetDateTime(nIndex13); }
                            if (sqlDr.IsDBNull(nIndex14)) { objBEVigenciaSiniestro.FecFinVigencia = NullTypes.FechaNull; } else { objBEVigenciaSiniestro.FecFinVigencia = sqlDr.GetDateTime(nIndex14); }
                        }

                        sqlDr.Close(); 
                    }
                }
            }

            return objBEVigenciaSiniestro;
        }
        #endregion

        #region Transaccional
        public Int32 Insertar(BEVigenciaSiniestro objBEVigenciaSiniestro) 
        {
            Int32 nResult = 0;

            using (SqlConnection sqlCn = new SqlConnection(DAConexion.ConexionBDMotor())) 
            {
                sqlCn.Open();

                DAVigenciaSiniestro objDAVigenciaSiniestro = new DAVigenciaSiniestro();
                nResult = objDAVigenciaSiniestro.Insertar(objBEVigenciaSiniestro, sqlCn);
            }

            return nResult;
        }
        #endregion
    }
}
