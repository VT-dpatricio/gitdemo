﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.DataAccess
{
    /// <summary>
    /// Clase de acceso a datos de la tabla ValorEquivalencia.
    /// </summary>
    public class DAValorEquivalencia
    {
        #region NoTransaccional
        /// <summary>
        /// Permite obtener los valores de equivalencias.
        /// </summary>
        /// <param name="pnIdTipoProceso">Código de tipo de proceso.</param>
        /// <param name="pnIdTipoArchivo">Código de tipo de archivo.</param>
        /// <param name="pnOrden">Nro de orden del campo.</param>
        /// <param name="pcValor">Valor enviado en el campo.</param>
        /// <param name="pSqlCn">Objeto que permite la conexion a la BD.</param>
        /// <returns>Objeto de tipo SqlDataReader.</returns>
        public SqlDataReader Obtener(Int32 pnIdTipoProceso, Int32 pnIdTipoArchivo, Int32 pnOrden, String pcValor, SqlConnection pSqlCn)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2012-05-03
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_ValorEquivalencia_Obtener]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idTipoProceso", SqlDbType.Int);
                sqlParam1.Value = pnIdTipoProceso;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idTipoArchivo", SqlDbType.Int);
                sqlParam2.Value = pnIdTipoArchivo;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@orden", SqlDbType.Int);
                sqlParam3.Value = pnOrden;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@valor", SqlDbType.VarChar, 50);
                sqlParam4.Value = pcValor;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);
            }

            return sqlDr;
        }
        #endregion        
    }
}
