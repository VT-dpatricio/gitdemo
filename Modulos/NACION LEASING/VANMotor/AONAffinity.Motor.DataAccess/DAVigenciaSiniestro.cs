﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessEntity;      

namespace AONAffinity.Motor.DataAccess
{
    public class DAVigenciaSiniestro
    {
        #region NoTransaccional
        public SqlDataReader Obtener(String pcNroMotor, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_VigenciaSiniestro_Obtener]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@nroMotor", SqlDbType.VarChar, 25);
                sqlParam1.Value = pcNroMotor;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);
            }

            return sqlDr;
        }
        #endregion

        #region Transaccional
        public Int32 Insertar(BEVigenciaSiniestro objBEVigenciaSiniestro, SqlConnection pSqlCn)
        {
            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_VigenciaSiniestro_Insertar]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@nroMotor", SqlDbType.VarChar, 25);
                sqlParam1.Value = objBEVigenciaSiniestro.NroMotor;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@cantSiniestro1Anio", SqlDbType.Int);
                sqlParam2.Value = objBEVigenciaSiniestro.CantSiniestro1Anio;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@cantSiniestro2Anio", SqlDbType.Int);
                sqlParam3.Value = objBEVigenciaSiniestro.CantSiniestro2Anio;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@cantSiniestro3Anio", SqlDbType.Int);
                sqlParam4.Value = objBEVigenciaSiniestro.CantSiniestro3Anio;

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@cantSiniestro4Anio", SqlDbType.Int);
                sqlParam5.Value = objBEVigenciaSiniestro.CantSiniestro4Anio;

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@cantSiniestro5Anio", SqlDbType.Int);
                sqlParam6.Value = objBEVigenciaSiniestro.CantSiniestro5Anio;

                SqlParameter sqlParam7 = sqlCmd.Parameters.Add("@montoSiniestro1Anio", SqlDbType.Decimal);
                sqlParam7.Value = objBEVigenciaSiniestro.MontoSiniestro1Anio;

                SqlParameter sqlParam8 = sqlCmd.Parameters.Add("@montoSiniestro2Anio", SqlDbType.Decimal);
                sqlParam8.Value = objBEVigenciaSiniestro.MontoSiniestro2Anio;

                SqlParameter sqlParam9 = sqlCmd.Parameters.Add("@montoSiniestro3Anio", SqlDbType.Decimal);
                sqlParam9.Value = objBEVigenciaSiniestro.MontoSiniestro3Anio;

                SqlParameter sqlParam10 = sqlCmd.Parameters.Add("@montoSiniestro4Anio", SqlDbType.Decimal);
                sqlParam10.Value = objBEVigenciaSiniestro.MontoSiniestro4Anio;

                SqlParameter sqlParam11 = sqlCmd.Parameters.Add("@montoSiniestro5Anio", SqlDbType.Decimal);
                sqlParam11.Value = objBEVigenciaSiniestro.MontoSiniestro5Anio;

                SqlParameter sqlParam12 = sqlCmd.Parameters.Add("@fecIniVigencia", SqlDbType.SmallDateTime);
                sqlParam12.Value = objBEVigenciaSiniestro.FecIniVigencia;

                SqlParameter sqlParam13 = sqlCmd.Parameters.Add("@fecFinVigencia", SqlDbType.SmallDateTime);
                sqlParam13.Value = objBEVigenciaSiniestro.FecFinVigencia;

                SqlParameter sqlParam14 = sqlCmd.Parameters.Add("@usrCreacion", SqlDbType.VarChar, 50);
                sqlParam14.Value = objBEVigenciaSiniestro.UsuarioCreacion;

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        }
        #endregion
    }
}
