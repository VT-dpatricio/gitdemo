﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Motor.BusinessEntity; 

namespace AONAffinity.Motor.DataAccess
{
    public class DAProductoTipoCanal 
    {
        public SqlDataReader Obtener(Int32 pnIdProducto, Int32 pnIdTipoCanal, SqlConnection pSqlCn) 
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_ProductoTipoCanal_Obtener]", pSqlCn)) 
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idTipoCanal", SqlDbType.Int);
                sqlParam2.Value = pnIdTipoCanal;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);
            }

            return sqlDr;
        }
    }
}
