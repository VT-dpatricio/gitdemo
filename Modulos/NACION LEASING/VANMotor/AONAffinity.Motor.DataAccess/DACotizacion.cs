﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;     
using AONAffinity.Motor.Resources;  
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;  

namespace AONAffinity.Motor.DataAccess
{
    public class DACotizacion
    {
        #region NoTransaccional
        //new2013
        public SqlDataReader Buscar(Int32 pnIdTipo, String pcNroDocumento, String pcApePaterno, String pcApeMaterno, String pcPriNombre, String pcSegNombre,  String pcNroPlaca, String pcUsuario, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cotizacion_Buscar]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@tipo", SqlDbType.Int);
                sqlParam1.Value = pnIdTipo;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@nroDocumento", SqlDbType.VarChar, 20);
                sqlParam2.Value = pcNroDocumento;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@apePaterno", SqlDbType.VarChar, 50);
                sqlParam3.Value = pcApePaterno;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@apeMaterno", SqlDbType.VarChar, 50);
                sqlParam4.Value = pcApeMaterno;

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@priNombre", SqlDbType.VarChar, 50);
                sqlParam5.Value = pcPriNombre;

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@segNombre", SqlDbType.VarChar, 50);
                sqlParam6.Value = pcSegNombre;

                SqlParameter sqlParam7 = sqlCmd.Parameters.Add("@nroPlaca", SqlDbType.VarChar, 50);
                sqlParam7.Value = pcNroPlaca;

                SqlParameter sqlParam8 = sqlCmd.Parameters.Add("@usuario", SqlDbType.VarChar, 50);
                sqlParam8.Value = pcUsuario;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }
        //new2013
        public Int32 ActualizarElegido(Decimal pnNroCotizacion, Int32 pnCorrelativo, Boolean pbElegido, String pcUsuario, SqlConnection pSqlCn) 
        {
            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_CotizacionDetalle_ActualizarElegido]", pSqlCn)) 
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@nroCotizacion", SqlDbType.Decimal);
                sqlParam1.Value = pnNroCotizacion;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@correlativo", SqlDbType.Int);
                sqlParam2.Value = pnCorrelativo;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@elegido", SqlDbType.Bit);
                sqlParam3.Value = pbElegido;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@usrModificacion", SqlDbType.VarChar, 50);
                sqlParam4.Value = pcUsuario;

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        }


        public SqlDataReader ListarxNroDocumento(String pcNroDocumento, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cotizacion_ListarxNroDocumento]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@nroDocumento", SqlDbType.VarChar, 20);
                sqlParam1.Value = pcNroDocumento;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }

        public SqlDataReader ListarxNomApe(String pcApePaterno, String pcApeMaterno, String pcPriNonbre, String pcSegNonbre, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cotizacion_ListarxNomApe]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@apePaterno", SqlDbType.VarChar, 50);
                sqlParam1.Value = pcApePaterno;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@apeMaterno", SqlDbType.VarChar, 50);
                sqlParam2.Value = pcApeMaterno;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@priNonbre", SqlDbType.VarChar, 50);
                sqlParam3.Value = pcPriNonbre;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@segNonbre", SqlDbType.VarChar, 50);
                if (pcSegNonbre == NullTypes.CadenaNull) { sqlParam4.Value = DBNull.Value; } else { sqlParam4.Value = pcSegNonbre; }

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }

        public SqlDataReader GenerarLog(Int32 pnIdProceso, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cotizacion_GenerarLog]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProceso", SqlDbType.Int);
                sqlParam1.Value = pnIdProceso;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }

        public SqlDataReader Obtener(Int32 pnIdPeriodo, Decimal pnNroCotizacion, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cotizacion_Obtener]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idPeriodo", SqlDbType.Int);
                sqlParam1.Value = pnIdPeriodo;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@nroCotizacion", SqlDbType.Decimal);
                sqlParam2.Value = pnNroCotizacion;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);
            }

            return sqlDr;
        }

        public SqlDataReader ValidarVehiculo(Int32 pnIdProducto, Int32 pnIdModelo, Boolean pbTimonCambiado, Decimal pnValorVehiculo, Int32 pnAnioFab, Int16 pnNroAsientos, Int32 pnidTipoVehiculo, Int32 pnIdUsoVehiculo, Boolean pbGPS, Boolean pbGas, Int32 pnIdCiudad, Int32 pnIdCiudadInsp, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cotizacion_ValidarVehiculo]", pSqlCn)) 
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idModelo", SqlDbType.Int);
                sqlParam2.Value = pnIdModelo;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@TimonCambiado", SqlDbType.Bit);
                sqlParam3.Value = pbTimonCambiado;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@ValorVehiculo", SqlDbType.Decimal);
                sqlParam4.Value = pnValorVehiculo;

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@anioFab", SqlDbType.Int);
                sqlParam5.Value = pnAnioFab;

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@nroAsientos", SqlDbType.SmallInt);
                sqlParam6.Value = pnNroAsientos;

                SqlParameter sqlParam7 = sqlCmd.Parameters.Add("@idTipoVehiculo", SqlDbType.Int);
                sqlParam7.Value = pnidTipoVehiculo;

                SqlParameter sqlParam8 = sqlCmd.Parameters.Add("@idUsoVehiculo", SqlDbType.Int);
                sqlParam8.Value = pnIdUsoVehiculo;

                SqlParameter sqlParam9 = sqlCmd.Parameters.Add("@GPS", SqlDbType.Bit);
                sqlParam9.Value = pbGPS;

                SqlParameter sqlParam10 = sqlCmd.Parameters.Add("@gas", SqlDbType.Bit);
                sqlParam10.Value = pbGas;

                SqlParameter sqlParam11 = sqlCmd.Parameters.Add("@idCiudad", SqlDbType.Int);
                sqlParam11.Value = pnIdCiudad;

                SqlParameter sqlParam12 = sqlCmd.Parameters.Add("@idCiudadInsp", SqlDbType.Int);
                sqlParam12.Value = pnIdCiudadInsp;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult); 
            }

            return sqlDr;
        }

        /// <summary>
        /// Permite obtener la cantidad de vigencias del cliente.
        /// </summary>
        /// <param name="pnIdProducto">Código de producto.</param>
        /// <param name="pcNroMotor">nro de motor.</param>
        /// <param name="pSqlCn">Objeto que permite la conexion a la BD.</param>
        /// <returns>Nro de vigencias del cliente.</returns>
        public SqlDataReader ObtenerVigencias(Int32 pnIdProducto, String pcNroMotor, SqlConnection pSqlCn)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2012-01-04
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cotizacion_ObtenerVigencias]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@nroMotor", SqlDbType.VarChar, 25);
                sqlParam2.Value = pcNroMotor;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);
            }

            return sqlDr;
        }

        /// <summary>
        /// Permite obtener el valor del vehiculo depresiado.
        /// </summary>
        /// <param name="pnAnioFab"></param>
        /// <param name="pnValorVehiculo"></param>
        /// <param name="pnIdProducto"></param>
        /// <param name="pSqlCn"></param>
        /// <returns></returns>
        public SqlDataReader ObtenerValorDepresiado(Int32 pnAnioFab, Decimal pnValorVehiculo, Int32 pnIdProducto, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cotizacion_ObtenerValorDepreciado]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@anioFab", SqlDbType.Int);
                sqlParam1.Value = pnAnioFab;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@valorVehiculo", SqlDbType.Decimal);
                sqlParam2.Value = pnValorVehiculo;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam3.Value = pnIdProducto;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);

            }

            return sqlDr;
        }

        public SqlDataReader ListarTipo(SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AONMotor_Cotizacion_ListarTipo]", pSqlCn)) 
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }

        public SqlDataReader GenerarCotizacion(Int32 pnTipoCot, Int32 pnIdProducto, Int32 pnIdSponsor, Int32 pnIdGrupProd,
                                               Decimal pnValorVehiculo, Int32 pnIdMarca, Int32 pnIdModelo, Int32 pnAnioFab, Boolean pbTimonCamb, Boolean pbGPS,
                                               Boolean pbScoring, Decimal pnPorcentValVeh, Int32 pnIdCiudad,
                                               Int32 pnIdDescuento, Int32 pnIdRecargo, Int32 pnAdicional, String pcIdAseguradores, 
                                               String pcParam1, String pcParam2, String pcParam3, String pcParam4, String pcParam5, 
                                               String pcParam6, String pcParam7, String pcParam8, String pcParam9, String pcParam10,
                                               SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AONMotor_Cotizacion_GenerarCotizacion]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@tipoCot", SqlDbType.Char, 1);
                sqlParam1.Value = pnTipoCot;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam2.Value = pnIdProducto;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@valorVehiculo", SqlDbType.Decimal);
                sqlParam3.Value = pnValorVehiculo;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@idMarca", SqlDbType.Int);
                sqlParam4.Value = pnIdMarca;

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@idModelo", SqlDbType.Int);
                sqlParam5.Value = pnIdModelo;

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@anioFab", SqlDbType.Int);
                sqlParam6.Value = pnAnioFab;

                SqlParameter sqlParam7 = sqlCmd.Parameters.Add("@gps", SqlDbType.Bit);
                sqlParam7.Value = pbGPS;

                SqlParameter sqlParam8 = sqlCmd.Parameters.Add("@timonCambiado", SqlDbType.Bit);
                sqlParam8.Value = pbTimonCamb;

                SqlParameter sqlParam9 = sqlCmd.Parameters.Add("@Scoring", SqlDbType.Bit);
                sqlParam9.Value = pbScoring;

                SqlParameter sqlParam10 = sqlCmd.Parameters.Add("@porcentValVeh", SqlDbType.Decimal);
                sqlParam10.Value = pnPorcentValVeh;

                SqlParameter sqlParam11 = sqlCmd.Parameters.Add("@idGrupoProd", SqlDbType.Int);
                sqlParam11.Value = pnIdGrupProd;

                SqlParameter sqlParam12 = sqlCmd.Parameters.Add("@idCiudad", SqlDbType.Int);
                sqlParam12.Value = pnIdCiudad;

                SqlParameter sqlParam13 = sqlCmd.Parameters.Add("@idDescuento", SqlDbType.Int);
                if (pnIdDescuento == -1) { sqlParam13.Value = DBNull.Value; } else { sqlParam13.Value = pnIdDescuento; }

                SqlParameter sqlParam14 = sqlCmd.Parameters.Add("@idRecargo", SqlDbType.Int);
                if (pnIdRecargo == -1) { sqlParam14.Value = DBNull.Value; } else { sqlParam14.Value = pnIdRecargo; }

                SqlParameter sqlParam15 = sqlCmd.Parameters.Add("@idAdicional", SqlDbType.Int);
                if (pnAdicional == -1) { sqlParam15.Value = DBNull.Value; } else { sqlParam15.Value = pnAdicional; }

                SqlParameter sqlParam16 = sqlCmd.Parameters.Add("@idSponsor", SqlDbType.Int);
                sqlParam16.Value = pnIdSponsor;

                SqlParameter sqlParam17 = sqlCmd.Parameters.Add("@idAseguradores", SqlDbType.VarChar, 100);
                sqlParam17.Value = pcIdAseguradores;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }

        public SqlDataReader ObtenerDetalle(Decimal pnNroCotizacion, SqlConnection pSqlCn) 
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cotizacion_ObtenerDetalle]", pSqlCn)) 
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam = sqlCmd.Parameters.Add("@nroCotizacion", SqlDbType.Decimal);
                sqlParam.Value = pnNroCotizacion;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }
        #endregion

        #region Transaccional
        public Int32 Insertar(BECotizacion pObjBECotizacion, SqlConnection pSqlCn)
        {
            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cotizacion_Insertar]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@apePaterno", SqlDbType.VarChar, 60);
                sqlParam1.Value = pObjBECotizacion.ApePaterno;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@apeMaterno", SqlDbType.VarChar, 60);
                sqlParam2.Value = pObjBECotizacion.ApeMaterno;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@priNombre", SqlDbType.VarChar, 60);
                sqlParam3.Value = pObjBECotizacion.PriNombre;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@segNombre", SqlDbType.VarChar, 60);
                if (pObjBECotizacion.SegNombre == NullTypes.CadenaNull) { sqlParam4.Value = DBNull.Value; } else { sqlParam4.Value = pObjBECotizacion.SegNombre; }

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@idTipoDocumento", SqlDbType.VarChar, 3);
                sqlParam5.Value = pObjBECotizacion.IdTipoDocumento;

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@nroDocumento", SqlDbType.VarChar, 20);
                sqlParam6.Value = pObjBECotizacion.NroDocumento;

                SqlParameter sqlParam7 = sqlCmd.Parameters.Add("@sexo", SqlDbType.VarChar, 1);
                if (pObjBECotizacion.Sexo == NullTypes.CadenaNull) { sqlParam7.Value = DBNull.Value; } else { sqlParam7.Value = pObjBECotizacion.Sexo; }

                SqlParameter sqlParam8 = sqlCmd.Parameters.Add("@fecNacimiento", SqlDbType.SmallDateTime);
                if (pObjBECotizacion.FecNacimiento == NullTypes.FechaNull) { sqlParam8.Value = DBNull.Value; } else { sqlParam8.Value = pObjBECotizacion.FecNacimiento; }

                SqlParameter sqlParam9 = sqlCmd.Parameters.Add("@idCiudad", SqlDbType.Int);
                sqlParam9.Value = pObjBECotizacion.IdCiudad;

                SqlParameter sqlParam10 = sqlCmd.Parameters.Add("@direccion", SqlDbType.VarChar, 300);
                sqlParam10.Value = pObjBECotizacion.Direccion;

                SqlParameter sqlParam11 = sqlCmd.Parameters.Add("@telDomicilio1", SqlDbType.VarChar, 15);
                sqlParam11.Value = pObjBECotizacion.TelDomicilio1;

                SqlParameter sqlParam12 = sqlCmd.Parameters.Add("@telDomicilio2", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelDomicilio2 == NullTypes.CadenaNull) { sqlParam12.Value = DBNull.Value; } else { sqlParam12.Value = pObjBECotizacion.TelDomicilio2; }

                SqlParameter sqlParam13 = sqlCmd.Parameters.Add("@telDomicilio3", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelDomicilio3 == NullTypes.CadenaNull) { sqlParam13.Value = DBNull.Value; } else { sqlParam13.Value = pObjBECotizacion.TelDomicilio3; }

                SqlParameter sqlParam14 = sqlCmd.Parameters.Add("@telMovil1", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelMovil1 == NullTypes.CadenaNull) { sqlParam14.Value = DBNull.Value; } else { sqlParam14.Value = pObjBECotizacion.TelMovil1; }

                SqlParameter sqlParam15 = sqlCmd.Parameters.Add("@telMovil2", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelMovil2 == NullTypes.CadenaNull) { sqlParam15.Value = DBNull.Value; } else { sqlParam15.Value = pObjBECotizacion.TelMovil2; }

                SqlParameter sqlParam16 = sqlCmd.Parameters.Add("@telMovil3", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelMovil3 == NullTypes.CadenaNull) { sqlParam16.Value = DBNull.Value; } else { sqlParam16.Value = pObjBECotizacion.TelMovil3; }

                SqlParameter sqlParam17 = sqlCmd.Parameters.Add("@telOficina1", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelOficina1 == NullTypes.CadenaNull) { sqlParam17.Value = DBNull.Value; } else { sqlParam17.Value = pObjBECotizacion.TelOficina1; }

                SqlParameter sqlParam18 = sqlCmd.Parameters.Add("@telOficina2", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelOficina2 == NullTypes.CadenaNull) { sqlParam18.Value = DBNull.Value; } else { sqlParam18.Value = pObjBECotizacion.TelOficina2; }

                SqlParameter sqlParam19 = sqlCmd.Parameters.Add("@telOficina3", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelOficina3 == NullTypes.CadenaNull) { sqlParam19.Value = DBNull.Value; } else { sqlParam19.Value = pObjBECotizacion.TelOficina3; }

                SqlParameter sqlParam20 = sqlCmd.Parameters.Add("@email1", SqlDbType.VarChar, 50);
                if (pObjBECotizacion.Email1 == NullTypes.CadenaNull) { sqlParam20.Value = DBNull.Value; } else { sqlParam20.Value = pObjBECotizacion.Email1; }

                SqlParameter sqlParam21 = sqlCmd.Parameters.Add("@email2", SqlDbType.VarChar, 50);
                if (pObjBECotizacion.Email2 == NullTypes.CadenaNull) { sqlParam21.Value = DBNull.Value; } else { sqlParam21.Value = pObjBECotizacion.Email2; }

                SqlParameter sqlParam22 = sqlCmd.Parameters.Add("@fecIniVigPoliza", SqlDbType.SmallDateTime);
                if (pObjBECotizacion.FecIniVigPoliza == NullTypes.FechaNull) { sqlParam22.Value = DBNull.Value; } else { sqlParam22.Value = pObjBECotizacion.FecIniVigPoliza; }

                SqlParameter sqlParam23 = sqlCmd.Parameters.Add("@fecFinVigPoliza", SqlDbType.SmallDateTime);
                if (pObjBECotizacion.FecFinVigPoliza == NullTypes.FechaNull) { sqlParam23.Value = DBNull.Value; } else { sqlParam23.Value = pObjBECotizacion.FecFinVigPoliza; }

                SqlParameter sqlParam24 = sqlCmd.Parameters.Add("@idMarca", SqlDbType.Int);
                sqlParam24.Value = pObjBECotizacion.IdMarca;

                SqlParameter sqlParam25 = sqlCmd.Parameters.Add("@idModelo", SqlDbType.Int);
                sqlParam25.Value = pObjBECotizacion.IdModelo;

                SqlParameter sqlParam26 = sqlCmd.Parameters.Add("@idClase", SqlDbType.Int);
                sqlParam26.Value = pObjBECotizacion.IdClase;

                SqlParameter sqlParam27 = sqlCmd.Parameters.Add("@nroPlaca", SqlDbType.VarChar, 20);
                if (pObjBECotizacion.NroPlaca == NullTypes.CadenaNull) { sqlParam27.Value = DBNull.Value; } else { sqlParam27.Value = pObjBECotizacion.NroPlaca; }

                SqlParameter sqlParam28 = sqlCmd.Parameters.Add("@anioFab", SqlDbType.Int);
                sqlParam28.Value = pObjBECotizacion.AnioFab;

                SqlParameter sqlParam29 = sqlCmd.Parameters.Add("@nroMotor", SqlDbType.VarChar, 25);
                sqlParam29.Value = pObjBECotizacion.NroMotor;

                SqlParameter sqlParam30 = sqlCmd.Parameters.Add("@nroSerie", SqlDbType.VarChar, 25);
                if (pObjBECotizacion.NroSerie == NullTypes.CadenaNull) { sqlParam30.Value = DBNull.Value; } else { sqlParam30.Value = pObjBECotizacion.NroSerie; }

                SqlParameter sqlParam31 = sqlCmd.Parameters.Add("@color", SqlDbType.VarChar, 25);
                if (pObjBECotizacion.Color == NullTypes.CadenaNull) { sqlParam31.Value = DBNull.Value; } else { sqlParam31.Value = pObjBECotizacion.Color; }

                SqlParameter sqlParam32 = sqlCmd.Parameters.Add("@idUsoVehiculo", SqlDbType.Int);
                if (pObjBECotizacion.IdUsoVehiculo == NullTypes.IntegerNull) { sqlParam32.Value = DBNull.Value; } else { sqlParam32.Value = pObjBECotizacion.IdUsoVehiculo; }

                SqlParameter sqlParam33 = sqlCmd.Parameters.Add("@nroAsientos", SqlDbType.Int);
                if (pObjBECotizacion.NroAsientos == NullTypes.IntegerNull) { sqlParam33.Value = DBNull.Value; } else { sqlParam33.Value = pObjBECotizacion.NroAsientos; }

                SqlParameter sqlParam34 = sqlCmd.Parameters.Add("@esTimonCambiado", SqlDbType.Bit);
                if (pObjBECotizacion.EsTimonCambiado == NullTypes.BoolNull) { sqlParam34.Value = DBNull.Value; } else { sqlParam34.Value = pObjBECotizacion.EsTimonCambiado; }

                SqlParameter sqlParam35 = sqlCmd.Parameters.Add("@reqGPS", SqlDbType.Bit);
                if (pObjBECotizacion.ReqGPS == NullTypes.BoolNull) { sqlParam35.Value = DBNull.Value; } else { sqlParam35.Value = pObjBECotizacion.ReqGPS; }

                SqlParameter sqlParam36 = sqlCmd.Parameters.Add("@valOriVehiculo", SqlDbType.Decimal);
                sqlParam36.Value = pObjBECotizacion.ValOriVehiculo;

                SqlParameter sqlParam37 = sqlCmd.Parameters.Add("@valVehiculo", SqlDbType.Decimal);
                sqlParam37.Value = pObjBECotizacion.ValVehiculo;

                SqlParameter sqlParam38 = sqlCmd.Parameters.Add("@primaAnual", SqlDbType.Decimal);
                if (pObjBECotizacion.PrimaAnual == NullTypes.DecimalNull) { sqlParam38.Value = DBNull.Value; } else { sqlParam38.Value = pObjBECotizacion.PrimaAnual; }

                SqlParameter sqlParam39 = sqlCmd.Parameters.Add("@tasaAnual", SqlDbType.Decimal);
                if (pObjBECotizacion.TasaAnual == NullTypes.DecimalNull) { sqlParam39.Value = DBNull.Value; } else { sqlParam39.Value = pObjBECotizacion.TasaAnual; }

                SqlParameter sqlParam40 = sqlCmd.Parameters.Add("@primaBianual", SqlDbType.Decimal);
                if (pObjBECotizacion.PrimaBianual == NullTypes.DecimalNull) { sqlParam40.Value = DBNull.Value; } else { sqlParam40.Value = pObjBECotizacion.PrimaBianual; }

                SqlParameter sqlParam41 = sqlCmd.Parameters.Add("@tasaBianual", SqlDbType.Decimal);
                if (pObjBECotizacion.TasaBianual == NullTypes.DecimalNull) { sqlParam41.Value = DBNull.Value; } else { sqlParam41.Value = pObjBECotizacion.TasaBianual; }

                SqlParameter sqlParam42 = sqlCmd.Parameters.Add("@condicion", SqlDbType.Int);
                sqlParam42.Value = pObjBECotizacion.Condicion;

                SqlParameter sqlParam43 = sqlCmd.Parameters.Add("@porcentajeC", SqlDbType.Decimal);
                if (pObjBECotizacion.PorcentajeC == NullTypes.DecimalNull) { sqlParam43.Value = DBNull.Value; } else { sqlParam43.Value = pObjBECotizacion.PorcentajeC; }

                SqlParameter sqlParam44 = sqlCmd.Parameters.Add("@primaAnualC", SqlDbType.Decimal);
                if (pObjBECotizacion.PrimaAnualC == NullTypes.DecimalNull) { sqlParam44.Value = DBNull.Value; } else { sqlParam44.Value = pObjBECotizacion.PrimaAnualC; }

                SqlParameter sqlParam45 = sqlCmd.Parameters.Add("@tasaAnualC", SqlDbType.Decimal);
                if (pObjBECotizacion.TasaAnualC == NullTypes.DecimalNull) { sqlParam45.Value = DBNull.Value; } else { sqlParam45.Value = pObjBECotizacion.TasaAnualC; }

                SqlParameter sqlParam46 = sqlCmd.Parameters.Add("@primaBianualC", SqlDbType.Decimal);
                if (pObjBECotizacion.PrimaBianualC == NullTypes.DecimalNull) { sqlParam46.Value = DBNull.Value; } else { sqlParam46.Value = pObjBECotizacion.PrimaBianualC; }

                SqlParameter sqlParam47 = sqlCmd.Parameters.Add("@tasaBianualC", SqlDbType.Decimal);
                if (pObjBECotizacion.TasaBianualC == NullTypes.DecimalNull) { sqlParam47.Value = DBNull.Value; } else { sqlParam47.Value = pObjBECotizacion.TasaBianualC; }

                SqlParameter sqlParam48 = sqlCmd.Parameters.Add("@idEstado", SqlDbType.Int);
                sqlParam48.Value = pObjBECotizacion.IdEstado;

                SqlParameter sqlParam49 = sqlCmd.Parameters.Add("@reqInspeccion", SqlDbType.Int);
                sqlParam49.Value = pObjBECotizacion.ReqInspeccion;

                SqlParameter sqlParam50 = sqlCmd.Parameters.Add("@dirInspeccion", SqlDbType.VarChar, 300);
                if (pObjBECotizacion.DirInspeccion == NullTypes.CadenaNull) { sqlParam50.Value = DBNull.Value; } else { sqlParam50.Value = pObjBECotizacion.DirInspeccion; }

                SqlParameter sqlParam51 = sqlCmd.Parameters.Add("@fecHorInspeccion", SqlDbType.VarChar, 20);
                if (pObjBECotizacion.FecHorInspeccion == NullTypes.CadenaNull) { sqlParam51.Value = DBNull.Value; } else { sqlParam51.Value = pObjBECotizacion.FecHorInspeccion; }

                SqlParameter sqlParam52 = sqlCmd.Parameters.Add("@fecValidez", SqlDbType.SmallDateTime);
                if (pObjBECotizacion.FecValidez == NullTypes.FechaNull) { sqlParam52.Value = DBNull.Value; } else { sqlParam52.Value = pObjBECotizacion.FecValidez; }

                SqlParameter sqlParam53 = sqlCmd.Parameters.Add("@idCategoria", SqlDbType.Int);
                sqlParam53.Value = pObjBECotizacion.IdCategoria;

                SqlParameter sqlParam54 = sqlCmd.Parameters.Add("@usrCreacion", SqlDbType.VarChar, 50);
                sqlParam54.Value = pObjBECotizacion.UsuarioCreacion;

                SqlParameter sqlParam55 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam55.Value = pObjBECotizacion.IdProducto;

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        }

        public Int32 Registrar(BECotizacion pObjBECotizacion, SqlConnection pSqlCn, SqlTransaction pSqlTrs, ref Decimal pnNroCotizacion)
        {
            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cotizacion_Registrar]", pSqlCn))
            {
                sqlCmd.Transaction = pSqlTrs;
                sqlCmd.CommandType = CommandType.StoredProcedure;


                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@apePaterno", SqlDbType.VarChar, 60);
                sqlParam1.Value = pObjBECotizacion.ApePaterno;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@apeMaterno", SqlDbType.VarChar, 60);
                sqlParam2.Value = pObjBECotizacion.ApeMaterno;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@priNombre", SqlDbType.VarChar, 60);
                sqlParam3.Value = pObjBECotizacion.PriNombre;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@segNombre", SqlDbType.VarChar, 60);
                if (pObjBECotizacion.SegNombre == NullTypes.CadenaNull) { sqlParam4.Value = DBNull.Value; } else { sqlParam4.Value = pObjBECotizacion.SegNombre; }

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@idTipoDocumento", SqlDbType.VarChar, 3);
                sqlParam5.Value = pObjBECotizacion.IdTipoDocumento;

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@nroDocumento", SqlDbType.VarChar, 20);
                sqlParam6.Value = pObjBECotizacion.NroDocumento;

                SqlParameter sqlParam7 = sqlCmd.Parameters.Add("@sexo", SqlDbType.VarChar, 1);
                if (pObjBECotizacion.Sexo == NullTypes.CadenaNull) { sqlParam7.Value = DBNull.Value; } else { sqlParam7.Value = pObjBECotizacion.Sexo; }

                SqlParameter sqlParam8 = sqlCmd.Parameters.Add("@fecNacimiento", SqlDbType.SmallDateTime);
                if (pObjBECotizacion.FecNacimiento == NullTypes.FechaNull) { sqlParam8.Value = DBNull.Value; } else { sqlParam8.Value = pObjBECotizacion.FecNacimiento; }

                SqlParameter sqlParam9 = sqlCmd.Parameters.Add("@idCiudad", SqlDbType.Int);
                sqlParam9.Value = pObjBECotizacion.IdCiudad;

                SqlParameter sqlParam10 = sqlCmd.Parameters.Add("@direccion", SqlDbType.VarChar, 300);
                sqlParam10.Value = pObjBECotizacion.Direccion;

                SqlParameter sqlParam11 = sqlCmd.Parameters.Add("@telDomicilio1", SqlDbType.VarChar, 15);
                sqlParam11.Value = pObjBECotizacion.TelDomicilio1;

                SqlParameter sqlParam12 = sqlCmd.Parameters.Add("@telDomicilio2", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelDomicilio2 == NullTypes.CadenaNull) { sqlParam12.Value = DBNull.Value; } else { sqlParam12.Value = pObjBECotizacion.TelDomicilio2; }

                SqlParameter sqlParam13 = sqlCmd.Parameters.Add("@telDomicilio3", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelDomicilio3 == NullTypes.CadenaNull) { sqlParam13.Value = DBNull.Value; } else { sqlParam13.Value = pObjBECotizacion.TelDomicilio3; }

                SqlParameter sqlParam14 = sqlCmd.Parameters.Add("@telMovil1", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelMovil1 == NullTypes.CadenaNull) { sqlParam14.Value = DBNull.Value; } else { sqlParam14.Value = pObjBECotizacion.TelMovil1; }

                SqlParameter sqlParam15 = sqlCmd.Parameters.Add("@telMovil2", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelMovil2 == NullTypes.CadenaNull) { sqlParam15.Value = DBNull.Value; } else { sqlParam15.Value = pObjBECotizacion.TelMovil2; }

                SqlParameter sqlParam16 = sqlCmd.Parameters.Add("@telMovil3", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelMovil3 == NullTypes.CadenaNull) { sqlParam16.Value = DBNull.Value; } else { sqlParam16.Value = pObjBECotizacion.TelMovil3; }

                SqlParameter sqlParam17 = sqlCmd.Parameters.Add("@telOficina1", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelOficina1 == NullTypes.CadenaNull) { sqlParam17.Value = DBNull.Value; } else { sqlParam17.Value = pObjBECotizacion.TelOficina1; }

                SqlParameter sqlParam18 = sqlCmd.Parameters.Add("@telOficina2", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelOficina2 == NullTypes.CadenaNull) { sqlParam18.Value = DBNull.Value; } else { sqlParam18.Value = pObjBECotizacion.TelOficina2; }

                SqlParameter sqlParam19 = sqlCmd.Parameters.Add("@telOficina3", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelOficina3 == NullTypes.CadenaNull) { sqlParam19.Value = DBNull.Value; } else { sqlParam19.Value = pObjBECotizacion.TelOficina3; }

                SqlParameter sqlParam20 = sqlCmd.Parameters.Add("@email1", SqlDbType.VarChar, 50);
                if (pObjBECotizacion.Email1 == NullTypes.CadenaNull) { sqlParam20.Value = DBNull.Value; } else { sqlParam20.Value = pObjBECotizacion.Email1; }

                SqlParameter sqlParam21 = sqlCmd.Parameters.Add("@email2", SqlDbType.VarChar, 50);
                if (pObjBECotizacion.Email2 == NullTypes.CadenaNull) { sqlParam21.Value = DBNull.Value; } else { sqlParam21.Value = pObjBECotizacion.Email2; }

                SqlParameter sqlParam22 = sqlCmd.Parameters.Add("@fecIniVigPoliza", SqlDbType.SmallDateTime);
                if (pObjBECotizacion.FecIniVigPoliza == NullTypes.FechaNull) { sqlParam22.Value = DBNull.Value; } else { sqlParam22.Value = pObjBECotizacion.FecIniVigPoliza; }

                SqlParameter sqlParam23 = sqlCmd.Parameters.Add("@fecFinVigPoliza", SqlDbType.SmallDateTime);
                if (pObjBECotizacion.FecFinVigPoliza == NullTypes.FechaNull) { sqlParam23.Value = DBNull.Value; } else { sqlParam23.Value = pObjBECotizacion.FecFinVigPoliza; }

                SqlParameter sqlParam24 = sqlCmd.Parameters.Add("@idMarca", SqlDbType.Int);
                sqlParam24.Value = pObjBECotizacion.IdMarca;

                SqlParameter sqlParam25 = sqlCmd.Parameters.Add("@idModelo", SqlDbType.Int);
                sqlParam25.Value = pObjBECotizacion.IdModelo;

                SqlParameter sqlParam26 = sqlCmd.Parameters.Add("@idClase", SqlDbType.Int);
                sqlParam26.Value = pObjBECotizacion.IdClase;

                SqlParameter sqlParam27 = sqlCmd.Parameters.Add("@nroPlaca", SqlDbType.VarChar, 20);
                if (pObjBECotizacion.NroPlaca == NullTypes.CadenaNull) { sqlParam27.Value = DBNull.Value; } else { sqlParam27.Value = pObjBECotizacion.NroPlaca; }

                SqlParameter sqlParam28 = sqlCmd.Parameters.Add("@anioFab", SqlDbType.Int);
                sqlParam28.Value = pObjBECotizacion.AnioFab;

                SqlParameter sqlParam29 = sqlCmd.Parameters.Add("@nroMotor", SqlDbType.VarChar, 25);
                sqlParam29.Value = pObjBECotizacion.NroMotor;

                SqlParameter sqlParam30 = sqlCmd.Parameters.Add("@nroSerie", SqlDbType.VarChar, 25);
                if (pObjBECotizacion.NroSerie == NullTypes.CadenaNull) { sqlParam30.Value = DBNull.Value; } else { sqlParam30.Value = pObjBECotizacion.NroSerie; }

                SqlParameter sqlParam31 = sqlCmd.Parameters.Add("@color", SqlDbType.VarChar, 25);
                if (pObjBECotizacion.Color == NullTypes.CadenaNull) { sqlParam31.Value = DBNull.Value; } else { sqlParam31.Value = pObjBECotizacion.Color; }

                SqlParameter sqlParam32 = sqlCmd.Parameters.Add("@idUsoVehiculo", SqlDbType.Int);
                if (pObjBECotizacion.IdUsoVehiculo == NullTypes.IntegerNull) { sqlParam32.Value = DBNull.Value; } else { sqlParam32.Value = pObjBECotizacion.IdUsoVehiculo; }

                SqlParameter sqlParam33 = sqlCmd.Parameters.Add("@nroAsientos", SqlDbType.Int);
                if (pObjBECotizacion.NroAsientos == NullTypes.IntegerNull) { sqlParam33.Value = DBNull.Value; } else { sqlParam33.Value = pObjBECotizacion.NroAsientos; }

                SqlParameter sqlParam34 = sqlCmd.Parameters.Add("@esTimonCambiado", SqlDbType.Bit);
                if (pObjBECotizacion.EsTimonCambiado == NullTypes.BoolNull) { sqlParam34.Value = DBNull.Value; } else { sqlParam34.Value = pObjBECotizacion.EsTimonCambiado; }

                SqlParameter sqlParam35 = sqlCmd.Parameters.Add("@reqGPS", SqlDbType.Bit);
                if (pObjBECotizacion.ReqGPS == NullTypes.BoolNull) { sqlParam35.Value = DBNull.Value; } else { sqlParam35.Value = pObjBECotizacion.ReqGPS; }

                SqlParameter sqlParam36 = sqlCmd.Parameters.Add("@valOriVehiculo", SqlDbType.Decimal);
                sqlParam36.Value = pObjBECotizacion.ValOriVehiculo;

                SqlParameter sqlParam37 = sqlCmd.Parameters.Add("@valVehiculo", SqlDbType.Decimal);
                sqlParam37.Value = pObjBECotizacion.ValVehiculo;

                SqlParameter sqlParam38 = sqlCmd.Parameters.Add("@primaAnual", SqlDbType.Decimal);
                if (pObjBECotizacion.PrimaAnual == NullTypes.DecimalNull) { sqlParam38.Value = DBNull.Value; } else { sqlParam38.Value = pObjBECotizacion.PrimaAnual; }

                SqlParameter sqlParam39 = sqlCmd.Parameters.Add("@tasaAnual", SqlDbType.Decimal);
                if (pObjBECotizacion.TasaAnual == NullTypes.DecimalNull) { sqlParam39.Value = DBNull.Value; } else { sqlParam39.Value = pObjBECotizacion.TasaAnual; }

                SqlParameter sqlParam40 = sqlCmd.Parameters.Add("@primaBianual", SqlDbType.Decimal);
                if (pObjBECotizacion.PrimaBianual == NullTypes.DecimalNull) { sqlParam40.Value = DBNull.Value; } else { sqlParam40.Value = pObjBECotizacion.PrimaBianual; }

                SqlParameter sqlParam41 = sqlCmd.Parameters.Add("@tasaBianual", SqlDbType.Decimal);
                if (pObjBECotizacion.TasaBianual == NullTypes.DecimalNull) { sqlParam41.Value = DBNull.Value; } else { sqlParam41.Value = pObjBECotizacion.TasaBianual; }

                SqlParameter sqlParam42 = sqlCmd.Parameters.Add("@condicion", SqlDbType.Int);
                sqlParam42.Value = pObjBECotizacion.Condicion;

                SqlParameter sqlParam43 = sqlCmd.Parameters.Add("@porcentajeC", SqlDbType.Decimal);
                if (pObjBECotizacion.PorcentajeC == NullTypes.DecimalNull) { sqlParam43.Value = DBNull.Value; } else { sqlParam43.Value = pObjBECotizacion.PorcentajeC; }

                SqlParameter sqlParam44 = sqlCmd.Parameters.Add("@primaAnualC", SqlDbType.Decimal);
                if (pObjBECotizacion.PrimaAnualC == NullTypes.DecimalNull) { sqlParam44.Value = DBNull.Value; } else { sqlParam44.Value = pObjBECotizacion.PrimaAnualC; }

                SqlParameter sqlParam45 = sqlCmd.Parameters.Add("@tasaAnualC", SqlDbType.Decimal);
                if (pObjBECotizacion.TasaAnualC == NullTypes.DecimalNull) { sqlParam45.Value = DBNull.Value; } else { sqlParam45.Value = pObjBECotizacion.TasaAnualC; }

                SqlParameter sqlParam46 = sqlCmd.Parameters.Add("@primaBianualC", SqlDbType.Decimal);
                if (pObjBECotizacion.PrimaBianualC == NullTypes.DecimalNull) { sqlParam46.Value = DBNull.Value; } else { sqlParam46.Value = pObjBECotizacion.PrimaBianualC; }

                SqlParameter sqlParam47 = sqlCmd.Parameters.Add("@tasaBianualC", SqlDbType.Decimal);
                if (pObjBECotizacion.TasaBianualC == NullTypes.DecimalNull) { sqlParam47.Value = DBNull.Value; } else { sqlParam47.Value = pObjBECotizacion.TasaBianualC; }

                SqlParameter sqlParam48 = sqlCmd.Parameters.Add("@idEstado", SqlDbType.Int);
                sqlParam48.Value = pObjBECotizacion.IdEstado;

                SqlParameter sqlParam49 = sqlCmd.Parameters.Add("@reqInspeccion", SqlDbType.Int);
                sqlParam49.Value = pObjBECotizacion.ReqInspeccion;

                SqlParameter sqlParam50 = sqlCmd.Parameters.Add("@dirInspeccion", SqlDbType.VarChar, 300);
                if (pObjBECotizacion.DirInspeccion == NullTypes.CadenaNull) { sqlParam50.Value = DBNull.Value; } else { sqlParam50.Value = pObjBECotizacion.DirInspeccion; }

                SqlParameter sqlParam51 = sqlCmd.Parameters.Add("@fecHorInspeccion", SqlDbType.VarChar, 20);
                if (pObjBECotizacion.FecHorInspeccion == NullTypes.CadenaNull) { sqlParam51.Value = DBNull.Value; } else { sqlParam51.Value = pObjBECotizacion.FecHorInspeccion; }

                SqlParameter sqlParam52 = sqlCmd.Parameters.Add("@fecValidez", SqlDbType.SmallDateTime);
                if (pObjBECotizacion.FecValidez == NullTypes.FechaNull) { sqlParam52.Value = DBNull.Value; } else { sqlParam52.Value = pObjBECotizacion.FecValidez; }

                SqlParameter sqlParam53 = sqlCmd.Parameters.Add("@idCategoria", SqlDbType.Int);
                sqlParam53.Value = pObjBECotizacion.IdCategoria;

                SqlParameter sqlParam54 = sqlCmd.Parameters.Add("@usrCreacion", SqlDbType.VarChar, 50);
                sqlParam54.Value = pObjBECotizacion.UsuarioCreacion;

                SqlParameter sqlParam55 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam55.Value = pObjBECotizacion.IdProducto;

                SqlParameter sqlParam56 = sqlCmd.Parameters.Add("@nroCotizacion", SqlDbType.Decimal);
                sqlParam56.Direction = ParameterDirection.Output;

                nResult = sqlCmd.ExecuteNonQuery();

                if (nResult > 0) 
                {
                    if (sqlParam56.Value != null)
                    {
                        pnNroCotizacion = Convert.ToDecimal(sqlParam56.Value);
                    }
                }                
            }

            return nResult;
        }

        public Int32 ActualizarValVeh(BECotizacion pObjBECotizacion, SqlConnection pSqlCn)
        {
            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cotizacion_ActualizarValVeh]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idPeriodo", SqlDbType.Int);
                sqlParam1.Value = pObjBECotizacion.IdPeriodo;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@nroCotizacion", SqlDbType.Decimal);
                sqlParam2.Value = pObjBECotizacion.NroCotizacion;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@camPorcentaje", SqlDbType.Decimal);
                sqlParam3.Value = pObjBECotizacion.CamPorcentaje;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@usrModificacion", SqlDbType.VarChar, 50);
                sqlParam4.Value = pObjBECotizacion.UsuarioModificacion;

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        }

        public Int32 RevertirValVeh(BECotizacion pObjBECotizacion, SqlConnection pSqlCn)
        {
            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cotizacion_RevertirValVeh]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idPeriodo", SqlDbType.Int);
                sqlParam1.Value = pObjBECotizacion.IdPeriodo;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@nroCotizacion", SqlDbType.Decimal);
                sqlParam2.Value = pObjBECotizacion.NroCotizacion;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@usrModificacion", SqlDbType.VarChar, 50);
                sqlParam3.Value = pObjBECotizacion.UsuarioModificacion;

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        }

        public Int32 Actualizar(BECotizacion pObjBECotizacion, SqlConnection pSqlCn)
        {
            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cotizacion_Actualizar]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idPeriodo", SqlDbType.Int);
                sqlParam1.Value = pObjBECotizacion.IdPeriodo;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@nroCotizacion", SqlDbType.Decimal);
                sqlParam2.Value = pObjBECotizacion.NroCotizacion;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@apePaterno", SqlDbType.VarChar, 60);
                sqlParam3.Value = pObjBECotizacion.ApePaterno;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@apeMaterno", SqlDbType.VarChar, 60);
                sqlParam4.Value = pObjBECotizacion.ApeMaterno;

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@priNombre", SqlDbType.VarChar, 60);
                sqlParam5.Value = pObjBECotizacion.PriNombre;

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@segNombre", SqlDbType.VarChar, 60);
                sqlParam6.Value = pObjBECotizacion.SegNombre;

                SqlParameter sqlParam7 = sqlCmd.Parameters.Add("@idTipoDocumento", SqlDbType.VarChar, 3);
                sqlParam7.Value = pObjBECotizacion.IdTipoDocumento;

                SqlParameter sqlParam8 = sqlCmd.Parameters.Add("@nroDocumento", SqlDbType.VarChar, 20);
                sqlParam8.Value = pObjBECotizacion.NroDocumento;

                SqlParameter sqlParam9 = sqlCmd.Parameters.Add("@sexo", SqlDbType.VarChar, 1);
                sqlParam9.Value = pObjBECotizacion.Sexo;

                SqlParameter sqlParam10 = sqlCmd.Parameters.Add("@fecNacimiento", SqlDbType.SmallDateTime);
                sqlParam10.Value = pObjBECotizacion.FecNacimiento;

                SqlParameter sqlParam11 = sqlCmd.Parameters.Add("@idCiudad", SqlDbType.Int);
                sqlParam11.Value = pObjBECotizacion.IdCiudad;

                SqlParameter sqlParam12 = sqlCmd.Parameters.Add("@direccion", SqlDbType.VarChar, 300);
                sqlParam12.Value = pObjBECotizacion.Direccion;

                SqlParameter sqlParam13 = sqlCmd.Parameters.Add("@telDomicilio1", SqlDbType.VarChar, 15);
                sqlParam13.Value = pObjBECotizacion.TelDomicilio1;

                SqlParameter sqlParam14 = sqlCmd.Parameters.Add("@telDomicilio2", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelDomicilio2 == NullTypes.CadenaNull) { sqlParam14.Value = DBNull.Value; } else { sqlParam14.Value = pObjBECotizacion.TelDomicilio2; }

                SqlParameter sqlParam15 = sqlCmd.Parameters.Add("@telDomicilio3", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelDomicilio3 == NullTypes.CadenaNull) { sqlParam15.Value = DBNull.Value; } else { sqlParam15.Value = pObjBECotizacion.TelDomicilio3; }

                SqlParameter sqlParam16 = sqlCmd.Parameters.Add("@telMovil1", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelMovil1 == NullTypes.CadenaNull) { sqlParam16.Value = DBNull.Value; } else { sqlParam16.Value = pObjBECotizacion.TelMovil1; }

                SqlParameter sqlParam17 = sqlCmd.Parameters.Add("@telMovil2", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelMovil2 == NullTypes.CadenaNull) { sqlParam17.Value = DBNull.Value; } else { sqlParam17.Value = pObjBECotizacion.TelMovil2; }

                SqlParameter sqlParam18 = sqlCmd.Parameters.Add("@telMovil3", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelMovil3 == NullTypes.CadenaNull) { sqlParam18.Value = DBNull.Value; } else { sqlParam18.Value = pObjBECotizacion.TelMovil3; }

                SqlParameter sqlParam19 = sqlCmd.Parameters.Add("@telOficina1", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelOficina1 == NullTypes.CadenaNull) { sqlParam19.Value = DBNull.Value; } else { sqlParam19.Value = pObjBECotizacion.TelOficina1; }

                SqlParameter sqlParam20 = sqlCmd.Parameters.Add("@telOficina2", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelOficina2 == NullTypes.CadenaNull) { sqlParam20.Value = DBNull.Value; } else { sqlParam20.Value = pObjBECotizacion.TelOficina2; }

                SqlParameter sqlParam21 = sqlCmd.Parameters.Add("@telOficina3", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelOficina3 == NullTypes.CadenaNull) { sqlParam21.Value = DBNull.Value; } else { sqlParam21.Value = pObjBECotizacion.TelOficina3; }

                SqlParameter sqlParam22 = sqlCmd.Parameters.Add("@email1", SqlDbType.VarChar, 50);
                if (pObjBECotizacion.Email1 == NullTypes.CadenaNull) { sqlParam22.Value = DBNull.Value; } else { sqlParam22.Value = pObjBECotizacion.Email1; }

                SqlParameter sqlParam23 = sqlCmd.Parameters.Add("@email2", SqlDbType.VarChar, 50);
                if (pObjBECotizacion.Email2 == NullTypes.CadenaNull) { sqlParam23.Value = DBNull.Value; } else { sqlParam23.Value = pObjBECotizacion.Email2; }

                SqlParameter sqlParam24 = sqlCmd.Parameters.Add("@nroPlaca", SqlDbType.VarChar, 20);
                sqlParam24.Value = pObjBECotizacion.NroPlaca;

                SqlParameter sqlParam25 = sqlCmd.Parameters.Add("@nroSerie", SqlDbType.VarChar, 25);
                sqlParam25.Value = pObjBECotizacion.NroSerie;

                SqlParameter sqlParam26 = sqlCmd.Parameters.Add("@color", SqlDbType.VarChar, 25);
                sqlParam26.Value = pObjBECotizacion.Color;

                SqlParameter sqlParam27 = sqlCmd.Parameters.Add("@idUsoVehiculo", SqlDbType.Int);
                if (pObjBECotizacion.IdUsoVehiculo == NullTypes.IntegerNull) { sqlParam27.Value = DBNull.Value; } else { sqlParam27.Value = pObjBECotizacion.IdUsoVehiculo; }

                SqlParameter sqlParam28 = sqlCmd.Parameters.Add("@nroAsientos", SqlDbType.Int);
                sqlParam28.Value = pObjBECotizacion.NroAsientos;

                SqlParameter sqlParam29 = sqlCmd.Parameters.Add("@esTimonCambiado", SqlDbType.Bit);
                if (pObjBECotizacion.EsTimonCambiado == NullTypes.BoolNull) { sqlParam29.Value = DBNull.Value; } else { sqlParam29.Value = pObjBECotizacion.EsTimonCambiado; }

                SqlParameter sqlParam30 = sqlCmd.Parameters.Add("@reqGPS", SqlDbType.Bit);
                if (pObjBECotizacion.ReqGPS == NullTypes.BoolNull) { sqlParam30.Value = DBNull.Value; } else { sqlParam30.Value = pObjBECotizacion.ReqGPS; }

                SqlParameter sqlParam31 = sqlCmd.Parameters.Add("@usrModificacion", SqlDbType.VarChar, 50);
                sqlParam31.Value = pObjBECotizacion.UsuarioModificacion;

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        }

        public Int32 ActualizarRechazo(BECotizacion pObjBECotizacion, SqlConnection pSqlCn)
        {
            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cotizacion_ActualizarRechazo]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idPeriodo", SqlDbType.Int);
                sqlParam1.Value = pObjBECotizacion.IdPeriodo;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@nroCotizacion", SqlDbType.Decimal);
                sqlParam2.Value = pObjBECotizacion.NroCotizacion;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@idTipRechazo", SqlDbType.Int);
                if (pObjBECotizacion.IdTipRechazo == NullTypes.IntegerNull) { sqlParam3.Value = DBNull.Value; } else { sqlParam3.Value = pObjBECotizacion.IdTipRechazo; }

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@obsTipRechazo", SqlDbType.VarChar, 350);
                if (pObjBECotizacion.ObsTipRechazo == NullTypes.CadenaNull) { sqlParam4.Value = DBNull.Value; } else { sqlParam4.Value = pObjBECotizacion.ObsTipRechazo; }

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@usrRegRechazo", SqlDbType.VarChar, 50);
                if (pObjBECotizacion.UsrRegRechazo == NullTypes.CadenaNull) { sqlParam5.Value = DBNull.Value; } else { sqlParam5.Value = pObjBECotizacion.UsrRegRechazo; }

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        }

        public Int32 ActualizarCliente(BECotizacion pObjBECotizacion, SqlConnection pSqlCn)
        {
            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cotizacion_ActualizarCli]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idPeriodo", SqlDbType.Int);
                sqlParam1.Value = pObjBECotizacion.IdPeriodo;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@nroCotizacion", SqlDbType.Decimal);
                sqlParam2.Value = pObjBECotizacion.NroCotizacion;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@apePaterno", SqlDbType.VarChar, 60);
                sqlParam3.Value = pObjBECotizacion.ApePaterno;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@apeMaterno", SqlDbType.VarChar, 60);
                sqlParam4.Value = pObjBECotizacion.ApeMaterno;

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@priNombre", SqlDbType.VarChar, 60);
                sqlParam5.Value = pObjBECotizacion.PriNombre;

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@segNombre", SqlDbType.VarChar, 60);
                sqlParam6.Value = pObjBECotizacion.SegNombre;

                SqlParameter sqlParam7 = sqlCmd.Parameters.Add("@idTipoDocumento", SqlDbType.VarChar, 3);
                sqlParam7.Value = pObjBECotizacion.IdTipoDocumento;

                SqlParameter sqlParam8 = sqlCmd.Parameters.Add("@nroDocumento", SqlDbType.VarChar, 20);
                sqlParam8.Value = pObjBECotizacion.NroDocumento;

                SqlParameter sqlParam9 = sqlCmd.Parameters.Add("@direccion", SqlDbType.VarChar, 300);
                sqlParam9.Value = pObjBECotizacion.Direccion;

                SqlParameter sqlParam10 = sqlCmd.Parameters.Add("@telDomicilio1", SqlDbType.VarChar, 15);
                sqlParam10.Value = pObjBECotizacion.TelDomicilio1;

                SqlParameter sqlParam11 = sqlCmd.Parameters.Add("@telDomicilio2", SqlDbType.VarChar, 15);
                sqlParam11.Value = pObjBECotizacion.TelDomicilio2;

                SqlParameter sqlParam12 = sqlCmd.Parameters.Add("@telDomicilio3", SqlDbType.VarChar, 15);
                sqlParam12.Value = pObjBECotizacion.TelDomicilio2;

                SqlParameter sqlParam13 = sqlCmd.Parameters.Add("@telMovil1", SqlDbType.VarChar, 15);
                sqlParam13.Value = pObjBECotizacion.TelMovil1;

                SqlParameter sqlParam14 = sqlCmd.Parameters.Add("@telMovil2", SqlDbType.VarChar, 15);
                sqlParam14.Value = pObjBECotizacion.TelMovil2;

                SqlParameter sqlParam15 = sqlCmd.Parameters.Add("@telMovil3", SqlDbType.VarChar, 15);
                sqlParam15.Value = pObjBECotizacion.TelMovil3;

                SqlParameter sqlParam16 = sqlCmd.Parameters.Add("@telOficina1", SqlDbType.VarChar, 15);
                sqlParam16.Value = pObjBECotizacion.TelOficina1;

                SqlParameter sqlParam17 = sqlCmd.Parameters.Add("@telOficina2", SqlDbType.VarChar, 15);
                sqlParam17.Value = pObjBECotizacion.TelOficina2;

                SqlParameter sqlParam18 = sqlCmd.Parameters.Add("@telOficina3", SqlDbType.VarChar, 15);
                sqlParam18.Value = pObjBECotizacion.TelOficina3;

                SqlParameter sqlParam19 = sqlCmd.Parameters.Add("@email1", SqlDbType.VarChar, 50);
                sqlParam19.Value = pObjBECotizacion.Email1;

                SqlParameter sqlParam20 = sqlCmd.Parameters.Add("@email2", SqlDbType.VarChar, 50);
                sqlParam20.Value = pObjBECotizacion.Email2;

                SqlParameter sqlParam21 = sqlCmd.Parameters.Add("@usrModificacion", SqlDbType.VarChar, 50);
                sqlParam21.Value = pObjBECotizacion.UsuarioModificacion;

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        }

        public Int32 ActualizarVehiculo(BECotizacion pObjBECotizacion, SqlConnection pSqlCn)
        {
            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cotizacion_ActualizarVeh]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idPeriodo", SqlDbType.Int);
                sqlParam1.Value = pObjBECotizacion.IdPeriodo;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@nroCotizacion", SqlDbType.Decimal);
                sqlParam2.Value = pObjBECotizacion.NroCotizacion;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@nroPlaca", SqlDbType.VarChar, 20);
                sqlParam3.Value = pObjBECotizacion.NroPlaca;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@color", SqlDbType.VarChar, 25);
                sqlParam4.Value = pObjBECotizacion.Color;

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@nroSerie", SqlDbType.VarChar, 25);
                sqlParam5.Value = pObjBECotizacion.NroSerie;

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@nroAsientos", SqlDbType.Int);
                sqlParam6.Value = pObjBECotizacion.NroAsientos;

                SqlParameter sqlParam7 = sqlCmd.Parameters.Add("@usrModificacion", SqlDbType.VarChar, 50);
                sqlParam7.Value = pObjBECotizacion.UsuarioModificacion;

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        }

        public Int32 Expedir(BECotizacion pObjBECotizacion, SqlConnection pSqlCn)
        {
            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cotizacion_Expedir]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idPedriodo", SqlDbType.Int);
                sqlParam1.Value = pObjBECotizacion.IdPeriodo;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@nroCotizacion", SqlDbType.Decimal);
                sqlParam2.Value = pObjBECotizacion.NroCotizacion;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@usuario", SqlDbType.VarChar, 50);
                sqlParam3.Value = pObjBECotizacion.UsuarioModificacion;

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        }

        public SqlDataReader Cotizar(Int32 pnIdProducto, Int32 pnIdModelo, Int32 pnAnioFab, Decimal pnValorVeh, Decimal pnTasaAnual, Decimal pnTasaBianual, Boolean pbTimon, String pcScoring, Decimal pnPorc, Decimal pnPrimaMin, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cotizacion_Cotizar]", pSqlCn)) 
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                SqlParameter sqlPara2 = sqlCmd.Parameters.Add("@idModelo", SqlDbType.Int);
                sqlPara2.Value = pnIdModelo;

                SqlParameter sqlPara3 = sqlCmd.Parameters.Add("@anioFab", SqlDbType.Int);
                sqlPara3.Value = pnAnioFab;

                SqlParameter sqlPara4 = sqlCmd.Parameters.Add("@valorVeh", SqlDbType.Decimal);
                sqlPara4.Value = pnValorVeh;

                SqlParameter sqlPara5 = sqlCmd.Parameters.Add("@tasaAnual", SqlDbType.Decimal);                
                if (pnTasaAnual == NullTypes.DecimalNull) { sqlPara5.Value = DBNull.Value; } else { sqlPara5.Value = pnTasaAnual; } 

                SqlParameter sqlPara6 = sqlCmd.Parameters.Add("@tasaBianual", SqlDbType.Decimal);
                if (pnTasaBianual == NullTypes.DecimalNull) { sqlPara6.Value = DBNull.Value; } else { sqlPara6.Value = pnTasaBianual; }

                SqlParameter sqlParam7 = sqlCmd.Parameters.Add("@timonCambiado", SqlDbType.Bit);
                sqlParam7.Value = pbTimon;

                SqlParameter sqlParam8 = sqlCmd.Parameters.Add("@Scoring", SqlDbType.VarChar, 1);
                if (pcScoring == NullTypes.CadenaNull || pcScoring == "-1" || pcScoring == null) { sqlParam8.Value = DBNull.Value; } else { sqlParam8.Value = pcScoring; }

                SqlParameter sqlParam9 = sqlCmd.Parameters.Add("@porcentaje", SqlDbType.Decimal);
                sqlParam9.Value = pnPorc;

                SqlParameter sqlParam10 = sqlCmd.Parameters.Add("@primaMinima", SqlDbType.Decimal);
                if (pnPrimaMin == NullTypes.DecimalNull) { sqlParam10.Value = DBNull.Value; } else { sqlParam10.Value = pnPrimaMin; }

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);  
            }

            return sqlDr;
        }

        public SqlDataReader Cotizar(Int32 pnIdProducto, Int32 pnIdModelo, Int32 pnAnioFab, Decimal pnValorVeh, Boolean pbTimon, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cotizacion_Cotizar]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                SqlParameter sqlPara2 = sqlCmd.Parameters.Add("@idModelo", SqlDbType.Int);
                sqlPara2.Value = pnIdModelo;

                SqlParameter sqlPara3 = sqlCmd.Parameters.Add("@anioFab", SqlDbType.Int);
                sqlPara3.Value = pnAnioFab;

                SqlParameter sqlPara4 = sqlCmd.Parameters.Add("@valorVeh", SqlDbType.Decimal);
                sqlPara4.Value = pnValorVeh;
                
                SqlParameter sqlParam7 = sqlCmd.Parameters.Add("@timonCambiado", SqlDbType.Bit);
                sqlParam7.Value = pbTimon;
                                               
                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }


        public SqlDataReader CotizarDEMO(Int32 pnIdProducto, Int32 pnIdModelo, Int32 pnAnioFab, Decimal pnValorVeh, Decimal pnTasaAnual, Decimal pnTasaBianual, Boolean pbTimon, String pcScoring, Decimal pnPorc, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cotizacion_CotizarDEMO]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                SqlParameter sqlPara2 = sqlCmd.Parameters.Add("@idModelo", SqlDbType.Int);
                sqlPara2.Value = pnIdModelo;

                SqlParameter sqlPara3 = sqlCmd.Parameters.Add("@anioFab", SqlDbType.Int);
                sqlPara3.Value = pnAnioFab;

                SqlParameter sqlPara4 = sqlCmd.Parameters.Add("@valorVeh", SqlDbType.Decimal);
                sqlPara4.Value = pnValorVeh;

                SqlParameter sqlPara5 = sqlCmd.Parameters.Add("@tasaAnual", SqlDbType.Decimal);
                if (pnTasaAnual == NullTypes.DecimalNull) { sqlPara5.Value = DBNull.Value; } else { sqlPara5.Value = pnTasaAnual; }

                SqlParameter sqlPara6 = sqlCmd.Parameters.Add("@tasaBianual", SqlDbType.Decimal);
                if (pnTasaBianual == NullTypes.DecimalNull) { sqlPara6.Value = DBNull.Value; } else { sqlPara6.Value = pnTasaBianual; }

                SqlParameter sqlParam7 = sqlCmd.Parameters.Add("@timonCambiado", SqlDbType.Bit);
                sqlParam7.Value = pbTimon;

                SqlParameter sqlParam8 = sqlCmd.Parameters.Add("@Scoring", SqlDbType.VarChar, 1);
                if (pcScoring == NullTypes.CadenaNull || pcScoring == "-1" || pcScoring == null) { sqlParam8.Value = DBNull.Value; } else { sqlParam8.Value = pcScoring; }

                SqlParameter sqlParam9 = sqlCmd.Parameters.Add("@porcentaje", SqlDbType.Decimal);
                sqlParam9.Value = pnPorc;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }
        #endregion

        #region Cotizacion RIMAC
        public SqlDataReader GenerarScoringRIMAC(Int32 pnIdProducto, String pcNroMotor, Decimal pnMontoSiniestro, SqlConnection pSqlcn) 
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cotizacion_GenerarScoringRIMAC]", pSqlcn)) 
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@nroMotor", SqlDbType.VarChar, 25);
                sqlParam2.Value = pcNroMotor;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@montoSiniestro", SqlDbType.Decimal);
                sqlParam3.Value = pnMontoSiniestro;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);
            }

            return sqlDr;
        }

        public Int32 InsertarRIMAC(BECotizacion pObjBECotizacion, SqlConnection pSqlCn) 
        {
            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cotizacion_InsertarRIMAC]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@apePaterno", SqlDbType.VarChar, 60);
                sqlParam1.Value = pObjBECotizacion.ApePaterno;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@apeMaterno", SqlDbType.VarChar, 60);
                sqlParam2.Value = pObjBECotizacion.ApeMaterno;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@priNombre", SqlDbType.VarChar, 60);
                sqlParam3.Value = pObjBECotizacion.PriNombre;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@segNombre", SqlDbType.VarChar, 60);
                if (pObjBECotizacion.SegNombre == NullTypes.CadenaNull) { sqlParam4.Value = DBNull.Value; } else { sqlParam4.Value = pObjBECotizacion.SegNombre; }

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@idTipoDocumento", SqlDbType.VarChar, 3);
                sqlParam5.Value = pObjBECotizacion.IdTipoDocumento;

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@nroDocumento", SqlDbType.VarChar, 20);
                sqlParam6.Value = pObjBECotizacion.NroDocumento;

                SqlParameter sqlParam7 = sqlCmd.Parameters.Add("@sexo", SqlDbType.VarChar, 1);
                if (pObjBECotizacion.Sexo == NullTypes.CadenaNull) { sqlParam7.Value = DBNull.Value; } else { sqlParam7.Value = pObjBECotizacion.Sexo; }

                SqlParameter sqlParam8 = sqlCmd.Parameters.Add("@fecNacimiento", SqlDbType.SmallDateTime);
                if (pObjBECotizacion.FecNacimiento == NullTypes.FechaNull) { sqlParam8.Value = DBNull.Value; } else { sqlParam8.Value = pObjBECotizacion.FecNacimiento; }

                SqlParameter sqlParam9 = sqlCmd.Parameters.Add("@idCiudad", SqlDbType.Int);
                sqlParam9.Value = pObjBECotizacion.IdCiudad;

                SqlParameter sqlParam10 = sqlCmd.Parameters.Add("@direccion", SqlDbType.VarChar, 300);
                sqlParam10.Value = pObjBECotizacion.Direccion;

                SqlParameter sqlParam11 = sqlCmd.Parameters.Add("@telDomicilio1", SqlDbType.VarChar, 15);
                sqlParam11.Value = pObjBECotizacion.TelDomicilio1;

                SqlParameter sqlParam12 = sqlCmd.Parameters.Add("@telDomicilio2", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelDomicilio2 == NullTypes.CadenaNull) { sqlParam12.Value = DBNull.Value; } else { sqlParam12.Value = pObjBECotizacion.TelDomicilio2; }

                SqlParameter sqlParam13 = sqlCmd.Parameters.Add("@telDomicilio3", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelDomicilio3 == NullTypes.CadenaNull) { sqlParam13.Value = DBNull.Value; } else { sqlParam13.Value = pObjBECotizacion.TelDomicilio3; }

                SqlParameter sqlParam14 = sqlCmd.Parameters.Add("@telMovil1", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelMovil1 == NullTypes.CadenaNull) { sqlParam14.Value = DBNull.Value; } else { sqlParam14.Value = pObjBECotizacion.TelMovil1; }

                SqlParameter sqlParam15 = sqlCmd.Parameters.Add("@telMovil2", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelMovil2 == NullTypes.CadenaNull) { sqlParam15.Value = DBNull.Value; } else { sqlParam15.Value = pObjBECotizacion.TelMovil2; }

                SqlParameter sqlParam16 = sqlCmd.Parameters.Add("@telMovil3", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelMovil3 == NullTypes.CadenaNull) { sqlParam16.Value = DBNull.Value; } else { sqlParam16.Value = pObjBECotizacion.TelMovil3; }

                SqlParameter sqlParam17 = sqlCmd.Parameters.Add("@telOficina1", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelOficina1 == NullTypes.CadenaNull) { sqlParam17.Value = DBNull.Value; } else { sqlParam17.Value = pObjBECotizacion.TelOficina1; }

                SqlParameter sqlParam18 = sqlCmd.Parameters.Add("@telOficina2", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelOficina2 == NullTypes.CadenaNull) { sqlParam18.Value = DBNull.Value; } else { sqlParam18.Value = pObjBECotizacion.TelOficina2; }

                SqlParameter sqlParam19 = sqlCmd.Parameters.Add("@telOficina3", SqlDbType.VarChar, 15);
                if (pObjBECotizacion.TelOficina3 == NullTypes.CadenaNull) { sqlParam19.Value = DBNull.Value; } else { sqlParam19.Value = pObjBECotizacion.TelOficina3; }

                SqlParameter sqlParam20 = sqlCmd.Parameters.Add("@email1", SqlDbType.VarChar, 50);
                if (pObjBECotizacion.Email1 == NullTypes.CadenaNull) { sqlParam20.Value = DBNull.Value; } else { sqlParam20.Value = pObjBECotizacion.Email1; }

                SqlParameter sqlParam21 = sqlCmd.Parameters.Add("@email2", SqlDbType.VarChar, 50);
                if (pObjBECotizacion.Email2 == NullTypes.CadenaNull) { sqlParam21.Value = DBNull.Value; } else { sqlParam21.Value = pObjBECotizacion.Email2; }

                SqlParameter sqlParam22 = sqlCmd.Parameters.Add("@fecIniVigPoliza", SqlDbType.SmallDateTime);
                if (pObjBECotizacion.FecIniVigPoliza == NullTypes.FechaNull) { sqlParam22.Value = DBNull.Value; } else { sqlParam22.Value = pObjBECotizacion.FecIniVigPoliza; }

                SqlParameter sqlParam23 = sqlCmd.Parameters.Add("@fecFinVigPoliza", SqlDbType.SmallDateTime);
                if (pObjBECotizacion.FecFinVigPoliza == NullTypes.FechaNull) { sqlParam23.Value = DBNull.Value; } else { sqlParam23.Value = pObjBECotizacion.FecFinVigPoliza; }

                SqlParameter sqlParam24 = sqlCmd.Parameters.Add("@idMarca", SqlDbType.Int);
                sqlParam24.Value = pObjBECotizacion.IdMarca;

                SqlParameter sqlParam25 = sqlCmd.Parameters.Add("@idModelo", SqlDbType.Int);
                sqlParam25.Value = pObjBECotizacion.IdModelo;

                SqlParameter sqlParam26 = sqlCmd.Parameters.Add("@idClase", SqlDbType.Int);
                sqlParam26.Value = pObjBECotizacion.IdClase;

                SqlParameter sqlParam27 = sqlCmd.Parameters.Add("@nroPlaca", SqlDbType.VarChar, 20);
                if (pObjBECotizacion.NroPlaca == NullTypes.CadenaNull) { sqlParam27.Value = DBNull.Value; } else { sqlParam27.Value = pObjBECotizacion.NroPlaca; }

                SqlParameter sqlParam28 = sqlCmd.Parameters.Add("@anioFab", SqlDbType.Int);
                sqlParam28.Value = pObjBECotizacion.AnioFab;

                SqlParameter sqlParam29 = sqlCmd.Parameters.Add("@nroMotor", SqlDbType.VarChar, 25);
                sqlParam29.Value = pObjBECotizacion.NroMotor;

                SqlParameter sqlParam30 = sqlCmd.Parameters.Add("@nroSerie", SqlDbType.VarChar, 25);
                if (pObjBECotizacion.NroSerie == NullTypes.CadenaNull) { sqlParam30.Value = DBNull.Value; } else { sqlParam30.Value = pObjBECotizacion.NroSerie; }

                SqlParameter sqlParam31 = sqlCmd.Parameters.Add("@color", SqlDbType.VarChar, 25);
                if (pObjBECotizacion.Color == NullTypes.CadenaNull) { sqlParam31.Value = DBNull.Value; } else { sqlParam31.Value = pObjBECotizacion.Color; }

                SqlParameter sqlParam32 = sqlCmd.Parameters.Add("@idUsoVehiculo", SqlDbType.Int);
                if (pObjBECotizacion.IdUsoVehiculo == NullTypes.IntegerNull) { sqlParam32.Value = DBNull.Value; } else { sqlParam32.Value = pObjBECotizacion.IdUsoVehiculo; }

                SqlParameter sqlParam33 = sqlCmd.Parameters.Add("@nroAsientos", SqlDbType.Int);
                if (pObjBECotizacion.NroAsientos == NullTypes.IntegerNull) { sqlParam33.Value = DBNull.Value; } else { sqlParam33.Value = pObjBECotizacion.NroAsientos; }

                SqlParameter sqlParam34 = sqlCmd.Parameters.Add("@esTimonCambiado", SqlDbType.Bit);
                if (pObjBECotizacion.EsTimonCambiado == NullTypes.BoolNull) { sqlParam34.Value = DBNull.Value; } else { sqlParam34.Value = pObjBECotizacion.EsTimonCambiado; }

                SqlParameter sqlParam35 = sqlCmd.Parameters.Add("@reqGPS", SqlDbType.Bit);
                if (pObjBECotizacion.ReqGPS == NullTypes.BoolNull) { sqlParam35.Value = DBNull.Value; } else { sqlParam35.Value = pObjBECotizacion.ReqGPS; }

                SqlParameter sqlParam36 = sqlCmd.Parameters.Add("@valOriVehiculo", SqlDbType.Decimal);
                sqlParam36.Value = pObjBECotizacion.ValOriVehiculo;

                SqlParameter sqlParam37 = sqlCmd.Parameters.Add("@valVehiculo", SqlDbType.Decimal);
                sqlParam37.Value = pObjBECotizacion.ValVehiculo;

                //SqlParameter sqlParam38 = sqlCmd.Parameters.Add("@primaAnual", SqlDbType.Decimal);
                //if (pObjBECotizacion.PrimaAnual == NullTypes.DecimalNull) { sqlParam38.Value = DBNull.Value; } else { sqlParam38.Value = pObjBECotizacion.PrimaAnual; }

                SqlParameter sqlParam38 = sqlCmd.Parameters.Add("@tasaAnual", SqlDbType.Decimal);
                if (pObjBECotizacion.TasaAnual == NullTypes.DecimalNull) { sqlParam38.Value = DBNull.Value; } else { sqlParam38.Value = pObjBECotizacion.TasaAnual; }

                //SqlParameter sqlParam40 = sqlCmd.Parameters.Add("@primaBianual", SqlDbType.Decimal);
                //if (pObjBECotizacion.PrimaBianual == NullTypes.DecimalNull) { sqlParam40.Value = DBNull.Value; } else { sqlParam40.Value = pObjBECotizacion.PrimaBianual; }

                SqlParameter sqlParam39 = sqlCmd.Parameters.Add("@tasaBianual", SqlDbType.Decimal);
                if (pObjBECotizacion.TasaBianual == NullTypes.DecimalNull) { sqlParam39.Value = DBNull.Value; } else { sqlParam39.Value = pObjBECotizacion.TasaBianual; }

                SqlParameter sqlParam40 = sqlCmd.Parameters.Add("@condicion", SqlDbType.Int);
                sqlParam40.Value = pObjBECotizacion.Condicion;

                SqlParameter sqlParam41 = sqlCmd.Parameters.Add("@porcentajeC", SqlDbType.Decimal);
                if (pObjBECotizacion.PorcentajeC == NullTypes.DecimalNull) { sqlParam41.Value = DBNull.Value; } else { sqlParam41.Value = pObjBECotizacion.PorcentajeC; }

                //SqlParameter sqlParam44 = sqlCmd.Parameters.Add("@primaAnualC", SqlDbType.Decimal);
                //if (pObjBECotizacion.PrimaAnualC == NullTypes.DecimalNull) { sqlParam44.Value = DBNull.Value; } else { sqlParam44.Value = pObjBECotizacion.PrimaAnualC; }

                //SqlParameter sqlParam45 = sqlCmd.Parameters.Add("@tasaAnualC", SqlDbType.Decimal);
                //if (pObjBECotizacion.TasaAnualC == NullTypes.DecimalNull) { sqlParam45.Value = DBNull.Value; } else { sqlParam45.Value = pObjBECotizacion.TasaAnualC; }

                //SqlParameter sqlParam46 = sqlCmd.Parameters.Add("@primaBianualC", SqlDbType.Decimal);
                //if (pObjBECotizacion.PrimaBianualC == NullTypes.DecimalNull) { sqlParam46.Value = DBNull.Value; } else { sqlParam46.Value = pObjBECotizacion.PrimaBianualC; }

                //SqlParameter sqlParam47 = sqlCmd.Parameters.Add("@tasaBianualC", SqlDbType.Decimal);
                //if (pObjBECotizacion.TasaBianualC == NullTypes.DecimalNull) { sqlParam47.Value = DBNull.Value; } else { sqlParam47.Value = pObjBECotizacion.TasaBianualC; }

                SqlParameter sqlParam42 = sqlCmd.Parameters.Add("@idEstado", SqlDbType.Int);
                sqlParam42.Value = pObjBECotizacion.IdEstado;

                SqlParameter sqlParam43 = sqlCmd.Parameters.Add("@reqInspeccion", SqlDbType.Int);
                sqlParam43.Value = pObjBECotizacion.ReqInspeccion;

                SqlParameter sqlParam44 = sqlCmd.Parameters.Add("@dirInspeccion", SqlDbType.VarChar, 300);
                if (pObjBECotizacion.DirInspeccion == NullTypes.CadenaNull) { sqlParam44.Value = DBNull.Value; } else { sqlParam44.Value = pObjBECotizacion.DirInspeccion; }

                SqlParameter sqlParam45 = sqlCmd.Parameters.Add("@fecHorInspeccion", SqlDbType.VarChar, 20);
                if (pObjBECotizacion.FecHorInspeccion == NullTypes.CadenaNull) { sqlParam45.Value = DBNull.Value; } else { sqlParam45.Value = pObjBECotizacion.FecHorInspeccion; }

                SqlParameter sqlParam46 = sqlCmd.Parameters.Add("@fecValidez", SqlDbType.SmallDateTime);
                if (pObjBECotizacion.FecValidez == NullTypes.FechaNull) { sqlParam46.Value = DBNull.Value; } else { sqlParam46.Value = pObjBECotizacion.FecValidez; }

                SqlParameter sqlParam47 = sqlCmd.Parameters.Add("@idCategoria", SqlDbType.Int);
                sqlParam47.Value = pObjBECotizacion.IdCategoria;

                SqlParameter sqlParam48 = sqlCmd.Parameters.Add("@usrCreacion", SqlDbType.VarChar, 50);
                sqlParam48.Value = pObjBECotizacion.UsuarioCreacion;

                SqlParameter sqlParam49 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam49.Value = pObjBECotizacion.IdProducto;

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        } 
        #endregion
    }
}
