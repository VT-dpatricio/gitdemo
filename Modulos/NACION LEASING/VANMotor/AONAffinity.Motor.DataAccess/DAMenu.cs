﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;   
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessEntity;  

namespace AONAffinity.Motor.DataAccess
{
    public class DAMenu
    {
        #region NoTransaccional
        public SqlDataReader Listar(Int32 pIDSistema,Int32 pIDEntidad,String pcIdUsuario, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("BW_ListarMenuSis", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@IDSistema", SqlDbType.Int).Value = pIDSistema;
                sqlCmd.Parameters.Add("@IDEntidad", SqlDbType.Int).Value = pIDEntidad;
                sqlCmd.Parameters.Add("@IDUsuario", SqlDbType.VarChar, 50).Value = pcIdUsuario;
                /*SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@IDUsuario", SqlDbType.VarChar, 50);
                sqlParam1.Value = pcIdUsuario;*/
                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }            
        #endregion
    }
}
