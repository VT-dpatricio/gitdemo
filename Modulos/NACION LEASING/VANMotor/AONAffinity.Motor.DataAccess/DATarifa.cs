﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.DataAccess
{
    public class DATarifa
    {
        #region NoTransaccional
        public SqlDataReader Obtener(Int32 pnIdProducto, Int32 pnIdModelo, Int32 pnAnioFab, Int16 pnNroAsientos, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Tarifa_Obtener]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idModelo", SqlDbType.Int);
                sqlParam2.Value = pnIdModelo;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@anioFac", SqlDbType.Int);
                sqlParam3.Value = pnAnioFab;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@nroAsientos", SqlDbType.SmallInt);
                sqlParam4.Value = pnNroAsientos;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }
        #endregion

        #region Antiguo
        //public SqlDataReader Obtener(Int32 pnIdModelo, Int32 pnIdAnio, SqlConnection pSqlCn)
        //{

        //    SqlDataReader sqlDr;

        //    using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Tarifa_Obtener]", pSqlCn))
        //    {
        //        sqlCmd.CommandType = CommandType.StoredProcedure;

        //        SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idModelo", SqlDbType.Int);
        //        sqlParam1.Value = pnIdModelo;

        //        SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idAnio", SqlDbType.Int);
        //        sqlParam2.Value = pnIdAnio;

        //        sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);
        //    }

        //    return sqlDr;
        //}
        #endregion
    }
}
