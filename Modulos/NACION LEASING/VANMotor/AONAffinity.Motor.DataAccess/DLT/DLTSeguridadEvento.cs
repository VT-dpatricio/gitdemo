﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using AONAffinity.Motor.BusinessEntity;
using System.Collections;
using System.Data;


namespace AONAffinity.Motor.DataAccess.DLT
{
    public class DLTSeguridadEvento : DLBase, IDLT
    {

        public bool Actualizar(BusinessEntity.BEBase pEntidad)
        {
            throw new NotImplementedException();
        }

        public bool Eliminar(int pCodigo)
        {
            throw new NotImplementedException();
        }

        public bool Insertar(BusinessEntity.BEBase pEntidad)
        {
            SqlConnection cn = new SqlConnection(base.CadenaConexion());
            SqlCommand cm = new SqlCommand("SE_InsertarEvento", cn);
            cm.CommandType = CommandType.StoredProcedure;
            cm = this.LlenarEstructura(pEntidad, cm, "I");
            bool resultado = false;
            try
            {
                cn.Open();
                cm.ExecuteNonQuery();
                resultado = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if ((cn.State == ConnectionState.Open))
                {
                    cn.Close();
                }
            }
            return resultado;
        }

        public System.Data.SqlClient.SqlCommand LlenarEstructura(BusinessEntity.BEBase pEntidad, System.Data.SqlClient.SqlCommand pcm, string TipoTransaccion)
        {
            BESeguridadEvento oBE = (BESeguridadEvento)pEntidad;
            if ((TipoTransaccion == "I"))
            {
                pcm.Parameters.Add("@IDSistema", SqlDbType.Int).Value = oBE.IDSistema;
                pcm.Parameters.Add("@IDUsuario", SqlDbType.VarChar, 50).Value = oBE.IDUsuario;
                pcm.Parameters.Add("@IDTipoEvento", SqlDbType.Int).Value = oBE.IDTipoEvento;
                pcm.Parameters.Add("@Detalle", SqlDbType.VarChar, 500).Value = oBE.Detalle;
                pcm.Parameters.Add("@Host", SqlDbType.VarChar, 50).Value = oBE.Host;
            }
            //If (TipoTransaccion = "A") Then
            //End If
            return pcm;
        }
    }
}
