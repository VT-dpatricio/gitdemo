﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessEntity.Bais;

namespace AONAffinity.Motor.DataAccess.Bais
{
    public class DAOpcionPoliza
    {
        public SqlDataReader ObtenerxTipoDoc(Int32 pnIdProducto, String pcOpcion, Int32 pnIdFrecuencia, String pcIdMonedaPrima, String pcIdTipoDocumento, Boolean pbEstado, SqlConnection pSqlCn) 
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_OpcionPoliza_ObtenerxTipoDoc]", pSqlCn)) 
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@opcion", SqlDbType.VarChar, 10);
                sqlParam2.Value = pcOpcion;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@idFrecuencia", SqlDbType.Int);
                sqlParam3.Value = pnIdFrecuencia;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@idMonedaPrima", SqlDbType.VarChar, 5);
                sqlParam4.Value = pcIdMonedaPrima;

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@idTipoDocumento", SqlDbType.VarChar, 3);
                sqlParam5.Value = pcIdTipoDocumento;

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@stsRegistro", SqlDbType.Bit);
                sqlParam6.Value = pbEstado;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }
    }
}
