﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessEntity.Bais;   

namespace AONAffinity.Motor.DataAccess.Bais
{
    public class DACertificado
    {
        #region NoTransaccional
        /// <summary>
        /// Permite obtener la información de un certificado.
        /// </summary>
        /// <param name="pcIdCertificado">Código de certificado</param>
        /// <param name="pSqlCn">Objeto que permite la conexión a la BD.</param>
        /// <returns>Objeto de tipo SqlDataReader.</returns>
        public SqlDataReader Obtener(String pcIdCertificado, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_Certificado_Obtener]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idCertificado", SqlDbType.VarChar, 25);
                sqlParam1.Value = pcIdCertificado;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);
            }

            return sqlDr;
        }

        /// <summary>
        /// Permite obtener la moneda de cobro.
        /// </summary>
        /// <param name="pcIdMedioPago">Código de medio de pago.</param>
        /// <param name="pcIdMonedaPrima">Código de moneda prima.</param>
        /// <param name="pnIdProducto">Código de producto.</param>
        /// <param name="pSqlCn">Objeto que permite la conexion a la BD.</param>
        /// <returns>Objeto de tipo SqlDataReader conteniendo el código de moneda de cobro.</returns>
        public SqlDataReader ObtenerMonedaCobro(String pcIdMedioPago, String pcIdMonedaPrima, Int32 pnIdProducto, SqlConnection pSqlCn)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-12-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_Certificado_ObtenerMonedaCobro]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idMedioPago", SqlDbType.VarChar, 2);
                sqlParam1.Value = pcIdMedioPago;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idMonedaPrima", SqlDbType.VarChar, 5);
                sqlParam2.Value = pcIdMonedaPrima;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam3.Value = pnIdProducto;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);
            }

            return sqlDr;
        }

        
        public SqlDataReader Consultar(Int32 pnTipo, String pcCcCliente, Decimal pnNumCertificado, String pcApellido1, String pcApellido2, String pcNombre1, String pcNombre2, String pcIdUsuario, SqlConnection pSqlCn)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-11-29
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Certificado_Consultar]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idTipoBusqueda", SqlDbType.Int);
                sqlParam1.Value = pnTipo;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@ccCliente", SqlDbType.VarChar, 20);
                if (pcCcCliente == NullTypes.CadenaNull) { sqlParam2.Value = DBNull.Value; } else { sqlParam2.Value = pcCcCliente; }

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@numCertificado", SqlDbType.Decimal);
                if (pnNumCertificado == NullTypes.DecimalNull) { sqlParam3.Value = DBNull.Value; } else { sqlParam3.Value = pnNumCertificado; }

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@nombre1", SqlDbType.VarChar, 60);
                if (pcNombre1 == NullTypes.CadenaNull) { sqlParam4.Value = DBNull.Value; } else { sqlParam4.Value = pcNombre1; }

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@nombre2", SqlDbType.VarChar, 20);
                if (pcNombre2 == NullTypes.CadenaNull) { sqlParam5.Value = DBNull.Value; } else { sqlParam5.Value = pcNombre2; }

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@apellido1", SqlDbType.VarChar, 60);
                if (pcApellido1 == NullTypes.CadenaNull) { sqlParam6.Value = DBNull.Value; } else { sqlParam6.Value = pcApellido1; }

                SqlParameter sqlParam7 = sqlCmd.Parameters.Add("@apellido2", SqlDbType.VarChar, 60);
                if (pcApellido2 == NullTypes.CadenaNull) { sqlParam7.Value = DBNull.Value; } else { sqlParam7.Value = pcApellido2; }

                SqlParameter sqlParam8 = sqlCmd.Parameters.Add("@idUsuario", SqlDbType.VarChar, 50);
                sqlParam8.Value = pcIdUsuario;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }

        /// <summary>
        /// Permite listar los certificados por vencer.
        /// </summary>
        /// <param name="pnNroMeses">Nro de meses a conciderar.</param>
        /// <param name="pnIdPproducto">Código de producto.</param>
        /// <param name="pSqlCn">Objeto que permite la conexion a la BD.</param>
        /// <returns>Objeto de tipo SqlDataReader.</returns>
        public SqlDataReader ListarPorVencer(Int32 pnNroMeses, Int32 pnIdPproducto, SqlConnection pSqlCn)
        {          
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_Certificado_ListarPorVencer]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@nroMeses", SqlDbType.Int);
                sqlParam1.Value = pnNroMeses;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam2.Value = pnIdPproducto;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }

        /// <summary>
        /// Permite obtener ele nro de certificado disponible para expedir.
        /// </summary>
        /// <param name="pnIdProducto">Código de producto.</param>
        /// <param name="pSqlCn">Objeto que permite la conexion a la BD.</param>
        /// <returns>Objeto de tipo SqlDataReader.</returns>
        public SqlDataReader ObtenerNroCertDisp(Int32 pnIdProducto, SqlConnection pSqlCn)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_Certificado_ObtenerNroCertDisp]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);
            }

            return sqlDr;
        }

        /// <summary>
        /// Permite calcular el inicio de vigencia.
        /// </summary>
        /// <param name="pnIdProducto">Código de producto.</param>
        /// <param name="pdFechaVenta">Fecha de venta.</param>
        /// <param name="pSqlCn">Objeto que permite la conexion a la BD.</param>
        /// <returns>Objeto de tipo SqlDataReader.</returns>
        public SqlDataReader ObtenerInicioVigencia(Int32 pnIdProducto,  DateTime pdFechaVenta, SqlConnection pSqlCn) 
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2012-06-04
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_Certificado_ObtenerIniVigencia]", pSqlCn)) 
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@fechaVenta", SqlDbType.SmallDateTime);
                sqlParam2.Value = pdFechaVenta;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);    
            }

            return sqlDr;
        }

        /// <summary>
        /// Permite calcular el fin de vigencia, segun el plan del producto.
        /// </summary>
        /// <param name="pnIdProducto">Código de producto.</param>
        /// <param name="pdFechaInicioVig">Fecah inicio vigencia.</param>
        /// <param name="pcOpcion">Plan del producto.</param>
        /// <param name="pSqlCn">Objeto que permite la conexion a la BD.</param>
        /// <returns>Objeto de tipo SqlDataReader.</returns>
        public SqlDataReader ObtenerFinVigenciaxPlan(Int32 pnIdProducto, DateTime pdFechaInicioVig, String pcOpcion, SqlConnection pSqlCn) 
        {
            /*---------------------------------------------------------------------------
            * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2012-06-04
            * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
           ---------------------------------------------------------------------------*/

            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_Certificado_ObtenerFinVigenciaxPlan]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@fechaIniVigencia", SqlDbType.SmallDateTime);
                sqlParam2.Value = pdFechaInicioVig;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@opcion", SqlDbType.VarChar, 10);
                sqlParam3.Value = pcOpcion;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);    
            } 

            return sqlDr;
        }

        public SqlDataReader ValidarNumCertificado(Int32 pnIdProducto, Int32 pnIdCanal, Decimal pnNumCertificado, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr = null;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Certificado_ValidaNumCert]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idCanal", SqlDbType.Int);
                sqlParam2.Value = pnIdCanal;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@numCertificado", SqlDbType.Decimal);
                sqlParam3.Value = pnNumCertificado;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }
        #endregion        

        #region Transaccional
        /// <summary>
        /// Permite insertar un certificado.
        /// </summary>
        /// <param name="pObjCertificado"></param>
        /// <param name="pSqlCn"></param>
        /// <returns></returns>
        public Int32 Insertar(BECertificado pObjCertificado, SqlConnection pSqlCn)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-09-14
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Int32 nResult = 0;
            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_Certificado_Insertar]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idCertificado", SqlDbType.VarChar, 25);
                sqlParam1.Value = pObjCertificado.IdCertificado;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam2.Value = pObjCertificado.IdProducto;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@numCertificado", SqlDbType.Decimal);
                sqlParam3.Value = pObjCertificado.NumCertificado;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@opcion", SqlDbType.VarChar, 10);
                sqlParam4.Value = pObjCertificado.Opcion;

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@idFrecuencia", SqlDbType.Int);
                sqlParam5.Value = pObjCertificado.IdFrecuencia;

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@idOficina", SqlDbType.Int);
                sqlParam6.Value = pObjCertificado.IdOficina;

                SqlParameter sqlParam7 = sqlCmd.Parameters.Add("@idInformador", SqlDbType.VarChar, 15);
                sqlParam7.Value = pObjCertificado.IdInformador;

                SqlParameter sqlParam8 = sqlCmd.Parameters.Add("@idMedioPago", SqlDbType.VarChar, 2);
                sqlParam8.Value = pObjCertificado.IdMedioPago;

                SqlParameter sqlParam9 = sqlCmd.Parameters.Add("@idEstadoCertificado", SqlDbType.Int);
                sqlParam9.Value = pObjCertificado.IdEstadoCertificado;

                SqlParameter sqlParam10 = sqlCmd.Parameters.Add("@montoAsegurado", SqlDbType.Decimal);
                sqlParam10.Value = pObjCertificado.MontoAsegurado;

                SqlParameter sqlParam11 = sqlCmd.Parameters.Add("@primaBruta", SqlDbType.Decimal);
                sqlParam11.Value = pObjCertificado.PrimaBruta;

                SqlParameter sqlParam12 = sqlCmd.Parameters.Add("@iva", SqlDbType.Decimal);
                sqlParam12.Value = pObjCertificado.Iva;

                SqlParameter sqlParam13 = sqlCmd.Parameters.Add("@primaTotal", SqlDbType.Decimal);
                sqlParam13.Value = pObjCertificado.PrimaTotal;

                SqlParameter sqlParam14 = sqlCmd.Parameters.Add("@primaCobrar", SqlDbType.Decimal);
                if (pObjCertificado.PrimaCobrar == NullTypes.DecimalNull) { sqlParam14.Value = DBNull.Value; } else { sqlParam14.Value = pObjCertificado.PrimaCobrar; }

                SqlParameter sqlParam15 = sqlCmd.Parameters.Add("@numeroCuenta", SqlDbType.VarChar, 20);
                sqlParam15.Value = pObjCertificado.NumeroCuenta;

                SqlParameter sqlParam16 = sqlCmd.Parameters.Add("@vencimiento", SqlDbType.SmallDateTime);
                if (pObjCertificado.Vencimiento == NullTypes.FechaNull) { sqlParam16.Value = DBNull.Value; } else { sqlParam16.Value = pObjCertificado.Vencimiento; }

                SqlParameter sqlParam17 = sqlCmd.Parameters.Add("@vigencia", SqlDbType.SmallDateTime);
                if (pObjCertificado.Vigencia == NullTypes.FechaNull) { sqlParam17.Value = DBNull.Value; } else { sqlParam17.Value = pObjCertificado.Vigencia; }

                SqlParameter sqlParam18 = sqlCmd.Parameters.Add("@puntos", SqlDbType.Int);
                if (pObjCertificado.Puntos == NullTypes.IntegerNull) { sqlParam18.Value = DBNull.Value; } else { sqlParam18.Value = pObjCertificado.Puntos; }

                SqlParameter sqlParam19 = sqlCmd.Parameters.Add("@idMotivoAnulacion", SqlDbType.Int);
                if (pObjCertificado.IdMotivoAnulacion == NullTypes.IntegerNull) { sqlParam19.Value = DBNull.Value; } else { sqlParam19.Value = pObjCertificado.IdMotivoAnulacion; }

                SqlParameter sqlParam20 = sqlCmd.Parameters.Add("@consistente", SqlDbType.Bit);
                sqlParam20.Value = pObjCertificado.Consistente;

                SqlParameter sqlParam21 = sqlCmd.Parameters.Add("@puntosAnual", SqlDbType.Int);
                sqlParam21.Value = pObjCertificado.PuntosAnual;

                SqlParameter sqlParam22 = sqlCmd.Parameters.Add("@idCiudad", SqlDbType.Int);
                sqlParam22.Value = pObjCertificado.IdCiudad;

                SqlParameter sqlParam23 = sqlCmd.Parameters.Add("@direccion", SqlDbType.VarChar, 250);
                sqlParam23.Value = pObjCertificado.Direccion;

                SqlParameter sqlParam24 = sqlCmd.Parameters.Add("@telefono", SqlDbType.VarChar, 15);
                sqlParam24.Value = pObjCertificado.Telefono;

                SqlParameter sqlParam25 = sqlCmd.Parameters.Add("@nombre1", SqlDbType.VarChar, 60);
                sqlParam25.Value = pObjCertificado.Nombre1;

                SqlParameter sqlParam26 = sqlCmd.Parameters.Add("@nombre2", SqlDbType.VarChar, 60);
                if (pObjCertificado.Nombre2 == NullTypes.CadenaNull) { sqlParam26.Value = DBNull.Value; } else { sqlParam26.Value = pObjCertificado.Nombre2; }

                SqlParameter sqlParam27 = sqlCmd.Parameters.Add("@apellido1", SqlDbType.VarChar, 60);
                sqlParam27.Value = pObjCertificado.Apellido1;

                SqlParameter sqlParam28 = sqlCmd.Parameters.Add("@apellido2", SqlDbType.VarChar, 60);
                if (pObjCertificado.Apellido2 == NullTypes.CadenaNull) { sqlParam28.Value = DBNull.Value; } else { sqlParam28.Value = pObjCertificado.Apellido2; }

                SqlParameter sqlParam29 = sqlCmd.Parameters.Add("@ccCliente", SqlDbType.VarChar, 20);
                sqlParam29.Value = pObjCertificado.CcCliente;

                SqlParameter sqlParam30 = sqlCmd.Parameters.Add("@idTipoDocumento", SqlDbType.VarChar, 3);
                sqlParam30.Value = pObjCertificado.IdTipoDocumento;

                SqlParameter sqlParam31 = sqlCmd.Parameters.Add("@digitacion", SqlDbType.SmallDateTime);
                sqlParam31.Value = pObjCertificado.Digitacion;

                SqlParameter sqlParam32 = sqlCmd.Parameters.Add("@incentivo", SqlDbType.Decimal);
                if (pObjCertificado.Incentivo == NullTypes.DecimalNull) { sqlParam32.Value = DBNull.Value; } else { sqlParam32.Value = pObjCertificado.Incentivo; }

                SqlParameter sqlParam33 = sqlCmd.Parameters.Add("@saldoIncentivo", SqlDbType.Decimal);
                if (pObjCertificado.SaldoIncentivo == NullTypes.DecimalNull) { sqlParam33.Value = DBNull.Value; } else { sqlParam33.Value = pObjCertificado.SaldoIncentivo; }

                SqlParameter sqlParam34 = sqlCmd.Parameters.Add("@usuarioCreacion", SqlDbType.VarChar, 50);
                sqlParam34.Value = pObjCertificado.UsuarioCreacion;

                SqlParameter sqlParam35 = sqlCmd.Parameters.Add("@venta", SqlDbType.SmallDateTime);
                sqlParam35.Value = pObjCertificado.Venta;

                SqlParameter sqlParam36 = sqlCmd.Parameters.Add("@finVigencia", SqlDbType.SmallDateTime);
                if (pObjCertificado.FinVigencia == NullTypes.FechaNull) { sqlParam36.Value = DBNull.Value; } else { sqlParam36.Value = pObjCertificado.FinVigencia; }

                SqlParameter sqlParam37 = sqlCmd.Parameters.Add("@idMonedaPrima", SqlDbType.VarChar, 5);
                sqlParam37.Value = pObjCertificado.IdMonedaPrima;

                SqlParameter sqlParam38 = sqlCmd.Parameters.Add("@idMonedaCobro", SqlDbType.VarChar, 5);
                sqlParam38.Value = pObjCertificado.IdMonedaCobro;

                SqlParameter sqlParam39 = sqlCmd.Parameters.Add("@afiliadoHasta", SqlDbType.SmallDateTime);
                if (pObjCertificado.AfiliadoHasta == NullTypes.FechaNull) { sqlParam39.Value = DBNull.Value; } else { sqlParam39.Value = pObjCertificado.AfiliadoHasta; }

                SqlParameter sqlParam40 = sqlCmd.Parameters.Add("@idCiclo", SqlDbType.Int);
                if (pObjCertificado.IdCiclo == NullTypes.IntegerNull) { sqlParam40.Value = DBNull.Value; } else { sqlParam40.Value = pObjCertificado.IdCiclo; }

                SqlParameter sqlParam41 = sqlCmd.Parameters.Add("@diaGenera", SqlDbType.SmallInt);
                if (pObjCertificado.DiaGenera == NullTypes.ShortNull) { sqlParam41.Value = DBNull.Value; } else { sqlParam41.Value = pObjCertificado.DiaGenera; }

                SqlParameter sqlParam42 = sqlCmd.Parameters.Add("@mesGenera", SqlDbType.SmallInt);
                if (pObjCertificado.MesGenera == NullTypes.ShortNull) { sqlParam42.Value = DBNull.Value; } else { sqlParam42.Value = pObjCertificado.MesGenera; }

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        }

        /// <summary>
        /// Permite isnerta un certificado.
        /// </summary>
        /// <param name="pObjCertificado">Objeto que contiene la información del certificado.</param>
        /// <param name="pSqlCn">Objeto que contiene la información del Certificado.</param>
        /// <param name="pSqlTrs">Objeto transacción.</param>
        /// <returns>Nro de filas insertadas.</returns>
        public Int32 Insertar(BECertificado pObjCertificado, SqlConnection pSqlCn, SqlTransaction pSqlTrs)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-09-14
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Int32 nResult = 0;
            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_Certificado_Insertar]", pSqlCn))
            {
                sqlCmd.Transaction = pSqlTrs;
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idCertificado", SqlDbType.VarChar, 25);
                sqlParam1.Value = pObjCertificado.IdCertificado;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam2.Value = pObjCertificado.IdProducto;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@numCertificado", SqlDbType.Decimal);
                sqlParam3.Value = pObjCertificado.NumCertificado;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@opcion", SqlDbType.VarChar, 10);
                sqlParam4.Value = pObjCertificado.Opcion;

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@idFrecuencia", SqlDbType.Int);
                sqlParam5.Value = pObjCertificado.IdFrecuencia;

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@idOficina", SqlDbType.Int);
                sqlParam6.Value = pObjCertificado.IdOficina;

                SqlParameter sqlParam7 = sqlCmd.Parameters.Add("@idInformador", SqlDbType.VarChar, 15);
                sqlParam7.Value = pObjCertificado.IdInformador;

                SqlParameter sqlParam8 = sqlCmd.Parameters.Add("@idMedioPago", SqlDbType.VarChar, 2);
                sqlParam8.Value = pObjCertificado.IdMedioPago;

                SqlParameter sqlParam9 = sqlCmd.Parameters.Add("@idEstadoCertificado", SqlDbType.Int);
                sqlParam9.Value = pObjCertificado.IdEstadoCertificado;

                SqlParameter sqlParam10 = sqlCmd.Parameters.Add("@montoAsegurado", SqlDbType.Decimal);
                sqlParam10.Value = pObjCertificado.MontoAsegurado;

                SqlParameter sqlParam11 = sqlCmd.Parameters.Add("@primaBruta", SqlDbType.Decimal);
                sqlParam11.Value = pObjCertificado.PrimaBruta;

                SqlParameter sqlParam12 = sqlCmd.Parameters.Add("@iva", SqlDbType.Decimal);
                sqlParam12.Value = pObjCertificado.Iva;

                SqlParameter sqlParam13 = sqlCmd.Parameters.Add("@primaTotal", SqlDbType.Decimal);
                sqlParam13.Value = pObjCertificado.PrimaTotal;

                SqlParameter sqlParam14 = sqlCmd.Parameters.Add("@primaCobrar", SqlDbType.Decimal);
                if (pObjCertificado.PrimaCobrar == NullTypes.DecimalNull) { sqlParam14.Value = DBNull.Value; } else { sqlParam14.Value = pObjCertificado.PrimaCobrar; }

                SqlParameter sqlParam15 = sqlCmd.Parameters.Add("@numeroCuenta", SqlDbType.VarChar, 20);
                sqlParam15.Value = pObjCertificado.NumeroCuenta;

                SqlParameter sqlParam16 = sqlCmd.Parameters.Add("@vencimiento", SqlDbType.SmallDateTime);
                if (pObjCertificado.Vencimiento == NullTypes.FechaNull) { sqlParam16.Value = DBNull.Value; } else { sqlParam16.Value = pObjCertificado.Vencimiento; }

                SqlParameter sqlParam17 = sqlCmd.Parameters.Add("@vigencia", SqlDbType.SmallDateTime);
                if (pObjCertificado.Vigencia == NullTypes.FechaNull) { sqlParam17.Value = DBNull.Value; } else { sqlParam17.Value = pObjCertificado.Vigencia; }

                SqlParameter sqlParam18 = sqlCmd.Parameters.Add("@puntos", SqlDbType.Int);
                if (pObjCertificado.Puntos == NullTypes.IntegerNull) { sqlParam18.Value = DBNull.Value; } else { sqlParam18.Value = pObjCertificado.Puntos; }

                SqlParameter sqlParam19 = sqlCmd.Parameters.Add("@idMotivoAnulacion", SqlDbType.Int);
                if (pObjCertificado.IdMotivoAnulacion == NullTypes.IntegerNull) { sqlParam19.Value = DBNull.Value; } else { sqlParam19.Value = pObjCertificado.IdMotivoAnulacion; }

                SqlParameter sqlParam20 = sqlCmd.Parameters.Add("@consistente", SqlDbType.Bit);
                sqlParam20.Value = pObjCertificado.Consistente;

                SqlParameter sqlParam21 = sqlCmd.Parameters.Add("@puntosAnual", SqlDbType.Int);
                sqlParam21.Value = pObjCertificado.PuntosAnual;

                SqlParameter sqlParam22 = sqlCmd.Parameters.Add("@idCiudad", SqlDbType.Int);
                sqlParam22.Value = pObjCertificado.IdCiudad;

                SqlParameter sqlParam23 = sqlCmd.Parameters.Add("@direccion", SqlDbType.VarChar, 250);
                sqlParam23.Value = pObjCertificado.Direccion;

                SqlParameter sqlParam24 = sqlCmd.Parameters.Add("@telefono", SqlDbType.VarChar, 15);
                sqlParam24.Value = pObjCertificado.Telefono;

                SqlParameter sqlParam25 = sqlCmd.Parameters.Add("@nombre1", SqlDbType.VarChar, 60);
                sqlParam25.Value = pObjCertificado.Nombre1;

                SqlParameter sqlParam26 = sqlCmd.Parameters.Add("@nombre2", SqlDbType.VarChar, 60);
                if (pObjCertificado.Nombre2 == NullTypes.CadenaNull) { sqlParam26.Value = DBNull.Value; } else { sqlParam26.Value = pObjCertificado.Nombre2; }

                SqlParameter sqlParam27 = sqlCmd.Parameters.Add("@apellido1", SqlDbType.VarChar, 60);
                sqlParam27.Value = pObjCertificado.Apellido1;

                SqlParameter sqlParam28 = sqlCmd.Parameters.Add("@apellido2", SqlDbType.VarChar, 60);
                if (pObjCertificado.Apellido2 == NullTypes.CadenaNull) { sqlParam28.Value = DBNull.Value; } else { sqlParam28.Value = pObjCertificado.Apellido2; }

                SqlParameter sqlParam29 = sqlCmd.Parameters.Add("@ccCliente", SqlDbType.VarChar, 20);
                sqlParam29.Value = pObjCertificado.CcCliente;

                SqlParameter sqlParam30 = sqlCmd.Parameters.Add("@idTipoDocumento", SqlDbType.VarChar, 3);
                sqlParam30.Value = pObjCertificado.IdTipoDocumento;

                SqlParameter sqlParam31 = sqlCmd.Parameters.Add("@digitacion", SqlDbType.SmallDateTime);
                sqlParam31.Value = pObjCertificado.Digitacion;

                SqlParameter sqlParam32 = sqlCmd.Parameters.Add("@incentivo", SqlDbType.Decimal);
                if (pObjCertificado.Incentivo == NullTypes.DecimalNull) { sqlParam32.Value = DBNull.Value; } else { sqlParam32.Value = pObjCertificado.Incentivo; }

                SqlParameter sqlParam33 = sqlCmd.Parameters.Add("@saldoIncentivo", SqlDbType.Decimal);
                if (pObjCertificado.SaldoIncentivo == NullTypes.DecimalNull) { sqlParam33.Value = DBNull.Value; } else { sqlParam33.Value = pObjCertificado.SaldoIncentivo; }

                SqlParameter sqlParam34 = sqlCmd.Parameters.Add("@usuarioCreacion", SqlDbType.VarChar, 50);
                sqlParam34.Value = pObjCertificado.UsuarioCreacion;

                SqlParameter sqlParam35 = sqlCmd.Parameters.Add("@venta", SqlDbType.SmallDateTime);
                sqlParam35.Value = pObjCertificado.Venta;

                SqlParameter sqlParam36 = sqlCmd.Parameters.Add("@finVigencia", SqlDbType.SmallDateTime);
                if (pObjCertificado.FinVigencia == NullTypes.FechaNull) { sqlParam36.Value = DBNull.Value; } else { sqlParam36.Value = pObjCertificado.FinVigencia; }

                SqlParameter sqlParam37 = sqlCmd.Parameters.Add("@idMonedaPrima", SqlDbType.VarChar, 5);
                sqlParam37.Value = pObjCertificado.IdMonedaPrima;

                SqlParameter sqlParam38 = sqlCmd.Parameters.Add("@idMonedaCobro", SqlDbType.VarChar, 5);
                sqlParam38.Value = pObjCertificado.IdMonedaCobro;

                SqlParameter sqlParam39 = sqlCmd.Parameters.Add("@afiliadoHasta", SqlDbType.SmallDateTime);
                if (pObjCertificado.AfiliadoHasta == NullTypes.FechaNull) { sqlParam39.Value = DBNull.Value; } else { sqlParam39.Value = pObjCertificado.AfiliadoHasta; }

                SqlParameter sqlParam40 = sqlCmd.Parameters.Add("@idCiclo", SqlDbType.Int);
                if (pObjCertificado.IdCiclo == NullTypes.IntegerNull) { sqlParam40.Value = DBNull.Value; } else { sqlParam40.Value = pObjCertificado.IdCiclo; }

                SqlParameter sqlParam41 = sqlCmd.Parameters.Add("@diaGenera", SqlDbType.SmallInt);
                if (pObjCertificado.DiaGenera == NullTypes.ShortNull) { sqlParam41.Value = DBNull.Value; } else { sqlParam41.Value = pObjCertificado.DiaGenera; }

                SqlParameter sqlParam42 = sqlCmd.Parameters.Add("@mesGenera", SqlDbType.SmallInt);
                if (pObjCertificado.MesGenera == NullTypes.ShortNull) { sqlParam42.Value = DBNull.Value; } else { sqlParam42.Value = pObjCertificado.MesGenera; }

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        }

        /// <summary>
        /// Permite actualizar la información de un certificado.
        /// </summary>
        /// <param name="pObjBECErtificado">Objeto que contiene la información del certificado.</param>
        /// <param name="pcObservacion">Descripción de la observación</param>
        /// <param name="pSqlCn">Objeto que permite la conexion a la BD.</param>
        /// <returns>0 No actualizó | 1 Si actualizó.</returns>
        public Int32 Actualizar(BECertificado pObjBECErtificado, String pcObservacion, SqlConnection pSqlCn)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-12-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_Certificado_Actualizar]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idCertificado", SqlDbType.VarChar, 25);
                sqlParam1.Value = pObjBECErtificado.IdCertificado;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idOficina", SqlDbType.Int);
                sqlParam2.Value = pObjBECErtificado.IdOficina;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@idInformador", SqlDbType.VarChar, 15);
                sqlParam3.Value = pObjBECErtificado.IdInformador;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@idMedioPago", SqlDbType.VarChar, 2);
                sqlParam4.Value = pObjBECErtificado.IdMedioPago;

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@idEstadoCertificado", SqlDbType.Int);
                sqlParam5.Value = pObjBECErtificado.IdEstadoCertificado;

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@nombre1", SqlDbType.VarChar, 60);
                sqlParam6.Value = pObjBECErtificado.Nombre1;

                SqlParameter sqlParam7 = sqlCmd.Parameters.Add("@nombre2", SqlDbType.VarChar, 60);
                sqlParam7.Value = pObjBECErtificado.Nombre2;

                SqlParameter sqlParam8 = sqlCmd.Parameters.Add("@apellido1", SqlDbType.VarChar, 60);
                sqlParam8.Value = pObjBECErtificado.Apellido1;

                SqlParameter sqlParam9 = sqlCmd.Parameters.Add("@apellido2", SqlDbType.VarChar, 60);
                sqlParam9.Value = pObjBECErtificado.Apellido2;

                SqlParameter sqlParam10 = sqlCmd.Parameters.Add("@idTipoDocumento", SqlDbType.VarChar, 3);
                sqlParam10.Value = pObjBECErtificado.IdTipoDocumento;

                SqlParameter sqlParam11 = sqlCmd.Parameters.Add("@ccCliente", SqlDbType.VarChar, 20);
                sqlParam11.Value = pObjBECErtificado.CcCliente;

                SqlParameter sqlParam12 = sqlCmd.Parameters.Add("@idCiudad", SqlDbType.Int);
                sqlParam12.Value = pObjBECErtificado.IdCiudad;

                SqlParameter sqlParam13 = sqlCmd.Parameters.Add("@Direccion", SqlDbType.VarChar, 250);
                sqlParam13.Value = pObjBECErtificado.Direccion;

                SqlParameter sqlParam14 = sqlCmd.Parameters.Add("@Telefono", SqlDbType.VarChar, 15);
                sqlParam14.Value = pObjBECErtificado.Telefono;

                SqlParameter sqlParam15 = sqlCmd.Parameters.Add("@numeroCuenta", SqlDbType.VarChar, 20);
                sqlParam15.Value = pObjBECErtificado.NumeroCuenta;

                SqlParameter sqlParam16 = sqlCmd.Parameters.Add("@vencimiento", SqlDbType.SmallDateTime);
                if (pObjBECErtificado.Vencimiento == NullTypes.FechaNull) { sqlParam16.Value = DBNull.Value; } else { sqlParam16.Value = pObjBECErtificado.Vencimiento; }

                SqlParameter sqlParam17 = sqlCmd.Parameters.Add("@idMonedaCobro", SqlDbType.VarChar, 3);
                sqlParam17.Value = pObjBECErtificado.IdMonedaCobro;

                SqlParameter sqlParam18 = sqlCmd.Parameters.Add("@usuarioModificacion", SqlDbType.VarChar, 50);
                sqlParam18.Value = pObjBECErtificado.UsuarioModificacion;

                SqlParameter sqlParam19 = sqlCmd.Parameters.Add("@descripcion", SqlDbType.VarChar, 255);
                sqlParam19.Value = pcObservacion;

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        }
        #endregion
    }
}
