﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessEntity.Bais; 

namespace AONAffinity.Motor.DataAccess.Bais
{
    public class DAExpedir
    {
        #region NoTransaccional
        /// <summary>
        /// Permite obtener las validaciones del certificado a expedir.
        /// </summary>
        /// <param name="pnIdProducto">Código de producto.</param>
        /// <param name="pcNumCertFisico">Nro de fisico.</param>
        /// <param name="pcCcCliente">Nro de documento.</param>
        /// <param name="pcNumCuenta">Nro de cuenta.</param>
        /// <param name="pSqlCn">Objeto que permite la conexion a la BD.</param>
        /// <returns>Obejto de tipo SqlDataReader.</returns>
        public SqlDataReader ValidaExpedirCertificado(Int32 pnIdProducto, String pcNumCertFisico, String pcCcCliente, String pcNumCuenta, SqlConnection pSqlCn)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-09-14
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_Certificado_ValidaExpedir]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@numCertFisico", SqlDbType.VarChar, 20);
                sqlParam2.Value = pcNumCertFisico;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@cccliente", SqlDbType.VarChar, 20);
                sqlParam3.Value = pcCcCliente;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@numcuenta", SqlDbType.VarChar, 20);
                sqlParam4.Value = pcNumCuenta;

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@ReturnValue", SqlDbType.Int);
                sqlParam5.Direction = ParameterDirection.ReturnValue;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);

                return sqlDr;
            }
        }

        /// <summary>
        /// Permite obtener las validaciones del asegurado del certificado a expedir.
        /// </summary>
        /// <param name="pnIdProducto">Código de producto.</param>
        /// <param name="pcCcAseg">Nro. documento del asegurado</param>
        /// <param name="pSqlCn">Objeto que permite la conexion a la BD.</param>
        /// <returns>Obejto de tipo SqlDataReader.</returns>
        public SqlDataReader ValidaExpedirAsegurado(Int32 pnIdProducto, String pcCcAseg, SqlConnection pSqlCn)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-09-14
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_Asegurado_ValidaExpedir]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@ccAseg", SqlDbType.VarChar);
                sqlParam1.Value = pcCcAseg;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idproducto", SqlDbType.Int);
                sqlParam2.Value = pnIdProducto;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@ReturnValue", SqlDbType.Int);
                sqlParam3.Direction = ParameterDirection.ReturnValue;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }
            return sqlDr;
        }

        public SqlDataReader ValidaExpedirVahiculoAsegurado(Int32 pnIdProducto, AONAffinity.Motor.BusinessEntity.BEVehiculo pObjBEVehiculo, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_Asegurado_VehiculoValidaExpedir]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idInfAsegPlaca", SqlDbType.Int);
                sqlParam2.Value = pObjBEVehiculo.IdInfasegPlaca = pObjBEVehiculo.IdInfasegPlaca;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@nroPlaca", SqlDbType.VarChar, 25);
                sqlParam3.Value = pObjBEVehiculo.Placa;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@idInfAsegMotor", SqlDbType.Int);
                sqlParam4.Value = pObjBEVehiculo.IdInfoAsegMotor;

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@nroMotor", SqlDbType.VarChar, 25);
                sqlParam5.Value = pObjBEVehiculo.NroMotor;

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@idInfAsegSerie", SqlDbType.Int);
                sqlParam6.Value = pObjBEVehiculo.IdInfAsegSerie;

                SqlParameter sqlParam7 = sqlCmd.Parameters.Add("@nroSerie", SqlDbType.VarChar, 25);
                sqlParam7.Value = pObjBEVehiculo.NroChasis;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }
        #endregion
    }
}
