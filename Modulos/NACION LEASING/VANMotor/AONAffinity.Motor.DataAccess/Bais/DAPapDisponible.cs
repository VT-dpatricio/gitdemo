﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessEntity.Bais;

namespace AONAffinity.Motor.DataAccess.Bais
{
    public class DAPapDisponible
    {
        /// <summary>
        /// Permite obtener el nro. de certificado disponible por producto y oficina.
        /// </summary>
        /// <param name="pnIdOficina">Código de oficina.</param>
        /// <param name="pnIdProducto">Código de producto.</param>
        /// <param name="pSqlCn">Objeto que permite las conexxion a la BD.</param>        
        /// <returns>Objeto de tipo SqlDataReader.</returns>
        public SqlDataReader ObtenerDisponible(Int32 pnIdOficina, Int32 pnIdProducto, SqlConnection pSqlCn)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_PapDisponible_ObtenerDisp]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idOficina", SqlDbType.Int);
                sqlParam1.Value = pnIdOficina;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam2.Value = pnIdProducto;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }
    }
}
