﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessEntity.Bais; 

namespace AONAffinity.Motor.DataAccess.Bais
{
    public class DAFrecuencia
    {
        /// <summary>
        /// Permite listar las frecuencias por producto.
        /// </summary>
        /// <param name="pnIdProducto">Código de producto.</param>
        /// <param name="pSqlCn">Objeto que permite la conexión a la BD.</param>
        /// <returns>Objeto de tipo SqlDataReader.</returns>
        public SqlDataReader Listar(Int32 pnIdProducto, SqlConnection pSqlCn)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_FrecuenciaOpcionPrima_Seleccion]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }


        /// <summary>
        /// Permite listar las frecuencias por producto 5105.
        /// </summary>
        /// <param name="pnIdProducto">Código de producto.</param>
        /// <param name="pSqlCn">Objeto que permite la conexión a la BD.</param>
        /// <returns>Objeto de tipo SqlDataReader.</returns>
        public SqlDataReader ListarFrecuencia(Int32 pnIdProducto, SqlConnection pSqlCn)
        {


            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_Frecuencia_Seleccion]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }

        public SqlDataReader ListarxProdPlan(Int32 pnIdProducto, Int32 pnIdPlanes, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_Frecuencia_ListarxProdPlan]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idPlanes", SqlDbType.Int);
                sqlParam2.Value = pnIdPlanes;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }

        public SqlDataReader ListarxProd(Int32 pnIdProducto, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_Frecuencia_ListarxProd]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }
    }
}
