﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessEntity.Bais;

namespace AONAffinity.Motor.DataAccess.Bais
{
    public class DAAsegurado
    {
        #region Transaccional
        /// <summary>
        /// Permite insertar un asegurado.
        /// </summary>
        /// <param name="pObjBEAsegurado">Objeto que contiene la información del asegurado.</param>
        /// <param name="pSqlCn">Objeto que permite la conexión a la BD.</param>
        /// <returns>Nro. de registros insertados.</returns>
        public Int32 Insertar(BEAsegurado pObjBEAsegurado, SqlConnection pSqlCn)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-09-14
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_Asegurado_Insertar]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idCertificado", SqlDbType.VarChar, 25);
                sqlParam1.Value = pObjBEAsegurado.IdCertificado;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@consecutivo", SqlDbType.SmallInt);
                sqlParam2.Value = pObjBEAsegurado.Consecutivo;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@idTipoDocumento", SqlDbType.VarChar, 3);
                sqlParam3.Value = pObjBEAsegurado.IdTipodocumento;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@ccAseg", SqlDbType.VarChar, 20);
                sqlParam4.Value = pObjBEAsegurado.Ccaseg;

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@nombre1", SqlDbType.VarChar, 60);
                sqlParam5.Value = pObjBEAsegurado.Nombre1;

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@nombre2", SqlDbType.VarChar, 60);
                sqlParam6.Value = pObjBEAsegurado.Nombre2;

                SqlParameter sqlParam7 = sqlCmd.Parameters.Add("@apellido1", SqlDbType.VarChar, 60);
                sqlParam7.Value = pObjBEAsegurado.Apellido1;

                SqlParameter sqlParam8 = sqlCmd.Parameters.Add("@apellido2", SqlDbType.VarChar, 60);
                sqlParam8.Value = pObjBEAsegurado.Apellido2;

                SqlParameter sqlParam9 = sqlCmd.Parameters.Add("@Direccion", SqlDbType.VarChar, 250);
                sqlParam9.Value = pObjBEAsegurado.Direccion;

                SqlParameter sqlParam10 = sqlCmd.Parameters.Add("@Telefono", SqlDbType.VarChar, 15);
                sqlParam10.Value = pObjBEAsegurado.Telefono;

                SqlParameter sqlParam11 = sqlCmd.Parameters.Add("@idCiudad", SqlDbType.Int);
                sqlParam11.Value = pObjBEAsegurado.IdCiudad;

                SqlParameter sqlParam12 = sqlCmd.Parameters.Add("@fechaNacimiento", SqlDbType.SmallDateTime);
                sqlParam12.Value = pObjBEAsegurado.FechaNacimiento;

                SqlParameter sqlParam13 = sqlCmd.Parameters.Add("@idParentesco", SqlDbType.SmallInt, 25);
                sqlParam13.Value = pObjBEAsegurado.IdParentesco;

                SqlParameter sqlParam14 = sqlCmd.Parameters.Add("@activo", SqlDbType.Bit, 25);
                sqlParam14.Value = pObjBEAsegurado.Activo;

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        }

        /// <summary>
        /// Permite insertar un asegurado.
        /// </summary>
        /// <param name="pObjBEAsegurado">Objeto que contiene la información del asegurado.</param>
        /// <param name="pSqlCn">Objeto que permite la conexión a la BD.</param>
        /// <param name="pSqlTrs">Objto transacción.</param>
        /// <returns>Nro. de registros insertados.</returns>
        public Int32 Insertar(BEAsegurado pObjBEAsegurado, SqlConnection pSqlCn, SqlTransaction pSqlTrs)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-09-14
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_Asegurado_Insertar]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Transaction = pSqlTrs;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idCertificado", SqlDbType.VarChar, 25);
                sqlParam1.Value = pObjBEAsegurado.IdCertificado;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@consecutivo", SqlDbType.SmallInt);
                sqlParam2.Value = pObjBEAsegurado.Consecutivo;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@idTipoDocumento", SqlDbType.VarChar, 3);
                sqlParam3.Value = pObjBEAsegurado.IdTipodocumento;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@ccAseg", SqlDbType.VarChar, 20);
                sqlParam4.Value = pObjBEAsegurado.Ccaseg;

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@nombre1", SqlDbType.VarChar, 60);
                sqlParam5.Value = pObjBEAsegurado.Nombre1;

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@nombre2", SqlDbType.VarChar, 60);
                sqlParam6.Value = pObjBEAsegurado.Nombre2;

                SqlParameter sqlParam7 = sqlCmd.Parameters.Add("@apellido1", SqlDbType.VarChar, 60);
                sqlParam7.Value = pObjBEAsegurado.Apellido1;

                SqlParameter sqlParam8 = sqlCmd.Parameters.Add("@apellido2", SqlDbType.VarChar, 60);
                sqlParam8.Value = pObjBEAsegurado.Apellido2;

                SqlParameter sqlParam9 = sqlCmd.Parameters.Add("@Direccion", SqlDbType.VarChar, 250);
                sqlParam9.Value = pObjBEAsegurado.Direccion;

                SqlParameter sqlParam10 = sqlCmd.Parameters.Add("@Telefono", SqlDbType.VarChar, 15);
                sqlParam10.Value = pObjBEAsegurado.Telefono;

                SqlParameter sqlParam11 = sqlCmd.Parameters.Add("@idCiudad", SqlDbType.Int);
                sqlParam11.Value = pObjBEAsegurado.IdCiudad;

                SqlParameter sqlParam12 = sqlCmd.Parameters.Add("@fechaNacimiento", SqlDbType.SmallDateTime);
                sqlParam12.Value = pObjBEAsegurado.FechaNacimiento;

                SqlParameter sqlParam13 = sqlCmd.Parameters.Add("@idParentesco", SqlDbType.SmallInt, 25);
                sqlParam13.Value = pObjBEAsegurado.IdParentesco;

                SqlParameter sqlParam14 = sqlCmd.Parameters.Add("@activo", SqlDbType.Bit, 25);
                sqlParam14.Value = pObjBEAsegurado.Activo;

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        }
        #endregion

        #region NoTransaccional
        /// <summary>
        /// Permite obtener los asegurados etarios.
        /// </summary>
        /// <param name="pSqlCn">Objeto que permite la conexión a la BD.</param>
        /// <param name="pIdProducto">Codigo de producto</param>
        /// <returns>Objto de tipo SqlDataReader.</returns>
        public SqlDataReader ConsultarEtarios(SqlConnection pSqlCn, Int32 pIdProducto)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-09-14
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BATCH_Asegurado_ListarEtario]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pIdProducto;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }
        #endregion
    }
}
