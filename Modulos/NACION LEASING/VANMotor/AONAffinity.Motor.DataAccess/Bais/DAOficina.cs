﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessEntity.Bais;

namespace AONAffinity.Motor.DataAccess.Bais
{
    public class DAOficina
    {
        #region NoTransaccional
        public SqlDataReader Obtener(Int32 pnCodOficina, Int32 pnIdEntidad, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_Oficina_ObtenerxCodExtEnt]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@codOficina", SqlDbType.Int);
                sqlParam1.Value = pnCodOficina;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idEntidad", SqlDbType.Int);
                sqlParam2.Value = pnIdEntidad;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }

        public SqlDataReader ListarxSponsor(Int32 pnIdEntidad, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;
            
            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Oficina_ListarxSponsor]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                
                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idSponsor", SqlDbType.Int);
                sqlParam1.Value = pnIdEntidad;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }

        public SqlDataReader ListarxEntidad(Int32 pnIdEntidad, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;
                        
            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_Oficina_ListarxEntidad]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idEntidad", SqlDbType.Int);
                sqlParam1.Value = pnIdEntidad;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }

        /// <summary>
        /// Permite obtener las oficinas por entidad y descripción.
        /// </summary>
        /// <param name="nIdEntidad">Código de entidad.</param>
        /// <param name="pcDescripcion">Descripción de oficina.</param>
        /// <param name="pSqlCn">Objeto que permite la conexion a la BD.</param>
        /// <returns>Objeto de tipo SqlDataReader.</returns>
        public SqlDataReader Listar(Int32 pnIdEntidad, String pcDescripcion, SqlConnection pSqlCn)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-03
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_Oficina_ListarxIdEntDescrpcion]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idEntidad", SqlDbType.Int);
                sqlParam1.Value = pnIdEntidad;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@descripcion", SqlDbType.VarChar, 50);
                sqlParam2.Value = pcDescripcion;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }
        #endregion
    }
}
