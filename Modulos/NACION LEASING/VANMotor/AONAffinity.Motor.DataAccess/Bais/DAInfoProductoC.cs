﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessEntity.Bais; 

namespace AONAffinity.Motor.DataAccess.Bais
{
    public class DAInfoProductoC
    {
        #region Transaccional
        /// <summary>
        /// Permite isertar un infoProductoC.
        /// </summary>
        /// <param name="pObjBEInfoProductoC">Objeto que contiene la información del info producto.</param>
        /// <param name="sqlCn">Objeto que permite la conexión a la BD.</param>
        /// <param name="pSqlTrs">Objeto transacción.</param>
        /// <returns>Nro de registros insertados.</returns>
        public Int32 Insertar(BEInfoProductoC pObjBEInfoProductoC, SqlConnection sqlCn, SqlTransaction pSqlTrs)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-09-14
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Int32 nResult = -1;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_InfoProductoC_Insertar]", sqlCn))
            {
                sqlCmd.Transaction = pSqlTrs;
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idCertificado", SqlDbType.VarChar, 25);
                sqlParam1.Value = pObjBEInfoProductoC.IdCertificado;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@valorNum", SqlDbType.Decimal);
                if (pObjBEInfoProductoC.ValorNum == NullTypes.DecimalNull) { sqlParam2.Value = DBNull.Value; } else { sqlParam2.Value = pObjBEInfoProductoC.ValorNum; }

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@valorDate", SqlDbType.DateTime);
                if (pObjBEInfoProductoC.ValorDate == NullTypes.FechaNull) { sqlParam3.Value = DBNull.Value; } else { sqlParam3.Value = pObjBEInfoProductoC.ValorDate; }

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@valorString", SqlDbType.VarChar, 250);
                if (pObjBEInfoProductoC.ValorString == NullTypes.CadenaNull) { sqlParam4.Value = DBNull.Value; } else { sqlParam4.Value = pObjBEInfoProductoC.ValorString; }

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@idInfoproducto", SqlDbType.Int);
                sqlParam5.Value = pObjBEInfoProductoC.IdInfoProducto;

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        }
        #endregion

        #region NoTransaccional
        /// <summary>
        /// Permite listar los info productos por certificado.
        /// </summary>
        /// <param name="pcIdCertificado">Id de certificado.</param>
        /// <param name="pSqlCn">Objeto que permite la conexion a la BD.</param>
        /// <returns>Objeto de tipo SqlDataReader.</returns>
        public SqlDataReader ListarxCertificado(String pcIdCertificado, SqlConnection pSqlCn)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-12-20
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("BAIS_InfoProductoC_ListarxCertificado", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idCertificado", SqlDbType.VarChar, 25);
                sqlParam1.Value = pcIdCertificado;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }
        #endregion
    }
}
