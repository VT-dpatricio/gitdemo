﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessEntity.Bais; 

namespace AONAffinity.Motor.DataAccess.Bais
{
    public class DAInfoAseguradoC
    {
        #region Transaccional
        /// <summary>
        /// Permite insertar un infoAseguradoC.
        /// </summary>
        /// <param name="pObjBEInfoAseguradoC">Objeto que contiene la información del InfoAseguradoC.</param>
        /// <param name="pIdProducto"></param>
        /// <param name="pSqlCn"></param>
        /// <param name="pSqlTrs"></param>
        /// <returns></returns>
        public Int32 Insertar(BEInfoAseguradoC pObjBEInfoAseguradoC, Int32 pIdProducto, SqlConnection pSqlCn, SqlTransaction pSqlTrs)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-09-14
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_InfoAseguradoC_Insertar]", pSqlCn))
            {
                sqlCmd.Transaction = pSqlTrs;
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idCertificado", SqlDbType.VarChar, 25);
                sqlParam1.Value = pObjBEInfoAseguradoC.IdCertificado;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@consecutivo", SqlDbType.SmallInt);
                sqlParam2.Value = pObjBEInfoAseguradoC.Consecutivo;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@valorNum", SqlDbType.Decimal);
                if (pObjBEInfoAseguradoC.ValorNum == NullTypes.DecimalNull) { sqlParam3.Value = DBNull.Value; } else { sqlParam3.Value = pObjBEInfoAseguradoC.ValorNum; }

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@valorDate", SqlDbType.DateTime);
                if (pObjBEInfoAseguradoC.ValorDate == NullTypes.FechaNull) { sqlParam4.Value = DBNull.Value; } else { sqlParam4.Value = pObjBEInfoAseguradoC.ValorDate; }

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@valorString", SqlDbType.VarChar, 250);
                if (pObjBEInfoAseguradoC.ValorString == String.Empty) { sqlParam5.Value = DBNull.Value; } else { sqlParam5.Value = pObjBEInfoAseguradoC.ValorString; }

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@idInfoAsegurado", SqlDbType.Int);
                sqlParam6.Value = pObjBEInfoAseguradoC.IdInfoAsegurado;

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        }


        /// <summary>
        /// Permite actualizar un registro de infoAseguradoC.
        /// </summary>
        /// <param name="pObjBEInfoAseguradoC">Objeto que contiene la información del infoAseguradoC.</param>
        /// <param name="pSqlCn">Objeto de conexión a la BD.</param>
        /// <returns>La cantidad de registros afectados.</returns>
        public Int32 Actualizar(BEInfoAseguradoC pObjBEInfoAseguradoC, SqlConnection pSqlCn)
        {
            /*
            * CREADO POR:              Gary Porras Paraguay
            * FECHA DE CREACION:       2011-05-05
            * MODIFICADO POR:          
            * FECHA DE MODIFICACION:   
            */

            Int32 iResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_InfoAseguradoC_Actualizar]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idInfoAsegurado", SqlDbType.Int);
                sqlParam1.Value = pObjBEInfoAseguradoC.IdInfoAsegurado;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idCertificado", SqlDbType.VarChar, 25);
                sqlParam2.Value = pObjBEInfoAseguradoC.IdCertificado;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@consecutivo", SqlDbType.SmallInt);
                sqlParam3.Value = pObjBEInfoAseguradoC.Consecutivo;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@ValorNum", SqlDbType.Decimal);
                if (pObjBEInfoAseguradoC.ValorNum == NullTypes.DecimalNull) { sqlParam4.Value = DBNull.Value; } else { sqlParam4.Value = pObjBEInfoAseguradoC.ValorNum; }

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@ValorDate", SqlDbType.SmallDateTime);
                if (pObjBEInfoAseguradoC.ValorDate == NullTypes.FechaNull) { sqlParam5.Value = DBNull.Value; } else { sqlParam5.Value = pObjBEInfoAseguradoC.ValorDate; }

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@ValorString", SqlDbType.VarChar, 250);
                if (pObjBEInfoAseguradoC.ValorString == NullTypes.CadenaNull) { sqlParam6.Value = DBNull.Value; } else { sqlParam6.Value = pObjBEInfoAseguradoC.ValorString; }

                iResult = sqlCmd.ExecuteNonQuery();
            }

            return iResult;
        }
        #endregion

        #region NoTransaccional
        /// <summary>
        /// Permite obtener un infoAseguradoC por código y certificado.
        /// </summary>
        /// <param name="piIdInfoAsegurado">Código de infoAsegurado.</param>
        /// <param name="psIdCertificado">Código de certificado.</param>
        /// <param name="pSqlCn">Objeto de conexión a la BD.</param>
        /// <returns>El registro de un infoAseguradoC.</returns>
        public SqlDataReader Obtener(Int32 piIdInfoAsegurado, String psIdCertificado, SqlConnection pSqlCn)
        {
            /*
            * CREADO POR:              Gary Porras Paraguay
            * FECHA DE CREACION:       2011-05-05
            * MODIFICADO POR:          
            * FECHA DE MODIFICACION:   
            */

            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idInfoAsegurado", SqlDbType.Int);
                sqlParam1.Value = piIdInfoAsegurado;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idCertificado", SqlDbType.VarChar, 25);
                sqlParam2.Value = psIdCertificado;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);
            }

            return sqlDr;
        }

        /// <summary>
        /// Permite obtener los infoAseguradoC por certificado.
        /// </summary>
        /// <param name="pcIdCertificado">Código de certificado.</param>
        /// <param name="pSqlCn">Objeto de conexión a la BD.</param>
        /// <returns>Los registros de infoAseguradoC.</returns>
        public SqlDataReader ListarxCertificado(String pcIdCertificado, SqlConnection pSqlCn)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-12-20
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("BAIS_InfoAseguradoC_ListarxCertificado", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idCertificado", SqlDbType.VarChar, 25);
                sqlParam1.Value = pcIdCertificado;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }
        #endregion
    }
}
