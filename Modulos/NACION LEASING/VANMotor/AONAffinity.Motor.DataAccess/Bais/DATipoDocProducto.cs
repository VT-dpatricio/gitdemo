﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessEntity.Bais;

namespace AONAffinity.Motor.DataAccess.Bais
{
    public class DATipoDocProducto
    {
        #region NoTransaccional
        /// <summary>
        /// Permite listar los tipos de documentos por producto.
        /// </summary>
        /// <param name="pnIdProducto">Código de producto.</param>
        /// <param name="pSqlCn">Objeto que permite la conexion a la BD.</param>
        /// <returns>Objeto de tipo SqlDataReader conteniendo los tipos de documentos.</returns>
        public SqlDataReader ListarxProducto(Int32 pnIdProducto, SqlConnection pSqlCn)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-11-29
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[BAIS_TipoDocProducto_ListarxProducto]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }
        #endregion
    }
}
