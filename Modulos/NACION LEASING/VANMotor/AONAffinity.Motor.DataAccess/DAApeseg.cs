﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient; 
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.DataAccess
{
    public class DAApeseg
    {
        public SqlDataReader ObtenerDisponible(Int32 pnIdModelo, Int32 pnIdAnio, SqlConnection pSqlcn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Apeseg_ObtenerDisp]", pSqlcn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;


                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idModelo", SqlDbType.Int);
                sqlParam1.Value = pnIdModelo;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idAnio ", SqlDbType.Int);
                sqlParam2.Value = pnIdAnio;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);
            }

            return sqlDr;
        }
    }
}
