﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessEntity; 

namespace AONAffinity.Motor.DataAccess
{
    /// <summary>
    /// Clase de acceso a datos a la tabla Cliente.
    /// </summary>
    public class DACliente
    {
        #region NoTransaccional
        public SqlDataReader ListarxProceso(Int32 pnIdProceso, SqlConnection pSqlCn)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.:2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.:____-__-__
            ---------------------------------------------------------------------------*/
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cliente_ListarxProceso]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam = sqlCmd.Parameters.Add("@idProceso", SqlDbType.Int);
                sqlParam.Value = pnIdProceso;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }

        public SqlDataReader ListarxCotizar(SqlConnection pSqlCn)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.:2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.:____-__-__
            ---------------------------------------------------------------------------*/
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cliente_ListarxCotizar]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }

        public SqlDataReader Obtener(String pcNroMotor, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cliente_Obtener]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@nroMotor", SqlDbType.VarChar, 25);
                sqlParam1.Value = pcNroMotor;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);
            }

            return sqlDr;
        }

        public SqlDataReader ObtenerxPlaca(String pcPlaca, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cliente_ObtenerxPlaca]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@nroPlaca", SqlDbType.VarChar, 25);
                sqlParam1.Value = pcPlaca;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);
            }

            return sqlDr;
        }

        public SqlDataReader ObtenerxSerie(String pcSerie, SqlConnection pSqlCn) 
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cliente_ObtenerxSerie]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@nroSerie", SqlDbType.VarChar, 25);
                sqlParam1.Value = pcSerie;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);
            }

            return sqlDr;
        }
        #endregion

        #region Transaccional
        public Int32 Insertar(BECliente pObjBECliente, SqlConnection pSqlCn)
        {
            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cliente_Insertar]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@nroMotor", SqlDbType.VarChar, 25);
                sqlParam1.Value = pObjBECliente.NroMotor;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@nroCertificado", SqlDbType.VarChar, 15);
                if (pObjBECliente.NroCertificado == NullTypes.CadenaNull) { sqlParam2.Value = DBNull.Value; } else { sqlParam2.Value = pObjBECliente.NroMotor; }

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@codAsegurador", SqlDbType.Int);
                sqlParam3.Value = pObjBECliente.CodAsegurador;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@apePaterno", SqlDbType.VarChar, 60);
                sqlParam4.Value = pObjBECliente.ApePaterno;

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@apeMaterno", SqlDbType.VarChar, 60);
                sqlParam5.Value = pObjBECliente.ApeMaterno;

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@priNombre", SqlDbType.VarChar, 60);
                sqlParam6.Value = pObjBECliente.PriNombre;

                SqlParameter sqlParam7 = sqlCmd.Parameters.Add("@segNombre", SqlDbType.VarChar, 60);
                if (pObjBECliente.SegNombre == NullTypes.CadenaNull) { sqlParam7.Value = DBNull.Value; } else { sqlParam7.Value = pObjBECliente.SegNombre; }

                SqlParameter sqlParam8 = sqlCmd.Parameters.Add("@idTipoDocumento", SqlDbType.VarChar, 3);
                sqlParam8.Value = pObjBECliente.IdTipoDocumento;

                SqlParameter sqlParam9 = sqlCmd.Parameters.Add("@nroDocumento", SqlDbType.VarChar, 20);
                sqlParam9.Value = pObjBECliente.NroDocumento;

                SqlParameter sqlParam10 = sqlCmd.Parameters.Add("@sexo", SqlDbType.VarChar, 1);
                if (pObjBECliente.Sexo == NullTypes.CadenaNull) { sqlParam10.Value = DBNull.Value; } else { sqlParam10.Value = pObjBECliente.Sexo; }

                SqlParameter sqlParam11 = sqlCmd.Parameters.Add("@fecNacimiento", SqlDbType.SmallDateTime);
                if (pObjBECliente.FecNacimiento == NullTypes.FechaNull) { sqlParam11.Value = DBNull.Value; } else { sqlParam11.Value = pObjBECliente.FecNacimiento; }

                SqlParameter sqlParam12 = sqlCmd.Parameters.Add("@idCiudad", SqlDbType.Int);
                if (pObjBECliente.idCiudad == NullTypes.IntegerNull) { sqlParam12.Value = DBNull.Value; } else { sqlParam12.Value = pObjBECliente.idCiudad; }

                SqlParameter sqlParam13 = sqlCmd.Parameters.Add("@direccion", SqlDbType.VarChar, 250);
                sqlParam13.Value = pObjBECliente.Direccion;

                SqlParameter sqlParam14 = sqlCmd.Parameters.Add("@telDomicilio1", SqlDbType.VarChar, 15);
                sqlParam14.Value = pObjBECliente.TelDomicilio1;

                SqlParameter sqlParam15 = sqlCmd.Parameters.Add("@telDomicilio2", SqlDbType.VarChar, 15);
                if (pObjBECliente.TelDomicilio2 == NullTypes.CadenaNull) { sqlParam15.Value = DBNull.Value; } else { sqlParam15.Value = pObjBECliente.TelDomicilio2; }

                SqlParameter sqlParam16 = sqlCmd.Parameters.Add("@telDomicilio3", SqlDbType.VarChar, 15);
                if (pObjBECliente.TelDomicilio3 == NullTypes.CadenaNull) { sqlParam16.Value = DBNull.Value; } else { sqlParam16.Value = pObjBECliente.TelDomicilio3; }

                SqlParameter sqlParam17 = sqlCmd.Parameters.Add("@telMovil1", SqlDbType.VarChar, 15);
                if (pObjBECliente.TelMovil1 == NullTypes.CadenaNull) { sqlParam17.Value = DBNull.Value; } else { sqlParam17.Value = pObjBECliente.TelMovil1; }

                SqlParameter sqlParam18 = sqlCmd.Parameters.Add("@telMovil2", SqlDbType.VarChar, 15);
                if (pObjBECliente.TelMovil2 == NullTypes.CadenaNull) { sqlParam18.Value = DBNull.Value; } else { sqlParam18.Value = pObjBECliente.TelMovil2; }

                SqlParameter sqlParam19 = sqlCmd.Parameters.Add("@telMovil3", SqlDbType.VarChar, 15);
                if (pObjBECliente.TelMovil3 == NullTypes.CadenaNull) { sqlParam19.Value = DBNull.Value; } else { sqlParam19.Value = pObjBECliente.TelMovil3; }

                SqlParameter sqlParam20 = sqlCmd.Parameters.Add("@telOficina1", SqlDbType.VarChar, 15);
                if (pObjBECliente.TelOficina1 == NullTypes.CadenaNull) { sqlParam20.Value = DBNull.Value; } else { sqlParam20.Value = pObjBECliente.TelOficina1; }

                SqlParameter sqlParam21 = sqlCmd.Parameters.Add("@telOficina2", SqlDbType.VarChar, 15);
                if (pObjBECliente.TelOficina2 == NullTypes.CadenaNull) { sqlParam21.Value = DBNull.Value; } else { sqlParam21.Value = pObjBECliente.TelOficina2; }

                SqlParameter sqlParam22 = sqlCmd.Parameters.Add("@telOficina3", SqlDbType.VarChar, 15);
                if (pObjBECliente.TelOficina3 == NullTypes.CadenaNull) { sqlParam22.Value = DBNull.Value; } else { sqlParam22.Value = pObjBECliente.TelOficina3; }

                SqlParameter sqlParam23 = sqlCmd.Parameters.Add("@email1", SqlDbType.VarChar, 50);
                if (pObjBECliente.Email1 == NullTypes.CadenaNull) { sqlParam23.Value = DBNull.Value; } else { sqlParam23.Value = pObjBECliente.Email1; }

                SqlParameter sqlParam24 = sqlCmd.Parameters.Add("@email2", SqlDbType.VarChar, 50);
                if (pObjBECliente.Email2 == NullTypes.CadenaNull) { sqlParam24.Value = DBNull.Value; } else { sqlParam24.Value = pObjBECliente.Email2; }

                SqlParameter sqlParam25 = sqlCmd.Parameters.Add("@fecVigencia", SqlDbType.SmallDateTime);
                if (pObjBECliente.FecVigencia == NullTypes.FechaNull) { sqlParam25.Value = DBNull.Value; } else { sqlParam25.Value = pObjBECliente.FecVigencia; }

                SqlParameter sqlParam26 = sqlCmd.Parameters.Add("@fecFinVigencia", SqlDbType.SmallDateTime);
                if (pObjBECliente.FecFinVigencia == NullTypes.FechaNull) { sqlParam26.Value = DBNull.Value; } else { sqlParam26.Value = pObjBECliente.FecFinVigencia; }

                SqlParameter sqlParam27 = sqlCmd.Parameters.Add("@idMarca", SqlDbType.Int);
                sqlParam27.Value = pObjBECliente.IdMarca;

                SqlParameter sqlParam28 = sqlCmd.Parameters.Add("@idModelo", SqlDbType.Int);
                sqlParam28.Value = pObjBECliente.IdModelo;

                SqlParameter sqlParam29 = sqlCmd.Parameters.Add("@nroPlaca", SqlDbType.VarChar, 20);
                if (pObjBECliente.NroPlaca == NullTypes.CadenaNull) { sqlParam29.Value = DBNull.Value; } else { sqlParam29.Value = pObjBECliente.NroPlaca; }

                SqlParameter sqlParam30 = sqlCmd.Parameters.Add("@anioFab", SqlDbType.Int);
                sqlParam30.Value = pObjBECliente.AnioFab;

                SqlParameter sqlParam31 = sqlCmd.Parameters.Add("@valorVehiculo", SqlDbType.Decimal);
                sqlParam31.Value = pObjBECliente.ValorVehiculo;

                SqlParameter sqlParam32 = sqlCmd.Parameters.Add("@nroSerie", SqlDbType.VarChar, 25);
                sqlParam32.Value = pObjBECliente.NroSerie;

                SqlParameter sqlParam33 = sqlCmd.Parameters.Add("@color", SqlDbType.VarChar, 25);
                sqlParam33.Value = pObjBECliente.Color;

                SqlParameter sqlParam34 = sqlCmd.Parameters.Add("@idUsoVehiculo", SqlDbType.Int);
                sqlParam34.Value = pObjBECliente.IdUsoVehiculo;

                SqlParameter sqlParam35 = sqlCmd.Parameters.Add("@idClase", SqlDbType.Int);
                sqlParam35.Value = pObjBECliente.IdClase;

                SqlParameter sqlParam36 = sqlCmd.Parameters.Add("@nroAsientos", SqlDbType.SmallInt);
                if (pObjBECliente.NroAsientos == NullTypes.IntegerNull) { sqlParam36.Value = DBNull.Value; } else { sqlParam36.Value = pObjBECliente.NroAsientos; }

                SqlParameter sqlParam37 = sqlCmd.Parameters.Add("@esTimonCambiado", SqlDbType.Bit);
                if (pObjBECliente.EsTimonCambiado == NullTypes.BoolNull) { sqlParam37.Value = DBNull.Value; } else { sqlParam37.Value = pObjBECliente.EsTimonCambiado; }

                SqlParameter sqlParam38 = sqlCmd.Parameters.Add("@esBlindado", SqlDbType.Bit);
                if (pObjBECliente.EsBlindado == NullTypes.BoolNull) { sqlParam38.Value = DBNull.Value; } else { sqlParam38.Value = pObjBECliente.EsBlindado; }

                SqlParameter sqlParam39 = sqlCmd.Parameters.Add("@reqGPS", SqlDbType.Bit);
                if (pObjBECliente.ReqGPS == NullTypes.BoolNull) { sqlParam39.Value = DBNull.Value; } else { sqlParam39.Value = pObjBECliente.ReqGPS; }

                SqlParameter sqlParam40 = sqlCmd.Parameters.Add("@idSponsor", SqlDbType.Int);
                if (pObjBECliente.IdSponsor == NullTypes.IntegerNull) { sqlParam40.Value = DBNull.Value; } else { sqlParam40.Value = pObjBECliente.IdSponsor; }

                SqlParameter sqlParam41 = sqlCmd.Parameters.Add("@idProceso", SqlDbType.Int);
                sqlParam41.Value = pObjBECliente.IdProceso;

                SqlParameter sqlParam42 = sqlCmd.Parameters.Add("@idEstadoCliente", SqlDbType.Int);
                sqlParam42.Value = pObjBECliente.IdEstadoCliente;

                SqlParameter sqlParam43 = sqlCmd.Parameters.Add("@usrCreacion", SqlDbType.VarChar, 50);
                sqlParam43.Value = pObjBECliente.UsuarioCreacion;

                SqlParameter sqlParam44 = sqlCmd.Parameters.Add("stsRegistro", SqlDbType.Bit);
                sqlParam44.Value = pObjBECliente.EstadoRegistro;

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        }

        /// <summary>
        /// Permite actualizar el estado del cliente.
        /// </summary>        
        /// <param name="pcNroMotor">Nro de motor.</param>
        /// <param name="pnEstadoCliente">Valor del nuevo estado del cliente.</param>
        /// <param name="pcUsuario">Código de usuario de actualización.</param>
        /// <param name="pSqlCn">Objeto que permite la conexión as la BD.</param>
        /// <returns>0 No Actualozó | 1 Si Actualizó</returns>
        public Int32 Atualizar(String pcNroMotor, Int32 pnEstadoCliente, String pcUsuario, SqlConnection pSqlCn)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.:2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.:____-__-__
            ---------------------------------------------------------------------------*/

            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Cliente_AtualizarEstCli]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@nroMotor", SqlDbType.VarChar, 25);
                sqlParam1.Value = pcNroMotor;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@estadoCliente", SqlDbType.Int);
                sqlParam2.Value = pnEstadoCliente;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@usrModificacion", SqlDbType.VarChar, 50);
                sqlParam3.Value = pcUsuario;

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        }
        #endregion
    }
}
