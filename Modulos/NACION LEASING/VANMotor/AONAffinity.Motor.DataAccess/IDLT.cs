﻿using System.Data;
using System.Diagnostics;
using System.Data.SqlClient;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.DataAccess
{
    interface IDLT
    {
        bool Actualizar(BEBase pEntidad);
        bool Eliminar(int pCodigo);
        bool Insertar(BEBase pEntidad);
        SqlCommand LlenarEstructura(BEBase pEntidad, SqlCommand pcm, string TipoTransaccion);
    }
}
