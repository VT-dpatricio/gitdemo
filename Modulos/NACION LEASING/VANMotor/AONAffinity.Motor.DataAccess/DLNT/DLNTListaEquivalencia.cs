﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.DataAccess.DLNT
{
    public class DLNTListaEquivalencia : DLBase, IDLNT
    {

        System.Collections.IList IDLNT.Seleccionar()
        {
            throw new NotImplementedException();
        }

        public System.Collections.IList Seleccionar(BusinessEntity.BEBase pEntidad)
        {
            SqlConnection cn = new SqlConnection(base.CadenaConexion());
            SqlCommand cm = new SqlCommand("BW_ListarEquivalencia", cn);
            cm.CommandType = CommandType.StoredProcedure;
            BEListaEquivalencia oBE = (BEListaEquivalencia)pEntidad;
            cm.Parameters.Add("@IdProducto", SqlDbType.Int).Value = oBE.IdProducto;
            cm.Parameters.Add("@Tabla", SqlDbType.VarChar, 50).Value = oBE.Tabla;
            ArrayList lista = new ArrayList();
            try
            {
                cn.Open();
                SqlDataReader rd = cm.ExecuteReader();
                while (rd.Read())
                {
                    oBE = new BEListaEquivalencia();
                    oBE.ValorAON = rd.GetString(rd.GetOrdinal("ValorAON"));
                    oBE.ValorEquivalencia = rd.GetString(rd.GetOrdinal("ValorEquivalencia"));
                    oBE.Descripcion = rd.GetString(rd.GetOrdinal("Descripcion"));
                    lista.Add(oBE);
                    oBE = null;
                }
                rd.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if ((cn.State == ConnectionState.Open))
                {
                    cn.Close();
                }
            }
            return lista;
        }

        System.Collections.IList IDLNT.Seleccionar(int pCodigo)
        {
            throw new NotImplementedException();
        }

        BusinessEntity.BEBase IDLNT.SeleccionarBE(int pCodigo)
        {
            throw new NotImplementedException();
        }
    }
}
