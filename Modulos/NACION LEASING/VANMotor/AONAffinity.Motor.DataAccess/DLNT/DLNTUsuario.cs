﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AONAffinity.Motor.BusinessEntity;
using System.Data.SqlClient;
using System.Data;

namespace AONAffinity.Motor.DataAccess.DLNT
{
    public class DLNTUsuario : DLBase, IDLNT
    {

        public System.Collections.IList Seleccionar()
        {
            throw new NotImplementedException();
        }

        public System.Collections.IList Seleccionar(BusinessEntity.BEBase pEntidad)
        {
            throw new NotImplementedException();
        }

        public System.Collections.IList Seleccionar(int pCodigo)
        {
            throw new NotImplementedException();
        }

        public BusinessEntity.BEBase SeleccionarBE(int pCodigo)
        {
            throw new NotImplementedException();
        }

        public BEUsuarioBW SeleccionarE(string pCodigo, Int32 pIDSistema)
        {
            SqlConnection cn = new SqlConnection(base.CadenaConexion());
            SqlCommand cm = new SqlCommand("BW_SeleccionarUsuarioxID", cn);
            cm.CommandType = CommandType.StoredProcedure;
            cm.Parameters.Add("@IDUsuario", SqlDbType.VarChar, 50).Value = pCodigo;
            cm.Parameters.Add("@IDSistema", SqlDbType.Int).Value = pIDSistema;
            BEUsuarioBW oBE = new BEUsuarioBW();
            try
            {
                cn.Open();
                SqlDataReader rd = cm.ExecuteReader();
                if (rd.Read())
                {
                    oBE.IDUsuario = rd.GetString(rd.GetOrdinal("IDUsuario"));
                    oBE.IDInformador = rd.GetString(rd.GetOrdinal("IDInformador"));

                    oBE.Nombre = rd.GetString(rd.GetOrdinal("Nombre"));
                    oBE.Nombre1 = rd.GetString(rd.GetOrdinal("Nombre1"));
                    oBE.Nombre2 = rd.GetString(rd.GetOrdinal("Nombre2"));
                    oBE.Apellido1 = rd.GetString(rd.GetOrdinal("Apellido1"));
                    oBE.Apellido2 = rd.GetString(rd.GetOrdinal("Apellido2"));

                    oBE.IDEntidad = rd.GetInt32(rd.GetOrdinal("IDEntidad"));
                    oBE.IDOficina = rd.GetInt32(rd.GetOrdinal("IDOficina"));
                    oBE.IDCargo = rd.GetInt32(rd.GetOrdinal("IDCargo"));
                    oBE.IDCanal = rd.GetInt32(rd.GetOrdinal("IDCanal"));

                    oBE.DNI = rd.GetString(rd.GetOrdinal("CC"));
                    oBE.Estado = rd.GetBoolean(rd.GetOrdinal("Activo"));
                    oBE.Acceso = rd.GetBoolean(rd.GetOrdinal("Acceso"));
                }
                rd.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if ((cn.State == ConnectionState.Open))
                {
                    cn.Close();
                }
            }
            return oBE;
        }

    }
}
