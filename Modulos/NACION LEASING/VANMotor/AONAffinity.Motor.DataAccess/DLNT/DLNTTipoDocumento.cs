﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.DataAccess.DLNT
{
    public class DLNTTipoDocumento : DLBase, IDLNT
    {

        System.Collections.IList IDLNT.Seleccionar()
        {
            throw new NotImplementedException();
        }

        public System.Collections.IList Seleccionar(BusinessEntity.BEBase pEntidad)
        {
            throw new NotImplementedException();
        }

        public System.Collections.IList Seleccionar(int pCodigo)
        {            
            SqlConnection cn = new SqlConnection(base.CadenaConexion());
            SqlCommand cm = new SqlCommand("BW_ListarTipoDocumento", cn);
            cm.CommandType = CommandType.StoredProcedure;
            BETipoDocProducto oBE;
            cm.Parameters.Add("@IdEntidad", SqlDbType.Int).Value = pCodigo;            
            ArrayList lista = new ArrayList();
            try
            {
                cn.Open();
                SqlDataReader rd = cm.ExecuteReader();
                while (rd.Read())
                {
                    oBE = new BETipoDocProducto();
                    oBE.IdTipoDocumento = rd.GetString(rd.GetOrdinal("IdTipoDocumento"));
                    oBE.Nombre = rd.GetString(rd.GetOrdinal("Nombre"));                    
                    lista.Add(oBE);
                    oBE = null;
                }
                rd.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if ((cn.State == ConnectionState.Open))
                {
                    cn.Close();
                }
            }
            return lista;
        }

        BusinessEntity.BEBase IDLNT.SeleccionarBE(int pCodigo)
        {
            throw new NotImplementedException();
        }
    }
}
