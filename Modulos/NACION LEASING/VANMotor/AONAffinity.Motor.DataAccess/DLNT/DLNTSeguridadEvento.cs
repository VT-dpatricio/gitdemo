﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using AONAffinity.Motor.BusinessEntity;
using System.Collections;
using System.Data;

namespace AONAffinity.Motor.DataAccess.DLNT
{
    public class DLNTSeguridadEvento : DLBase, IDLNT
    {
        public BESeguridadEvento SeleccionarUltimo(string pUsuario, int pIDTipoEvento)
        {
            SqlConnection cn = new SqlConnection(CadenaConexion());
            SqlCommand cm = new SqlCommand("SE_SeleccionarEventoUltimo", cn);
            cm.CommandType = CommandType.StoredProcedure;
            cm.Parameters.Add("@IDUsuario", SqlDbType.VarChar, 50).Value = pUsuario;
            cm.Parameters.Add("@IDTipoEvento", SqlDbType.Int).Value = pIDTipoEvento;
            BESeguridadEvento oBE = new BESeguridadEvento();
            try
            {
                cn.Open();
                SqlDataReader rd = cm.ExecuteReader();
                if (rd.Read())
                {
                    oBE.IDEvento = rd.GetInt32(rd.GetOrdinal("IDEvento"));
                    oBE.IDSistema = rd.GetInt32(rd.GetOrdinal("IDSistema"));
                    oBE.FechaRegistro = rd.GetDateTime(rd.GetOrdinal("FechaRegistro"));
                    oBE.Detalle = rd.GetString(rd.GetOrdinal("Detalle"));
                }
                rd.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if ((cn.State == ConnectionState.Open))
                {
                    cn.Close();
                }
            }
            return oBE;
        }


        public IList Seleccionar()
        {
            throw new NotImplementedException();
        }

        public IList Seleccionar(BEBase pEntidad)
        {
            throw new NotImplementedException();
        }

        public IList Seleccionar(int pCodigo)
        {
            throw new NotImplementedException();
        }

        public BEBase SeleccionarBE(int pCodigo)
        {
            throw new NotImplementedException();
        }
    }
}
