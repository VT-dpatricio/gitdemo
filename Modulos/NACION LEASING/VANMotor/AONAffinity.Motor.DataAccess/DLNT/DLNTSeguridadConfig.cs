﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using AONAffinity.Motor.BusinessEntity;
using System.Collections;
using System.Data;

namespace AONAffinity.Motor.DataAccess.DLNT
{
    public class DLNTSeguridadConfig : DLBase, IDLNT
    {

        public System.Collections.IList Seleccionar()
        {
            throw new NotImplementedException();
        }

        public System.Collections.IList Seleccionar(BusinessEntity.BEBase pEntidad)
        {
            throw new NotImplementedException();
        }

        public System.Collections.IList Seleccionar(int pCodigo)
        {
            SqlConnection cn = new SqlConnection(base.CadenaConexion());
            SqlCommand cm = new SqlCommand("SE_SeleccionarConfiguracion", cn);
            cm.CommandType = CommandType.StoredProcedure;
            BESeguridadConfig oBE = default(BESeguridadConfig);
            cm.Parameters.Add("@IDEntidad", SqlDbType.Int).Value = pCodigo;
            ArrayList lista = new ArrayList();
            try
            {
                cn.Open();
                SqlDataReader rd = cm.ExecuteReader();
                while (rd.Read())
                {
                    oBE = new BESeguridadConfig();
                    oBE.IDParametro = rd.GetString(rd.GetOrdinal("IDParametro"));
                    oBE.Valor = rd.GetString(rd.GetOrdinal("Valor"));
                    lista.Add(oBE);
                    oBE = null;
                }
                rd.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if ((cn.State == ConnectionState.Open))
                {
                    cn.Close();
                }
            }
            return lista;
        }

        public BusinessEntity.BEBase SeleccionarBE(int pCodigo)
        {
            throw new NotImplementedException();
        }
    }
}
