﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;  

namespace AONAffinity.Motor.DataAccess
{
    public class DACotizacionDetalle
    {
        public Int32 Insertar(BECotizacionDetalle pObjBECotizacionDetalle, SqlConnection pSqlCn, SqlTransaction pSqlTrs) 
        {
            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_CotizacionDetalle_Insertar]", pSqlCn)) 
            {
                sqlCmd.Transaction = pSqlTrs;
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam0 = sqlCmd.Parameters.Add("@correlativo", SqlDbType.Int);
                sqlParam0.Value = pObjBECotizacionDetalle.Correlativo;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@nroCotizacion", SqlDbType.Decimal);
                sqlParam1.Value = pObjBECotizacionDetalle.NroCotizacion;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam2.Value = pObjBECotizacionDetalle.IdProducto;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@valorAsegurado", SqlDbType.Decimal);
                sqlParam3.Value = pObjBECotizacionDetalle.ValorAsegurado;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@idTasa", SqlDbType.Int);
                sqlParam4.Value = pObjBECotizacionDetalle.IdTasa;

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@impDescuento", SqlDbType.Decimal);
                sqlParam5.Value = pObjBECotizacionDetalle.ImpDescuento;

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@impRecargo", SqlDbType.Decimal);
                sqlParam6.Value = pObjBECotizacionDetalle.ImpRecargo;

                SqlParameter sqlParam7 = sqlCmd.Parameters.Add("@porcDescuento", SqlDbType.Decimal);
                sqlParam7.Value = pObjBECotizacionDetalle.PorcDescuento;

                SqlParameter sqlParam8 = sqlCmd.Parameters.Add("@porcRecargo", SqlDbType.Decimal);
                sqlParam8.Value = pObjBECotizacionDetalle.PorcRecargo;

                SqlParameter sqlParam9 = sqlCmd.Parameters.Add("@primaCosto", SqlDbType.Decimal);
                sqlParam9.Value = pObjBECotizacionDetalle.PrimaCosto;

                SqlParameter sqlParam10 = sqlCmd.Parameters.Add("@primaTotal", SqlDbType.Decimal);
                sqlParam10.Value = pObjBECotizacionDetalle.PrimaTotal;

                SqlParameter sqlParam11 = sqlCmd.Parameters.Add("@primaMensual", SqlDbType.Decimal);
                sqlParam11.Value = pObjBECotizacionDetalle.PrimaMensual;

                SqlParameter sqlParam12 = sqlCmd.Parameters.Add("@monedaPrima", SqlDbType.VarChar, 15);
                sqlParam12.Value = pObjBECotizacionDetalle.MonedaPrima;

                SqlParameter sqlParam13 = sqlCmd.Parameters.Add("@opcionPlan", SqlDbType.VarChar, 25);
                sqlParam13.Value = pObjBECotizacionDetalle.OpcionPlan;
					
                SqlParameter sqlParam14 = sqlCmd.Parameters.Add("@elegido", SqlDbType.Bit);
                sqlParam14.Value = pObjBECotizacionDetalle.Elegido;
					            
                SqlParameter sqlParam15 = sqlCmd.Parameters.Add("@usrCreacion", SqlDbType.VarChar, 50);
                sqlParam15.Value = pObjBECotizacionDetalle.UsuarioCreacion;

                SqlParameter sqlParam16 = sqlCmd.Parameters.Add("@idPromocion", SqlDbType.Int);
                sqlParam16.Value = pObjBECotizacionDetalle.IdPromocion;
                
                //Nuevos
                SqlParameter sqlParam17 = sqlCmd.Parameters.Add("@IdCategoria", SqlDbType.Int);
                sqlParam17.Value = pObjBECotizacionDetalle.IdCategoria;

                SqlParameter sqlParam18 = sqlCmd.Parameters.Add("@IdDescuento", SqlDbType.Int);
                sqlParam18.Value = pObjBECotizacionDetalle.IdDescuento;

                SqlParameter sqlParam19 = sqlCmd.Parameters.Add("@IdRecargo", SqlDbType.Int);
                sqlParam19.Value = pObjBECotizacionDetalle.IdRecargo;

                SqlParameter sqlParam20 = sqlCmd.Parameters.Add("@IdAdicional", SqlDbType.Int);
                sqlParam20.Value = pObjBECotizacionDetalle.IdAdicional;

                SqlParameter sqlParam21 = sqlCmd.Parameters.Add("@impAdicional", SqlDbType.Decimal);
                sqlParam21.Value = pObjBECotizacionDetalle.MontoAdicional;

                SqlParameter sqlParam22 = sqlCmd.Parameters.Add("@PagoContado", SqlDbType.Bit);
                sqlParam22.Value = pObjBECotizacionDetalle.PagoContado;

                SqlParameter sqlParam23 = sqlCmd.Parameters.Add("@Tasa", SqlDbType.Decimal);
                sqlParam23.Value = pObjBECotizacionDetalle.Tasa;
                
                SqlParameter sqlParam24 = sqlCmd.Parameters.Add("@PorcTasa", SqlDbType.Decimal);
                sqlParam24.Value = pObjBECotizacionDetalle.PorcTasa;
         
                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;

        }
    }
}
