﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Motor.Resources;  
using AONAffinity.Motor.BusinessEntity;  

namespace AONAffinity.Motor.DataAccess
{
    /// <summary>
    /// Clase de acceso a datos a la tabla ProductoEmail.
    /// </summary>
    public class DAProductoEmail
    {
        #region NoTransaccional
        /// <summary>
        /// Permite obteber
        /// </summary>
        /// <param name="pnIdProducto">Código Producto</param>
        /// <param name="pSqlCn">Objeto que permite la conbexion a la BD.</param>
        /// <returns>Objeto de tipo SqlDataReader.</returns>
        public SqlDataReader Obtener(Int32 pnIdProducto, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_ProductoMail_Obtener]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam.Value = pnIdProducto;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);
            }

            return sqlDr;
        }
        #endregion        
    }
}