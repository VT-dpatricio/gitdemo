﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.DataAccess
{
    public class DAProceso
    {
        #region Transaccional
        //ok
        public Int32 Insertar(BEProceso pObjBEProceso, SqlConnection pSqlCn)
        {
            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Proceso_Insertar]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@descripcion", SqlDbType.VarChar, 50);
                sqlParam1.Value = pObjBEProceso.Descripcion;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@archivo", SqlDbType.VarChar, 200);
                sqlParam2.Value = pObjBEProceso.Archivo;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@idTipProceso", SqlDbType.Int);
                sqlParam3.Value = pObjBEProceso.IdTipProceso;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@usrCreacion", SqlDbType.VarChar, 50);
                sqlParam4.Value = pObjBEProceso.UsuarioCreacion;

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        }

        //ok
        public Int32 Actualizar(BEProceso pObjBEProceso, SqlConnection pSqlCn)
        {
            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Proceso_Actualizar]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProceso", SqlDbType.Int);
                sqlParam1.Value = pObjBEProceso.IdProceso;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@descripcion", SqlDbType.VarChar, 50);
                sqlParam2.Value = pObjBEProceso.Descripcion;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@archivo", SqlDbType.VarChar, 200);
                sqlParam3.Value = pObjBEProceso.Archivo;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@procesado", SqlDbType.Bit);
                sqlParam4.Value = pObjBEProceso.Procesado;

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@idTipProceso", SqlDbType.Int);
                sqlParam5.Value = pObjBEProceso.IdProceso;

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@usrModificacion", SqlDbType.VarChar, 50);
                sqlParam6.Value = pObjBEProceso.UsuarioModificacion;

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        }

        public Int32 ActualizarLog(BEProceso pObjBEProceso, SqlConnection pSqlCn)
        {
            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("AonMotor_Proceso_ActualizarLog", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProceso", SqlDbType.Int);
                sqlParam1.Value = pObjBEProceso.IdProceso;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@archivoLog", SqlDbType.VarChar, 200);
                sqlParam2.Value = pObjBEProceso.ArchivoLog;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@procesado", SqlDbType.Bit);
                sqlParam3.Value = pObjBEProceso.Procesado;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@regTotal", SqlDbType.Int);
                sqlParam4.Value = pObjBEProceso.RegTotal;

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@regTotalVal", SqlDbType.Int);
                sqlParam5.Value = pObjBEProceso.RegTotalVal;

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@regTotalErr", SqlDbType.Int);
                sqlParam6.Value = pObjBEProceso.RegTotalErr;

                SqlParameter sqlParam7 = sqlCmd.Parameters.Add("@nroObservaciones", SqlDbType.Int);
                sqlParam7.Value = pObjBEProceso.NroObservaciones;

                SqlParameter sqlParam8 = sqlCmd.Parameters.Add("@usrModificacion", SqlDbType.VarChar, 50);
                sqlParam8.Value = pObjBEProceso.UsuarioModificacion;

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        }

        public Int32 ActualizarEstado(BEProceso pObjBEProceso, SqlConnection pSqlCn)
        {
            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Proceso_ActualizarEst]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProceso", SqlDbType.Int);
                sqlParam1.Value = pObjBEProceso.IdProceso;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@usrModificacion", SqlDbType.VarChar, 50);
                sqlParam2.Value = pObjBEProceso.UsuarioModificacion;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@stsRegistro", SqlDbType.Bit);
                sqlParam3.Value = pObjBEProceso.EstadoRegistro;

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        }

        public Int32 ActualizarCotizacion(BEProceso pObjBEProceso, SqlConnection pSqlCn)
        {
            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Proceso_ActualizarCotizacion]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProceso", SqlDbType.Int);
                sqlParam1.Value = pObjBEProceso.IdProceso;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@archivoLogCotizacion", SqlDbType.VarChar, 200);
                sqlParam2.Value = pObjBEProceso.ArchivoLogCotizacion;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@cotizado", SqlDbType.Bit);
                sqlParam3.Value = pObjBEProceso.Cotizado;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@usrModificacion", SqlDbType.VarChar, 50);
                sqlParam4.Value = pObjBEProceso.UsuarioModificacion;

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        }
        #endregion

        #region NoTransaccional
        //ok
        public SqlDataReader ListarDisponible(Int32 pnIdTipProceso, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Proceso_ListarDispxTipProc]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idTipProceso", SqlDbType.Int);
                sqlParam1.Value = pnIdTipProceso;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }

        //ok
        public SqlDataReader ListarDisponible(SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Proceso_ListarDisp]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }

        //ok
        public SqlDataReader Listar(Nullable<Boolean> pbStsRegistro, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Proceso_Listar]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@stsRegistro", SqlDbType.Bit);
                if (pbStsRegistro == NullTypes.BoolNull) { sqlParam1.Value = DBNull.Value; } else { sqlParam1.Value = pbStsRegistro; }

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }

        //ok
        public SqlDataReader Listar(Int32 pnIdTipProceso, Nullable<Boolean> pbStsRegistro, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Proceso_ListarxTipProc]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idTipProceso", SqlDbType.Int);
                sqlParam1.Value = pnIdTipProceso;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@stsRegistro", SqlDbType.Bit);
                if (pbStsRegistro == NullTypes.BoolNull) { sqlParam2.Value = DBNull.Value; } else { sqlParam2.Value = pbStsRegistro; }

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }

        public SqlDataReader GenerarTramaExport(Int32 pnIdProceso, String pcProcedure, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand(pcProcedure, pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProceso", SqlDbType.Int);
                sqlParam1.Value = pnIdProceso;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }

        //ok
        public SqlDataReader Obtener(Int32 pnIdProceso, SqlConnection pSqlCn) 
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Proceso_Obtener]", pSqlCn)) 
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProceso", SqlDbType.Int);
                sqlParam1.Value = pnIdProceso;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);    
            } 

            return sqlDr;
        }

        #endregion
    }
}
