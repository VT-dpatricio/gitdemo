﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.DataAccess
{
    public class DATipoVehiculo
    {
        #region NoTransaccional
        /// <summary>
        /// Permite listar las marcas de automóviles por estado.
        /// </summary>
        /// <param name="pbStsRegistro">Estado del registro.</param>
        /// <param name="pSqlCn">Objeto de conexión a la BD.</param>
        /// <returns>Objeto de tipo SqlDataReader.</returns>
        public SqlDataReader Listar(Nullable<Boolean> pbStsRegistro, SqlConnection pSqlCn)
        {           
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_TipoVehiculo_Listar]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@stsRegistro", SqlDbType.Bit);
                if (pbStsRegistro == NullTypes.BoolNull) { sqlParam1.Value = DBNull.Value; } else { sqlParam1.Value = pbStsRegistro; };

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }
        #endregion
    }
}
