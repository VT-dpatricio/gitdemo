﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;   
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;  
using AONAffinity.Motor.BusinessEntity;      

namespace AONAffinity.Motor.DataAccess
{
    public class DADescuentoRecargo
    {
        #region NoTransaccional
        public SqlDataReader Obtener(String pcTipo, Int32 pnValor, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_DescuentoRecargo_ObtenerxTipVal]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@tipo", SqlDbType.VarChar, 1);
                sqlParam1.Value = pcTipo;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@valor", SqlDbType.Int);
                sqlParam2.Value = pnValor;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);
            }

            return sqlDr;
        }

        public SqlDataReader ListarxGrupoProducto(Int32 pnIdGrupo, String pcTipo, Boolean? pbStsRegistro, SqlConnection pSqlCn) 
        {
            SqlDataReader sqlDr;
            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_DescuentoRecargo_ListarxGrupoProd]", pSqlCn)) 
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@IdGrupo", SqlDbType.Int);
                sqlParam1.Value = pnIdGrupo;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@Tipo", SqlDbType.VarChar, 1);
                sqlParam2.Value = pcTipo;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@StsRegistro", SqlDbType.Bit);
                if (pbStsRegistro == null) { sqlParam3.Value = DBNull.Value; } else { sqlParam3.Value = pbStsRegistro; }

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }
        #endregion
    }
}
