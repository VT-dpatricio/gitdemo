﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Motor.BusinessEntity; 

namespace AONAffinity.Motor.DataAccess
{
    public class DAProductoUsoVehiculo
    {
        #region NoTransaccional
        public SqlDataReader ListarxProducto(Int32 pnIdproducto, Boolean? pbStsRegistro, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_ProductoUsoVehiculo_ObtenerxProducto]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdproducto;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@stsRegistro", SqlDbType.Int);
                if (pbStsRegistro == null) { sqlParam2.Value = DBNull.Value; } else { sqlParam2.Value = pnIdproducto; }

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }
        #endregion
    }
}
