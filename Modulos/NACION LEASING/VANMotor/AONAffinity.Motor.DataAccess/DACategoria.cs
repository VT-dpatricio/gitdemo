﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace AONAffinity.Motor.DataAccess
{
    /// <summary>
    /// Clase de acceso a datos a la tabla Categoria.
    /// </summary>
    public class DACategoria
    {
        #region NoTransaccional
        /// <summary>
        /// Permite obtener un registro de una categoria por codigo. 
        /// </summary>
        /// <param name="pnIdCategoria">Codigo de categoria.</param>
        /// <param name="pSqlCn">Objeto que permite la conexion a la BD.</param>
        /// <returns>Objeto de tipo SqlDataReader.</returns>
        public SqlDataReader Obtener(Int32 pnIdCategoria, SqlConnection pSqlCn)
        {
            /*---------------------------------------------------------------------------
            * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2012-01-04
            * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
           ---------------------------------------------------------------------------*/

            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Categoria_Obtener]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idCategoria", SqlDbType.Int);
                sqlParam1.Value = pnIdCategoria;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);
            }

            return sqlDr;
        }

        public SqlDataReader ObtenerxModelo(Int32 pnIdModelo, SqlConnection pSqlCn)        
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Categoria_ObtenerxModelo]", pSqlCn)) 
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idModelo", SqlDbType.Int);
                sqlParam1.Value = pnIdModelo;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);
            }

            return sqlDr;
        }

        public SqlDataReader ListarxProducto(Int32 pnIdProducto, Boolean? pbStsRegistro, SqlConnection pSqlCn) 
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Categoria_LitarxProducto]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@stsRegistro", SqlDbType.Bit);
                if (pbStsRegistro == null) { sqlParam2.Value = DBNull.Value; } else { sqlParam2.Value = pbStsRegistro; }                

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }
        #endregion
    }
}
