﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;  
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.DataAccess;
using AONAffinity.Motor.BusinessEntity;        

namespace AONAffinity.Motor.DataAccess
{
    public class DAEstructuraArchivo
    {
        #region NoTransaccional
        public SqlDataReader Listar(Int32 pnIdTipoProceso, Int32 pnIdTipoArchivo, Nullable<Boolean> pbStsRegistro, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_EstructuraArchivo_ListarxTipProc]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idTipoProceso", SqlDbType.Int);
                sqlParam1.Value = pnIdTipoProceso;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idTipoArchivo", SqlDbType.Int);
                sqlParam2.Value = pnIdTipoArchivo;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@stsRegistro", SqlDbType.Bit);
                if (pbStsRegistro == NullTypes.BoolNull) { sqlParam3.Value = DBNull.Value; } else { sqlParam3.Value = pbStsRegistro; };

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }
        #endregion

        #region Transaccional
        public Int32 Actualizar(BEEstructuraArchivo pObjBEEstructuraArchivo, SqlConnection pSqlCn) 
        {
            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_EstructuraArchivo_Actualizar]", pSqlCn)) 
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idTipoProceso", SqlDbType.Int);
                sqlParam1.Value = pObjBEEstructuraArchivo.IdTipoProceso;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@orden", SqlDbType.Int);
                sqlParam2.Value = pObjBEEstructuraArchivo.Orden;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@idTipoArchivo", SqlDbType.Int);
                sqlParam3.Value = pObjBEEstructuraArchivo.IdTipoArchivo;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@idTipoDato", SqlDbType.VarChar, 1);
                sqlParam4.Value = pObjBEEstructuraArchivo.TipoDato;

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@valorFijo", SqlDbType.VarChar, 50);
                if (pObjBEEstructuraArchivo.ValorFijo == NullTypes.CadenaNull) { sqlParam5.Value = DBNull.Value; } else { sqlParam5.Value = pObjBEEstructuraArchivo.ValorFijo; }

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@nombre", SqlDbType.VarChar, 100);
                sqlParam6.Value = pObjBEEstructuraArchivo.Nombre;

                SqlParameter sqlParam7 = sqlCmd.Parameters.Add("@inicio", SqlDbType.Int);
                if (pObjBEEstructuraArchivo.Inicio == NullTypes.IntegerNull) { sqlParam7.Value = DBNull.Value; } else { sqlParam7.Value = pObjBEEstructuraArchivo.Inicio; }

                SqlParameter sqlParam8 = sqlCmd.Parameters.Add("@longitud", SqlDbType.Int);
                if (pObjBEEstructuraArchivo.Longitud == NullTypes.IntegerNull) { sqlParam8.Value = DBNull.Value; } else { sqlParam8.Value = pObjBEEstructuraArchivo.Longitud; }

                SqlParameter sqlParam9 = sqlCmd.Parameters.Add("@obligatorio", SqlDbType.Bit);
                sqlParam9.Value = pObjBEEstructuraArchivo.Obligatorio;

                SqlParameter sqlParam10 = sqlCmd.Parameters.Add("@usrModificacion", SqlDbType.VarChar, 50);
                sqlParam10.Value = pObjBEEstructuraArchivo.UsuarioModificacion;

                nResult = sqlCmd.ExecuteNonQuery();  
            }

            return nResult;
        }
        #endregion
    }
}
