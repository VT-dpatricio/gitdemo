﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;  

namespace AONAffinity.Motor.DataAccess
{
    public class DACategoriaModelo
    {
        public SqlDataReader ValidarVehiculo(Int32 pnIdProducto, Int32 pnIdModelo, Int32 pnAnioFab, SqlConnection pSqlCn) 
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_CategoriaModelo_ValidarVehiculo]", pSqlCn)) 
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idModelo", SqlDbType.Int);
                sqlParam2.Value = pnIdModelo;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@anioFab", SqlDbType.Int);
                sqlParam3.Value = pnAnioFab;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);  
            } 

            return sqlDr; 
        }

        public SqlDataReader Obtener(Int32 pnIdProducto, Int32 pnIdModelo, SqlConnection pSqlCn) 
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_CategoriaModelo_Obtener]", pSqlCn)) 
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idModelo", SqlDbType.Int);
                sqlParam2.Value = pnIdModelo;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow); 
            }

            return sqlDr;
        }
    }
}
