﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessEntity;   

namespace AONAffinity.Motor.DataAccess
{
    public class DAMarca
    {
        #region Transaccional

        public Int32 Insertar(BEMarca pObjBEMarca, SqlConnection pSqlCn)
        {
            Int32 nResult = 0;
            using (SqlCommand sqlCmd = new SqlCommand("AonMotor_Marca_Insertar", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;                
                sqlCmd.Parameters.Add("@descripcion", SqlDbType.VarChar, 100).Value = pObjBEMarca.Descripcion;
                sqlCmd.Parameters.Add("@codExterno", SqlDbType.VarChar, 15).Value = pObjBEMarca.CodExterno;
                sqlCmd.Parameters.Add("@Usuario", SqlDbType.VarChar, 50).Value = pObjBEMarca.UsuarioCreacion;
                sqlCmd.Parameters.Add("@stsRegistro", SqlDbType.Bit).Value = pObjBEMarca.EstadoRegistro;
                nResult = sqlCmd.ExecuteNonQuery();
            }
            return nResult;
        }

        public Int32 Actualizar(BEMarca pObjBEMarca, SqlConnection pSqlCn)
        {
            Int32 nResult = 0;
            using (SqlCommand sqlCmd = new SqlCommand("AonMotor_Marca_Actualizar", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@idMarca", SqlDbType.Int).Value = pObjBEMarca.IdMarca;
                sqlCmd.Parameters.Add("@descripcion", SqlDbType.VarChar,100).Value = pObjBEMarca.Descripcion;
                sqlCmd.Parameters.Add("@codExterno", SqlDbType.VarChar, 15).Value = pObjBEMarca.CodExterno;
                sqlCmd.Parameters.Add("@Usuario", SqlDbType.VarChar, 50).Value = pObjBEMarca.UsuarioCreacion;
                sqlCmd.Parameters.Add("@stsRegistro", SqlDbType.Bit).Value = pObjBEMarca.EstadoRegistro;
                nResult = sqlCmd.ExecuteNonQuery();
            }
            return nResult;
        }
        #endregion

        #region NoTransaccional
        /// <summary>
        /// Permite listar las marcas de automóviles por estado.
        /// </summary>
        /// <param name="pbStsRegistro">Estado del registro.</param>
        /// <param name="pSqlCn">Objeto de conexión a la BD.</param>
        /// <returns>Objeto de tipo SqlDataReader.</returns>
        public SqlDataReader Listar(Nullable<Boolean> pbStsRegistro, SqlConnection pSqlCn)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-24
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Marca_Listar]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@stsRegistro", SqlDbType.Bit);
                if (pbStsRegistro == NullTypes.BoolNull) { sqlParam1.Value = DBNull.Value; } else { sqlParam1.Value = pbStsRegistro; };

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }

        public SqlDataReader Buscar(String pcDescripcion, Nullable<Boolean> pbStsRegistro, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;
            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Marca_Buscar]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@stsRegistro", SqlDbType.Bit);
                if (pbStsRegistro == NullTypes.BoolNull) { sqlParam1.Value = DBNull.Value; } else { sqlParam1.Value = pbStsRegistro; };
                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@descripcion", SqlDbType.VarChar, 100);
                sqlParam2.Value = pcDescripcion;
                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }
        #endregion
    }
}

