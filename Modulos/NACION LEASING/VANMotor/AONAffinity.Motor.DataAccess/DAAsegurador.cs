﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace AONAffinity.Motor.DataAccess
{
    public class DAAsegurador
    {
        public SqlDataReader Listar(Boolean pbEstado, SqlConnection pSqlCn) 
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Asegurador_Listar]", pSqlCn)) 
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam = sqlCmd.Parameters.Add("@stsRegistro", SqlDbType.Bit);
                sqlParam.Value = pbEstado;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }

        public SqlDataReader ListarxGrupoProducto(Int32 pnIdGrupo, Boolean pbEstado, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Asegurador_ListarxGrupoProd]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idGrupoProd", SqlDbType.Int);
                sqlParam1.Value = pnIdGrupo;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@stsRegistro", SqlDbType.Bit);
                sqlParam2.Value = pbEstado;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }
    }
}
