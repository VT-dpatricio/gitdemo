﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources; 
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.DataAccess
{
    public class DAProductoPoliza
    {
        public SqlDataReader Obtener(Int32 pnIdProducto, String pcOpcionPlan, String pcParametro, SqlConnection pSqlCn) 
        {
            SqlDataReader sqlDr;

            using(SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_ProductoPoliza_Obtener]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add ("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;
 
                SqlParameter sqlParam2 = sqlCmd.Parameters.Add ("@opcionPlan", SqlDbType.VarChar, 10);
                sqlParam2.Value = pcOpcionPlan;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add ("@parametro", SqlDbType.VarChar, 50);
                if (pcParametro == NullTypes.CadenaNull) { sqlParam3.Value = DBNull.Value; } else { sqlParam3.Value = pcOpcionPlan; }

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow); 
            }

            return sqlDr;
        }

    }
}
