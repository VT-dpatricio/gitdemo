﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessEntity; 

namespace AONAffinity.Motor.DataAccess
{
    public class DATasa
    {
        #region NoTransaccional
        public SqlDataReader Obtener(Int32 pnIdModelo, Int32 pnIdTipo, Int32 pnAnioAntiguedad, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Tasa_Obtener]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idModelo", SqlDbType.Int);
                sqlParam1.Value = pnIdModelo;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idTipo", SqlDbType.Int);
                sqlParam2.Value = pnIdTipo;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@anioFab", SqlDbType.Int);
                sqlParam3.Value = pnAnioAntiguedad;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);
            }

            return sqlDr;
        }
        #endregion
    }
}
