﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessEntity; 

namespace AONAffinity.Motor.DataAccess
{
    public class DATipoArchivo
    {
        public SqlDataReader ListarxTipoProceso(Int32 pnIdTipoProceso, SqlConnection pSqlCn) 
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_TipoArchivo_ObtenerxProceso]", pSqlCn)) 
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam = sqlCmd.Parameters.Add("@idTipoProceso", SqlDbType.Int);
                sqlParam.Value = pnIdTipoProceso;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);   
            }

            return sqlDr;
        }

    }
}
