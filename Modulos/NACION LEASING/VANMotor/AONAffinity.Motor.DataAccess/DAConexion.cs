﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using AONArchitectural.Security;

namespace AONAffinity.Motor.DataAccess
{
    public class DAConexion
    {
        /// <summary>
        /// Devuelve la cadena de conexion a la base de datos BDJanus.
        /// </summary>
        /// <returns>Objeto de tipo String.</returns>
        public static String ConexionBDJanus()
        {
            return ConfigurationManager.ConnectionStrings["cnBDJanus"].ConnectionString;
            //AONCipher enc = new AONCipher();
            //return enc.Decrypt(ConfigurationManager.ConnectionStrings["cnBDJanus"].ConnectionString);
        }

        /// <summary>
        /// Devuelve la cadena de conexion a la base de datos BDMotor.
        /// </summary>        
        /// <returns>Objeto de tipo String.</returns>
        public static String ConexionBDMotor()
        {
            return ConfigurationManager.ConnectionStrings["cnBDMotor"].ConnectionString;
            //AONCipher enc = new AONCipher();
            //return enc.Decrypt(ConfigurationManager.ConnectionStrings["cnBDMotor"].ConnectionString);
        }

        /// <summary>
        /// Devuelve la cadena de conexion a la base de datos BDInegration.
        /// </summary>
        /// <returns>Objeto de tipo String.</returns>
        public static String ConexionBDIntegration()
        {
            return ConfigurationManager.ConnectionStrings["cnBDIntegration"].ConnectionString;
            //AONCipher enc = new AONCipher();
            //return enc.Decrypt(ConfigurationManager.ConnectionStrings["cnBDIntegration"].ConnectionString);
        }

        public static String ConexionBDAdmTrama()
        {
            return ConfigurationManager.ConnectionStrings["cnBDAdmTrama"].ConnectionString;
            //AONCipher enc = new AONCipher();
            //return enc.Decrypt(ConfigurationManager.ConnectionStrings["cnBDAdmTrama"].ConnectionString);
        }
    }
}
