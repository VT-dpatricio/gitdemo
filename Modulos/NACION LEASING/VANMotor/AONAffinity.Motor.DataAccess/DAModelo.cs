﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;  
using System.Data.SqlClient;  
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessEntity;       

namespace AONAffinity.Motor.DataAccess
{
    public class DAModelo
    {
        #region Transaccional

        public Int32 Insertar(BEModelo pObjBEModelo, SqlConnection pSqlCn)
        {
            Int32 nResult = 0;
            using (SqlCommand sqlCmd = new SqlCommand("AonMotor_Modelo_Insertar", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@descripcion", SqlDbType.VarChar, 100).Value = pObjBEModelo.Descripcion;
                sqlCmd.Parameters.Add("@codExterno", SqlDbType.VarChar, 15).Value = pObjBEModelo.CodExterno;
                sqlCmd.Parameters.Add("@idMarca", SqlDbType.Int).Value = pObjBEModelo.IdMarca;
                sqlCmd.Parameters.Add("@idClase", SqlDbType.Int).Value = pObjBEModelo.IdClase;
                sqlCmd.Parameters.Add("@idTipoVehiculo", SqlDbType.Int).Value = pObjBEModelo.IdTipoVehiculo;
                sqlCmd.Parameters.Add("@asegurable", SqlDbType.Bit).Value = pObjBEModelo.Asegurable;
                sqlCmd.Parameters.Add("@Usuario", SqlDbType.VarChar, 50).Value = pObjBEModelo.UsuarioCreacion;
                sqlCmd.Parameters.Add("@stsRegistro", SqlDbType.Bit).Value = pObjBEModelo.EstadoRegistro;
                nResult = sqlCmd.ExecuteNonQuery();
            }
            return nResult;
        }

        public Int32 Actualizar(BEModelo pObjBEModelo, SqlConnection pSqlCn)
        {
            Int32 nResult = 0;
            using (SqlCommand sqlCmd = new SqlCommand("AonMotor_Modelo_Actualizar", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@idModelo", SqlDbType.Int).Value = pObjBEModelo.IdModelo;
                sqlCmd.Parameters.Add("@descripcion", SqlDbType.VarChar, 100).Value = pObjBEModelo.Descripcion;
                sqlCmd.Parameters.Add("@codExterno", SqlDbType.VarChar, 15).Value = pObjBEModelo.CodExterno;
                sqlCmd.Parameters.Add("@idMarca", SqlDbType.Int).Value = pObjBEModelo.IdMarca;
                sqlCmd.Parameters.Add("@idClase", SqlDbType.Int).Value = pObjBEModelo.IdClase;
                sqlCmd.Parameters.Add("@idTipoVehiculo", SqlDbType.Int).Value = pObjBEModelo.IdTipoVehiculo;
                sqlCmd.Parameters.Add("@asegurable", SqlDbType.Bit).Value = pObjBEModelo.Asegurable;
                sqlCmd.Parameters.Add("@Usuario", SqlDbType.VarChar, 50).Value = pObjBEModelo.UsuarioCreacion;
                sqlCmd.Parameters.Add("@stsRegistro", SqlDbType.Bit).Value = pObjBEModelo.EstadoRegistro;
                nResult = sqlCmd.ExecuteNonQuery();
            }
            return nResult;
        }
        #endregion

        #region NoTransaccional
        public SqlDataReader Obtener(Int32 pnIdModelo, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("AonMotor_Modelo_Obtener", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idModelo", SqlDbType.Int);
                sqlParam1.Value = pnIdModelo;                

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);
            }

            return sqlDr;
        }

        /// <summary>
        /// Permite listar los modelos de automóviles por estado.
        /// </summary>        
        /// <param name="pbStsRegistro">Estado del registro.</param>
        /// <param name="sqlCn">Objeto de conexión a la BD.</param>
        /// <returns>Objeto de tipo SqlDataReader.</returns>
        public SqlDataReader Listar(Nullable<Boolean> pbStsRegistro, SqlConnection sqlCn)
        {           
            SqlDataReader sqlDr = null;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Modelo_Listar]", sqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@stsRegistro", SqlDbType.Bit);
                if (pbStsRegistro == NullTypes.BoolNull) { sqlParam1.Value = DBNull.Value; } else { sqlParam1.Value = pbStsRegistro; }

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }

        
        public SqlDataReader ListarNoAseg(SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Modelo_ListarNoAseg]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }

        public SqlDataReader ListarxMarcaDesc(Int32 pnIdMarca, String pcDescripcion, Nullable<Boolean> pbStsRegistro, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Modelo_ListarxMarcaDesc]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idMarca", SqlDbType.Int);
                sqlParam1.Value = pnIdMarca;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@descripcion", SqlDbType.VarChar, 100);
                sqlParam2.Value = pcDescripcion;
                //if (pcDescripcion == NullTypes.CadenaNull) { sqlParam2.Value = DBNull.Value; } else { sqlParam2.Value = pcDescripcion; }

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@stsRegistro", SqlDbType.Bit);
                if (pbStsRegistro == NullTypes.BoolNull) { sqlParam3.Value = DBNull.Value; } else { sqlParam3.Value = pbStsRegistro; }

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }

        public SqlDataReader ObtenerPorCodExterno(String pcCodExternoMod, String pcCodExternoMar, Nullable<Boolean> pbAsegurable, Nullable<Boolean> pbStsRegistro, String pcCodCIA, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("AonMotor_Modelo_ObtenerPorCodExterno", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@codExternoMod", SqlDbType.VarChar, 15);
                sqlParam1.Value = pcCodExternoMod;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@codExternoMar", SqlDbType.VarChar, 15);
                sqlParam2.Value = pcCodExternoMar;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@asegurable", SqlDbType.Bit);
                if (pbAsegurable == NullTypes.BoolNull) { sqlParam3.Value = DBNull.Value; } else { sqlParam3.Value = pbAsegurable; }

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@stsRegistro", SqlDbType.Bit);
                if (pbStsRegistro == NullTypes.BoolNull) { sqlParam4.Value = DBNull.Value; } else { sqlParam4.Value = pbStsRegistro; }

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@codCIA", SqlDbType.VarChar, 15);
                if (pcCodCIA == NullTypes.CadenaNull) { sqlParam5.Value = DBNull.Value; } else { sqlParam5.Value = pcCodCIA; }

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);
            }

            return sqlDr;
        }

        public SqlDataReader ListarxMarcaClaseTipo(Int32 pnIdMarca, Int32 pnIdClase, Int32 pnIdTipoVehiculo, Nullable<Boolean> pbStsRegistro, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Modelo_ListarxMarcaClaTip]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idMarca", SqlDbType.Int);
                sqlParam1.Value = pnIdMarca;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idClase", SqlDbType.Int);
                sqlParam2.Value = pnIdClase;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@idTipoVahiculo", SqlDbType.Int);
                sqlParam3.Value = pnIdTipoVehiculo;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@stsRegistro", SqlDbType.Bit);
                sqlParam4.Value = pbStsRegistro;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }

        public SqlDataReader ListarxMarcaTipo(Int32 pnIdProducto, Int32 pnIdMarca, Int32 pnIdTipoVehiculo, Boolean? pbStsRegistro, SqlConnection pSqlCn)
        {
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Modelo_ObtenerxMarcaTipo]", pSqlCn)) 
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idMarca", SqlDbType.Int);
                sqlParam2.Value = pnIdMarca;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@idTipoVehiculo", SqlDbType.Int);
                sqlParam3.Value = pnIdTipoVehiculo;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@stsRegistro", SqlDbType.Bit);
                if (pbStsRegistro == null) { sqlParam4.Value = DBNull.Value; } else { sqlParam4.Value = pnIdTipoVehiculo; }

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult);
            }

            return sqlDr;
        }
        #endregion
    }
}
