﻿using System;
using System.Collections;
using AONAffinity.Motor.BusinessEntity;

namespace AONAffinity.Motor.DataAccess
{
    interface IDLNT
    {
        IList Seleccionar();
        IList Seleccionar(BEBase pEntidad);
        IList Seleccionar(int pCodigo);
        BEBase SeleccionarBE(int pCodigo);
    }
}
