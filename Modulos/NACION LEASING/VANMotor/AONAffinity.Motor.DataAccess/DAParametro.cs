﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessEntity;     

namespace AONAffinity.Motor.DataAccess
{
    public class DAParametro
    {
        #region NoTransaccional
        /// <summary>
        /// Permite obtener los datos de un parametro.
        /// </summary>
        /// <param name="pnIdParametro">Codigo de parámetro.</param>
        /// <param name="pbStsRegistro">Estado del registro.</param>
        /// <param name="pSqlCn">Objeto que permite la conexión a la BD.</param>
        /// <returns>Objeto de tipo SqlDataReader.</returns>
        public SqlDataReader Obtener(Int32 pnIdParametro, SqlConnection pSqlCn)
        {           
            SqlDataReader sqlDr;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Parametro_Obtener]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idParametro", SqlDbType.Int);
                sqlParam1.Value = pnIdParametro;

                sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow);
            }

            return sqlDr;
        }

        public Int32 ActualizarValNumEnt(Int32 pnIdParametro, Int32 pnValorNumeroEnt, SqlConnection pSqlCn)
        {
            Int32 nResult = 0;

            using (SqlCommand sqlCmd = new SqlCommand("[dbo].[AonMotor_Parametro_ActualizarValNumEnt]", pSqlCn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idParametro", SqlDbType.Int);
                sqlParam1.Value = pnIdParametro;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@valorNumeroEnt", SqlDbType.Int);
                sqlParam2.Value = pnValorNumeroEnt;

                nResult = sqlCmd.ExecuteNonQuery();
            }

            return nResult;
        }
        #endregion
    }
}
