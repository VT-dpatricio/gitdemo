﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using AONAffinity.Resources;
using AONAffinity.Motor.BusinessLogic;  
using AONAffinity.Business.Entity.BDMotor;
using AONAffinity.Motor.Resources;  

namespace AONConsolaMotor
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BLParametro objBLParametro = new BLParametro();
            BEParametro objBEParametro = null;
            Int32 nProcActivo = 1;
            Int32 nProcNoActivo = 0;
            Boolean bActParametro = false;

            try
            {
                objBEParametro = null; // objBLParametro.Obtener(AppSettings.ParametroProcesoAuto);

                if (objBEParametro == null) 
                {
                    throw new Exception("No existe parametro de inicador de ejecución.");
                }

                if (objBEParametro.ValorNumeroEnt == nProcActivo) 
                {
                    BLProceso objBLProceso = new BLProceso();
                    List<BEProceso> lstBEPRoceso = null;

                    lstBEPRoceso = objBLProceso.ListarDisponible();

                    if (lstBEPRoceso != null) 
                    {
                        //Si no se pudo actualizar el el parametro de proceso, generar excepción.
                        if (objBLParametro.ActualizarValNumEnt(AppSettings.ParametroProcesoAuto, nProcNoActivo) == 0)
                        {
                            throw new Exception("No se puede actualizar el parametro de ejecucion de proceso.");
                        }

                        bActParametro = true;
                        
                        //Ejecutar proceso automatico.
                        BLAutomatico objBLAutomatico = new BLAutomatico();
                        objBLAutomatico.Ejecutar(); 
                    }                   
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally 
            {
                //Validar si el parametro de proceso esta bloqueado.
                if (bActParametro == true) 
                {
                    objBLParametro.ActualizarValNumEnt(AppSettings.ParametroProcesoAuto, nProcActivo);
                }
            }            
        }        
    }
}
