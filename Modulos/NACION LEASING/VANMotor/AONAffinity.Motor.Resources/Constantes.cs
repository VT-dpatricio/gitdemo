﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.Resources
{
    /// <summary>
    /// Constantes de estado del certificado.
    /// </summary>
    public class EstadoCertificado
    {
        public const Int32 Activo = 1;
        public const Int32 Inactivo = 2;
        public const Int32 Cancelado = 3;
        public const Int32 Eliminado = 4;
        public const Int32 Cotizado = 5;
        public const Int32 PreActivo = 6;        
    }

    /// <summary>
    /// Constantes del medio de pago.
    /// </summary>
    public class MedioPago
    {
        public const String NoAplica = "NA";
    }

    /// <summary>
    /// Constantes de tipo de proceso
    /// </summary>
    public class TipoProceso 
    {
        public const Int32 ImportClienteIBK = 1;
        public const Int32 ImportSiniestroBBVA = 4;
        public const Int32 ImportClienteBBVA = 5;
        public const Int32 ImportClienteIBK_5292 = 7;
    }

    public class TipoTasa 
    {
        public const Int32 Anual =1;
        public const Int32 Bianual = 2;
        
    }

    public class CondicionCotizacion 
    {
        public const Int32 Ninguno = 0;
        public const Int32 Descuento = 1;
        public const Int32 Recargo = 2;
    }

    public class EstadoCotizacion
    {
        public const Int32 Creada = 1;
        public const Int32 Pendiente = 2;
        public const Int32 Rechazada = 3;
        public const Int32  Atendido = 4;
        public const Int32  Expedido = 5;
    }

    public class EstadoCliente
    {
        public const Int32 NoExisteValorAPESEG = 1;
        public const Int32 NoExisteValorTasa = 2;
        public const Int32 NoRenovable = 3;
        public const Int32 Cotizado = 4;
        public const Int32 NoCotizado = 5;
        public const Int32 ModeloNoAsegurable = 6;
        public const Int32 NO_ASEG_ANTIGUEDAD = 7;
        public const Int32 NO_ASEG_TIMONCAMB = 8;
        public const Int32 NO_ASEG_MINANTPICKUP = 9;
        public const Int32 NO_ASEG_BLINDADO = 10;
        public const Int32 NO_ASEG_MAXVALORVEH = 11;
        public const Int32 NO_EXISTE_TASADEPRECIACION = 12;
        public const Int32 NO_EXISTE_SINIESTROREG = 13;
    }

    public class Producto 
    {
        public const Int32 SegVehicularIBK_5250 = 5250;
        public const Int32 SegVehicularBBVA_5251 = 5251;
    }
}
