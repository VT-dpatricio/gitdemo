﻿using System;
using System.IO; 
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.Resources
{
    /// <summary>
    /// 
    /// </summary>
    public class Funciones
    {
        public static Encoding GetFileEncoding(String srcFile, FileStream fs)
        {
            byte[] buffer = new byte[5];
            fs.Read(buffer, 0, 5);
            fs.Position = 0;
            Encoding enc = Encoding.Default;
            if (buffer[0] == 0xef && buffer[1] == 0xbb && buffer[2] == 0xbf)
                enc = Encoding.UTF8;
            else if (buffer[0] == 0xfe && buffer[1] == 0xff)
                enc = Encoding.Unicode;
            else if (buffer[0] == 0 && buffer[1] == 0 && buffer[2] == 0xfe && buffer[3] == 0xff)
                enc = Encoding.UTF32;
            else if (buffer[0] == 0x2b && buffer[1] == 0x2f && buffer[2] == 0x76)
                enc = Encoding.UTF7;
            return enc;
        }

        public static Boolean getBool(String pValor)
        {
            Boolean bResult = false;

            if (pValor == "0")
            {
                bResult = false;
            }
            else if (pValor == "1")
            {
                bResult = true;
            }
            else
            {
                throw new Exception("Error al convertir valor a Boolean: " + pValor);
            }

            return bResult;
        }

        public static DateTime getDate(String pFecha, String pFormato)
        {
            String dia = "";
            String mes = "";
            String anio = "";

            if (pFormato == "MM/AAAA")
            {
                pFecha = "01/" + pFecha;
                Char split = Char.Parse("/");
                String[] elementos = pFecha.Split(split);

                dia = elementos[0];
                mes = elementos[1];
                anio = elementos[2];
            }
            else if (pFormato == "AAAAMMDD")
            {
                anio = pFecha.Substring(0, 4);
                mes = pFecha.Substring(4, 2);
                dia = pFecha.Substring(6, 2);
            }
            else if (pFormato == "DD/MM/AAAA")
            {
                dia = pFecha.Substring(0, 2);
                mes = pFecha.Substring(3, 2);
                anio = pFecha.Substring(6, 4);
            }
            if (dia.Equals("00") || dia.Trim().Equals(""))
                dia = "01";
            if (mes.Equals("00") || mes.Trim().Equals(""))
                mes = "01";
            if (anio.Equals("0000") || anio.Trim().Equals(""))
                anio = "1900";
            DateTime dt = new DateTime(Int32.Parse(anio), Int32.Parse(mes), Int32.Parse(dia));

            return dt;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pcTexto"></param>
        /// <param name="pcArrValores"></param>
        /// <returns></returns>
        public static String RemplazarValores(String pcTexto, String[] pcArrValores)         
        {
            String cResult = String.Empty;
           
            if (pcTexto != String.Empty && pcArrValores != null && pcArrValores.Length > 0) 
            {
                Int32 nIndex = 0;

                while (nIndex < pcArrValores.Length) 
                {
                    cResult = pcTexto.Replace("[" + nIndex + "]", pcArrValores[nIndex]);
                    pcTexto = cResult;
                    nIndex++;
                }                
            }

            return cResult;
        }
    }
}
