﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.Resources
{
    public class TipoArchivo
    {
        public const Int32 Texto = 1;
        public const Int32 Excel = 2;
    }
}
