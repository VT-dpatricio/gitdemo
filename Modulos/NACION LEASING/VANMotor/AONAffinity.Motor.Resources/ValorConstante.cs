﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.Resources
{
    public class ValorConstante
    {        
        #region Nombre Listas
        public static String ListaCiudad = "ListaCiudad";
        public static String ListaModelo = "ListaModelo";
        #endregion

        #region EstadoCotizacion
        public static Int32 CotizacionAprobada = 1;
        public static Int32 CotizacionPendiente = 2;
        public static Int32 CotizacionRechazada = 3;
        #endregion

        #region Mensajes Confirmacion
        /*public static String MsjRegistrar = "¿Está seguro de registrar los datos ingresados?";
        public static String MsjAgregar = "¿Está seguro de agregar los datos ingresados?";*/
        public static String PlanAnual = "ANUAL";
        public static String PlanBianual = "BIANUAL";
        #endregion

        #region Variables
        public static Int32 CodPeru = 51;
        public static String SeparadorText = "£";
        #endregion

        #region Tipo Eventos Ejecutar
        public static String EventoRegistrar = "Registrar";
        public static String EventoActualizar = "Actualizar";
        #endregion        

        #region Medidas MsgBox
        public static Int32 MsgBoxAlto = 180;
        public static Int32 MsgBoxAncho = 400;
        #endregion

        #region Descuento Recarg
        public static String TipoDescuento = "D";
        public static String TipoRecargo = "R";
        #endregion        

        #region Enumerados
        /// <summary>
        /// Retorna los estados que puede tener el cliente.
        /// </summary>
        public enum EstadoCliente
        {
            NoExisteValorAPESEG = 1,
            NoExisteValorTasa = 2,
            NO_RENO_MAXCANTSINIESTRO = 3,
            COTIZADO = 4,
            NoCotizado = 5,
            NO_ASEG_MODELO = 6,
            NO_ASEG_ANTIGUEDAD = 7,
            NO_ASEG_TIMONCAMB = 8,
            NO_ASEG_MINANTPICKUP = 9,
            NO_ASEG_BLINDADO = 10,
            NO_ASEG_MAXVALORVEH = 11,
            NO_EXISTE_TASADEPRECIACION = 12,
            NO_EXISTE_SINIESTROREG = 13
        }

        public enum CondicionCotizacion
        {
            NO_APLICA = 0,
            DESCUENTO = 1,
            RECARGO = 2    
        }

        public enum TipoTasa 
        {
            Anual = 1,
            Bianual = 2
        }

        public enum EstadoCotizacion 
        {
            Creada = 1,
            Pendiente = 2,
            Rechazada = 3,
            ATENDIDO = 4,
            EXPEDIDO = 5
        }

        public enum CodigoAsegurador 
        {
            OTROS = 0,
            RIMAC = 1
        }

        public enum TipoFrecuencia 
        {
            PAGO_UNICO = 0,
            PAGO_MENSUAL = 1,
            PAGO_TRIMESTRAL = 3,
            PAGO_SEMESTRAL = 6,
            PAGO_ANUAL = 12
        }

        public enum TipoProceso 
        {
            CLIENTE_IBK = 1,
            CLIENTE_BBVA = 2,
            CLIENTE_SINIESTRO = 3
        }

        public enum Clase 
        {
            COUPE = 1,
            HATCHBACK = 2,
            SEDAN = 4,
            SUV = 5,
            STATION_WAGON = 6,
            PICK_UP = 12
        }

        /// <summary>
        /// Determina el tipo de atención.
        /// </summary>
        public enum TipoAtencion 
        {
            PENDIENTE = 1,
            AGENDA = 2,
            GENERADA = 3
        }
        #endregion
    }
}
