﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace AONAffinity.Motor.Resources
{
    /// <summary>
    /// Clase para acceder a los valores de WebConfig.
    /// </summary>
    public class AppSettings
    {
        #region Archivo Excel
        /// <summary>
        /// Permite obtener el provedor de versiones del archivo microsoft excel.
        /// </summary>
        public static String ProviderExcel = ConfigurationManager.AppSettings["ProviderExcel"];

        /// <summary>
        /// Permite obtener la propiedad extendida del archivo microsoft excel.
        /// </summary>
        public static String PrividerExtended = ConfigurationManager.AppSettings["ExtendedPropExcel"];
        #endregion

        #region Producto
        public static Int32 IdProdCliente = Convert.ToInt32(ConfigurationManager.AppSettings["IdProdTramaCliente"]);
        public static Int32 IdProdSiniestro = Convert.ToInt32(ConfigurationManager.AppSettings["IdProdTramaSiniestro"]);   
        #endregion
       
        #region Valores
        /// <summary>
        /// Código del producto seguro vehicular.
        /// </summary>
        public static Int32 CodigoProducto = Convert.ToInt32(ConfigurationManager.AppSettings["CodProducto"]);
        #endregion

        #region Tramas
        /// <summary>
        /// Directorio de archivo de Tramas de Cliente.
        /// </summary>
        public static String DirectorioTramaCli = ConfigurationManager.AppSettings["DirTramaCliente"];

        public static String DirectorioTramaCliDwld = ConfigurationManager.AppSettings["DirTramaClienteDwld"];

        public static String DirectorioTramaCliLogDwld = ConfigurationManager.AppSettings["DirTramaClienteLogDwld"];  

        /// <summary>
        /// Directorio de archivo de Tramas de Cliente Log.
        /// </summary>
        public static String DirectorioTramaCliLog = ConfigurationManager.AppSettings["DirTramaClienteLog"];   
        #endregion

        #region Parametro
        public static Int32 ParametroProcesoAuto = Convert.ToInt32(ConfigurationManager.AppSettings["ParProcesoAuto"]);   
        #endregion

        #region Tipo Proceso
        /*public static Int32 TipoProcesoClienteIBK = Convert.ToInt32(ConfigurationManager.AppSettings["TipoProcCliIBK"]);
        public static Int32 TipoProcesoClienteBBVA = Convert.ToInt32(ConfigurationManager.AppSettings["TipoProcCliBBVA"]);
        public static Int32 TipoProcesoClienteSiniestro = Convert.ToInt32(ConfigurationManager.AppSettings["TipoProcCliSiniestro"]);*/
        #endregion
       
        #region Correo
        public static String MailFrom = ConfigurationManager.AppSettings["MailFrom"];
        public static String MailPswd = ConfigurationManager.AppSettings["MailPswd"];
        public static String SMTPClient = ConfigurationManager.AppSettings["SMTPClient"];
        
        //Correo Gmail
        public static String SMTP_Port = ConfigurationManager.AppSettings["SMTP_Port"];
        public static String SMTP_Host = ConfigurationManager.AppSettings["SMTP_Host"];
        public static String EmailAcount = ConfigurationManager.AppSettings["EmailAcount"];
        public static String EmailAcountPwd = ConfigurationManager.AppSettings["EmailAcountPwd"];

        #endregion

        #region Archivo Cotizacion RIMAC
        /// <summary>
        /// Directorio de archivos de cotización
        /// </summary>
        public static String DirCotizacionRIMAC = ConfigurationManager.AppSettings["DirCotizacionRIMAC"];

        /// <summary>
        /// Directorio de imagenes para gerenerar archivo pdf.
        /// </summary>
        public static String DirImagenesRIMAC = ConfigurationManager.AppSettings["DirImagenesRIMAC"];

        /// <summary>
        /// Nombre del archivo de cotización.
        /// </summary>
        public static String ArchivoCotizacionRIMAC = ConfigurationManager.AppSettings["ArchivoCotRIMAC"];

        /// <summary>
        /// Nombre de la imagen del Logo de Aon.
        /// </summary>
        public static String LogoAon = ConfigurationManager.AppSettings["LogoAon"];

        /// <summary>
        /// Nombr de la imagen del logo de Rimac.
        /// </summary>
        public static String LogoRimac = ConfigurationManager.AppSettings["LogoRimac"];
        #endregion

        /// <summary>
        /// Nombre del servidor de correo.
        /// </summary>
        public static String ServidorCorreo = ConfigurationManager.AppSettings["ServCorreo"];

        /// <summary>
        /// Dirreccion de correo de envio.
        /// </summary>
        public static String Correo = ConfigurationManager.AppSettings["Correo"];

        /// <summary>
        /// Dirección de correo de origen.
        /// </summary>
        public static String Usuario = ConfigurationManager.AppSettings["Usr"];

        /// <summary>
        /// Clave de usuario de correo.
        /// </summary>
        public static String Clave = ConfigurationManager.AppSettings["Pwd"];

        /// <summary>
        /// Código de parámetro del título del mail de cotización
        /// </summary>
        public static String CodParTitMailCot = ConfigurationManager.AppSettings["CodParTitMailCot"];

        /// <summary>
        /// Código de info parámetro del título del mail de cotizacion.
        /// </summary>
        public static String CodInfoParMailCotTitulo = ConfigurationManager.AppSettings["CodInfoParMailCotTitulo"];

        /// <summary>
        /// Código de parámetro del nensaje del mail de cotización.
        /// </summary>
        public static String CodParMsjMailCot = ConfigurationManager.AppSettings["CodParMsjMailCot"];

        /// <summary>
        /// Código de info parámetro del mensaje del mail de cotización.
        /// </summary>
        public static String CodOnfoParMailCotMensaje = ConfigurationManager.AppSettings["CodOnfoParMailCotMensaje"];
        
       
        

       

    }
    
}
