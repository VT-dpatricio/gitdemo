﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.Resources
{
    /// <summary>
    /// Clase que contiene las direcciones de los formularios.
    /// </summary>
    public class UrlPagina
    {
        public static String Default = "~/Default.aspx";        
        public static String Login = "~/Login.aspx?CS=SignOut";
        public static String InicioDefault = "~/Default.aspx";
        public static String InicioLogin = "~/Inicio/Login.aspx";

        public static String DirTramaCliente = "~/Archivo/Rimac/Cliente/";
        public static String DirTramaClienteLog = "~/Archivo/Rimac/Cliente/Log/";
 

        #region Cliente
        /// <summary>
        /// Dirección del formulario de registro datos del cliente.
        /// </summary>
        public static String ClienteRegDato = "~/Cliente/frmRegCliente.aspx";             
        #endregion

        #region Vehiculo
        /// <summary>
        /// Dirección del formulario de registro datos específicos del vehículo.
        /// </summary>
        public static String VehiculoRegDatoEsp = "~/Vehiculo/frmRegEspVehiculo.aspx";
        #endregion

        #region Póliza
        /// <summary>
        /// Dirección del formulario de registro tomador póliza auto.
        /// </summary>
        public static String PolizaRegTomador = "~/Poliza/frmTomPolizas.aspx";

        /// <summary>
        /// Dirección del formulario de registro de póliza.
        /// </summary>
        public static String PolizaRegDato  = "~/Poliza/frmDatPoliza.aspx";
        #endregion

        #region Pago
        /// <summary>
        /// Direccíón del formulario de pago credito financiera.
        /// </summary>
        public static String PagoCreFinanciera = "~/Pago/frmPagCreFinanciera.aspx";

        /// <summary>
        /// Direccíón del formulario de pago efectivo.
        /// </summary>
        public static String PagoEfectivo = "~/Pago/frmPagEfectivo.aspx";

        /// <summary>
        /// Direccíón del formulario de pago con tarjeta.
        /// </summary>
        public static String PagoTarjeta = "~/Pago/frmPagTarjeta.aspx";
        #endregion

        #region Cotizacion
        public static String CotizacionInicial = "~/Cotizacion/frmCotInicial.aspx";
        public static String CotizacionResultado = "~/Cotizacion/frmCotResultado.aspx";    
        #endregion

        #region Certificado
        public static String EditarCertificado = "~/Certificado/frmEditar.aspx";
        //public static String ImprimirCertificado = "~/Certificado/frmPrint.aspx";
        public static String ImprimirCertificado = "~/Certificado/frmImprimir.aspx";
        public static String CertificadoExpedir = "~/Certificado/frmExpedir.aspx";
        public static String Expedir5251 = "~/Certificado/frm_Expedir5251.aspx";
        #endregion

        #region Consulta
        public static String ConsultarCertificado = "~/Consulta/frmConsultaCert.aspx";
        public static String ModificarCertificado = "~/Consulta/frmModificarCert.aspx";
        #endregion

        #region
        public static String DownloadTrama = "~/frmDownload.aspx";
        public static String DownloadFile = "~/Trama/frmDownload.aspx";
        #endregion
    }
}
