﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.Resources
{
    /// <summary>
    /// Clase que define los tipos nulos para las entidades.
    /// </summary>
    public class NullTypes
    {
        /// <summary>
        /// Representa el valor null de un String
        /// </summary>
        public static String CadenaNull = String.Empty;

        /// <summary>
        /// Representa el valor null de un DateTime
        /// </summary>
        public static DateTime FechaNull = DateTime.Parse("1900-01-01");

        /// <summary>
        /// Representa el valor null de un Decimal
        /// </summary>
        public static Decimal DecimalNull = -1;

        /// <summary>
        /// Representa el valor null de un Int32
        /// </summary>
        public static Int32 IntegerNull = -1;

        /// <summary>
        /// Representa el valor null de un Int16
        /// </summary>
        public static Int16 ShortNull = -1;

        /// <summary>
        /// Representa el valor null de un Int64
        /// </summary>
        public static Int64 LongNull = -1;

        /// <summary>
        /// Representa el valor null de un Boolean;
        /// </summary>
        public static Nullable<Boolean> BoolNull = null;
    }
}
