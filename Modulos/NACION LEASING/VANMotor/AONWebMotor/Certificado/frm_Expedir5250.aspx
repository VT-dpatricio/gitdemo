﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true" CodeBehind="frm_Expedir5250.aspx.cs" Inherits="AONWebMotor.Certificado.frm_Expedir5250" %>
<%@ Register src="../Controles/MsgBox.ascx" tagname="MsgBox" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="Form_SubTitulo" colspan="6">
                            Opciones
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer" style="width: 150px">
                            Cambiar Valor Vehículo:
                        </td>
                        <td class="Form_TextoIzq" style="width: 50px">
                            <asp:DropDownList ID="ddlSigno" runat="server" Width="40px" AutoPostBack="True" OnSelectedIndexChanged="ddlSigno_SelectedIndexChanged">
                                <asp:ListItem Text="" Value="-1"></asp:ListItem>
                                <asp:ListItem Text="+" Value="+"></asp:ListItem>
                                <asp:ListItem Text="-" Value="-"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvSigno" runat="server" Text="*" ControlToValidate="ddlSigno"
                                Display="None" ErrorMessage="Seleccione signo de porcentaje" SetFocusOnError="true"
                                InitialValue="-1" ValidationGroup="CambiarValor">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:UpdatePanel ID="upSigno" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlPorcentaje" runat="server" Width="40px">
                                    </asp:DropDownList>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlSigno" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="btnCambiarValVeh" runat="server" Text="Cambiar" ValidationGroup="CambiarValor"
                                OnClick="btnCambiarValVeh_Click" />
                            <asp:Button ID="btnValorOri" runat="server" Text="Valor Original" OnClick="btnValorOri_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                <div style="width: 100%; height: 380px; overflow: auto;">
                </div>
                <br />
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <div style="display: none;">
        <asp:HiddenField ID="hfIdCertificado" runat="server" />
        <asp:HiddenField ID="hfIdPeriodo" runat="server" />
        <asp:HiddenField ID="hfNroCotizacion" runat="server" />
        <asp:HiddenField ID="hfSaludo" runat="server" />
        <asp:HiddenField ID="hfCond" runat="server" />
        <asp:HiddenField ID="hfConf" runat="server" />
        <asp:HiddenField id="hfEstForm" runat="server" />                                       
    </div>
    <uc1:MsgBox ID="msgBox" runat="server"   />
</asp:Content>
