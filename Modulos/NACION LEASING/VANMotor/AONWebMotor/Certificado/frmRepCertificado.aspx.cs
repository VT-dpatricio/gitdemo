﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AONWebMotor.Certificado
{
    public partial class frmRepCertificado : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["idCot"]))
                {
                    this.rvCertificado.LocalReport.Refresh();
                }

            }
        }
    }
}
