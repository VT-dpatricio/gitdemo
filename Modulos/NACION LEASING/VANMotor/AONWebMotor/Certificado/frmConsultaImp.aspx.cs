﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;

namespace AONWebMotor.Certificado
{
    
    public partial class frmConsultaImp : PageBase 
    {
        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.CargarFormulario();
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.ConsultarCertificado();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void gvCotizacion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes.Add("OnMouseOut", "this.className = this.orignalclassName;");
                    e.Row.Attributes.Add("OnMouseOver", "this.orignalclassName = this.className;this.className = 'GridWiew_SelectedRow';");

                    ImageButton ibtnSelect = (ImageButton)e.Row.FindControl("ibtnSelect");
                    AONAffinity.Motor.BusinessEntity.Bais.BECertificado objBECertificado = (AONAffinity.Motor.BusinessEntity.Bais.BECertificado)(e.Row.DataItem);

                    if (ibtnSelect != null)
                    {
                        ibtnSelect.CommandArgument = objBECertificado.IdCertificado + "@" + objBECertificado.IdProducto;
                    }
                }
            }
            catch (Exception ex) 
            {
            }            
        }

        protected void gvCotizacion_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try 
            {
                switch (e.CommandName)
                {
                    case "Select":
                        String[] cArrValores = e.CommandArgument.ToString().Split('@');
                        Response.Redirect("../" + UrlPagina.ImprimirCertificado.Substring(2) + "?idCert=" + cArrValores[0] + "&idProd=" + cArrValores[1]);
                        break;
                }
            }
            catch (Exception ex) 
            {
            }
        }
        #endregion
        #region Métodos
        private void CargarFormulario()
        {
            this.CargarCabecera();
        }

        private void CargarCabecera()
        {
            if (this.gvCotizacion.Rows.Count == 0)
            {
                List<AONAffinity.Motor.BusinessEntity.Bais.BECertificado> lstBEBECertificado = new List<AONAffinity.Motor.BusinessEntity.Bais.BECertificado>();
                AONAffinity.Motor.BusinessEntity.Bais.BECertificado objBECertificado = new AONAffinity.Motor.BusinessEntity.Bais.BECertificado();
                lstBEBECertificado.Add(objBECertificado);
                this.CargarGridView(this.gvCotizacion, lstBEBECertificado);
                this.gvCotizacion.Rows[0].Visible = false;
            }
        }

        private void ConsultarCertificado() 
        {
            this.lblResultado.Visible = false; 

            AONAffinity.Motor.BusinessLogic.Bais.BLCertificado objBLCertificado = new AONAffinity.Motor.BusinessLogic.Bais.BLCertificado();
            
            if (this.ddlTipoBusq.SelectedValue == "0") 
            {
                this.CargarGridView(this.gvCotizacion, objBLCertificado.Consultar(Convert.ToInt32(this.ddlTipoBusq.SelectedValue ), this.txtNro.Text.Trim(), NullTypes.DecimalNull, NullTypes.CadenaNull, NullTypes.CadenaNull, NullTypes.CadenaNull, NullTypes.CadenaNull, Session[NombreSession.Usuario ].ToString ()));
            }

            if (this.ddlTipoBusq.SelectedValue == "1")
            {
                this.CargarGridView(this.gvCotizacion, objBLCertificado.Consultar(Convert.ToInt32(this.ddlTipoBusq.SelectedValue), NullTypes.CadenaNull, NullTypes.DecimalNull, this.txtApePat.Text.Trim(), this.txtApeMat.Text.Trim(), this.txtPriNom.Text.Trim(), this.txtSegNom.Text.Trim(), Session[NombreSession.Usuario].ToString()));
            }

            if (this.ddlTipoBusq.SelectedValue == "2")
            {
                this.CargarGridView(this.gvCotizacion, objBLCertificado.Consultar(Convert.ToInt32(this.ddlTipoBusq.SelectedValue), NullTypes.CadenaNull, Convert.ToDecimal(this.txtNro.Text.Trim())  , NullTypes.CadenaNull, NullTypes.CadenaNull, NullTypes.CadenaNull, NullTypes.CadenaNull, Session[NombreSession.Usuario].ToString()));
            }

            if (this.gvCotizacion.Rows.Count == 0)
            {
                this.CargarCabecera();
                this.lblResultado.Visible = true;
            }
        }
        #endregion

        protected void gvCotizacion_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                this.gvCotizacion.PageIndex = e.NewPageIndex;
                this.ConsultarCertificado();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }    
        }

        

       

    }    
}