<%@ Page Title="Registro Certificado" Language="C#" MasterPageFile="~/Master/Principal.Master"
    AutoEventWireup="true" CodeBehind="frmCertRegistro.aspx.cs" Inherits="AONWebMotor.Certificado.frmCertRegistro" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
        function FormClear() {
            document.getElementById('<%=txtCodOficina.ClientID %>').value = "";
            document.getElementById('<%=txtDesOficina.ClientID %>').value = "";
            document.getElementById('<%=txtFecha.ClientID %>').value = "";  
            document.getElementById('<%=txtPriNombre.ClientID %>').value = "";
            document.getElementById('<%=txtSegNombre.ClientID %>').value = "";
            document.getElementById('<%=ddlTipDocumento.ClientID %>').value = "L";
            document.getElementById('<%=txtApePaterno.ClientID %>').value = "";
            document.getElementById('<%=txtApeMaterno.ClientID %>').value = "";
            document.getElementById('<%=txtNroDocumento.ClientID %>').value = "";
            document.getElementById('<%=ddlDepartamento.ClientID %>').value = "5115";
            document.getElementById('<%=hfProvincia.ClientID %>').value = "511501";
            document.getElementById('<%=hfDistrito.ClientID %>').value = "511501001";
            document.getElementById('<%=txtDireccion.ClientID %>').value = "";  
            document.getElementById('<%=txtEmail.ClientID %>').value = "";
            document.getElementById('<%=txtTelefono.ClientID %>').value = "";
            document.getElementById('<%=txtFecNacimiento.ClientID %>').value = "";
            document.getElementById('<%=ddlSexo.ClientID %>').value = "0";
            document.getElementById('<%=btnRefreshClear.ClientID %>').click();                                                                          
        }        
    </script> 
    <script language="javascript" type="text/javascript">
        function OnOficinaSelected(pCod, pDes) {
            document.getElementById('<%=txtCodOficina.ClientID %>').value = pCod;
            document.getElementById('<%=txtDesOficina.ClientID %>').value = Encoder.htmlDecode(pDes);
            document.getElementById('<%=txtCodOficina.ClientID %>').focus();
            hideModalPopup('bmpeOficina');
        }        
    </script>
    <script language="javascript" type="text/javascript">
        function CotizacionAsincrono(e) {
            var evt = e ? e : window.event;
            if (evt.keyCode == 13) {
                var nroCotizacion = document.getElementById('<%=txtNroCotizacion.ClientID %>').value;

                if (nroCotizacion != "") {
                    ObtenerCotizacion(nroCotizacion);
                    return false;
                }
                else {
                    return false;
                }
            }
        }
        function ObtenerCotizacion(pNroCotizacion) {
            var cotizacion = PageMethods.ObtenerCotizacion(pNroCotizacion, onSucceedCotizacion, OnErrorCotizacion);
        }
        function onSucceedCotizacion(result) {
            var arrValores = result.split('�');
            if (result != "") {
                document.getElementById('<%=ddlTipDocumento.ClientID %>').value = Encoder.htmlDecode(arrValores[0]);
                document.getElementById('<%=txtNroDocumento.ClientID %>').value = Encoder.htmlDecode(arrValores[1]);
                document.getElementById('<%=txtApePaterno.ClientID %>').value = Encoder.htmlDecode(arrValores[2]);
                document.getElementById('<%=txtApeMaterno.ClientID %>').value = Encoder.htmlDecode(arrValores[3]);
                document.getElementById('<%=txtPriNombre.ClientID %>').value = Encoder.htmlDecode(arrValores[4]);
                document.getElementById('<%=txtSegNombre.ClientID %>').value = Encoder.htmlDecode(arrValores[5]);
                document.getElementById('<%=txtDireccion.ClientID %>').value = Encoder.htmlDecode(arrValores[6]);
                document.getElementById('<%=txtFecha.ClientID %>').value = Encoder.htmlDecode(arrValores[8]);
                document.getElementById('<%=txtEmail.ClientID %>').value = Encoder.htmlDecode(arrValores[9]);
                document.getElementById('<%=txtTelefono.ClientID %>').value = Encoder.htmlDecode(arrValores[10]);
                document.getElementById('<%=ddlDepartamento.ClientID %>').value = Encoder.htmlDecode(arrValores[7].substring(0, 4));
                document.getElementById('<%=hfProvincia.ClientID %>').value = Encoder.htmlDecode(arrValores[7].substring(0, 6));
                document.getElementById('<%=hfDistrito.ClientID %>').value = Encoder.htmlDecode(arrValores[7]);
                document.getElementById('<%=btnRefresh.ClientID %>').click();                                                
            }
            else {
                this.FormClear();
                alert('No se encontr� la cotizaci�n.');
            }
        }
        function OnErrorCotizacion(result) {
            this.FormClear();
            alert('No se encontr� la cotizaci�n.');
        }  
    </script>       
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div id="divSeq" runat="server">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td class="Form_tdBorde">
                </td>
                <td class="Seq_Girs" style="height: 15px;">
                    Cotizaci�n
                </td>
                <td class="Seq_Girs" style="height: 15px;">
                    Informaci�n del Veh�culo
                </td>
                <td class="Seq_Red" style="height: 15px;">
                    Registro de Certifiado
                </td>
                <td class="Form_tdBorde">
                </td>
            </tr>
        </table>
    </div>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="Form_SubTitulo" colspan="8">
                            DATOS DEL SEGURO
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer" style="width: 110px">
                            Nro. Certificado: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtNroCertificado" runat="server" Width="100px" CssClass="Form_TextBoxDisable"
                                MaxLength="15">
                            </asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="ftxtNroCertificado" runat="server" TargetControlID="txtNroCertificado"
                                ValidChars="1234567890">
                            </cc1:FilteredTextBoxExtender>
                            <asp:RequiredFieldValidator ID="rfvNroCertificado" runat="server" ControlToValidate="txtNroCertificado"
                                SetFocusOnError="true" Display="None" ErrorMessage="Nro. certificado obligatorio."
                                Text="*" ValidationGroup="Certificado">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td>
                            Nro. Cotizaci�n:
                        </td>
                        <td>
                            <asp:TextBox ID="txtNroCotizacion" runat="server" CssClass="Form_TextBox" Width="100px"
                                MaxLength="15" ReadOnly="true"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="fteNroCotizacion" runat="server" TargetControlID="txtNroCotizacion"
                                ValidChars="1234567890">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                        <td class="Form_TextoDer">                            
                            Oficina: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtCodOficina" runat="server" CssClass="Form_TextBoxCod" 
                                onkeyup="showPopupKey('bmpeOficina','ctl00_ContentPlaceHolder1_txtBusqOficina');">                                
                            </asp:TextBox>
                            <asp:TextBox ID="txtDesOficina" runat="server" CssClass="Form_TextBox">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCodOficina"
                                SetFocusOnError="true" Display="None" ErrorMessage="C�digo oficina obligatorio."
                                Text="*" ValidationGroup="Certificado">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Fecha: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtFecha" runat="server" Width="60px" CssClass="Form_TextBox"></asp:TextBox>
                            <cc1:MaskedEditExtender ID="meeFecha" runat="server" TargetControlID="txtFecha" Mask="99/99/9999"
                                MaskType="Date">
                            </cc1:MaskedEditExtender>
                            <asp:RequiredFieldValidator ID="rfvFecha" runat="server" ControlToValidate="txtFecha"
                                SetFocusOnError="true" Display="None" ErrorMessage="Fecha obligatorio." Text="*"
                                ValidationGroup="Certificado">
                            </asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvFecha" runat="server" ControlToValidate="txtFecha" SetFocusOnError="true"
                                Display="None" ErrorMessage="Fecha formato incorrecto." Text="*" ValidationGroup="Certificado"
                                Operator="DataTypeCheck" Type="Date">
                            </asp:CompareValidator>
                        </td>
                    </tr>
                </table>
                <br />
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="Form_SubTitulo" colspan="6">
                            DATOS DEL CLIENTE
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer" style="width: 110px;">
                            Primer Nombre: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtPriNombre" runat="server" CssClass="Form_TextBox" Width="120px">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvPriNombre" runat="server" ControlToValidate="txtPriNombre"
                                Text="*" ErrorMessage="Primer nombre obligatorio." Display="None" SetFocusOnError="true"
                                ValidationGroup="Certificado"></asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Segundo Nombre:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtSegNombre" runat="server" CssClass="Form_TextBox" Width="120px">
                            </asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            Tipo Documento: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlTipDocumento" runat="server" Width="180px">                                
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Apellido Paterno: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtApePaterno" runat="server" CssClass="Form_TextBox" Width="120px">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvApePaterno" runat="server" ControlToValidate="txtApePaterno"
                                Text="*" ErrorMessage="Apellido paterno obligatorio." Display="None" SetFocusOnError="true"
                                ValidationGroup="Certificado"></asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Apellido Materno:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtApeMaterno" runat="server" CssClass="Form_TextBox" Width="120px">
                            </asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            Nro. Documento: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtNroDocumento" runat="server" Width="90px" CssClass="Form_TextBox">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvNroDocumento" runat="server" ControlToValidate="txtNroDocumento"
                                Text="*" ErrorMessage="Nro Documento obligatorio." Display="None" SetFocusOnError="true"
                                ValidationGroup="Certificado"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Departamento:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlDepartamento" runat="server" Width="140px" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td class="Form_TextoDer">
                            Provincia:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlProvincia" runat="server" Width="140px" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlProvincia_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td class="Form_TextoDer">
                            Distrito:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlDistrito" runat="server" Width="140px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Direccci�n: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq" colspan="2">
                            <asp:TextBox ID="txtDireccion" runat="server" CssClass="Form_TextBox" Width="200px">
                            </asp:TextBox>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                <br />
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="Form_SubTitulo" colspan="8">
                            INFORMACI�N ADICIONAL DEL CLIENTE
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer" style="width: 110px;">
                            Email:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtEmail" runat="server" CssClass="Form_TextBox" Width="200px"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                                Display="None" SetFocusOnError="true" ErrorMessage="Email formato incorrecto."
                                Text="*" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Certificado">
                            </asp:RegularExpressionValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Tel�fono:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtTelefono" runat="server" CssClass="Form_TextBox" Width="100px">
                            </asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            Fecha Nacimiento:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtFecNacimiento" runat="server" CssClass="Form_TextBox" Width="60px">
                            </asp:TextBox>
                            <cc1:MaskedEditExtender ID="meeFecNacimiento" runat="server" Mask="99/99/9999" MaskType="Date"
                                TargetControlID="txtFecNacimiento">
                            </cc1:MaskedEditExtender>
                            <asp:CompareValidator ID="cvFecNacimiento" runat="server" ControlToValidate="txtFecNacimiento"
                                SetFocusOnError="true" Display="None" ErrorMessage="Fecha nacimiento formato incorrecto."
                                Text="*" ValidationGroup="Certificado" Operator="DataTypeCheck" Type="Date">
                            </asp:CompareValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Sexo:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlSexo" runat="server" Width="100px">
                                <asp:ListItem Text="-Seleccione-" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Masculino" Value="M"></asp:ListItem>
                                <asp:ListItem Text="Femenino" Value="F"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="Form_SubTitulo" colspan="8">
                            IMFORMACI�N DE PAGO
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer" style="width: 110px;">
                            Plan: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlPlan" runat="server" Width="120px">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvPlan" runat="server" Text="*" ControlToValidate="ddlPlan"
                                Display="None" ErrorMessage="Seleccione plan." ForeColor="Transparent" SetFocusOnError="true"
                                InitialValue="0" ValidationGroup="Certificado">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Fecuencia: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlFrecuencia" runat="server" Width="120px">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvFrecuencia" runat="server" Text="*" ControlToValidate="ddlFrecuencia"
                                Display="None" ErrorMessage="Seleccione frecuencia." ForeColor="Transparent"
                                SetFocusOnError="true" InitialValue="0" ValidationGroup="Certificado">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Moneda Prima: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlMonPrima" runat="server" Width="120px">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvMonPrima" runat="server" Text="*" ControlToValidate="ddlMonPrima"
                                Display="None" ErrorMessage="Seleccione moneda prima." ForeColor="Transparent"
                                SetFocusOnError="true" InitialValue="0" ValidationGroup="Certificado">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Prima Total:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtPrimTotal" runat="server" CssClass="Form_TextBoxMonto" Width="60px">53.00</asp:TextBox>
                            <cc1:FilteredTextBoxExtender id="ftxtPrimTotal" runat="server" TargetControlID="txtPrimTotal"
                                ValidChars="1234567890,." ></cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Medio de Pago: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlMedPago" runat="server" Width="120px">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvMedPago" runat="server" Text="*" ControlToValidate="ddlMedPago"
                                Display="None" ErrorMessage="Seleccione medio de pago." ForeColor="Transparent"
                                SetFocusOnError="true" InitialValue="0" ValidationGroup="Certificado">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Nro. Cuenta: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtNroCuenta" runat="server" CssClass="Form_TextBox" Width="116px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvNroCuenta" runat="server" ControlToValidate="txtNroCuenta"
                                SetFocusOnError="true" Display="None" ErrorMessage="Nro. cuenta obligatorio."
                                Text="*" ValidationGroup="Certificado">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Moneda Cuenta: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlMonCuenta" runat="server" Width="120px">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvMonCuenta" runat="server" Text="*" ControlToValidate="ddlMonCuenta"
                                Display="None" ErrorMessage="Seleccione moneda cuenta." ForeColor="Transparent"
                                SetFocusOnError="true" InitialValue="0" ValidationGroup="Certificado">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <div class="Form_RegButtonDer">
                    <asp:Button ID="btnGuaVenta" runat="server" Text="Expedir" ValidationGroup="Certificado"
                        OnClick="btnGuaVenta_Click" />
                    <asp:Button ID="btnImprimir" runat="server" Text="Imprimir" CausesValidation="false"
                        Enabled="false" OnClientClick="return showModalPopup('bmpeCertificado');" />
                    <span class="Form_TextoObligatorio">&nbsp;(*)Campos Obligatorios</span>
                </div>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <div style="display:none;">
        <asp:HiddenField ID="hfNroCertificado" runat="server" />
        <asp:HiddenField ID="hfIdCertificado" runat="server" />
        <asp:HiddenField id="hfProvincia" runat="server" />
        <asp:HiddenField id="hfDistrito" runat="server" />
        <asp:Button id="btnRefresh" runat="server" style="display:none" 
            onclick="btnRefresh_Click" />
        <asp:Button id="btnRefreshClear" runat="server" style="display:none" 
            onclick="btnRefreshClear_Click" />
    </div>    
    <asp:ValidationSummary ID="vsCertificado" runat="server" ShowMessageBox="true" ShowSummary="false"
        ValidationGroup="Certificado" />                            
    <asp:Panel ID="pnOficina" runat="server" Width="500px" CssClass="Modal_Panel" Style="display: none;"
        DefaultButton="btnBusqOficina">
        <div class="Modal_Head" style="width: 100%;">
            <div id="divHeadTitulo" style="float: left;">
                Buscar Oficina
            </div>
            <div id="divHeadCerrar" style="float: right;">
                <img alt="Click para cerrar." class="Modal_CloseButton" onclick="hideModalPopup('bmpeOficina')"
                    style="cursor: hand;" />
            </div>
        </div>
        <div class="Modal_Body">
            <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="Form_TextoIzq">
                        Oficina:
                    </td>
                    <td class="Form_TextoIzq">
                        <asp:TextBox ID="txtBusqOficina" runat="server" CssClass="Form_TextBox" Width="300px">
                        </asp:TextBox>
                    </td>
                    <td class="Form_TextoIzq" style="width: 16px;">
                        <asp:UpdateProgress ID="upsBusqOficina" runat="server" AssociatedUpdatePanelID="upBusqOficina"
                            DisplayAfter="0">
                            <ProgressTemplate>
                                <asp:Image ID="imgLoadOficina" runat="server" SkinID="sknImgLoading" Width="16px"
                                    Height="16px" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                    <td class="Form_TextoDer">
                        <asp:UpdatePanel ID="upBusqOficina" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnBusqOficina" runat="server" Text="Buscar" Width="80px" OnClick="btnBusqOficina_Click" />
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnBusqOficina" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
            <br />
            <div style="overflow: auto; height: 200px;">
                <asp:UpdatePanel ID="upGvOficina" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="gvBusqOficina" runat="server" SkinID="sknGridView" AutoGenerateColumns="false"
                            Width="98%" OnRowDataBound="gvBusqOficina_RowDataBound">
                            <Columns>
                                <asp:BoundField DataField="IdOficina" HeaderText="C�digo">
                                    <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="nombre" HeaderText="Oficina">
                                    <HeaderStyle HorizontalAlign="Center" Width="250px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="zona" HeaderText="Zona">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="gvBusqOficina" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <br />
        </div>
    </asp:Panel>
    <asp:Label ID="lblPopOficina" runat="server" Style="display: none;"></asp:Label>
    <cc1:ModalPopupExtender ID="mpeOficina" runat="server" Enabled="true" TargetControlID="lblPopOficina"
        BehaviorID="bmpeOficina" PopupControlID="pnOficina" BackgroundCssClass="Modal_Background">
    </cc1:ModalPopupExtender>
    <asp:Panel ID="pnCertificado" runat="server" Width="800px" Height="600px" CssClass="Modal_Panel"
       Style="Display:none;" >
        <div class="Modal_Head" style="width: 100%;">
            <div id="div1" style="float: left;">
                Certificado
            </div>
            <div id="div2" style="float: right;">
                <img alt="Click para cerrar." class="Modal_CloseButton" onclick="hideModalPopup('bmpeCertificado')"
                    style="cursor: hand;" />
            </div>
        </div>
        <div class="Modal_Body" style="height: 95%;">
            <asp:TextBox ID="txtCertificado" runat="server" Style="display: none;"></asp:TextBox>
            <rsweb:ReportViewer ID="rvCertificado" runat="server" Width="100%" Height="92%" 
                Font-Names="Verdana" Font-Size="8pt">
                <LocalReport ReportPath="rdlc\rdlcCertificadoSegVeh.rdlc">
                    <DataSources>
                        <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="BECertificado" />
                    </DataSources>
                </LocalReport>
            </rsweb:ReportViewer>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="Obtener"
                TypeName="AONAffinity.Business.Logic.BLReporte">
                <SelectParameters>
                    <asp:ControlParameter Name="pcIdCertificado" ControlID="hfIdCertificado" 
                        Type="String" PropertyName="Value" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </div>
    </asp:Panel>
    <asp:Label ID="lblPopCertificado" runat="server" Style="display: none;"></asp:Label>    
    <cc1:ModalPopupExtender ID="mpeCertificado" runat="server" Enabled="true" TargetControlID="lblPopCertificado"
        BehaviorID="bmpeCertificado" PopupControlID="pnCertificado" BackgroundCssClass="Modal_Background">
    </cc1:ModalPopupExtender>
</asp:Content>
