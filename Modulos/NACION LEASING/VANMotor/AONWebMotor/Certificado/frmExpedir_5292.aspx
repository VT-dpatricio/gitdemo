﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true"
    CodeBehind="frmExpedir_5292.aspx.cs" Inherits="AONWebMotor.Certificado.frmExpedir_5292" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v8.3" Namespace="DevExpress.Web.ASPxPanel"
    TagPrefix="dxp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>                
                <dxrp:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" Width="100%" HeaderText="INFORMACIÓN DEL CERTIFICADO/CLIENTE">
                    <PanelCollection>
                        <dxp:PanelContent runat="server" SupportsDisabledAttribute="True">
                            <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td class="Form_TextoIzq">
                                        <asp:Image ID="imgSpeechCli" runat="server" SkinID="sknImgUsuario" Width="25px" Height="25px" />
                                    </td>
                                    <td class="Form_TextoIzq" colspan="7">
                                        <asp:Label ID="lblSpecchCli" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="Form_TextoDer">
                                        Informador:</td>
                                    <td class="Form_TextoIzq">
                                        <asp:DropDownList ID="ddlInformador" runat="server" Width="120px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Oficina:</td>
                                    <td class="Form_TextoIzq">
                                        <asp:DropDownList ID="ddlOficina" runat="server" Width="120px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Fecha:</td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtFechaVenta" runat="server" CssClass="Form_TextBoxDisable" 
                                            Width="90px"></asp:TextBox>
                                    </td>
                                    <td class="Form_TextoDer">
                                        &nbsp;</td>
                                    <td class="Form_TextoIzq">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="Form_TextoDer">
                                        Tipo Doc.:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:DropDownList ID="ddlTipDocumento" runat="server" Width="95px">
                                            <asp:ListItem Text="DNI" Value="L"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Nro. Doc.:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtNroDocumento" runat="server" CssClass="Form_TextBox" 
                                            MaxLength="8" Width="90px"></asp:TextBox>
                                        <span class="Form_TextoObligatorio">*</span>
                                        <cc1:FilteredTextBoxExtender ID="fteNroDocumento" runat="server" Enabled="True" 
                                            TargetControlID="txtNroDocumento" ValidChars="0123456789">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td class="Form_TextoDer">
                                        1er Nombre:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtNombre1" runat="server" CssClass="Form_TextBox" 
                                            MaxLength="60" Width="90px"></asp:TextBox>
                                        <span class="Form_TextoObligatorio">*</span>
                                        <cc1:FilteredTextBoxExtender ID="fteNombre1" runat="server" Enabled="True" 
                                            TargetControlID="txtNombre1" 
                                            ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnñopqrstuvwxyzÁÉÍÓÚÜáéíóúü">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td class="Form_TextoDer">
                                        2do Nombre:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtNombre2" runat="server" CssClass="Form_TextBox" 
                                            MaxLength="60" Width="90px"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="fteNombre2" runat="server" Enabled="True" 
                                            TargetControlID="txtNombre2" 
                                            ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnñopqrstuvwxyzÁÉÍÓÚÜáéíóúü">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="Form_TextoDer">
                                        Sexo:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:DropDownList ID="ddlSexo" runat="server" Width="95px">
                                            <asp:ListItem Text="MÁSCULINO" Value="M"></asp:ListItem>
                                            <asp:ListItem Text="FEMENINO" Value="F"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Estado Civil:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:DropDownList ID="ddlEstadoCiv" runat="server" Width="95px">
                                            <asp:ListItem Text="SOLTERO" Value="S"></asp:ListItem>
                                            <asp:ListItem Text="CASADO" Value="C"></asp:ListItem>
                                            <asp:ListItem Text="VIUDO" Value="V"></asp:ListItem>
                                            <asp:ListItem Text="DIVORCIADO" Value="D"></asp:ListItem>
                                            <asp:ListItem Text="SEPARADO" Value="X"></asp:ListItem>
                                            <asp:ListItem Text="UNIÓN LIBRE" Value="U"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Form_TextoDer">
                                        1er Apellido:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtApellidoPat" runat="server" CssClass="Form_TextBox" MaxLength="60"
                                            Width="90px">
                                        </asp:TextBox>
                                        <span class="Form_TextoObligatorio">*</span>
                                        <cc1:FilteredTextBoxExtender ID="fteApellidoPat" runat="server" TargetControlID="txtApellidoPat"
                                            ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnñopqrstuvwxyzÁÉÍÓÚÜáéíóúü"
                                            Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td class="Form_TextoDer">
                                        2do Apellido:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtApellidoMat" runat="server" CssClass="Form_TextBox" MaxLength="60"
                                            Width="90px"></asp:TextBox>
                                        <span class="Form_TextoObligatorio">*</span>
                                        <cc1:FilteredTextBoxExtender ID="fteApellidoMat" runat="server" TargetControlID="txtApellidoMat"
                                            ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnñopqrstuvwxyzÁÉÍÓÚÜáéíóúü"
                                            Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="Form_TextoDer">
                                        Fec. Nacimiento:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtFecNacimiento" runat="server" Width="90px" 
                                            CssClass="Form_TextBox">
                                        </asp:TextBox>
                                        <span class="Form_TextoObligatorio">*</span>
                                        <cc1:MaskedEditExtender ID="meeFecNacimiento" runat="server" TargetControlID="txtFecNacimiento"
                                            Mask="99/99/9999" MaskType="Date">
                                        </cc1:MaskedEditExtender>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Nacionalidad:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtMzLteNro" runat="server" CssClass="Form_TextBox" MaxLength="100"
                                            Width="90px">
                                        </asp:TextBox>
                                        <span class="Form_TextoObligatorio">*</span>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Ocupacion:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtAptoInt" runat="server" CssClass="Form_TextBox" MaxLength="100"
                                            Width="90px">                                    
                                        </asp:TextBox>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Nro de Hijos:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtUrbanizacion" runat="server" CssClass="Form_TextBox" MaxLength="15"
                                            Width="90px">
                                        </asp:TextBox>
                                        <span class="Form_TextoObligatorio">*</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="Form_TextoDer">
                                        Nro Teléfono:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtTelefono1" runat="server" CssClass="Form_TextBox" MaxLength="15"
                                            Width="90px"></asp:TextBox>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Nro Movil:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtTelefono2" runat="server" CssClass="Form_TextBox" MaxLength="15"
                                            Width="90px"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="fteTelefono2" runat="server" Enabled="True" TargetControlID="txtTelefono2"
                                            ValidChars="0123456789*-">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Email:
                                    </td>
                                    <td class="Form_TextoIzq" colspan="3">
                                        <span class="Form_TextoObligatorio">
                                            <asp:TextBox ID="txtDireccion0" runat="server" CssClass="Form_TextBox" MaxLength="300"
                                                Width="200px"></asp:TextBox>
                                            *</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="Form_TextoDer">
                                        Departamento:
                                    </td>
                                    <td class="Form_TextoIzq" style="width: 125px;">
                                        <asp:UpdatePanel ID="upDepartamento" runat="server">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                    <tr>
                                                        <td style="width: 100px;">
                                                            <asp:DropDownList ID="ddlDepartamento" runat="server" Width="105px" AutoPostBack="True"
                                                                OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 5px;">
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td style="width: 10px;">
                                                            <asp:UpdateProgress ID="upsDepartamento" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upDepartamento">
                                                                <ProgressTemplate>
                                                                    <asp:Image ID="imgLoadDep" runat="server" SkinID="sknImgLoading" Width="10px" Height="10px" />
                                                                </ProgressTemplate>
                                                            </asp:UpdateProgress>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Provincia:
                                    </td>
                                    <td class="Form_TextoIzq" style="width: 125px;">
                                        <asp:UpdatePanel ID="upProvincia" runat="server">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                    <tr>
                                                        <td style="width: 100px;">
                                                            <asp:DropDownList ID="ddlProvincia" runat="server" Width="105px" AutoPostBack="True"
                                                                OnSelectedIndexChanged="ddlProvincia_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 5px;">
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td style="width: 10px;">
                                                            <asp:UpdateProgress ID="upsProvincia" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upProvincia">
                                                                <ProgressTemplate>
                                                                    <asp:Image ID="imgLoadProv" runat="server" SkinID="sknImgLoading" Width="10px" Height="10px" />
                                                                </ProgressTemplate>
                                                            </asp:UpdateProgress>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddlDepartamento" EventName="selectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Distrito:
                                    </td>
                                    <td class="Form_TextoIzq" colspan="3">
                                        <asp:UpdatePanel ID="upDistrito" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="ddlDistrito" runat="server" Width="105px">
                                                </asp:DropDownList>
                                                <span class="Form_TextoObligatorio">*</span>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddlProvincia" EventName="selectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="Form_TextoDer">
                                        Dirección:
                                    </td>
                                    <td class="Form_TextoIzq" colspan="3">
                                        <asp:TextBox ID="txtEmail" runat="server" CssClass="Form_TextBox" Width="200px"></asp:TextBox>
                                        <span class="Form_TextoObligatorio">*</span>
                                    </td>
                                    <td class="Form_TextoDer">
                                    </td>
                                    <td class="Form_TextoIzq" colspan="3">
                                    </td>
                                </tr>
                            </table>
                        </dxp:PanelContent>
                    </PanelCollection>
                </dxrp:ASPxRoundPanel>
                <br />
                <dxrp:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" Width="100%" HeaderText="INFORMACIÓN DEL VEHÍCULO">
                    <PanelCollection>
                        <dxp:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                            <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td class="Form_TextoDer">
                                        Marca:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:DropDownList ID="ddlMarca" runat="server" Width="105px">
                                        </asp:DropDownList>                                        
                                    </td>
                                    <td class="Form_TextoDer">
                                        Modelo:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:DropDownList ID="ddlModelo" runat="server" Width="105px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Año Fab.:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:DropDownList ID="ddlAnioFab" runat="server" Width="95px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Nro Motor:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtNroMotor" runat="server" CssClass="Form_TextBoxDisable" Width="95px"
                                            ReadOnly="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="Form_TextoDer">
                                        Nro Placa:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtNroPlaca" runat="server" CssClass="Form_TextBox" Width="95px"
                                            MaxLength="20"></asp:TextBox>
                                        <span class="Form_TextoObligatorio">*</span>
                                        <cc1:FilteredTextBoxExtender ID="fteNroPlaca" runat="server" TargetControlID="txtNroPlaca"
                                            ValidChars="123456789-abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ"
                                            Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Color:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlColor" runat="server" Width="95px">
                                        </asp:DropDownList>
                                        <span class="Form_TextoObligatorio">*</span>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Tipo Vehículo:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:DropDownList ID="ddlTipoVehiculo" runat="server" Width="95px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Uso Vehículo:</td>
                                    <td>
                                        <asp:DropDownList ID="ddlUso0" runat="server" Width="95px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="Form_TextoDer">
                                        Nro. Asientos:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtNroAsientos" runat="server" CssClass="Form_TextBox" Width="95px"
                                            MaxLength="1">
                                        </asp:TextBox>
                                        <span class="Form_TextoObligatorio">*</span>
                                        <cc1:FilteredTextBoxExtender ID="fteNroAsientos" runat="server" TargetControlID="txtNroAsientos"
                                            ValidChars="1234567890" Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Nro. Chasis:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtNroChasis" runat="server" CssClass="Form_TextBox" 
                                            Width="90px">
                                        </asp:TextBox>
                                        <span class="Form_TextoObligatorio">*</span>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Valor Vehículo:</td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtTipVehiculo1" runat="server" CssClass="Form_TextBoxDisable" 
                                            ReadOnly="True" Width="95px"></asp:TextBox>
                                    </td>
                                    <td class="Form_TextoDer">
                                        &nbsp;</td>
                                    <td class="Form_TextoIzq">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="Form_TextoDer">
                                        Timón Cambiado:</td>
                                    <td class="Form_TextoIzq">
                                        <asp:DropDownList ID="ddlTimonCambiado" runat="server" Width="95px" >
                                            <asp:ListItem Text="NO" Value="SI" Selected="True"></asp:ListItem>
                                            <asp:ListItem  Text="SI" Value="SI"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Form_TextoDer">
                                        GPS:</td>
                                    <td class="Form_TextoIzq">
                                        <asp:DropDownList ID="ddlGps" runat="server" Width="95px" >
                                            <asp:ListItem Text="NO" Value="SI" Selected="True"></asp:ListItem>
                                            <asp:ListItem  Text="SI" Value="SI"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Sistema Gas:</td>
                                    <td class="Form_TextoIzq">
                                        <asp:DropDownList ID="ddlGas" runat="server" Width="95px" >
                                            <asp:ListItem Text="NO" Value="SI" Selected="True"></asp:ListItem>
                                            <asp:ListItem  Text="SI" Value="SI"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Form_TextoDer">
                                        &nbsp;</td>
                                    <td class="Form_TextoIzq">
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </dxp:PanelContent>
                    </PanelCollection>
                </dxrp:ASPxRoundPanel>
                <br />
                <dxrp:ASPxRoundPanel ID="ASPxRoundPanel4" runat="server" Width="100%" HeaderText="INFORMACIÓN DEL PLAN">
                    <PanelCollection>
                        <dxp:PanelContent ID="PanelContent3" runat="server" SupportsDisabledAttribute="True">
                            <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td class="Form_TextoDer">
                                        Plan:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:DropDownList ID="ddlPlan" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPlan_SelectedIndexChanged"
                                            Width="95px">
                                        </asp:DropDownList>
                                        <span class="Form_TextoObligatorio">*</span>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Frecuencia:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:DropDownList ID="ddlFrecuencia" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlFrecuencia_SelectedIndexChanged"
                                            Width="95px">
                                        </asp:DropDownList>
                                        <span class="Form_TextoObligatorio">*</span>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Moneda Prima:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:DropDownList ID="ddlMonPrima" runat="server" Width="95px">
                                        </asp:DropDownList>
                                        <span class="Form_TextoObligatorio">*</span>
                                    </td>
                                    <td class="Form_TextoIzq">
                                        Valor de Cuota:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:UpdatePanel ID="upCotPrimCertificado" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtPrimTotal" runat="server" CssClass="Form_TextBoxMontoDisable"
                                                    Width="90px" ReadOnly="true"></asp:TextBox>
                                                <span class="Form_TextoObligatorio">*</span>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddlPlan" EventName="SelectedIndexChanged" />
                                                <asp:AsyncPostBackTrigger ControlID="ddlFrecuencia" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>                                        
                                    </td>
                                </tr>
                            </table>
                        </dxp:PanelContent>
                    </PanelCollection>
                </dxrp:ASPxRoundPanel>
                <br />
                <dxrp:ASPxRoundPanel ID="ASPxRoundPanel3" runat="server" Width="100%" HeaderText="INFORMACIÓN DE INSPECCIÓN">
                    <PanelCollection>
                        <dxp:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True">
                            <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                <tr>
                                    <td class="Form_TextoIzq">
                                        <asp:CheckBox ID="cbxContacto" runat="server" Text="¿Cliente es Contacto?" />
                                    </td>
                                    <td class="Form_TextoDer">
                                        Contacto:
                                    </td>
                                    <td class="Form_TextoIzq" >
                                        <asp:TextBox ID="txtContacto" runat="server" CssClass="Form_TextBox" 
                                            Width="180px"></asp:TextBox>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Nro Telefono:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="TextBox2" runat="server" CssClass="Form_TextBox" Width="90px"></asp:TextBox>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Nro Movil:</td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="TextBox3" runat="server" CssClass="Form_TextBox" Width="90px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="Form_TextoDer">
                                    </td>                                    
                                    <td class="Form_TextoDer">
                                        Email:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="TextBox4" runat="server" CssClass="Form_TextBox" Width="180px"></asp:TextBox>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Observaciones:
                                    </td>
                                    <td class="Form_TextoIzq" colspan="3">
                                        <asp:TextBox ID="txtobservaciones" runat="server" CssClass="Form_TextBox" 
                                            Width="275px"></asp:TextBox>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="Form_TextoIzq">
                                        <asp:CheckBox ID="CheckBox1" runat="server" Text="¿Dirección de cliente?"  />
                                    </td>
                                    <td class="Form_TextoDer">
                                        Dirección:</td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtDirEntrega" runat="server" CssClass="Form_TextBox" 
                                            MaxLength="250" Width="180px"></asp:TextBox>
                                        <span class="Form_TextoObligatorio">*</span>                                        
                                    </td>
                                    <td class="Form_TextoDer">
                                        &nbsp;</td>
                                    <td class="Form_TextoIzq">
                                        &nbsp;</td>
                                    <td class="Form_TextoDer">
                                        &nbsp;</td>
                                    <td class="Form_TextoIzq">
                                        &nbsp;</td>
                                </tr>
                                
                                <tr>
                                    <td class="Form_TextoIzq">
                                    </td>
                                    <td class="Form_TextoDer">
                                        Departamento:</td>
                                    <td class="Form_TextoIzq">
                                        <asp:UpdatePanel ID="upDepartamentoIsnp" runat="server">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                    <tr>
                                                        <td style="width: 100px;">
                                                            <asp:DropDownList ID="ddlDepartamentoInsp" runat="server" AutoPostBack="True" 
                                                                OnSelectedIndexChanged="ddlDepartamentoInsp_SelectedIndexChanged" Width="105px">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 5px;">
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td style="width: 10px;">
                                                            <asp:UpdateProgress ID="UpdateProgress1" runat="server" 
                                                                AssociatedUpdatePanelID="upDepartamentoIsnp" DisplayAfter="0">
                                                                <ProgressTemplate>
                                                                    <asp:Image ID="imgLoadDepInsp" runat="server" Height="10px" 
                                                                        SkinID="sknImgLoading" Width="10px" />
                                                                </ProgressTemplate>
                                                            </asp:UpdateProgress>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Provincia:</td>
                                    <td class="Form_TextoIzq">
                                        <asp:UpdatePanel ID="upProvinciaInsp" runat="server">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                    <tr>
                                                        <td style="width: 100px;">
                                                            <asp:DropDownList ID="ddlProvinciaInsp" runat="server" AutoPostBack="True" 
                                                                OnSelectedIndexChanged="ddlProvinciaInsp_SelectedIndexChanged" Width="105px">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 5px;">
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td style="width: 10px;">
                                                            <asp:UpdateProgress ID="UpdateProgress2" runat="server" 
                                                                AssociatedUpdatePanelID="upProvinciaInsp" DisplayAfter="0">
                                                                <ProgressTemplate>
                                                                    <asp:Image ID="imgLoadProvInsp" runat="server" Height="10px" 
                                                                        SkinID="sknImgLoading" Width="10px" />
                                                                </ProgressTemplate>
                                                            </asp:UpdateProgress>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddlDepartamentoInsp" 
                                                    EventName="selectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Distrito:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:UpdatePanel ID="upDistritoInsp" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="ddlDistritoInsp" runat="server" Width="105px">
                                                </asp:DropDownList>
                                                <span class="Form_TextoObligatorio">*</span>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddlProvinciaInsp" 
                                                    EventName="selectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                
                            </table>
                            <br />
                        </dxp:PanelContent>
                    </PanelCollection>
                </dxrp:ASPxRoundPanel>
                <br />
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="Form_TextoIzq">
                            <span class="Form_TextoObligatorio">&nbsp;(*)Campos Obligatorios</span>
                        </td>
                        <td class="Form_TextoDer">
                                <asp:Button ID="btnImprimir" runat="server" Text="Imprimir" Width="90px" CausesValidation="true"
                                    Enabled="false" 
                                    OnClientClick="return showModalPopup('bmpCotDocCertificado');" />
                                <asp:Button ID="btnExpedir" runat="server" Text="Expedir" Width="90px" CausesValidation="true"
                                    ValidationGroup="valExpedir" OnClick="btnExpedir_Click" />
                        </td>
                    </tr>
                </table>
                <br />
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>        
&nbsp;&nbsp;&nbsp;         
</asp:Content>
