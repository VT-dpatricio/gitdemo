﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Motor.Resources; 
using AONAffinity.Motor.BusinessLogic;
using AONAffinity.Motor.BusinessEntity;

namespace AONWebMotor.Certificado
{
    public partial class frmExpedir_5292 : PageBase 
    {
        Int32 nIdProducto = 5292;
        Int32 nidSponsor = 3;
        Int32 nIdPais = 51;

        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack) 
                {
                    this.CargarFormulario();
                }                
            }
            catch (Exception ex) 
            {

            }
        }

        protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.CargarProvincia();
                this.CargarDistrito();
            }
            catch (Exception ex) 
            {

            } 
        }

        protected void ddlProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.CargarDistrito();
            }
            catch (Exception ex) 
            {
                
            }
        }

        protected void ddlDepartamentoInsp_SelectedIndexChanged(object sender, EventArgs e)
        {
            try 
            {
                this.CargarProvinciaEntrega();
                this.CargarDistritoEntrega();
            }
            catch (Exception ex)             
            {
 
            }
        }

        protected void ddlProvinciaInsp_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.CargarDistritoEntrega();
            }
            catch (Exception ex)
            {

            }
        }

        protected void ddlPlan_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlFrecuencia_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnExpedir_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex) 
            {

            }
        }

        #endregion


        #region Tab1
        private void CargarFormulario() 
        {
            this.CargarInformador();
            this.CargarOficina();
            this.CargarFecha(); 
            this.CargarTipoDocumento();
            this.CargarCiudades();
            this.CargarDepartamento();
            this.CargarDepartamentoEntrega();
            this.CargarMarca(); 
            this.CargarColor();
            this.CargarPlan();
            this.CargarFrecuencia();
            this.CargarMonedaPrima(); 
        }

        private void CargarInformador()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLInformador objBLInformador = new AONAffinity.Motor.BusinessLogic.Bais.BLInformador();
            this.CargarDropDownList(this.ddlInformador, "IdInformador", "Nombre", objBLInformador.ListarxSponsor(this.nidSponsor), true);
        }

        private void CargarOficina() 
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLOficina objBLOficina = new AONAffinity.Motor.BusinessLogic.Bais.BLOficina();
            this.CargarDropDownList(this.ddlOficina, "IdOficina", "Nombre", objBLOficina.ListarxSponsor(this.nidSponsor), true);
        }

        private void CargarFecha() 
        {
            this.txtFechaVenta.Text = DateTime.Now.ToShortDateString().ToString();      
        }

        private void CargarTipoDocumento() 
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLTipoDocProducto objBLTipoDocProducto = new AONAffinity.Motor.BusinessLogic.Bais.BLTipoDocProducto();     
            this.CargarDropDownList(this.ddlTipDocumento, "IdTipoDocumento", "Nombre", objBLTipoDocProducto.ListarxProducto(nIdProducto), true);    
        }

        private void CargarCiudades()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.Bais.BLCiudad();
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad = objBLCiudad.Listar(this.nIdPais);
            ViewState[ValorConstante.ListaCiudad] = lstBECiudad;
        }

        private void CargarDepartamento()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLDepartamento objBLDepartamento = new AONAffinity.Motor.BusinessLogic.Bais.BLDepartamento();
            this.CargarDropDownList(this.ddlDepartamento, "idDepartamento", "nombre", objBLDepartamento.Listar(51), true);
        }

        private void CargarProvincia()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.Bais.BLCiudad();
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad = (List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad>)(ViewState[ValorConstante.ListaCiudad]);
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad_Prov = objBLCiudad.ListarProvincia(Convert.ToInt32(this.ddlDepartamento.SelectedValue), lstBECiudad);
            this.CargarDropDownList(this.ddlProvincia, "idProvincia", "nombre", lstBECiudad_Prov, true);
        }

        private void CargarDistrito()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.Bais.BLCiudad();
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad = (List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad>)(ViewState[ValorConstante.ListaCiudad]);
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad_Dist = objBLCiudad.ListarDistrito(this.ddlProvincia.SelectedValue, lstBECiudad);
            this.CargarDropDownList(this.ddlDistrito, "idCiudad", "nombre", lstBECiudad_Dist, true);
        }

        private void CargarDepartamentoEntrega()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLDepartamento objBLDepartamento = new AONAffinity.Motor.BusinessLogic.Bais.BLDepartamento();
            this.CargarDropDownList(this.ddlDepartamentoInsp, "idDepartamento", "nombre", objBLDepartamento.Listar(this.nIdPais), true);
        }

        private void CargarProvinciaEntrega()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.Bais.BLCiudad();
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad = (List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad>)(ViewState[ValorConstante.ListaCiudad]);
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad_Prov = objBLCiudad.ListarProvincia(Convert.ToInt32(this.ddlDepartamentoInsp.SelectedValue), lstBECiudad);
            this.CargarDropDownList(this.ddlProvinciaInsp, "idProvincia", "nombre", lstBECiudad_Prov, true);
        }

        private void CargarDistritoEntrega()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.Bais.BLCiudad();
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad = (List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad>)(ViewState[ValorConstante.ListaCiudad]);
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad_Dist = objBLCiudad.ListarDistrito(this.ddlProvinciaInsp.SelectedValue, lstBECiudad);
            this.CargarDropDownList(this.ddlDistritoInsp, "idCiudad", "nombre", lstBECiudad_Dist, true);
        }

        private void CargarMarca() 
        {
            BLProductoMarca objBLProductoMarca = new BLProductoMarca();
            this.CargarDropDownList(this.ddlMarca, "IdMarca", "Descripcion", objBLProductoMarca.ListarxProducto(this.nIdProducto, true), true);
        }

        private void CargarColor() 
        {
            BLProductoColor objBLProductoColor = new BLProductoColor();
            this.CargarDropDownList(this.ddlColor, "IdColor", "Descripcion", objBLProductoColor.ListarxProducto(this.nIdProducto, true), true);
        }

        private void CargarPlan() 
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLOpcion objBLopcion = new AONAffinity.Motor.BusinessLogic.Bais.BLOpcion();
            this.CargarDropDownList(this.ddlPlan, "Opcion", "Opcion", objBLopcion.Listar(this.nIdProducto), true);    
        }

        private void CargarFrecuencia() 
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLFrecuencia objBLFrecuencia = new AONAffinity.Motor.BusinessLogic.Bais.BLFrecuencia();
            this.CargarDropDownList(this.ddlFrecuencia, "IdFrecuencia", "Nombre", objBLFrecuencia.Listar(nIdProducto), true); 
        }

        private void CargarMonedaPrima() 
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLMoneda objBLMoneda = new AONAffinity.Motor.BusinessLogic.Bais.BLMoneda();
            this.CargarDropDownList(this.ddlMonPrima, "IdMoneda", "IdMoneda", objBLMoneda.Listar(nIdProducto), true);    
        }

        private void Expedir() 
        {

        }
        #endregion

        
       

        

        
    }
}