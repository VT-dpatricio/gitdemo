﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessLogic;
using AONAffinity.Motor.BusinessEntity;
      
namespace AONWebMotor.Certificado
{
    public partial class frm_Expedir5251 : PageBase
    {
        #region Variables
        private static List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoProducto> lstBEInfoProducto = new List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoProducto>();
        private static List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAsegurado> lstBEInfoAsegurado = new List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAsegurado>();
        private static AONAffinity.Motor.BusinessEntity.Bais.BEUsuario objBEUsuario = new AONAffinity.Motor.BusinessEntity.Bais.BEUsuario();
        private static String cIdInformador;
        private static Int32 nIdOficina = NullTypes.IntegerNull;
        private static String cIdMoneda = "USD";
        private static String cNombreInformador = String.Empty;
        #endregion

        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (!String.IsNullOrEmpty(Request.QueryString["idPer"]) && !String.IsNullOrEmpty(Request.QueryString["nroCot"]))
                    {
                        this.hfIdPeriodo.Value = Request.QueryString["idPer"];
                        this.hfNroCotizacion.Value = Request.QueryString["nroCot"];
                        this.hfIdProducto.Value = Request.QueryString["idProd"];
                        this.CargarFormulario();
                    }
                }
            }
            catch (Exception ex)
            {                
                this.MostrarMensaje(this.Controls, "Se presentó un problema al cargar el formulario. " + ex.Message, false);
            }
        }
        #endregion

        

        #region Panel Cliente
        protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.CargarProvincia();
                this.CargarDistrito();
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);  
            }
        }

        protected void ddlProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {                
                this.CargarDistrito();
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void btnAcualizar_Click(object sender, EventArgs e)
        {
            try 
            {
                if (this.ValidarCliente()) 
                {
                    this.ActualizarClienteVehiculo();
                }                
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void btnSiguiente_Click(object sender, EventArgs e)
        {
            try
            {
                this.pnCliente.Visible = false;
                this.pnCertificado.Visible = true;
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }
        #endregion        
        
        #region Panel Vehiculo 
        protected void ddlDepEntrega_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.CargarProvinciaEntrega();
                this.CargarDistritoEntrega();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void ddlProvEntrega_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.CargarDistritoEntrega();
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void ddlPlan_SelectedIndexChanged(object sender, EventArgs e)
        {
            try 
            {
                this.ddlFrecuencia_SelectedIndexChanged(sender, e);
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }            
        }

        protected void ddlFrecuencia_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.MostrarPrima();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void btnExpedir_Click(object sender, EventArgs e)
        {
            try
            {
                this.Expedir();
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            } 
        }
        #endregion


        #region Métodos Privados
        private void CargarFormulario() 
        {
            this.CargarInfoProducto();
            this.CargarInfoAsegurado();
            this.CargarPlan();
            this.CargarFrecuencia();
            this.CargarMonedaPrima();
            this.CargarCiudades();
            this.CargarDepartamento();
            this.CargarDepartamentoEntrega();
            this.CargarInfoUsuario();
            this.CargarInformadorOficina();
            this.CargarFuncionesJS();
            this.CargarCotizacion();
            this.CargarSpeech();
        }

        private void CargarFuncionesJS() 
        {
            this.btnAcualizar.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro actualizar los datos del cliente y del vehículo?')== false) return false;");
            this.btnExpedir.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de guardar la venta?')== false) return false;");
        }

        private void CargarCiudades()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.Bais.BLCiudad();
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad = objBLCiudad.Listar(51);
            ViewState[ValorConstante.ListaCiudad] = lstBECiudad;
        }

        private void CargarPlan() 
        {
            BLCotizacion objBLCotizacion = new BLCotizacion();
            BECotizacion objBECotizacion = objBLCotizacion.Obtener(Convert.ToInt32(this.hfIdPeriodo.Value), Convert.ToDecimal(this.hfNroCotizacion.Value));  

            AONAffinity.Motor.BusinessLogic.Bais.BLOpcion objBLOpcion = new AONAffinity.Motor.BusinessLogic.Bais.BLOpcion();
            List<AONAffinity.Motor.BusinessEntity.Bais.BEOpcion> lstBEOpcion = objBLOpcion.Listar(Convert.ToInt32(this.hfIdProducto.Value));

            if (lstBEOpcion.Count > 1)
            {
                this.CargarDropDownList(this.ddlPlan, "OPCION", "OPCION", lstBEOpcion, true);
            }
            else 
            {
                this.CargarDropDownList(this.ddlPlan, "OPCION", "OPCION", lstBEOpcion, true);
            }

            //Si cotización solo tiene tasa anual, solo se muestra plan anual.
            if (objBECotizacion.TasaAnual != NullTypes.DecimalNull && objBECotizacion.TasaBianual == NullTypes.DecimalNull)
            {
                this.ddlPlan.SelectedValue = ValorConstante.PlanAnual;
                this.ddlPlan.Enabled = false;
            }

            //Si cotización solo tiene tasa bianual, solo se muestra plan bianual.
            if (objBECotizacion.TasaAnual == NullTypes.DecimalNull && objBECotizacion.TasaBianual != NullTypes.DecimalNull)
            {
                this.ddlPlan.SelectedValue = ValorConstante.PlanBianual;
                this.ddlPlan.Enabled = false;
            }
        }

        private void CargarFrecuencia()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLFrecuencia objBLFrecuencia = new AONAffinity.Motor.BusinessLogic.Bais.BLFrecuencia();
            List<AONAffinity.Motor.BusinessEntity.Bais.BEFrecuencia> lstBEFrecuencia = objBLFrecuencia.Listar(Convert.ToInt32(this.hfIdProducto.Value));

            if (lstBEFrecuencia == null)
            {
                throw new Exception("No existe frecuencia configurada para el producto.");
            }

            if (lstBEFrecuencia.Count > 1)
            {
                this.CargarDropDownList(this.ddlFrecuencia, "IdFrecuencia", "Nombre", lstBEFrecuencia, true);
            }
            else
            {
                this.CargarDropDownList(this.ddlFrecuencia, "IdFrecuencia", "Nombre", lstBEFrecuencia, true);
            }
        }

        private void CargarMonedaPrima()
        {            
            AONAffinity.Motor.BusinessLogic.Bais.BLMoneda objBLMoneda = new AONAffinity.Motor.BusinessLogic.Bais.BLMoneda();
            List<AONAffinity.Motor.BusinessEntity.Bais.BEMoneda> lstBEMoneda = objBLMoneda.Listar(Convert.ToInt32(this.hfIdProducto.Value));

            if (lstBEMoneda != null)
            {
                if (lstBEMoneda.Count > 1)
                {
                    this.CargarDropDownList(this.ddlMonPrima, "IdMoneda", "nombre", lstBEMoneda, true);
                }
                else
                {
                    this.CargarDropDownList(this.ddlMonPrima, "IdMoneda", "nombre", lstBEMoneda, false);
                }
            }
        }

        private void CargarDepartamento()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLDepartamento objBLDepartamento = new AONAffinity.Motor.BusinessLogic.Bais.BLDepartamento();
            this.CargarDropDownList(this.ddlDepartamento, "idDepartamento", "nombre", objBLDepartamento.Listar(51), true);
        }

        private void CargarProvincia()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.Bais.BLCiudad();
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad = (List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad>)(ViewState[ValorConstante.ListaCiudad]);
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad_Prov = objBLCiudad.ListarProvincia(Convert.ToInt32(this.ddlDepartamento.SelectedValue), lstBECiudad);
            this.CargarDropDownList(this.ddlProvincia, "idProvincia", "nombre", lstBECiudad_Prov, true);
        }

        private void CargarDistrito()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.Bais.BLCiudad();
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad = (List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad>)(ViewState[ValorConstante.ListaCiudad]);
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad_Dist = objBLCiudad.ListarDistrito(this.ddlProvincia.SelectedValue, lstBECiudad);
            this.CargarDropDownList(this.ddlDistrito, "idCiudad", "nombre", lstBECiudad_Dist, true);
        }

        private void CargarDepartamentoEntrega() 
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLDepartamento objBLDepartamento = new AONAffinity.Motor.BusinessLogic.Bais.BLDepartamento();
            this.CargarDropDownList(this.ddlDepEntrega, "idDepartamento", "nombre", objBLDepartamento.Listar(51), true);
        }

        private void CargarProvinciaEntrega()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.Bais.BLCiudad();
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad = (List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad>)(ViewState[ValorConstante.ListaCiudad]);
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad_Prov = objBLCiudad.ListarProvincia(Convert.ToInt32(this.ddlDepEntrega.SelectedValue), lstBECiudad);
            this.CargarDropDownList(this.ddlProvEntrega, "idProvincia", "nombre", lstBECiudad_Prov, true);
        }

        private void CargarDistritoEntrega()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.Bais.BLCiudad();
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad = (List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad>)(ViewState[ValorConstante.ListaCiudad]);
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad_Dist = objBLCiudad.ListarDistrito(this.ddlProvEntrega.SelectedValue, lstBECiudad);
            this.CargarDropDownList(this.ddlDistEntrega, "idCiudad", "nombre", lstBECiudad_Dist, true);
        }

        private void CargarInfoUsuario()
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            AONAffinity.Motor.BusinessLogic.Bais.BLUsuario objBLUsuario = new AONAffinity.Motor.BusinessLogic.Bais.BLUsuario();
            objBEUsuario = objBLUsuario.Obtener(Session[NombreSession.Usuario].ToString());

            if (objBEUsuario == null)
            {
                throw new Exception("No existe información del informador, contactar con administrador.");
            }
        }

        private void CargarInfoProducto() 
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLInfoProducto objBLInfoProducto = new AONAffinity.Motor.BusinessLogic.Bais.BLInfoProducto();
            lstBEInfoProducto = objBLInfoProducto.ObtenerxProducto(Convert.ToInt32(this.hfIdProducto.Value));      
        }

        private void CargarInfoAsegurado() 
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLInfoAsegurado objBLInfoAsegurado = new AONAffinity.Motor.BusinessLogic.Bais.BLInfoAsegurado();
            lstBEInfoAsegurado = objBLInfoAsegurado.ObtenerxProducto(Convert.ToInt32(this.hfIdProducto.Value));      
        }

        private void CargarCotizacion() 
        {
            BLCotizacion objBLCotizacion = new BLCotizacion();
            BECotizacion objBECotizacion = objBLCotizacion.Obtener(Convert.ToInt32(this.hfIdPeriodo.Value), Convert.ToDecimal(this.hfNroCotizacion.Value));
            
            //Información del cliente
            this.txtNombre1.Text = objBECotizacion.PriNombre;
            this.txtNombre2.Text = objBECotizacion.SegNombre;
            this.txtApellidoPat.Text = objBECotizacion.ApePaterno;
            this.txtApellidoMat.Text = objBECotizacion.ApeMaterno;            
            this.txtNroDocumento.Text = objBECotizacion.NroDocumento;
            if (objBECotizacion.FecNacimiento != NullTypes.FechaNull) { this.txtFecNacimiento.Text = objBECotizacion.FecNacimiento.ToShortDateString(); }
            this.txtDireccion.Text = objBECotizacion.Direccion;
            this.txtTelefono1.Text = objBECotizacion.TelDomicilio1;
            this.txtTelefono2.Text = objBECotizacion.TelMovil1;
            this.txtEmail.Text = objBECotizacion.Email1;
            //Información del vehículo
            this.txtMarca.Text = objBECotizacion.DesMarca;
            this.txtModelo.Text = objBECotizacion.DesModelo;
            this.txtAnioFab.Text = objBECotizacion.AnioFab.ToString();
            this.txtNroMotor.Text = objBECotizacion.NroMotor;
            if (objBECotizacion.NroPlaca == "PORESPECIFICAR" || objBECotizacion.NroPlaca == "POR ESPECIFICAR") { this.txtNroPlaca.Text = String.Empty; } else { this.txtNroPlaca.Text = objBECotizacion.NroPlaca; }
            if (objBECotizacion.Color == "PORESPECIFICAR" || objBECotizacion.Color == "POR ESPECIFICAR") { this.txtColor.Text = String.Empty; } else { this.txtColor.Text = objBECotizacion.Color; }
            this.txtTipVehiculo.Text = objBECotizacion.DesTipoVehiculo;
            this.txtClaseVehiculo.Text = objBECotizacion.DesClase;
            if (objBECotizacion.NroAsientos != NullTypes.IntegerNull) { this.txtNroAsientos.Text = objBECotizacion.NroAsientos.ToString(); }
            if (objBECotizacion.NroSerie == "PORESPECIFICAR" || objBECotizacion.NroSerie == "POR ESPECIFICAR") { } else { this.txtNroChasis.Text = objBECotizacion.NroSerie; }            
        }

        private void CargarSpeech() 
        {
            BLCotizacion objBLCotizacion = new BLCotizacion();
            BECotizacion objBECotizacion = objBLCotizacion.Obtener(Convert.ToInt32(this.hfIdPeriodo.Value), Convert.ToDecimal(this.hfNroCotizacion.Value));

            BLParametro objBLParametro = new BLParametro();
            BEParametro objBEParametro = objBLParametro.Obtener(24);            

            if (objBEParametro != null)
            {
                String[] cArrCli = new String[1];
                cArrCli[0] = objBECotizacion.ApePaterno;
                this.lblSpecchCli.Text = Funciones.RemplazarValores(objBEParametro.ValorCadena, cArrCli); 
            }

            objBEParametro = objBLParametro.Obtener(25);

            if (objBEParametro != null)
            {
                String[] cArrCli = new String[1];
                cArrCli[0] = objBECotizacion.ApePaterno;
                this.lblSpecchVeh.Text = Funciones.RemplazarValores(objBEParametro.ValorCadena, cArrCli);
            }

            objBEParametro = objBLParametro.Obtener(29);

            if (objBEParametro != null)
            {
                String[] cArrBen = new String[1];
                cArrBen[0] = objBECotizacion.ApePaterno;
                this.lblSpeechBen.Text = Funciones.RemplazarValores(objBEParametro.ValorCadena, cArrBen);
            }

            //Speech Cobertturas y Exclusiones.
            objBEParametro = objBLParametro.Obtener(30);

            if (objBEParametro != null)
            {
                this.lblSpeechCob.Text = objBEParametro.ValorCadena;
            }

            //Speech Coordinación de Envio de Póliza y Pago                
            objBEParametro = objBLParametro.Obtener(31);

            if (objBEParametro != null)
            {
                String[] cArrCoor = new String[1];
                cArrCoor[0] = objBECotizacion.ApePaterno;
                this.lblSpeechCoor.Text = Funciones.RemplazarValores(objBEParametro.ValorCadena, cArrCoor);
            }

            //Planes y medios de pago
            if (this.hfIdProducto.Value == "5250")
            {
                objBEParametro = objBLParametro.Obtener(40);

                if (objBLParametro != null) 
                {
                    String[] cArrPlan = new String[1];
                    cArrPlan[0] = objBECotizacion.ApePaterno;
                    this.lblSpechPlan.Text =  Funciones.RemplazarValores(objBEParametro.ValorCadena, cArrPlan);
                }                                 
            }
            else 
            {
                objBEParametro = objBLParametro.Obtener(26);

                if (objBEParametro != null)
                {
                    this.lblSpechPlan.Text = objBEParametro.ValorCadena;
                }
            }

            //Excluciones
            objBEParametro = objBLParametro.Obtener(41);
            if (objBEParametro != null)             
            {
                this.lblExcluciones.Text = objBEParametro.ValorCadena;
            }

            //Importante
            objBEParametro = objBLParametro.Obtener(42);
            if (objBEParametro != null)
            {
                String[] cArrImp = new String[1];
                cArrImp[0] = objBECotizacion.ApePaterno;
                this.lblImportante.Text = Funciones.RemplazarValores(objBEParametro.ValorCadena, cArrImp);
            }
            
            //Despedida
            objBEParametro = objBLParametro.Obtener(43);
            if (objBEParametro != null)
            {
                String[] cArrImp = new String[1];
                cArrImp[0] = objBECotizacion.ApePaterno;
                this.lblDespedida.Text = Funciones.RemplazarValores(objBEParametro.ValorCadena, cArrImp);
            }
            
            //Inspeccion
            if (objBECotizacion.FecFinVigPoliza < DateTime.Now)
            {
                objBEParametro = objBLParametro.Obtener(44);

                if (objBEParametro != null)
                {
                    String[] cArrIsnp = new String[1];
                    cArrIsnp[0] = objBECotizacion.ApePaterno;
                    this.lblInspeccion.Text = Funciones.RemplazarValores(objBEParametro.ValorCadena, cArrIsnp);
                }
            }
            else 
            {
                this.lblInspeccion.Visible = false;
            }

            //Speech Despedida
            //objBEParametro = objBLParametro.Obtener(32);

            //if (objBEParametro != null)
            //{
            //    String[] cArrDesp = new String[1];
            //    cArrDesp[0] = objBECotizacion.ApePaterno;
            //    this.lblSpeechDesp.Text = Funciones.RemplazarValores(objBEParametro.ValorCadena, cArrDesp);
            //}
        }

        private void CargarInformadorOficina()
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            if (objBEUsuario.IdInformador == NullTypes.CadenaNull)
            {
                throw new Exception("El usuario no tiene asignado un código de informador.");
            }

            AONAffinity.Motor.BusinessLogic.Bais.BLInformador objBLInformador = new AONAffinity.Motor.BusinessLogic.Bais.BLInformador();
            AONAffinity.Motor.BusinessEntity.Bais.BEInformador objBEInformador = objBLInformador.Obtener(objBEUsuario.IdInformador);

            if (objBEInformador == null)
            {
                throw new Exception("El usuario no tiene asignado un código de informador.");
            }

            if (objBEInformador.IdOficina == NullTypes.IntegerNull)
            {
                throw new Exception("No existe una oficina asignada al informador.");
            }

            cIdInformador = objBEInformador.IdInformador;
            nIdOficina = objBEInformador.IdOficina;
            cNombreInformador = objBEInformador.Nombre;
        }

        private void ActualizarClienteVehiculo() 
        {
            BLCotizacion objBLCotizacion = new BLCotizacion();

            BECotizacion objBECotizacion = objBLCotizacion.Obtener(Convert.ToInt32(this.hfIdPeriodo.Value), Convert.ToDecimal(this.hfNroCotizacion.Value));  
            
            BECotizacion objBECotizacion_Cli = new BECotizacion();

            objBECotizacion_Cli.IdPeriodo = Convert.ToInt32(this.hfIdPeriodo.Value);
            objBECotizacion_Cli.NroCotizacion = Convert.ToDecimal(this.hfNroCotizacion.Value);            
            objBECotizacion_Cli.ApePaterno = this.txtApellidoPat.Text.Trim().ToUpper();
            objBECotizacion_Cli.ApeMaterno = this.txtApellidoMat.Text.Trim().ToUpper();
            objBECotizacion_Cli.PriNombre = this.txtNombre1.Text.Trim().ToUpper();
            objBECotizacion_Cli.SegNombre = this.txtNombre2.Text.Trim().ToUpper();
            objBECotizacion_Cli.IdTipoDocumento = this.ddlTipDocumento.SelectedValue;
            objBECotizacion_Cli.NroDocumento = this.txtNroDocumento.Text.Trim();
            objBECotizacion_Cli.Sexo = this.ddlSexo.SelectedValue;            
            objBECotizacion_Cli.FecNacimiento = Convert.ToDateTime(this.txtFecNacimiento.Text);
            objBECotizacion_Cli.IdCiudad = Convert.ToInt32(this.ddlDistrito.SelectedValue);
            objBECotizacion_Cli.Direccion = this.txtDireccion.Text.Trim().ToUpper();
            objBECotizacion_Cli.TelDomicilio1 = this.txtTelefono1.Text.Trim();
            objBECotizacion_Cli.TelDomicilio2 = NullTypes.CadenaNull;
            objBECotizacion_Cli.TelDomicilio3 = NullTypes.CadenaNull;
            objBECotizacion_Cli.TelMovil1 = this.txtTelefono2.Text.Trim();
            objBECotizacion_Cli.TelMovil2 = NullTypes.CadenaNull;
            objBECotizacion_Cli.TelMovil3 = NullTypes.CadenaNull;
            objBECotizacion_Cli.TelOficina1 = objBECotizacion.TelOficina1;
            objBECotizacion_Cli.TelOficina2 = NullTypes.CadenaNull;
            objBECotizacion_Cli.TelOficina3 = NullTypes.CadenaNull;
            objBECotizacion_Cli.Email1 = this.txtEmail.Text.Trim();
            objBECotizacion_Cli.Email2 = NullTypes.CadenaNull;
            objBECotizacion_Cli.NroPlaca = this.txtNroPlaca.Text.Trim().ToUpper();   
            objBECotizacion_Cli.NroSerie = this.txtNroChasis.Text.Trim();
            objBECotizacion_Cli.Color = this.txtColor.Text.Trim();
            objBECotizacion_Cli.IdUsoVehiculo = 1;
            objBECotizacion_Cli.NroAsientos = Convert.ToInt32(this.txtNroAsientos.Text.Trim());
            objBECotizacion_Cli.EsTimonCambiado = false;
            objBECotizacion_Cli.ReqGPS = false; 
            objBECotizacion_Cli.UsuarioModificacion = Session[NombreSession.Usuario].ToString();

            if (objBLCotizacion.Actualizar(objBECotizacion_Cli) > 0)
            {
                this.MostrarMensaje(this.Controls, "Se actualizó los datos del cliente y del vehículo.", false);
                this.btnSiguiente.Enabled = true;                             
            }
        }

        private List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC> CargarInfoProductoC() 
        {
            List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC> lstBEInfoProductoC = new List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC>();
            AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC objBEInfoProductoC = null;

            foreach (AONAffinity.Motor.BusinessEntity.Bais.BEInfoProducto objBEInfoProducto in lstBEInfoProducto) 
            {
                objBEInfoProductoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC();

                if (objBEInfoProducto.Nombre == "SEXO") 
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = this.ddlSexo.SelectedValue;
                    lstBEInfoProductoC.Add(objBEInfoProductoC);                        
                }
                if (objBEInfoProducto.Nombre == "ESTADOCIVIL") 
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = this.ddlEstadoCiv.SelectedValue;
                    lstBEInfoProductoC.Add(objBEInfoProductoC);                  
                }
                if (objBEInfoProducto.Nombre == "FECNACIMIENTO")
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoProductoC.ValorDate = Convert.ToDateTime(this.txtFecNacimiento.Text);  
                    objBEInfoProductoC.ValorString = NullTypes.CadenaNull;
                    lstBEInfoProductoC.Add(objBEInfoProductoC);                  
                }
                if (objBEInfoProducto.Nombre == "DIRENTREGA")
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = this.txtDirEntrega.Text.Trim(); 
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }               

                if (objBEInfoProducto.Nombre == "CODUBIGEOENTREGA")
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = Convert.ToDecimal(this.ddlDistEntrega.SelectedValue);
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = NullTypes.CadenaNull; 
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }
                if (objBEInfoProducto.Nombre == "CODSPONSOR")
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = NullTypes.DecimalNull; 
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = "01";
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }

                if (objBEInfoProducto.Nombre == "PERCOTIZACION")
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = Convert.ToDecimal(this.hfIdPeriodo.Value); 
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = NullTypes.CadenaNull;
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }

                if (objBEInfoProducto.Nombre == "NROCOTIZACION")
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = Convert.ToDecimal(this.hfNroCotizacion.Value);
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = NullTypes.CadenaNull;
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }
                if (objBEInfoProducto.Nombre == "PREFDIRECCION") 
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = NullTypes.DecimalNull; 
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = this.ddlPrefijo.SelectedValue; 
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }
                if (objBEInfoProducto.Nombre == "IDENTDOMICILIO")
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = this.txtMzLteNro.Text.Trim().ToUpper();   
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }
                if (objBEInfoProducto.Nombre == "IDENTEDIFICIO") 
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = this.txtAptoInt.Text.Trim().ToUpper();
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }
                if (objBEInfoProducto.Nombre == "REFDOMICILIO") 
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = this.txtReferencia.Text.Trim().ToUpper();
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }
                if (objBEInfoProducto.Nombre == "URBANIZACION") 
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = this.txtUrbanizacion.Text.Trim().ToUpper();
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }
                if (objBEInfoProducto.Nombre == "REFDOMICILIO2") 
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = this.txtReferencia2.Text.Trim().ToUpper();
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }
            }

            return lstBEInfoProductoC;
        }

        private List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC> CargarInfoAseguradoC(BECotizacion pObjBECotizacion)
        {
            List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC> lstBEInfoAseguradoC = new List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC>();
            AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC objBEInfoAseguradoC = null;

            foreach (AONAffinity.Motor.BusinessEntity.Bais.BEInfoAsegurado objBEInfoAsegurado in lstBEInfoAsegurado)
            {
                objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();

                if (objBEInfoAsegurado.Nombre == "REFERENCIA") 
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = this.txtReferencia.Text.Trim();  
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "MARCA")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = pObjBECotizacion.IdMarca;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = pObjBECotizacion.DesMarca;
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "MODELO")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = pObjBECotizacion.IdModelo;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = pObjBECotizacion.DesModelo;
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }                
                if (objBEInfoAsegurado.Nombre == "COLOR")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull; 
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = pObjBECotizacion.Color;
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "AÑOFABRICACIÓN")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = pObjBECotizacion.AnioFab; 
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull; 
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "NUMERODECHASIS")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = pObjBECotizacion.NroSerie; 
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "NUMERODEMOTOR")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = pObjBECotizacion.NroMotor;
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "NUMERODEMATRICULAPLACA")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = pObjBECotizacion.NroPlaca;
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }

                if (objBEInfoAsegurado.Nombre == "CATEGORÍAVEHÍCULO")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = pObjBECotizacion.IdCategoria;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull; 
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "NUMERODEASIENTOS")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = pObjBECotizacion.NroAsientos;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull;
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "INDICADORLIMAPROVINCIA")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull; 
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    if (pObjBECotizacion.IdCiudad.ToString().Substring(0, 4) == "5115") { objBEInfoAseguradoC.ValorString = "L"; } else { objBEInfoAseguradoC.ValorString = "P"; }                    
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);                    
                }
                if (objBEInfoAsegurado.Nombre == "INDICADORDEDSCTOPORFLOTA")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    if (pObjBECotizacion.Condicion == 1) 
                    { 
                        objBEInfoAseguradoC.ValorString = "D"; 
                    }
                    else if (pObjBECotizacion.Condicion == 2)
                    { 
                        objBEInfoAseguradoC.ValorString = "R"; 
                    }
                    else 
                    { 
                        objBEInfoAseguradoC.ValorString = "N"; 
                    }
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "NROVEHICULOSPORFLOTA")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull;
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "CLASEDEVEHÍCULOS")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = pObjBECotizacion.IdClase;  
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = pObjBECotizacion.DesClase; 
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "USODEVEHÍCULOS")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = pObjBECotizacion.IdUsoVehiculo;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = "PARTICULAR";
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }

                if (objBEInfoAsegurado.Nombre == "TIPODEVEHICULO")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = pObjBECotizacion.IdTipoVehiculo;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = pObjBECotizacion.DesTipoVehiculo; 
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }

                if (objBEInfoAsegurado.Nombre == "NUMEROTELÉFONO2")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull; 
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = pObjBECotizacion.TelDomicilio2; 
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);


                }
                if (objBEInfoAsegurado.Nombre == "NUMEROTELÉFONO3")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull; 
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }

                if (objBEInfoAsegurado.Nombre == "CORREOELECTRÓNICO")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = pObjBECotizacion.Email1; 
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "PORCENTAJEDSTORCGO")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    if (pObjBECotizacion.PorcentajeC != NullTypes.DecimalNull)
                    {
                        objBEInfoAseguradoC.ValorNum = pObjBECotizacion.PorcentajeC;
                    }
                    else 
                    {
                        objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull; 
                    }                    
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull; 
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "BLINDADO") 
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;                    
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;                    
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = "N";
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "TIMONCAMBIADO") 
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    if (pObjBECotizacion.EsTimonCambiado == true)
                    {
                        objBEInfoAseguradoC.ValorString = "S";
                    }
                    else 
                    {
                        objBEInfoAseguradoC.ValorString = "N";
                    }                    
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "INIDICADORGPS") 
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    if (pObjBECotizacion.ReqGPS == true)
                    {
                        objBEInfoAseguradoC.ValorString = "S";
                    }
                    else
                    {
                        objBEInfoAseguradoC.ValorString = "N";
                    }
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }                                
            }

            return lstBEInfoAseguradoC;
        }

        private void MostrarPrima()
        {
            Decimal nPrima = 0;

            if (this.ddlFrecuencia.SelectedValue != "-1")
            {
                BLCotizacion objBLCotizacion = new BLCotizacion();
                BECotizacion objBECotizacion = objBLCotizacion.Obtener(Convert.ToInt32(this.hfIdPeriodo.Value), Convert.ToDecimal(this.hfNroCotizacion.Value));

                //Si es plan anual
                if (this.ddlPlan.SelectedValue == ValorConstante.PlanAnual)
                {
                    //Obtener prima con recargo o descuento
                    if (objBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.DESCUENTO) || objBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.RECARGO))
                    {
                        nPrima = objBECotizacion.PrimaAnualC;
                    }
                    else //Obtener prima sin recargo o descuento                         
                    {
                        nPrima = objBECotizacion.PrimaAnual;
                    }

                    //Obtener la prima segun la frecuencia
                    if (this.ddlFrecuencia.SelectedValue == Convert.ToInt32(ValorConstante.TipoFrecuencia.PAGO_MENSUAL).ToString())
                    {
                        this.txtPrimTotal.Text = (nPrima == NullTypes.DecimalNull ? String.Empty : Decimal.Round(nPrima / 12, 2).ToString());
                    }
                    else
                    {
                        this.txtPrimTotal.Text = (nPrima == NullTypes.DecimalNull ? String.Empty : Decimal.Round(nPrima, 2).ToString());
                    }
                }

                //Si es plan bianual
                if (this.ddlPlan.SelectedValue == ValorConstante.PlanBianual)
                {
                    //Obtener prima con recargo o descuento
                    if (objBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.DESCUENTO) || objBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.RECARGO))
                    {
                        nPrima = objBECotizacion.PrimaBianualC;
                    }
                    else //Obtener prima sin recargo o descuento
                    {
                        nPrima = objBECotizacion.PrimaBianual;
                    }

                    //Obtener la prima segun la frecuencia
                    if (this.ddlFrecuencia.SelectedValue == Convert.ToInt32(ValorConstante.TipoFrecuencia.PAGO_MENSUAL).ToString())
                    {
                        this.txtPrimTotal.Text = (nPrima == NullTypes.DecimalNull ? String.Empty : Decimal.Round((nPrima / 24), 2).ToString());
                    }
                    else
                    {
                        this.txtPrimTotal.Text = (nPrima == NullTypes.DecimalNull ? String.Empty : Decimal.Round((nPrima), 2).ToString());
                    }
                }
            }
        }

        private void Expedir() 
        {
            BLCotizacion objBLCotizacion = new BLCotizacion();
            BECotizacion objBECotizacion = objBLCotizacion.Obtener(Convert.ToInt32(this.hfIdPeriodo.Value), Convert.ToDecimal(this.hfNroCotizacion.Value));

            AONAffinity.Motor.BusinessEntity.Bais.BECertificado objBECertificado = this.CargarCertificado(objBECotizacion);
            List<AONAffinity.Motor.BusinessEntity.Bais.BEAsegurado> lstBEAsegurado = this.CargarAsegurado(objBECotizacion);
            List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC> lstBEInfoProductoC = this.CargarInfoProductoC();
            List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC> lstBEInfoAseguradoC = this.CargarInfoAseguradoC(objBECotizacion);

            AONAffinity.Motor.BusinessLogic.Bais.BLCertificado objBLCertificado = new AONAffinity.Motor.BusinessLogic.Bais.BLCertificado();
            List<AONAffinity.Motor.BusinessEntity.Bais.BEExpedir> lstBEExpedir = objBLCertificado.Expedir(objBECertificado, lstBEAsegurado, null, lstBEInfoProductoC, lstBEInfoAseguradoC, String.Empty);

            if (lstBEExpedir[0].Codigo == 1)
            {
                this.MostrarMensaje(this.Controls, "El certificado se expidió con exito.", false);

                this.hfIdCertificado.Value = lstBEExpedir[0].Descripcion;
                Session["idCertificado"] = lstBEExpedir[0].Descripcion;
                this.btnExpedir.Enabled = false; 
                this.btnImprimir.Enabled = true;
                this.btnFinalizar.Enabled = true;  
                
                BECotizacion objBECotizacion_Exp = new BECotizacion();
                objBECotizacion_Exp.IdPeriodo = Convert.ToInt32(this.hfIdPeriodo.Value);
                objBECotizacion_Exp.NroCotizacion = Convert.ToDecimal(this.hfNroCotizacion.Value);
                objBECotizacion_Exp.UsuarioModificacion = Session[NombreSession.Usuario].ToString();
                objBLCotizacion.Expedir(objBECotizacion_Exp);
                                             
                this.rwCertificado.LocalReport.Refresh();
            }
            else 
            {                
                this.MostrarMensaje(this.Controls, "Se produjo un error al expedir el certificado." + lstBEExpedir[0].Descripcion, false);  
            }
        }

        private AONAffinity.Motor.BusinessEntity.Bais.BECertificado CargarCertificado(BECotizacion pObjBECotizacion) 
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLCertificado objBLCertificado = new AONAffinity.Motor.BusinessLogic.Bais.BLCertificado();
            AONAffinity.Motor.BusinessEntity.Bais.BECertificado objBECertificado = new AONAffinity.Motor.BusinessEntity.Bais.BECertificado();

            objBECertificado.IdProducto = Convert.ToInt32(this.hfIdProducto.Value);
            objBECertificado.Opcion = this.ddlPlan.SelectedValue;
            objBECertificado.IdFrecuencia = Convert.ToInt32(this.ddlFrecuencia.SelectedValue);
            objBECertificado.IdOficina = nIdOficina;
            objBECertificado.IdInformador = cIdInformador;
            objBECertificado.IdMedioPago = MedioPago.NoAplica;
            objBECertificado.IdEstadoCertificado = EstadoCertificado.PreActivo;
            objBECertificado.MontoAsegurado = pObjBECotizacion.ValVehiculo;
            objBECertificado.PrimaBruta = this.ObtenerPrimaTotal(pObjBECotizacion);
            objBECertificado.Iva = 0;
            objBECertificado.PrimaTotal = this.ObtenerPrimaTotal(pObjBECotizacion);
            objBECertificado.PrimaCobrar = this.ObtenerPrimaCobrar(pObjBECotizacion);
            objBECertificado.NumeroCuenta = "000000000000";
            objBECertificado.Vencimiento = NullTypes.FechaNull;
            objBECertificado.Vigencia = objBLCertificado.ObtenerInicioVigencia(Convert.ToInt32(this.hfIdProducto.Value), pObjBECotizacion.FecFinVigPoliza);
            objBECertificado.FinVigencia = objBLCertificado.ObtenerFinVigenciaxPlan(Convert.ToInt32(this.hfIdProducto.Value), objBECertificado.Vigencia, objBECertificado.Opcion);
            objBECertificado.Puntos = NullTypes.IntegerNull;
            objBECertificado.IdMotivoAnulacion = NullTypes.IntegerNull;
            objBECertificado.Consistente = false;
            objBECertificado.PuntosAnual = NullTypes.IntegerNull;
            objBECertificado.IdCiudad = pObjBECotizacion.IdCiudad;
            objBECertificado.Direccion = pObjBECotizacion.Direccion;
            objBECertificado.Telefono = pObjBECotizacion.TelDomicilio1;
            objBECertificado.Nombre1 = pObjBECotizacion.PriNombre;
            objBECertificado.Nombre2 = pObjBECotizacion.SegNombre;
            objBECertificado.Apellido1 = pObjBECotizacion.ApePaterno;
            objBECertificado.Apellido2 = pObjBECotizacion.ApeMaterno;
            objBECertificado.CcCliente = pObjBECotizacion.NroDocumento;
            objBECertificado.IdTipoDocumento = pObjBECotizacion.IdTipoDocumento;
            objBECertificado.CcCliente = pObjBECotizacion.NroDocumento;
            objBECertificado.Digitacion = DateTime.Now;
            objBECertificado.Incentivo = NullTypes.DecimalNull;
            objBECertificado.SaldoIncentivo = NullTypes.DecimalNull;
            objBECertificado.UsuarioCreacion = Session[NombreSession.Usuario].ToString();
            objBECertificado.Venta = DateTime.Now.Date;            
            objBECertificado.IdMonedaPrima = this.ddlMonPrima.SelectedValue;
            objBECertificado.IdMonedaCobro = cIdMoneda;
            objBECertificado.AfiliadoHasta = NullTypes.FechaNull;

            return objBECertificado;
        }

        private List<AONAffinity.Motor.BusinessEntity.Bais.BEAsegurado> CargarAsegurado(BECotizacion pObjBECotizacion)
        {
            List<AONAffinity.Motor.BusinessEntity.Bais.BEAsegurado> lstBEAsegurado = new List<AONAffinity.Motor.BusinessEntity.Bais.BEAsegurado>();
            AONAffinity.Motor.BusinessEntity.Bais.BEAsegurado objBEAsegurado = new AONAffinity.Motor.BusinessEntity.Bais.BEAsegurado();

            objBEAsegurado.Consecutivo = 1;
            objBEAsegurado.IdTipodocumento = pObjBECotizacion.IdTipoDocumento;
            objBEAsegurado.Ccaseg = pObjBECotizacion.NroDocumento;
            objBEAsegurado.Nombre1 = pObjBECotizacion.PriNombre;
            objBEAsegurado.Nombre2 = pObjBECotizacion.SegNombre;
            objBEAsegurado.Apellido1 = pObjBECotizacion.ApePaterno;
            objBEAsegurado.Apellido2 = pObjBECotizacion.ApeMaterno;
            objBEAsegurado.Direccion = pObjBECotizacion.Direccion;
            objBEAsegurado.Telefono = pObjBECotizacion.TelDomicilio1;
            objBEAsegurado.IdCiudad = pObjBECotizacion.IdCiudad;
            objBEAsegurado.FechaNacimiento = pObjBECotizacion.FecNacimiento;
            objBEAsegurado.IdParentesco = 2;
            objBEAsegurado.Activo = true;
            lstBEAsegurado.Add(objBEAsegurado);            
            return lstBEAsegurado;
        }

        public Decimal ObtenerPrimaTotal(BECotizacion pObjBECotizacion)
        {
            /*---------------------------------------------------------------------------
            * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
            * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Decimal nPrimaTot = 0;

            BLCotizacion objBlCotizacion = new BLCotizacion();
            pObjBECotizacion = objBlCotizacion.Obtener(Convert.ToInt32(this.hfIdPeriodo.Value), Convert.ToDecimal(this.hfNroCotizacion.Value));

            if (this.ddlPlan.SelectedValue == ValorConstante.PlanAnual)
            {
                if (pObjBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.DESCUENTO) || pObjBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.RECARGO))
                {
                    //nPrimaTot = Decimal.Round(objBECotizacion.CalcPrimaAnualC, 2);
                    nPrimaTot = Decimal.Round(pObjBECotizacion.PrimaAnualC, 2);
                }
                else
                {
                    //nPrimaTot = Decimal.Round(objBECotizacion.PrimaAnual, 2);
                    nPrimaTot = Decimal.Round(pObjBECotizacion.PrimaAnual, 2);
                }
            }

            if (this.ddlPlan.SelectedValue == ValorConstante.PlanBianual)
            {
                if (pObjBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.DESCUENTO) || pObjBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.RECARGO))
                {
                    nPrimaTot = Decimal.Round(pObjBECotizacion.PrimaBianualC, 2);
                }
                else
                {
                    nPrimaTot = Decimal.Round(pObjBECotizacion.PrimaBianual, 2);
                }
            }

            return nPrimaTot;
        }

        public Decimal ObtenerPrimaCobrar(BECotizacion pObjBECotizacion)
        {
            /*---------------------------------------------------------------------------
            * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
            * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Decimal nPrimaTot = 0;

            //Si selecciono pago unico, caso contratio pago mensual.
            if (Convert.ToInt32(this.ddlFrecuencia.SelectedValue) == Convert.ToInt32(ValorConstante.TipoFrecuencia.PAGO_UNICO))
            {
                nPrimaTot = this.ObtenerPrimaTotal(pObjBECotizacion);
            }
            else
            {
                Decimal nPrimaTea = 0;

                //Si plan es anual, la prima a cobrar es la prima total / 12,
                //caso plan es bianual, la prima a cobrar es la total / 24.
                if (this.ddlPlan.SelectedValue == ValorConstante.PlanAnual)
                {
                    nPrimaTot = Decimal.Round(this.ObtenerPrimaTotal(pObjBECotizacion) / 12, 2);
                    //nPrimaTea = nPrimaTot * (nTea / 100);
                    //nPrimaTot = nPrimaTot + nPrimaTea;
                }
                else
                {
                    nPrimaTot = Decimal.Round(this.ObtenerPrimaTotal(pObjBECotizacion) / 24, 2);
                    //nPrimaTea = nPrimaTot * (nTea / 100);
                    //nPrimaTot = nPrimaTot + nPrimaTea;
                }
            }
            return nPrimaTot;
        }

        public DateTime ObtenerInicioVigencia(DateTime pdFecFinVigPoliza)
        {
            if (pdFecFinVigPoliza < DateTime.Now)
            {
                return DateTime.Now.AddDays(1);
            }
            else
            {
                return pdFecFinVigPoliza.AddDays(1);
            }
        }

        public DateTime ObtenerFinVigencia(DateTime pdFecIniVigencia)
        {
            /*---------------------------------------------------------------------------
            * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
            * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            DateTime dFinVigencia = pdFecIniVigencia;

            if (this.ddlPlan.SelectedValue == ValorConstante.PlanAnual)
            {
                dFinVigencia = dFinVigencia.AddYears(1);
                //dFinVigencia = dFinVigencia.AddDays(-1);
            }
            else
            {
                dFinVigencia = dFinVigencia.AddYears(2);
                //dFinVigencia = dFinVigencia.AddDays(-1);
            }

            return dFinVigencia;
        }

        public Boolean ValidarCliente()
        {
            Boolean bResult = true;

            if (this.ddlTipDocumento.SelectedValue == "L" && this.txtNroDocumento.Text.Trim().Length != 8) 
            {
                this.MostrarMensaje(this.Controls, "El nro. de documento ingresado es incorrecto, debe de ser de 8 dígitos.", false);
                return false;
            }

            DateTime dFecNacimiento = NullTypes.FechaNull;            
 
            if(DateTime.TryParse(this.txtFecNacimiento.Text.Trim(),out dFecNacimiento) != true )
            {
                this.MostrarMensaje(this.Controls, "La fecha de nacimiento tiene formato incorrecto.", false);
                return false;
            }

            Int32 nAnioNac = Convert.ToDateTime(this.txtFecNacimiento.Text.Trim()).Year;
            Int32 nAnioAct = DateTime.Now.Year;  
            Int32 Edad = nAnioAct - nAnioNac;

            if(nAnioNac >  nAnioAct)
            {
                this.MostrarMensaje(this.Controls, "La fecha de nacimiento es incorrecta.", false);
                return false;
            }            

            if (this.txtNroAsientos.Text.Trim() == "0") 
            {
                this.MostrarMensaje(this.Controls, "Nro de asientos es incorrecto.", false);                
                return false;
            }            
                          
            return bResult;
        }
        #endregion       

        protected void btnFinalizar_Click(object sender, EventArgs e)
        {
            this.pnCertificado.Visible = false;
            this.pnCondiciones.Visible = true;
        }
    }
}
