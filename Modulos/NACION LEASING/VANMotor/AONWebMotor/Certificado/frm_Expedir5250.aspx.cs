﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessLogic;
using AONAffinity.Library.Resources;
using AONAffinity.Library.BusinessEntity.BDMotor;  

namespace AONWebMotor.Certificado
{
    public partial class frm_Expedir5250 : PageBase
    {
        #region Variables Globales
        private static List<BEParametro> lstBEParametro_Max = null;
        private static List<BEParametro> lstBEParametro_Min = null;  
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) 
            {

            }
        }

        #region Eventos Formulario
        protected void ddlSigno_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            if (ddlSigno.SelectedValue == "-1")
            {
                this.ddlPorcentaje.Items.Clear();
            }
            else if (ddlSigno.SelectedValue == "+")
            {
                this.CargarDropDownList(this.ddlPorcentaje, "ValorNumeroDec", "ValorNumeroDec", lstBEParametro_Max, false);
            }
            else
            {
                this.ddlPorcentaje.Items.Clear();
                this.CargarDropDownList(this.ddlPorcentaje, "ValorNumeroDec", "ValorNumeroDec", lstBEParametro_Min, false);
            }
        }

        protected void btnCambiarValVeh_Click(object sender, EventArgs e)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            try
            {
                BECotizacion objBECotizacion = new BECotizacion();

                objBECotizacion.IdPeriodo = Convert.ToInt32(this.hfIdPeriodo.Value);
                objBECotizacion.NroCotizacion = Convert.ToInt32(this.hfNroCotizacion.Value);
                objBECotizacion.CamPorcentaje = Convert.ToDecimal(this.ddlSigno.SelectedValue + this.ddlPorcentaje.SelectedValue);
                objBECotizacion.UsuarioModificacion = Session[NombreSession.Usuario].ToString();

                BLCotizacion objBLCotizacion = new BLCotizacion();

                if (objBLCotizacion.ActualizarValVeh(objBECotizacion) > 0)
                {
                    //this.rvCotizacion.LocalReport.Refresh();
                    objBECotizacion = objBLCotizacion.Obtener(Convert.ToInt32(this.hfIdPeriodo.Value), Convert.ToDecimal(this.hfNroCotizacion.Value));
                    this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Success, "Se modificó el valor del vehículo.", ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
                }
                else
                {
                    throw new Exception("Error al cambiar el valor del vehículo.");
                }
            }
            catch (Exception ex)
            {
                this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
            }
        }
        #endregion

        #region Métodos Privados

        #endregion
    }
}
