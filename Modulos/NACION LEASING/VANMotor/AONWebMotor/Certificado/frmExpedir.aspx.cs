﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessLogic;
using AONAffinity.Motor.BusinessEntity;
using AONAffinity.Motor.BusinessEntity.Bais;

namespace AONWebMotor.Certificado
{
    public partial class frmExpedir : PageBase
    {
        #region Variables
        private static BEUsuario objBEUsuario = new BEUsuario();
        private static List<BEParametro> lstBEParametro_Max = null;
        private static List<BEParametro> lstBEParametro_Min = null;
        private List<BEDepartamento> lstBEDepartamento = new List<BEDepartamento>();
        private static List<BECiudad> lstBECiudad = new List<BECiudad>();       
        private static List<BEProductoMedioPago> lstBEProductoMedioPago = new List<BEProductoMedioPago>();
        private static BECategoria objBECategoria = new BECategoria();
        private static BECotizacion objBECotizacion = new BECotizacion();
        private static String cIdInformador;
        private static Int32 nIdOficina = NullTypes.IntegerNull;
        private static String cIdMoneda = "USD";
        private Decimal nTea = 0;
        private static String cEmail = String.Empty; 
        //private static Decimal nNroCertificado = NullTypes.DecimalNull;
        private static String cTituloMail = String.Empty;
        private static String cMensajeMail = String.Empty;  
        private static String cNombreInformador = String.Empty;
        private enum EstadoForm 
        {
            NUEVO = 1,
            EXPEDIDO = 2,
            RECHAZADO = 3
        }
        #endregion      

        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            try
            {
                if (!Page.IsPostBack)
                {
                    if (!String.IsNullOrEmpty(Request.QueryString["idPer"]) && !String.IsNullOrEmpty(Request.QueryString["nroCot"]) && !String.IsNullOrEmpty(Request.QueryString["idProd"])) 
                    {
                        this.hfIdPeriodo.Value = Request.QueryString["idPer"];
                        this.hfNroCotizacion.Value = Request.QueryString["nroCot"];
                        this.hfIdProducto.Value = Request.QueryString["idProd"];

                        this.CargarInformador();                    
                        //Si existe cotización a atender, cargar funciones del formulario.
                        if (this.CargarCotizacion())
                        {                        
                            this.CargarFuncionesJS();
                            this.CargarValoresMail();
                            this.CargarPorcentajeCambio();
                            //this.CargarTEA();
                            this.CargarInfoCliente();
                            this.CargarMotivoRechazo();
                            this.CargarPlan();
                            this.CargarFrecuencia();
                            this.CargarMonedaPrima();
                            this.CargarMedioPago();
                            this.CargarMonedaCuenta();
                            this.CargarDepartamento();
                            this.CargarCiudad();
                            this.CargarMail();
                            this.CargarCategoria();
                        }
                        else
                        {
                            this.BloquearFormulario(false);
                            this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Info, "No existe cotización disponible.", ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);                        
                        }
                    }     
                }
                else
                {
                    BLCotizacion objBLCotizacion = new BLCotizacion();
                    objBECotizacion = objBLCotizacion.Obtener(Convert.ToInt32(this.hfIdPeriodo.Value), Convert.ToDecimal(this.hfNroCotizacion.Value));
                    this.CargarInfoCliente(objBECotizacion);
                }       
            }
            catch (Exception ex)
            {
                this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
            }
        }

        protected void ddlSigno_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            if (ddlSigno.SelectedValue == "-1") 
            {
                this.ddlPorcentaje.Items.Clear();
            }
            else if (ddlSigno.SelectedValue == "+")
            {                
                this.CargarDropDownList(this.ddlPorcentaje, "ValorNumeroDec", "ValorNumeroDec", lstBEParametro_Max, false);
            }
            else
            {
                this.ddlPorcentaje.Items.Clear();                          
                this.CargarDropDownList(this.ddlPorcentaje, "ValorNumeroDec", "ValorNumeroDec", lstBEParametro_Min, false);
            }
        }

        protected void btnCambiarValVeh_Click(object sender, EventArgs e)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            try
            {
                BECotizacion objBECotizacion = new BECotizacion();

                objBECotizacion.IdPeriodo = Convert.ToInt32(this.hfIdPeriodo.Value);
                objBECotizacion.NroCotizacion = Convert.ToInt32(this.hfNroCotizacion.Value);   
                objBECotizacion.CamPorcentaje = Convert.ToDecimal(this.ddlSigno.SelectedValue + this.ddlPorcentaje.SelectedValue);
                objBECotizacion.UsuarioModificacion = Session[NombreSession.Usuario].ToString();

                BLCotizacion objBLCotizacion = new BLCotizacion();

                if (objBLCotizacion.ActualizarValVeh(objBECotizacion) > 0)
                {
                    this.rvCotizacion.LocalReport.Refresh();
                    objBECotizacion = null;
                    objBECotizacion = objBLCotizacion.Obtener(Convert.ToInt32(this.hfIdPeriodo.Value), Convert.ToDecimal(this.hfNroCotizacion.Value));
                    this.CargarInfoCliente(objBECotizacion);
                    //this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Success, "Se modificó el valor del vehículo.", ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
                    this.MostrarMensaje(this.Controls, "Se modificó el valor del vehículo.", false);   
                }
                else
                {
                    throw new Exception("Error al cambiar el valor del vehículo.");
                }
            }
            catch (Exception ex)
            {
                this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
            }
        }

        protected void btnValorOri_Click(object sender, EventArgs e)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            try
            {
                BECotizacion objBECotizacion = new BECotizacion();

                objBECotizacion.IdPeriodo =  Convert.ToInt32(this.hfIdPeriodo.Value);
                objBECotizacion.NroCotizacion = Convert.ToInt32(this.hfNroCotizacion.Value);
                objBECotizacion.UsuarioModificacion = Session[NombreSession.Usuario].ToString();

                BLCotizacion objBLCotizacion = new BLCotizacion();

                if (objBLCotizacion.RevertirValVeh(objBECotizacion) > 0)
                {
                    this.rvCotizacion.LocalReport.Refresh();                    
                    this.MostrarMensaje(this.Controls, "Se revirtió el valor del vehículo", false);  
                }
                else
                {
                    throw new Exception("Error al revertrir el valor del vehiculo.");
                }
            }
            catch (Exception ex)
            {
                this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
                this.MostrarMensaje(this.Controls, "Se revirtió el valor del vehículo", false);  
            }
        }
        #endregion

        #region Popup Opcion
        //Tab rechazo
        protected void btnRegCotRechazo_Click(object sender, EventArgs e)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            try
            {                
                BECotizacion objBECotizacion = new BECotizacion();
                objBECotizacion.IdPeriodo = Convert.ToInt32(this.hfIdPeriodo.Value);
                objBECotizacion.NroCotizacion = Convert.ToDecimal(this.hfNroCotizacion.Value);
                objBECotizacion.IdTipRechazo = Convert.ToInt32(this.ddlTipMotRechazo.SelectedValue);
                objBECotizacion.ObsTipRechazo = this.txtMotRechazo.Text.Trim().ToUpper();
                objBECotizacion.UsrRegRechazo = Session[NombreSession.Usuario].ToString();

                BLCotizacion objBLCotizacion = new BLCotizacion();

                if (objBLCotizacion.ActualizarRechazo(objBECotizacion) > 0)
                {
                    this.BloquearFormulario(false);
                    this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Success, "Se registró el rechazo de la cotización.", ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);               
                }
                else 
                {
                    this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Success, "Error al registrar el rechazo de la cotización.", ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
                }
            }
            catch (Exception ex)
            {
                this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
            }
        }

        //Tab Agenda
        //protected void btnRegAgenda_Click(object sender, EventArgs e)
        //{
        //    try
        //    {                
        //        DateTime dAgenda = Convert.ToDateTime(this.txtFechaAge.Text.Trim() + " " + this.txtHoraAge.Text.Trim());
        //        BEAgendaCotizacion objBEAgendaCotizacion = new BEAgendaCotizacion();
        //        objBEAgendaCotizacion.IdPeriodo = Convert.ToInt32(this.hfIdPeriodo.Value);
        //        objBEAgendaCotizacion.NroCotizacion = Convert.ToInt32(this.hfNroCotizacion.Value);
        //        objBEAgendaCotizacion.FechaAgenda = dAgenda; //Convert.ToDateTime(this.txtFechaAge.Text.Trim());
        //        objBEAgendaCotizacion.CodInformador = Session[NombreSession.Usuario].ToString();
        //        objBEAgendaCotizacion.Observaciones = this.txtObsAgenda.Text.Trim();
        //        objBEAgendaCotizacion.UsuarioCreacion = Session[NombreSession.Usuario].ToString();

        //        BLAgendaCotizacion objBLAgendaCotizacion = new BLAgendaCotizacion();

        //        if (objBLAgendaCotizacion.Insertar(objBEAgendaCotizacion) > 0)
        //        {
        //            this.BloquearFormulario(false);
        //            this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Success, "La cotización fué agendada.", ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
        //        }
        //        else 
        //        {
        //            this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, "Error al agendar la cotización.", ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
        //        }
                
        //    }
        //    catch (Exception ex) 
        //    {
        //        this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Success, ex.Message, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
        //    } 
        //}
        #endregion

        #region Popup Cotizacion Mail
        protected void btnEnvCotMail_Click(object sender, EventArgs e)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            try
            {
                this.lblResCotMail.Text = String.Empty;

                BLGeneraArchivo objBLGeneraArchivo = new BLGeneraArchivo();               
                String cArchivo = objBLGeneraArchivo.CotizacionPDF(objBECotizacion, objBEUsuario);
                                
                if (cArchivo != null)
                {
                    BLMail objBLMail = new BLMail();
                    objBLMail.Enviar(this.txtEmail.Text.Trim(), this.txtEmailCc.Text.Trim(), cTituloMail, cMensajeMail, cArchivo);                                        
                    this.lblResCotMail.Text = "- En breve se envirá la cotización al correo inidcado.";
                }
            }
            catch (Exception ex)
            {
                this.lblResCotMail.Text = ex.Message.ToString();
            }
        }
        #endregion

        #region Popup Acepta
        #region Tab 1
        protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {                
                AONAffinity.Motor.BusinessLogic.Bais.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.Bais.BLCiudad();
                List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad = (List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad>)(ViewState[ValorConstante.ListaCiudad]);
                List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad_Prov = objBLCiudad.ListarProvincia(Convert.ToInt32(this.ddlDepartamento.SelectedValue), lstBECiudad);
                this.CargarDropDownList(this.ddlProvincia, "idProvincia", "nombre", lstBECiudad_Prov, true);
            }
            catch (Exception ex)
            {
                this.msgCliente.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
            }
        }

        protected void ddlProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                AONAffinity.Motor.BusinessLogic.Bais.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.Bais.BLCiudad();
                List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad = (List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad>)(ViewState[ValorConstante.ListaCiudad]);
                List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad_Dist = objBLCiudad.ListarDistrito(this.ddlProvincia.SelectedValue, lstBECiudad);
                this.CargarDropDownList(this.ddlDistrito, "idCiudad", "nombre", lstBECiudad_Dist, true);
            }
            catch (Exception ex)
            {
                this.msgCliente.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
            }
        }

        protected void btnActuCliente_Click(object sender, EventArgs e)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            try
            {
                this.ActualizarCliente();
            }
            catch (Exception ex)
            {
                this.msgCliente.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
            }
        }
        #endregion

        #region Tab 2
        protected void btnActVehiculo_Click(object sender, EventArgs e)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            try
            {
                this.ActualizarVehiculo();
            }
            catch (Exception ex)
            {
                this.msgVehiculo.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
            }
        }
        #endregion

        #region Tab 3
        protected void ddlDepEntrega_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                AONAffinity.Motor.BusinessLogic.Bais.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.Bais.BLCiudad();
                List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad = (List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad>)(ViewState[ValorConstante.ListaCiudad]);
                List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad_Prov = objBLCiudad.ListarProvincia(Convert.ToInt32(this.ddlDepEntrega.SelectedValue), lstBECiudad);
                this.CargarDropDownList(this.ddlProvEntrega, "idProvincia", "nombre", lstBECiudad_Prov, true);                
            }
            catch (Exception ex)
            {
                this.msgCliente.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
            }
        }

        protected void ddlProvEntrega_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                AONAffinity.Motor.BusinessLogic.Bais.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.Bais.BLCiudad();
                List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad = (List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad>)(ViewState[ValorConstante.ListaCiudad]);
                List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad_Dist = objBLCiudad.ListarDistrito(this.ddlProvEntrega.SelectedValue, lstBECiudad);
                this.CargarDropDownList(this.ddlDistEntrega, "idCiudad", "nombre", lstBECiudad_Dist, true);                
            }
            catch (Exception ex)
            {
                this.msgCliente.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
            }
        }

        protected void ddlMedPago_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*---------------------------------------------------------------------------
            * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
            * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
           ---------------------------------------------------------------------------*/

            try
            {
                this.CargarMonedaCuenta();
            }
            catch (Exception ex)
            {
                this.msgCertificado.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
            }
        }

        protected void ddlPlan_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            this.ddlFrecuencia_SelectedIndexChanged(sender, e);
        }

        protected void ddlFrecuencia_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            try
            {
                this.MostrarPrima();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void btnExpedir_Click(object sender, EventArgs e)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            try
            {
                this.Expedir();
            }
            catch (Exception ex)
            {
                this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
            }
        }
        #endregion                           
        #endregion

        


        #region Métodos Privados
        private void MostrarPrima()
        {
            Decimal nPrima = 0;

            if (this.ddlFrecuencia.SelectedValue != "-1")
            {
                BLCotizacion objBLCotizacion = new BLCotizacion();
                objBECotizacion = objBLCotizacion.Obtener(Convert.ToInt32(this.hfIdPeriodo.Value), Convert.ToDecimal(this.hfNroCotizacion.Value));

                //Si es plan anual
                if (this.ddlPlan.SelectedValue == ValorConstante.PlanAnual)
                {
                    //Obtener prima con recargo o descuento
                    if (objBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.DESCUENTO) || objBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.RECARGO))
                    {
                        nPrima = objBECotizacion.PrimaAnualC;
                    }
                    else //Obtener prima sin recargo o descuento                         
                    {
                        nPrima = objBECotizacion.PrimaAnual;
                    }

                    //Obtener la prima segun la frecuencia
                    if (this.ddlFrecuencia.SelectedValue == Convert.ToInt32(ValorConstante.TipoFrecuencia.PAGO_MENSUAL).ToString())
                    {
                        this.txtPrimTotal.Text = (nPrima == NullTypes.DecimalNull ? String.Empty : Decimal.Round(nPrima / 12, 2).ToString());
                    }
                    else
                    {
                        this.txtPrimTotal.Text = (nPrima == NullTypes.DecimalNull ? String.Empty : Decimal.Round(nPrima, 2).ToString());
                    }
                }

                //Si es plan bianual
                if (this.ddlPlan.SelectedValue == ValorConstante.PlanBianual)
                {
                    //Obtener prima con recargo o descuento
                    if (objBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.DESCUENTO) || objBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.RECARGO))
                    {
                        nPrima = objBECotizacion.PrimaBianualC;
                    }
                    else //Obtener prima sin recargo o descuento
                    {
                        nPrima = objBECotizacion.PrimaBianual;
                    }

                    //Obtener la prima segun la frecuencia
                    if (this.ddlFrecuencia.SelectedValue == Convert.ToInt32(ValorConstante.TipoFrecuencia.PAGO_MENSUAL).ToString())
                    {
                        this.txtPrimTotal.Text = (nPrima == NullTypes.DecimalNull ? String.Empty : Decimal.Round((nPrima / 24), 2).ToString());
                    }
                    else
                    {
                        this.txtPrimTotal.Text = (nPrima == NullTypes.DecimalNull ? String.Empty : Decimal.Round((nPrima), 2).ToString());
                    }
                }
            }
        }
        private Boolean ValidarAgenda() 
        {
            Boolean bResult = true;

            
            if (Convert.ToDateTime(this.txtFechaAge.Text.Trim()) < DateTime.Now) 
            {
                this.MostrarMensaje(this.Controls, "La fecha debe ser mayor a la actual", false);
                return false;
            }

            return bResult;
        }        

        private void BloquearFormulario(Boolean pbEstado) 
        {
            this.ddlSigno.Enabled = pbEstado;
            this.ddlPorcentaje.Enabled = pbEstado;
            this.btnCambiarValVeh.Enabled = pbEstado;
            this.btnValorOri.Enabled = pbEstado;
            this.rvCotizacion.Enabled = pbEstado;
            this.btnAceptar.Enabled = pbEstado;
            this.btnImprimir.Enabled = pbEstado;  
            this.btnCotRechazo.Enabled = pbEstado;
            this.btnCotMail.Enabled = pbEstado;            
        }

        /// <summary>
        /// Permite cargar la información del informador.
        /// </summary>
        private void CargarInformador()
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            this.CargarInfoUsuario();
            this.CargarInformadorOficina();
        }

        /// <summary>
        /// Permite cargar la información del usuario.
        /// </summary>
        private void CargarInfoUsuario()
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            AONAffinity.Motor.BusinessLogic.Bais.BLUsuario objBLUsuario = new AONAffinity.Motor.BusinessLogic.Bais.BLUsuario();
            objBEUsuario = objBLUsuario.Obtener(Session[NombreSession.Usuario].ToString());

            if (objBEUsuario == null) 
            {
                throw new Exception("No existe información del informador, contactar con administrador.");
            }
        }

        private void CargarMail() 
        {
            BLParametro objBLParametro = new BLParametro();
            BEParametro objBEParametro = objBLParametro.Obtener(36);

            if (objBEParametro != null) 
            {                
                cEmail = objBEParametro.ValorCadena;
            }            
        }

        private void CargarCategoria() 
        {
            if (objBECotizacion != null) 
            {
                BLCategoria objBLCategoria = new BLCategoria();
                objBECategoria = objBLCategoria.Obtener(objBECotizacion.IdCategoria);
            }            
        }

        /// <summary>
        /// Permite obtner el nro de certificado diponible;
        /// </summary>
        //private void ObtenerNroCertificadoDisponible() 
        //{
        //    if (nIdOficina == NullTypes.IntegerNull) 
        //    {
        //        throw new Exception("No existe oficina asignada al informador, contactar con administrador.");
        //    }

        //    AONAffinity.Motor.BusinessLogic.Bais.BLPapDisponible objBLPapDisponible = new AONAffinity.Motor.BusinessLogic.Bais.BLPapDisponible();
        //    nNroCertificado = objBLPapDisponible.ObtenerDisponible(nIdOficina, this.hfIdProducto.Value);

        //    if (nNroCertificado == NullTypes.DecimalNull) 
        //    {
        //        throw new Exception("No existe nro. certificado disponible, contactar con administrador.");
        //    }
        //    this.hfIdCertificado.Value = this.hfIdProducto.Value.ToString() + "-" + Decimal.Round(nNroCertificado, 0).ToString();
        //}

        /// <summary>
        /// Permite cargar la información de la oficina.
        /// </summary>
        private void CargarInformadorOficina()
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            if (objBEUsuario.IdInformador == NullTypes.CadenaNull)
            {
                throw new Exception("El usuario no tiene asignado un código de informador.");
            }

            AONAffinity.Motor.BusinessLogic.Bais.BLInformador objBLInformador = new AONAffinity.Motor.BusinessLogic.Bais.BLInformador();
            AONAffinity.Motor.BusinessEntity.Bais.BEInformador objBEInformador = objBLInformador.Obtener(objBEUsuario.IdInformador);

            if (objBEInformador == null)
            {
                throw new Exception("El usuario no tiene asignado un código de informador.");
            }

            if (objBEInformador.IdOficina == NullTypes.IntegerNull)
            {
                throw new Exception("No existe una oficina asignada al informador.");
            }

            cIdInformador = objBEInformador.IdInformador;
            nIdOficina = objBEInformador.IdOficina;
            cNombreInformador = objBEInformador.Nombre;  
        }

        /// <summary>
        /// Permite cargar los porcentajes de cambio del valor del vehículo.
        /// </summary>
        private void CargarPorcentajeCambio()
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            BLParametro objBLParametro = new BLParametro();
            lstBEParametro_Max = objBLParametro.ObtenerPorcMaxCambio();
            lstBEParametro_Min = objBLParametro.ObtenerPorcMinCambio();        
        }

        /// <summary>
        /// Permite cargar los parametros del Speech de Venta.
        /// </summary>
        private void CargarValoresSpeech()
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            BLParametro objBLParametro = new BLParametro();
            BEParametro objBEParametro;

            //Saludo
            if (this.hfIdProducto.Value == "5250")
            {
                objBLParametro = new BLParametro();
                objBEParametro = objBLParametro.Obtener(20);
                
                if (objBEParametro != null)
                {
                        String[] cArrSaludo = new String[3];
                        cArrSaludo[0] = objBECotizacion.PriNombre + " " + objBECotizacion.SegNombre + " " + objBECotizacion.ApePaterno + " " + objBECotizacion.ApeMaterno;
                        cArrSaludo[1] = objBECotizacion.FecIniVigPoliza.ToShortDateString();
                        cArrSaludo[2] = objBECotizacion.FecFinVigPoliza.ToShortDateString();
                        this.hfSaludo.Value = Funciones.RemplazarValores(objBEParametro.ValorCadena, cArrSaludo);
                }
            }
            else 
            {
                objBLParametro = new BLParametro();
                objBEParametro = objBLParametro.Obtener(37);

                if (objBEParametro != null)
                {
                        String[] cArrSaludo = new String[3];
                        cArrSaludo[0] = objBECotizacion.PriNombre + " " + objBECotizacion.SegNombre + " " + objBECotizacion.ApePaterno + " " + objBECotizacion.ApeMaterno;
                        cArrSaludo[1] = objBECotizacion.FecIniVigPoliza.ToShortDateString();
                        cArrSaludo[2] = objBECotizacion.FecFinVigPoliza.ToShortDateString();
                        this.hfSaludo.Value = Funciones.RemplazarValores(objBEParametro.ValorCadena, cArrSaludo);
                }
            }
            
            //Motivo de llamada.
            if (this.hfIdProducto.Value == "5250")
            {
                if (objBECotizacion.FecFinVigPoliza > DateTime.Now)
                {
                    objBEParametro = objBLParametro.Obtener(38);
                    String[] cArrCond = new String[1];
                    cArrCond[0] = objBECotizacion.FecFinVigPoliza.Date.ToShortDateString();
                    this.hfCond.Value = Funciones.RemplazarValores(objBEParametro.ValorCadena, cArrCond);  
                }
                else 
                {
                    objBEParametro = objBLParametro.Obtener(39);
                    this.hfCond.Value = objBEParametro.ValorCadena;
                }
            }
            else 
            {                    
                if (objBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.DESCUENTO))
                {
                    objBEParametro = objBLParametro.Obtener(21);

                    if (objBEParametro != null)
                    {
                        this.hfCond.Value = objBEParametro.ValorCadena;
                    }
                }
                else
                {
                    objBEParametro = objBLParametro.Obtener(22);
                    if (objBEParametro != null)
                    {
                        this.hfCond.Value = objBEParametro.ValorCadena;
                    }
                }
            }
                                
            //Speech conformidad
            objBEParametro = objBLParametro.Obtener(23);

            if (objBEParametro != null)
            {
                DateTime dFecVigencia = DateTime.Now;

                if (objBECotizacion.FecFinVigPoliza > DateTime.Now) 
                {
                    dFecVigencia = objBECotizacion.FecFinVigPoliza;
                }                

                String[] cArrConf = new String[4];
                cArrConf[0] = objBECotizacion.ApePaterno;
                cArrConf[1] = dFecVigencia.Day.ToString();
                cArrConf[2] = this.ObtenerNombreMes(dFecVigencia.Month);
                cArrConf[3] = dFecVigencia.Year.ToString(); 
                this.hfConf.Value = Funciones.RemplazarValores(objBEParametro.ValorCadena, cArrConf);
            }

            //Speech Datos Cliente
            //objBEParametro = objBLParametro.Obtener(24);

            //if(objBEParametro != null)
            //{
            //    String[] cArrCli = new String[1];
            //    cArrCli[0] = objBECotizacion.ApePaterno;
            //    this.lblSpecchCli.Text = Funciones.RemplazarValores(objBEParametro.ValorCadena, cArrCli);
            //}

            //Speech Datos del Vehículo
            //objBEParametro = objBLParametro.Obtener(25);
                
            //if (objBEParametro != null)
            //{
            //    this.lblSpecchVeh.Text = objBEParametro.ValorCadena;
            //}
                
            //Speech Medio de Pago
            //objBEParametro = objBLParametro.Obtener(26);

            //if (objBEParametro != null)
            //{                   
            //    this.lblSpecchMedPag.Text = objBEParametro.ValorCadena;
            //}
                
            //Speech Rechazo
            //objBEParametro = objBLParametro.Obtener(27);

            //if (objBEParametro != null) 
            //{
            //    String[] cArrRechazo = new String[1];
            //    cArrRechazo[0] = objBECotizacion.ApePaterno;
            //    this.lblSpeechRec.Text = Funciones.RemplazarValores(objBEParametro.ValorCadena, cArrRechazo); 
            //}

            //Speech Agendamiento.                
            //objBEParametro = objBLParametro.Obtener(28);

            //if (objBEParametro != null) 
            //{
            //    String[] cArrAgenda = new String[1];
            //    cArrAgenda[0] = objBECotizacion.ApePaterno;
            //    this.lblSpeechAge.Text = Funciones.RemplazarValores(objBEParametro.ValorCadena, cArrAgenda);
            //}

            //Speech Beneficios.
            //objBEParametro = objBLParametro.Obtener(29);

            //if (objBEParametro != null) 
            //{
            //    String[] cArrBen = new String[1];
            //    cArrBen[0] = objBECotizacion.ApePaterno;
            //    this.lblSpeechBen.Text = Funciones.RemplazarValores(objBEParametro.ValorCadena, cArrBen);  
            //}

            //Speech Cobertturas y Exclusiones.
            objBEParametro = objBLParametro.Obtener(30);
                
            if (objBEParametro != null) 
            {
                this.lblSpeechCob.Text = objBEParametro.ValorCadena; 
            }

            //Speech Coordinación de Envio de Póliza y Pago                
            objBEParametro = objBLParametro.Obtener(31);

            if (objBEParametro != null)
            {
                String [] cArrCoor = new String[1];
                cArrCoor[0] = objBECotizacion.ApePaterno;  
                this.lblSpeechCoor.Text = Funciones.RemplazarValores(objBEParametro.ValorCadena, cArrCoor); 
            }

            //Speech Despedida
            objBEParametro = objBLParametro.Obtener(32);

            if (objBEParametro != null) 
            {
                String[] cArrDesp = new String[1];
                cArrDesp[0] = objBECotizacion.ApePaterno;
                this.lblSpeechDesp.Text = Funciones.RemplazarValores(objBEParametro.ValorCadena, cArrDesp);
            }

            //Speech Mail.
            objBEParametro = objBLParametro.Obtener(33);

            if (objBEParametro != null) 
            {
                String[] cArrMail = new String[1];
                cArrMail[0] = objBECotizacion.ApePaterno;
                this.lblSpeechMail.Text = Funciones.RemplazarValores(objBEParametro.ValorCadena, cArrMail);
            }

            //Speech Valor Vehiculo
            objBEParametro = objBLParametro.Obtener(34);
            if (objBEParametro != null) 
            {
                String[] cArrValVeh = new String[3];
                cArrValVeh[0] = objBECotizacion.ApePaterno;
                cArrValVeh[1] = Decimal.Round(objBLParametro.Obtener(11).ValorNumeroDec, 0).ToString();
                cArrValVeh[2] = Decimal.Round(objBLParametro.Obtener(12).ValorNumeroDec, 0).ToString();
                this.hfValVehiculo.Value = Funciones.RemplazarValores(objBEParametro.ValorCadena, cArrValVeh);    
            }                
        }

        private String ObtenerNombreMes(Int32 pnMes) 
        {
            String cMes = String.Empty;

            if (pnMes == 1) { cMes = "ENERO"; }
            else if (pnMes == 2) { cMes = "FEBRERO"; }
            else if (pnMes == 3) { cMes = "MARZO"; }
            else if (pnMes == 4) { cMes = "ABRIL"; }
            else if (pnMes == 5) { cMes = "MAYO"; }
            else if (pnMes == 6) { cMes = "JUNIO"; }
            else if (pnMes == 7) { cMes = "JULIO"; }
            else if (pnMes == 8) { cMes = "AGOSTO"; }
            else if (pnMes == 9) { cMes = "SEPTIEMBRE"; }
            else if (pnMes == 10) { cMes = "OCTUBRE"; }
            else if (pnMes == 11) { cMes = "NOVIEMBRE"; }
            else { cMes = "DICIEMBRE"; }

            return cMes;
        }

        /// <summary>
        /// Permite cargar los parametros del mail de cotización.
        /// </summary>
        private void CargarValoresMail() 
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            if(objBECotizacion != null )
            {
                BEParametro objBEParametro = new BEParametro();
                BLParametro objBLParametro = new BLParametro();

                Int32 nCodParTitMail = 13;
                objBEParametro = objBLParametro.Obtener(nCodParTitMail);

                if (objBEParametro != null) 
                {
                    cTituloMail = objBEParametro.ValorCadena;
                }

                Int32 nCodParMsgMail = 14;
                objBEParametro = objBLParametro.Obtener(nCodParMsgMail);

                if (objBEParametro != null) 
                {                
                    String [] cArrMsg = new String[2];
                    cArrMsg[0] = objBECotizacion.PriNombre + " " + objBECotizacion.ApePaterno;
                    cArrMsg[1] = cNombreInformador;   
                    cMensajeMail = Funciones.RemplazarValores(objBEParametro.ValorCadena, cArrMsg);                    
                }
            }                        
        }

        private void CargarDepartamento() 
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLDepartamento objBLDepartamento = new AONAffinity.Motor.BusinessLogic.Bais.BLDepartamento();
            this.lstBEDepartamento = objBLDepartamento.Listar(51);
            this.CargarDropDownList(this.ddlDepartamento, "IdDepartamento", "Nombre", this.lstBEDepartamento, true);
            this.CargarDropDownList(this.ddlDepEntrega, "IdDepartamento", "Nombre", this.lstBEDepartamento, true);
        }

        private void CargarCiudad() 
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.Bais.BLCiudad();
            lstBECiudad = objBLCiudad.Listar(51);
            ViewState[ValorConstante.ListaCiudad] = objBLCiudad.Listar(51);  
        }

        /// <summary>
        /// Permitre cargar Funciones JavaScript en los controles.
        /// </summary>
        private void CargarFuncionesJS()
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            this.btnAceptar.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro realizar la venta del seguro?')== false) return false;");
            //Formulario principal
            this.btnCambiarValVeh.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de cambiar el valor del vehículo?')== false) return false;");
            this.btnValorOri.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de revertir el valor del vehículo?')== false) return false;");

            //Popup Mail Cotizacion
            this.btnEnvCotMail.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de enviar la cotización por email?')== false) return false;");

            //Popup Opcion
            this.btnRegCotRechazo.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de registrar el rechazo de la cotización?')== false) return false;");
            //this.btnRegAgenda.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de agendar la cotización?')== false) return false;");

            //Popup Certificado Cotización
            this.btnActuCliente.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de actualizar los datos del cliente?')== false) return false;");
            this.btnActVehiculo.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de actualizar los datos del vehículo?')== false) return false;");
            this.btnExpedir.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de expedir el certificado?')== false) return false;");

            
        }

        /// <summary>
        /// Permite cargar Información del cliente.
        /// </summary>
        private void CargarInfoCliente()
        {
            /*---------------------------------------------------------------------------
            * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
            * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            if (objBECotizacion != null)
            {
                //Popup Acepta
                this.txtNombre1.Text = objBECotizacion.PriNombre;
                this.txtNombre2.Text = objBECotizacion.SegNombre;
                this.txtApellidoPat.Text = objBECotizacion.ApePaterno;
                this.txtApellidoMat.Text = objBECotizacion.ApeMaterno;
                this.txtNroDocumento.Text = objBECotizacion.NroDocumento;
                this.txtDireccion.Text = objBECotizacion.Direccion;
                this.txtTelDomicio1.Text = objBECotizacion.TelDomicilio1;
                this.txtTelDomicio2.Text = objBECotizacion.TelDomicilio2;
                this.txtTelDomicio3.Text = objBECotizacion.TelDomicilio3;
                this.txtTelMovil1.Text = objBECotizacion.TelMovil1;
                this.txtTelMovil2.Text = objBECotizacion.TelMovil2;
                this.txtTelMovil3.Text = objBECotizacion.TelMovil3;
                this.txtTelOficina1.Text = objBECotizacion.TelOficina1;
                this.txtTelOficina2.Text = objBECotizacion.TelOficina2;
                this.txtTelOficina3.Text = objBECotizacion.TelOficina3;
                //this.txtEmail1.Text = objBECotizacion.Email1;
                //this.txtEmail2.Text = objBECotizacion.Email2;
                if (objBECotizacion.ReqInspeccion) { this.pnInspeccion.Visible = true; } else { this.pnInspeccion.Visible = false; }

                this.txtMarca.Text = objBECotizacion.DesMarca;
                this.txtModelo.Text = objBECotizacion.DesModelo;
                this.txtNroMotor.Text = objBECotizacion.NroMotor;
                this.txtNroPlaca.Text = objBECotizacion.NroPlaca;
                this.txtColor.Text = objBECotizacion.Color;
                this.txtAnioFab.Text = objBECotizacion.AnioFab.ToString();
                this.txtTipVehiculo.Text = objBECotizacion.DesTipoVehiculo;
                this.txtClaseVehiculo.Text = objBECotizacion.DesClase;
                this.txtNroChasis.Text = objBECotizacion.NroSerie;  
                if (objBECotizacion.NroAsientos == NullTypes.IntegerNull) { this.txtNroAsientos.Text = String.Empty; } else { this.txtNroAsientos.Text = objBECotizacion.NroAsientos.ToString(); }

                //Popup Email
                //this.txtEmail.Text = objBECotizacion.Email1;
                //this.txtEmailCc.Text = objBECotizacion.Email2;
            }
        }


        private void CargarInfoCliente(BECotizacion pBECotizacion)
        {
            /*---------------------------------------------------------------------------
            * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
            * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            if (objBECotizacion != null)
            {
                //Popup Acepta
                this.txtNombre1.Text = pBECotizacion.PriNombre;
                this.txtNombre2.Text = pBECotizacion.SegNombre;
                this.txtApellidoPat.Text = pBECotizacion.ApePaterno;
                this.txtApellidoMat.Text = pBECotizacion.ApeMaterno;
                this.txtNroDocumento.Text = pBECotizacion.NroDocumento;
                this.txtDireccion.Text = pBECotizacion.Direccion;
                this.txtTelDomicio1.Text = pBECotizacion.TelDomicilio1;
                this.txtTelDomicio2.Text = pBECotizacion.TelDomicilio2;
                this.txtTelDomicio3.Text = pBECotizacion.TelDomicilio3;
                this.txtTelMovil1.Text = pBECotizacion.TelMovil1;
                this.txtTelMovil2.Text = pBECotizacion.TelMovil2;
                this.txtTelMovil3.Text = pBECotizacion.TelMovil3;
                this.txtTelOficina1.Text = pBECotizacion.TelOficina1;
                this.txtTelOficina2.Text = pBECotizacion.TelOficina2;
                this.txtTelOficina3.Text = pBECotizacion.TelOficina3;

                //if (pBECotizacion.Email1 != String.Empty) { this.txtEmail1.Text = pBECotizacion.Email1; }
                //if (pBECotizacion.Email2 != String.Empty) { this.txtEmail2.Text = pBECotizacion.Email2; }
                                
                if (objBECotizacion.ReqInspeccion) { this.pnInspeccion.Visible = true; } else { this.pnInspeccion.Visible = false; }

                this.txtMarca.Text = pBECotizacion.DesMarca;
                this.txtModelo.Text = pBECotizacion.DesModelo;
                this.txtNroMotor.Text = pBECotizacion.NroMotor;
                this.txtNroPlaca.Text = pBECotizacion.NroPlaca;
                this.txtColor.Text = pBECotizacion.Color;
                this.txtAnioFab.Text = pBECotizacion.AnioFab.ToString();
                this.txtTipVehiculo.Text = pBECotizacion.DesTipoVehiculo;
                this.txtClaseVehiculo.Text = pBECotizacion.DesClase;
                this.txtNroChasis.Text = pBECotizacion.NroSerie;
                if (pBECotizacion.NroAsientos == NullTypes.IntegerNull) { this.txtNroAsientos.Text = String.Empty; } else { this.txtNroAsientos.Text = pBECotizacion.NroAsientos.ToString(); }

                //Popup Email
                //this.txtEmail.Text = pBECotizacion.Email1;
                //this.txtEmailCc.Text = pBECotizacion.Email2;
            }
        }

        /// <summary>
        /// Permite cargar el motivo de rechazo.
        /// </summary>
        private void CargarMotivoRechazo()
        {
            /*---------------------------------------------------------------------------
            * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
            * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            BLTipoRechazoCotizacion objBLTipoRechazoCotizacion = new BLTipoRechazoCotizacion();
            this.CargarDropDownList(this.ddlTipMotRechazo, "IdTipRechazo", "Descripcion", objBLTipoRechazoCotizacion.Listar(true), true);
        }

        /// <summary>
        /// Permite cargar la información de los planes del producto.
        /// </summary>
        private void CargarPlan()
        {
            /*---------------------------------------------------------------------------
            * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
            * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            AONAffinity.Motor.BusinessLogic.Bais.BLOpcion objBLOpcion = new AONAffinity.Motor.BusinessLogic.Bais.BLOpcion();
            List<AONAffinity.Motor.BusinessEntity.Bais.BEOpcion> lstBEOpcion = objBLOpcion.Listar(Convert.ToInt32(this.hfIdProducto.Value));

            if (lstBEOpcion == null) 
            {
                throw new Exception("No existe planes configurados para el producto.");
            }

            if (lstBEOpcion.Count > 1)
            {
                this.CargarDropDownList(this.ddlPlan, "OPCION", "OPCION", objBLOpcion.Listar(Convert.ToInt32(this.hfIdProducto.Value)), true);
            }
            else 
            {
                this.CargarDropDownList(this.ddlPlan, "OPCION", "OPCION", objBLOpcion.Listar(Convert.ToInt32(this.hfIdProducto.Value)), false);
            }            

            //Si cotización solo tiene tasa anual, solo se muestra plan anual.
            if (objBECotizacion.TasaAnual != NullTypes.DecimalNull && objBECotizacion.TasaBianual == NullTypes.DecimalNull)
            {
                this.ddlPlan.SelectedValue = ValorConstante.PlanAnual;
                this.ddlPlan.Enabled = false;
            }

            //Si cotización solo tiene tasa bianual, solo se muestra plan bianual.
            if (objBECotizacion.TasaAnual == NullTypes.DecimalNull && objBECotizacion.TasaBianual != NullTypes.DecimalNull)
            {
                this.ddlPlan.SelectedValue = ValorConstante.PlanBianual;
                this.ddlPlan.Enabled = false;
            }
        }

        /// <summary>
        /// Permite cargar la onformación de la frecuencia de pago.
        /// </summary>
        private void CargarFrecuencia()
        {
            /*---------------------------------------------------------------------------
            * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
            * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            AONAffinity.Motor.BusinessLogic.Bais.BLFrecuencia objBLFrecuencia = new AONAffinity.Motor.BusinessLogic.Bais.BLFrecuencia();
            List<AONAffinity.Motor.BusinessEntity.Bais.BEFrecuencia> lstBEFrecuencia = objBLFrecuencia.Listar(Convert.ToInt32(this.hfIdProducto.Value));

            if (lstBEFrecuencia == null) 
            {
                throw new Exception("No existe frecuencia configurada para el producto.");
            }

            if (lstBEFrecuencia.Count > 1)
            {
                this.CargarDropDownList(this.ddlFrecuencia, "IdFrecuencia", "Nombre", lstBEFrecuencia, true);
            }
            else 
            {
                this.CargarDropDownList(this.ddlFrecuencia, "IdFrecuencia", "Nombre", lstBEFrecuencia, true);
            }            
        }

        /// <summary>
        /// Permite cargar la información de la moneda de la prima.
        /// </summary>
        private void CargarMonedaPrima()
        {
            /*---------------------------------------------------------------------------
            * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
            * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            AONAffinity.Motor.BusinessLogic.Bais.BLMoneda objBLMoneda = new AONAffinity.Motor.BusinessLogic.Bais.BLMoneda();
            List<AONAffinity.Motor.BusinessEntity.Bais.BEMoneda> lstBEMoneda = objBLMoneda.Listar(Convert.ToInt32(this.hfIdProducto.Value));

            if (lstBEMoneda!= null) 
            {
                if (lstBEMoneda.Count > 1)
                {
                    this.CargarDropDownList(this.ddlMonPrima, "IdMoneda", "nombre", lstBEMoneda, true);
                }
                else
                {
                    this.CargarDropDownList(this.ddlMonPrima, "IdMoneda", "nombre", lstBEMoneda, false);
                }
            }            
        }

        /// <summary>
        /// Permite obtener el medio de pago.
        /// </summary>
        private void CargarMedioPago() 
        {
            /*---------------------------------------------------------------------------
            * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
            * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            AONAffinity.Motor.BusinessLogic.Bais.BLProductoMedioPago objBLProductoMedioPago = new AONAffinity.Motor.BusinessLogic.Bais.BLProductoMedioPago();
            lstBEProductoMedioPago = objBLProductoMedioPago.Listar(Convert.ToInt32(this.hfIdProducto.Value));

            this.CargarDropDownList(this.ddlMedPago, "IdMedioPago", "DesMedioPago", lstBEProductoMedioPago, true);      
        }

        /// <summary>
        /// Permite cargar la moneda de la cuenta.
        /// </summary>
        private void CargarMonedaCuenta() 
        {
           /*---------------------------------------------------------------------------
           * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
           * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
           ---------------------------------------------------------------------------*/

            if (lstBEProductoMedioPago != null)
            {
                this.ddlMonCuenta.Items.Clear();

                List<AONAffinity.Motor.BusinessEntity.Bais.BEProductoMedioPago> lstBEProductoMedioPago_MonedaCta = new List<AONAffinity.Motor.BusinessEntity.Bais.BEProductoMedioPago>();

                foreach (AONAffinity.Motor.BusinessEntity.Bais.BEProductoMedioPago objBEProductoMedioPago in lstBEProductoMedioPago) 
                {
                    if (objBEProductoMedioPago.IdMedioPago == this.ddlMedPago.SelectedValue) 
                    {
                        lstBEProductoMedioPago_MonedaCta.Add(objBEProductoMedioPago);                        
                    }
                }

                if (lstBEProductoMedioPago_MonedaCta.Count > 1)
                {
                    this.CargarDropDownList(this.ddlMonCuenta, "IdMonedaCobro", "DesMonedacobro", lstBEProductoMedioPago_MonedaCta, true);
                }
                else 
                {
                    this.CargarDropDownList(this.ddlMonCuenta, "IdMonedaCobro", "DesMonedacobro", lstBEProductoMedioPago_MonedaCta, false);
                }
            }
            else 
            {
                this.ddlMonCuenta.Items.Clear();
                this.ddlMedPago.Items.Add(new ListItem("Seleccione", "-1"));    
            }
        }

        private void MostrarAgenda(Int32 pnTipo) 
        {
            if (pnTipo == Convert.ToInt32(ValorConstante.TipoAtencion.AGENDA)) 
            {

            }
        }
        #endregion

        #region Funciones Privadas
        private Boolean ValidarPrimaMinima() 
        {
            //Si Existe registro de categoria.
            if(objBECategoria != null)
            {
                //Si Si registro esta activo.
                if(objBECategoria.EstadoRegistro  == true )
                {
                    Decimal nPrimaCobrar = this.ObtenerPrimaCobrar();

                    if (nPrimaCobrar < objBECategoria.MontoPrima) 
                    {
                        return false;
                    }
                }
            }
                        
            return true;
        }

        /// <summary>
        /// Permite validar la información de los campos opcionales.
        /// </summary>
        private Boolean ValidaExpedirOpcional()
        {
            /*---------------------------------------------------------------------------
            * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
            * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Boolean bResult = true;

            String[] cArrMensaje = new String[3];
            Int32 nIndex = 0;

            if (this.ddlMedPago.SelectedValue == "-1")
            {
                cArrMensaje[nIndex] = "- Seleccione el medio de pago.";
                nIndex++;
            }
            if (this.txtNroCuenta.Text == String.Empty)
            {
                cArrMensaje[nIndex] = "- Ingrese el nro de cuenta.";
                nIndex++;
            }
            if (this.ddlMonCuenta.SelectedValue == "-1")
            {
                cArrMensaje[nIndex] = "- Seleccione la moneda de la cuenta.";
            }

            if (nIndex > 0)
            {
                this.MostrarMensaje(this.upActVehiculo, cArrMensaje);
                return false;
            }

            return bResult;
        }

        /// <summary>
        /// Permite cargar un cotización
        /// </summary>
        /// <returns>True Existe cotizacion | False no existe cotizacion</returns>
        private Boolean CargarCotizacion()
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Boolean bResult = false;

            BLCotizacion objBLCotizacion = new BLCotizacion();
            //BLAtencionCotizacion objBLAtencionCotizacion = new BLAtencionCotizacion();
            //BEAtencionCotizacion objBEAtencionCotizacion = objBLAtencionCotizacion.Generar(Session[NombreSession.Usuario].ToString());

            //if (objBEAtencionCotizacion == null) 
            //{
            //    return bResult;
            //}

            //this.MostrarAgenda();                       

            //this.hfIdPeriodo.Value = objBEAtencionCotizacion.IdPeriodo.ToString();
            //this.hfNroCotizacion.Value = objBEAtencionCotizacion.NroCotizacion.ToString();  
            objBECotizacion = objBLCotizacion.Obtener(Convert.ToInt32(this.hfIdPeriodo.Value), Convert.ToDecimal(this.hfNroCotizacion.Value));
          
            if (objBECotizacion == null) 
            {
                this.hfNroCotizacion.Value = "0"; 
                return bResult;
            }
            
            this.CargarValoresSpeech();
            this.rvCotizacion.LocalReport.Refresh();
            bResult = true;
            return bResult;            
        }        

        /// <summary>
        /// Permite obtener la prima a cobrar.
        /// </summary>
        /// <returns>Monto de la prima a cobrar.</returns>
        public Decimal ObtenerPrimaCobrar()
        {
            /*---------------------------------------------------------------------------
            * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
            * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Decimal nPrimaTot = 0;

            //Si selecciono pago unico, caso contratio pago mensual.
            if (Convert.ToInt32(this.ddlFrecuencia.SelectedValue) == Convert.ToInt32(ValorConstante.TipoFrecuencia.PAGO_UNICO))
            {
                nPrimaTot = this.ObtenerPrimaTotal();
            }
            else
            {                
                Decimal nPrimaTea = 0; 
                
                //Si plan es anual, la prima a cobrar es la prima total / 12,
                //caso plan es bianual, la prima a cobrar es la total / 24.
                if (this.ddlPlan.SelectedValue == ValorConstante.PlanAnual)
                {
                    nPrimaTot = this.ObtenerPrimaTotal() / 12;
                    //nPrimaTea = nPrimaTot * (nTea / 100);
                    //nPrimaTot = nPrimaTot + nPrimaTea;
                }
                else
                {
                    nPrimaTot = this.ObtenerPrimaTotal() / 24;
                    //nPrimaTea = nPrimaTot * (nTea / 100);
                    //nPrimaTot = nPrimaTot + nPrimaTea;
                }
            }
            return nPrimaTot;
        }

        /// <summary>
        /// Permite obtener el find de vigencia.
        /// </summary>
        /// <returns>Fecha de fin de vuigencia.</returns>
        public DateTime ObtenerFinVigencia(DateTime pdFecIniVigencia)
        {
            /*---------------------------------------------------------------------------
            * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
            * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            DateTime dFinVigencia = pdFecIniVigencia;

            if (this.ddlPlan.SelectedValue == ValorConstante.PlanAnual)
            {
                dFinVigencia = dFinVigencia.AddYears(1);
                dFinVigencia = dFinVigencia.AddDays(-1);
            }
            else
            {
                dFinVigencia = dFinVigencia.AddYears(2);
                dFinVigencia = dFinVigencia.AddDays(-1);
            }

            return dFinVigencia;
        }

        /// <summary>
        /// Permite cargar la información de un certificado.
        /// </summary>
        /// <returns>Objeto de tipo BECertificado.</returns>
        private AONAffinity.Motor.BusinessEntity.Bais.BECertificado CargarCertificado()
        {
            /*---------------------------------------------------------------------------
            * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
            * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            AONAffinity.Motor.BusinessEntity.Bais.BECertificado objBECertificado = new AONAffinity.Motor.BusinessEntity.Bais.BECertificado();

            if (objBECotizacion != null)
            {
                objBECertificado.IdProducto = Convert.ToInt32(this.hfIdProducto.Value);               
                objBECertificado.Opcion = this.ddlPlan.SelectedValue;
                objBECertificado.IdFrecuencia = Convert.ToInt32(this.ddlFrecuencia.SelectedValue);
                objBECertificado.IdOficina = nIdOficina;
                objBECertificado.IdInformador = cIdInformador;
                objBECertificado.IdMedioPago = (this.cbxSolCuenta.Checked ? this.ddlMedPago.SelectedValue : "NA");
                objBECertificado.IdEstadoCertificado = 1;
                objBECertificado.MontoAsegurado = objBECotizacion.ValVehiculo;
                objBECertificado.PrimaBruta = this.ObtenerPrimaTotal();
                objBECertificado.Iva = 0;
                objBECertificado.PrimaTotal = this.ObtenerPrimaTotal();
                objBECertificado.PrimaCobrar = this.ObtenerPrimaCobrar();
                objBECertificado.NumeroCuenta = (this.cbxSolCuenta.Checked ? this.txtNroCuenta.Text.Trim() : "000000000000");
                objBECertificado.Vencimiento = (this.cbxSolCuenta.Checked ? Convert.ToDateTime(this.txtFecVencimiento.Text.Trim()) : NullTypes.FechaNull);
                objBECertificado.Vigencia = this.ObtenerInicioVigencia(objBECotizacion.FecFinVigPoliza);
                objBECertificado.Puntos = NullTypes.IntegerNull;
                objBECertificado.IdMotivoAnulacion = NullTypes.IntegerNull;
                objBECertificado.Consistente = false;
                objBECertificado.PuntosAnual = NullTypes.IntegerNull;
                objBECertificado.IdCiudad = objBECotizacion.IdCiudad;
                objBECertificado.Direccion = objBECotizacion.Direccion;
                objBECertificado.Telefono = objBECotizacion.TelDomicilio1;
                objBECertificado.Nombre1 = objBECotizacion.PriNombre;
                objBECertificado.Nombre2 = objBECotizacion.SegNombre;
                objBECertificado.Apellido1 = objBECotizacion.ApePaterno;
                objBECertificado.Apellido2 = objBECotizacion.ApeMaterno;
                objBECertificado.CcCliente = objBECotizacion.NroDocumento;
                objBECertificado.IdTipoDocumento = objBECotizacion.IdTipoDocumento;
                objBECertificado.Digitacion = DateTime.Now;
                objBECertificado.Incentivo = NullTypes.DecimalNull;
                objBECertificado.SaldoIncentivo = NullTypes.DecimalNull;
                objBECertificado.UsuarioCreacion = Session[NombreSession.Usuario].ToString();
                objBECertificado.Venta = DateTime.Now.Date;
                objBECertificado.FinVigencia = this.ObtenerFinVigencia(objBECertificado.Vigencia);
                objBECertificado.IdMonedaPrima = this.ddlMonPrima.SelectedValue;
                objBECertificado.IdMonedaCobro = cIdMoneda;
                objBECertificado.AfiliadoHasta = NullTypes.FechaNull;
            }

            return objBECertificado;
        }

        public DateTime ObtenerInicioVigencia(DateTime pdFecFinVigPoliza) 
        {
            if (pdFecFinVigPoliza < DateTime.Now)
            {
                return DateTime.Now.AddDays(1);
            }
            else 
            {
                return pdFecFinVigPoliza.AddDays(1);
            }
        } 

        /// <summary>
        /// Permite cargar la información de los infoProductosC.
        /// </summary>
        /// <returns>Lista de infoproductos.</returns>
        private List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC> CargarInfoProductoC() 
        {
            /*---------------------------------------------------------------------------
            * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
            * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/
            List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC> lstBEInfoProductoC = new List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC>();

            if (this.hfIdProducto.Value == "5250") 
            {
                lstBEInfoProductoC = this.CargarInfoProducto5250();
            }
            if (this.hfIdProducto.Value == "5251") 
            {
                lstBEInfoProductoC = this.CargarInfoProducto5251();  
            }

            return lstBEInfoProductoC;
        }

        private List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC> CargarInfoProducto5250() 
        {
            List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC> lstBEInfoProductoC = new List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC>();
            AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC objBEInfoProductoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC();

            //Sexo
            objBEInfoProductoC.IdInfoProducto = 1004;
            objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
            objBEInfoProductoC.ValorString = objBECotizacion.Sexo;
            lstBEInfoProductoC.Add(objBEInfoProductoC);

            //Estado Civil
            objBEInfoProductoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC();
            objBEInfoProductoC.IdInfoProducto = 1005;
            objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
            objBEInfoProductoC.ValorString = NullTypes.CadenaNull;
            lstBEInfoProductoC.Add(objBEInfoProductoC);

            //Fecha de Nacimiento
            objBEInfoProductoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC();
            objBEInfoProductoC.IdInfoProducto = 1006;
            objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoProductoC.ValorDate = objBECotizacion.FecNacimiento;
            objBEInfoProductoC.ValorString = NullTypes.CadenaNull;
            lstBEInfoProductoC.Add(objBEInfoProductoC);

            //Dirección de Entrega
            objBEInfoProductoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC();
            objBEInfoProductoC.IdInfoProducto = 1019;
            objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
            objBEInfoProductoC.ValorString = this.txtDirEntrega.Text.Trim().ToUpper();
            lstBEInfoProductoC.Add(objBEInfoProductoC);

            //Ubigeo de Entrega
            objBEInfoProductoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC();
            objBEInfoProductoC.IdInfoProducto = 1020;
            objBEInfoProductoC.ValorNum = Convert.ToDecimal(this.ddlDistEntrega.SelectedValue);
            objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
            objBEInfoProductoC.ValorString = NullTypes.CadenaNull;
            lstBEInfoProductoC.Add(objBEInfoProductoC);

            //Código Sponsor            
            objBEInfoProductoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC();
            objBEInfoProductoC.IdInfoProducto = 1052;
            objBEInfoProductoC.ValorNum = objBECotizacion.CodSponsor;
            objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
            objBEInfoProductoC.ValorString = NullTypes.CadenaNull;
            lstBEInfoProductoC.Add(objBEInfoProductoC);

            //Periodo Cotización
            objBEInfoProductoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC();
            objBEInfoProductoC.IdInfoProducto = 1054;
            objBEInfoProductoC.ValorNum = objBECotizacion.IdPeriodo;
            objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
            objBEInfoProductoC.ValorString = NullTypes.CadenaNull;
            lstBEInfoProductoC.Add(objBEInfoProductoC);

            //Nro Cotización
            objBEInfoProductoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC();
            objBEInfoProductoC.IdInfoProducto = 1055;
            objBEInfoProductoC.ValorNum = objBECotizacion.NroCotizacion;
            objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
            objBEInfoProductoC.ValorString = NullTypes.CadenaNull;
            lstBEInfoProductoC.Add(objBEInfoProductoC);

            return lstBEInfoProductoC;
        }

        private List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC> CargarInfoProducto5251() 
        {
            List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC> lstBEInfoProductoC = new List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC>();
            AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC objBEInfoProductoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC();

            //Sexo
            objBEInfoProductoC.IdInfoProducto = 1078; //1060;
            objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
            objBEInfoProductoC.ValorString = objBECotizacion.Sexo;
            lstBEInfoProductoC.Add(objBEInfoProductoC);

            //Estado Civil
            objBEInfoProductoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC();
            objBEInfoProductoC.IdInfoProducto = 1079; //1061;
            objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
            objBEInfoProductoC.ValorString = "S";//NullTypes.CadenaNull;
            lstBEInfoProductoC.Add(objBEInfoProductoC);

            //Fecha de Nacimiento
            objBEInfoProductoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC();
            objBEInfoProductoC.IdInfoProducto = 1080; //1061;
            objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoProductoC.ValorDate = objBECotizacion.FecNacimiento;
            objBEInfoProductoC.ValorString = NullTypes.CadenaNull;
            lstBEInfoProductoC.Add(objBEInfoProductoC);

            //Dirección de Entrega
            objBEInfoProductoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC();
            objBEInfoProductoC.IdInfoProducto = 1081; //1063;
            objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
            objBEInfoProductoC.ValorString = this.txtDirEntrega.Text.Trim().ToUpper();
            lstBEInfoProductoC.Add(objBEInfoProductoC);

            //Ubigeo de Entrega
            objBEInfoProductoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC();
            objBEInfoProductoC.IdInfoProducto = 1082; //1064;
            objBEInfoProductoC.ValorNum = Convert.ToDecimal(this.ddlDistEntrega.SelectedValue);
            objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
            objBEInfoProductoC.ValorString = NullTypes.CadenaNull;
            lstBEInfoProductoC.Add(objBEInfoProductoC);

            //Código Sponsor            
            objBEInfoProductoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC();
            objBEInfoProductoC.IdInfoProducto = 1083; //1065;
            objBEInfoProductoC.ValorNum = objBECotizacion.CodSponsor;
            objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
            objBEInfoProductoC.ValorString = NullTypes.CadenaNull;
            lstBEInfoProductoC.Add(objBEInfoProductoC);

            //Periodo Cotización
            objBEInfoProductoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC();
            objBEInfoProductoC.IdInfoProducto = 1084; //1066;
            objBEInfoProductoC.ValorNum = objBECotizacion.IdPeriodo;
            objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
            objBEInfoProductoC.ValorString = NullTypes.CadenaNull;
            lstBEInfoProductoC.Add(objBEInfoProductoC);

            //Nro Cotización
            objBEInfoProductoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC();
            objBEInfoProductoC.IdInfoProducto = 1085; //1067;
            objBEInfoProductoC.ValorNum = objBECotizacion.NroCotizacion;
            objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
            objBEInfoProductoC.ValorString = NullTypes.CadenaNull;
            lstBEInfoProductoC.Add(objBEInfoProductoC);

            return lstBEInfoProductoC;
        }

        /// <summary>
        /// Permite cargar la información del Asegurado.
        /// </summary>
        /// <returns>Objeto de tipo BEAsegurado.</returns>
        private List<AONAffinity.Motor.BusinessEntity.Bais.BEAsegurado> CargarAsegurado()
        {
            List<AONAffinity.Motor.BusinessEntity.Bais.BEAsegurado> lstBEAsegurado = new List<AONAffinity.Motor.BusinessEntity.Bais.BEAsegurado>();

            if (objBECotizacion != null)
            {
                AONAffinity.Motor.BusinessEntity.Bais.BEAsegurado objBEAsegurado = new AONAffinity.Motor.BusinessEntity.Bais.BEAsegurado();
                
                objBEAsegurado.Consecutivo = 1;
                objBEAsegurado.IdTipodocumento = objBECotizacion.IdTipoDocumento;
                objBEAsegurado.Ccaseg = objBECotizacion.NroDocumento;
                objBEAsegurado.Nombre1 = objBECotizacion.PriNombre;
                objBEAsegurado.Nombre2 = objBECotizacion.SegNombre;
                objBEAsegurado.Apellido1 = objBECotizacion.ApePaterno;
                objBEAsegurado.Apellido2 = objBECotizacion.ApeMaterno;
                objBEAsegurado.Direccion = objBECotizacion.Direccion;
                objBEAsegurado.Telefono = objBECotizacion.TelDomicilio1;
                objBEAsegurado.IdCiudad = objBECotizacion.IdCiudad;
                objBEAsegurado.FechaNacimiento = objBECotizacion.FecNacimiento;
                objBEAsegurado.IdParentesco = 2;
                objBEAsegurado.Activo = true;

                lstBEAsegurado.Add(objBEAsegurado);
            }

            return lstBEAsegurado;
        }

        /// <summary>
        /// Permite cargar la información de los infoAseguradosC.
        /// </summary>
        /// <returns></returns>
        private List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC> CargarInfoAseguradoC() 
        {
            List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC> lstBEInfoAseguradoC = new List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC>();

            if (this.hfIdProducto.Value == "5250") 
            {
                lstBEInfoAseguradoC = this.CargarInfoAsegurado5250();
            }
            if (this.hfIdProducto.Value == "5251") 
            {
                lstBEInfoAseguradoC = this.CargarInfoAsegurado5251();
            }

            return lstBEInfoAseguradoC;
        }

        private List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC> CargarInfoAsegurado5250() 
        {
            List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC> lstBEInfoAseguradoC = new List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC>();
            AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();

            //Referencia
            objBEInfoAseguradoC.IdInfoAsegurado = 305;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Marca
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 306;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = objBECotizacion.IdMarca;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = objBECotizacion.DesMarca;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Modelo
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 307;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = objBECotizacion.IdModelo;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = objBECotizacion.DesModelo;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Color
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 308;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = objBECotizacion.Color;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Año fabricación
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 309;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = objBECotizacion.AnioFab;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Numero de Chasis
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 310;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = objBECotizacion.NroSerie;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Numero de Motor
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 311;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = objBECotizacion.NroMotor;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Numero de Matricula/Placa
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 312;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = objBECotizacion.NroPlaca;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Numero de asientos
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 313;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = objBECotizacion.NroAsientos;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Categoría Vehículo
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 314;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = objBECotizacion.IdCategoria;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Indicador Lima/Provincia
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 315;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            if (objBECotizacion.IdCiudad.ToString().Substring(1, 4) == "5115") { objBEInfoAseguradoC.ValorString = "L"; } else { objBEInfoAseguradoC.ValorString = "P"; }
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Indicador de Dscto. Por Flota
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 316;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            if (objBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.DESCUENTO)) { objBEInfoAseguradoC.ValorString = "D"; }
            else if (objBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.RECARGO)) { objBEInfoAseguradoC.ValorString = "R"; }
            else { objBEInfoAseguradoC.ValorString = "N"; }
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Nro.Vehiculos por Flota
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 317;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Clase de Vehículos
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 318;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = objBECotizacion.IdClase;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = objBECotizacion.DesClase;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Uso de Vehículos
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 319;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = objBECotizacion.IdUsoVehiculo;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = objBECotizacion.DesUsoVehiculo;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Tipo de Vehiculo
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 320;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = objBECotizacion.IdTipoVehiculo; 
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Numero Teléfono 2
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 321;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = objBECotizacion.TelDomicilio2;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Numero Teléfono 3
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 322;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = objBECotizacion.TelDomicilio3;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Correo electrónico
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 323;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = objBECotizacion.Email1;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //% DSCTO/RCGO
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 336;
            objBEInfoAseguradoC.Consecutivo = 1;
            if (objBECotizacion.PorcentajeC == NullTypes.DecimalNull) { objBEInfoAseguradoC.ValorNum = 0; } else { objBEInfoAseguradoC.ValorNum = Decimal.Round(objBECotizacion.PorcentajeC, 2); }
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
            
            return lstBEInfoAseguradoC; 
        }

        private List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC> CargarInfoAsegurado5251() 
        {
            List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC> lstBEInfoAseguradoC = new List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC>();
            AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();

            //Referencia
            objBEInfoAseguradoC.IdInfoAsegurado = 346; //334;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Marca
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 347; //335;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = objBECotizacion.IdMarca;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = objBECotizacion.DesMarca;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Modelo
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 347; //336;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = objBECotizacion.IdModelo;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = objBECotizacion.DesModelo;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Color
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 349; //337;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = objBECotizacion.Color;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Año fabricación
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 350; //338;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = objBECotizacion.AnioFab;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Numero de Chasis
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 351; // 339;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = objBECotizacion.NroSerie;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Numero de Motor
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 352; // 340;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = objBECotizacion.NroMotor;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Numero de Matricula/Placa
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 353; //341;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = objBECotizacion.NroPlaca;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);              

            //Categoría Vehículo
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 355; // 342;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = objBECotizacion.IdCategoria;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Numero de asientos
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 354; // 343;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = objBECotizacion.NroAsientos;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);      
               
            //Indicador Lima/Provincia
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 356; // 344;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            if (objBECotizacion.IdCiudad.ToString().Substring(1, 4) == "5115") { objBEInfoAseguradoC.ValorString = "L"; } else { objBEInfoAseguradoC.ValorString = "P"; }
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Indicador de Dscto. Por Flota
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 357; //345;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            if (objBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.DESCUENTO)) { objBEInfoAseguradoC.ValorString = "D"; }
            else if (objBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.RECARGO)) { objBEInfoAseguradoC.ValorString = "R"; }
            else { objBEInfoAseguradoC.ValorString = "N"; }
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Nro.Vehiculos por Flota
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 358; //346;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Clase de Vehículos
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 359; // 347;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = objBECotizacion.IdClase;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = objBECotizacion.DesClase;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Uso de Vehículos
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 360; // 348;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = objBECotizacion.IdUsoVehiculo;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = objBECotizacion.DesUsoVehiculo;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Tipo de Vehiculo
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 361; // 349;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = objBECotizacion.IdTipoVehiculo;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Numero Teléfono 2
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 362;// 350;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = objBECotizacion.TelDomicilio2;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Numero Teléfono 3
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 363; //351;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = objBECotizacion.TelDomicilio3;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //Correo electrónico
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 364; // 352;
            objBEInfoAseguradoC.Consecutivo = 1;
            objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = objBECotizacion.Email1;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);

            //% DSCTO/RCGO
            objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();
            objBEInfoAseguradoC.IdInfoAsegurado = 365; // 353;
            objBEInfoAseguradoC.Consecutivo = 1;
            if (objBECotizacion.PorcentajeC == NullTypes.DecimalNull) { objBEInfoAseguradoC.ValorNum = 0; } else { objBEInfoAseguradoC.ValorNum = Decimal.Round(objBECotizacion.PorcentajeC, 2); }
            objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
            objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull;
            lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                                 
            return lstBEInfoAseguradoC; 
        }

        /// <summary>
        /// Permite obtener la prima total.
        /// </summary>
        /// <returns>Monto de la prima total.</returns>
        public Decimal ObtenerPrimaTotal()
        {
            /*---------------------------------------------------------------------------
            * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
            * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            Decimal nPrimaTot = 0;

            BLCotizacion objBlCotizacion = new BLCotizacion();
            objBECotizacion = objBlCotizacion.Obtener(Convert.ToInt32(this.hfIdPeriodo.Value), Convert.ToDecimal(this.hfNroCotizacion.Value));

            if (this.ddlPlan.SelectedValue == ValorConstante.PlanAnual)
            {
                if (objBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.DESCUENTO) || objBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.RECARGO))
                {
                    //nPrimaTot = Decimal.Round(objBECotizacion.CalcPrimaAnualC, 2);
                    nPrimaTot = Decimal.Round(objBECotizacion.PrimaAnualC, 2);
                }
                else
                {
                    //nPrimaTot = Decimal.Round(objBECotizacion.PrimaAnual, 2);
                    nPrimaTot = Decimal.Round(objBECotizacion.PrimaAnual, 2);
                }
            }

            if (this.ddlPlan.SelectedValue == ValorConstante.PlanBianual)
            {
                if (objBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.DESCUENTO) || objBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.RECARGO))
                {
                    nPrimaTot = Decimal.Round(objBECotizacion.PrimaBianualC , 2);
                }
                else
                {
                    nPrimaTot = Decimal.Round(objBECotizacion.PrimaBianualC, 2);
                }
            }

            return nPrimaTot;
        }

        private void ActualizarCliente() 
        {
            BLCotizacion objBLCotizacion = new BLCotizacion();
            BECotizacion objBECotizacion_Cli = new BECotizacion();

            objBECotizacion_Cli.IdPeriodo = Convert.ToInt32(this.hfIdPeriodo.Value);
            objBECotizacion_Cli.NroCotizacion = Convert.ToDecimal(this.hfNroCotizacion.Value);
            objBECotizacion_Cli.PriNombre = this.txtNombre1.Text.Trim().ToUpper();
            objBECotizacion_Cli.SegNombre = this.txtNombre2.Text.Trim().ToUpper();
            objBECotizacion_Cli.ApePaterno = this.txtApellidoPat.Text.Trim().ToUpper();
            objBECotizacion_Cli.ApeMaterno = this.txtApellidoMat.Text.Trim().ToUpper();
            objBECotizacion_Cli.IdTipoDocumento = this.ddlTipDocumento.SelectedValue;
            objBECotizacion_Cli.NroDocumento = this.txtNroDocumento.Text.Trim();
            objBECotizacion_Cli.Direccion = this.txtDireccion.Text.Trim().ToUpper();
            objBECotizacion_Cli.TelDomicilio1 = this.txtTelDomicio1.Text.Trim();
            objBECotizacion_Cli.TelDomicilio2 = this.txtTelDomicio2.Text.Trim();
            objBECotizacion_Cli.TelDomicilio2 = this.txtTelDomicio3.Text.Trim();
            objBECotizacion_Cli.TelMovil1 = this.txtTelMovil1.Text.Trim();
            objBECotizacion_Cli.TelMovil2 = this.txtTelMovil2.Text.Trim();
            objBECotizacion_Cli.TelMovil3 = this.txtTelMovil3.Text.Trim();
            objBECotizacion_Cli.TelOficina1 = this.txtTelOficina1.Text.Trim();
            objBECotizacion_Cli.TelOficina2 = this.txtTelOficina2.Text.Trim();
            objBECotizacion_Cli.TelOficina3 = this.txtTelOficina3.Text.Trim();
            objBECotizacion_Cli.Email1 = this.txtEmail1.Text.Trim();
            objBECotizacion_Cli.Email2 = this.txtEmail2.Text.Trim();
            objBECotizacion_Cli.UsuarioModificacion = Session[NombreSession.Usuario].ToString();

            if (objBLCotizacion.ActualizarCliente(objBECotizacion_Cli) > 0)
            {
                //this.msgCliente.Show(AONWebMotor.controles.MsgBox.MessageType.Success, "Se actualizó los datos del cliente.", ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
                this.MostrarMensaje(this.upActCliente, "Se actualizó los datos del cliente.");            
                objBECotizacion = objBLCotizacion.Obtener(Convert.ToInt32(this.hfIdPeriodo.Value), Convert.ToDecimal(this.hfNroCotizacion.Value));
                //this.hfDatoCliente.Value = "1";                
            }
        }


        private void ActualizarVehiculo() 
        {
            //if(this.hfDatoCliente.Value != "1")
            //{
            //    this.MostrarMensaje(this.upActVehiculo, "Pimero debe actualizar la información del cliente.");                
            //    return;
            //}

            BLCotizacion objBLCotizacion = new BLCotizacion();
            BECotizacion objBECotizacion_Veh = new BECotizacion();
            objBECotizacion_Veh.IdPeriodo = Convert.ToInt32(this.hfIdPeriodo.Value);
            objBECotizacion_Veh.NroCotizacion = Convert.ToDecimal(this.hfNroCotizacion.Value);
            objBECotizacion_Veh.NroPlaca = this.txtNroPlaca.Text.Trim().ToUpper();
            objBECotizacion_Veh.Color = this.txtColor.Text.Trim();
            objBECotizacion_Veh.NroAsientos = Convert.ToInt32(this.txtNroAsientos.Text.Trim());
            objBECotizacion_Veh.NroSerie = this.txtNroChasis.Text.Trim().ToUpper();   
            objBECotizacion_Veh.UsuarioModificacion = Session[NombreSession.Usuario].ToString();

            if (objBLCotizacion.ActualizarVehiculo(objBECotizacion) > 0)
            {
                //this.msgVehiculo.Show(AONWebMotor.controles.MsgBox.MessageType.Success, "Se actualizó los datos del vehículo.", ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
                this.MostrarMensaje(this.upActVehiculo, "Se actualizó los datos del vehículo.");            
                //this.hfDatoVehiculo.Value = "1";
            }            
        }

        private void Expedir() 
        {
            //if (this.hfDatoCliente.Value != "1" && this.hfDatoVehiculo.Value != "1") 
            //{
            //    this.msgVehiculo.Show(AONWebMotor.controles.MsgBox.MessageType.Success, "Primero debe actualizar la información del cliente y del vehículo.", ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
            //    return;
            //}

            if (this.txtPrimTotal.Text != "")
            {
                if (Convert.ToDecimal(this.txtPrimTotal.Text) == 0)
                {
                    throw new Exception("No se puede expedir con prima valor 0.");
                }
            }
            if (this.cbxSolCuenta.Checked)
            {
                if (!this.ValidaExpedirOpcional())
                {
                    return;
                }
            }

            if (!(this.ValidarPrimaMinima()))
            {
                throw new Exception("El monto de la prima no puede ser menor a: " + Decimal.Round(objBECategoria.MontoPrima, 2));
            }

            BLCotizacion objBLCotizacion = new BLCotizacion();
            objBECotizacion = objBLCotizacion.Obtener(Convert.ToInt32(this.hfIdPeriodo.Value), Convert.ToDecimal(this.hfNroCotizacion.Value));

            AONAffinity.Motor.BusinessEntity.Bais.BECertificado objBECertificado = new AONAffinity.Motor.BusinessEntity.Bais.BECertificado();
            objBECertificado = this.CargarCertificado();

            List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC> lstBEInfoProductoC = new List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC>();
            lstBEInfoProductoC = this.CargarInfoProductoC();

            List<AONAffinity.Motor.BusinessEntity.Bais.BEAsegurado> lstBEAsegurado = new List<AONAffinity.Motor.BusinessEntity.Bais.BEAsegurado>();
            lstBEAsegurado = this.CargarAsegurado();

            List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC> lstBEInfoAseguradoC = new List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC>();
            lstBEInfoAseguradoC = this.CargarInfoAseguradoC();

            AONAffinity.Motor.BusinessLogic.Bais.BLCertificado objBLCertificado = new AONAffinity.Motor.BusinessLogic.Bais.BLCertificado();
            List<AONAffinity.Motor.BusinessEntity.Bais.BEExpedir> lstBEExpedir = objBLCertificado.Expedir(objBECertificado, lstBEAsegurado, null, lstBEInfoProductoC, lstBEInfoAseguradoC, String.Empty);

            if (lstBEExpedir[0].Codigo == 1)
            {
                this.BloquearFormulario(false);
                this.btnImprimir.Enabled = true;
                //this.rvCertificado.LocalReport.Refresh(); O_O
                //this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Success, "El certificado fue expedido.", ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
                this.MostrarMensaje(this.Controls, "Se expidió con exito el certificado.", false);  

                BECotizacion objBECotizacion_Exp = new BECotizacion();
                objBECotizacion_Exp.IdPeriodo = Convert.ToInt32(this.hfIdPeriodo.Value);
                objBECotizacion_Exp.NroCotizacion = Convert.ToDecimal(this.hfNroCotizacion.Value);
                objBECotizacion_Exp.UsuarioModificacion = Session[NombreSession.Usuario].ToString();

                objBLCotizacion.Expedir(objBECotizacion_Exp);
                this.hfIdCertificado.Value = lstBEExpedir[0].Descripcion;
            }
            else
            {
                //this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, "Se produjo un error al expedir el certificado. <br/> " + lstBEExpedir[0].Descripcion, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
                this.MostrarMensaje(this.Controls, "Se produjo un error al expedir el certificado. <br/> " + lstBEExpedir[0].Descripcion, false);  
            }
        }

       

        #endregion        

        protected void btnPostBack_Click(object sender, EventArgs e)
        {

        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            Response.Redirect("../" + UrlPagina.Expedir5251.Substring(2) + "?idPer=" + this.hfIdPeriodo.Value + "&nroCot=" + this.hfNroCotizacion.Value + "&idProd=" + this.hfIdProducto.Value);
        }

        
    }
}
