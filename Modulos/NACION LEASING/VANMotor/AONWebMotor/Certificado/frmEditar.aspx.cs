﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Motor.BusinessLogic.Bais;
using AONAffinity.Motor.BusinessEntity.Bais;

namespace AONWebMotor.Certificado
{
    public partial class frmEditar : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    this.CargarFormulario();
                }
            }
            catch (Exception ex) 
            {

            }
        }

        #region 
        private void CargarFormulario() 
        {
            this.CargarCertificado();
        }

        private void CargarCertificado() 
        {
            BLCertificado objBLCertificado = new BLCertificado();
            BECertificado objBECertificado = objBLCertificado.Obtener(Request.QueryString["idCert"].ToString());

            this.CargarTabCertificado(objBECertificado);

        }

        private void CargarTabCertificado(BECertificado pObjBECertificado) 
        {
            if (pObjBECertificado != null)
            {
                //Datos del certificad
                this.txtNroCertificado.Text = pObjBECertificado.NumCertificado.ToString();
                this.txtIniVigencia.Text = pObjBECertificado.Vigencia.ToShortDateString();
                this.txtFinVigencia.Text = pObjBECertificado.FinVigencia.ToShortDateString();
                this.txtMontoAseg.Text = Decimal.Round(pObjBECertificado.MontoAsegurado, 2).ToString();
                this.txtFecVenta.Text = pObjBECertificado.Venta.ToShortDateString();
                this.txtFecDigitacion.Text = pObjBECertificado.Digitacion.ToShortDateString();

                //Datos del titular
                this.txtNombre1.Text = pObjBECertificado.Nombre1.Trim();
                this.txtNombre2.Text = pObjBECertificado.Nombre2.Trim();
                this.txtApellido1.Text = pObjBECertificado.Apellido1.Trim();
                this.txtApellido2.Text = pObjBECertificado.Apellido2.Trim();
                this.txtCcCliente.Text = pObjBECertificado.CcCliente.Trim();
                this.txtDireccion.Text = pObjBECertificado.Direccion.Trim();
                this.txtTelefono.Text = pObjBECertificado.Telefono.Trim();

                //Datos del plan y medio de pago
                this.txtPlan.Text = pObjBECertificado.Opcion;
                this.txtPrimaCob.Text = Decimal.Round(pObjBECertificado.PrimaCobrar, 2).ToString();
                this.txtPrimaTot.Text = Decimal.Round(pObjBECertificado.PrimaTotal, 2).ToString();
            }
        }

        #endregion
    }
}