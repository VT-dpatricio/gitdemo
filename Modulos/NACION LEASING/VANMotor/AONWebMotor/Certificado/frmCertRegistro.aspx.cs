﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Resources;  
using AONAffinity.Business.Logic;
using AONAffinity.Business.Entity.BDJanus;
using AONAffinity.Business.Entity.BDMotor;

namespace AONWebMotor.Certificado
{
    public partial class frmCertRegistro : PageBase
    {
        #region Variables
        /// <summary>
        /// Código del producto.
        /// </summary>
        private Int32 nIdProducto = 5300;

        /// <summary>
        /// Código de departamento por defecto.
        /// </summary>
        private Int32 nCodDepartamento = 5115;

        /// <summary>
        /// Código de entidad
        /// </summary>
        private Int32 nIdEntidad = 3;
        #endregion        

        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-03
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try
            {
                if (!Page.IsPostBack)
                {
                    Page.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    //this.CargarOficina();
                    this.CargarTipoDocumento();
                    this.CargarDepartamento();
                    this.CargarCiudad();
                    this.CargarProvincia();
                    this.CargarDistrito();
                    this.CargarPlan();
                    this.CargarFrecuencia();
                    this.CargarMonedaPrima();
                    this.CargarMedioPago();
                    this.CargarCabBusqOficina();

                    if (!String.IsNullOrEmpty(Request.QueryString["idCot"]))
                    {
                        this.divSeq.Visible = true;
                        this.txtNroCotizacion.ReadOnly = true;

                        BLCotizacion objBLCotizacion = new BLCotizacion();
                        BECotizacion objBECotizacion = null; // objBLCotizacion.Obtener(Convert.ToDecimal(Request.QueryString["idCot"]));

                        if (objBECotizacion != null)
                        {
                            //this.txtNroCotizacion.Text = objBECotizacion.IdCotizacion.ToString();
                            this.txtPriNombre.Text = objBECotizacion.PriNombre;
                            this.txtSegNombre.Text = objBECotizacion.SegNombre;
                            this.txtApePaterno.Text = objBECotizacion.ApePaterno;
                            this.txtApeMaterno.Text = objBECotizacion.ApeMaterno;
                            this.ddlTipDocumento.SelectedValue = objBECotizacion.IdTipoDocumento;
                            this.txtNroDocumento.Text = objBECotizacion.NroDocumento;
                            this.txtDireccion.Text = objBECotizacion.Direccion;
                        }
                    }
                    else
                    {
                        this.divSeq.Visible = false;
                        this.txtNroCotizacion.ReadOnly = false;
                        this.CargarFuncionesJS();
                        this.txtNroCotizacion.CssClass = "Form_TextBoxCod"; 
                    }

                    BLCertificado objBLCertificado = new BLCertificado();
                    this.hfNroCertificado.Value = Convert.ToString(objBLCertificado.ObtenerNroCertCorrelativo(this.nIdProducto));
                    this.txtNroCertificado.Text = hfNroCertificado.Value;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-03
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try
            {
                this.CargarCiudad();
                this.CargarProvincia();
                this.CargarDistrito();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void ddlProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-03
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try
            {
                this.CargarDistrito();
            }
            catch (Exception ex)
            {
                throw ex;
            } 
        }

        protected void btnGuaVenta_Click(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-03
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try
            {
                BECertificado objBECertificado = new BECertificado();
                objBECertificado.IdCertificado = nIdProducto + "-" + this.txtNroCertificado.Text.Trim();
                objBECertificado.IdProducto = nIdProducto;
                objBECertificado.NumCertificado = Convert.ToDecimal(this.txtNroCertificado.Text.Trim());
                objBECertificado.Opcion = this.ddlPlan.SelectedValue;
                objBECertificado.IdFrecuencia = 1;
                objBECertificado.IdOficina = Convert.ToInt32(this.txtCodOficina.Text.Trim());
                objBECertificado.IdInformador = "013816";
                objBECertificado.IdMedioPago = this.ddlMedPago.SelectedValue;
                objBECertificado.IdEstadoCertificado = 1;
                objBECertificado.MontoAsegurado = 15000;
                objBECertificado.PrimaBruta = 15;
                objBECertificado.Iva = 0;
                objBECertificado.PrimaTotal = Convert.ToDecimal(this.txtPrimTotal.Text);
                objBECertificado.PrimaCobrar = NullTypes.DecimalNull;
                objBECertificado.NumeroCuenta = this.txtNroCuenta.Text;
                objBECertificado.Vencimiento = NullTypes.FechaNull;
                objBECertificado.Vigencia = NullTypes.FechaNull;
                objBECertificado.Puntos = NullTypes.IntegerNull;
                objBECertificado.IdMotivoAnulacion = NullTypes.IntegerNull;
                objBECertificado.Consistente = false;
                objBECertificado.PuntosAnual = 0;
                objBECertificado.IdCiudad = Convert.ToInt32(this.ddlDistrito.SelectedValue);
                objBECertificado.Direccion = this.txtDireccion.Text.Trim().ToUpper();
                objBECertificado.Telefono = this.txtTelefono.Text.Trim();
                objBECertificado.Nombre1 = this.txtPriNombre.Text.Trim().ToUpper();
                objBECertificado.Nombre2 = this.txtSegNombre.Text.Trim().ToUpper();
                objBECertificado.Apellido1 = this.txtApePaterno.Text.Trim().ToUpper();
                objBECertificado.Apellido2 = this.txtApeMaterno.Text.Trim().ToUpper();
                objBECertificado.CcCliente = this.txtNroDocumento.Text.Trim();
                objBECertificado.IdTipoDocumento = this.ddlTipDocumento.SelectedValue;
                objBECertificado.Digitacion = Convert.ToDateTime(this.txtFecha.Text.Trim());
                objBECertificado.Incentivo = NullTypes.DecimalNull;
                objBECertificado.UsuarioCreacion = Session[NombreSession.Usuario].ToString();
                objBECertificado.Venta = Convert.ToDateTime(this.txtFecha.Text.Trim());
                objBECertificado.FinVigencia = NullTypes.FechaNull;
                objBECertificado.IdMonedaPrima = this.ddlMonPrima.SelectedValue;
                objBECertificado.IdMonedaCobro = this.ddlMonCuenta.SelectedValue;
                objBECertificado.AfiliadoHasta = NullTypes.FechaNull;
                objBECertificado.IdCiclo = NullTypes.IntegerNull;
                objBECertificado.DiaGenera = NullTypes.ShortNull;
                objBECertificado.MesGenera = NullTypes.ShortNull;

                BLCertificado objBLCertificado = new BLCertificado();

                if (objBLCertificado.Insertar(objBECertificado) > 0)
                {
                    this.MostrarMensaje(this.Controls, "Se registro la informacion del certificado.", false);
                    this.btnImprimir.Enabled = true;
                    this.hfIdCertificado.Value = this.nIdProducto + "-" + this.txtNroCertificado.Text.Trim();
                    this.rvCertificado.LocalReport.Refresh();                     
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-03
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            this.CargarProvincia();
            this.ddlProvincia.SelectedValue = this.hfProvincia.Value;
            this.CargarDistrito();
            this.ddlDistrito.SelectedValue = this.hfDistrito.Value;
        }

        protected void btnRefreshClear_Click(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-03
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            this.CargarProvincia();
            this.ddlProvincia.SelectedValue = this.hfProvincia.Value;
            this.CargarDistrito();
            this.ddlDistrito.SelectedValue = this.hfDistrito.Value;
        }
        #endregion

        #region Métodos Privados
        /*private void CargarOficina() 
        {
            BLOficina objBLOficina = new BLOficina();
            this.ASPxComboBox1.DataSource = objBLOficina.Listar(this.nIdEntidad, this.txtBusqOficina.Text.Trim());
            this.ASPxComboBox1.ValueField = "IdOficina";
            this.ASPxComboBox1.TextField = "Nombre";  
            this.ASPxComboBox1.DataBind();                               
        }*/

        /// <summary>
        /// Permite cargar el tipo de documento de identidad.
        /// </summary>
        private void CargarTipoDocumento()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-03
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLTipoDocumento objBLTipoDocumento = new BLTipoDocumento();
            this.CargarDropDownList(this.ddlTipDocumento, "idTipoDocumento", "nombre", objBLTipoDocumento.Obtener(), true);
        }

        /// <summary>
        /// Permite cargar los departamentos.
        /// </summary>
        private void CargarDepartamento()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-03
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLDepartamento objBLDepartamento = new BLDepartamento();
            this.CargarDropDownList(this.ddlDepartamento, "idDepartamento", "nombre", objBLDepartamento.Listar(Constantes.CodPeru), false);
            this.ddlDepartamento.SelectedValue = this.nCodDepartamento.ToString();
        }

        /// <summary>
        /// Permite cargar los ubigeos o cuidades.
        /// </summary>
        private void CargarCiudad()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-03
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLCiudad objBLCiudad = new BLCiudad();
            ViewState[Constantes.ListaCiudad] = objBLCiudad.Listar();               
        }

        /// <summary>
        /// Permite cargar las provincias.
        /// </summary>
        private void CargarProvincia()
        {
            /*
              * CREADO POR:              Gary Porras Paraguay
              * FECHA DE CREACION:       2011-06-03
              * MODIFICADO POR:          
              * FECHA DE MODIFICACION:   
              */

            List<BECiudad> lstBECiudad = ((List<BECiudad>)(ViewState[Constantes.ListaCiudad]));
            BLCiudad objBLCiudad = new BLCiudad();
            this.CargarDropDownList(this.ddlProvincia, "IdProvincia", "Provincia", objBLCiudad.Listar(Convert.ToInt32(this.ddlDepartamento.SelectedValue), lstBECiudad), false);            
        }

        /// <summary>
        /// Permite cargar los distritos.
        /// </summary>
        private void CargarDistrito()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-03
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            List<BECiudad> lstBECiudad = ((List<BECiudad>)(ViewState[Constantes.ListaCiudad]));
            BLCiudad objBLCiudad = new BLCiudad();
            this.CargarDropDownList(this.ddlDistrito, "idCiudad", "nombre", objBLCiudad.Listar(Convert.ToInt32(this.ddlDepartamento.SelectedValue), Convert.ToInt32(this.ddlProvincia.SelectedValue), lstBECiudad), false);  
        }

        /// <summary>
        /// Permite cargar el plan por producto.
        /// </summary>
        private void CargarPlan() 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-03
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLOpcion objBLOpcion = new BLOpcion();
            this.CargarDropDownList(this.ddlPlan, "OPCION", "OPCION", objBLOpcion.Listar(nIdProducto), true);    
        }

        /// <summary>
        /// Permite cargar la frecuencia por producto.
        /// </summary>
        private void CargarFrecuencia() 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-03
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLFrecuencia objBLFrecuencia = new BLFrecuencia();
            this.CargarDropDownList(this.ddlFrecuencia, "IdFrecuencia", "Nombre", objBLFrecuencia.Listar(nIdProducto), true);    
        }

        /// <summary>
        /// Permite cargar la moneda prima por producto.
        /// </summary>
        private void CargarMonedaPrima() 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-03
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLMoneda objBLMoneda = new BLMoneda();
            this.CargarDropDownList(this.ddlMonPrima, "IdMoneda", "nombre", objBLMoneda.Listar(nIdProducto), true);
            this.CargarDropDownList(this.ddlMonCuenta, "IdMoneda", "nombre", objBLMoneda.Listar(nIdProducto), true); 
        }

        /// <summary>
        /// Permite cargar el meédio de pago.
        /// </summary>
        public void CargarMedioPago() 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-03
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLProductoMedioPago objBLProductoMedioPago = new BLProductoMedioPago();
            this.CargarDropDownList(this.ddlMedPago, "IdMedioPago", "DesMedioPago", objBLProductoMedioPago.Listar(this.nIdProducto), true);
        }

        /// <summary>
        /// Permite cargar funciones JavaScript
        /// a los controles del formulario.
        /// </summary>
        private void CargarFuncionesJS()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-03
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */
            
            this.btnGuaVenta.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de expedir el certificado?')== false) return false;");
            this.txtNroCotizacion.Attributes.Add("onkeypress", "return CotizacionAsincrono(event);");            
        }
        #endregion              

        #region Popup Buscar Oficina - Eventos
        protected void btnBusqOficina_Click(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-03
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try
            {
                BLOficina objBLOficina = new BLOficina();
                this.gvBusqOficina.DataSource = objBLOficina.Listar(this.nIdEntidad, this.txtBusqOficina.Text.Trim());
                this.gvBusqOficina.DataBind();
                this.CargarCabBusqOficina();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void gvBusqOficina_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-03
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes.Add("OnMouseOut", "this.className = this.orignalclassName;");
                    e.Row.Attributes.Add("OnMouseOver", "this.orignalclassName = this.className;this.className = 'GridWiew_SelectedRow';");
                    e.Row.Attributes.Add("onclick", "javascript:OnOficinaSelected('" + e.Row.Cells[0].Text + "','" + e.Row.Cells[1].Text + "' )");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Popup Buscar Oficina - Métodos Privados
        /// <summary>
        /// Permite cargar la cabecera del gridview.
        /// </summary>
        private void CargarCabBusqOficina()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-03
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            if (this.gvBusqOficina.Rows.Count == 0)
            {
                BEOficina objBEOficina = new BEOficina();
                List<BEOficina> lstBEOficina = new List<BEOficina>();
                lstBEOficina.Add(objBEOficina);

                this.gvBusqOficina.DataSource = lstBEOficina;
                this.gvBusqOficina.DataBind();
                this.gvBusqOficina.Rows[0].Visible = false;
            }
        }
        #endregion                                  

        #region WebMethods
        [System.Web.Services.WebMethod]
        public static String ObtenerCotizacion(Decimal pcIdCotizacion) 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-09
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */
            String cResult = "";
            BLCotizacion objBLCotizacion = new BLCotizacion();
            BECotizacion objBECotizacion = null; // objBLCotizacion.Obtener(pcIdCotizacion);

            if (objBECotizacion != null) 
            {
                cResult = objBECotizacion.IdTipoDocumento + Constantes.SeparadorText;
                cResult = cResult + objBECotizacion.NroDocumento + Constantes.SeparadorText;
                cResult = cResult + objBECotizacion.ApePaterno + Constantes.SeparadorText;
                cResult = cResult + objBECotizacion.ApeMaterno + Constantes.SeparadorText;
                cResult = cResult + objBECotizacion.PriNombre + Constantes.SeparadorText;
                cResult = cResult + objBECotizacion.SegNombre + Constantes.SeparadorText;
                cResult = cResult + objBECotizacion.Direccion + Constantes.SeparadorText;
                //cResult = cResult + objBECotizacion.idCiudad + Constantes.SeparadorText;
                cResult = cResult + objBECotizacion.FechaCreacion.ToShortDateString() + Constantes.SeparadorText;
                //cResult = cResult + objBECotizacion.Email + Constantes.SeparadorText;
                //cResult = cResult + objBECotizacion.NroTelefono; 
            }
            
            return cResult;
        }
        #endregion                
    }
}
