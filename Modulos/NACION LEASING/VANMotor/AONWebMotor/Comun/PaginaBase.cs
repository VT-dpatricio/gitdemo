﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Configuration;
using VAN.Common.BL;

namespace AONWebMotor
{
    public class PaginaBase : System.Web.UI.Page
    {
        /*
        public void CargarEntidad(DropDownList ddl)
        {
            BLEntidad oBLEntidad = new BLEntidad();
            ddl.DataSource = oBLEntidad.Seleccionar;
            ddl.DataTextField = "Nombre";
            ddl.DataValueField = "IDEntidad";
            ddl.DataBind();
        }
*/
        public bool ValidarAcceso(string pUrl)
        {
            BLSeguridad oBLSeg = new BLSeguridad();
            bool valida = false;
            try
            {
                valida = oBLSeg.ValidarAccesoFrm(Session["IDUsuario"].ToString(), pUrl);
            }
            catch (Exception ex)
            {
                valida = false;
            }
            return valida;
        }

        public bool ValidarEstadoClaveUsuario()
        {
            bool valida = false;
            try
            {
                //If ) Or CBool(Session["ClaveCaducada"]) Then
                valida = Convert.ToBoolean(Session["PrimerLogin"]) | Convert.ToBoolean(Session["ClaveCaducada"]);
                //End If
            }
            catch (Exception ex)
            {
                valida = false;
            }
            return valida;

        }

        public Int32 IDSistema()
        {
            return Int32.Parse(ConfigurationManager.AppSettings["IDSistema"].ToString());
        }

        public Int32 IDEntidad()
        {
            return Int32.Parse(ConfigurationManager.AppSettings["IDEntidad"].ToString());
        }


        
    }
}