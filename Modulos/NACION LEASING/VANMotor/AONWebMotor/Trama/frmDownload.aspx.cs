﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AONWebMotor.Trama
{
    public partial class frmDownload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString.HasKeys()) {

                    string nombreArchivo = Request.QueryString["archivo"];
                    System.IO.FileInfo file = new System.IO.FileInfo(nombreArchivo);

                    if (file.Exists)
                    {
                         Response.Clear();
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                    Response.AddHeader("Content-Length", file.Length.ToString());
                    Response.ContentType = "application/octet-stream";
                    Response.WriteFile(file.FullName);
                    Response.Flush();
                    Response.Close();
                    }
                    else 
                    { 
                    String scriptString = "";
                    scriptString = " javascript:history.back(alert('El archivo no existe en la ruta.'));";                    
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ConfirmScript", scriptString, true);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
