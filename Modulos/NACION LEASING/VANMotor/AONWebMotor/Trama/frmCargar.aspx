<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true"
    CodeBehind="frmCargar.aspx.cs" Inherits="AONWebMotor.Trama.frmCargar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="Form_SubTitulo" colspan="6">
                            CARGAR INFORMACI�N MASIVA
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td class="Form_TextoDer">
                            Tipo de Proceso:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlTipProceso" runat="server" Width="200px" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlTipProceso_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvTipProceso" runat="server" Text="*" ControlToValidate="ddlTipProceso"
                                Display="None" ErrorMessage="Seleccione tipo de proceso." SetFocusOnError="true"
                                InitialValue="-1" ValidationGroup="CargarArchivo">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td class="Form_TextoDer">
                            Tipo Archivo:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlTipoArchivo" runat="server" Width="200px">
                            </asp:DropDownList>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td class="Form_TextoDer">
                            Archivo:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:FileUpload ID="fuTrama" runat="server" />
                            <asp:Button ID="btnCargar" runat="server" Text="Cargar" ValidationGroup="CargarArchivo"
                                OnClick="btnCargar_Click" />
                        </td>
                        <td class="Form_TextoIzq">
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                <br />
                <asp:GridView ID="gvProceso" runat="server" SkinID="sknGridView" OnRowDataBound="gvProceso_RowDataBound"
                    AutoGenerateColumns="False" OnRowCommand="gvProceso_RowCommand" AllowPaging="True"
                    OnPageIndexChanging="gvProceso_PageIndexChanging" PageSize="8" 
                    EnableModelValidation="True">
                    <Columns>
                        <asp:BoundField DataField="IdProceso" HeaderText="C�digo">
                            <HeaderStyle HorizontalAlign="Center" Width="50px" />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Archivo">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnArchivo" runat="server"></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="DesProcesado" HeaderText="Procesado">
                            <HeaderStyle HorizontalAlign="Center" Width="60px" />
                            <ItemStyle HorizontalAlign="Center" Width="60px" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Fec. Proceso">
                            <ItemTemplate>
                                <asp:Label ID="lblFecProceso" runat="server"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="50px" />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Log">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnArchivoLog" runat="server"></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reg. Validos">
                            <ItemTemplate>
                                <asp:Label ID="lblRegTotalVal" runat="server"></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="60px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reg. No Validos">
                            <ItemTemplate>
                                <asp:Label ID="lblRegTotalErr" runat="server"></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="60px" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total Reg.">
                            <ItemTemplate>
                                <asp:Label ID="lblRegTotal" runat="server"></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="60px" />
                            <ItemStyle HorizontalAlign="Center" Width="60px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Obs." Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblNroObservaciones" runat="server"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="50px" HorizontalAlign="Center" />
                            <ItemStyle Width="50px" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Opciones" Visible="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="ibtnDarBaja" runat="server" SkinID="sknIbtnIcoAnular" CommandName="DarBaja" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="50px" />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Export">
                            <ItemTemplate>
                                <asp:ImageButton ID="ibtnTrama" runat="server" CommandName="GeneraTrama" SkinID="sknIbtnExport"
                                    Width="16px" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="50px" />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <asp:ValidationSummary ID="vsCargarArchivo" runat="server" ValidationGroup="CargarArchivo"
        ShowMessageBox="true" ShowSummary="false" />
</asp:Content>
