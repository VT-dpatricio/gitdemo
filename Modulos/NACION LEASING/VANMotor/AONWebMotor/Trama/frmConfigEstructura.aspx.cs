﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessLogic;
using AONAffinity.Motor.BusinessEntity; 

namespace AONWebMotor.Trama
{
    public partial class frmConfigEstructura : PageBase
    {
        #region Valiables
        private List<BETipoDato> lstBETipoDato;
        private static Boolean bCargado = false;
        #endregion
        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)   
                {
                    this.CargarFormulario();
                }                
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, "Se presentó un error al cargar el formulario. " + ex.Message, false);
            }
        }

        protected void ddlTipProceso_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.CargarTipoArchivoxProceso();
                this.CargarEstructuraArchivo();
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, "Se presentó un error al cargar el tipo de archivo del proceso seleccionado. " + ex.Message, false);
            }
        }

        protected void ddlTipoArchivo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try 
            {
                this.CargarEstructuraArchivo();
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, "Se presentó un error al cargar la estructura del archivo. " + ex.Message, false);
            }
        }

        protected void gvProceso_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try 
            {
                if (bCargado)                 
                {
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        this.CargarStyleGV(e);
                        BEEstructuraArchivo objBEEstructuraArchivo = (BEEstructuraArchivo)(e.Row.DataItem);
                        this.CargarTemplateGV(e, objBEEstructuraArchivo);
                    }
                }                
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void gvProceso_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try 
            {
                this.gvProceso.PageIndex = e.NewPageIndex;
                this.CargarEstructuraArchivo();
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void gvProceso_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)             
            { 
                case "Actualizar": 
                     
                    this.ActualizarEstructura(Convert.ToInt32(e.CommandArgument.ToString()));
                    break;
                case "Quitar":
                    break;
            }
        }
        #endregion        

        #region Metodos Privados
        private void CargarFormulario()
        {
            this.CargarTipoProceso();
            this.CargarCabeceraGVEstructura();
            bCargado = true; 
        }

        private void CargarTipoProceso() 
        {
            List<BETipoProceso> lstBETipoProceso = new List<BETipoProceso>();
            BLTipoProceso objBLTipoProceso = new BLTipoProceso();
            this.CargarDropDownList(this.ddlTipProceso, "IdTipoProceso", "Descripcion", objBLTipoProceso.Listar(true), true);
        }

        private void CargarTipoArchivoxProceso() {
        
            if(this.ddlTipProceso.SelectedValue != null )
            {
                if (Convert.ToInt32(ddlTipProceso.SelectedValue) != -1) 
                {
                    this.ddlTipoArchivo.Items.Clear();  
                    BLTipoArchivo objBLTipoArchivo = new BLTipoArchivo();
                    this.CargarDropDownList(this.ddlTipoArchivo, "IdTipoArchivo", "Descripcion", objBLTipoArchivo.ListarxTipoProceso(Convert.ToInt32(this.ddlTipProceso.SelectedValue)), true);
                }                
            }                       
        }

        private void CargarTipoArchivo() 
        {
            if (lstBETipoDato == null) 
            {
                BLTipoDato objBLTipoDato = new BLTipoDato();
                lstBETipoDato = objBLTipoDato.Listar();  
            }            
        }

        private void CargarEstructuraArchivo() 
        {
            this.gvProceso.DataSource = null;
            this.gvProceso.DataBind();

            if (Convert.ToInt32(this.ddlTipProceso.SelectedValue) != -1 && Convert.ToInt32( this.ddlTipoArchivo.SelectedValue) != -1)
            {
                this.lblResultado.Visible = false;
                BLEstructuraArchivo objBLEstructuraArchivo = new BLEstructuraArchivo();
                this.CargarGridView(this.gvProceso, objBLEstructuraArchivo.Listar(Convert.ToInt32(this.ddlTipProceso.SelectedValue), Convert.ToInt32(this.ddlTipoArchivo.SelectedValue), true));                
            }
                                    
            if (this.gvProceso.Rows.Count == 0) 
            {
                this.CargarCabeceraGVEstructura();
                this.lblResultado.Visible = true;
            }                       
        }        

        private void CargarCabeceraGVEstructura() 
        {
            if (this.gvProceso.Rows.Count == 0) 
            {
                List<BEEstructuraArchivo> lstBEEstructuraArchivo = new List<BEEstructuraArchivo>();
                BEEstructuraArchivo objBEEstructuraArchivo = new BEEstructuraArchivo();
                lstBEEstructuraArchivo.Add(objBEEstructuraArchivo);
                this.CargarGridView(this.gvProceso, lstBEEstructuraArchivo);
                this.gvProceso.Rows[0].Visible = false;  
            }
        }

        private void CargarStyleGV(GridViewRowEventArgs e) 
        {
            e.Row.Attributes.Add("OnMouseOut", "this.className = this.orignalclassName;");
            e.Row.Attributes.Add("OnMouseOver", "this.orignalclassName = this.className;this.className = 'GridWiew_SelectedRow';");
        }

        private void CargarTemplateGV(GridViewRowEventArgs e, BEEstructuraArchivo pObjBEEstructuraArchivo) 
        {
            DropDownList ddlTipoDato = (DropDownList)e.Row.FindControl("dllTipoDato");
            TextBox txtValorFijo = (TextBox)e.Row.FindControl("txtValorFijo");
            TextBox txtCampo = (TextBox)e.Row.FindControl("txtCampo");
            TextBox txtInicio = (TextBox)e.Row.FindControl("txtInicio");
            TextBox txtLongitud = (TextBox)e.Row.FindControl("txtLongitud");
            CheckBox cbxObligatorio = (CheckBox)e.Row.FindControl("cbxObligatorio");
            ImageButton ibtnActualizar = (ImageButton)e.Row.FindControl("btnActualizar");
            ImageButton ibtnQuitar = (ImageButton)e.Row.FindControl("ibtnQuitar");

            if (ddlTipoDato != null)
            {
                this.CargarTipoArchivo();
                this.CargarDropDownList(ddlTipoDato, "IdTipoDato", "Descripcion", lstBETipoDato, false);
                ddlTipoDato.SelectedValue = pObjBEEstructuraArchivo.TipoDato;
            }

            if (txtValorFijo != null)
            {
                txtValorFijo.Text = pObjBEEstructuraArchivo.ValorFijo;
            }

            if (txtCampo != null)
            {
                txtCampo.Text = pObjBEEstructuraArchivo.Nombre;
            }

            if (txtInicio != null && this.ddlTipoArchivo.SelectedValue != null)
            {
                if (this.ddlTipoArchivo.SelectedValue.ToString() != String.Empty)
                {
                    if (Convert.ToInt32(this.ddlTipoArchivo.SelectedValue) == TipoArchivo.Texto)
                    {
                        if (pObjBEEstructuraArchivo.Inicio == NullTypes.IntegerNull) { txtInicio.Text = String.Empty; } else { txtInicio.Text = pObjBEEstructuraArchivo.Inicio.ToString(); }
                    }
                    else
                    {
                        txtInicio.Visible = false;
                    }
                }
            }

            if (txtLongitud != null && this.ddlTipoArchivo.SelectedValue != null)
            {
                if (this.ddlTipoArchivo.SelectedValue.ToString() != String.Empty)
                {
                    if (Convert.ToInt32(this.ddlTipoArchivo.SelectedValue) == TipoArchivo.Texto)
                    {
                        if (pObjBEEstructuraArchivo.Longitud == NullTypes.IntegerNull) { txtLongitud.Text = String.Empty; } else { txtLongitud.Text = pObjBEEstructuraArchivo.Longitud.ToString(); }
                    }
                    else
                    {
                        txtLongitud.Visible = false;
                    }
                }
            }

            if (cbxObligatorio != null)
            {
                if (pObjBEEstructuraArchivo.Obligatorio == true)
                {
                    cbxObligatorio.Checked = true;
                }
            }

            if (ibtnActualizar != null) 
            {
                ibtnActualizar.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de actualizar los datos del registro?')== false) return false;");
                ibtnActualizar.CommandArgument = e.Row.RowIndex.ToString();   
            }

            if (ibtnQuitar != null) 
            {
                ibtnQuitar.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de eliminar el registro?')== false) return false;");
            }
        }

        private void ActualizarEstructura(Int32 pnIndex)
        {
            if (pnIndex < 0) 
            {
                this.MostrarMensaje(this.Controls, "Se ha encontrado un error al obtener los datos del registro seleccionado.", false);
                return;
            }
            Label lblOrden = (Label)this.gvProceso.Rows[pnIndex].FindControl("lblOrden");
            DropDownList ddlTipoDato = (DropDownList)this.gvProceso.Rows[pnIndex].FindControl("dllTipoDato");
            TextBox txtValorFijo = (TextBox)this.gvProceso.Rows[pnIndex].FindControl("txtValorFijo");
            TextBox txtCampo = (TextBox)this.gvProceso.Rows[pnIndex].FindControl("txtCampo");
            TextBox txtInicio = (TextBox)this.gvProceso.Rows[pnIndex].FindControl("txtInicio");
            TextBox txtLongitud = (TextBox)this.gvProceso.Rows[pnIndex].FindControl("txtLongitud");
            CheckBox cbxObligatorio = (CheckBox)this.gvProceso.Rows[pnIndex].FindControl("cbxObligatorio");

            BEEstructuraArchivo objBEEstructuraArchivo = new BEEstructuraArchivo();
            objBEEstructuraArchivo.IdTipoProceso = Convert.ToInt32(this.ddlTipProceso.SelectedValue);
            objBEEstructuraArchivo.Orden = Convert.ToInt32(lblOrden.Text.Trim());
            objBEEstructuraArchivo.IdTipoArchivo = Convert.ToInt32(this.ddlTipoArchivo.SelectedValue); 
            objBEEstructuraArchivo.TipoDato = ddlTipoDato.SelectedValue.ToString();
            objBEEstructuraArchivo.ValorFijo = txtValorFijo.Text.Trim();
            objBEEstructuraArchivo.Nombre = txtCampo.Text.Trim();
            if (txtInicio.Text != String.Empty) { objBEEstructuraArchivo.Inicio = Convert.ToInt32(txtInicio.Text.Trim()); } else { objBEEstructuraArchivo.Inicio = NullTypes.IntegerNull; }
            if (txtLongitud.Text != String.Empty) { objBEEstructuraArchivo.Longitud = Convert.ToInt32(txtLongitud.Text.Trim()); } else { objBEEstructuraArchivo.Longitud = NullTypes.IntegerNull; }
            if (cbxObligatorio.Checked) { objBEEstructuraArchivo.Obligatorio = true; } else { objBEEstructuraArchivo.Obligatorio = false; }
            objBEEstructuraArchivo.UsuarioModificacion = "adminmotor";//Session[NombreSession.Usuario].ToString();            


            BLEstructuraArchivo objBLEstructuraArchivo = new BLEstructuraArchivo();
            if (objBLEstructuraArchivo.Actualizar(objBEEstructuraArchivo) > 0)
            {
                this.MostrarMensaje(this.Controls, "Se actualizó los datos de la estrructura de archivo.", false);
            }
            else 
            {
                this.MostrarMensaje(this.Controls, "Se presentó un problema al actualizar los datos de  la estructura del archivo.", false);
            }            
        }
        #endregion        
    }    
}