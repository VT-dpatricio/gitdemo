﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;   
using AONAffinity.Motor.BusinessLogic;
using AONAffinity.Motor.BusinessEntity;   

namespace AONWebMotor.Trama
{
    public partial class frmCargar : PageBase
    {
        #region Variables
        private static List<BETipoArchivo> lstBETipoArchivo = new List<BETipoArchivo>();
        #endregion

        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {                    
                    this.CargarTipoProceso();
                    this.CargarFuncionesJS();
                    this.CargarCabeceraGvProceso();
                }
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);                
            }
        }

        protected void btnCargar_Click(object sender, EventArgs e)
        {
            try
            {
                this.CargarArchivo();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void ddlTipProceso_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.ddlTipProceso.SelectedValue != "-1")
                {
                    this.CargarTipoArchivo();
                    this.CargarProcesos();
                }
                else
                {
                    this.gvProceso.DataSource = null;
                    this.gvProceso.DataBind();
                    this.CargarCabeceraGvProceso();
                }
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void gvProceso_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes.Add("OnMouseOut", "this.className = this.orignalclassName;");
                    e.Row.Attributes.Add("OnMouseOver", "this.orignalclassName = this.className;this.className = 'GridWiew_SelectedRow';");

                    LinkButton lbtnArchivo = (LinkButton)e.Row.FindControl("lbtnArchivo");
                    LinkButton lbtnArchivoLog = (LinkButton)e.Row.FindControl("lbtnArchivoLog");
                    Label lblFecProceso = (Label)e.Row.FindControl("lblFecProceso");
                    Label lblRegTotal = (Label)e.Row.FindControl("lblRegTotal");
                    Label lblRegTotalVal = (Label)e.Row.FindControl("lblRegTotalVal");
                    Label lblRegTotalErr = (Label)e.Row.FindControl("lblRegTotalErr");
                    Label lblNroObservaciones = (Label)e.Row.FindControl("lblNroObservaciones");
                    ImageButton ibtnDarBaja = (ImageButton)e.Row.FindControl("ibtnDarBaja");
                    ImageButton ibtnTrama = (ImageButton)e.Row.FindControl("ibtnTrama");
                    BEProceso objBEProceso = (BEProceso)(e.Row.DataItem);

                    if (lbtnArchivo != null)
                    {
                        lbtnArchivo.Text = objBEProceso.Archivo;
                        lbtnArchivo.Attributes.Add("href", UrlPagina.DownloadTrama.Substring(2) + "?archivo=" + objBEProceso.RutaArchivo + objBEProceso.Archivo);
                        lbtnArchivo.Attributes.Add("target", "_blank");
                    }

                    if (lblRegTotal != null && objBEProceso.RegTotal != NullTypes.IntegerNull)
                    {
                        lblRegTotal.Text = objBEProceso.RegTotal.ToString();
                    }

                    if (lblRegTotalVal != null && objBEProceso.RegTotalVal != NullTypes.IntegerNull)
                    {
                        lblRegTotalVal.Text = objBEProceso.RegTotalVal.ToString();
                    }

                    if (lblRegTotalErr != null && objBEProceso.RegTotalErr != NullTypes.IntegerNull)
                    {
                        lblRegTotalErr.Text = objBEProceso.RegTotalErr.ToString();
                    }

                    if (lblNroObservaciones != null && objBEProceso.NroObservaciones != NullTypes.IntegerNull)
                    {
                        lblNroObservaciones.Text = objBEProceso.NroObservaciones.ToString();
                    }

                    if (lbtnArchivoLog != null && lblFecProceso != null)
                    {
                        if (objBEProceso.Procesado == true)
                        {
                            ibtnDarBaja.Enabled = false;
                            lbtnArchivoLog.Text = objBEProceso.ArchivoLog;
                            lbtnArchivoLog.Attributes.Add("href", UrlPagina.DownloadTrama.Substring(2) + "?archivo=" + objBEProceso.RutaArchivoLog + objBEProceso.ArchivoLog);
                            lbtnArchivoLog.Attributes.Add("target", "_blank");
                            lblFecProceso.Text = objBEProceso.FecProceso.ToShortDateString();
                        }
                        else
                        {
                            if (lblRegTotal != null) { lblRegTotal.Text = String.Empty; }
                            if (lblRegTotalVal != null) { lblRegTotalVal.Text = String.Empty; }
                            if (lblRegTotalErr != null) { lblRegTotalErr.Text = String.Empty; }
                            lbtnArchivoLog.Visible = false;
                        }
                    }

                    if (ibtnDarBaja != null)
                    {
                        if (objBEProceso.Procesado == true)
                        {
                            ibtnDarBaja.Visible = false;
                        }
                        else
                        {
                            ibtnDarBaja.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de dar de baja el archivo?')== false) return false;");
                        }
                    }

                    //Si archivo Genera export, habilitar la opcion.
                    if (objBEProceso.GeneraExport == true)
                    {
                        if (ibtnTrama != null)
                        {
                            ibtnTrama.CommandArgument = objBEProceso.IdProceso.ToString() + "@" + objBEProceso.SpExport + "@" + objBEProceso.Archivo + "@" + objBEProceso.RutaArchivoExport;
                        }
                    }
                    else 
                    {
                        if (ibtnTrama != null) 
                        {
                            ibtnTrama.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);                
            }
        }

        protected void gvProceso_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                BLProceso objBLProceso = new BLProceso();

                switch (e.CommandName)
                {
                    case "DarBaja":
                        BEProceso objBEProceso = new BEProceso();
                        objBEProceso.IdProceso = Convert.ToInt32(e.CommandArgument);
                        objBEProceso.UsuarioModificacion = Session[NombreSession.Usuario].ToString();
                        objBEProceso.EstadoRegistro = false;                        

                        if (objBLProceso.ActualizarEstado(objBEProceso) > 0)
                        {
                            this.MostrarMensaje(this.Controls, "El archivo se dio de baja.", false);
                            this.CargarProcesos();
                        }
                        else
                        {                            
                            throw new Exception("Error al dar de baja el archivo.");
                        }

                        break;

                    case "GeneraTrama":
                        String[] arrCValores = e.CommandArgument.ToString().Split('@');

                        if (arrCValores.Length == 4)
                        {
                            Int32 nIdProceso = Convert.ToInt32(arrCValores[0]);
                            String cProcedure = arrCValores[1];
                            String[] cArchivoTemp = arrCValores[2].Split('.');  
                            String cArchivo = "Exp_" + cArchivoTemp[0] + ".txt";
                            String cRuta = arrCValores[3];

                            objBLProceso.CrearArchivoExport(nIdProceso, cProcedure, cRuta, cArchivo);
                            Response.Redirect(UrlPagina.DownloadTrama.Substring(2) + "?archivo=" + cRuta + cArchivo);                            
                        }
                        else 
                        {
                            throw new Exception("No se puede generar el archivo export porque no esta configurado.");
                        }
                                                                       
                        break;
                }
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);                
            }
        }

        protected void gvProceso_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                this.gvProceso.PageIndex = e.NewPageIndex;
                this.CargarProcesos();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);                
            }
        }
        #endregion

        #region Métodos Privados
        private void CargarArchivo() 
        {
            if (ValidarCargar() == false) 
            {
                return;
            }

            BLTipoProceso objBLTipoProceso = new BLTipoProceso();
            BETipoProceso objBETipoProceso = objBLTipoProceso.Obtener(Convert.ToInt32(this.ddlTipProceso.SelectedValue));   //this.CargarInfoTipoProceso();

            if (objBETipoProceso == null)
            {
                this.MostrarMensaje(this.Controls, "No está configurado el tipo de proceso.", false);
                return;
            }

            if (this.ValidarDirectorioArchivo(objBETipoProceso) == false)
            {
                return;
            }
                                     
            BETipoArchivo objBETipoArchivo = this.ObtenerTipoArchivoSelecion();
            this.ValidarArchivo(objBETipoArchivo);

            String cArchivoTrama = objBETipoProceso.RutaArchivo + this.fuTrama.FileName;
            this.fuTrama.SaveAs(cArchivoTrama);


            if (this.ValidarArchivoExcel(objBETipoProceso.RutaArchivo, this.fuTrama.FileName) == false) 
            {
                this.BorrarArchivo(objBETipoProceso.RutaArchivo + this.fuTrama.FileName);
                return;
            }

            BEProceso objBEProceso = new BEProceso();
            objBEProceso.Descripcion = this.ddlTipProceso.SelectedItem.Text;
            objBEProceso.Archivo = this.fuTrama.FileName;
            objBEProceso.IdTipProceso = Convert.ToInt32(this.ddlTipProceso.SelectedValue);
            objBEProceso.UsuarioCreacion = Session[NombreSession.Usuario].ToString();

            BLProceso objBLProceso = new BLProceso();
            if (objBLProceso.Insertar(objBEProceso) > 0)
            {
                //String cArchivoTrama = objBETipoProceso.RutaArchivo + fuTrama.FileName;
                                        
                objBLProceso.Ejecutar(objBETipoProceso, objBETipoArchivo, Session[NombreSession.Usuario].ToString());
                this.CargarProcesos();
                this.MostrarMensaje(this.Controls, "El archivo se cargó y procesó satisfactoriamente.", false);
            }
            else
            {
                this.MostrarMensaje(this.Controls, "Error al cargar el archivo.", false);
            }            
        }

        private void CargarTipoProceso()
        {
            BLTipoProceso objBLTipoProceso = new BLTipoProceso();
            this.CargarDropDownList(this.ddlTipProceso, "IdTipoProceso", "Descripcion", objBLTipoProceso.Listar(true), true);
        }

        private void CargarFuncionesJS()
        {
            this.btnCargar.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de cargar el archivo?')== false) return false;");
        }

        private void CargarCabeceraGvProceso()
        {
            if (this.gvProceso.Rows.Count == 0)
            {
                List<BEProceso> lstBEProceso = new List<BEProceso>();
                BEProceso objBEProceso = new BEProceso();
                lstBEProceso.Add(objBEProceso);
                this.CargarGridView(this.gvProceso, lstBEProceso);
                this.gvProceso.Rows[0].Visible = false;
            }
        }

        private void CargarProcesos()
        {
            BLProceso objBLProceso = new BLProceso();
            this.CargarGridView(this.gvProceso, objBLProceso.Listar(Convert.ToInt32(this.ddlTipProceso.SelectedValue), true));
            this.CargarCabeceraGvProceso();
        }

        private void CargarTipoArchivo() 
        {
            BLTipoArchivo objBLTipoArchivo = new BLTipoArchivo();
         
            lstBETipoArchivo = objBLTipoArchivo.ListarxTipoProceso(Convert.ToInt32(this.ddlTipProceso.SelectedValue));
            this.ddlTipoArchivo.Items.Clear();  
            this.CargarDropDownList(this.ddlTipoArchivo, "idtipoarchivo", "descripcion", lstBETipoArchivo, true);
        }

        private void BorrarArchivo(String pcArchivo) 
        {
            if (System.IO.File.Exists(pcArchivo)) 
            {
                System.IO.File.Delete(pcArchivo);
            }            
        }

        private Boolean ValidarCargar() 
        {
            if (this.ddlTipoArchivo.Items.Count == 0)
            {
                this.MostrarMensaje(this.Controls, "No existe ningun tipo de archivo configurado para el proceso.", false);
                return false;
            }

            if (Convert.ToInt32(this.ddlTipoArchivo.SelectedValue) == -1)
            {
                this.MostrarMensaje(this.Controls, "Seleccione el tipo de archivo a cargar.", false);
                return false;
            }

            if (!this.fuTrama.HasFile)
            {
                this.MostrarMensaje(this.Controls, "Seleccione el archivo a cargar.", false);
                this.fuTrama.Focus();
            }

            return true;
        }

        private Boolean ValidarDirectorioArchivo(BETipoProceso objBETipoProceso)
        {            
            if (objBETipoProceso.RutaArchivo == NullTypes.CadenaNull)
            {
                this.MostrarMensaje(this.Controls, "No existe infomación del directorio para almacenar los archivos.", false);
                return false;
            }

            if (objBETipoProceso.RutaArchivoLog == NullTypes.CadenaNull)             
            {
                this.MostrarMensaje(this.Controls, "No existe infomación del directorio para almacenar los archivos Log.", false);
                return false;
            }            

            DirectoryInfo objDirectorio = new DirectoryInfo(objBETipoProceso.RutaArchivo);
            DirectoryInfo objDirectorioLog = new DirectoryInfo(objBETipoProceso.RutaArchivoLog);

            if (!objDirectorio.Exists)
            {                
                this.MostrarMensaje(this.Controls, "No está configurado el directorio para archivos: " + objBETipoProceso.RutaArchivo, false);
                return false;
            }

            if (!objDirectorioLog.Exists)
            {
                this.MostrarMensaje(this.Controls,  "No está configurado el directorio para archivos log: " + objBETipoProceso.RutaArchivoLog, false);
                return false;
            }

            String cUrlFile = objBETipoProceso.RutaArchivo + fuTrama.FileName;
            FileInfo objFileInfo = new FileInfo(cUrlFile);

            if (objFileInfo.Exists)
            {
                this.MostrarMensaje(this.Controls, "Existe un archivo con el mismo nombre cargado en el directorio.", false);
                return false;                
            }

            return true;
        }

        private void ValidarArchivo(BETipoArchivo pObjBETipoArchivo)
        {
            if (pObjBETipoArchivo != null) 
            {
                String cExtencion = System.IO.Path.GetExtension(this.fuTrama.FileName).ToLower();
                Int32 nIndexIni = pObjBETipoArchivo.NombreArchivo.IndexOf(".");
                Int32 nIndexFin = pObjBETipoArchivo.NombreArchivo.Length - pObjBETipoArchivo.NombreArchivo.IndexOf(".");

                if (cExtencion != pObjBETipoArchivo.Extencion) 
                {
                    throw new Exception("El archivo no tiene formato correcto, debe ser: " + pObjBETipoArchivo.Extencion);
                }

                if (pObjBETipoArchivo.NombreArchivo.Length != this.fuTrama.FileName.ToString().Length)
                {
                    throw new Exception("El nombre del archivo es incorrecto debe tener el formato: " + pObjBETipoArchivo.NombreArchivo);
                }

                String cFecha = "AAAAMMDD";
                String cArchivoCargar = this.fuTrama.FileName;
                String cArchivoPatron = pObjBETipoArchivo.NombreArchivo;

                cArchivoCargar = cArchivoCargar.Remove(cArchivoCargar.Length - (cFecha.Length + nIndexFin), cFecha.Length);
                cArchivoPatron = cArchivoPatron.Remove(cArchivoPatron.Length - (cFecha.Length + nIndexFin), cFecha.Length);

                if (cArchivoCargar != cArchivoPatron)
                {
                    throw new Exception("El nombre del archivo es incorrecto debe tener el formato: " + pObjBETipoArchivo.NombreArchivo);
                }                                                    
            }

            //V1
            /*
            if (pObjBETipoProceso != null)
            {
                String cExtencion = System.IO.Path.GetExtension(this.fuTrama.FileName).ToLower();
                Int32 nIndexIni = pObjBETipoProceso.NombreArchivo.IndexOf(".");
                Int32 nIndexFin = pObjBETipoProceso.NombreArchivo.Length - pObjBETipoProceso.NombreArchivo.IndexOf(".");
                String cFormatoPermitido = pObjBETipoProceso.NombreArchivo.Substring(nIndexIni, nIndexFin);

                if (cExtencion != cFormatoPermitido)
                {
                    throw new Exception("El archivo no tiene formato correcto, debe ser: " + cFormatoPermitido);
                }
                
                String cFecha = "AAAAMMDD";
                String cArchivoCargar = this.fuTrama.FileName;
                String cArchivoPatron = pObjBETipoProceso.NombreArchivo;                

                cArchivoCargar = cArchivoCargar.Remove(cArchivoCargar.Length - (cFecha.Length + nIndexFin), cFecha.Length);
                cArchivoPatron = cArchivoPatron.Remove(cArchivoPatron.Length - (cFecha.Length + nIndexFin), cFecha.Length);

                if (cArchivoCargar != cArchivoPatron)
                {
                    throw new Exception("El nombre del archivo es incorrecto debe tener el formato: " + pObjBETipoProceso.NombreArchivo);
                }

                if (pObjBETipoProceso.NombreArchivo.Length != this.fuTrama.FileName.ToString().Length)
                {
                    throw new Exception("El nombre del archivo es incorrecto debe tener el formato: " + pObjBETipoProceso.NombreArchivo);
                }
            }*/
        }        
        #endregion

        #region Funciones Privadas
        private BETipoProceso CargarInfoTipoProceso()
        {
            BETipoProceso objBETipoProceso = new BETipoProceso();
            BLTipoProceso objBLTipoProceso = new BLTipoProceso();
            objBETipoProceso = objBLTipoProceso.Obtener(Convert.ToInt32(this.ddlTipProceso.SelectedValue));

            return objBETipoProceso;
        }

        private BETipoArchivo ObtenerTipoArchivoSelecion() 
        {
            BETipoArchivo objBETipoArchivo = null;

            if (lstBETipoArchivo != null) 
            {
                foreach (BETipoArchivo objBETipoArchivoTmp in lstBETipoArchivo)
                {
                    if (this.ddlTipProceso.SelectedValue == objBETipoArchivoTmp.IdTipoProceso.ToString()) 
                    {
                        if (this.ddlTipoArchivo.SelectedValue == objBETipoArchivoTmp.IdTipoArchivo.ToString()) 
                        {
                            objBETipoArchivo = objBETipoArchivoTmp;
                        }
                    }
                }
            }

            return objBETipoArchivo;
        }

        private Boolean ValidarArchivoExcel(String pcRutaArchivo, String pcArchivo) 
        {            
            try
            {
                if (Convert.ToInt32(this.ddlTipoArchivo.SelectedValue) == TipoArchivo.Excel)
                {
                    BLArchivo objBLArchivo = new BLArchivo();

                    if (objBLArchivo.ValidarLectura(pcRutaArchivo, pcArchivo) == false)
                    {
                        System.IO.File.Delete(pcRutaArchivo + pcArchivo);
                        this.MostrarMensaje(this.Controls, "Error al leer el archivo, asegúrese que el nombre de la hoja sea: Hoja1", false);
                        return false;
                    }
                }
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls,  ex.Message, false);
                return false;
            }

            return true;                        
        }
        #endregion
    }
}
