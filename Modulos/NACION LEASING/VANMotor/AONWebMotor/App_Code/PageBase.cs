﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Runtime.InteropServices;
using System.Drawing;
using VAN.Common.BL;


namespace AONWebMotor
{
    public class PageBase : System.Web.UI.Page 
    {

        public bool ValidarAcceso(string pUrl)
        {
            BLSeguridad oBLSeg = new BLSeguridad();
            bool valida = false;
            try
            {
                valida = oBLSeg.ValidarAccesoFrm(Session["IDUsuario"].ToString(), pUrl);
            }
            catch (Exception ex)
            {
                valida = false;
            }
            return valida;
        }


        public void MostrarMensaje(System.Web.UI.ControlCollection control, String message, Boolean cerrarPagina)
        {
            String str = null;
            if (cerrarPagina)
            {
                str = "<script language='JavaScript'>alert('" + message.Replace("'", "\\'").Replace(System.Environment.NewLine, "<br/>") + "'); CloseFormOK();</script>";
            }
            else
            {
                str = "<script language='JavaScript'>alert('" + message.Replace("'", "\\'").Replace(System.Environment.NewLine, "<br/>") + "');</script>";
            }
            control.Add(new LiteralControl(str));
        }

        public void EjecutarJavaScript(System.Web.UI.ControlCollection control, String message)
        {
            String str = null;           
            str = "<script language='JavaScript'>" + message + "</script>";
            control.Add(new LiteralControl(str));
        }

        public void MostrarMensaje(UpdatePanel pObjUpdatePanel, String pcMensaje)
        {
            String script = "alert('" + pcMensaje + "');";
            Guid oGuid = Guid.NewGuid();
            ScriptManager.RegisterStartupScript(pObjUpdatePanel, pObjUpdatePanel.GetType(), oGuid.ToString(), script, true);
        }

        public void MostrarMensaje(UpdatePanel pObjUpdatePanel, String[] pcMensaje)
        {
            Int32 nIndex = 0;
            String cScript; // = "alert('" + pcMensaje + "');";
            String cFila = "";

            while (nIndex < pcMensaje.Length) 
            {
                cFila = cFila + pcMensaje[nIndex] + "\\n";
                nIndex++;
            }

            cScript = "alert('" + cFila + "');";

            
            Guid oGuid = Guid.NewGuid();
            ScriptManager.RegisterStartupScript(pObjUpdatePanel, pObjUpdatePanel.GetType(), oGuid.ToString(), cScript, true);
        }

        public void CargarDropDownList(DropDownList pDdlControl, String pcFieldValue, String pcFieldText, Object pObjLista, Boolean pbItenSelect)
        {
            ListItemCollection values = pDdlControl.Items;
            if (values.FindByValue("-") != null)
            {
                String displayField = values.FindByValue("-").Text;
                pDdlControl.Items.Clear();
                ListItem otmp = new ListItem(displayField, "-");
                pDdlControl.Items.Add(otmp);
                pDdlControl.SelectedValue = "-";
            }
            else if (values.FindByValue("%") != null)
            {
                String displayField = values.FindByValue("%").Text;
                pDdlControl.Items.Clear();
                ListItem otmp = new ListItem(displayField, "%");
                pDdlControl.Items.Add(otmp);
                pDdlControl.SelectedValue = "%";
            }

            pDdlControl.DataTextField = pcFieldText;
            pDdlControl.DataValueField = pcFieldValue;
            pDdlControl.DataSource = pObjLista;
            pDdlControl.DataBind();

            if (pbItenSelect == true)
            {
                pDdlControl.Items.Insert(0, new ListItem("-Seleccione-", "-1"));
                pDdlControl.SelectedValue = "-1";
            }            
        }

        /// <summary>
        /// Permite cargar datos en un GridView.
        /// </summary>
        /// <param name="grdControl"></param>
        /// <param name="oControl"></param>
        public void CargarGridView(GridView grdControl, object oControl)
        {
            grdControl.DataSource = oControl;
            grdControl.DataBind();
        }



        public bool ValidarEstadoClaveUsuario()
        {
            bool valida = false;
            try
            {
                //If ) Or CBool(Session["ClaveCaducada"]) Then
                valida = Convert.ToBoolean(Session["PrimerLogin"]) | Convert.ToBoolean(Session["ClaveCaducada"]);
                //End If
            }
            catch (Exception ex)
            {
                valida = false;
            }
            return valida;

        }

        public Int32 IDSistema()
        {
            return Int32.Parse(ConfigurationManager.AppSettings["IDSistema"].ToString());
        }

        public Int32 IDEntidad()
        {
            return Int32.Parse(ConfigurationManager.AppSettings["IDEntidad"].ToString());
        }

        public void PintarNuevoRegistro(GridViewRowEventArgs e, System.Int16 nroCol)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                System.String codigo;
                try
                {
                    codigo = ((Label)e.Row.Cells[0].Controls[1]).Text;
                    if (codigo == "0" && e.Row.RowState != DataControlRowState.Edit && e.Row.RowState != (DataControlRowState.Alternate | DataControlRowState.Edit))
                    {
                        ((ImageButton)e.Row.Cells[e.Row.Cells.Count - 1].Controls[1]).ImageUrl = "~/Img/nuevo.gif";
                        ((ImageButton)e.Row.Cells[e.Row.Cells.Count - 1].Controls[1]).ToolTip = "Nuevo Registro";
                        for (System.Int16 i = 0; i <= e.Row.Cells.Count - nroCol; i++)
                        {
                            e.Row.Cells[i].BackColor = Color.FromArgb(253, 248, 194);
                        }
                    }
                }
                catch
                {
                }

            }
        }


    }



}
