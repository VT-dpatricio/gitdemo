﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="AONWebMotor.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta id="FirstCtrlID" http-equiv="X-UA-Compatible" content="IE=8" />
    <title>Nación Leasing - Cotizador</title>
	<link id="lnk_favicon" runat="server" rel="icon" href="~/img/favicon.ico" type="image/ico" />
    <style type="text/css">
        body
        {
            background-color: #EBEBEB;
        }        
    </style>
</head>
<body>
    <form id="form1" runat="server">

        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    <br />
        <br />
        <br />
    <br />
    <br />
    <br />
    <br />
        <br />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
            <asp:HiddenField ID="HFIntentos" runat="server" Value="0" />
                <asp:MultiView ID="mvLogin" runat="server" ActiveViewIndex="0">
                    <asp:View ID="View1" runat="server">
 

 <table style="MARGIN: auto; WIDTH: 617px" cellSpacing="0" cellPadding="0" border="0">
 
         <tr>
             <td class="LEncaSup">
             </td>
         </tr>
             <tr>
                 <td class="LEncaCen">
                     <BR />
                     <BR />
                     <table border="0" cellpadding="0" cellspacing="7" 
                         style="MARGIN: auto; WIDTH: 416px">
                         <tr>
                             <td colspan="2" style="HEIGHT: 34px; text-align: right;">
                                 <asp:Image ID="Image1" runat="server" ImageUrl="~/App_Themes/Imagenes/Login/LAcceso.png" />
                             </td>
                             <td style="HEIGHT: 34px">
                             </td>
                         </tr>
                         <tr>
                             <td class="LEtiquetaC" style="WIDTH: 83px">
                                 Usuario:</td>
                             <td class="txtLoginBorde" style="WIDTH: 272px">
                                 <asp:TextBox ID="txtUsuario" runat="server" SkinID="txtLogin" Width="256px"></asp:TextBox>
                             </td>
                             <td>
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                     ControlToValidate="txtUsuario" ErrorMessage="Campo Requerido" 
                                     ToolTip="Campo Requerido" ValidationGroup="Ingresar">*</asp:RequiredFieldValidator>
                             </td>
                         </tr>
                         <tr>
                             <td class="LEtiquetaC" style="width: 83px;">
                                 Contraseña:</td>
                             <td class="txtLoginBorde" style="WIDTH: 272px">
                                 <asp:TextBox ID="txtClave" runat="server" SkinID="txtLogin" 
                                     TextMode="Password" Width="256px"></asp:TextBox>
                             </td>
                             <td>
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                     ControlToValidate="txtClave" ErrorMessage="Campo Requerido" 
                                     ToolTip="Campo Requerido" ValidationGroup="Ingresar">*</asp:RequiredFieldValidator>
                             </td>
                         </tr>
                         <tr>
                             <td colspan="2" style="text-align: right;">
                                 <asp:Label ID="lblMsg" runat="server" ForeColor="#C00000"></asp:Label>
                                 &nbsp;<asp:ImageButton ID="btnIngresar" runat="server" 
                                     ImageUrl="~/Img/Login/btnIngresar.png" ValidationGroup="Ingresar" 
                                     onclick="btnIngresar_Click" />
                                
                             </td>
                             <td>
                             </td>
                         </tr>
                     </table>
                     <BR />
                     <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0">
                         <progresstemplate>
                             <div 
                             style="width: 171px; height: 39px; margin:auto; background-color: #ffffff;">
                                 <table style="width: 169px">
                                     <tr>
                                         <td style="width: 38px">
                                             <asp:Image ID="Image2" runat="server" ImageUrl="~/Img/loading.gif" />
                                         </td>
                                         <td style="width: 157px">
                                             Validando Usuario...</td>
                                     </tr>
                                 </table>
                             </div>
                         </progresstemplate>
                     </asp:UpdateProgress>
                     <BR />
                     <BR />
                 </td>
             </tr>
          <tr>
                     <td class="LEncaInf">
                     </td>
                 </tr>
             
</table>
 
 
 
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <div class="divLoginMsg" >
                            <br />
                            <br />
                            <br />
                            Usted a superado el número de Intentos para el inicio de sesión<br />
                            Por favor contacte con el Administrador</div>
                    </asp:View>
                    <asp:View ID="View3" runat="server">

 <table style="MARGIN: auto; WIDTH: 617px" cellSpacing="0" cellPadding="0" border="0">
 
         <tr>
             <td class="LEncaSup">
             </td>
         </tr>
             <tr>
                 <td class="LEncaCen">
                     <BR />
                     <BR />
                     <table border="0" cellpadding="0" cellspacing="7" 
                         style="MARGIN: auto; WIDTH: 416px; height: 167px;">
                         <tr>
                             <td style="text-align: center;" class="style2">
                                 <asp:Image ID="imgMsg" runat="server" ImageUrl="~/Img/Icons/denegado.png" />
                                 <br />
                                 <br />
                                 <asp:Label ID="lblMensaje" runat="server" Font-Size="12px" ForeColor="#C00000">Acceso denegado</asp:Label>
                             </td>
                         </tr>
                     </table>
                     <BR />
                     
                     <BR />
                     <BR />
                 </td>
             </tr>
          <tr>
                     <td class="LEncaInf">
                     </td>
                 </tr>
             
</table>
                    </asp:View>

                </asp:MultiView>
            </ContentTemplate>
        
       
        </asp:UpdatePanel>
  
    

        
  
    </form>
</body>
</html>
