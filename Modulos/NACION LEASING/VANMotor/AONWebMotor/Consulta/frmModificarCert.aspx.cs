﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessLogic;
using AONAffinity.Motor.BusinessEntity;

namespace AONWebMotor.Consulta
{
    /// <summary>
    /// Formulario de modificadiones del certificado.
    /// </summary>
    public partial class frmModificarCert : PageBase
    {
        #region Valiables Globales
        private List<AONAffinity.Library.BusinessEntity.BDJanus.BEDepartamento> lstBEDepartamento = new List<AONAffinity.Library.BusinessEntity.BDJanus.BEDepartamento>();
        #endregion

        #region Eventos
        #region Tab 1
        protected void Page_Load(object sender, EventArgs e)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-12-22
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            try
            {
                if (!Page.IsPostBack)
                {
                    this.CargarFormulario();
                }
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-12-22
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            try 
            {
                this.CargarProvincia();
                this.CargarDistrito();
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void ddlProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-12-22
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            try 
            {
                this.CargarDistrito();
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-12-22
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            try 
            {
                this.ActualizarCertificado();                
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }        
        }
        #endregion                

        #region Tab 2
        protected void ddlDepEntrega_SelectedIndexChanged(object sender, EventArgs e)
        {
            try 
            {
                this.CargarProvEntrega();
                this.CargarDistEntrega();
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void ddlProvEntrega_SelectedIndexChanged(object sender, EventArgs e)
        {
            try 
            {
                this.CargarDistEntrega();
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }
        #endregion
        #endregion

        #region Métodos
        private void CargarFormulario() 
        {
            if (!String.IsNullOrEmpty(Request.QueryString["idCert"]) && !String.IsNullOrEmpty(Request.QueryString["idProd"]) && !String.IsNullOrEmpty(Request.QueryString["idPais"])) 
            {
                this.hfIdCertificado.Value = Request.QueryString["idCert"];
                this.hfIdProducto.Value = Request.QueryString["idProd"];
                this.hfIdPais.Value = Request.QueryString["idPais"];
                this.CargarTipoDocumento();
                this.CargarCiudades();
                this.CargarDepartamento();
                //this.CargarProvincia();
                //this.CargarDistrito(); 
                this.CargarMedioPago();
                this.CargarEstadoCivil();
                this.CargarDepEntrega();
                //this.CargarProvEntrega();
                //this.CargarDistEntrega(); 
                this.MostrarDatos();
                this.CargarFincionesJS();
            }
        }

        private void CargarFincionesJS() 
        {
            this.btnActualizar.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de actualizar los datos del certificado?')== false) return false;");
            this.btnActInfoProducto.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de actualizar los datos adicionales del certificado?')== false) return false;");
            this.btnActInfoAsegurado.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de actualizar los datos del vehículo?')== false) return false;");  
        }

        private void CargarTipoDocumento() 
        {
            AONAffinity.Motor.BusinessLogic.BDJanus.BLTipoDocProducto objBLTipoDocProducto = new AONAffinity.Motor.BusinessLogic.BDJanus.BLTipoDocProducto();
            this.CargarDropDownList(this.ddlTipDocumento, "IdTipoDocumento", "Nombre", objBLTipoDocProducto.ListarxProducto(Convert.ToInt32(this.hfIdProducto.Value)), false);             
        }        

        private void CargarCiudades()
        {
            AONAffinity.Motor.BusinessLogic.BDJanus.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.BDJanus.BLCiudad();
            List<AONAffinity.Library.BusinessEntity.BDJanus.BECiudad> lstBECiudad = objBLCiudad.Listar(Convert.ToInt32(this.hfIdPais.Value));
            ViewState[ValorConstante.ListaCiudad] = lstBECiudad;     
        }

        private void CargarDepartamento()
        {
            AONAffinity.Motor.BusinessLogic.BDJanus.BLDepartamento objBLDepartamento = new AONAffinity.Motor.BusinessLogic.BDJanus.BLDepartamento();
            this.CargarDropDownList(this.ddlDepartamento, "idDepartamento", "nombre", objBLDepartamento.Listar(Convert.ToInt32(this.hfIdPais.Value)), false);
        }

        private void CargarProvincia() 
        {
            AONAffinity.Motor.BusinessLogic.BDJanus.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.BDJanus.BLCiudad();
            List<AONAffinity.Library.BusinessEntity.BDJanus.BECiudad> lstBECiudad = (List < AONAffinity.Library.BusinessEntity.BDJanus.BECiudad>)(ViewState[ValorConstante.ListaCiudad]);
            List<AONAffinity.Library.BusinessEntity.BDJanus.BECiudad> lstBECiudad_Prov = objBLCiudad.ListarProvincia(Convert.ToInt32(this.ddlDepartamento.SelectedValue), lstBECiudad);
            this.CargarDropDownList(this.ddlProvincia, "idProvincia", "nombre", lstBECiudad_Prov, false);   
        }

        private void CargarDistrito() 
        {
            AONAffinity.Motor.BusinessLogic.BDJanus.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.BDJanus.BLCiudad();
            List<AONAffinity.Library.BusinessEntity.BDJanus.BECiudad> lstBECiudad = (List<AONAffinity.Library.BusinessEntity.BDJanus.BECiudad>)(ViewState[ValorConstante.ListaCiudad]);
            List<AONAffinity.Library.BusinessEntity.BDJanus.BECiudad> lstBECiudad_Dist = objBLCiudad.ListarDistrito(this.ddlProvincia.SelectedValue, lstBECiudad);
            this.CargarDropDownList(this.ddlDistrito, "idCiudad", "nombre", lstBECiudad_Dist, false);
        }

        private void CargarMedioPago()
        {
            if (this.hfIdProducto.Value != String.Empty) 
            {
                AONAffinity.Motor.BusinessLogic.BDJanus.BLProductoMedioPago objBLProductoMedioPago = new AONAffinity.Motor.BusinessLogic.BDJanus.BLProductoMedioPago();
                this.CargarDropDownList(this.ddlMedPago, "idMedioPago", "desMedioPago", objBLProductoMedioPago.Listar(Convert.ToInt32(this.hfIdProducto.Value)), false); 
            }                 
        }

        private void CargarEstadoCivil() 
        {
            AONAffinity.Motor.BusinessLogic.BDJanus.BLEstadoCivil objBLEstadoCivil = new AONAffinity.Motor.BusinessLogic.BDJanus.BLEstadoCivil();
            this.CargarDropDownList(this.ddlEstadoCivil, "IdEstadoCivil", "Nombre", objBLEstadoCivil.Listar(), false);     
        }

        private void CargarDepEntrega() 
        {
            AONAffinity.Motor.BusinessLogic.BDJanus.BLDepartamento objBLDepartamento = new AONAffinity.Motor.BusinessLogic.BDJanus.BLDepartamento();
            this.CargarDropDownList(this.ddlDepEntrega, "idDepartamento", "nombre", objBLDepartamento.Listar(Convert.ToInt32(this.hfIdPais.Value)), false);
        }

        private void CargarProvEntrega()
        {
            AONAffinity.Motor.BusinessLogic.BDJanus.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.BDJanus.BLCiudad();
            List<AONAffinity.Library.BusinessEntity.BDJanus.BECiudad> lstBECiudad = (List<AONAffinity.Library.BusinessEntity.BDJanus.BECiudad>)(ViewState[ValorConstante.ListaCiudad]);
            List<AONAffinity.Library.BusinessEntity.BDJanus.BECiudad> lstBECiudad_Prov = objBLCiudad.ListarProvincia(Convert.ToInt32(this.ddlDepEntrega.SelectedValue), lstBECiudad);
            this.CargarDropDownList(this.ddlProvEntrega, "idProvincia", "nombre", lstBECiudad_Prov, false);   
        }

        private void CargarDistEntrega()
        {
            AONAffinity.Motor.BusinessLogic.BDJanus.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.BDJanus.BLCiudad();
            List<AONAffinity.Library.BusinessEntity.BDJanus.BECiudad> lstBECiudad = (List<AONAffinity.Library.BusinessEntity.BDJanus.BECiudad>)(ViewState[ValorConstante.ListaCiudad]);
            List<AONAffinity.Library.BusinessEntity.BDJanus.BECiudad> lstBECiudad_Dist = objBLCiudad.ListarDistrito(this.ddlProvEntrega.SelectedValue, lstBECiudad);
            this.CargarDropDownList(this.ddlDistEntrega, "idCiudad", "nombre", lstBECiudad_Dist, false);   
        }

        private void MostrarDatos() 
        {
            this.MostarDatoCertificado();
            this.MostrarDatoInfoProductoC();
            this.MostrarDatoInfoAseguradoC();
        }

        private void MostarDatoCertificado() 
        {
            AONAffinity.Motor.BusinessLogic.BDJanus.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.BDJanus.BLCiudad();
            AONAffinity.Motor.BusinessLogic.BDJanus.BLCertificado objBLCertificado = new AONAffinity.Motor.BusinessLogic.BDJanus.BLCertificado();
            AONAffinity.Library.BusinessEntity.BDJanus.BECertificado objBECertificado = objBLCertificado.Obtener(this.hfIdCertificado.Value);

            if (objBECertificado != null)
            {
                this.txtNroCertificado.Text = objBECertificado.NumCertificado.ToString();
                this.txtProducto.Text = objBECertificado.DesProducto;
                this.txtEstCertificado.Text = objBECertificado.DesEstadoCertificado;
                this.hfIdEstCertificado.Value = objBECertificado.IdEstadoCertificado.ToString(); 
                this.txtIniVigencia.Text = objBECertificado.Vigencia.ToShortDateString();
                this.txtFinVigencia.Text = objBECertificado.FinVigencia.ToShortDateString();
                this.txtFecDigitacion.Text = objBECertificado.Digitacion.ToShortDateString();
                this.txtFecVenta.Text = objBECertificado.Venta.ToShortDateString();
                this.txtCodInformador.Text = objBECertificado.IdInformador;
                this.hfIdInformador.Value = objBECertificado.IdInformador;
                this.txtCodOficina.Text = objBECertificado.IdOficina.ToString();
                this.hfIdOficina.Value = objBECertificado.IdOficina.ToString();                

                this.txtNombre1.Text = objBECertificado.Nombre1;
                this.txtNombre2.Text = objBECertificado.Nombre2;
                this.txtApellido1.Text = objBECertificado.Apellido1;
                this.txtApellido2.Text = objBECertificado.Apellido2;
                this.ddlTipDocumento.SelectedValue = objBECertificado.IdTipoDocumento;
                this.txtNroDocumento.Text = objBECertificado.CcCliente;
                this.txtTelefono.Text = objBECertificado.Telefono;
                this.ddlDepartamento.SelectedValue = objBLCiudad.ObtenerIdDepartamento(objBECertificado.IdCiudad.ToString());
                this.CargarProvincia();
                this.ddlProvincia.SelectedValue = objBLCiudad.ObtenerIdProvincia(objBECertificado.IdCiudad.ToString());   
                this.CargarDistrito();
                this.ddlDistrito.SelectedValue = objBECertificado.IdCiudad.ToString();  
                this.txtDireccion.Text = objBECertificado.Direccion;

                this.txtPlan.Text = objBECertificado.Opcion;
                this.txtFrecuencia.Text = objBECertificado.DesFrecuencia;
                this.txtMonedaPri.Text = objBECertificado.DesMonedaPrima;
                this.hfIdMonPrima.Value = objBECertificado.IdMonedaPrima; 
                this.txtValCuota.Text = Decimal.Round(objBECertificado.PrimaCobrar, 2).ToString();
                this.ddlMedPago.SelectedValue = objBECertificado.IdMedioPago;
                this.MostrarControlxMedioPago(objBECertificado.IdMedioPago);
                this.txtNroCuenta.Text = objBECertificado.NumeroCuenta;
            }
        }

        private void MostrarDatoInfoProductoC() 
        {
            AONAffinity.Motor.BusinessLogic.BDJanus.BLInfoProductoC objBLInfoProductoC = new AONAffinity.Motor.BusinessLogic.BDJanus.BLInfoProductoC();
            List<AONAffinity.Library.BusinessEntity.BDJanus.BEInfoProductoC> lstBEInfoProductoC = objBLInfoProductoC.ListarxCertificado(this.hfIdCertificado.Value);

            if (lstBEInfoProductoC != null) 
            {
                foreach (AONAffinity.Library.BusinessEntity.BDJanus.BEInfoProductoC objBEInfoProductoC in lstBEInfoProductoC) 
                {
                    switch (objBEInfoProductoC.Nombre) 
                    {
                        case "FECNACIMIENTO":
                            if (objBEInfoProductoC.ValorDate != NullTypes.FechaNull)
                            {
                                this.txtFecNacimiento.Text = objBEInfoProductoC.ValorDate.ToShortDateString();
                                this.hfFecNacimiento.Value = objBEInfoProductoC.IdInfoProducto.ToString();
                            }
                            break;
                        case "SEXO":
                            if (objBEInfoProductoC.ValorString != NullTypes.CadenaNull)
                            {
                                this.ddlSexo.SelectedValue = objBEInfoProductoC.ValorString;
                                this.hfSexo.Value = objBEInfoProductoC.IdInfoProducto.ToString();    
                            }
                            break;
                        case "ESTADOCIVIL":
                            if (objBEInfoProductoC.ValorString != NullTypes.CadenaNull) 
                            {
                                this.ddlEstadoCivil.SelectedValue = objBEInfoProductoC.ValorString;
                                this.hfEstCivil.Value = objBEInfoProductoC.IdInfoProducto.ToString();   
                            }
                            break;
                        case "DIRENTREGA":
                            if (objBEInfoProductoC.ValorString != NullTypes.CadenaNull) 
                            {
                                this.txtDirEntrega.Text = objBEInfoProductoC.ValorString;
                                this.hfDirEntrega.Value = objBEInfoProductoC.IdInfoProducto.ToString();
                            }
                            break;
                        case "CODUBIGEOENTREGA":
                            if (objBEInfoProductoC.ValorNum != NullTypes.DecimalNull)
                            {
                                AONAffinity.Motor.BusinessLogic.BDJanus.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.BDJanus.BLCiudad();
                                this.CargarDepEntrega();
                                this.ddlDepEntrega.SelectedValue = objBLCiudad.ObtenerIdDepartamento(Decimal.Round(objBEInfoProductoC.ValorNum, 0).ToString());
                                this.CargarProvEntrega();
                                this.ddlProvEntrega.SelectedValue = objBLCiudad.ObtenerIdProvincia(Decimal.Round(objBEInfoProductoC.ValorNum, 0).ToString());
                                this.CargarDistEntrega();
                                this.ddlDistEntrega.SelectedValue = Decimal.Round(objBEInfoProductoC.ValorNum, 0).ToString();
                            }                            
                            break;
                        case "CODSPONSOR":
                            if (objBEInfoProductoC.ValorNum != NullTypes.DecimalNull) 
                            {
                                this.txtSponsor.Text = Decimal.Round(objBEInfoProductoC.ValorNum, 0).ToString();   
                                this.hfSponsor.Value = objBEInfoProductoC.IdInfoProducto.ToString();   
                            }                                                            
                            break;
                        case "PERCOTIZACION":
                            if (objBEInfoProductoC.ValorNum != NullTypes.DecimalNull) 
                            {
                                this.txtPerCotizacion.Text = Decimal.Round(objBEInfoProductoC.ValorNum, 0).ToString();
                                this.hfPerCotizacion.Value = objBEInfoProductoC.IdInfoProducto.ToString();   
                            }
                            break;
                        case "NROCOTIZACION":
                            if (objBEInfoProductoC.ValorNum != NullTypes.DecimalNull) 
                            {
                                this.txtNroCotizacion.Text = Decimal.Round(objBEInfoProductoC.ValorNum, 0).ToString();
                                this.hfNroCotizacion.Value = objBEInfoProductoC.IdInfoProducto.ToString();   
                            }
                            break;
                    }                    
                }
            }
        }

        private void MostrarDatoInfoAseguradoC() 
        {
            AONAffinity.Motor.BusinessLogic.BDJanus.BLInfoAseguradoC objBLInfoAseguradoC = new AONAffinity.Motor.BusinessLogic.BDJanus.BLInfoAseguradoC();
            List<AONAffinity.Library.BusinessEntity.BDJanus.BEInfoAseguradoC> lstBEInfoAseguradoC = objBLInfoAseguradoC.ListarxCertificado(this.hfIdCertificado.Value);

            if (lstBEInfoAseguradoC != null) 
            {
                foreach (AONAffinity.Library.BusinessEntity.BDJanus.BEInfoAseguradoC objBEInfoAseguradoC in lstBEInfoAseguradoC) 
                {
                    switch (objBEInfoAseguradoC.Nombre) 
                    {
                        case "IDMARCA":
                            if (objBEInfoAseguradoC.ValorNum != NullTypes.DecimalNull) 
                            {
                                if (objBEInfoAseguradoC.ValorString != NullTypes.CadenaNull) 
                                {
                                    this.txtMarca.Text = objBEInfoAseguradoC.ValorString;  
                                }
                            }
                            break;
                        case "IDMODELO":
                            if (objBEInfoAseguradoC.ValorNum != NullTypes.DecimalNull) 
                            {
                                if (objBEInfoAseguradoC.ValorString != NullTypes.CadenaNull)
                                {
                                    this.txtModelo.Text = objBEInfoAseguradoC.ValorString;
                                }
                            }
                            break;

                        case "COLOR":
                            if (objBEInfoAseguradoC.ValorString != NullTypes.CadenaNull) 
                            {
                                this.txtColor.Text = objBEInfoAseguradoC.ValorString;   
                            }
                            break;

                        case "ANIOFAB":
                            if (objBEInfoAseguradoC.ValorNum != NullTypes.DecimalNull) 
                            {
                                this.txtAnioFab.Text = Decimal.Round(objBEInfoAseguradoC.ValorNum, 0).ToString();   
                            }
                            break;
                        case "NROCHASIS":
                            if (objBEInfoAseguradoC.ValorString != NullTypes.CadenaNull) 
                            {
                                this.txtNroChasis.Text = objBEInfoAseguradoC.ValorString;  
                            }
                            break;
                        case "NROMOTOR":
                            if (objBEInfoAseguradoC.ValorString != NullTypes.CadenaNull) 
                            {
                                this.txtNroMotor.Text = objBEInfoAseguradoC.ValorString;  
                            }
                            break;
                        
                        case "NROPLACA":
                            if (objBEInfoAseguradoC.ValorString != NullTypes.CadenaNull) 
                            {
                                this.txtNroPlaca.Text = objBEInfoAseguradoC.ValorString;
                            }
                            break;
                        case "NROASIENTOS":
                            if (objBEInfoAseguradoC.ValorNum != NullTypes.DecimalNull) 
                            {
                                this.txtNroAsientos.Text = Decimal.Round(objBEInfoAseguradoC.ValorNum, 0).ToString();     
                            }
                            break;
                        case "IDCATEGORIA":
                            if (objBEInfoAseguradoC.ValorNum != NullTypes.DecimalNull) 
                            {
                                this.txtCategoria.Text = Decimal.Round(objBEInfoAseguradoC.ValorNum, 0).ToString();     
                            }
                            break;
                        case "IDCLASE":
                            if (objBEInfoAseguradoC.ValorNum != NullTypes.DecimalNull) 
                            {
                                this.txtClase.Text = objBEInfoAseguradoC.ValorString;
                            }
                            break;
                        case "IDUSO":
                            if (objBEInfoAseguradoC.ValorNum != NullTypes.DecimalNull) 
                            {
                                this.txtUso.Text = objBEInfoAseguradoC.ValorString;    
                            }
                            break;
                        case "IDTIPO":
                            if (objBEInfoAseguradoC.ValorNum != NullTypes.DecimalNull) 
                            {
                                this.txtTipo.Text = objBEInfoAseguradoC.ValorString; 
                            }
                            break;
                    }
                }
            }
        }

        private void MostrarControlxMedioPago(String pcMedPago) 
        {
            if (pcMedPago == "NA") 
            {
                //this.lblVencimiento.Visible = false;
                //this.txtVencimiento.Visible = false;
                //this.lblReqVencimiento.Visible = false;
                //this.lblMonCuenta.Visible = false;
                //this.ddlMonCuenta.Visible = false;
                this.ddlMedPago.Items.Insert(0, new ListItem("-SELECCIONE-", "-1"));  
            }
            else if (pcMedPago == "TV" || pcMedPago == "TA" || pcMedPago == "TM")
            {
                //this.lblVencimiento.Visible = true;
                //this.txtVencimiento.Visible = true;
                //this.lblReqVencimiento.Visible = true;
                //this.lblMonCuenta.Visible = false;
                //this.ddlMonCuenta.Visible = false;
            }
            else 
            {
                //this.lblVencimiento.Visible = false;
                //this.txtVencimiento.Visible = false;
                //this.lblReqVencimiento.Visible = false;
                //this.lblMonCuenta.Visible = true;
                //this.ddlMonCuenta.Visible = true;
            }  
        }

        private void ActualizarCertificado() 
        {
            AONAffinity.Motor.BusinessLogic.BDJanus.BLCertificado objBLCertificado = new AONAffinity.Motor.BusinessLogic.BDJanus.BLCertificado();

            String cIdmonedaCobro = objBLCertificado.ObtenerMonedaCobro(this.ddlMedPago.SelectedValue, this.hfIdMonPrima.Value, Convert.ToInt32(this.hfIdProducto.Value));

            if (cIdmonedaCobro == String.Empty) 
            {
                throw new Exception("No se encontró moneda de cobro.");
            }

            AONAffinity.Library.BusinessEntity.BDJanus.BECertificado objBECertificado = new AONAffinity.Library.BusinessEntity.BDJanus.BECertificado();
            
            objBECertificado.IdCertificado = this.hfIdCertificado.Value;
            objBECertificado.IdOficina = Convert.ToInt32(this.hfIdOficina.Value);
            objBECertificado.IdInformador = this.hfIdInformador.Value;              
            objBECertificado.IdMedioPago = this.ddlMedPago.SelectedValue;
            objBECertificado.IdEstadoCertificado = Convert.ToInt32(this.hfIdEstCertificado.Value);
            objBECertificado.Nombre1 = this.txtNombre1.Text.Trim().ToUpper();
            objBECertificado.Nombre2 = this.txtNombre2.Text.Trim().ToUpper();
            objBECertificado.Apellido1 = this.txtApellido1.Text.Trim().ToUpper();
            objBECertificado.Apellido2 = this.txtApellido2.Text.Trim().ToUpper();
            objBECertificado.IdTipoDocumento = this.ddlTipDocumento.SelectedValue;
            objBECertificado.CcCliente = this.txtNroDocumento.Text.Trim();
            objBECertificado.IdCiudad = Convert.ToInt32(this.ddlDistrito.SelectedValue);
            objBECertificado.Direccion = this.txtDireccion.Text.Trim().ToUpper();
            objBECertificado.Telefono = this.txtTelefono.Text.Trim();
            objBECertificado.NumeroCuenta = this.txtNroCuenta.Text.Trim();
            objBECertificado.Vencimiento = this.ObtenerVencimiento();
            objBECertificado.IdMonedaCobro = cIdmonedaCobro;
            objBECertificado.UsuarioModificacion = "gparaguay"; 

            if (objBLCertificado.Actualizar(objBECertificado, String.Empty) > 0)
            {
                this.MostrarMensaje(this.Controls, "Se actualizó los datos del certificado.", false);
                this.ActualizarCotizacion();
            }
        }

        private void ActualizarCotizacion() 
        {
            if (this.hfPerCotizacion.Value == String.Empty && this.hfNroCotizacion.Value == string.Empty) 
            {
                return;
            }
            if (!Validaciones.IsNumericoInt32(this.hfPerCotizacion.Value)) 
            {
                return;
            }

            if (!Validaciones.IsNumericoDecimal(this.hfNroCotizacion.Value)) 
            {
                return;
            }

            BLCotizacion objBLCotizacion = new BLCotizacion();
            BECotizacion objBECotizacion = new BECotizacion();
            objBECotizacion.IdPeriodo = Convert.ToInt32(this.hfPerCotizacion.Value);
            objBECotizacion.NroCotizacion = Convert.ToDecimal(this.hfNroCotizacion.Value);
            objBECotizacion.ApePaterno = this.txtApellido1.Text.Trim().ToUpper();
            objBECotizacion.ApeMaterno = this.txtApellido2.Text.Trim().ToUpper();    
            //O_O
            objBLCotizacion.ActualizarCliente(objBECotizacion);  
        }
        #endregion       

        #region Funciones
        private DateTime ObtenerVencimiento() 
        {
            DateTime dVencimiento = NullTypes.FechaNull;

            if (this.ddlMedPago.SelectedValue == "TV" || this.ddlMedPago.SelectedValue == "TM" || this.ddlMedPago.SelectedValue == "TA") 
            {

                if (!Validaciones.ValidaFecha("01/" + this.txtVencimiento.Text.Trim().Substring(0, 2) + "/" + this.txtVencimiento.Text.Trim().Substring(2, 4)))
                {
                    throw new Exception("La fecha de vencimiento de la tarjeta no es valida.");                    
                }

                dVencimiento = Convert.ToDateTime("01/" + this.txtVencimiento.Text.Trim().Substring(0, 2) + "/" + this.txtVencimiento.Text.Trim().Substring(2, 4));                
            }

            return dVencimiento;
        }
        #endregion
    }   
}
