<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true"
    CodeBehind="frmModificarCert.aspx.cs" Inherits="AONWebMotor.Consulta.frmModificarCert" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
        function pageLoad() {
            this.ValidarMedioPago();
        }
        function ValidarMedioPago() {
            //Si no tiene medio de pago.
            if (document.getElementById('<%=ddlMedPago.ClientID %>').value == "-1") {
                document.getElementById('<%=lblMonCuenta.ClientID %>').style.display = 'none';
                document.getElementById('<%=ddlMonCuenta.ClientID %>').style.display = 'none';

                document.getElementById('<%=lblVencimiento.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtVencimiento.ClientID %>').style.display = 'none';
                document.getElementById('<%=lblObligatorio.ClientID %>').style.display = 'none';  
            }
            //Si medio de pago es tarjeta VISA.
            if (document.getElementById('<%=ddlMedPago.ClientID %>').value == "TV") {
                document.getElementById('<%=lblMonCuenta.ClientID %>').style.display = 'none';
                document.getElementById('<%=ddlMonCuenta.ClientID %>').style.display = 'none';

                document.getElementById('<%=lblVencimiento.ClientID %>').style.display = '';
                document.getElementById('<%=txtVencimiento.ClientID %>').style.display = '';
                document.getElementById('<%=lblObligatorio.ClientID %>').style.display = '';
            }
            //Si medio de pago es tarjeta MASTER CARD.
            if (document.getElementById('<%=ddlMedPago.ClientID %>').value == "TM") {
                document.getElementById('<%=lblMonCuenta.ClientID %>').style.display = 'none';
                document.getElementById('<%=ddlMonCuenta.ClientID %>').style.display = 'none';

                document.getElementById('<%=lblVencimiento.ClientID %>').style.display = '';
                document.getElementById('<%=txtVencimiento.ClientID %>').style.display = '';
                document.getElementById('<%=lblObligatorio.ClientID %>').style.display = ''; 
            }
            //Si medio de pago es tarjeta AMEX.
            if (document.getElementById('<%=ddlMedPago.ClientID %>').value == "TA") {
                document.getElementById('<%=lblMonCuenta.ClientID %>').style.display = 'none';
                document.getElementById('<%=ddlMonCuenta.ClientID %>').style.display = 'none';

                document.getElementById('<%=lblVencimiento.ClientID %>').style.display = '';
                document.getElementById('<%=txtVencimiento.ClientID %>').style.display = '';
                document.getElementById('<%=lblObligatorio.ClientID %>').style.display = '';
            }
            //Si medio de pago es cuenta de ahorros.
            if (document.getElementById('<%=ddlMedPago.ClientID %>').value == "A") {
                document.getElementById('<%=lblMonCuenta.ClientID %>').style.display = '';
                document.getElementById('<%=ddlMonCuenta.ClientID %>').style.display = '';
                document.getElementById('<%=lblObligatorio.ClientID %>').style.display = '';

                document.getElementById('<%=lblVencimiento.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtVencimiento.ClientID %>').style.display = 'none';
            }
            //Si medio de pago es cuenta corriente.
            if (document.getElementById('<%=ddlMedPago.ClientID %>').value == "C") {
                document.getElementById('<%=lblMonCuenta.ClientID %>').style.display = '';
                document.getElementById('<%=ddlMonCuenta.ClientID %>').style.display = '';
                document.getElementById('<%=lblObligatorio.ClientID %>').style.display = '';

                document.getElementById('<%=lblVencimiento.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtVencimiento.ClientID %>').style.display = 'none';
            }

            //Si medio de pago es cuenta corriente.
            if (document.getElementById('<%=ddlMedPago.ClientID %>').value == "M") {
                document.getElementById('<%=lblMonCuenta.ClientID %>').style.display = '';
                document.getElementById('<%=ddlMonCuenta.ClientID %>').style.display = '';
                document.getElementById('<%=lblObligatorio.ClientID %>').style.display = '';

                document.getElementById('<%=lblVencimiento.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtVencimiento.ClientID %>').style.display = 'none';
            }
        }
    </script>     
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height: auto;">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;
                    height: auto;">
                    <tr>
                        <td class="Form_SubTitulo" colspan="6">
                            ACTUALIZAR CERTIFICADO
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <cc1:TabContainer ID="tcCertificado" runat="server" Width="100%" class="Form_Fondo"
                                ActiveTabIndex="0">
                                <cc1:TabPanel ID="tpCertificado" runat="server" CssClass="Form_FondoTab">
                                    <HeaderTemplate>
                                        Datos del Certificado
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height: auto;"
                                            class="Form_Fondo">
                                            <tr>
                                                <td class="Form_SubTitulo" colspan="6">
                                                    INFORMACI�N B�SICA
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Form_TextoDer">
                                                    Nro Certificado:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtNroCertificado" runat="server" CssClass="Form_TextBoxDisable"
                                                        ReadOnly="True" Width="100px"></asp:TextBox>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Producto:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtProducto" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="True"
                                                        Width="150px"></asp:TextBox>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Estado:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtEstCertificado" runat="server" CssClass="Form_TextBoxDisable"
                                                        ReadOnly="True" Width="100px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Form_TextoDer">
                                                    Inicio Vigencia:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtIniVigencia" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="True"
                                                        Width="100px"></asp:TextBox>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Fin Vigencia:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtFinVigencia" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="True"></asp:TextBox>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Fec.Digitaci�n:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtFecDigitacion" runat="server" CssClass="Form_TextBoxDisable"
                                                        Width="100px" ReadOnly="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Form_TextoDer">
                                                    Fec.Venta:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtFecVenta" runat="server" CssClass="Form_TextBoxDisable" Width="100px"
                                                        ReadOnly="True"></asp:TextBox>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Informador:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtCodInformador" runat="server" CssClass="Form_TextBoxDisable"
                                                        ReadOnly="True"></asp:TextBox>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Oficina:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtCodOficina" runat="server" CssClass="Form_TextBoxDisable" Width="100px"
                                                        ReadOnly="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;
                                            height: auto;">
                                            <tr>
                                                <td class="Form_SubTitulo" colspan="8">
                                                    INFORMACI�N DEL T�TULAR O CLIENTE
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Form_TextoDer">
                                                    1er. Nombre:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtNombre1" runat="server" CssClass="Form_TextBox" Width="120px"></asp:TextBox><span
                                                        class="Form_TextoObligatorio">*</span>
                                                    <asp:RequiredFieldValidator ID="rfvNombre1" runat="server" ControlToValidate="txtNombre1"
                                                        SetFocusOnError="true" Display="None" ErrorMessage="Ingrese el primer nombre." ValidationGroup="valCertificado" ></asp:RequiredFieldValidator>                                                            
                                                </td>
                                                <td class="Form_TextoDer">
                                                    2do. Nombre:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtNombre2" runat="server" CssClass="Form_TextBox" Width="120px"></asp:TextBox>                                                    
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Tipo Documento:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:DropDownList ID="ddlTipDocumento" runat="server" Width="120px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Form_TextoDer">
                                                    1er Apellido:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtApellido1" runat="server" CssClass="Form_TextBox" Width="120px"></asp:TextBox><span
                                                        class="Form_TextoObligatorio">*</span>
                                                    <asp:RequiredFieldValidator ID="rfvApellido1" runat="server" ControlToValidate="txtApellido1"
                                                        SetFocusOnError="true" Display="None" ErrorMessage="Ingrese el primer apellido." ValidationGroup="valCertificado" ></asp:RequiredFieldValidator>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    2do Apellido:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtApellido2" runat="server" CssClass="Form_TextBox" Width="120px"></asp:TextBox><span
                                                        class="Form_TextoObligatorio">*</span>
                                                    <asp:RequiredFieldValidator ID="rfvApellido2" runat="server" ControlToValidate="txtApellido2"
                                                        SetFocusOnError="true" Display="None" ErrorMessage="Ingrese el primer apellido." ValidationGroup="valCertificado" ></asp:RequiredFieldValidator>   
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Nro. Documento:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtNroDocumento" runat="server" CssClass="Form_TextBox" Width="120px"></asp:TextBox><span
                                                        class="Form_TextoObligatorio">*</span>
                                                    <asp:RequiredFieldValidator ID="rfvNroDocumento" runat="server" ControlToValidate="txtNroDocumento"
                                                        SetFocusOnError="true" Display="None" ErrorMessage="Ingrese el nro. de documento." ValidationGroup="valCertificado" ></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Form_TextoDer">
                                                    Departamento:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:UpdatePanel ID="upDepartamento" runat="server">
                                                        <ContentTemplate>
                                                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                                <tr>
                                                                    <td class="Form_TextoIzq">
                                                                        <asp:DropDownList ID="ddlDepartamento" runat="server" Width="125px" AutoPostBack="True"
                                                                            OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td class="Form_TextoIzq" style="width: 15px;">
                                                                        <asp:UpdateProgress ID="upsDepartamento" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upDepartamento">
                                                                            <ProgressTemplate>
                                                                                <asp:Image ID="imgLoadDep" runat="server" SkinID="sknImgLoading" Width="14px" Height="14px" />
                                                                            </ProgressTemplate>
                                                                        </asp:UpdateProgress>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="ddlProvincia" EventName="selectedIndexChanged" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Provincia:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:UpdatePanel ID="upProvincia" runat="server">
                                                        <ContentTemplate>
                                                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                                <tr>
                                                                    <td class="Form_TextoIzq">
                                                                        <asp:DropDownList ID="ddlProvincia" runat="server" Width="125px" AutoPostBack="True"
                                                                            OnSelectedIndexChanged="ddlProvincia_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td class="Form_TextoIzq" style="width: 15px;">
                                                                        <asp:UpdateProgress ID="upsProvincia" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upProvincia">
                                                                            <ProgressTemplate>
                                                                                <asp:Image ID="imgLoadProv" runat="server" SkinID="sknImgLoading" Width="14px" Height="14px" />
                                                                            </ProgressTemplate>
                                                                        </asp:UpdateProgress>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="ddlDistrito" EventName="selectedIndexChanged" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Distrito:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:UpdatePanel ID="upDistrito" runat="server">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlDistrito" runat="server" Width="125px">
                                                            </asp:DropDownList>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="ddlProvincia" EventName="selectedIndexChanged" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Form_TextoDer">
                                                    Direcci�n:
                                                </td>
                                                <td class="Form_TextoIzq" colspan="3">
                                                    <asp:TextBox ID="txtDireccion" runat="server" CssClass="Form_TextBox" Width="280px"></asp:TextBox><span
                                                        class="Form_TextoObligatorio">*</span>
                                                    <asp:RequiredFieldValidator ID="rfvDireccion" runat="server" ControlToValidate="txtDireccion" SetFocusOnError="True" 
                                                        Display="None" ErrorMessage="Ingrese la direcci�n." ValidationGroup="valCertificado"  ></asp:RequiredFieldValidator>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Tel�fono:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtTelefono" runat="server" CssClass="Form_TextBox" Width="90px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvTelefono" runat="server" ControlToValidate="txtTelefono"
                                                        SetFocusOnError="true" Display="None" ErrorMessage="Ingrese el nro. de tel�fono." ValidationGroup="valCertificado" ></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;
                                            height: auto;">
                                            <tr>
                                                <td class="Form_SubTitulo" colspan="8">
                                                    INFORMACI�N DEL PLAN Y MEDIO DE PAGO
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Form_TextoDer">
                                                    Plan:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtPlan" runat="server" CssClass="Form_TextBoxDisable" Width="100px"
                                                        ReadOnly="True"></asp:TextBox>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Frecuencia:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtFrecuencia" runat="server" CssClass="Form_TextBoxDisable" Width="80px"
                                                        ReadOnly="True"></asp:TextBox>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Moneda Prima:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtMonedaPri" runat="server" CssClass="Form_TextBoxDisable" Width="130px"
                                                        ReadOnly="True"></asp:TextBox>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Valor Cuota:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtValCuota" runat="server" CssClass="Form_TextBoxMontoDisable"
                                                        ReadOnly="True" Width="60px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Form_TextoDer">
                                                     M�dio Pago:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:DropDownList ID="ddlMedPago" runat="server" Width="120px" onchange="return ValidarMedioPago();">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Nro. Cuenta:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtNroCuenta" runat="server" CssClass="Form_TextBox" Width="80px"></asp:TextBox>
                                                </td >                                                                                                  
                                                <td class="Form_TextoDer">
                                                    <asp:Label ID="lblVencimiento" runat="server" Text="Vencimiento:" ></asp:Label>
                                                    <asp:Label ID="lblMonCuenta" runat="server" Text="Moneda:"></asp:Label>      
                                                </td>
                                                <td class="Form_TextoIzq" colspan="3" >                                                    
                                                    <asp:TextBox ID="txtVencimiento" runat="server" CssClass="Form_TextBox" Width="60px"></asp:TextBox>
                                                    <cc1:MaskedEditExtender ID="meeVencimiento" runat="server" Mask="99/9999"
                                                        TargetControlID="txtVencimiento" CultureAMPMPlaceholder="" 
                                                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                                        CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                                        CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" >
                                                    </cc1:MaskedEditExtender>                                                                                                   
                                                    <asp:DropDownList ID="ddlMonCuenta" runat="server" Width="90px" >
                                                    </asp:DropDownList>
                                                    <asp:Label ID="lblObligatorio" runat="server" Text="*" ForeColor="Red" ></asp:Label>
                                                </td>                                                
                                            </tr>
                                        </table>
                                        <br />
                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                            <tr>
                                                <td>
                                                    <span class="Form_TextoObligatorio">&nbsp;(*)Campos Obligatorios</span>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    <asp:Button ID="btnActualizar" runat="server" Text="Actualizar" Width="90px" 
                                                        onclick="btnActualizar_Click" ValidationGroup="valCertificado" CausesValidation="true"  />
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:ValidationSummary ID="vsValCertificado" runat="server" ValidationGroup="valCertificado" ShowMessageBox="true" ShowSummary="false" />
                                        <asp:HiddenField ID="hfIdOficina" runat="server" />
                                        <asp:HiddenField ID="hfIdInformador" runat="server" />
                                        <asp:HiddenField ID="hfIdEstCertificado" runat="server" />
                                        <asp:HiddenField ID="hfIdMonPrima" runat="server" />
                                    </ContentTemplate>
                                </cc1:TabPanel>
                                <cc1:TabPanel ID="tpInfoProducto" runat="server" SkinID="sknTbContenedor">
                                    <HeaderTemplate>
                                        Datos Adicionales del Certificado
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;
                                            height: auto;">
                                            <tr>
                                                <td class="Form_SubTitulo" colspan="6">
                                                    INFORMACI�N ADICIONAL DEL CLIENTE
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Form_TextoDer">
                                                    Fecha Nacimiento:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtFecNacimiento" runat="server" CssClass="Form_TextBox" Width="120px"></asp:TextBox>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Sexo:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:DropDownList ID="ddlSexo" runat="server" Width="120px">
                                                        <asp:ListItem Text="MASCULINO" Value="M"></asp:ListItem>
                                                        <asp:ListItem Text="FEMENINO" Value="F"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Estado Civil:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:DropDownList ID="ddlEstadoCivil" runat="server" Width="120px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Form_TextoDer">
                                                    Direcci�n Entrega:
                                                </td>
                                                <td class="Form_TextoIzq" colspan="3">
                                                    <asp:TextBox ID="txtDirEntrega" runat="server" CssClass="Form_TextBox" Width="350px"></asp:TextBox>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Form_TextoDer">
                                                    Dep. Entrega:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:UpdatePanel ID="upDepEntrega" runat="server">
                                                        <ContentTemplate>
                                                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                                <tr>
                                                                    <td class="Form_TextoIzq">
                                                                        <asp:DropDownList ID="ddlDepEntrega" runat="server" Width="120px" AutoPostBack="True"
                                                                            OnSelectedIndexChanged="ddlDepEntrega_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td class="Form_TextoIzq" style="width: 15px;">
                                                                        <asp:UpdateProgress ID="upsDepEntrega" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upDepEntrega">
                                                                            <ProgressTemplate>
                                                                                <asp:Image ID="imgLoadDepEntrega" runat="server" SkinID="sknImgLoading" Width="14px"
                                                                                    Height="14px" />
                                                                            </ProgressTemplate>
                                                                        </asp:UpdateProgress>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Prov. Entrega:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:UpdatePanel ID="upProvEntrega" runat="server">
                                                        <ContentTemplate>
                                                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                                <tr>
                                                                    <td class="Form_TextoIzq">
                                                                        <asp:DropDownList ID="ddlProvEntrega" runat="server" Width="120px" AutoPostBack="True"
                                                                            OnSelectedIndexChanged="ddlProvEntrega_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td class="Form_TextoIzq" style="width: 15px;">
                                                                        <asp:UpdateProgress ID="upsProvEntrega" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upProvEntrega">
                                                                            <ProgressTemplate>
                                                                                <asp:Image ID="imgLoadProvEntrega" runat="server" SkinID="sknImgLoading" Width="14px" Height="14px" />
                                                                            </ProgressTemplate>
                                                                        </asp:UpdateProgress>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Dist. Entrega:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:UpdatePanel ID="upDistEntrega" runat="server">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlDistEntrega" runat="server" Width="120px">
                                                    </asp:DropDownList>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="ddlProvEntrega" EventName="selectedIndexChanged" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Form_TextoDer">
                                                    Sponsor:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtSponsor" runat="server" CssClass="Form_TextBoxDisable" Width="120px"
                                                        ReadOnly="True"></asp:TextBox>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Periodo Cotizaci�n:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtPerCotizacion" runat="server" CssClass="Form_TextBoxDisable"
                                                        Width="120px" ReadOnly="True"></asp:TextBox>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Nro. Cotizaci�n:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtNroCotizacion" runat="server" CssClass="Form_TextBoxDisable"
                                                        Width="120px" ReadOnly="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                            <tr>
                                                <td>
                                                    <span class="Form_TextoObligatorio">&nbsp;(*)Campos Obligatorios</span>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    <asp:Button ID="btnActInfoProducto" runat="server" Text="Actualizar" Width="90px" />
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <asp:HiddenField ID="hfFecNacimiento" runat="server" />
                                        <asp:HiddenField ID="hfEstCivil" runat="server" />
                                        <asp:HiddenField ID="hfSexo" runat="server" />
                                        <asp:HiddenField ID="hfDirEntrega" runat="server" />
                                        <asp:HiddenField ID="hfSponsor" runat="server" />
                                        <asp:HiddenField ID="hfPerCotizacion" runat="server" />
                                        <asp:HiddenField ID="hfNroCotizacion" runat="server" />
                                    </ContentTemplate>
                                </cc1:TabPanel>
                                <cc1:TabPanel ID="tpAsegurado" runat="server" SkinID="sknTbContenedor">
                                    <HeaderTemplate>
                                        Datos del Veh�culo Asegurado
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;
                                            height: auto;">
                                            <tr>
                                                <td class="Form_SubTitulo" colspan="6">
                                                    INFORMACI�N DEL VEH�CULO
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Form_TextoDer">
                                                    Marca:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtMarca" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                        Width="120px"></asp:TextBox>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Modelo:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtModelo" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                        Width="120px"></asp:TextBox>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Color:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtColor" runat="server" CssClass="Form_TextBox" Width="120px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Form_TextoDer">
                                                    A�o Fabricaci�n:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtAnioFab" runat="server" CssClass="Form_TextBox" Width="120px"></asp:TextBox>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Nro. Chasis:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtNroChasis" runat="server" CssClass="Form_TextBox" Width="120px"></asp:TextBox>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Nro. Motor:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtNroMotor" runat="server" CssClass="Form_TextBox" Width="120px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Form_TextoDer">
                                                    Nro. Placa:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtNroPlaca" runat="server" CssClass="Form_TextBox" Width="120px"></asp:TextBox>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Nro. Asientos:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtNroAsientos" runat="server" CssClass="Form_TextBox" Width="120px"></asp:TextBox>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Categoria:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtCategoria" runat="server" CssClass="Form_TextBox" Width="120px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Form_TextoDer">
                                                    Clase:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtClase" runat="server" CssClass="Form_TextBox" Width="120px"></asp:TextBox>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Uso:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtUso" runat="server" CssClass="Form_TextBox" Width="120px"></asp:TextBox>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Tipo:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtTipo" runat="server" CssClass="Form_TextBox" Width="120px"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                            <tr>
                                                <td>
                                                    <span class="Form_TextoObligatorio">&nbsp;(*)Campos Obligatorios</span>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    <asp:Button ID="btnActInfoAsegurado" runat="server" Text="Actualizar" Width="90px" />
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                    </ContentTemplate>
                                </cc1:TabPanel>
                            </cc1:TabContainer>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hfIdCertificado" runat="server" />
    <asp:HiddenField ID="hfIdProducto" runat="server" />
    <asp:HiddenField ID="hfIdPais" runat="server" />
</asp:Content>
