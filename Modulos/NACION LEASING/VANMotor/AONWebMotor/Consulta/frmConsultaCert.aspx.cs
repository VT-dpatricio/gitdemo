﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessLogic;   
using AONAffinity.Library.Resources;
using AONAffinity.Library.BusinessEntity.BDMotor; 

namespace AONWebMotor.Consulta
{
    public partial class frmConsultaCert : PageBase
    {
        #region Variables
        private Int32 nIdProducto = 5250;
        #endregion
        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-12-07
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            try 
            {
                if (!Page.IsPostBack)
                {
                    this.CargarFormulario();
                }                
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }            
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-12-07
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            try 
            {
                this.BuscarCertificado();
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void gvBusqueda_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-12-07
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            try 
            {              
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes.Add("OnMouseOut", "this.className = this.orignalclassName;");
                    e.Row.Attributes.Add("OnMouseOver", "this.orignalclassName = this.className;this.className = 'GridWiew_SelectedRow';");

                    ImageButton ibtnSelect = (ImageButton)e.Row.FindControl("ibtnSiguiente");
                    AONAffinity.Library.BusinessEntity.BDJanus.BECertificado objBECertificado = (AONAffinity.Library.BusinessEntity.BDJanus.BECertificado)(e.Row.DataItem);

                    String idCiudad = "0";

                    if(objBECertificado.IdCiudad.ToString().Length >= 2 )
                    {
                        idCiudad = objBECertificado.IdCiudad.ToString().Substring(0, 2);  
                    }

                    if (ibtnSelect != null)
                    {
                        ibtnSelect.CommandArgument = objBECertificado.IdCertificado + "@" + objBECertificado.IdProducto + "@" + idCiudad;
                    }
                }                
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void gvBusqueda_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-12-07
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            try 
            {
                switch (e.CommandName)
                {
                    case "Select":
                        String[] arrcParam = new String[3];
                        arrcParam = e.CommandArgument.ToString().Split('@');
                        Response.Redirect("../" + UrlPagina.ModificarCertificado.Substring(2) + "?idCert=" + arrcParam[0] + "&idProd=" + arrcParam[1] + "&idPais=" + arrcParam[2]);
                        break ;
                }
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }
        #endregion        

        #region MetodosPrivados
        private void CargarFormulario()
        {
            this.CargarTipoDocumento();
            this.CargarCabecera();
        }

        private void CargarTipoDocumento() 
        {
            AONAffinity.Motor.BusinessLogic.BDJanus.BLTipoDocProducto objBLTipoDocProducto = new AONAffinity.Motor.BusinessLogic.BDJanus.BLTipoDocProducto();
            this.CargarDropDownList(this.ddlTipDocumento, "IdTipoDocumento", "Nombre", objBLTipoDocProducto.ListarxProducto(nIdProducto), false);

            if (this.ddlTipDocumento.Items.Count > 0) 
            {
                this.ddlTipDocumento.SelectedValue = "L";  
            }
        }

        private void CargarCabecera() 
        {
            if (this.gvBusqueda.Rows.Count == 0) 
            {
                AONAffinity.Library.BusinessEntity.BDJanus.BECertificado objBECertificado = new AONAffinity.Library.BusinessEntity.BDJanus.BECertificado();
                List<AONAffinity.Library.BusinessEntity.BDJanus.BECertificado> lstBECertificado = new List<AONAffinity.Library.BusinessEntity.BDJanus.BECertificado>();                   
                lstBECertificado.Add(objBECertificado);
                this.CargarGridView(this.gvBusqueda, lstBECertificado);
                this.gvBusqueda.Rows[0].Visible = false;   
            }

        }

        private void BuscarCertificado() 
        {            
            if (this.ddlTipConsulta.SelectedValue == "0") 
            {
                this.BuscarxCertificado();                
            }
            else if (this.ddlTipConsulta.SelectedValue == "1") 
            {
                this.BuscarxDocumento();                
            }
            else if (this.ddlTipConsulta.SelectedValue == "2") 
            {
                this.BuscarxNomApe();                
            }
            else 
            {
                this.BuscarxCuenta();                
            }
        }

        private void BuscarxCertificado() 
        {
            AONAffinity.Motor.BusinessLogic.BDJanus.BLCertificado objBLCertificado = new AONAffinity.Motor.BusinessLogic.BDJanus.BLCertificado();
            this.CargarGridView(this.gvBusqueda, objBLCertificado.Buscar(Convert.ToInt32(this.ddlTipConsulta.SelectedValue), 5250, Convert.ToDecimal(this.txtNroCertificado.Text.Trim()), String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, "am10123382"));

            if (this.gvBusqueda.Rows.Count == 0)
            {
                this.CargarCabecera();
                this.lblResultado.Visible = true;
            }
            else
            {
                this.lblResultado.Visible = false;
            }
        }

        private void BuscarxDocumento()
        {
            AONAffinity.Motor.BusinessLogic.BDJanus.BLCertificado objBLCertificado = new AONAffinity.Motor.BusinessLogic.BDJanus.BLCertificado();
            this.CargarGridView(this.gvBusqueda, objBLCertificado.Buscar(Convert.ToInt32(this.ddlTipConsulta.SelectedValue), 5250, 0, this.ddlTipDocumento.SelectedValue, this.txtNroDocumento.Text.Trim(), String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, "am10123382"));

            if (this.gvBusqueda.Rows.Count == 0)
            {
                this.CargarCabecera();
                this.lblResultado.Visible = true;
            }
            else
            {
                this.lblResultado.Visible = false;
            }
        }

        private void BuscarxNomApe()
        {
            AONAffinity.Motor.BusinessLogic.BDJanus.BLCertificado objBLCertificado = new AONAffinity.Motor.BusinessLogic.BDJanus.BLCertificado();
            this.CargarGridView(this.gvBusqueda, objBLCertificado.Buscar(Convert.ToInt32(this.ddlTipConsulta.SelectedValue), 5250, 0, String.Empty, String.Empty, this.txtApePaterno.Text.Trim(), this.txtApeMaterno.Text.Trim(), this.txtPriNombre.Text.Trim(), this.txtSegNombre.Text.Trim(),String.Empty, "am10123382"));

            if (this.gvBusqueda.Rows.Count == 0)
            {
                this.CargarCabecera();
                this.lblResultado.Visible = true;
            }
            else
            {
                this.lblResultado.Visible = false;
            }
        }

        private void BuscarxCuenta()
        {
            AONAffinity.Motor.BusinessLogic.BDJanus.BLCertificado objBLCertificado = new AONAffinity.Motor.BusinessLogic.BDJanus.BLCertificado();
            this.CargarGridView(this.gvBusqueda, objBLCertificado.Buscar(Convert.ToInt32(this.ddlTipConsulta.SelectedValue), 5250, 0, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, this.txtNroCuenta.Text.Trim(), "am10123382"));

            if (this.gvBusqueda.Rows.Count == 0)
            {
                this.CargarCabecera();
                this.lblResultado.Visible = true;
            }
            else
            {
                this.lblResultado.Visible = false;
            }
        }
        #endregion

        
        

        
    }    
}
