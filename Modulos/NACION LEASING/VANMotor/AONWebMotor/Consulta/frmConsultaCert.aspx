<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true" CodeBehind="frmConsultaCert.aspx.cs" Inherits="AONWebMotor.Consulta.frmConsultaCert" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script language="javascript" type="text/javascript">
        function pageLoad() {
            this.ValidarFiltro();
        }
        function ValidarFiltro() {
            if (document.getElementById('<%=ddlTipConsulta.ClientID %>').value == 0) {
                 document.getElementById('<%=lblNroCertificado.ClientID %>').style.display = '';
                 document.getElementById('<%=txtNroCertificado.ClientID %>').style.display = '';

                 document.getElementById('<%=lblTipDocumento.ClientID %>').style.display = 'none';
                 document.getElementById('<%=ddlTipDocumento.ClientID %>').style.display = 'none';
                 document.getElementById('<%=lblNroDocumento.ClientID %>').style.display = 'none';
                 document.getElementById('<%=txtNroDocumento.ClientID %>').style.display = 'none';

                 document.getElementById('<%=lblApePaterno.ClientID %>').style.display = 'none';
                 document.getElementById('<%=txtApePaterno.ClientID %>').style.display = 'none';
                 document.getElementById('<%=lblApeMaterno.ClientID %>').style.display = 'none';
                 document.getElementById('<%=txtApeMaterno.ClientID %>').style.display = 'none';

                 document.getElementById('<%=lblPriNombre.ClientID %>').style.display = 'none';
                 document.getElementById('<%=txtPriNombre.ClientID %>').style.display = 'none';
                 document.getElementById('<%=lblSegNombre.ClientID %>').style.display = 'none';
                 document.getElementById('<%=txtSegNombre.ClientID %>').style.display = 'none';

                 document.getElementById('<%=lblNroCuenta.ClientID %>').style.display = 'none';
                 document.getElementById('<%=txtNroCuenta.ClientID %>').style.display = 'none';

                 document.getElementById('<%=txtNroCertificado.ClientID %>').focus();
             }
             if (document.getElementById('<%=ddlTipConsulta.ClientID %>').value == 1) {
                 document.getElementById('<%=lblNroCertificado.ClientID %>').style.display = 'none';
                 document.getElementById('<%=txtNroCertificado.ClientID %>').style.display = 'none';

                 document.getElementById('<%=lblTipDocumento.ClientID %>').style.display = '';
                 document.getElementById('<%=ddlTipDocumento.ClientID %>').style.display = '';
                 document.getElementById('<%=lblNroDocumento.ClientID %>').style.display = '';
                 document.getElementById('<%=txtNroDocumento.ClientID %>').style.display = '';

                 document.getElementById('<%=lblApePaterno.ClientID %>').style.display = 'none';
                 document.getElementById('<%=txtApePaterno.ClientID %>').style.display = 'none';
                 document.getElementById('<%=lblApeMaterno.ClientID %>').style.display = 'none';
                 document.getElementById('<%=txtApeMaterno.ClientID %>').style.display = 'none';

                 document.getElementById('<%=lblPriNombre.ClientID %>').style.display = 'none';
                 document.getElementById('<%=txtPriNombre.ClientID %>').style.display = 'none';
                 document.getElementById('<%=lblSegNombre.ClientID %>').style.display = 'none';
                 document.getElementById('<%=txtSegNombre.ClientID %>').style.display = 'none';

                 document.getElementById('<%=lblNroCuenta.ClientID %>').style.display = 'none';
                 document.getElementById('<%=txtNroCuenta.ClientID %>').style.display = 'none';

                 document.getElementById('<%=txtNroDocumento.ClientID %>').focus();
             }

             if (document.getElementById('<%=ddlTipConsulta.ClientID %>').value == 2) {
                 document.getElementById('<%=lblNroCertificado.ClientID %>').style.display = 'none';
                 document.getElementById('<%=txtNroCertificado.ClientID %>').style.display = 'none';

                 document.getElementById('<%=lblTipDocumento.ClientID %>').style.display = 'none';
                 document.getElementById('<%=ddlTipDocumento.ClientID %>').style.display = 'none';
                 document.getElementById('<%=lblNroDocumento.ClientID %>').style.display = 'none';
                 document.getElementById('<%=txtNroDocumento.ClientID %>').style.display = 'none';

                 document.getElementById('<%=lblApePaterno.ClientID %>').style.display = '';
                 document.getElementById('<%=txtApePaterno.ClientID %>').style.display = '';
                 document.getElementById('<%=lblApeMaterno.ClientID %>').style.display = '';
                 document.getElementById('<%=txtApeMaterno.ClientID %>').style.display = '';

                 document.getElementById('<%=lblPriNombre.ClientID %>').style.display = '';
                 document.getElementById('<%=txtPriNombre.ClientID %>').style.display = '';
                 document.getElementById('<%=lblSegNombre.ClientID %>').style.display = '';
                 document.getElementById('<%=txtSegNombre.ClientID %>').style.display = '';

                 document.getElementById('<%=lblNroCuenta.ClientID %>').style.display = 'none';
                 document.getElementById('<%=txtNroCuenta.ClientID %>').style.display = 'none';

                 document.getElementById('<%=txtApePaterno.ClientID %>').focus();
             }

             if (document.getElementById('<%=ddlTipConsulta.ClientID %>').value == 3) {
                 document.getElementById('<%=lblNroCertificado.ClientID %>').style.display = 'none';
                 document.getElementById('<%=txtNroCertificado.ClientID %>').style.display = 'none';

                 document.getElementById('<%=lblTipDocumento.ClientID %>').style.display = 'none';
                 document.getElementById('<%=ddlTipDocumento.ClientID %>').style.display = 'none';
                 document.getElementById('<%=lblNroDocumento.ClientID %>').style.display = 'none';
                 document.getElementById('<%=txtNroDocumento.ClientID %>').style.display = 'none';

                 document.getElementById('<%=lblApePaterno.ClientID %>').style.display = 'none';
                 document.getElementById('<%=txtApePaterno.ClientID %>').style.display = 'none';
                 document.getElementById('<%=lblApeMaterno.ClientID %>').style.display = 'none';
                 document.getElementById('<%=txtApeMaterno.ClientID %>').style.display = 'none';

                 document.getElementById('<%=lblPriNombre.ClientID %>').style.display = 'none';
                 document.getElementById('<%=txtPriNombre.ClientID %>').style.display = 'none';
                 document.getElementById('<%=lblSegNombre.ClientID %>').style.display = 'none';
                 document.getElementById('<%=txtSegNombre.ClientID %>').style.display = 'none';

                 document.getElementById('<%=lblNroCuenta.ClientID %>').style.display = '';
                 document.getElementById('<%=txtNroCuenta.ClientID %>').style.display = '';

                 document.getElementById('<%=txtNroCuenta.ClientID %>').focus();
             }       
         }
     </script>     
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height:auto;" >
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%; height:auto;" >
                    <tr>
                        <td class="Form_SubTitulo" colspan="6">
                            CONSULTAR CERTIFICADO
                        </td>                        
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Consultar Por:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlTipConsulta" runat ="server" Width="150px" onchange="return ValidarFiltro();" >
                                <asp:ListItem Text="CERTIFICADO" Value="0"> </asp:ListItem>
                                <asp:ListItem Text="NRO. DOCUMENTO" Value="1"></asp:ListItem>
                                <asp:ListItem Text="NOMBRES Y APELLIDOS" Value="2"></asp:ListItem>
                                <asp:ListItem Text="NRO. CUENTA" Value="3"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                         <td></td>                        
                        <td class="Form_TextoDer">
                            <asp:Label ID="lblNroCertificado" runat="server" text="Nro. Certificado:" ></asp:Label>
                            <asp:Label ID="lblNroCuenta" runat="server" Text="Nro. Cuenta:" style="display:none;" ></asp:Label>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtNroCertificado" runat="server" CssClass="Form_TextBox" ></asp:TextBox> 
                            <asp:TextBox ID="txtNroCuenta" runat="server" CssClass="Form_TextBox" Width="145px" style="display: none;"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender id="fteNroCertificado" runat="server" TargetControlID="txtNroCertificado" ValidChars="01234567890"></cc1:FilteredTextBoxExtender>
                            <cc1:FilteredTextBoxExtender id="fteNroCuenta" runat="server" TargetControlID="txtNroCuenta" ValidChars="01234567890"></cc1:FilteredTextBoxExtender>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" 
                                onclick="btnBuscar_Click"/>
                        </td>                       
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            <asp:Label ID="lblTipDocumento" runat="server" text="Tipo Documento:" style="display: none;" ></asp:Label>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlTipDocumento" runat="server" Width="150px" style="display: none;" ></asp:DropDownList>
                        </td>
                        <td></td>
                        <td class="Form_TextoDer">
                            <asp:Label ID="lblNroDocumento" runat="server" text="Tipo Documento:" style="display: none;"  ></asp:Label>
                        </td>                        
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtNroDocumento" runat="server" with="150px" CssClass="Form_TextBox" style="display: none;" ></asp:TextBox>
                            <cc1:FilteredTextBoxExtender id="ftetxtNroDocumento" runat="server" TargetControlID="txtNroDocumento" ValidChars="01234567890"></cc1:FilteredTextBoxExtender>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            <asp:Label ID="lblApePaterno" runat="server" Text="Apellido Paterno:" style="display: none;" ></asp:Label>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtApePaterno" runat="server" CssClass="Form_TextBox" Width="145px" style="display: none;" ></asp:TextBox>
                        </td>
                        <td></td>
                        <td class="Form_TextoDer">
                            <asp:Label ID="lblApeMaterno" runat="server" Text="Apellido Materno:" style="display: none;" ></asp:Label>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtApeMaterno" runat="server" CssClass="Form_TextBox" Width="145px" style="display: none;" ></asp:TextBox>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            <asp:Label ID="lblPriNombre" runat="server" Text="Primer Nombre:" style="display: none;" ></asp:Label>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtPriNombre" runat="server" CssClass="Form_TextBox" Width="145px" style="display: none;" ></asp:TextBox>
                        </td>
                        <td></td>
                        <td class="Form_TextoDer">
                            <asp:Label ID="lblSegNombre" runat="server" Text="Segundo Nombre:" style="display: none;" ></asp:Label>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtSegNombre" runat="server" CssClass="Form_TextBox" Width="145px" style="display: none;" ></asp:TextBox>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            
                        </td>
                        <td class="Form_TextoIzq">
                            
                        </td>
                        <td></td>
                        <td class="Form_TextoDer">                            
                        </td>
                        <td class="Form_TextoIzq">                            
                        </td>
                        <td></td>
                    </tr> 
                </table>
                <br />
                <div style="text-align:center;" >
                    <asp:GridView ID="gvBusqueda" runat="server" SkinID="sknGridView" 
                        AutoGenerateColumns="False" onrowcommand="gvBusqueda_RowCommand" 
                        onrowdatabound="gvBusqueda_RowDataBound">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ibtnSiguiente" runat="server" CommandName="Select" 
                                        SkinID="sknIbtnSiguiente" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="40px" />
                                <ItemStyle HorizontalAlign="Center" Width="40px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="DesProducto" HeaderText="Producto">
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NumCertificado" HeaderText="Nro. Certificado">
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DesEstadoCertificado" HeaderText="Estado">
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DesTipoDocumento" HeaderText="Tipo Doc.">
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Nombre1" HeaderText="1er Nombre">
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Nombre2" HeaderText="2do Nombre">
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Apellido1" HeaderText="1er Apellido">
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Apellido2" HeaderText="2do Apellido">
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>                    
                    </asp:GridView>
                    <br />
                    <asp:Label id="lblResultado" runat ="server" Text="No se encontraron resultados." Visible="false" CssClass="Form_MsjLabel"></asp:Label>
                </div>
            </td>
            <td class="Form_tdBorde">
            </td>            
        </tr> 
    </table>
</asp:Content>
