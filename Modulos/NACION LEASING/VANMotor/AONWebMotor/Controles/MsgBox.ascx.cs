﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace AONWebMotor.controles
{
    public partial class MsgBox : System.Web.UI.UserControl
    {
        public bool ShowCloseButton { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (ShowCloseButton)
                    CloseButton.Attributes.Add("onclick", "document.getElementById('" + MessageBox.ClientID + "').style.display = 'none'");
            }
            catch
            {
            }        
        }

        public void ShowError(string message, int height, int width)
        {
            Show(MessageType.Error, message, height, width);
        }

        public void ShowInfo(string message, int height, int width)
        {
            Show(MessageType.Info, message, height, width);
        }

        public void ShowSuccess(string message, int height, int width)
        {
            Show(MessageType.Success, message, height, width);
        }

        public void ShowWarning(string message, int height, int width)
        {
            Show(MessageType.Warning, message, height, width);
        }

        public void Show(MessageType messageType, string message, int height, int width)
        {
            //CloseButton.Visible = ShowCloseButton;
            //litMessage.Text = message;
            this.lblMsg.Text = message;            
            MessageBox.Height = height;
            MessageBox.Width = width;
            MessageBox.CssClass = messageType.ToString().ToLower();
            //this.MessageBox.Visible = true;
            ModalPopupExtenderMessage.Show();
            //this.Visible = true;
        }     

        public enum MessageType
        {
            Error = 1,
            Info = 2,
            Success = 3,
            Warning = 4
        }
    }
}