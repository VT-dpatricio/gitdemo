﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MsgBox.ascx.cs" Inherits="AONWebMotor.controles.MsgBox" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:UpdatePanel ID="updMSGBOX" runat="server">
    <ContentTemplate>
        <link href="../App_Themes/Themes/StyleMsgBox.css" rel="stylesheet" type="text/css" />
        <link href="../App_Themes/Themes/StyleForm.css" rel="stylesheet" type="text/css" />
        <asp:LinkButton ID="LinkButtonTargetControl" runat="server"></asp:LinkButton>
        <asp:Panel ID="MessageBox" runat="server" BorderColor="#666666" BorderStyle="Solid"
            BorderWidth="5px" style="display:none;">
            <div style="background-image: url('../../App_Themes/Imagenes/MsgBox/Banner.jpg');
                background-repeat: repeat-x; vertical-align: top; height: 33px; background-color: Gray;  ">                
                <div style="width: 68px; height: 31px; float: right;" style="display:none;">
                    <asp:HyperLink ID="CloseButton" runat="server">
                        <asp:Image ID="Image1" runat="server" ImageUrl="../App_Themes/Imagenes/MsgBox/closelabel.gif"
                            AlternateText="Click aqui para cerrar ventana" />
                    </asp:HyperLink>
                </div>
            </div>
            <div >
                <p>
                    <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                    <asp:Label ID="lblMsg" runat="server" Font-Bold="True" ForeColor="#666666" ></asp:Label>                    
                </p>
            </div>
            <div class="messagefooter">
                <asp:ImageButton ID="iButtonOK" runat="server" ImageUrl="../App_Themes/Imagenes/MsgBox/btnAceptar.png" />
            </div>
        </asp:Panel>
        <cc1:ModalPopupExtender runat="server" ID="ModalPopupExtenderMessage" TargetControlID="LinkButtonTargetControl"
            PopupControlID="MessageBox" OkControlID="iButtonOK" CancelControlID="CloseButton"
            BackgroundCssClass="messagemodalbackground">
        </cc1:ModalPopupExtender>
    </ContentTemplate>
</asp:UpdatePanel>
