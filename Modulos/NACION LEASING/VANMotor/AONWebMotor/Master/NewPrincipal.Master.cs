﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessLogic;
using AONAffinity.Motor.BusinessEntity;
  

namespace AONWebMotor.Master
{
    public partial class NewPrincipal : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session[NombreSession.Usuario] == null)
                {
                    Response.Redirect("../" + UrlPagina.Login.Substring(2));
                }
                else
                {
                    this.lblUsuario.Text = Session[NombreSession.Usuario].ToString();
                    this.CargarMenu();
                }
            }
            else
            {
                if (Session[NombreSession.Usuario] == null)
                {
                    this.lblUsuario.Text = String.Empty;
                    Response.Redirect("../" + UrlPagina.Login.Substring(2));
                }
            }
        }
        protected void lbtnSalir_Click(object sender, EventArgs e)
        {
            try
            {
                Session[NombreSession.Usuario] = null;
                Session.Clear();
                Response.Redirect("../" + UrlPagina.Login.Substring(2));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void CargarMenu()
        {
            AONWebMotor.PaginaBase PagBase = new AONWebMotor.PaginaBase();
            BLMenu objBLMenu = new BLMenu();

            List<BEMenu> lstBEMenu = null;

            if (Session[NombreSession.Menu] == null)
            {
                lstBEMenu = objBLMenu.Listar(PagBase.IDSistema(), PagBase.IDEntidad(), Session[NombreSession.Usuario].ToString());
            }
            else
            {
                lstBEMenu = ((List<BEMenu>)(Session[NombreSession.Menu]));
            }

            if (lstBEMenu != null)
            {
                Session[NombreSession.Menu] = lstBEMenu;
                this.ArmarMenu(lstBEMenu);
            }
        }
        private void ArmarMenu(List<BEMenu> pLstBEMenu)
        {
            //Int32 nConta = 0; 

            foreach (BEMenu objBEMenu in pLstBEMenu)
            {
                if (objBEMenu.NodPadre == NullTypes.IntegerNull)
                {
                    //MenuItem miPadre = new MenuItem();
                    //miPadre.Text = objBEMenu.Nombre;
                    //miPadre.Value = objBEMenu.IdMenu.ToString();
                    //miPadre.Selectable = false;
                    //this.mMenu.Items.Add(miPadre);

                    DevExpress.Web.ASPxMenu.MenuItem dvxMIPadre = new DevExpress.Web.ASPxMenu.MenuItem();
                    dvxMIPadre.Text = objBEMenu.Nombre;
                    dvxMIPadre.GroupName = objBEMenu.IdMenu.ToString();
                    this.dvxMenu.Items.Add(dvxMIPadre);

                    this.AgregarHijo(dvxMIPadre, pLstBEMenu);
                }
                //else 
                //{                    
                //    this.AgregaHijo(objBEMenu, nConta); 
                //}
            }
        }

        private void AgregarHijo(DevExpress.Web.ASPxMenu.MenuItem pMenuItem, List<BEMenu> pLstBEMenu)
        {
            foreach (BEMenu objBEMenu in pLstBEMenu)
            {
                if (pMenuItem.GroupName.ToString().Equals(objBEMenu.NodPadre.ToString()) && objBEMenu.NodPadre != NullTypes.IntegerNull)
                {
                    DevExpress.Web.ASPxMenu.MenuItem dvxMIHijo = new DevExpress.Web.ASPxMenu.MenuItem();
                    dvxMIHijo.Text = objBEMenu.Nombre;
                    dvxMIHijo.GroupName = objBEMenu.IdMenu.ToString();
                    dvxMIHijo.NavigateUrl = objBEMenu.Ruta;
                    //this.dvxMenu.Items.Add(dvxMIPadre);
                    //this.dvxMenu.Items.Add(new DevExpress.Web.ASPxMenu.MenuItem(objBEMenu.Nombre, objBEMenu.Nombre, String.Empty, objBEMenu.Ruta));
                    pMenuItem.Items.Add(dvxMIHijo);
                    AgregarHijo(dvxMIHijo, pLstBEMenu);
                }
            }
        }


        private void AgregaHijo(BEMenu pObjBEMenu, Int32 nPos)
        {
            //foreach (MenuItem miNodo in this.mMenu.Items) 
            //{
            //    if (miNodo.Value == pObjBEMenu.NodPadre.ToString() ) 
            //    {
            //        MenuItem miHijo = new MenuItem();
            //        miHijo.Text = pObjBEMenu.Nombre;
            //        miHijo.Value = pObjBEMenu.IdMenu.ToString();
            //        miHijo.NavigateUrl = pObjBEMenu.Ruta;

            //        miNodo.ChildItems.Add(miHijo);                                        
            //    }                
            //}

            foreach (DevExpress.Web.ASPxMenu.MenuItem dvxMINodo in this.dvxMenu.Items)
            {
                if (dvxMINodo.GroupName == pObjBEMenu.NodPadre.ToString())
                {
                    DevExpress.Web.ASPxMenu.MenuItem dvxMIHijo = new DevExpress.Web.ASPxMenu.MenuItem();
                    dvxMIHijo.Text = pObjBEMenu.Nombre;
                    dvxMIHijo.NavigateUrl = pObjBEMenu.Ruta;

                    dvxMINodo.Items.Add(dvxMIHijo);
                }
            }
        } 
    }
}