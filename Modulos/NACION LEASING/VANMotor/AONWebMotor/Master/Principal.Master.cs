﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Library.Resources;
using AONAffinity.Motor.Resources;
using System.Collections;
using VAN.Common.BE;
using VAN.Common.BL;

  
namespace AONWebMotor.Master
{
    public partial class Principal : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HttpContext.Current.Request.IsAuthenticated)
            {
                Response.Redirect("~/Login.aspx");
            }
            Page.Header.DataBind();
            if (!IsPostBack)
            {
                try
                {
                    //lblUsuario.Text = Session["NombreUsuario"].ToString() + " (" + Session["IDUsuario"].ToString() + " | " + Session["IDUsuarioDominio"].ToString() + ")";
                    lblUsuario.Text = "Usuario: " + Session["NombreUsuario"].ToString();
                }
                catch (Exception ex)
                {
                    Response.Redirect("~/Login.aspx");
                }
                CargarMenu();
            }


            //if (!Page.IsPostBack)
            //{
            //    if (Session[NombreSession.Usuario] != null)
            //    {
            //      // Response.Redirect("../" + UrlPagina.Login.Substring(2));
            //    /*}
            //    else
            //    {*/
            //        this.lblUsuario.Text = Session[NombreSession.Usuario].ToString();
            //        this.CargarMenu();
            //    }
            //}
            //else
            //{
            //    if (Session[NombreSession.Usuario] == null)
            //    {
            //        this.lblUsuario.Text = String.Empty;
            //        //Response.Redirect("../" + UrlPagina.Login.Substring(2));
            //        Response.Redirect(UrlPagina.Login);
            //    }
            //}


        }

        protected void lbtnSalir_Click(object sender, EventArgs e)
        {
            try 
            {                
                Session[NombreSession.Usuario] = null;
                Session.Clear();
                Response.Redirect(UrlPagina.Login);                
            }
            catch (Exception ex)
            {
                throw ex;   
            }
        }



        private void CargarMenu()
        {
            string newline = System.Environment.NewLine;
            
            BLMenu oBLMenu = new BLMenu();
            ArrayList Lista = oBLMenu.ListarMenu(Session["IDUsuario"].ToString());
            string sMenu = null;
            string sSubMenu = "";
            string IDSubMenu = "";
            sMenu = "<ul>" + newline;
            foreach (BEMenu oBEMenu in Lista)
            {
                if ((oBEMenu.IDMenuPadre == 0))
                {
                    IDSubMenu = "";
                    if (NroSubMenu(oBEMenu.IDMenu, Lista) > 0)
                    {
                        IDSubMenu = "rel=\"ddsubmenu" + oBEMenu.IDMenu + "\"";
                        sSubMenu += "<ul id=\"ddsubmenu" + oBEMenu.IDMenu + "\" class=\"ddsubmenustyle\">" + newline;
                        sSubMenu = this.AgregarSubMenuSis(Lista, sSubMenu, oBEMenu.IDMenu);
                        sSubMenu += "</ul>" + newline;
                    }
                    sMenu += "<li><a href=\"" + Page.ResolveClientUrl(oBEMenu.Url) + "\" " + IDSubMenu + " >" + oBEMenu.Descripcion + "</a></li>" + newline;
                }
            }
            sMenu += "</ul>" + newline;
            ltMenu.Text = sMenu;
            ltSMenu.Text = sSubMenu;
        }

        private string AgregarSubMenuSis(ArrayList Lista, string sSubMenu, int IDMenu)
        {
            string newline = System.Environment.NewLine;
            foreach (BEMenu oBEMenu in Lista)
            {
                if ((oBEMenu.IDMenuPadre == IDMenu))
                {
                    sSubMenu += "<li><a href=\"" + Page.ResolveClientUrl(oBEMenu.Url) + "\">" + oBEMenu.Descripcion + "</a>" + newline;
                    if (NroSubMenu(oBEMenu.IDMenu, Lista) > 0)
                    {
                        sSubMenu += "<ul>" + newline;
                        sSubMenu = AgregarSubMenuSis(Lista, sSubMenu, oBEMenu.IDMenu);
                        sSubMenu += "</ul>" + newline;
                    }
                    sSubMenu += "</li>" + newline;
                }
            }
            return sSubMenu;
        }

        public int NroSubMenu(int pIDMenu, ArrayList pLista)
        {
            int c = 0;
            foreach (BEMenu oBE in pLista)
            {
                if (oBE.IDMenuPadre == pIDMenu)
                {
                    c += 1;
                }
            }
            return c;
        }





        //private void CargarMenu() 
        //{
        //    AONWebMotor.PaginaBase PagBase = new AONWebMotor.PaginaBase();
        //    BLMenu objBLMenu = new BLMenu();

        //    List<BEMenu> lstBEMenu = null;

        //    if (Session[NombreSession.Menu] == null)
        //    {
        //        lstBEMenu = objBLMenu.Listar(PagBase.IDSistema(), PagBase.IDEntidad(), Session[NombreSession.Usuario].ToString());
        //    }
        //    else 
        //    {
        //        lstBEMenu = ((List<BEMenu>)(Session[NombreSession.Menu]));
        //    }
            
        //    if (lstBEMenu != null) 
        //    {
        //        Session[NombreSession.Menu] = lstBEMenu;
        //        this.ArmarMenu(lstBEMenu);
        //    }
        //}

        //private void ArmarMenu(List<BEMenu> pLstBEMenu)
        //{
        //    //Int32 nConta = 0; 

        //    foreach (BEMenu objBEMenu in pLstBEMenu)
        //    {
        //        if (objBEMenu.NodPadre == 0)//NullTypes.IntegerNull)
        //        {
        //            //MenuItem miPadre = new MenuItem();
        //            //miPadre.Text = objBEMenu.Nombre;
        //            //miPadre.Value = objBEMenu.IdMenu.ToString();
        //            //miPadre.Selectable = false;
        //            //this.mMenu.Items.Add(miPadre);

        //            DevExpress.Web.ASPxMenu.MenuItem dvxMIPadre = new DevExpress.Web.ASPxMenu.MenuItem();
        //            dvxMIPadre.Text = objBEMenu.Nombre;
        //            dvxMIPadre.GroupName = objBEMenu.IdMenu.ToString();
        //            if (objBEMenu.Ruta != "")
        //            {
        //                dvxMIPadre.NavigateUrl = objBEMenu.Ruta;
        //            }

        //            this.dvxMenu.Items.Add(dvxMIPadre);
        //            this.AgregarHijo(dvxMIPadre, pLstBEMenu);
        //        }
        //        //else 
        //        //{                    
        //        //    this.AgregaHijo(objBEMenu, nConta); 
        //        //}
        //    }
        //}

        //private void AgregarHijo(DevExpress.Web.ASPxMenu.MenuItem pMenuItem, List<BEMenu> pLstBEMenu)
        //{
        //    foreach (BEMenu objBEMenu in pLstBEMenu)
        //    {
        //        if (pMenuItem.GroupName.ToString().Equals(objBEMenu.NodPadre.ToString()) && objBEMenu.NodPadre != 0)// NullTypes.IntegerNull)
        //        {
        //            DevExpress.Web.ASPxMenu.MenuItem dvxMIHijo = new DevExpress.Web.ASPxMenu.MenuItem();
        //            dvxMIHijo.Text = objBEMenu.Nombre;
        //            dvxMIHijo.GroupName = objBEMenu.IdMenu.ToString();
        //            dvxMIHijo.NavigateUrl = objBEMenu.Ruta;
        //            //this.dvxMenu.Items.Add(dvxMIPadre);
        //            //this.dvxMenu.Items.Add(new DevExpress.Web.ASPxMenu.MenuItem(objBEMenu.Nombre, objBEMenu.Nombre, String.Empty, objBEMenu.Ruta));
        //            pMenuItem.Items.Add(dvxMIHijo);
        //            AgregarHijo(dvxMIHijo, pLstBEMenu);
        //        }
        //    }
        //}


        //private void AgregaHijo(BEMenu pObjBEMenu, Int32 nPos) 
        //{
        //    //foreach (MenuItem miNodo in this.mMenu.Items) 
        //    //{
        //    //    if (miNodo.Value == pObjBEMenu.NodPadre.ToString() ) 
        //    //    {
        //    //        MenuItem miHijo = new MenuItem();
        //    //        miHijo.Text = pObjBEMenu.Nombre;
        //    //        miHijo.Value = pObjBEMenu.IdMenu.ToString();
        //    //        miHijo.NavigateUrl = pObjBEMenu.Ruta;

        //    //        miNodo.ChildItems.Add(miHijo);                                        
        //    //    }                
        //    //}

        //    foreach (DevExpress.Web.ASPxMenu.MenuItem dvxMINodo in this.dvxMenu.Items) 
        //    {
        //        if (dvxMINodo.GroupName == pObjBEMenu.NodPadre.ToString()) 
        //        {
        //            DevExpress.Web.ASPxMenu.MenuItem dvxMIHijo = new DevExpress.Web.ASPxMenu.MenuItem();
        //            dvxMIHijo.Text = pObjBEMenu.Nombre;
        //            dvxMIHijo.NavigateUrl = pObjBEMenu.Ruta;

        //            dvxMINodo.Items.Add(dvxMIHijo);  
        //        }
        //    }
        //}



        protected void Page_PreRender(object sender, System.EventArgs e)
        {
            Control MyFirstCtrl = Page.Header.FindControl("FirstCtrlID");
            Page.Header.Controls.Remove(MyFirstCtrl);
            Page.Header.Controls.AddAt(0, MyFirstCtrl);
        }

   

    }
}
