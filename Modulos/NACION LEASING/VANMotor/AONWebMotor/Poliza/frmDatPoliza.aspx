﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true" CodeBehind="frmDatPoliza.aspx.cs" Inherits="AONWebMotor.Poliza.frmDatPoliza" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>
        Dato de Poliza de Auto
    </h1>
    <table border="0" cellpadding="0" cellspacing="0" style="height: 100%; width: 100%;">
        <tr>
            <td>                
                Intermediario:
            </td>
            <td>
                <asp:DropDownList id="ddlIntermediario" runat="server" >
                </asp:DropDownList>                
            </td>
            <td>
                Aseguradora:                
            </td>
            <td>                
                <asp:DropDownList ID="ddlAseguradora" runat="server">
                </asp:DropDownList> 
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Inicio Vigencia:
            </td>
            <td>
                <asp:TextBox id="txtIniVigencia" runat="server">
                </asp:TextBox>
            </td>
            <td>
                Fin Vigencia:
            </td>
            <td>
                <asp:TextBox id="txtFinVigencia" runat="server">
                </asp:TextBox>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>
                Nro Poliza:
            </td>
            <td>
                <asp:TextBox ID="txtNroPoliza" runat="server">
                </asp:TextBox>
            </td>
            <td>
                Valor Asegurado:
            </td>
            <td>
                <asp:TextBox id="txtValAsegurado" runat="server" >
                </asp:TextBox>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Valor Prima:
            </td>
            <td>
                <asp:TextBox id="txtValPrima" runat="server" >
                </asp:TextBox>
            </td>
            <td>
                Iva:
            </td>
            <td>
                <asp:TextBox id="txtIva" runat="server"
                ></asp:TextBox>
            </td>
            <td></td>
            <td></td>            
        </tr>
        <tr>
            <td>
                Valor Total:
            </td>
            <td>
                <asp:TextBox id="txtValTotal" runat="server">
                </asp:TextBox>
            </td>
            <td>
                Forma de Pago:
            </td>
            <td>
                <asp:DropDownList id="ddlForPago" runat="server" >
                </asp:DropDownList>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Estado Seguro:
            </td>
            <td>
                <asp:DropDownList ID="ddlEstSeguro" runat="server" >
                </asp:DropDownList>
            </td>
            <td>
                Tarjeta Fidelización:
            </td>
            <td>
                <asp:TextBox id="txtTarFidelizacion" runat="server">
                </asp:TextBox>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>
                Cargar Póliza:
            </td>
            <td>
                <asp:FileUpload id="fuPoliza" runat="server" />
            </td>
            <td>
                <asp:Button id="btnCargar" runat="server" Text="Cargar"/>
            </td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>
                Observaciones:
            </td>
            <td colspan="2" rowspan="2" style="vertical-align:top;">
                <asp:TextBox id="txtObservaciones" runat="server" TextMode="MultiLine">
                </asp:TextBox>
            </td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>        
    </table>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="height: 100%; width: 100%;">
        <tr>
            <td>
                <asp:Button id="btnGuardar" runat="server" Text="Guardar" />
                <asp:Button id="btnPrincipal" runat="server" Text="Principal" />
                <asp:Button id="btnPanTareas" runat="server" Text="Panel Tareas" />
                <asp:Button id="btnAnterior" runat="server" Text="Anterior" 
                    onclick="btnAnterior_Click" />
                <asp:Button id="btnSiguiente" runat="server" Text="Siguiente" 
                    onclick="btnSiguiente_Click"  />
            </td>            
        </tr>
    </table>
</asp:Content>
