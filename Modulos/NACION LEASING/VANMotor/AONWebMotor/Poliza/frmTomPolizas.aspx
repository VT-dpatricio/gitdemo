﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true" CodeBehind="frmTomPolizas.aspx.cs" Inherits="AONWebMotor.Poliza.frmTomPolizas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Datos del Tomador Póliza Autos</h1>
    <table border="0" cellpadding="0" cellspacing="0" style="height: 100%; width: 100%;">
        <tr>
            <td colspan="4" >                
                <asp:CheckBox id="cbxTomador" runat="server" Text ="¿El tomador es el mismo asegurado?" TextAlign="Left"/> 
            </td>                       
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Tipo y Nro. Documento:
            </td>
            <td>
                <asp:DropDownList id="ddlTipDocumento" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox id="txtNroDocumento" runat="server">
                </asp:TextBox>
            </td>
            <td>
            </td>
            <td colspan="2" rowspan="9">
                <asp:Image id="imgFondo" runat="server" SkinID="skinImgManos" />                    
            </td>
        </tr>
        <tr>
            <td>
                Apellidos:
            </td>
            <td>
                <asp:TextBox ID="txtApellidos" runat ="server" ></asp:TextBox> 
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Nombres:
            </td>
            <td>
                <asp:TextBox ID="txtNombres" runat ="server" ></asp:TextBox> 
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Sexo:
            </td>
            <td>
                <asp:DropDownList ID="ddlSexo" runat="server" ></asp:DropDownList>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Dirección:
            </td>
            <td>
                <asp:TextBox id="txtDireccion" runat="server" >
                </asp:TextBox>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>
                E-Mail:
            </td>
            <td>
                <asp:TextBox id="txtEmail" runat="server" >
                </asp:TextBox>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>
                Tel. Fijo / Cel.:
            </td>
            <td>
                <asp:TextBox id="txtTelFijo" runat="server" >
                </asp:TextBox>
            </td>
            <td>
                <asp:TextBox id="txtTelCelular" runat="server" >
                </asp:TextBox>
            </td>
            <td></td>
        </tr>
        <tr>
            <td>
                Dpto./Ciudad:
            </td>
            <td>
                <asp:DropDownList ID="ddlDepartamento" runat="server" >
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddlCiudad" runat="server" >
                </asp:DropDownList>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Fecha Nacimiento:
            </td>
            <td>
                <asp:TextBox id="txtFecNacimiento" runat="server">
                </asp:TextBox>
            </td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" >
        <tr>
            <td>
                <asp:Button id="btnGuardar" runat ="server" Text="Guardar" />
                <asp:Button id="btnPrincipal" runat ="server" Text="Principal" />
                <asp:Button id="btnPanTareas" runat ="server" Text="Panel Tareas" />
                <asp:Button id="btnAnterior" runat ="server" Text="Anterior" 
                    onclick="btnAnterior_Click" />
                <asp:Button id="btnSiguiente" runat ="server" Text="Siguiente" 
                    onclick="btnSiguiente_Click" />
            </td>            
        </tr>
    </table>
</asp:Content>
