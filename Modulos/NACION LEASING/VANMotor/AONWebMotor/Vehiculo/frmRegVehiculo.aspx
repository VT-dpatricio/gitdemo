﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true" CodeBehind="frmRegVehiculo.aspx.cs" Inherits="AONWebMotor.Vehiculo.frmRegVehiculo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>
        Datos Generales del Vehículo
    </h1>
    <table border="0" cellpadding="0" cellspacing="0" style="height:100%; width:100%;">
        <tr>
            <td>
                Línea de Negocio:
            </td>
            <td>
                <asp:DropDownList id="ddlLineaNegocio" runat ="server" ></asp:DropDownList>
            </td>
            <td>
                No. Operación:
            </td>
            <td>
                <asp:TextBox id="txtOperacion" runat ="server"></asp:TextBox>
            </td>
            <td rowspan="17">
                <asp:Image id="imgFondo" runat="server" SkinID="skinImgAuto"/>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                Datos Genéricos del Vehículo
            </td>
        </tr>
        <tr>
            <td>
                Marca:
            </td>
            <td>
                <asp:DropDownList id="ddlMarca" runat ="server" ></asp:DropDownList>
            </td>
            <td>
                Clase:                
            </td>
            <td>
                <asp:DropDownList id="ddlClase" runat ="server" ></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                Tipo:
            </td>
            <td>
                <asp:DropDownList id="ddlTipo" runat ="server" ></asp:DropDownList>
            </td> 
            <td>
                Modelo:
            </td>
            <td>
                <asp:DropDownList ID="ddlModelo" runat="server" ></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                SubTipo:
            </td>
            <td>
                <asp:DropDownList ID="ddlSubTipo" runat ="server" >
                </asp:DropDownList>
            </td>
            <td>
                Referencia:
            </td>    
            <td>
                <asp:DropDownList ID="ddlReferencia" runat="server">
                </asp:DropDownList> 
            </td>
        </tr>
        <tr>
            <td>
                Servicio:
            </td>
            <td>
                <asp:DropDownList id="ddlServicio" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                Código Fasecolda:
            </td>
            <td>
                <asp:TextBox ID="txtCodFesecolda" runat="server">
                </asp:TextBox> 
            </td>
        </tr>
        <tr>
            <td colspan ="4">
                Opciones del Seguro
            </td>
        </tr>
        <tr>
            <td>
                Estado del Vehículo:            
            </td>
            <td>
                <asp:DropDownList id="ddlEstVehiculo" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                Uso:
            </td>
            <td>
                <asp:DropDownList id="ddlUso" runat ="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                Zona de Circulación:
            </td>
            <td>
                <asp:DropDownList id="ddlZonCirculacion" runat ="server">
                </asp:DropDownList>
            </td>
            <td>
                Opción de RCE:
            </td>
            <td>
                <asp:CheckBox id="cbxOpcRce" runat ="server" />
            </td>
        </tr>
        <tr>
            <td>
                Lucro Cesante:
            </td> 
            <td>
                <asp:CheckBox id="cbxLucCesante" runat ="server" />
            </td>
            <td>
                Edad del Asegurado:
            </td>
            <td>
               <asp:TextBox id="txtEdaAsegurado" runat="server">
               </asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Sexo del Asegurado:
            </td>
            <td>
                <asp:DropDownList id="ddlSexAsegurado" runat ="server">
                </asp:DropDownList>
            </td>
            <td>
                Capacidad Carga:
            </td>
            <td>
                <asp:DropDownList id="ddlCapCarga" runat ="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                Años Cobertura:
            </td>
            <td>
                <asp:TextBox id="txtAniCobertura" runat="server">
                </asp:TextBox>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                Valores Calculados
            </td>            
        </tr>
        <tr>
            <td>
                Valor Fasecolda:
            </td>
            <td>
                <asp:TextBox id="txtValFasecolda" runat="server">
                </asp:TextBox>
            </td>
            <td>
                Valor Vehículo:
            </td>
            <td>
                <asp:TextBox id="txtValVehiculo" runat="server">
                </asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Valor Accesorios:
            </td>
            <td>
                <asp:TextBox id="txtValAccesorios" runat="server">
                </asp:TextBox>
            </td>
            <td>
                Valor Carrocería:
            </td>
            <td>
                <asp:TextBox id="txtValCorroceria" runat="server">
                </asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Valor Asegurado:
            </td>
            <td>
                <asp:TextBox ID="txtValAsegurado" runat="server">
                </asp:TextBox>
            </td>
            <td>
                Valor Prima:
            </td>
            <td>
                <asp:TextBox id="txtValPrima" runat="server">
                </asp:TextBox>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
        <tr>
            <td>
                <asp:Button id="btnCotizar" runat="server" Text="Cotizar" />
                <asp:Button id="btnGuardar" runat="server" Text="Guardar" />
                <asp:Button id="btnVerCotizacion" runat="server" Text="Ver Cotización" />
                <asp:Button id="btnPrincipal" runat="server" Text="Pricipal" />
                <asp:Button id="btnPanTareas" runat="server" Text="Panel Tareas" />
                <asp:Button id="btnAnterior" runat="server" Text="Anterior" 
                    onclick="btnAnterior_Click" />
                <asp:Button id="btnSiguiente" runat="server" Text ="Siguiente" 
                    onclick="btnSiguiente_Click" />
            </td> 
        </tr>
    </table> 
    <br />
     <table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
        <tr>
            <td>
                <asp:Button id="btnBuscar" runat="server" Text="Buscar" />
                <asp:Button id="btnImpCotizacion" runat="server" Text="Imprimir Cotización" />
                <asp:Button id="btnNueOperacion" runat="server" Text="Nueva Operación" />
                <asp:Button id="btnEnviar" runat="server" Text="Enviar" />
                <asp:Button id="btnAprobar" runat="server" Text="Aprobar" />
                <asp:Button id="btnDevolver" runat="server" Text="Devolver" />
                <asp:Button id="btnRechazar" runat="server" Text ="Rechazar" />
                <asp:Button id="btnAnular" runat="server" Text ="Anular" />
            </td> 
        </tr>
    </table> 
    <br />
</asp:Content>
