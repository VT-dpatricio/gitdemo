<%@ Page Title="Informaci�n del Veh�culo" Language="C#" MasterPageFile="~/Master/Principal.Master"
    AutoEventWireup="true" CodeBehind="frmRegEspVehiculo.aspx.cs" Inherits="AONWebMotor.Vehiculo.frmRegParVehiculo"
    EnableEventValidation="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register src="../controles/MsgBox.ascx" tagname="MsgBox" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script language="javascript" type="text/javascript">
        function FormatMoney(num) {
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                num = num.substring(0, num.length - (4 * i + 3)) + ',' +
                    num.substring(num.length - (4 * i + 3));
            return (((sign) ? '' : '-') + num + '.' + cents);
        }
        function LimpiarForm() {
            document.getElementById('<%=hfIdCotizacion.ClientID %>').value = "";
            document.getElementById('<%=txtCliente.ClientID %>').value = "";
            document.getElementById('<%=hfCliente.ClientID %>').value = "";
            document.getElementById('<%=txtPlan.ClientID %>').value = "";
            document.getElementById('<%=hfPlan.ClientID %>').value = "";
            document.getElementById('<%=txtClase.ClientID %>').value = "";
            document.getElementById('<%=hfClase.ClientID %>').value = "";
            document.getElementById('<%=txtMarca.ClientID %>').value = "";
            document.getElementById('<%=hfMarca.ClientID %>').value = "";
            document.getElementById('<%=txtModelo.ClientID %>').value = "";
            document.getElementById('<%=hfModelo.ClientID %>').value = "";
            document.getElementById('<%=txtAnio.ClientID %>').value = "";
            document.getElementById('<%=hfAnio.ClientID %>').value = "";
            document.getElementById('<%=txtNroPlaca.ClientID %>').value = "";
            document.getElementById('<%=ddlTipCarroceria.ClientID %>').value = "";
            document.getElementById('<%=hfCodColor.ClientID %>').value = "";
            document.getElementById('<%=txtColor.ClientID %>').value = "";
            document.getElementById('<%=hfDesColor.ClientID %>').value = "";
            document.getElementById('<%=hfDesColor.ClientID %>').value = "";
            document.getElementById('<%=txtNroPasjeros.ClientID %>').value = "";
            document.getElementById('<%=txtNroMotor.ClientID %>').value = "";
            document.getElementById('<%=ddlTipCombustible.ClientID %>').value = "";
            document.getElementById('<%=ddlTipMercancia.ClientID %>').value = "";
            document.getElementById('<%=txtNroChasis.ClientID %>').value = "";
            document.getElementById('<%=txtObsVehiculo.ClientID %>').value = "";
            document.getElementById('<%=ddlTipAccesorio.ClientID %>').value = 0;
            document.getElementById('<%=txtValAccesorio.ClientID %>').value = "";
            document.getElementById('<%=txtObsAccesorio.ClientID %>').value = "";
            document.getElementById('<%=hfTipoEvento.ClientID %>').value = "";
            document.getElementById('<%=btnRefreshClear.ClientID %>').click();
        }
    </script>

    <script language="javascript" type="text/javascript">
        function OnColorSelected(pCod, pDes) {
            document.getElementById('<%=hfCodColor.ClientID %>').value = pCod;
            document.getElementById('<%=txtColor.ClientID %>').value = Encoder.htmlDecode(pDes);
            document.getElementById('<%=hfDesColor.ClientID %>').value = Encoder.htmlDecode(pDes);
            document.getElementById('<%=txtColor.ClientID %>').focus();
            hideModalPopup('bmpeColor');
        }

        function OnAccesorioSelected(pCod, pVal, pDes) {
            document.getElementById('<%=ddlTipAccesorio.ClientID %>').value = pCod;
            document.getElementById('<%=txtValAccesorio.ClientID %>').value = Encoder.htmlDecode(pVal);
            document.getElementById('<%=txtObsAccesorio.ClientID %>').value = Encoder.htmlDecode(pDes);
            document.getElementById('<%=txtValAccesorio.ClientID %>').focus();
        }
    </script>

    <script language="javascript" type="text/javascript">
        function CotizacionAsincrono(e) {
            var evt = e ? e : window.event;
            if (evt.keyCode == 13) {
                var nroCotizacion = document.getElementById('<%=txtNroCotizacion.ClientID %>').value;

                if (nroCotizacion != "") {
                    ObtenerCotizacion(nroCotizacion);
                    return false;
                }
                else {
                    return false;
                }
            }
        }
        function ObtenerCotizacion(pNroCotizacion) {
            var cotizacion = PageMethods.ObtenerCotizacion(pNroCotizacion, onSucceedCotizacion, OnErrorCotizacion);
        }
        function onSucceedCotizacion(result) {
            this.LimpiarForm();
            var arrValores = result.split('�');
            if (result != "") {

                document.getElementById('<%=hfIdCotizacion.ClientID %>').value = Encoder.htmlDecode(arrValores[0]);
                document.getElementById('<%=txtNroCotizacion.ClientID %>').value = Encoder.htmlDecode(arrValores[0]);
                document.getElementById('<%=txtCliente.ClientID %>').value = Encoder.htmlDecode(arrValores[3]);
                document.getElementById('<%=hfCliente.ClientID %>').value = Encoder.htmlDecode(arrValores[3]);
                document.getElementById('<%=txtPlan.ClientID %>').value = Encoder.htmlDecode(arrValores[4]);
                document.getElementById('<%=hfPlan.ClientID %>').value = Encoder.htmlDecode(arrValores[4]);
                document.getElementById('<%=txtClase.ClientID %>').value = Encoder.htmlDecode(arrValores[5]);
                document.getElementById('<%=hfClase.ClientID %>').value = Encoder.htmlDecode(arrValores[5]);
                document.getElementById('<%=txtMarca.ClientID %>').value = Encoder.htmlDecode(arrValores[6]);
                document.getElementById('<%=hfMarca.ClientID %>').value = Encoder.htmlDecode(arrValores[6]);
                document.getElementById('<%=txtModelo.ClientID %>').value = Encoder.htmlDecode(arrValores[7]);
                document.getElementById('<%=hfModelo.ClientID %>').value = Encoder.htmlDecode(arrValores[7]);
                document.getElementById('<%=txtAnio.ClientID %>').value = Encoder.htmlDecode(arrValores[8]);
                document.getElementById('<%=hfAnio.ClientID %>').value = Encoder.htmlDecode(arrValores[8]);

                //Dato Vehiculo
                if (arrValores.length > 9) {
                    document.getElementById('<%=txtNroPlaca.ClientID %>').value = Encoder.htmlDecode(arrValores[9]);
                    document.getElementById('<%=ddlTipCarroceria.ClientID %>').value = Encoder.htmlDecode(arrValores[10]);
                    document.getElementById('<%=hfCodColor.ClientID %>').value = Encoder.htmlDecode(arrValores[11]);
                    document.getElementById('<%=txtColor.ClientID %>').value = Encoder.htmlDecode(arrValores[12]);
                    document.getElementById('<%=hfDesColor.ClientID %>').value = Encoder.htmlDecode(arrValores[12]);
                    document.getElementById('<%=hfDesColor.ClientID %>').value = Encoder.htmlDecode(arrValores[12]);
                    document.getElementById('<%=txtNroPasjeros.ClientID %>').value = Encoder.htmlDecode(arrValores[13]);
                    document.getElementById('<%=txtNroMotor.ClientID %>').value = Encoder.htmlDecode(arrValores[14]);
                    document.getElementById('<%=ddlTipCombustible.ClientID %>').value = Encoder.htmlDecode(arrValores[15]);
                    document.getElementById('<%=ddlTipMercancia.ClientID %>').value = Encoder.htmlDecode(arrValores[16]);
                    document.getElementById('<%=txtNroChasis.ClientID %>').value = Encoder.htmlDecode(arrValores[17]);
                    document.getElementById('<%=txtObsVehiculo.ClientID %>').value = Encoder.htmlDecode(arrValores[18]);
                    document.getElementById('<%=hfTipoEvento.ClientID %>').value = "Actualizar";
                }
                else {
                    document.getElementById('<%=hfTipoEvento.ClientID %>').value = "Registrar";
                    alert('No existe informaci�n registrada del veh�culo.');
                }
                document.getElementById('<%=btnRefresh.ClientID %>').click();
            }
            else {
                this.LimpiarForm();
                alert('No se encontr� la cotizaci�n.');
            }
        }
        function OnErrorCotizacion(result) {
            this.LimpiarForm();
            alert('No se encontr� la cotizaci�n.');
        }  
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div id="divSeq" runat="server">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td class="Form_tdBorde">
                </td>
                <td class="Seq_Girs" style="height: 15px;">
                    Cotizaci�n
                </td>
                <td class="Seq_Red" style="height: 15px;">
                    Informaci�n del Veh�culo
                </td>
                <td class="Seq_Girs" style="height: 15px;">
                    Registro de Certifiado
                </td>
                <td class="Form_tdBorde" style="height: 15px;">
                </td>
            </tr>
        </table>
    </div>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td colspan="8" class="Form_SubTitulo">
                            COTIZACI�N
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer" style="width: 80px;">
                            Cotizaci�n: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtNroCotizacion" runat="server" CssClass="Form_TextBoxDisable"
                                Width="100px"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="ftxtNroCotizacion" runat="server" TargetControlID="txtNroCotizacion"
                                ValidChars="0123456789">
                            </cc1:FilteredTextBoxExtender>
                            <asp:RequiredFieldValidator ID="rfvNroCotizacion" runat="server" ControlToValidate="txtNroCotizacion"
                                ErrorMessage="Nro. Cotizacion obligatorio" Text="*" Display="None" ValidationGroup="DatoVehiculo">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Plan:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtPlan" runat="server" CssClass="Form_TextBoxDisable" Width="100px"
                                ReadOnly="true"></asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            Cliente:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtCliente" runat="server" CssClass="Form_TextBoxDisable" Width="220px"
                                ReadOnly="true"></asp:TextBox>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer" style="width: 80px;">
                            Clase:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtClase" runat="server" CssClass="Form_TextBoxDisable" Width="100px"
                                ReadOnly="true"></asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            Marca:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtMarca" runat="server" CssClass="Form_TextBoxDisable" Width="100px"
                                ReadOnly="true"></asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            Modelo:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtModelo" runat="server" CssClass="Form_TextBoxDisable" Width="220px"
                                ReadOnly="true"></asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            A�o:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtAnio" runat="server" CssClass="Form_TextBoxDisable" Width="60px"
                                ReadOnly="true"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <asp:Panel ID="pnDatoVehiculo" runat="server" DefaultButton="btnGrabDatVehiculo">
                    <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td colspan="8" class="Form_SubTitulo">
                                DATOS DEL VEH�CULO
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer" style="width: 80px;">
                                Nro. Placa: <span class="Form_TextoObligatorio">*</span>
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:TextBox ID="txtNroPlaca" runat="server" CssClass="Form_TextBox" Width="100px"
                                    MaxLength="15">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvNroPlaca" runat="server" ControlToValidate="txtNroPlaca"
                                    SetFocusOnError="true" Text="*" Display="None" ErrorMessage="Nro. placa obligatorio."
                                    ValidationGroup="DatoVehiculo">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="fteNroPlaca" runat="server" TargetControlID="txtNroPlaca"
                                    ValidChars="ABCDEFGHIJKMN�OPQRSTUVWXYZabcdefghijkmn�opqrstuvwxyz -0123456789">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td class="Form_TextoDer">
                                Carrocer�a: <span class="Form_TextoObligatorio">*</span>
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:DropDownList ID="ddlTipCarroceria" runat="server" Width="140px">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvTipCarroceria" runat="server" ControlToValidate="ddlTipCarroceria"
                                    SetFocusOnError="true" Text="*" Display="None" ErrorMessage="Tipo carroceria obligatorio."
                                    ValidationGroup="DatoVehiculo" InitialValue="0">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td class="Form_TextoDer">
                                Color: <span class="Form_TextoObligatorio">*</span>
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:TextBox ID="txtColor" runat="server" CssClass="Form_TextBoxCod" Width="125px"
                                    ReadOnly="true" onkeyup="showPopupKey('bmpeColor','ctl00_ContentPlaceHolder1_ddlColorBase');">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvCodColor" runat="server" ControlToValidate="txtColor"
                                    Display="None" ErrorMessage="Color obligatorio." Text="*" ValidationGroup="DatoVehiculo"></asp:RequiredFieldValidator>
                            </td>
                            <td class="Form_TextoDer">
                                Nro. Pasajeros: <span class="Form_TextoObligatorio">*</span>
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:TextBox ID="txtNroPasjeros" runat="server" CssClass="Form_TextBox" Width="60px"
                                    MaxLength="2">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvNroPasjeros" runat="server" ControlToValidate="txtNroPasjeros"
                                    SetFocusOnError="true" Text="*" Display="None" ErrorMessage="Nro. pasajeros obligatorio."
                                    ValidationGroup="DatoVehiculo">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="fteNroCarroceria" runat="server" TargetControlID="txtNroPasjeros"
                                    ValidChars="0123456789">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer">
                                Nro. Motor: <span class="Form_TextoObligatorio">*</span>
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:TextBox ID="txtNroMotor" runat="server" CssClass="Form_TextBox" Width="100px"
                                    MaxLength="15">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvNroMotor" runat="server" ControlToValidate="txtNroMotor"
                                    SetFocusOnError="true" Text="*" Display="None" ErrorMessage="Nro. motor obligatorio."
                                    ValidationGroup="DatoVehiculo">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td class="Form_TextoDer">
                                Combustible: <span class="Form_TextoObligatorio">*</span>
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:DropDownList ID="ddlTipCombustible" runat="server" Width="140px">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvTipCombustible" runat="server" ControlToValidate="ddlTipCombustible"
                                    SetFocusOnError="true" Text="*" Display="None" ErrorMessage="Tipo conbustible obligatorio."
                                    ValidationGroup="DatoVehiculo" InitialValue="0">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td class="Form_TextoDer">
                                Mercancia: <span class="Form_TextoObligatorio">*</span>
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:DropDownList ID="ddlTipMercancia" runat="server" Width="130px">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvTipMercancia" runat="server" ControlToValidate="ddlTipMercancia"
                                    SetFocusOnError="true" Text="*" Display="None" ErrorMessage="Tipo mercancia obligatorio."
                                    ValidationGroup="DatoVehiculo" InitialValue="0">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td class="Form_TextoDer">
                                Nro. Chasis:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:TextBox ID="txtNroChasis" runat="server" CssClass="Form_TextBox" Width="60px"
                                    MaxLength="15">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer" style="vertical-align: top">
                                Observaciones:
                            </td>
                            <td class="Form_TextoIzq" colspan="5" rowspan="2" style="vertical-align: top">
                                <asp:TextBox ID="txtObsVehiculo" runat="server" CssClass="Form_TextBox" TextMode="MultiLine"
                                    Width="350px" Height="25px" MaxLength="500"></asp:TextBox>
                            </td>
                            <td class="Form_TextoDer">
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:Button ID="btnGrabDatVehiculo" runat="server" Text="Grabar" ValidationGroup="DatoVehiculo"
                                    OnClick="btnGrabDatVehiculo_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnAgrAccesorio">                
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="Form_SubTitulo">
                            INFORMACI�N DE ACCESORIOS
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td class="Form_TextoDer" style="width: 80px;">
                                        Tipo: <span class="Form_TextoObligatorio">*</span>
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:DropDownList ID="ddlTipAccesorio" runat="server" Width="140px">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvTipAccesorio" runat="server" ControlToValidate="ddlTipAccesorio"
                                            SetFocusOnError="true" Text="*" Display="None" ErrorMessage="Tipo accesorio obligatorio."
                                            ValidationGroup="DatoAccesorio" InitialValue="0">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Valor $: <span class="Form_TextoObligatorio">*</span>
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtValAccesorio" runat="server" CssClass="Form_TextBoxMonto" Width="70px"
                                            MaxLength="4" onBlur="this.value=FormatMoney(this.value);"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="fteValAccesorio" runat="server" TargetControlID="txtValAccesorio"
                                            ValidChars="0123456789.,">
                                        </cc1:FilteredTextBoxExtender>
                                        <asp:RequiredFieldValidator ID="rfvValAccesorio" runat="server" ControlToValidate="txtValAccesorio"
                                            SetFocusOnError="true" Text="*" Display="None" ErrorMessage="Valor accesorio obligatorio."
                                            ValidationGroup="DatoAccesorio">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Descripci�n:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtObsAccesorio" runat="server" Width="260px" CssClass="Form_TextBox"
                                            MaxLength="300"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="Form_TextoDer">
                                        <asp:Button ID="btnAgrAccesorio" runat="server" Text="Agregar" OnClick="btnAgrAccesorio_Click"
                                            ValidationGroup="DatoAccesorio" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                </asp:Panel>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <div style="width: 100%; height: 100px; text-align: center; overflow: auto;">
                    <asp:GridView ID="gvDetAccesorio" runat="server" SkinID="sknGridView" Width="95%"
                        OnRowDataBound="gvDetAccesorio_RowDataBound" OnRowCommand="gvDetAccesorio_RowCommand">
                        <Columns>
                            <asp:BoundField DataField="DesAccesorio" HeaderText="Accesorio">
                                <HeaderStyle HorizontalAlign="Center" Width="300px" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ValorAccesorio" HeaderText="Valor">
                                <HeaderStyle HorizontalAlign="Center" Width="150px" />
                                <ItemStyle HorizontalAlign="Right" Width="150px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="IdAccesorio" HeaderText="idAccesorio">
                                <HeaderStyle CssClass="Form_ObjetoOculto" />
                                <ItemStyle CssClass="Form_ObjetoOculto" />
                            </asp:BoundField>
                            <asp:BoundField DataField="IdCotizacion" HeaderText="IdCotizacion">
                                <HeaderStyle CssClass="Form_ObjetoOculto" />
                                <ItemStyle CssClass="Form_ObjetoOculto" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Quitar">
                                <ItemTemplate>
                                    <asp:ImageButton ID="ibtnAnular" runat="server" SkinID="sknIbtnIcoAnular" CommandName="Quitar" />
                                </ItemTemplate>
                                <HeaderStyle Width="50px" />
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <div class="Form_RegButtonDer">
                    <asp:Button ID="btnContinuar" runat="server" Text="Continuar" OnClick="btnContinuar_Click"
                        Enabled="false" />
                    <span class="Form_TextoObligatorio">&nbsp;(*)Campos Obligatorios</span>
                </div>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <div style="display: none;">
        <asp:HiddenField ID="hfTipoEvento" runat="server" />
        <asp:HiddenField ID="hfIdCotizacion" runat="server" />
        <asp:HiddenField ID="hfCodColor" runat="server" />
        <asp:HiddenField ID="hfDesColor" runat="server" />
        <asp:HiddenField ID="hfPlan" runat="server" />
        <asp:HiddenField ID="hfCliente" runat="server" />
        <asp:HiddenField ID="hfClase" runat="server" />
        <asp:HiddenField ID="hfMarca" runat="server" />
        <asp:HiddenField ID="hfModelo" runat="server" />
        <asp:HiddenField ID="hfAnio" runat="server" />
    </div>
    <uc1:MsgBox ID="msgBox" runat="server" />
    <asp:ValidationSummary ID="vsDatVehiculo" runat="server" ShowMessageBox="true" ShowSummary="false"
        ValidationGroup="DatoVehiculo" />
    <asp:ValidationSummary ID="vsDatAccesorio" runat="server" ShowMessageBox="true" ShowSummary="false"
        ValidationGroup="DatoAccesorio" />
    <asp:Button ID="btnRefresh" runat="server" Style="display: none;" OnClick="btnRefresh_Click" />
    <asp:Button ID="btnRefreshClear" runat="server" Style="display: none;" OnClick="btnRefreshClear_Click" />
    <asp:Panel ID="pnColor" runat="server" Width="500px" CssClass="Modal_Panel" Style="display: none;"
        DefaultButton="btnBusqColor">
        <div class="Modal_Head" style="width: 100%;">
            <div id="divHeadTitulo" style="float: left;">
                Motivos
            </div>
            <div id="divHeadCerrar" style="float: right;">
                <img alt="Click para cerrar." class="Modal_CloseButton" onclick="hideModalPopup('bmpeColor')"
                    style="cursor: hand;" />
            </div>
        </div>
        <div class="Modal_Body">
            <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="Form_TextoIzq">
                        Descripci�n:
                    </td>
                </tr>
                <tr>
                    <td class="Form_TextoIzq">
                        <asp:TextBox id="txtDescrip" runat="server" CssClass="Form_TextBox" TextMode="MultiLine" Width="450px" Height="200px">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="Form_TextoIzq">
                        <asp:Button id="btnGuardar" runat="server" Text="Guardar"  />
                    </td>
                </tr>
            </table>
        </div>
        
        <div class="Modal_Body" style="display:none;  ">
            <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="Form_TextoIzq">
                        Descripci�n:
                    </td>
                    <td class="Form_TextoIzq">
                        <asp:DropDownList ID="ddlColorBase" runat="server" Width="150px">
                        </asp:DropDownList>
                    </td>
                    <td class="Form_TextoIzq" style="width: 16px;">
                        <asp:UpdateProgress ID="upsBusqColor" runat="server" AssociatedUpdatePanelID="upBusqColor"
                            DisplayAfter="0">
                            <ProgressTemplate>
                                <asp:Image ID="imgLoadColor" runat="server" SkinID="sknImgLoading" Width="16px" Height="16px" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                    <td class="Form_TextoDer">
                        <asp:UpdatePanel ID="upBusqColor" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnBusqColor" runat="server" Text="Buscar" OnClick="btnBusqColor_Click"
                                    Width="80px" />
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnBusqColor" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    
                </tr>
            </table>
            <br />
            <div style="overflow: auto; height: 200px; display:none; " >
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="gvBusqColor" runat="server" SkinID="sknGridView" AutoGenerateColumns="false"
                            Width="98%" OnRowDataBound="gvBusqColor_RowDataBound">
                            <Columns>
                                <asp:BoundField DataField="IdColorOpcion" HeaderText="C�digo">
                                    <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Descripcion" HeaderText="Color">
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="gvBusqColor" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <br />
        </div>
    </asp:Panel>
    <asp:Label ID="lblPopColor" runat="server" Style="display: none;"></asp:Label>
    <cc1:ModalPopupExtender ID="mpeColor" runat="server" Enabled="true" TargetControlID="lblPopColor"
        BehaviorID="bmpeColor" PopupControlID="pnColor" BackgroundCssClass="Modal_Background">
    </cc1:ModalPopupExtender>
</asp:Content>
