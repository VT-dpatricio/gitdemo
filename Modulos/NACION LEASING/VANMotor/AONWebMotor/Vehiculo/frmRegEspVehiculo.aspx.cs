﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Resources;
using AONAffinity.Business.Logic;
using AONAffinity.Business.Entity.BDJanus;
using AONAffinity.Business.Entity.BDMotor;

namespace AONWebMotor.Vehiculo
{
    public partial class frmRegParVehiculo : PageBase
    {
        #region Variables
        /// <summary>
        /// Variable de registro activo.
        /// </summary>
        private Boolean bRegActivo = true;

        /// <summary>
        /// Valor por defecto de la mercancia.
        /// </summary>
        private Int32 nIdMercancia = 3;
        #endregion
        
        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */
            try 
            {
                if (!Page.IsPostBack)
                {           
                    this.CargarTipoCarroceria();
                    this.CargarTipoConbustible();
                    this.CargarTipoMercancia();
                    this.CargarTipoAccesorio();
                    this.CargarColor();
                    this.CargarCabecera();
                    this.CargarCabeceraGvBusqColor();
                    this.CargarFuncionesJS();                    

                    if (!String.IsNullOrEmpty(Request.QueryString["idCot"]))
                    {
                        this.hfTipoEvento.Value = Constantes.EventoRegistrar; 
                        this.hfIdCotizacion.Value = Request.QueryString["idCot"];
                        this.txtNroCotizacion.ReadOnly = true;
                        this.txtNroCotizacion.CssClass = "Form_TextBoxDisable";
                        this.CargarDatoCotizacion(Convert.ToDecimal(Request.QueryString["idCot"]));
                        this.HabilitaDatoAccesorio(false);
                    }
                    else 
                    {
                        this.divSeq.Visible = false;
                        this.txtNroCotizacion.CssClass = "Form_TextBoxCod";
                        this.btnContinuar.Visible = false;
                        this.txtNroCotizacion.Attributes.Add("onkeypress", "return CotizacionAsincrono(event);");
                        this.HabilitaDatoAccesorio(true);
                    }
                }
                else
                {
                    this.txtColor.Text = this.hfDesColor.Value;                    
                }
            }
            catch (Exception ex) 
            {
                this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, Constantes.MsgBoxAlto, Constantes.MsgBoxAncho);
            }            
        }
        
        protected void btnGrabDatVehiculo_Click(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try
            {
                //Validar si existe nro de cotizacion ingresada.
                if (this.hfTipoEvento.Value == String.Empty) 
                {
                    this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Warning, "Nro. de cotización no existe.", Constantes.MsgBoxAlto, Constantes.MsgBoxAncho);
                    return;
                }

                BEDatoVehiculo objBEDatoVehiculo = new BEDatoVehiculo();
                BLDatoVehiculo objBLDatoVehiculo = new BLDatoVehiculo();

                //Registra información del véhículo. 
                if (this.hfTipoEvento.Value == Constantes.EventoRegistrar) 
                {                    
                    objBEDatoVehiculo.IdCotizacion = Convert.ToDecimal(this.hfIdCotizacion.Value);                    
                    objBEDatoVehiculo.IdCotizacion = Convert.ToDecimal(this.txtNroCotizacion.Text.Trim());                 
                    objBEDatoVehiculo.NroPlaca = this.txtNroPlaca.Text.Trim().ToUpper();
                    objBEDatoVehiculo.NroMotor = this.txtNroMotor.Text.Trim().ToUpper();
                    objBEDatoVehiculo.NroChasis = this.txtNroChasis.Text.Trim().ToUpper();
                    objBEDatoVehiculo.IdCarroceria = Convert.ToInt32(this.ddlTipCarroceria.SelectedValue);
                    objBEDatoVehiculo.IdCombustible = Convert.ToInt32(this.ddlTipCombustible.SelectedValue);
                    objBEDatoVehiculo.IdMercancia = Convert.ToInt32(this.ddlTipMercancia.SelectedValue);
                    objBEDatoVehiculo.NroCarroceria = String.Empty; 
                    objBEDatoVehiculo.IdColorOpcion = Convert.ToInt32(this.hfCodColor.Value);
                    objBEDatoVehiculo.NroPasajero = Convert.ToInt32(this.txtNroPasjeros.Text.Trim());
                    objBEDatoVehiculo.Observacion = this.txtObsVehiculo.Text.Trim().ToUpper();
                    objBEDatoVehiculo.UsuarioCreacion = Session[NombreSession.Usuario].ToString();

                    if (objBLDatoVehiculo.Insertar(objBEDatoVehiculo) > 0)
                    {
                        this.hfTipoEvento.Value = Constantes.EventoActualizar;                        
                        this.btnContinuar.Enabled = true;
                        this.HabilitaDatoAccesorio(true);
                        this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Success, "Se registró la información del vehículo.", Constantes.MsgBoxAlto, Constantes.MsgBoxAncho);
                        this.txtNroCotizacion.Text = this.hfIdCotizacion.Value; 
                    }
                    else 
                    {
                        this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, "No se pudo registrar la información del vehículo.", Constantes.MsgBoxAlto, Constantes.MsgBoxAncho);
                    }
                }
                //Actualiza información del véhículo. 
                else 
                {
                    objBEDatoVehiculo.IdCotizacion = Convert.ToDecimal(this.hfIdCotizacion.Value);
                    objBEDatoVehiculo.NroPlaca = this.txtNroPlaca.Text.Trim().ToUpper();
                    objBEDatoVehiculo.NroMotor = this.txtNroMotor.Text.Trim().ToUpper();
                    objBEDatoVehiculo.NroChasis = this.txtNroChasis.Text.Trim().ToUpper();
                    objBEDatoVehiculo.IdCarroceria = Convert.ToInt32(this.ddlTipCarroceria.SelectedValue);
                    objBEDatoVehiculo.IdCombustible = Convert.ToInt32(this.ddlTipCombustible.SelectedValue);
                    objBEDatoVehiculo.IdMercancia = Convert.ToInt32(this.ddlTipMercancia.SelectedValue);
                    objBEDatoVehiculo.IdColorOpcion = Convert.ToInt32(this.hfCodColor.Value);
                    objBEDatoVehiculo.NroPasajero = Convert.ToInt32(this.txtNroPasjeros.Text.Trim());
                    objBEDatoVehiculo.Observacion = this.txtObsVehiculo.Text.Trim().ToUpper();
                    objBEDatoVehiculo.UsuarioModificacion = Session[NombreSession.Usuario].ToString();

                    if (objBLDatoVehiculo.Actualizar(objBEDatoVehiculo) > 0) 
                    {
                        this.txtNroCotizacion.Text = this.hfIdCotizacion.Value;
                        this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Success, "Se actualizó la información del vehículo.", Constantes.MsgBoxAlto, Constantes.MsgBoxAncho);                        
                    }
                    else
                    {
                        this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, "No se pudo actualizar la información del vehículo.", Constantes.MsgBoxAlto, Constantes.MsgBoxAncho);
                    }
                }               
            }
            catch (Exception ex)
            {
                this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, Constantes.MsgBoxAlto, Constantes.MsgBoxAncho);
            }
        }

        protected void btnAgrAccesorio_Click(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try
            {
                //Validar si existe nro de cotizacion ingresada.
                if (this.hfTipoEvento.Value == String.Empty)
                {
                    this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Warning, "Nro. de cotización no existe.", Constantes.MsgBoxAlto, Constantes.MsgBoxAncho);
                    return;
                }

                BLAccesorioVehiculo objBLAccesorioVehiculo = new BLAccesorioVehiculo();
                BEAccesorioVehiculo objBEAccesorioVehiculo = new BEAccesorioVehiculo();

                if (objBLAccesorioVehiculo.Obtener(Convert.ToDecimal(this.hfIdCotizacion.Value), Convert.ToInt32(this.ddlTipAccesorio.SelectedValue)) != null)
                {
                    objBEAccesorioVehiculo.IdCotizacion = Convert.ToDecimal(this.hfIdCotizacion.Value);
                    objBEAccesorioVehiculo.IdAccesorio = Convert.ToInt32(this.ddlTipAccesorio.SelectedValue);
                    if (this.txtValAccesorio.Text != String.Empty) { objBEAccesorioVehiculo.ValorAccesorio = Convert.ToDecimal(this.txtValAccesorio.Text); } else { objBEAccesorioVehiculo.ValorAccesorio = NullTypes.DecimalNull; }
                    objBEAccesorioVehiculo.Descripcion = this.txtObsAccesorio.Text.Trim().ToUpper();
                    objBEAccesorioVehiculo.UsuarioModificacion = Session[NombreSession.Usuario].ToString();
                    objBEAccesorioVehiculo.EstadoRegistro = true;

                    if (objBLAccesorioVehiculo.Actualizar(objBEAccesorioVehiculo) > 0)
                    {
                        this.CargarDatoAccesorio(Convert.ToDecimal(this.hfIdCotizacion.Value));
                        this.LimpiarAccesorio();
                        this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Success, "Se agregó la información adicional del vehículo.", Constantes.MsgBoxAlto, Constantes.MsgBoxAncho);
                    }
                    else 
                    {
                        this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, "No se pudo agregar la información adicional del vehículo.", Constantes.MsgBoxAlto, Constantes.MsgBoxAncho);
                    }
                }
                else
                {
                    objBEAccesorioVehiculo.IdCotizacion = Convert.ToDecimal(this.hfIdCotizacion.Value);
                    objBEAccesorioVehiculo.IdAccesorio = Convert.ToInt32(this.ddlTipAccesorio.SelectedValue);
                    if (this.txtValAccesorio.Text != String.Empty) { objBEAccesorioVehiculo.ValorAccesorio = Convert.ToDecimal(this.txtValAccesorio.Text); } else { objBEAccesorioVehiculo.ValorAccesorio = NullTypes.DecimalNull; }
                    objBEAccesorioVehiculo.Descripcion = this.txtObsAccesorio.Text.Trim().ToUpper();
                    objBEAccesorioVehiculo.UsuarioCreacion = Session[NombreSession.Usuario].ToString();

                    if (objBLAccesorioVehiculo.Insertar(objBEAccesorioVehiculo) > 0)
                    {
                        this.CargarDatoAccesorio(Convert.ToDecimal(this.hfIdCotizacion.Value));
                        this.LimpiarAccesorio();
                        this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Success, "Se agregó la información adicional del vehículo.", Constantes.MsgBoxAlto, Constantes.MsgBoxAncho);                        
                    }
                    else 
                    {
                        this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, "No se pudo agregar la información adicional del vehículo.", Constantes.MsgBoxAlto, Constantes.MsgBoxAncho);
                    }
                }
            }
            catch (Exception ex)
            {
                this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, Constantes.MsgBoxAlto, Constantes.MsgBoxAncho);
            }
        }

        protected void gvDetAccesorio_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-04-14
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try 
            {
                if (e.Row.RowType == DataControlRowType.DataRow) 
                {                    
                    ImageButton ibtnAnular = (ImageButton)e.Row.FindControl("ibtnAnular");
                    BEAccesorioVehiculo objBEAccesorioVehiculo = (BEAccesorioVehiculo)(e.Row.DataItem); 
                        
                    if (ibtnAnular != null) 
                    {
                        ibtnAnular.CommandArgument = objBEAccesorioVehiculo.IdCotizacion + "@" + objBEAccesorioVehiculo.IdAccesorio;
                        ibtnAnular.Attributes.Add("onClick", "javascript:if(confirm('¿Está seguro de quitar el accesorio?')== false) return false;");
                    }

                    e.Row.Attributes.Add("OnMouseOut", "this.className = this.orignalclassName;");
                    e.Row.Attributes.Add("OnMouseOver", "this.orignalclassName = this.className;this.className = 'GridWiew_SelectedRow';");
                    e.Row.Attributes.Add("onclick", "javascript:OnAccesorioSelected('" + e.Row.Cells[3].Text + "','" + e.Row.Cells[1].Text + "','" + e.Row.Cells[2].Text + "')");
                }                
            }
            catch(Exception ex)
            {
                this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, Constantes.MsgBoxAlto, Constantes.MsgBoxAncho);
            } 
        }

        protected void gvDetAccesorio_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try 
            {
                switch (e.CommandName) 
                { 
                    case "Quitar":
                        String[] cValores = e.CommandArgument.ToString().Split('@');
                        BEAccesorioVehiculo objBEAccesorioVehiculo = new BEAccesorioVehiculo();
                        objBEAccesorioVehiculo.IdCotizacion = Convert.ToDecimal(cValores[0]);
                        objBEAccesorioVehiculo.IdAccesorio = Convert.ToInt32(cValores[1]);
                        objBEAccesorioVehiculo.UsuarioModificacion = Session[NombreSession.Usuario].ToString();
                        objBEAccesorioVehiculo.EstadoRegistro = false; 

                        BLAccesorioVehiculo objBLAccesorioVehiculo = new BLAccesorioVehiculo();
                        if (objBLAccesorioVehiculo.ActualizarEstado(objBEAccesorioVehiculo) > 0)
                        {
                            this.LimpiarAccesorio();
                            this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Success, "La información del accseorio del vehículo fue quitada.", Constantes.MsgBoxAlto, Constantes.MsgBoxAncho);
                            this.CargarDatoAccesorio(Convert.ToDecimal(this.hfIdCotizacion.Value));
                        }
                        else 
                        {
                            this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, "No se pudo quitar al información del accessorio del vehículo.", Constantes.MsgBoxAlto, Constantes.MsgBoxAncho);
                        }

                        break;
                }
            }
            catch (Exception ex) 
            {
                this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, Constantes.MsgBoxAlto, Constantes.MsgBoxAncho);                
            }
        }

        protected void btnContinuar_Click(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try 
            {
                Response.Redirect("../" + UrlPagina.CertificadoRegistro.Substring(2) + "?idCot=" + Request.QueryString["idCot"]);
            }
            catch (Exception ex) 
            {
                this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, Constantes.MsgBoxAlto, Constantes.MsgBoxAncho);                
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try
            {
                if (this.txtNroCotizacion.Text != String.Empty) 
                {
                    this.CargarDatoAccesorio(Convert.ToDecimal(this.txtNroCotizacion.Text.Trim()));
                    this.CargarCabecera();
                }

                this.txtCliente.Text = this.hfCliente.Value;
                this.txtPlan.Text = this.hfPlan.Value;
                this.txtClase.Text = this.hfClase.Value;
                this.txtMarca.Text = this.hfMarca.Value;
                this.txtModelo.Text = this.hfModelo.Value;
                this.txtAnio.Text = this.hfAnio.Value;
                this.txtNroCotizacion.Focus();                 
            }
            catch (Exception ex) 
            {
                this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, Constantes.MsgBoxAlto, Constantes.MsgBoxAncho);                
            }
        }

        protected void btnRefreshClear_Click(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            this.txtPlan.Text = String.Empty;
            this.txtCliente.Text = String.Empty;
            this.txtClase.Text = String.Empty;
            this.txtMarca.Text = String.Empty;
            this.txtModelo.Text = String.Empty;
            this.txtAnio.Text = String.Empty;
  
            this.gvDetAccesorio.DataSource = null;
            this.gvDetAccesorio.DataBind();
            this.CargarCabecera();
        }
        #endregion        

        #region Metodos Privados
        /// <summary>
        /// Permite cargar el tipo de carroceria.
        /// </summary>
        private void CargarTipoCarroceria() 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-11
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLCarroceria objBLCarroceria = new BLCarroceria();
            this.CargarDropDownList(this.ddlTipCarroceria, "IdCarroceria", "Descripcion", objBLCarroceria.Listar(bRegActivo), true);               
        }

        /// <summary>
        /// Permite cargar el tipo de combustible.
        /// </summary>
        private void CargarTipoConbustible() 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-11
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLConbustible objBLConbustible = new BLConbustible();
            this.CargarDropDownList(this.ddlTipCombustible, "IdCombustible", "Descripcion", objBLConbustible.Listar(this.bRegActivo), true);                       
        }

        /// <summary>
        /// Permite cargar el tipo de mercancia.
        /// </summary>
        private void CargarTipoMercancia() 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-11
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLMercancia objBLMercancia = new BLMercancia();
            this.CargarDropDownList(this.ddlTipMercancia, "IdMercancia", "Descripcion", objBLMercancia.Listar(this.bRegActivo), false);

            if (this.ddlTipMercancia.Items.Count > 0) 
            {
                this.ddlTipMercancia.SelectedValue = this.nIdMercancia.ToString();
            }
        }

        /// <summary>
        /// Permite cargar el tipo de accesorio.
        /// </summary>
        private void CargarTipoAccesorio() 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-11
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLAccesorio objBLAccesorio = new BLAccesorio();
            this.CargarDropDownList(this.ddlTipAccesorio, "IdAccesorio", "Descripcion", objBLAccesorio.Listar(this.bRegActivo), true);    
        }

        /// <summary>
        /// Permite cargar el color.
        /// </summary>
        private void CargarColor() 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:                
             */

            BLColor objBLColor = new BLColor();            
            this.CargarDropDownList(this.ddlColorBase, "IdColor", "Descripcion", objBLColor.Listar(this.bRegActivo), true);  
        }

        /// <summary>
        /// Permite cargar la cabecera del GridView accesorio.
        /// </summary>
        private void CargarCabecera()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            if (this.gvDetAccesorio.Rows.Count == 0) 
            {
                BEAccesorioVehiculo objBEAccesorioVehiculo = new BEAccesorioVehiculo();
                List<BEAccesorioVehiculo> lstBEAccesorioVehiculo = new List<BEAccesorioVehiculo>();
                lstBEAccesorioVehiculo.Add(objBEAccesorioVehiculo);
                this.gvDetAccesorio.DataSource = lstBEAccesorioVehiculo;
                this.gvDetAccesorio.DataBind();
                this.gvDetAccesorio.Rows[0].Visible = false;  
            }
        }        

        /// <summary>
        /// Permite cargar funciones JS en los controles.
        /// </summary>
        private void CargarFuncionesJS()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            //this.btnGrabDatVehiculo.Attributes.Add("OnClick", "javascript:if(confirm('" + Constantes.MsjRegistrar + "')== false) return false;");
            //this.btnAgrAccesorio.Attributes.Add("OnClick", "javascript:if(confirm('" + Constantes.MsjAgregar + "')== false) return false;");
        }

        /// <summary>
        /// Permite cargar la información de la cotización
        /// </summary>
        private void CargarDatoCotizacion(Decimal pnIdCotizacion) 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-14
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLCotizacion objBLCotizacion = new BLCotizacion();
            BECotizacion objBECotizacion = null; // objBLCotizacion.Obtener(pnIdCotizacion);

            if (objBECotizacion != null) 
            {
                //this.txtNroCotizacion.Text = objBECotizacion.IdCotizacion.ToString();
                //this.txtPlan.Text = objBECotizacion.IdPlan;
                this.txtCliente.Text = objBECotizacion.PriNombre + " " + objBECotizacion.SegNombre + " " + objBECotizacion.ApePaterno + " " + objBECotizacion.ApeMaterno;       
                this.txtClase.Text = objBECotizacion.DesClase;
                this.txtMarca.Text = objBECotizacion.DesMarca;
                this.txtModelo.Text = objBECotizacion.DesModelo;
                //this.txtAnio.Text = objBECotizacion.IdAnio.ToString();
            }
        }

        /// <summary>
        /// Permite cargar la información de los accesorios 
        /// del vehículo.
        /// </summary>
        private void CargarDatoAccesorio(Decimal pnIdCotizacion) 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-14
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLAccesorioVehiculo objBLAccesorioVehiculo = new BLAccesorioVehiculo();
            this.gvDetAccesorio.DataSource = objBLAccesorioVehiculo.Listar(pnIdCotizacion, this.bRegActivo);
            this.gvDetAccesorio.DataBind();
        }

        /// <summary>
        /// Permite cambiar el estado habilitado o 
        /// desabilitado de los controles de la información 
        /// adicional del vehículo.
        /// </summary>
        /// <param name="pbEstado">Estado del control.</param>
        private void HabilitaDatoAccesorio(Boolean pbEstado) 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-14
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            this.ddlTipAccesorio.Enabled = pbEstado;
            this.txtValAccesorio.Enabled = pbEstado;
            this.txtObsAccesorio.Enabled = pbEstado;
            this.btnAgrAccesorio.Enabled = pbEstado;
        }

        /// <summary>
        /// Permite limpiar loas controles del accsesorio del vehículo.
        /// </summary>
        private void LimpiarAccesorio() 
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            this.ddlTipAccesorio.SelectedValue = "0";
            this.txtValAccesorio.Text = String.Empty;
            this.txtObsAccesorio.Text = String.Empty;
        }
        #endregion      

        #region Eventos Popup Color
        protected void btnBusqColor_Click(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try 
            {
                if(this.ddlColorBase.SelectedValue != "0" )
                {
                    BLColorOpcion objBLColorOpcion = new BLColorOpcion();
                    this.gvBusqColor.DataSource = objBLColorOpcion.Listar(Convert.ToInt32(this.ddlColorBase.SelectedValue), this.bRegActivo);
                    this.gvBusqColor.DataBind(); 
                }                
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void gvBusqColor_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-14
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow) 
                {
                    e.Row.Attributes.Add("OnMouseOut", "this.className = this.orignalclassName;");
                    e.Row.Attributes.Add("OnMouseOver", "this.orignalclassName = this.className;this.className = 'GridWiew_SelectedRow';");
                    e.Row.Attributes.Add("onclick", "javascript:OnColorSelected('" + e.Row.Cells[0].Text + "','" + e.Row.Cells[1].Text + "')");
                }
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }
        #endregion

        #region Metodos Privados Popup Color
        /// <summary>
        /// Permite cargar la cabera el el GridView color.
        /// </summary>
        public void CargarCabeceraGvBusqColor()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            if (this.gvBusqColor.Rows.Count == 0)
            {
                BEColorOpcion objBEColorOpcion = new BEColorOpcion();
                List<BEColorOpcion> lstBEColorOpcion = new List<BEColorOpcion>();
                lstBEColorOpcion.Add(objBEColorOpcion);
                this.gvBusqColor.DataSource = lstBEColorOpcion;
                this.gvBusqColor.DataBind();
                this.gvBusqColor.Rows[0].Visible = false;
            }
        }
        #endregion                

        #region WebMethods
        [System.Web.Services.WebMethod]
        public static String ObtenerCotizacion(Decimal pcIdCotizacion)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-09
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */
            String cResult = "";
            BLCotizacion objBLCotizacion = new BLCotizacion();
            BECotizacion objBECotizacion = null; // objBLCotizacion.Obtener(pcIdCotizacion);

            if (objBECotizacion != null)
            {
                //cResult = objBECotizacion.IdCotizacion + Constantes.SeparadorText;
                cResult = cResult + objBECotizacion.IdTipoDocumento + Constantes.SeparadorText;
                cResult = cResult + objBECotizacion.NroDocumento + Constantes.SeparadorText;                                
                cResult = cResult + objBECotizacion.PriNombre + " " + objBECotizacion.SegNombre + " " + objBECotizacion.ApePaterno  + " " + objBECotizacion.ApeMaterno + Constantes.SeparadorText;
                //cResult = cResult + objBECotizacion.IdPlan + Constantes.SeparadorText;
                cResult = cResult + objBECotizacion.DesClase + Constantes.SeparadorText;
                cResult = cResult + objBECotizacion.DesMarca + Constantes.SeparadorText;
                cResult = cResult + objBECotizacion.DesModelo + Constantes.SeparadorText;
                //cResult = cResult + objBECotizacion.IdAnio;

                BLDatoVehiculo objBLDatoVehiculo = new BLDatoVehiculo();
                BEDatoVehiculo objBEDatoVehiculo = objBLDatoVehiculo.Obtener(pcIdCotizacion);

                if (objBEDatoVehiculo != null)
                {
                    cResult = cResult + Constantes.SeparadorText;
                    cResult = cResult + objBEDatoVehiculo.NroPlaca + Constantes.SeparadorText;
                    cResult = cResult + objBEDatoVehiculo.IdCarroceria + Constantes.SeparadorText;
                    cResult = cResult + objBEDatoVehiculo.IdColorOpcion + Constantes.SeparadorText;
                    cResult = cResult + objBEDatoVehiculo.DesColorOpcion + Constantes.SeparadorText;
                    cResult = cResult + objBEDatoVehiculo.NroPasajero + Constantes.SeparadorText;
                    cResult = cResult + objBEDatoVehiculo.NroMotor + Constantes.SeparadorText;      
                    cResult = cResult + objBEDatoVehiculo.IdCombustible + Constantes.SeparadorText;
                    cResult = cResult + objBEDatoVehiculo.IdMercancia + Constantes.SeparadorText;
                    cResult = cResult + objBEDatoVehiculo.NroChasis + Constantes.SeparadorText;
                    cResult = cResult + objBEDatoVehiculo.Observacion;
                }
            }            

            return cResult;
        }
        #endregion        
    }
}
