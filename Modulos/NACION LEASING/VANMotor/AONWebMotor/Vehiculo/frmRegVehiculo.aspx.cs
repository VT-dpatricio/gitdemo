﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Resources;  

namespace AONWebMotor.Vehiculo
{
    public partial class frmRegVehiculo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAnterior_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("../" + UrlPagina.ClienteRegDato.Substring(2));
            }
            catch (Exception ex) 
            {
                throw ex;
            }
        }

        protected void btnSiguiente_Click(object sender, EventArgs e)
        {
            try 
            {
                Response.Redirect("../" + UrlPagina.VehiculoRegDatoEsp.Substring(2));
            }
            catch (Exception ex) 
            {
                throw ex;
            }
        }
    }
}
