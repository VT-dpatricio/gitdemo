﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessLogic;
     

namespace AONWebMotor.Modelo
{
    public partial class frmMantModelo : PageBase
    {
        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) 
            {
                this.CargarMarca();
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.BuscarModelo();
            }
            catch (Exception ex) 
            {
            }
        }
        #endregion

        #region Métodos Privados
        private void CargarMarca()
        {
            BLMarca objBLMarca = new BLMarca();
            this.CargarDropDownList(this.ddlMarca, "IdMarca", "Descripcion", objBLMarca.Listar(true), true);
        }

        private void BuscarModelo() 
        {
            if (this.ddlMarca.SelectedValue != "-1") 
            {
                BLModelo objBLModelo = new BLModelo();
                this.CargarGridView(this.gvModelo, objBLModelo.ListarxMarcaDesc(Convert.ToInt32(this.ddlMarca.SelectedValue), this.txtDescripcion.Text.Trim(), null)); 
            }            
        }
        #endregion

        
    }    
}
