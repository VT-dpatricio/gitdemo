﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Library.Resources;    
using AONAffinity.Motor.BusinessEntity;
using AONAffinity.Motor.BusinessLogic;  

namespace AONWebMotor.Maestro
{
    public partial class frmMatProducto : PageBase 
    {
        Int32 nIdProducto = 5292; 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) 
            {
                CargarMarcaxProducto();
            }
        }

        #region TB_ProductoMarca
        #region Métodos Privados
        private void CargarMarcaxProducto() 
        {
            BLProductoMarca objBLProductoMarca = new BLProductoMarca();
            this.CargarGridView(this.gvMarca, objBLProductoMarca.ListarxProducto(this.nIdProducto, null));    
        }

        private void LLenarGridMarca(GridViewRowEventArgs e) 
        {
            if (e.Row.RowType == DataControlRowType.DataRow) 
            {
                e.Row.Attributes.Add("OnMouseOut", "this.className = this.orignalclassName;");
                e.Row.Attributes.Add("OnMouseOver", "this.orignalclassName = this.className;this.className = 'GridWiew_SelectedRow';");

                BEProductoMarca objBEProductoMarca = (BEProductoMarca)(e.Row.DataItem);

                TextBox txtCodExterno = (TextBox)e.Row.FindControl("txtCodExterno");
                CheckBox cbxEstado = (CheckBox)e.Row.FindControl("cbxEstado");
                ImageButton ibtnEditar = (ImageButton)e.Row.FindControl("ibtnEditar");
                Label lblUsrMod = (Label)e.Row.FindControl("lblUsrMod");
                Label lblFecMod = (Label)e.Row.FindControl("lblFecMod");

                if (txtCodExterno != null) 
                {
                    txtCodExterno.Text = objBEProductoMarca.CodExterno;
                }

                if (cbxEstado != null) 
                {
                    if (objBEProductoMarca.EstadoRegistro == true) { cbxEstado.Checked = true; } else { cbxEstado.Checked = false; }
                }

                if (lblUsrMod != null) 
                {
                    if (objBEProductoMarca.UsuarioModificacion != NullTypes.CadenaNull) { lblUsrMod.Text = objBEProductoMarca.UsuarioModificacion; }
                }

                if (lblFecMod != null)
                {
                    if (objBEProductoMarca.FechaModificacion != NullTypes.FechaNull) { lblFecMod.Text = objBEProductoMarca.FechaModificacion.ToString(); } else { lblFecMod.Text = String.Empty; }
                }                
            }
        }

        private void ActualizarMarca() 
        {
            
        }
        #endregion

        #region Eventos
        protected void gvMarca_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                this.LLenarGridMarca(e);
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);   
            }
        }

        protected void gvMarca_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                this.gvMarca.PageIndex = e.NewPageIndex;
                this.CargarMarcaxProducto();
            }
            catch (Exception ex)
            {

            }
        }

        protected void gvMarca_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try 
            {
                switch (e.CommandName) 
                {
                    case "Editar":

                        break;

                }
            }
            catch (Exception ex) 
            {

            }
        }
        #endregion      

        

        
        #endregion
    }
}