﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AONWebMotor.Maestro
{
    public partial class frmMantMaestro : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.CargarFormulario();
        }

        private void CargarFormulario() 
        {
            this.MostrarH1();
        }

        private void MostrarH1() 
        {
            if (!String.IsNullOrEmpty(Request.QueryString["tipo"])) 
            {
                switch (Request.QueryString["tipo"]) 
                {
                    case "1":
                        this.lblH1.Text = "Mantenimiento de Marca";
                        break;
                    case "2":
                        this.lblH1.Text = "Mantenimiento de Modelo";
                        break;
                }
            }
        }
    }
}
