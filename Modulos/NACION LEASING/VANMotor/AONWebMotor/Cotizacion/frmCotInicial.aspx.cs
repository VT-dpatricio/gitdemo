﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using AONAffinity.Resources; 
using AONAffinity.Business.Logic;
using AONAffinity.Business.Entity; 
using AONAffinity.Business.Entity.BDMotor;
using AONAffinity.Business.Entity.BDJanus;  

namespace AONWebMotor.Cotizacion
{
    public partial class frmCotInicial : PageBase 
    {
        #region Variables Privadas
        /// <summary>
        /// Variable que indica los registros
        /// de estado activo.
        /// </summary>
        private Boolean bEstadoReg = true;
        
        /// <summary>
        ///Variable de codigo de departamento por
        ///defecto Lima.
        /// </summary>
        private Int32 nCodDepartamento = 5115;

        /// <summary>
        /// Variable de codigo de tipo de documento
        /// por defecto DNI.
        /// </summary>
        private String cCodDocIdentidad = "L";
        #endregion        

        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try
            {
                if (!Page.IsPostBack)
                {
                    Page.Response.Cache.SetCacheability(HttpCacheability.NoCache);                    
                    //this.CargarClase();
                    //this.CargarMarca();
                    //this.LimpiarDropDownListModelo();
                    //this.CargarModelo();
                    //this.CargarAnio();
                    //this.CargarTipoDocumento();
                    //this.CargarDepartamento();
                    //this.CargarCiudad();
                    //this.CargarProvincia();
                    //this.CargarDistrito();
                    this.CargarFuncionesJS();                                                      
                }
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void ddlClase_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try 
            {
                this.ddlMarca.SelectedValue = "0";
                this.ddlModelo.SelectedValue = "0";  
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void ddlMarca_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try
            {
                List<BEModelo> lstBEModelo = ((List<BEModelo>)(ViewState[Constantes.ListaModelo]));
                BLModelo objBLModelo = new BLModelo();
                this.CargarDropDownList(this.ddlModelo, "IdModelo", "Descripcion", objBLModelo.ListarxMarca(lstBEModelo, Convert.ToInt32(this.ddlMarca.SelectedValue), Convert.ToInt32(this.ddlClase.SelectedValue)), false);                
                this.ddlModelo.Focus();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try
            {
                this.CargarProvincia();
                this.CargarDistrito();
                this.ddlProvincia.Focus();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void ddlProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try
            {
                this.CargarDistrito();
                this.ddlDistrito.Focus();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void btnCotizar_Click(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try
            {
                BECotizacion objBECotizacion = new BECotizacion();
                //objBECotizacion.IdEstCotizacion = Constantes.CotizacionPendiente;
                objBECotizacion.IdClase = Convert.ToInt32(this.ddlClase.SelectedValue);
                objBECotizacion.IdMarca = Convert.ToInt32(this.ddlMarca.SelectedValue);
                objBECotizacion.IdModelo = Convert.ToInt32(this.ddlModelo.SelectedValue);
                //objBECotizacion.IdAnio = Convert.ToInt32(this.ddlAniFabricacion.SelectedValue);
                //objBECotizacion.ValorVehiculo = Convert.ToDecimal(this.txtValVehiculo.Text);
                //objBECotizacion.IdPlan = String.Empty;// this.ddlPlan.SelectedValue;
                //if (this.rbtnTimCambiadoSi.Checked) { objBECotizacion.TimonCambiado = true; } else { objBECotizacion.TimonCambiado = false; }
                //if (this.rbtnNuevoSi.Checked) { objBECotizacion.Nuevo = true; } else { objBECotizacion.Nuevo = false; }
                //if (this.rbtnGpsSi.Checked) { objBECotizacion.ReqGPS = true; } else { objBECotizacion.ReqGPS = false; }
                objBECotizacion.IdTipoDocumento = this.ddlTipDocumento.SelectedValue;
                objBECotizacion.NroDocumento = this.txtNroDocumento.Text.Trim();
                objBECotizacion.ApePaterno = this.txtApePaterno.Text.Trim().ToUpper();
                objBECotizacion.ApeMaterno = this.txtApeMaterno.Text.Trim().ToUpper();
                objBECotizacion.PriNombre = this.txtPriNombre.Text.Trim().ToUpper();
                objBECotizacion.SegNombre = this.txtSegNombre.Text.Trim().ToUpper();
                objBECotizacion.Direccion = this.txtDireccion.Text.Trim().ToUpper();
                //objBECotizacion.idCiudad = Convert.ToInt32(this.ddlDistrito.SelectedValue);
                //objBECotizacion.NroTelefono = this.txtTelefono.Text.Trim();
                //objBECotizacion.Email = this.txtCorreo.Text.Trim(); 
                objBECotizacion.UsuarioCreacion = Session[NombreSession.Usuario].ToString();

                BLCotizacion objBLCotizacion = new BLCotizacion();
                this.hfIdCotizacion.Value = objBLCotizacion.Insertar(objBECotizacion).ToString();

                if (this.hfIdCotizacion.Value != String.Empty) 
                {
                    this.MostrarMensaje(this.Controls, "Se registró la cotización con exito.", false);
                    this.hfCliente.Value = objBECotizacion.PriNombre + " " + objBECotizacion.SegNombre + " "  + objBECotizacion.ApePaterno + " " + objBECotizacion.ApeMaterno;
                    this.btnCotizar.Enabled = false;  
                    this.btnImprimir.Enabled = true;
                    this.btnEnvMail.Enabled = true; 
                    this.btnSiguiente.Enabled = true;
                    this.txtEmail.Text = this.txtCorreo.Text.Trim();  
                    this.rvCotizacion.LocalReport.Refresh();                     
                }
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void btnSiguiente_Click(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try
            {
                Response.Redirect("../" + UrlPagina.VehiculoRegDatoEsp.Substring(2) + "?idCot=" + this.hfIdCotizacion.Value);
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }
        
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try
            {
                this.CargarModelo();
                this.ddlModelo.SelectedValue = this.hfIdModelo.Value;
                this.CargarProvincia();
                this.ddlProvincia.SelectedValue = this.hfProvincia.Value;
                this.CargarDistrito();
                this.ddlDistrito.SelectedValue = this.hfDistrito.Value;
                this.txtNroPlaca.Focus();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void btnRefreshClear_Click(object sender, EventArgs e)
        {
            /*
            * CREADO POR:              Gary Porras Paraguay
            * FECHA DE CREACION:       2011-05-16
            * MODIFICADO POR:          
            * FECHA DE MODIFICACION:   
            */

            try
            {
                this.CargarProvincia();
                this.ddlProvincia.SelectedValue = this.hfProvincia.Value;
                this.CargarDistrito();
                this.ddlDistrito.SelectedValue = this.hfDistrito.Value;
                this.rbtnTimCambiadoNo.Checked = true;
                this.rbtnNuevoNo.Checked = true;
                this.rbtnGpsNo.Checked = true; 
                this.txtNroPlaca.Focus();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        #endregion

        #region Eventos Popup Mail
        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try
            {
                String cMsj = "";
                DirectoryInfo directorioPdf = new DirectoryInfo("");                

                if (!directorioPdf.Exists)
                {
                    cMsj = "No existe el directorio de cotizaciones.";
                    throw new Exception(cMsj);
                }

                FileInfo archivoPdf = new FileInfo("AppSettings.DirCotizacion + AppSettings.ArchivoCotizacion" + this.hfIdCotizacion.Value + ".pdf");
                BLCotizacion objBLCotizacion = new BLCotizacion();

                //Si archivo de cotización no existe, se crea 
                //el archivo, caso contratio se obtiene el nombre 
                //del archivo en el control hfNomArchivo.
                if (!archivoPdf.Exists)
                {
                    //this.hfNomArchivo.Value = objBLCotizacion.GenerarPDF(Convert.ToInt32(this.hfIdCotizacion.Value));
                }
                else 
                {
                    //this.hfNomArchivo.Value = AppSettings.ArchivoCotizacion + this.hfIdCotizacion.Value + ".pdf";
                }               

                String cRutaPdf = "AppSettings.DirCotizacion + this.hfNomArchivo.Value";
                String cTituloCorreo = String.Empty;
                String cMensajeCorreo = String.Empty;

                BLParametro objBLParametro = new BLParametro();
                List<AONAffinity.Business.Entity.BEParametro> lstBEParametro = objBLParametro.Listar(AppSettings.CodParTitMailCot);

                if (lstBEParametro != null)
                {
                    cTituloCorreo = lstBEParametro[0].ValorString;
                }
                else 
                {
                    cMsj = "No existe el parámetro del título del correo.";
                    throw new Exception(cMsj);
                }

                lstBEParametro = objBLParametro.Listar(AppSettings.CodParMsjMailCot);

                if (lstBEParametro != null)
                {
                    String[] cArrValores = new String[2];
                    cArrValores[0] = this.hfCliente.Value;
                    cArrValores[1] = DateTime.Now.ToString();

                    cMensajeCorreo = Funciones.RemplazarValores(lstBEParametro[0].ValorString, cArrValores);

                    BLMail objBLMail = new BLMail();
                    
                    //objBLMail.Enviar(AppSettings.Correo, this.txtEmail.Text.Trim(), this.txtEmailCc.Text.Trim(), cTituloCorreo, cRutaPdf, cMensajeCorreo);
                    this.lblMensaje.Text = "Se envió correo con cotización adjunta.";
                }
                else 
                {
                    cMsj = "No existe el parámetro del mensaje del correo.";
                    throw new Exception(cMsj);
                }                
            }
            catch (Exception ex)
            {
                this.lblMensaje.Text = ex.Message;
            }
        }
        #endregion                        

        #region Métodos Privados
        /// <summary>
        /// Permite cargar las clases de vehículos.
        /// </summary>
        private void CargarClase()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLClase objBLClase = new BLClase();            
            this.CargarDropDownList(ddlClase, "IdClase", "Descripcion", objBLClase.Listar(this.bEstadoReg), true);              
        }

        /// <summary>
        /// Permite cargar las marcas de vehículos
        /// </summary>
        private void CargarMarca()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLMarca objBLMarca = new BLMarca();            
            this.CargarDropDownList(this.ddlMarca, "idMarca", "descripcion", objBLMarca.Listar(this.bEstadoReg), true);                   
        }

        /// <summary>
        /// Permite limpiar los items del control ddlModelo.
        /// </summary>
        private void LimpiarDropDownListModelo()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            List<BEModelo> lstBEModelo = null;            
            this.CargarDropDownList(this.ddlModelo, "IdModelo", "Descripcion", lstBEModelo, true);
        }

        /// <summary>
        /// Permite cargar todos los modelos de vehiculos y almacenar 
        /// la información en un ViewState.
        /// </summary>
        private void CargarModelo()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLModelo objBLModelo = new BLModelo();
            List<BEModelo> lstBEModelo = objBLModelo.Listar(this.bEstadoReg);
            ViewState[Constantes.ListaModelo] = lstBEModelo;
        }

        /// <summary>
        /// Permite cargar los años de los modelos.
        /// </summary>
        private void CargarAnio()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLAnio objBLAnio = new BLAnio();            
            this.CargarDropDownList(this.ddlAniFabricacion, "IdAnio", "descripcion", objBLAnio.Listar(this.bEstadoReg), true);                         
        }

        /// <summary>
        /// Permite cargar el tipo de documento de identidad.
        /// </summary>
        private void CargarTipoDocumento()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLTipoDocumento objBLTipoDocumento = new BLTipoDocumento();                      
            this.CargarDropDownList(this.ddlTipDocumento, "idTipoDocumento", "nombre", objBLTipoDocumento.Obtener(), false);
            
            if (this.ddlTipDocumento.Items.Count > 0) 
            {
                this.ddlTipDocumento.SelectedValue = this.cCodDocIdentidad;
            }
        }

        /// <summary>
        /// Permite cargar los departamentos.
        /// </summary>
        private void CargarDepartamento()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLDepartamento objBLDepartamento = new BLDepartamento();
            this.CargarDropDownList(this.ddlDepartamento, "idDepartamento", "nombre", objBLDepartamento.Listar(Constantes.CodPeru), false);

            if (this.ddlDepartamento.Items.Count > 0) 
            {
                this.ddlDepartamento.SelectedValue = this.nCodDepartamento.ToString();
            }            
        }

        /// <summary>
        /// Permite cargar todas las ciudaddes y almacenar
        /// la información en un ViewState.
        /// </summary>
        private void CargarCiudad()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLCiudad objBLCiudad = new BLCiudad();
            List<BECiudad> lstBECiudad = objBLCiudad.Listar();
            ViewState[Constantes.ListaCiudad] = lstBECiudad;
        }

        /// <summary>
        /// Permite cargar las provincias por departamento.
        /// </summary>
        private void CargarProvincia()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:                
             */

            List<BECiudad> lstBECiudad = ((List<BECiudad>)(ViewState[Constantes.ListaCiudad]));
            BLCiudad objBLCiudad = new BLCiudad();
            this.CargarDropDownList(this.ddlProvincia, "IdProvincia", "Provincia", objBLCiudad.Listar(Convert.ToInt32(this.ddlDepartamento.SelectedValue), lstBECiudad), false);
        }

        /// <summary>
        /// Permite cargar los distritos por provincia.
        /// </summary>
        private void CargarDistrito()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            List<BECiudad> lstBECiudad = ((List<BECiudad>)(ViewState[Constantes.ListaCiudad]));
            BLCiudad objBLCiudad = new BLCiudad();
            this.CargarDropDownList(this.ddlDistrito, "idCiudad", "nombre", objBLCiudad.Listar(Convert.ToInt32(this.ddlDepartamento.SelectedValue), Convert.ToInt32(this.ddlProvincia.SelectedValue), lstBECiudad), false);            
        }

        /// <summary>
        /// Permite cargar funciones JavaScript a los controles del
        /// formulario.
        /// </summary>
        private void CargarFuncionesJS()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            this.btnCotizar.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de registrar la cotización?')== false) return false;");
            this.txtNroDocumento.Attributes.Add("onkeypress", "return ClienteAsincrono(event);");
            this.txtNroPlaca.Attributes.Add("onkeypress", "return ClientexNroPlacaAsincrono(event);");
        }
        #endregion                               

        #region WebMethod
        /// <summary>
        /// Permite obtener la información de un cliente
        /// por nro de documento.
        /// </summary>
        /// <param name="pcNroDocumento">Nro de documento.</param>
        /// <returns>Objeto de tipo String.</returns>
        [System.Web.Services.WebMethod]
        public static String ObtenerCliente(String pcNroDocumento)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            String cResult = "";            

            BLCliente objBLCliente  = new BLCliente ();
            BECliente objBECliente = objBLCliente.Obtener(pcNroDocumento);

            if (objBECliente != null) 
            {
                cResult = objBECliente.PriNombre + "*";
                cResult = cResult + objBECliente.SegNombre + "*";
                cResult = cResult + objBECliente.ApePaterno + "*";
                cResult = cResult + objBECliente.ApeMaterno + "*";
                cResult = cResult + objBECliente.IdTipoDocumento + "*";
                cResult = cResult + objBECliente.NroDocumento + "*";
                cResult = cResult + objBECliente.idCiudad + "*";
                cResult = cResult + objBECliente.Direccion + "*";
                cResult = cResult + objBECliente.IdClase + "*";
                cResult = cResult + objBECliente.IdMarca + "*";
                cResult = cResult + objBECliente.IdModelo + "*";
                //cResult = cResult + objBECliente.IdAnio + "*";
                cResult = cResult + objBECliente.ValorVehiculo + "*";
                cResult = cResult + objBECliente.TelDomicilio + "*";
                cResult = cResult + objBECliente.Email;    
            }
            return cResult;
        }

        /// <summary>
        /// Permite obtener la información de un cliente
        /// por nro de placa.
        /// </summary>
        /// <param name="pcNroPlaca"></param>
        /// <returns></returns>
        [System.Web.Services.WebMethod]
        public static String ObtenerClietexNroPlaca(String pcNroPlaca) 
        {
            String cResult = "";

            BLCliente objBLCliente = new BLCliente();
            BECliente objBECliente = objBLCliente.ObtenerxNroPlaca(pcNroPlaca);

            if (objBECliente != null) 
            {
                cResult = objBECliente.PriNombre + "*";
                cResult = cResult + objBECliente.SegNombre + "*";
                cResult = cResult + objBECliente.ApePaterno + "*";
                cResult = cResult + objBECliente.ApeMaterno + "*";
                cResult = cResult + objBECliente.IdTipoDocumento + "*";
                cResult = cResult + objBECliente.NroDocumento + "*";
                cResult = cResult + objBECliente.idCiudad + "*";
                cResult = cResult + objBECliente.Direccion + "*";
                cResult = cResult + objBECliente.IdClase + "*";
                cResult = cResult + objBECliente.IdMarca + "*";
                cResult = cResult + objBECliente.IdModelo + "*";
                //cResult = cResult + objBECliente.IdAnio + "*";
                cResult = cResult + objBECliente.ValorVehiculo + "*";
                cResult = cResult + objBECliente.TelDomicilio + "*";
                cResult = cResult + objBECliente.Email;    
            }

            return cResult;
        }
        #endregion                                               

        protected void ddlAniFabricacion_SelectedIndexChanged(object sender, EventArgs e)
        {
            BLTarifa objBLTarifa = new BLTarifa();
            BETarifa objBETarifa = objBLTarifa.Obtener(Convert.ToInt32(this.ddlModelo.SelectedValue), Convert.ToInt32(this.ddlAniFabricacion.SelectedValue));

            if (objBETarifa != null) 
            {
                this.txtValVehiculo.Text = objBETarifa.ValorVehiculo.ToString();   
            }
        }
    }
}
