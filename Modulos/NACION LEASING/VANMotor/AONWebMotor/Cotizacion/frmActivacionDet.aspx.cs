﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Motor.Resources.Constantes;
using AONAffinity.Motor.BusinessLogic;
using AONAffinity.Motor.BusinessEntity;

namespace AONWebMotor.Cotizacion
{
    public partial class ActivacionDetalle : PageBase
    {
        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.CargarFormulario();
            }
        }

        protected void gvDetalle_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("OnMouseOut", "this.className = this.orignalclassName;");
                e.Row.Attributes.Add("OnMouseOver", "this.orignalclassName = this.className;this.className = 'GridWiew_SelectedRow';");
                Label lblCorrelativo = (Label)e.Row.FindControl("lblCorrelativo");
                CheckBox cbxEstado = (CheckBox)e.Row.FindControl("cbxEstado");
                ImageButton ibtnEditar = (ImageButton)e.Row.FindControl("btnEditar");
                BECotizacionDetalle objBECotizacion = (BECotizacionDetalle)(e.Row.DataItem);
                if (e.Row.Cells[3].Text == "0,00")
                {
                    e.Row.Cells[3].Text = "Contado";
                }



                if (ibtnEditar != null)
                {
                    ibtnEditar.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de realizar el cambio de estado?')== false) return false;");
                }

                if (lblCorrelativo != null)
                {
                    lblCorrelativo.Text = objBECotizacion.Correlativo.ToString();
                }

                if (cbxEstado != null)
                {
                    if (objBECotizacion.Elegido == true)
                    {
                        cbxEstado.Checked = true;
                    }
                }

                if (ibtnEditar != null)
                {
                    ibtnEditar.CommandArgument = e.Row.RowIndex.ToString();
                }
            }
        }

        protected void gvDetalle_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Editar":
                    this.ActualizarEstado(Convert.ToInt32(e.CommandArgument));

                    break;
            }
        }

        protected void gvDetalle_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                this.gvDetalle.PageIndex = e.NewPageIndex;
                List<BECotizacionDetalle> lstBECotizacionDetalle = (List<BECotizacionDetalle>)(ViewState[Lista.lstCotizacionDetalle]);
                this.CargarGridView(this.gvDetalle, lstBECotizacionDetalle);
            }
            catch (Exception ex)
            {
            }
        }
        #endregion

        #region Metodos
        private void CargarFormulario() 
        {
            if (!String.IsNullOrEmpty(Request.QueryString["idCot"]))
            {
                BLCotizacion objBLCotizacion = new BLCotizacion();
                List<BECotizacionDetalle> lstBECotizacionDetalle = objBLCotizacion.ObtenerDetalle(Convert.ToDecimal(Request.QueryString["idCot"]));
                this.CargarGridView(this.gvDetalle, lstBECotizacionDetalle);
                this.txtNtoCot.Text = Request.QueryString["idCot"].ToString();

                ViewState[Lista.lstCotizacionDetalle] = lstBECotizacionDetalle;


                if (lstBECotizacionDetalle == null)
                {
                    this.lblResultado.Visible = true;
                }

                if (!String.IsNullOrEmpty(Request.QueryString["cli"]))
                {
                    this.txtCliente.Text = Request.QueryString["cli"].ToString();
                }
                if (!String.IsNullOrEmpty(Request.QueryString["nroDoc"]))
                {
                    this.txtDocumento.Text = Request.QueryString["nroDoc"];
                }
                if (!String.IsNullOrEmpty(Request.QueryString["marca"]))
                {
                    this.txtMarca.Text = Request.QueryString["marca"].ToString();
                }
                if (!String.IsNullOrEmpty(Request.QueryString["modelo"]))
                {
                    this.txtModelo.Text = Request.QueryString["modelo"].ToString();
                }
                if (!String.IsNullOrEmpty(Request.QueryString["anio"]))
                {
                    this.txtAnio.Text = Request.QueryString["anio"].ToString();
                }
                if (!String.IsNullOrEmpty(Request.QueryString["valor"]))
                {
                    this.txtValor.Text = String.Format("{0:#,#0.00}", Convert.ToDecimal(Request.QueryString["valor"].ToString()));
                }
            }

            this.CargarCabeceraGV();
        }

        private void CargarCabeceraGV() 
        {
            if (this.gvDetalle.Rows.Count == 0) 
            {
                List<BECotizacionDetalle> lstBECotizacionDetalle = new List<BECotizacionDetalle>();
                BECotizacionDetalle objBECotizacionDetalle = new BECotizacionDetalle();
                lstBECotizacionDetalle.Add(objBECotizacionDetalle);
                this.CargarGridView(this.gvDetalle, lstBECotizacionDetalle);
                this.gvDetalle.Rows[0].Visible = false;
            }
        }

        private void ActualizarEstado(Int32 pnIndex)
        {
            Label lblCorrelativo = (Label)this.gvDetalle.Rows[pnIndex].FindControl("lblCorrelativo");
            CheckBox cbxEstado = (CheckBox)this.gvDetalle.Rows[pnIndex].FindControl("cbxEstado");

            Boolean bEstado = false;

            if (cbxEstado.Checked) { bEstado = true; }

            BLCotizacion objBLCotizacion = new BLCotizacion();

            if (objBLCotizacion.ActualizarElegido(Convert.ToDecimal(Request.QueryString["idCot"].ToString()), Convert.ToInt32(lblCorrelativo.Text), bEstado, Session[AONAffinity.Motor.Resources.NombreSession.Usuario].ToString()) > 0)
            {
                this.MostrarMensaje(this.UpdatePanel1, "Se actualizo el estado del registro.");
            }
        }
        #endregion
    }
}