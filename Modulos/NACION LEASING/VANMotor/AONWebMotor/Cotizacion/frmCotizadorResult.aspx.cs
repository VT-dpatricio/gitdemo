﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Motor.BusinessLogic;
using AONAffinity.Motor.BusinessEntity;
//using AONAffinity.Motor.Resources;
using AONAffinity.Motor.Resources.Constantes;

namespace AONWebMotor.Cotizacion
{
    public partial class frmCotizadorResult : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) 
            {
                this.CargarParametros();
                this.Cotizar();
            }            
        }

        #region Métodos
        private void CargarParametros() 
        {
            if (!String.IsNullOrEmpty(Request.QueryString["idGrupoAseg"]))
            {
                this.hfGrupAseg.Value = Request.QueryString["idGrupoAseg"];
            }

            if(!String.IsNullOrEmpty(Request.QueryString["idMarca"]))
            {
                this.hfMarca.Value = Request.QueryString["idMarca"];
            }

            if (!String.IsNullOrEmpty(Request.QueryString["idModelo"]))
            {
                this.hfModelo.Value = Request.QueryString["idModelo"];
            }

            if (!String.IsNullOrEmpty(Request.QueryString["anio"]))
            {
                this.hfAnio.Value = Request.QueryString["anio"];
            }

            if (!String.IsNullOrEmpty(Request.QueryString["valor"]))
            {
                this.hfValor.Value = Request.QueryString["valor"];
            }
        }

        private void Cotizar()
        {
            Decimal nValor = 0;
            Int32 nIdMarca = 0;
            Int32 nIdModelo = 0;
            Int32 nAnio = 0;
            Int32 nIdGrupoAseg = 0;

            if (this.hfValor.Value != String.Empty) nValor = Convert.ToDecimal(this.hfValor.Value);
            if (this.hfMarca.Value != String.Empty) nIdMarca = Convert.ToInt32(this.hfMarca.Value);
            if (this.hfModelo.Value != String.Empty) nIdModelo = Convert.ToInt32(this.hfModelo.Value);
            if (this.hfAnio.Value != String.Empty) nAnio = Convert.ToInt32(this.hfAnio.Value);
            if (this.hfGrupAseg.Value != String.Empty) nIdGrupoAseg = Convert.ToInt32(this.hfGrupAseg.Value);
            

            BLCotizacion objBLCotizacion = new BLCotizacion();
            //List<BECotizacion> lstBECotizacion = objBLCotizacion.GenerarCotizacion(5301, nValor, nIdMarca, nIdModelo, nAnio, false, false, 0, 1, 0, String.Empty, String.Empty);

            if (lstBECotizacion != null)
            {
                StringBuilder sbHtml = new StringBuilder();
                /*
                 <div class="ps_album" style="opacity:0;"><img src="albums/album1/thumb/thumb.jpg" alt=""/><div class="ps_desc"><h2>The Night</h2><span>Top Cat! The most effectual Top Cat! Who's intellectual close friends get to call him T.C., providing it's with dignity.Top Cat! The indisputable leader!</span></div></div>
                 */
                //sbHtml.Append(@"<table border=" + (char)(34) + "0" + (char)(34) + " cellpadding=" + (char)(34) + "0" + (char)(34) + " cellspacing=" + (char)(34) + "0" + (char)(34) + " style=" + (char)(34) + "width: 99%" + (char)(34) + ">");
                //sbHtml.Append(@"<tr>");

                //Int32 nColumna = 0;
                foreach (BECotizacion objBECotizacion in lstBECotizacion)
                {
                    sbHtml.Append("<div class=" + (char)(34) + "ps_album" + (char)(34) + " style=" + (char)(34) + "opacity:0;" + (char)(34) + ">");
                    sbHtml.Append("<div class=" + (char)(34) + "ps_desc" + (char)(34) + ">");
                    sbHtml.Append("<table>");

                    //sbHtml.Append("<table>");
                    //sbHtml.Append(@"<td>");
                    sbHtml.Append("<tr><td>" + "<img src=" + (char)(34) + objBECotizacion.RutaLogoAseg + (char)(34) + "/>" + "</td></tr>");
                    //sbHtml.Append("<table>");
                    sbHtml.Append("<tr><td>" + objBECotizacion.DesProducto + "</td></tr>");
                    sbHtml.Append("<tr><td>" + objBECotizacion.SeguroPlan + " " + objBECotizacion.DesCategoria + "</td></tr>");
                    sbHtml.Append("<tr><td>" + "Valor Vehiculo USD: " + Decimal.Round(objBECotizacion.ValVehiculo, 2) + "</td></tr>");
                    sbHtml.Append("<tr><td>" + "Tasa: " + Decimal.Round(objBECotizacion.TasaPorc, 2).ToString() + "%" + "</td></tr>");
                    sbHtml.Append("<tr><td>" + "Prima Anual USD: " + objBECotizacion.PrimaAnual.ToString() + "</td></tr>");
                    sbHtml.Append("<tr><td>" + "Prima Anual Mensual USD: " + objBECotizacion.PrimaAnualMensual.ToString() + "</td></tr>");
                    sbHtml.Append("<tr><td><input id=" + (char)(34) + "" + (char)(34) + " type=" + (char)(34) + "button" + (char)(34) + "name=" + (char)(34) + "Contratar" + (char)(34) + "value=" + (char)(34) + "Contratar" + (char)(34) + "/></td></tr>");

                    //sbHtml.Append("</td>");
                    //nColumna++;

                    /*if (nColumna == 3) 
                    {
                        nColumna = 0;
                        sbHtml.Append(@"</tr>");
                        sbHtml.Append(@"<tr>");
                        
                    }*/
                    sbHtml.Append("</table>");
                    sbHtml.Append("</div>");
                    sbHtml.Append("</div>");
                }

                //if (nColumna != 3) 
                //{
                //    for (int td = 3 - nColumna; td < nColumna; td++) 
                //    {
                //        sbHtml.Append(@"<td>");
                //        sbHtml.Append(@"</td>");
                //    }
                //}

                //sbHtml.Append(@"</tr>");
                //sbHtml.Append(@"</table>");

                ps_albums.InnerHtml = sbHtml.ToString();
            }

        }
        #endregion
    }
}