﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Motor.BusinessLogic;

namespace AONWebMotor.Cotizacion
{
    public partial class frmPrint : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) 
            {
                this.odsCotDetalle.SelectParameters["pnNroCotizacion"].DefaultValue = Request.QueryString["idCot"];
                this.ReportViewer1.LocalReport.Refresh();
            }
        }
    }
}