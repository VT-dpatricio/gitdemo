﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Library.Resources; 
using AONAffinity.Motor.Resources; 
using AONAffinity.Motor.BusinessLogic;
using AONAffinity.Motor.BusinessEntity;

namespace AONWebMotor.Cotizacion
{
    public partial class frmCotizar : PageBase
    {
        Int32 nIdProducto = 5250;
        Int32 nidSponsor = 3;
        Int32 nIdPais = 51;

        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.CargarFormulario();
            }
            else
            {
                this.hfAnio.Value = this.ddlAnioFab.SelectedValue.ToString();
                this.hfValVahiculo.Value = this.txtValVehiculo.Text;
                this.hfIdMarca.Value = this.ddlMarca.SelectedValue;
                this.hfDesMarca.Value = this.ddlMarca.SelectedItem.Text;
                this.hfIdModelo.Value = this.ddlModelo.SelectedValue;
                if (this.ddlModelo.SelectedItem != null)
                {
                    this.hfDesModelo.Value = this.ddlModelo.SelectedItem.Text;
                }

            }
        }

        #region Tab 1
        protected void ddlDep_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.CargarProv();
                this.CargarDist();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false); 
            }
        }

        protected void ddlProv_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.CargarDist();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false); 
            }
        }

        protected void ddlTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try 
            {
                this.ddlMarca.SelectedValue = "-1";
                ddlMarca_SelectedIndexChanged(null, null);
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false); 
            }
        }

        protected void ddlMarca_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.CargarModelo();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void btnCotizar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cotizar();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            try
            {
                this.NuevaCotizacion();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void ddlSigno_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.CambiarSigno();
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void ibtnCambiarValVeh_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                this.CambiarValorVehiculo();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, "Se presentó un problema al cambiar el valor del vehículo. " + ex.Message, false);
            }
        }

        protected void ibtnValorOri_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                this.RevertirValorVehiculo();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, "Se presentó un problema al revertir el valor del vehículo. " + ex.Message, false);
            }
        }
        #endregion

        #region Tab 2
        protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.CargarProvincia();
                this.CargarDistrito();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void ddlProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.CargarDistrito();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void ddlDepEntrega_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.CargarProvinciaEntrega();
                this.CargarDistritoEntrega();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void ddlProvEntrega_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.CargarDistritoEntrega();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void ddlPlan_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.ddlFrecuencia_SelectedIndexChanged(sender, e);
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void ddlFrecuencia_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.MostrarPrima();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void btnExpedir_Click(object sender, EventArgs e)
        {
            try
            {
               
                this.Expedir();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }
        #endregion
        #endregion
        
        #region Métodos Privados
        private void CargarParametro()
        {
            if (!String.IsNullOrEmpty(Request.QueryString["idProd"]))
            {
                this.hfIdProducto.Value = Request.QueryString["idProd"];
                BLProducto objBLProducto = new BLProducto();
                BEProducto objBEProducto = new BEProducto();
                objBEProducto = objBLProducto.Obtener(Convert.ToInt32(this.hfIdProducto.Value));
                ViewState["Producto"] = objBEProducto;
            }
            else
            {
                this.hfIdProducto.Value = "0";
            }
        }

        private void CargarFormulario() 
        {
            this.CargarParametro();            
            //BAIS
            this.CargarInformador();
            this.CargarOficina();
            this.CargarFecha();
            this.CargarTipoDocumento();
            this.CargarCiudades();
            this.CargarDepartamento();
            this.CargarDepartamentoEntrega();
            this.CargarPlan();
            this.CargarFrecuencia();
            this.CargarMonedaPrima();
            this.CargarInfoAsegurado();
            //MOTOR
            this.CargarTipoVehiculo();
            this.CargarMarca();
            this.CargarUsoVehiculo();
            this.CargarAnioFab();
            this.CargarProducto(); 
            this.CargarFuncionesJS();
            this.CargarPorcentajesCambio();
            //CREACION DE SESSION
            Session["objCotizacion"] = new BECotizacion();

        }

        private void CargarInformador()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLInformador objBLInformador = new AONAffinity.Motor.BusinessLogic.Bais.BLInformador();
            this.CargarDropDownList(this.ddlInformador, "IdInformador", "Nombre", objBLInformador.ListarxSponsor(this.nidSponsor), true);
        }

        private void CargarOficina()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLOficina objBLOficina = new AONAffinity.Motor.BusinessLogic.Bais.BLOficina();
            this.CargarDropDownList(this.ddlOficina, "IdOficina", "Nombre", objBLOficina.ListarxSponsor(this.nidSponsor), true);
        }

        private void CargarFecha()
        {
            this.txtFechaVenta.Text = DateTime.Now.ToShortDateString().ToString();
        }

        private void CargarTipoDocumento()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLTipoDocProducto objBLTipoDocProducto = new AONAffinity.Motor.BusinessLogic.Bais.BLTipoDocProducto();
            this.CargarDropDownList(this.ddlTipDocumento, "IdTipoDocumento", "Nombre", objBLTipoDocProducto.ListarxProducto(nIdProducto), true);
        }

        private void CargarCiudades()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.Bais.BLCiudad();
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad = objBLCiudad.Listar(this.nIdPais);
            ViewState[ValorConstante.ListaCiudad] = lstBECiudad;
        }

        private void CargarDepartamento()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLDepartamento objBLDepartamento = new AONAffinity.Motor.BusinessLogic.Bais.BLDepartamento();
            this.CargarDropDownList(this.ddlDepartamento, "idDepartamento", "nombre", objBLDepartamento.Listar(51), true);
            this.CargarDropDownList(this.ddlDep, "idDepartamento", "nombre", objBLDepartamento.Listar(51), true);
        }

        private void CargarProv()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.Bais.BLCiudad();
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad = (List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad>)(ViewState[ValorConstante.ListaCiudad]);
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad_Prov = objBLCiudad.ListarProvincia(Convert.ToInt32(this.ddlDep.SelectedValue), lstBECiudad);
            this.CargarDropDownList(this.ddlProv, "idProvincia", "nombre", lstBECiudad_Prov, true);
        }

        private void CargarDist()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.Bais.BLCiudad();
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad = (List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad>)(ViewState[ValorConstante.ListaCiudad]);
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad_Dist = objBLCiudad.ListarDistrito(this.ddlProv.SelectedValue, lstBECiudad);
            this.CargarDropDownList(this.ddlDis, "idCiudad", "nombre", lstBECiudad_Dist, true);
        }

        private void CargarProvincia()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.Bais.BLCiudad();
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad = (List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad>)(ViewState[ValorConstante.ListaCiudad]);
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad_Prov = objBLCiudad.ListarProvincia(Convert.ToInt32(this.ddlDepartamento.SelectedValue), lstBECiudad);
            this.CargarDropDownList(this.ddlProvincia, "idProvincia", "nombre", lstBECiudad_Prov, true);
        }

        private void CargarDistrito()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.Bais.BLCiudad();
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad = (List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad>)(ViewState[ValorConstante.ListaCiudad]);
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad_Dist = objBLCiudad.ListarDistrito(this.ddlProvincia.SelectedValue, lstBECiudad);
            this.CargarDropDownList(this.ddlDistrito, "idCiudad", "nombre", lstBECiudad_Dist, true);
        }

        private void CargarDepartamentoEntrega()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLDepartamento objBLDepartamento = new AONAffinity.Motor.BusinessLogic.Bais.BLDepartamento();
            this.CargarDropDownList(this.ddlDepEntrega, "idDepartamento", "nombre", objBLDepartamento.Listar(this.nIdPais), true);
        }

        private void CargarProvinciaEntrega()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.Bais.BLCiudad();
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad = (List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad>)(ViewState[ValorConstante.ListaCiudad]);
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad_Prov = objBLCiudad.ListarProvincia(Convert.ToInt32(this.ddlDepEntrega.SelectedValue), lstBECiudad);
            this.CargarDropDownList(this.ddlProvEntrega, "idProvincia", "nombre", lstBECiudad_Prov, true);
        }

        private void CargarDistritoEntrega()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLCiudad objBLCiudad = new AONAffinity.Motor.BusinessLogic.Bais.BLCiudad();
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad = (List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad>)(ViewState[ValorConstante.ListaCiudad]);
            List<AONAffinity.Motor.BusinessEntity.Bais.BECiudad> lstBECiudad_Dist = objBLCiudad.ListarDistrito(this.ddlProvEntrega.SelectedValue, lstBECiudad);
            this.CargarDropDownList(this.ddlDistEntrega, "idCiudad", "nombre", lstBECiudad_Dist, true);
        }

        private void CargarPlan()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLOpcion objBLopcion = new AONAffinity.Motor.BusinessLogic.Bais.BLOpcion();
            this.CargarDropDownList(this.ddlPlan, "Opcion", "Opcion", objBLopcion.Listar(this.nIdProducto), true);
        }

        private void CargarFrecuencia()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLFrecuencia objBLFrecuencia = new AONAffinity.Motor.BusinessLogic.Bais.BLFrecuencia();
            this.CargarDropDownList(this.ddlFrecuencia, "IdFrecuencia", "Nombre", objBLFrecuencia.Listar(nIdProducto), true);
        }

        private void CargarMonedaPrima()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLMoneda objBLMoneda = new AONAffinity.Motor.BusinessLogic.Bais.BLMoneda();
            this.CargarDropDownList(this.ddlMonPrima, "IdMoneda", "IdMoneda", objBLMoneda.Listar(nIdProducto), true);
        }

        private void CargarInfoAsegurado() 
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLInfoAsegurado objBLInfoAsegurado = new AONAffinity.Motor.BusinessLogic.Bais.BLInfoAsegurado();
            ViewState["InfoAsegurado"] = objBLInfoAsegurado.ObtenerxProducto(Convert.ToInt32(this.hfIdProducto.Value));
        }

        private void CargarPorcentajesCambio()
        {
            BLParametro objBLParametro = new BLParametro();
            BEProducto objBEProducto = (BEProducto)(ViewState["Producto"]);

            if (objBEProducto != null)
            {
                if (objBEProducto.MaxPorcMas != NullTypes.ShortNull)
                {
                    ViewState["lstBEParametro_Max"] = objBLParametro.ObtenerPorcMaxCambio();
                }

            }
            
            ViewState["lstBEParametro_Max"] = objBLParametro.ObtenerPorcMaxCambio();
            ViewState["lstBEParametro_Min"] = objBLParametro.ObtenerPorcMinCambio();
        }

        private void CargarTipoVehiculo()
        {
            BLProductoTipoVehiculo objBLTipoVehiculo = new BLProductoTipoVehiculo();
            this.CargarDropDownList(this.ddlTipo, "IdTipoVehiculo", "Descripcion", objBLTipoVehiculo.ListarxProducto(nIdProducto, true), true);
        }

        private void CargarMarca()
        {
            BLProductoMarca objBLProductoMarca = new BLProductoMarca();
            this.CargarDropDownList(this.ddlMarca, "IdMarca", "Descripcion", objBLProductoMarca.ListarxProducto(nIdProducto, true), true);
        }

        private void CargarModelo()
        {            
            this.ddlModelo.Items.Clear();
            BLModelo objBLModelo = new BLModelo ();
            this.CargarDropDownList(this.ddlModelo, "IdModelo", "Descripcion", objBLModelo.ListarxMarcaTipo(nIdProducto, Convert.ToInt32(this.ddlMarca.SelectedValue), Convert.ToInt32(this.ddlTipo.SelectedValue), true), true);     
        }

        private void CargarUsoVehiculo() 
        {
            BLProductoUsoVehiculo objBLProductoUsoVehiculo = new BLProductoUsoVehiculo();
            this.CargarDropDownList(this.ddlUso, "IdUsoVehiculo", "Descripcion", objBLProductoUsoVehiculo.ListarxProducto(nIdProducto, true), true);
        }

        private void CargarAnioFab()
        {
            BLProducto objBLProducto = new BLProducto();
            this.CargarDropDownList(this.ddlAnioFab, "", "", objBLProducto.ListarAnios(Convert.ToInt32(this.hfIdProducto.Value)), true);
        }

        private void CargarProducto()
        {
            if(this.hfIdProducto.Value != String.Empty)
            {
                BLProducto objBLProducto = new BLProducto();
                BEProducto objBEProducto = objBLProducto.Obtener(Convert.ToInt32(this.hfIdProducto.Value));
                ViewState["Producto"] = objBEProducto; 
            }
        }

        private void CargarFuncionesJS() 
        {
            this.btnCotizar.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de realizar la cotización?')== false) return false;");
            this.ibtnCambiarValVeh.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de cambiar el valor del vehículo?')== false) return false;");
            this.ibtnValorOri.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de revertir el valor original del vehículo?')== false) return false;"); 
            this.btnExpedir.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de expedir el certificado?')== false) return false;");
            this.btnNuevo.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de realizar una nueva cotización?')== false) return false;");
        }

        private void CambiarSigno()
        {
            if (this.ddlSigno.SelectedValue == "-1")
            {
                this.ddlPorcentaje.Items.Clear();
            }
            else
            {
                BEProducto objBEProducto = (BEProducto)(ViewState["Producto"]);

                if (objBEProducto != null)
                {
                    if (ddlSigno.SelectedValue == "+")
                    {
                        if (objBEProducto.MaxPorcMas != NullTypes.ShortNull)
                        {
                            List<BEParametro> lstBEParametro_Max = (List<BEParametro>)(ViewState["lstBEParametro_Max"]);
                            this.CargarDropDownList(this.ddlPorcentaje, "ValorNumeroDec", "ValorNumeroDec", lstBEParametro_Max, false);
                        }
                    }

                }
            }
        }

        private void CambiarValorVehiculo()
        {
            //Guardara valor actual.            

            if (this.txtValorAnt.Text == String.Empty)
            {
                this.txtValorAnt.Text = this.txtValorVehiculoDep.Text;
            }

            Decimal nNewValor = NullTypes.DecimalNull;

            if (this.ddlSigno.SelectedValue != "-1" && this.txtValorVehiculoDep.Text.Trim() != String.Empty)
            {
                if (ddlSigno.SelectedValue == "+")
                {
                    nNewValor = Convert.ToDecimal(this.txtValorAnt.Text) + (Convert.ToDecimal(this.txtValorAnt.Text) * (Convert.ToDecimal(ddlPorcentaje.SelectedValue) / 100));
                }

                if (ddlSigno.SelectedValue == "-")
                {
                    nNewValor = Convert.ToDecimal(this.txtValorAnt.Text) - (Convert.ToDecimal(this.txtValorAnt.Text) * (Convert.ToDecimal(ddlPorcentaje.SelectedValue) / 100));
                }

                this.txtValorVehiculoDep.Text = Decimal.Round(nNewValor, 2).ToString();
                this.txtMontoAsegurado.Text = this.txtValorVehiculoDep.Text;
                this.CotizarValorCambiado();
            }

            this.ddlPlan.SelectedIndex = 0;
            this.ddlFrecuencia.SelectedIndex = 0;
            this.txtPrimaTot.Text = String.Empty;
            this.txtPrimCob.Text = String.Empty;
        }

        private void CotizarValorCambiado()
        {
            Boolean bTimon = false;
            if (this.ddlTimon.SelectedValue == "1") { bTimon = true; }

            Decimal nPrimaMin = NullTypes.DecimalNull;
            BECategoria objBECategoria = (BECategoria)(ViewState["objBECategoria"]);

            if (objBECategoria != null)
            {
                nPrimaMin = objBECategoria.MontoPrima;
            }

            BLCotizacion objBLCotizacion = new BLCotizacion();
            BECotizacion objBECotizacion = objBLCotizacion.Cotizar(Convert.ToInt32(this.hfIdProducto.Value), Convert.ToInt32(this.ddlModelo.SelectedValue), Convert.ToInt32(this.ddlAnioFab.SelectedValue), Convert.ToDecimal(this.txtValorVehiculoDep.Text.Trim()), bTimon, this.ddlTimon.SelectedValue, NullTypes.DecimalNull, nPrimaMin);

            if (objBECotizacion != null)
            {
                ViewState["objBECotizacion"] = objBECotizacion;

                if (objBECotizacion.TasaAnual != NullTypes.DecimalNull)
                {
                    this.txtTasaAnual.Text = Decimal.Round(objBECotizacion.TasaAnual, 2).ToString();
                    this.txtPrimaAnual.Text = Decimal.Round(objBECotizacion.PrimaAnual, 2).ToString();
                    this.txtPrimaAnualMen.Text = Decimal.Round((objBECotizacion.PrimaAnual / 12), 2).ToString();
                }

                if (objBECotizacion.TasaBianual != NullTypes.DecimalNull)
                {
                    this.txtTasaBianual.Text = Decimal.Round(objBECotizacion.TasaBianual, 2).ToString();
                    this.txtPrimaBianual.Text = Decimal.Round(objBECotizacion.PrimaBianual, 2).ToString();
                    this.txtPrimaBianualMen.Text = Decimal.Round((objBECotizacion.PrimaBianual / 24), 2).ToString();
                }
            }

            this.ddlPlan.SelectedIndex = 0;
            this.ddlFrecuencia.SelectedIndex = 0;
            this.txtPrimaTot.Text = String.Empty;
            this.txtPrimCob.Text = String.Empty;
        }

        private void RevertirValorVehiculo()
        {
            if (this.txtValorAnt.Text != String.Empty)
            {
                this.ddlSigno.SelectedValue = "-1";
                this.ddlPorcentaje.Items.Clear();
                this.txtValorVehiculoDep.Text = this.txtValorAnt.Text;
                this.txtMontoAsegurado.Text = this.txtValorVehiculoDep.Text;
                this.txtValorAnt.Text = String.Empty;
                this.CotizarValorCambiado();
            }
        }

        //private void CargarExpedir() 
        //{
        //    BLCotizacion objBLCotizacion = new BLCotizacion();
        //    Boolean bTimon = false;
        //    String cScoring = "-1";
        //    Decimal dporc = 0;

        //    BECategoria objBECategoria = (BECategoria)(ViewState["objBECategoria"]);

        //    Decimal pnPrimaMin = NullTypes.DecimalNull;

        //    if (objBECategoria != null) 
        //    {
        //        pnPrimaMin = objBECategoria.MontoPrima;
        //    }

        //    List<BECotizacion> lstBECotizacion = objBLCotizacion.Cotizar(Convert.ToInt32(this.hfIdProducto.Value), Convert.ToInt32(this.ddlModelo.SelectedValue), Convert.ToInt32(this.ddlAnioFab.SelectedValue), Convert.ToDecimal(this.txtValorVehiculoDep.Text.Trim()), Convert.ToInt32(this.ddlMarca.SelectedValue), this.ddlMarca.SelectedItem.Text, this.ddlModelo.SelectedItem.Text, bTimon, cScoring, dporc, pnPrimaMin);
        //    ViewState["lstCotizacion"] = lstBECotizacion;
        //    this.txtMarca.Text = this.hfDesMarca.Value;
        //    this.txtModelo.Text = this.hfDesModelo.Value;
        //    this.txtAnio.Text = this.hfAnio.Value;
        //    this.txtTipVehiculo.Text = this.ddlTipo.SelectedItem.Text;
        //    //this.txtClaseVehiculo.Text = this.ddlClase.SelectedItem.Text;                
        //}

        private void MostrarPrima()
        {
            this.txtPrimCob.Text = String.Empty;
            this.txtPrimaTot.Text = String.Empty;
 
            List<BECotizacion> lstBECotizacion = (List<BECotizacion>)(ViewState["lstCotizacion"]);
            BECotizacion objBECotizacion = (BECotizacion)(ViewState["objBECotizacion"]);

            if (this.hfIdProducto.Value != String.Empty) 
            {
                if (this.ddlPlan.SelectedValue == ValorConstante.PlanAnual)
                {
                    if(objBECotizacion.TasaAnual != NullTypes.DecimalNull )
                    {
                        if (this.ddlFrecuencia.SelectedValue.ToString() == "1")
                        {
                            this.txtPrimaTot.Text = Decimal.Round(objBECotizacion.PrimaAnual, 2).ToString();
                            this.txtPrimCob.Text = Decimal.Round((objBECotizacion.PrimaAnual / 12), 2).ToString();
                        }
                        if (this.ddlFrecuencia.SelectedValue.ToString() == "0")
                        {
                            this.txtPrimaTot.Text = Decimal.Round(objBECotizacion.PrimaAnual, 2).ToString();
                            this.txtPrimCob.Text = Decimal.Round(objBECotizacion.PrimaAnual, 2).ToString();
                        }
                    }                    
                }
                else
                {
                     if(objBECotizacion.TasaBianual != NullTypes.DecimalNull )
                     {
                         if (this.ddlFrecuencia.SelectedValue.ToString() == "1")
                        {
                            this.txtPrimaTot.Text = Decimal.Round(objBECotizacion.PrimaBianual, 2).ToString();
                            this.txtPrimCob.Text = Decimal.Round((objBECotizacion.PrimaBianual / 24), 2).ToString();
                        }

                        if (this.ddlFrecuencia.SelectedValue.ToString() == "0")
                        {
                            this.txtPrimaTot.Text = Decimal.Round(objBECotizacion.PrimaBianual, 2).ToString();
                            this.txtPrimCob.Text = Decimal.Round(objBECotizacion.PrimaBianual, 2).ToString();
                        }
                     }                    
                }
            }


            //foreach (BECotizacion objBECotizacion in lstBECotizacion) 
            //{
            //    //if (this.ddlAsegurador.SelectedValue == "1" && objBECotizacion.IdProducto == Convert.ToInt32(this.hfIdProducto.Value))
            //    //{
            //    //    if (this.ddlPlan.SelectedValue == ValorConstante.PlanAnual)
            //    //    {
            //    //        if (this.ddlFrecuencia.SelectedValue.ToString() == "1")
            //    //        {
            //    //            this.txtPrimTotal.Text = Decimal.Round((objBECotizacion.PrimaAnual / 12), 2).ToString();
            //    //        }

            //    //        if (this.ddlFrecuencia.SelectedValue.ToString() == "0")
            //    //        {
            //    //            this.txtPrimTotal.Text = Decimal.Round(objBECotizacion.PrimaAnual, 2).ToString();
            //    //        }
            //    //    }
            //    //    else 
            //    //    {
            //    //        if (this.ddlFrecuencia.SelectedValue.ToString() == "1")
            //    //        {
            //    //            this.txtPrimTotal.Text = Decimal.Round((objBECotizacion.PrimaBianual / 24), 2).ToString();
            //    //        }

            //    //        if (this.ddlFrecuencia.SelectedValue.ToString() == "0")
            //    //        {
            //    //            this.txtPrimTotal.Text = Decimal.Round(objBECotizacion.PrimaBianual, 2).ToString();
            //    //        }
            //    //    }
            //    //}
            //    //if (this.ddlAsegurador.SelectedValue == "2" && objBECotizacion.IdProducto == 5252) 
            //    //{
                    
                //}
            //}            
        }

        //private void CargarOpciones() 
        //{
        //    if (this.hfIdProducto.Value != String.Empty) 
        //    {
        //        //CargarPorcentajesCambio();
        //        //this.ReportViewer1.Visible = false;
        //    }
        //}

        private void Cotizar()
        {
            if (this.ValidarCotizar() == false)
            {
                return;
            }

            BLCategoria objBLCategoria = new BLCategoria();
            BECategoria objBECategoria = objBLCategoria.ObtenerxModelo(Convert.ToInt32(this.ddlModelo.SelectedValue));

            if (objBECategoria == null)
            {
                return;
            }

            Decimal nPrimaMinima = NullTypes.DecimalNull;
            nPrimaMinima = objBECategoria.MontoPrima;
            ViewState["objBECategoria"] = objBECategoria;

            BLCotizacion objBLCotizacion = new BLCotizacion();
            Decimal nValorVehiculoDep = objBLCotizacion.ObtenerValorDepresiado(Convert.ToInt32(this.ddlAnioFab.SelectedValue), Convert.ToDecimal(this.txtValVehiculo.Text.Trim()), Convert.ToInt32(this.hfIdProducto.Value));
            this.txtValorVehiculoDep.Text = Decimal.Round(nValorVehiculoDep, 2).ToString();

            Boolean bTimon = false;
            if (this.ddlTimon.SelectedValue == "1") { bTimon = true; }
            BECotizacion objBECotizacion = objBLCotizacion.Cotizar(Convert.ToInt32(this.hfIdProducto.Value), Convert.ToInt32(this.ddlModelo.SelectedValue), Convert.ToInt32(this.ddlAnioFab.SelectedValue), Convert.ToDecimal(this.txtValorVehiculoDep.Text.Trim()), bTimon, this.ddlTimon.SelectedValue, NullTypes.DecimalNull, nPrimaMinima);

            if (objBECotizacion != null)
            {
                ViewState["objBECotizacion"] = objBECotizacion;

                if (objBECotizacion.TasaAnual != NullTypes.DecimalNull)
                {
                    this.txtTasaAnual.Text = Decimal.Round(objBECotizacion.TasaAnual, 2).ToString();
                    this.txtPrimaAnual.Text = Decimal.Round(objBECotizacion.PrimaAnual, 2).ToString();
                    this.txtPrimaAnualMen.Text = Decimal.Round((objBECotizacion.PrimaAnual / 12), 2).ToString();
                }

                if (objBECotizacion.TasaBianual != NullTypes.DecimalNull)
                {
                    this.txtTasaBianual.Text = Decimal.Round(objBECotizacion.TasaBianual, 2).ToString();
                    this.txtPrimaBianual.Text = Decimal.Round(objBECotizacion.PrimaBianual, 2).ToString();
                    this.txtPrimaBianualMen.Text = Decimal.Round((objBECotizacion.PrimaBianual / 24), 2).ToString();
                }
            }

            this.txtMarca.Text = this.hfDesMarca.Value;
            this.txtModelo.Text = this.hfDesModelo.Value;
            this.txtAnio.Text = this.hfAnio.Value;
            this.txtTipVehiculo.Text = this.ddlTipo.SelectedItem.Text;
            this.txtMontoAsegurado.Text = this.txtValorVehiculoDep.Text;
            this.hfAnio.Value = this.ddlAnioFab.SelectedValue.ToString();
            this.hfIdMarca.Value = this.ddlMarca.SelectedValue;
            this.hfDesMarca.Value = this.ddlMarca.SelectedItem.Text;
            this.hfIdModelo.Value = this.ddlModelo.SelectedValue;
            this.hfDesModelo.Value = this.ddlModelo.SelectedItem.Text;
            this.hfValVahiculo.Value = this.txtValorVehiculoDep.Text.Trim();
            objBECotizacion.IdProducto = 5250;
            objBECotizacion.DesMarca = this.ddlMarca.SelectedItem.Text;
            objBECotizacion.DesModelo = this.ddlModelo.SelectedItem.Text;
            Session["objCotizacion"] = objBECotizacion;
            objBECotizacion.IdCategoria = objBECategoria.IdCategoria;
            //this.CargarExpedir();
        }

        private void Expedir()
        {
            if (this.ValidarDatosExpedir() == false) 
            {
                return;
            }

            if (this.ValidarExpedir() == false)
            {
                return;
            }

            AONAffinity.Motor.BusinessEntity.Bais.BECertificado objBECertificado = this.CargarCertificado();
            List<AONAffinity.Motor.BusinessEntity.Bais.BEAsegurado> lstBEAsegurado = this.CargarAsegurado();
            List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC> lstBEInfoProductoC = this.CargarInfoProductoC();
            List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC> lstBEInfoAseguradoC = this.CargarInfoAseguradoC();

            AONAffinity.Motor.BusinessLogic.Bais.BLCertificado objBLCertificado = new AONAffinity.Motor.BusinessLogic.Bais.BLCertificado();
            List<AONAffinity.Motor.BusinessEntity.Bais.BEExpedir> lstBEExpedir = objBLCertificado.Expedir(objBECertificado, lstBEAsegurado, null, lstBEInfoProductoC, lstBEInfoAseguradoC, String.Empty);

            if (lstBEExpedir[0].Codigo == 1)
            {
                this.MostrarMensaje(this.Controls, "El certificado se expidió con exito.", false);
                this.btnExpedir.Enabled = false;
            }
            else
            {
                this.MostrarMensaje(this.Controls, "Se produjo un error al expedir el certificado." + lstBEExpedir[0].Descripcion, false);
            }
        }

        private void NuevaCotizacion()
        {
            this.ddlTipo.SelectedValue = "-1";
            this.ddlMarca.SelectedValue = "-1";
            this.ddlModelo.SelectedValue = "-1";
            this.ddlAnioFab.SelectedIndex = 0;
            this.txtValVehiculo.Text = String.Empty;
            this.ddlSigno.SelectedIndex = 0;
            this.ddlPorcentaje.SelectedIndex = 0;
            this.txtValorAnt.Text = String.Empty;
            this.txtValorVehiculoDep.Text = String.Empty;
            this.txtTasaAnual.Text = String.Empty;
            this.txtPrimaAnual.Text = String.Empty;
            this.txtPrimaAnualMen.Text = String.Empty;
            this.txtTasaBianual.Text = String.Empty;
            this.txtPrimaBianual.Text = String.Empty;
            this.txtPrimaBianualMen.Text = String.Empty;

            //Datos Cliente
            this.txtNombre1.Text = String.Empty;
            this.txtNombre2.Text = String.Empty;
            this.ddlTipDocumento.SelectedIndex = 0;
            this.txtNroDocumento.Text = String.Empty;
            this.txtApellidoPat.Text = String.Empty;
            this.txtApellidoMat.Text = String.Empty;
            this.ddlSexo.SelectedIndex = 0;
            this.ddlEstadoCiv.SelectedIndex = 0;
            this.txtFecNacimiento.Text = String.Empty;
            this.txtDireccion.Text = String.Empty;
            this.ddlPrefijo.SelectedIndex = 0;
            this.txtMzLteNro.Text = String.Empty;
            this.txtAptoInt.Text = String.Empty;
            this.txtUrbanizacion.Text = String.Empty;
            this.txtReferencia.Text = String.Empty;
            this.txtReferencia2.Text = String.Empty;
            this.ddlDepartamento.SelectedIndex = 0;
            this.txtTelefono1.Text = String.Empty;
            this.txtTelefono2.Text = String.Empty;
            this.txtEmail.Text = String.Empty;
            this.txtDirEntrega.Text = String.Empty;
            this.ddlDepEntrega.SelectedIndex = 0;

            //Datos Vehiculo
            this.txtMarca.Text = String.Empty;
            this.txtModelo.Text = String.Empty;
            this.txtAnio.Text = String.Empty;
            this.txtNroMotor.Text = String.Empty;
            this.txtNroPlaca.Text = String.Empty;
            this.txtColor.Text = String.Empty;
            this.txtTipVehiculo.Text = String.Empty;
            this.txtNroAsientos.Text = String.Empty;
            this.txtNroChasis.Text = String.Empty;
            this.txtMontoAsegurado.Text = String.Empty;

            //Datos Plan Prima
            this.ddlPlan.SelectedIndex = 0;
            this.ddlFrecuencia.SelectedIndex = 0;
            this.ddlMonPrima.SelectedIndex = 0;
            this.txtPrimaTot.Text = String.Empty;
            this.txtPrimCob.Text = String.Empty;
            this.btnExpedir.Enabled = true;

            ViewState["objBECategoria"] = null;
            ViewState["lstCotizacion"] = null;
        }
        #endregion

        #region Funciones Privadas
        private Boolean ValidarCotizar() 
        {
            Boolean bResult = true;
            BLCotizacion objBLCotizacion = new BLCotizacion();
            Decimal nValorVehiculoDep = objBLCotizacion.ObtenerValorDepresiado(Convert.ToInt32(this.ddlAnioFab.SelectedValue), Convert.ToDecimal(this.txtValVehiculo.Text.Trim()), Convert.ToInt32(this.hfIdProducto.Value));
            
            Boolean bTimonCamb = false;
            if (this.ddlTimon.SelectedValue == "NO") { bTimonCamb = false; } else { bTimonCamb = true; }
            
            List<BERespuesta> lstBERespuesta = new List<BERespuesta>();
            lstBERespuesta = objBLCotizacion.ValidarVehiculo(Convert.ToInt32(this.hfIdProducto.Value), Convert.ToInt32(this.ddlModelo.SelectedValue), bTimonCamb, nValorVehiculoDep, Convert.ToInt32(this.ddlAnioFab.SelectedValue), 0, Convert.ToInt32(this.ddlTipo.SelectedValue), Convert.ToInt32(this.ddlUso.SelectedValue), false, false, Convert.ToInt32(this.ddlDis.SelectedValue), Convert.ToInt32(this.ddlDis.SelectedValue));
      
            if(lstBERespuesta != null)
            {
                bResult = false;
                List<String> cArrMsj = new List<String>();

                String cMsj = String.Empty; 

                foreach (BERespuesta objBERespuesta in lstBERespuesta) 
                {
                    if (cMsj != String.Empty)
                    {
                        cMsj = cMsj + "\\n" + objBERespuesta.Descripcion;
                    }
                    else 
                    {
                        cMsj = objBERespuesta.Descripcion;
                    }                    
                }

                this.MostrarMensaje(this.Controls, cMsj, false);
            }

            return bResult;
        }

        private Boolean ValidarExpedir() 
        {
            Boolean bResult = true;
            BLCotizacion objBLCotizacion = new BLCotizacion();
            
            Boolean bTimonCamb = false;
            if (this.ddlTimon.SelectedValue == "NO") { bTimonCamb = false; } else { bTimonCamb = true; }
            
            List<BERespuesta> lstBERespuesta = new List<BERespuesta>();
            lstBERespuesta = objBLCotizacion.ValidarVehiculo(Convert.ToInt32(this.hfIdProducto.Value), Convert.ToInt32(this.ddlModelo.SelectedValue), bTimonCamb, Convert.ToDecimal(this.txtMontoAsegurado.Text.Trim()), Convert.ToInt32(this.ddlAnioFab.SelectedValue), 0, Convert.ToInt32(this.ddlTipo.SelectedValue), Convert.ToInt32(this.ddlUso.SelectedValue), false, false, Convert.ToInt32(this.ddlDistrito.SelectedValue), Convert.ToInt32(this.ddlDistEntrega.SelectedValue));
      
            if(lstBERespuesta != null)
            {
                bResult = false;
                List<String> cArrMsj = new List<String>();

                String cMsj = String.Empty; 

                foreach (BERespuesta objBERespuesta in lstBERespuesta) 
                {
                    if (cMsj != String.Empty)
                    {
                        cMsj = cMsj + "\\n" + objBERespuesta.Descripcion;
                    }
                    else 
                    {
                        cMsj = objBERespuesta.Descripcion;
                    }                    
                }

                this.MostrarMensaje(this.Controls, cMsj, false);
            }

            //BEVehiculo objBEVehiculo = new BEVehiculo();
            //List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAsegurado> lstBEInfoAsegurado = (List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAsegurado>)(ViewState["InfoAsegurado"]);
            

            /*
              SqlParameter sqlParam1 = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int);
                sqlParam1.Value = pnIdProducto;

                SqlParameter sqlParam2 = sqlCmd.Parameters.Add("@idInfAsegPlaca", SqlDbType.Int);
                sqlParam2.Value = pObjBEVehiculo.IdInfasegPlaca = pObjBEVehiculo.IdInfasegPlaca;

                SqlParameter sqlParam3 = sqlCmd.Parameters.Add("@nroPlaca", SqlDbType.VarChar, 25);
                sqlParam3.Value = pObjBEVehiculo.Placa;

                SqlParameter sqlParam4 = sqlCmd.Parameters.Add("@idInfAsegMotor", SqlDbType.Int);
                sqlParam4.Value = pObjBEVehiculo.IdInfoAsegMotor;

                SqlParameter sqlParam5 = sqlCmd.Parameters.Add("@nroMotor", SqlDbType.VarChar, 25);
                sqlParam5.Value = pObjBEVehiculo.NroMotor;

                SqlParameter sqlParam6 = sqlCmd.Parameters.Add("@idInfAsegSerie", SqlDbType.Int);
                sqlParam6.Value = pObjBEVehiculo.IdInfAsegSerie;

                SqlParameter sqlParam7 = sqlCmd.Parameters.Add("@nroSerie", SqlDbType.VarChar, 25);
                sqlParam7.Value = pObjBEVehiculo.NroChasis;
             */

            AONAffinity.Motor.BusinessLogic.Bais.BLExpedir objBLExpedir = new AONAffinity.Motor.BusinessLogic.Bais.BLExpedir();    
            List<AONAffinity.Motor.BusinessEntity.Bais.BEExpedir> lstBEExpedir = new List<AONAffinity.Motor.BusinessEntity.Bais.BEExpedir>();  

            return bResult;
        }

        private Boolean ValidarDatosExpedir() 
        {
            Boolean bResult = true;

            if (this.ddlTipDocumento.SelectedValue.ToString() == "L" && this.txtNroDocumento.Text.Trim().Length != 8)
            {
                this.MostrarMensaje(this.Controls, "Ingrese un nro de documento no válido.", false);
                this.txtNroDocumento.Focus();
                bResult = false;
            }

            if (this.txtPrimCob.Text == String.Empty)
            {
                this.MostrarMensaje(this.Controls, "No existe monto de prima.", false);
                this.txtPrimCob.Focus();
                bResult = false;
            }

            DateTime dFecNacimiento;

            if (DateTime.TryParseExact(this.txtFecNacimiento.Text, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.AssumeLocal, out dFecNacimiento) != true)
            {
                this.MostrarMensaje(this.Controls, "La fecha de nacimiento tiene formato incorrecto.", false);
                this.txtFecNacimiento.Focus();
                bResult = false;
            }

            DateTime dFechaFinVigencia;

            if (DateTime.TryParseExact(this.txtFinVigencia.Text, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.AssumeLocal, out dFechaFinVigencia) != true)
            {
                this.MostrarMensaje(this.Controls, "La fecha de fin de vigencia tiene formato incorrecto.", false);
                this.txtFinVigencia.Focus();
                bResult = false;
            }

            return bResult;
        }

        private String ObtenerNroPoliza()
        {
            String nroPoliza = String.Empty;

            if (this.hfIdProducto.Value != String.Empty)
            {
                AONAffinity.Motor.BusinessLogic.Bais.BLOpcionPoliza objBLOpcionPoliza = new AONAffinity.Motor.BusinessLogic.Bais.BLOpcionPoliza();
                AONAffinity.Motor.BusinessEntity.Bais.BEOpcionPoliza objBEOpcionPoliza = objBLOpcionPoliza.ObtenerxTipoDoc(Convert.ToInt32(this.hfIdProducto.Value), this.ddlPlan.SelectedValue, Convert.ToInt32(this.ddlFrecuencia.SelectedValue), this.ddlMonPrima.SelectedValue, this.ddlTipDocumento.SelectedValue, true);

                if (objBEOpcionPoliza != null)
                {
                    nroPoliza = objBEOpcionPoliza.NroPoliza;
                }
            }

            return nroPoliza;
        }

        private AONAffinity.Motor.BusinessEntity.Bais.BECertificado CargarCertificado()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLCertificado objBLCertificado = new AONAffinity.Motor.BusinessLogic.Bais.BLCertificado();
            AONAffinity.Motor.BusinessEntity.Bais.BECertificado objBECertificado = new AONAffinity.Motor.BusinessEntity.Bais.BECertificado();

            objBECertificado.IdProducto = Convert.ToInt32(this.hfIdProducto.Value);
            objBECertificado.Opcion = this.ddlPlan.SelectedValue;
            objBECertificado.IdFrecuencia = Convert.ToInt32(this.ddlFrecuencia.SelectedValue);
            objBECertificado.IdOficina = Convert.ToInt32(this.ddlOficina.SelectedValue); 
            objBECertificado.IdInformador = this.ddlInformador.SelectedValue; 
            objBECertificado.IdMedioPago = MedioPago.NoAplica;
            objBECertificado.IdEstadoCertificado = EstadoCertificado.PreActivo;
            objBECertificado.MontoAsegurado = Convert.ToDecimal(this.txtMontoAsegurado.Text);
            objBECertificado.PrimaBruta = Convert.ToDecimal(this.txtPrimaTot.Text.Trim());
            objBECertificado.Iva = 0;
            objBECertificado.PrimaTotal = Convert.ToDecimal(this.txtPrimaTot.Text.Trim());
            objBECertificado.PrimaCobrar = Convert.ToDecimal(this.txtPrimCob.Text.Trim());
            objBECertificado.NumeroCuenta = "000000000000";
            objBECertificado.Vencimiento = NullTypes.FechaNull;
            objBECertificado.Vigencia = objBLCertificado.ObtenerInicioVigencia(Convert.ToInt32(this.hfIdProducto.Value), Convert.ToDateTime(this.txtFinVigencia.Text.Trim()));
            objBECertificado.FinVigencia = objBLCertificado.ObtenerFinVigenciaxPlan(Convert.ToInt32(this.hfIdProducto.Value), objBECertificado.Vigencia, objBECertificado.Opcion);
            objBECertificado.Puntos = NullTypes.IntegerNull;
            objBECertificado.IdMotivoAnulacion = NullTypes.IntegerNull;
            objBECertificado.Consistente = false;
            objBECertificado.PuntosAnual = NullTypes.IntegerNull;
            objBECertificado.IdCiudad = Convert.ToInt32(this.ddlDistrito.SelectedValue);
            objBECertificado.Direccion = this.txtDireccion.Text.Trim().ToUpper();
            objBECertificado.Telefono = this.txtTelefono1.Text.Trim();
            objBECertificado.Nombre1 = this.txtNombre1.Text.Trim().ToUpper();
            objBECertificado.Nombre2 = this.txtNombre2.Text.Trim().ToUpper();
            objBECertificado.Apellido1 = this.txtApellidoPat.Text.Trim().ToUpper();
            objBECertificado.Apellido2 = this.txtApellidoMat.Text.Trim().ToUpper();
            objBECertificado.CcCliente = this.txtNroDocumento.Text.Trim();
            objBECertificado.IdTipoDocumento = this.ddlTipDocumento.SelectedValue;
            objBECertificado.Digitacion = DateTime.Now;
            objBECertificado.Incentivo = NullTypes.DecimalNull;
            objBECertificado.SaldoIncentivo = NullTypes.DecimalNull;
            objBECertificado.UsuarioCreacion = Session[NombreSession.Usuario].ToString();
            objBECertificado.Venta = DateTime.Now.Date;
            objBECertificado.IdMonedaPrima = this.ddlMonPrima.SelectedValue;
            objBECertificado.IdMonedaCobro = this.ddlMonPrima.SelectedValue;
            objBECertificado.AfiliadoHasta = NullTypes.FechaNull;

            return objBECertificado;
        }

        private List<AONAffinity.Motor.BusinessEntity.Bais.BEAsegurado> CargarAsegurado()
        {
            List<AONAffinity.Motor.BusinessEntity.Bais.BEAsegurado> lstBEAsegurado = new List<AONAffinity.Motor.BusinessEntity.Bais.BEAsegurado>();
            AONAffinity.Motor.BusinessEntity.Bais.BEAsegurado objBEAsegurado = new AONAffinity.Motor.BusinessEntity.Bais.BEAsegurado();

            objBEAsegurado.Consecutivo = 1;
            objBEAsegurado.IdTipodocumento = this.ddlTipDocumento.SelectedValue;
            objBEAsegurado.Ccaseg = this.txtNroDocumento.Text.Trim();
            objBEAsegurado.Nombre1 = this.txtNombre1.Text.Trim().ToUpper();
            objBEAsegurado.Nombre2 = this.txtNombre2.Text.Trim().ToUpper();
            objBEAsegurado.Apellido1 = this.txtApellidoPat.Text.Trim().ToUpper();
            objBEAsegurado.Apellido2 = this.txtApellidoMat.Text.Trim().ToUpper();
            objBEAsegurado.Direccion = this.txtDireccion.Text.Trim().ToUpper();
            objBEAsegurado.Telefono = this.txtTelefono2.Text.Trim();
            objBEAsegurado.IdCiudad = Convert.ToInt32(this.ddlDistrito.SelectedValue);
            objBEAsegurado.FechaNacimiento = Convert.ToDateTime(this.txtFecNacimiento.Text.Trim());
            objBEAsegurado.IdParentesco = 2;
            objBEAsegurado.Activo = true;
            lstBEAsegurado.Add(objBEAsegurado);
            return lstBEAsegurado;
        }

        private List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC> CargarInfoProductoC()
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLInfoProducto objBLInfoProducto = new AONAffinity.Motor.BusinessLogic.Bais.BLInfoProducto();
            List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoProducto> lstBEInfoProducto = new List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoProducto>();
            lstBEInfoProducto = objBLInfoProducto.ObtenerxProducto(Convert.ToInt32(this.hfIdProducto.Value));

            List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC> lstBEInfoProductoC = new List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC>();
            AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC objBEInfoProductoC = null;

            foreach (AONAffinity.Motor.BusinessEntity.Bais.BEInfoProducto objBEInfoProducto in lstBEInfoProducto)
            {
                objBEInfoProductoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoProductoC();

                if (objBEInfoProducto.Nombre == "SEXO")
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = this.ddlSexo.SelectedValue;
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }
                if (objBEInfoProducto.Nombre == "ESTADOCIVIL")
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = this.ddlEstadoCiv.SelectedValue;
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }
                if (objBEInfoProducto.Nombre == "FECNACIMIENTO")
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoProductoC.ValorDate = Convert.ToDateTime(this.txtFecNacimiento.Text);
                    objBEInfoProductoC.ValorString = NullTypes.CadenaNull;
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }
                if (objBEInfoProducto.Nombre == "DIRENTREGA")
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = this.txtDirEntrega.Text.Trim();
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }

                if (objBEInfoProducto.Nombre == "CODUBIGEOENTREGA")
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = Convert.ToDecimal(this.ddlDistEntrega.SelectedValue);
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = NullTypes.CadenaNull;
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }
                if (objBEInfoProducto.Nombre == "CODSPONSOR")
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = "01";
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }

                if (objBEInfoProducto.Nombre == "PERCOTIZACION")
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = NullTypes.CadenaNull;
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }

                if (objBEInfoProducto.Nombre == "NROCOTIZACION")
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = NullTypes.CadenaNull;
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }
                if (objBEInfoProducto.Nombre == "PREFDIRECCION")
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = this.ddlPrefijo.SelectedValue;
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }
                if (objBEInfoProducto.Nombre == "IDENTDOMICILIO")
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = this.txtMzLteNro.Text.Trim().ToUpper();
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }
                if (objBEInfoProducto.Nombre == "IDENTEDIFICIO")
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = this.txtAptoInt.Text.Trim().ToUpper();
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }
                if (objBEInfoProducto.Nombre == "REFDOMICILIO")
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = this.txtReferencia.Text.Trim().ToUpper();
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }
                if (objBEInfoProducto.Nombre == "URBANIZACION")
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = this.txtUrbanizacion.Text.Trim().ToUpper();
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }
                if (objBEInfoProducto.Nombre == "REFDOMICILIO2")
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = this.txtReferencia2.Text.Trim().ToUpper();
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }
                if (objBEInfoProducto.Nombre == "NROPOLIZA")
                {
                    objBEInfoProductoC.IdInfoProducto = objBEInfoProducto.IdInfoProducto;
                    objBEInfoProductoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoProductoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoProductoC.ValorString = this.ObtenerNroPoliza();
                    lstBEInfoProductoC.Add(objBEInfoProductoC);
                }
            }

            return lstBEInfoProductoC;
        }

        private List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC> CargarInfoAseguradoC()
        {
            BECategoria objBECategoria = (BECategoria)(ViewState["objBECategoria"]);
            BLModelo objBLModelo = new BLModelo();
            BEModelo objBEModelo = objBLModelo.Obtener(Convert.ToInt32(this.ddlModelo.SelectedValue));

            AONAffinity.Motor.BusinessLogic.Bais.BLInfoAsegurado objBLInfoAsegurado = new AONAffinity.Motor.BusinessLogic.Bais.BLInfoAsegurado();
            List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAsegurado> lstBEInfoAsegurado = (List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAsegurado>)(ViewState["InfoAsegurado"]);
                
            List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC> lstBEInfoAseguradoC = new List<AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC>();
            AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC objBEInfoAseguradoC = null;

            foreach (AONAffinity.Motor.BusinessEntity.Bais.BEInfoAsegurado objBEInfoAsegurado in lstBEInfoAsegurado)
            {
                objBEInfoAseguradoC = new AONAffinity.Motor.BusinessEntity.Bais.BEInfoAseguradoC();

                if (objBEInfoAsegurado.Nombre == "REFERENCIA")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = this.txtReferencia.Text.Trim().ToUpper();
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "MARCA")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = Convert.ToInt32(this.hfIdMarca.Value);
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = this.hfDesMarca.Value.ToUpper();
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "MODELO")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = Convert.ToInt32(this.hfIdModelo.Value);
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = this.hfDesModelo.Value.ToUpper();
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "COLOR")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = this.txtColor.Text.Trim().ToUpper();
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "AÑOFABRICACIÓN")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = Convert.ToDecimal(this.txtAnio.Text.Trim());
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull;
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "NUMERODECHASIS")
                {
                    objBEInfoAseguradoC.Nombre = objBEInfoAsegurado.Nombre;
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = this.txtNroChasis.Text.Trim().ToUpper();
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "NUMERODEMOTOR")
                {
                    objBEInfoAseguradoC.Nombre = objBEInfoAsegurado.Nombre;
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = this.txtNroMotor.Text.Trim().ToUpper();
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "NUMERODEMATRICULAPLACA")
                {
                    objBEInfoAseguradoC.Nombre = objBEInfoAsegurado.Nombre;
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = this.txtNroPlaca.Text.Trim().ToUpper();
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }

                if (objBEInfoAsegurado.Nombre == "CATEGORÍAVEHÍCULO")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = objBECategoria.IdCategoria;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull;
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "NUMERODEASIENTOS")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = Convert.ToDecimal(this.txtNroAsientos.Text.Trim());
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull;
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "INDICADORLIMAPROVINCIA")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    if (this.ddlDistrito.SelectedValue.ToString().Substring(0, 4) == "5115") { objBEInfoAseguradoC.ValorString = "L"; } else { objBEInfoAseguradoC.ValorString = "P"; }
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "INDICADORDEDSCTOPORFLOTA")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = "N";
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "NROVEHICULOSPORFLOTA")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull;
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "CLASEDEVEHÍCULOS")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = objBEModelo.IdClase;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = objBEModelo.DesClase;
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "USODEVEHÍCULOS")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = Convert.ToDecimal(this.ddlUso.SelectedValue);
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = this.ddlUso.SelectedItem.Text;
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }

                if (objBEInfoAsegurado.Nombre == "TIPODEVEHICULO")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = Convert.ToDecimal(this.ddlTipo.SelectedValue);
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = this.ddlTipo.SelectedItem.Text;
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }

                if (objBEInfoAsegurado.Nombre == "NUMEROTELÉFONO2")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = this.txtTelefono2.Text.Trim();
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "NUMEROTELÉFONO3")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull;
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }

                if (objBEInfoAsegurado.Nombre == "CORREOELECTRÓNICO")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = this.txtEmail.Text.Trim();
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "PORCENTAJEDSTORCGO")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = NullTypes.CadenaNull;
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "BLINDADO")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = "N";
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "TIMONCAMBIADO")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    if (this.ddlTimon.SelectedValue == "NO") { objBEInfoAseguradoC.ValorString = "N"; } else { objBEInfoAseguradoC.ValorString = "S"; }
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
                if (objBEInfoAsegurado.Nombre == "INIDICADORGPS")
                {
                    objBEInfoAseguradoC.IdInfoAsegurado = objBEInfoAsegurado.IdInfoAsegurado;
                    objBEInfoAseguradoC.Consecutivo = 1;
                    objBEInfoAseguradoC.ValorNum = NullTypes.DecimalNull;
                    objBEInfoAseguradoC.ValorDate = NullTypes.FechaNull;
                    objBEInfoAseguradoC.ValorString = "N";
                    lstBEInfoAseguradoC.Add(objBEInfoAseguradoC);
                }
            }

            return lstBEInfoAseguradoC;
        }
        #endregion
    }    
}