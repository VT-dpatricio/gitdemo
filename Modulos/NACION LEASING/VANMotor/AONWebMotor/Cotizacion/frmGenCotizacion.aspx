﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true" CodeBehind="frmGenCotizacion.aspx.cs" Inherits="AONWebMotor.Cotizacion.frmGenCotizacion" %>
<%@ Register src="../Controles/MsgBox.ascx" tagname="MsgBox" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="Form_SubTitulo" colspan="6">
                            GENERAR COTIZACIÓN
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" style="text-align:center;" >
                            Esta opción permitirá generar las cotizaciones masivas, debe tener en cuenta que 
                            primero se debe haber cargado los siniestros de los clientes.</td>                        
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td class="Form_TextoDer" >
                            Tipo Proceso:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlTipProceso" runat="server" Width="350px" 
                                AutoPostBack="True" onselectedindexchanged="ddlTipProceso_SelectedIndexChanged">                            
                            </asp:DropDownList> 
                        </td>
                        <td></td>
                        <td></td>                        
                    </tr>                    
                </table>
                <br />                
                <asp:GridView ID="gvProceso" runat="server" SkinID="sknGridView" 
                        AutoGenerateColumns="False" AllowPaging="True" PageSize="8" 
                    onrowcommand="gvProceso_RowCommand" 
                    onrowdatabound="gvProceso_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="IdProceso" HeaderText="Código">
                            <HeaderStyle HorizontalAlign="Center" Width="50px" />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Archivo" HeaderText="Archivo">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Fecha Carga">
                            <ItemTemplate>
                                <asp:Label ID="lblFecProceso" runat="server"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Log">
                            <ItemTemplate>
                                <asp:ImageButton ID="ibtnLog" runat="server" CommandName="GenerarLog" 
                                    SkinID="sknImgEditar" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="50px" />                            
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cotizar">
                            <ItemTemplate>
                                <asp:ImageButton ID="ibtnCotizar" runat="server" CommandName="Cotizar" 
                                    SkinID="sknIbtnExport" Width="16px" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="50px" />
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
            <td class="Form_tdBorde">                
            </td>            
        </tr>
    </table>
    <uc1:MsgBox ID="msgMensaje" runat="server" />
</asp:Content>
