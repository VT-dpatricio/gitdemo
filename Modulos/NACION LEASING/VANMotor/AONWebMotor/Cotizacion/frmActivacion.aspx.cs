﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Motor.Resources.Constantes;
using AONAffinity.Motor.BusinessLogic;
using AONAffinity.Motor.BusinessEntity;

namespace AONWebMotor.Cotizacion
{
    public partial class frmActivacion : PageBase
    {
        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            
            
            try
            {
                if (!Page.IsPostBack)
                {
                    if (!ValidarAcceso(Page.AppRelativeVirtualPath))
                    {
                        Response.Redirect("~/Login.aspx", false);
                    }
                    
                    if (ValidarEstadoClaveUsuario())
                    {
                        Response.Redirect("~/Seguridad/CambiarClave.aspx", false);
                    }
                    this.CargarFormulario();
                }
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, "Se presentó un problema al cargar el formulario. " + ex.Message, false);
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try 
            {
                this.BuscarCotizacion();
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, "Se presentó un problema al realizar la búsqueda. " + ex.Message, false);
            }
        }

        protected void gvCotizacion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try 
            {
                if (e.Row.RowType == DataControlRowType.DataRow) 
                {
                    e.Row.Attributes.Add("OnMouseOut", "this.className = this.orignalclassName;");
                    e.Row.Attributes.Add("OnMouseOver", "this.orignalclassName = this.className;this.className = 'GridWiew_SelectedRow';");
               
                
                
                }
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void gvCotizacion_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                switch (e.CommandName) 
                {
                    case  "Detalle":
                        break;
                }
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void gvCotizacion_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                this.gvCotizacion.PageIndex = e.NewPageIndex;
                List<BECotizacion> lstBECotizacion = (List<BECotizacion>)(ViewState[Lista.lstCotizacion]);
                this.CargarGridView(this.gvCotizacion, lstBECotizacion);
            }
            catch (Exception ex) 
            {

            }
        }
        #endregion

        #region Métodos
        // <summary>
        /// Permite cargar el formulario.
        /// </summary>
        private void CargarFormulario()
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/           

            this.CargarCabecera();
        }

        /// <summary>
        /// Permite cargar la cabecera de la grilla de consulta.
        /// </summary>
        private void CargarCabecera()
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/
            
            if (this.gvCotizacion.Rows.Count == 0)
            {
                List<BECotizacion> lstBECotizacion = new List<BECotizacion>();
                BECotizacion objBECotizacion = new BECotizacion();
                lstBECotizacion.Add(objBECotizacion);
                this.CargarGridView(this.gvCotizacion, lstBECotizacion);
                this.gvCotizacion.Rows[0].Visible = false;
            }
        }

        /// <summary>
        /// Permite buscar la información de la cotización.
        /// </summary>
        private void BuscarCotizacion()
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            this.lblResultado.Visible = false;
            BLCotizacion objBLCotizacion = new BLCotizacion();
            List<BECotizacion> lstBECotizacion = objBLCotizacion.Buscar(Convert.ToInt32(this.ddlTipoBusq.SelectedValue), this.txtDNI.Text.Trim(), String.Empty, String.Empty, String.Empty, String.Empty, this.txtNroPlaca.Text, Session[AONAffinity.Motor.Resources.NombreSession.Usuario].ToString());

            ViewState[Lista.lstCotizacion] = lstBECotizacion;

            this.CargarGridView(this.gvCotizacion, lstBECotizacion);
            
            if (this.gvCotizacion.Rows.Count == 0)
            {
                this.CargarCabecera();
                this.lblResultado.Visible = true;
            }
        }
        #endregion
        //Soluciona problema de compatibilidad en IE7
        protected override void Render(HtmlTextWriter writer)
        {
            Control meta = Header.FindControl(DevExpress.Web.ASPxClasses.Internal.RenderUtils.IE8CompatibilityMetaID);
            if (meta != null) meta.Parent.Controls.Remove(meta);

            base.Render(writer);
        }


    }
}