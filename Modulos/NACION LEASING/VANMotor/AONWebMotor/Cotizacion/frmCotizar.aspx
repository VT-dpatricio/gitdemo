﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true"
    CodeBehind="frmCotizar.aspx.cs" Inherits="AONWebMotor.Cotizacion.frmCotizar" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v8.3" Namespace="DevExpress.Web.ASPxEditors"
    TagPrefix="dxe" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v8.3" Namespace="DevExpress.Web.ASPxRoundPanel"
    TagPrefix="dxrp" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="DevExpress.Web.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v8.3" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v8.3" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v8.3" Namespace="DevExpress.Web.ASPxPanel"
    TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v8.3" Namespace="DevExpress.Web.ASPxLoadingPanel"
    TagPrefix="dxlp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <dxtc:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" 
                    Width="100%">
                    <TabPages>
                        <dxtc:TabPage Name="tpProducto" Text="Paso 1: Cotización">
                            <ContentCollection>
                                <dxw:ContentControl runat="server" SupportsDisabledAttribute="True">
                                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel7" runat="server" Width="100%" HeaderText="DATOS DEL CLIENTE">
                                        <PanelCollection>
                                            <dxp:PanelContent ID="PanelContent7" runat="server" SupportsDisabledAttribute="True">
                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" class="Form_Fondo">
                                                    <tr>
                                                        <td class="Form_TextoDer">
                                                            Cliente:
                                                        </td>
                                                        <td class="Form_TextoIzq" colspan="2">
                                                            <asp:TextBox ID="txtCliente" runat="server" Width="200px" 
                                                                CssClass="Form_TextBox"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Nro. Documento:
                                                        </td>
                                                         <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="TextBox1" runat="server" Width="120px" CssClass="Form_TextBox"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr style="border: 0;">
                                                        <td class="Form_TextoDer">
                                                            Departamento:
                                                        </td>
                                                        <td class="Form_TextoIzq" style="width: 120px;">
                                                            <asp:UpdatePanel ID="upDep" runat="server">
                                                                <ContentTemplate>
                                                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                                        <tr style="border: 0;">
                                                                            <td style="width: 110px;">
                                                                                <asp:DropDownList ID="ddlDep" runat="server" Width="105px" AutoPostBack="True" 
                                                                                    onselectedindexchanged="ddlDep_SelectedIndexChanged">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td>
                                                                                <span class="Form_TextoObligatorio">*</span>
                                                                            </td>
                                                                            <td style="width: 10px;">
                                                                                <asp:UpdateProgress ID="upsDep" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upDep">
                                                                                    <ProgressTemplate>
                                                                                        <asp:Image ID="imgLoadD" runat="server" SkinID="sknImgLoading" Width="5px" Height="5px" />
                                                                                    </ProgressTemplate>
                                                                                </asp:UpdateProgress>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Provincia:
                                                        </td>
                                                        <td class="Form_TextoIzq" style="width: 130px;">
                                                            <asp:UpdatePanel ID="upProv" runat="server">
                                                                <ContentTemplate>
                                                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                                        <tr style="border: 0;">
                                                                            <td style="width: 110px">
                                                                                <asp:DropDownList ID="ddlProv" runat="server" Width="105px" AutoPostBack="True" 
                                                                                    onselectedindexchanged="ddlProv_SelectedIndexChanged">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td>
                                                                                <span class="Form_TextoObligatorio">*</span>
                                                                            </td>
                                                                            <td style="width: 10px;">
                                                                                <asp:UpdateProgress ID="upsProv" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upProv">
                                                                                    <ProgressTemplate>
                                                                                        <asp:Image ID="imgLoadP" runat="server" SkinID="sknImgLoading" Width="10px" Height="10px" />
                                                                                    </ProgressTemplate>
                                                                                </asp:UpdateProgress>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="ddlDep" EventName="selectedIndexChanged" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Distrito:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:UpdatePanel ID="upDis" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:DropDownList ID="ddlDis" runat="server" Width="105px">
                                                                    </asp:DropDownList>
                                                                    <span class="Form_TextoObligatorio">*</span>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="ddlProv" EventName="selectedIndexChanged" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div style="display: none;">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlDep"
                                                        Display="None" ErrorMessage="Seleccione el departamento del cliente." ForeColor="Transparent"
                                                        InitialValue="-1" SetFocusOnError="true" Text="*" ValidationGroup="valCotizar">
                                                    </asp:RequiredFieldValidator>
                                                 </div>
                                            </dxp:PanelContent>
                                        </PanelCollection>
                                    </dxrp:ASPxRoundPanel>
                                    <br />
                                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel5" runat="server" Width="100%" HeaderText="DATOS DEL VEHÍCULO">
                                        <PanelCollection>
                                            <dxp:PanelContent ID="PanelContent5" runat="server" SupportsDisabledAttribute="True">
                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" class="Form_Fondo">
                                                    <tr>
                                                        <td class="Form_TextoDer">
                                                            Tipo Vehículo:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:DropDownList ID="ddlTipo" runat="server" Width="90px">
                                                            </asp:DropDownList>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Marca:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:DropDownList ID="ddlMarca" runat="server" Width="90px" AutoPostBack="True" OnSelectedIndexChanged="ddlMarca_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Modelo:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:DropDownList ID="ddlModelo" runat="server" Width="150px">
                                                            </asp:DropDownList>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Año:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:DropDownList ID="ddlAnioFab" runat="server" Width="85px">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Form_TextoDer">
                                                            Uso Vehículo:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:DropDownList ID="ddlUso" runat="server" Width="90px">
                                                            </asp:DropDownList>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Valor USD:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtValVehiculo" runat="server" CssClass="Form_TextBoxMonto" Width="80px"
                                                                MaxLength="9"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                            <cc1:FilteredTextBoxExtender ID="ftetValVehiculo" runat="server" TargetControlID="txtValVehiculo"
                                                                ValidChars="0123456789.">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Timón Cambiado:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:DropDownList ID="ddlTimon" runat="server" Width="85px">
                                                                <asp:ListItem Text="NO" Value="NO"></asp:ListItem>
                                                                <asp:ListItem Text="SI" Value="SI"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div style="display: none;">                                                  
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlTipo"
                                                        Display="None" ErrorMessage="Seleccione el tipo de vehículo." ForeColor="Transparent"
                                                        InitialValue="-1" SetFocusOnError="true" Text="*" ValidationGroup="valCotizar">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlMarca"
                                                        Display="None" ErrorMessage="Seleccione la marca del vehículo." ForeColor="Transparent"
                                                        InitialValue="-1" SetFocusOnError="true" Text="*" ValidationGroup="valCotizar">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlModelo"
                                                        Display="None" ErrorMessage="Seleccione el modelo del vehículo." ForeColor="Transparent"
                                                        InitialValue="-1" SetFocusOnError="true" Text="*" ValidationGroup="valCotizar">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtValVehiculo"
                                                        Display="None" ErrorMessage="Ingrese el valor del vehículo." ForeColor="Transparent"
                                                        SetFocusOnError="true" Text="*" ValidationGroup="valCotizar">
                                                    </asp:RequiredFieldValidator>
                                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ddlUso"
                                                        Display="None" ErrorMessage="Seleccione el uso del vehículo." ForeColor="Transparent"
                                                        InitialValue="-1" SetFocusOnError="true" Text="*" ValidationGroup="valCotizar">
                                                    </asp:RequiredFieldValidator>
                                                </div>  
                                            </dxp:PanelContent>
                                        </PanelCollection>
                                    </dxrp:ASPxRoundPanel>
                                    <br />
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="Form_TextoIzq">
                                                <span class="Form_TextoObligatorio">&nbsp;(*)Campos Obligatorios</span>
                                            </td>
                                            <td class="Form_TextoDer">
                                                <asp:Button ID="btnCotizar" runat="server" Text="Cotizar" OnClick="btnCotizar_Click"
                                                    ValidationGroup="valCotizar" Width="90px" ToolTip="Click para generar cotización." />
                                                <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" OnClick="btnNuevo_Click" Width="90px"
                                                    ToolTip="Click para genear nueva cotización." />
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel6" runat="server" Width="100%" HeaderText="DETALLE COTIZACIÓN">
                                        <PanelCollection>
                                            <dxp:PanelContent ID="PanelContent6" runat="server" SupportsDisabledAttribute="True">
                                                <asp:Panel ID="pnOpcion" runat="server">
                                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" class="Form_Fondo">
                                                        <tr>
                                                            <td class="Form_TextoDer">
                                                                Valor Comercial USD:
                                                            </td>
                                                            <td class="Form_TextoIzq">
                                                                <asp:TextBox ID="txtValorVehiculoDep" runat="server" CssClass="Form_TextBoxMontoDisable"
                                                                    ReadOnly="True" Width="90px"></asp:TextBox>
                                                            </td>
                                                            <td class="Form_TextoDer">
                                                                Cambiar Valor:
                                                            </td>
                                                            <td class="Form_TextoDer" colspan="2">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td class="Form_TextoIzq">
                                                                            <asp:DropDownList ID="ddlSigno" runat="server" Width="90px" AutoPostBack="True" OnSelectedIndexChanged="ddlSigno_SelectedIndexChanged">
                                                                                <asp:ListItem Text="Seleccione" Value="-1"></asp:ListItem>
                                                                                <asp:ListItem Text="AUMENTAR" Value="+"></asp:ListItem>
                                                                                <asp:ListItem Text="DISMINUIR" Value="-"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <asp:RequiredFieldValidator ID="rfvSigno" runat="server" Text="*" ControlToValidate="ddlSigno"
                                                                                Display="None" ErrorMessage="Seleccione signo de porcentaje" SetFocusOnError="true"
                                                                                InitialValue="-1" ValidationGroup="CambiarValor">
                                                                            </asp:RequiredFieldValidator>
                                                                        </td>
                                                                        <td class="Form_TextoIzq">
                                                                            <asp:UpdatePanel ID="upSigno" runat="server">
                                                                                <ContentTemplate>
                                                                                    <asp:DropDownList ID="ddlPorcentaje" runat="server" Width="50px">
                                                                                    </asp:DropDownList>
                                                                                    %
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:AsyncPostBackTrigger ControlID="ddlSigno" EventName="SelectedIndexChanged" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                        <td>
                                                                            <asp:ImageButton ID="ibtnCambiarValVeh" runat="server" SkinID="sknIbtnCambiar" Width="20"
                                                                                Height="20" OnClick="ibtnCambiarValVeh_Click" ToolTip="Click para cambiar el valor del vehículo." />
                                                                        </td>
                                                                        <td>
                                                                            <asp:ImageButton ID="ibtnValorOri" runat="server" SkinID="sknIbtnAtras" Width="20"
                                                                                Height="20" OnClick="ibtnValorOri_Click" ToolTip="Click para revertir el valor del vehículo." />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="Form_TextoDer">
                                                                <asp:Label ID="lblValorAnt" runat="server" Text="Valor Anterior USD:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtValorAnt" runat="server" CssClass="Form_TextBoxMontoDisable"
                                                                    ReadOnly="True" Width="90px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="Form_TextoDer">
                                                                Tasa Anual:
                                                            </td>
                                                            <td class="Form_TextoIzq">
                                                                <asp:TextBox ID="txtTasaAnual" runat="server" CssClass="Form_TextBoxMontoDisable"
                                                                    Width="90px" ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                            <td class="Form_TextoDer">
                                                                Prima Anual:
                                                            </td>
                                                            <td class="Form_TextoIzq">
                                                                <asp:TextBox ID="txtPrimaAnual" runat="server" CssClass="Form_TextBoxMontoDisable"
                                                                    Width="90px" ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                            <td class="Form_TextoDer">
                                                                Prima Anual Mensual
                                                            </td>
                                                            <td class="Form_TextoIzq">
                                                                <asp:TextBox ID="txtPrimaAnualMen" runat="server" CssClass="Form_TextBoxMontoDisable"
                                                                    Width="90px" ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                            <td class="Form_TextoIzq">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="Form_TextoDer">
                                                                Tasa Bianual:
                                                            </td>
                                                            <td class="Form_TextoIzq">
                                                                <asp:TextBox ID="txtTasaBianual" runat="server" CssClass="Form_TextBoxMontoDisable"
                                                                    Width="90px" ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                            <td class="Form_TextoDer">
                                                                Prima Bianual:
                                                            </td>
                                                            <td class="Form_TextoIzq">
                                                                <asp:TextBox ID="txtPrimaBianual" runat="server" CssClass="Form_TextBoxMontoDisable"
                                                                    Width="90px" ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                            <td class="Form_TextoDer">
                                                                Prima Bianual Mensual:
                                                            </td>
                                                            <td class="Form_TextoIzq">
                                                                <asp:TextBox ID="txtPrimaBianualMen" runat="server" CssClass="Form_TextBoxMontoDisable"
                                                                    Width="90px" ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                            <td class="Form_TextoIzq">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </dxp:PanelContent>
                                        </PanelCollection>
                                    </dxrp:ASPxRoundPanel>
                                    <br />
                                    <div style="width:100%; overflow:auto;" >
                                        <asp:GridView ID="gvCotizacion" runat="server" Width="99%" 
                                            SkinID="sknGridView" Visible="False" >
                                            <Columns>
                                                <asp:BoundField DataField="idPlan" HeaderText="Plan"></asp:BoundField>
                                                <asp:BoundField HeaderText="Valor Vehículo"></asp:BoundField>
                                                <asp:BoundField DataField="Tasa" HeaderText="Tasa"></asp:BoundField>
                                                <asp:BoundField HeaderText="Moneda Prima">
                                                    <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                                    <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Prima Total"></asp:BoundField>
                                                <asp:BoundField HeaderText="Prima Mensual"></asp:BoundField>
                                                <asp:TemplateField>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ImageButton1" runat="server" SkinID="sknIbtnSiguiente" />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Width="40px" />
                                                    <ItemStyle HorizontalAlign="Center" Width="40px" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div> 
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="valCotizar"
                                        ShowMessageBox="true" ShowSummary="false" />
                                </dxw:ContentControl>
                            </ContentCollection>
                        </dxtc:TabPage>
                        <dxtc:TabPage Name="tpExpedir" Text="Paso 2: Expedir">
                            <ContentCollection>
                                <dxw:ContentControl ID="ContentControl2" runat="server" SupportsDisabledAttribute="True"
                                    Width="100%">
                                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" Width="100%" HeaderText="INFORMACIÓN DEL CERTIFICADO/CLIENTE">
                                        <PanelCollection>
                                            <dxp:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                                                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                    <tr>
                                                        <td class="Form_TextoDer">
                                                            Informador:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:DropDownList ID="ddlInformador" runat="server" Width="105px">
                                                            </asp:DropDownList>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Oficina:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:DropDownList ID="ddlOficina" runat="server" Width="105px">
                                                            </asp:DropDownList>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Fecha:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtFechaVenta" runat="server" CssClass="Form_TextBoxDisable" Width="90px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Fin Vigencia:</td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtFinVigencia" runat="server" CssClass="Form_TextBox" 
                                                                Width="90px"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                            <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txtFinVigencia"
                                                                Mask="99/99/9999" MaskType="Date">
                                                            </cc1:MaskedEditExtender>
                                                        </td>
                                                    </tr>
                                                    <tr style="border: 0;">
                                                        <td class="Form_TextoDer">
                                                            Tipo Doc.:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:DropDownList ID="ddlTipDocumento" runat="server" Width="90px">
                                                                <asp:ListItem Text="DNI" Value="L"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Nro. Doc.:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtNroDocumento" runat="server" CssClass="Form_TextBox" MaxLength="8"
                                                                Width="90px"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                            <cc1:FilteredTextBoxExtender ID="fteNroDocumento" runat="server" TargetControlID="txtNroDocumento"
                                                                ValidChars="0123456789" Enabled="True">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            1er Nombre:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtNombre1" runat="server" CssClass="Form_TextBox" MaxLength="60"
                                                                Width="90px"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                            <cc1:FilteredTextBoxExtender ID="fteNombre1" runat="server" TargetControlID="txtNombre1"
                                                                ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnñopqrstuvwxyzÁÉÍÓÚÜáéíóúü"
                                                                Enabled="True">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            2do Nombre:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtNombre2" runat="server" CssClass="Form_TextBox" MaxLength="60"
                                                                Width="90px"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="fteNombre2" runat="server" TargetControlID="txtNombre2"
                                                                ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnñopqrstuvwxyzÁÉÍÓÚÜáéíóúü"
                                                                Enabled="True">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </td>
                                                    </tr>
                                                    <tr style="border: 0;">
                                                        <td class="Form_TextoDer">
                                                            Sexo:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:DropDownList ID="ddlSexo" runat="server" Width="90px">
                                                                <asp:ListItem Text="MÁSCULINO" Value="M"></asp:ListItem>
                                                                <asp:ListItem Text="FEMENINO" Value="F"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Estado Civil:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:DropDownList ID="ddlEstadoCiv" runat="server" Width="95px">
                                                                <asp:ListItem Text="SOLTERO" Value="S"></asp:ListItem>
                                                                <asp:ListItem Text="CASADO" Value="C"></asp:ListItem>
                                                                <asp:ListItem Text="VIUDO" Value="V"></asp:ListItem>
                                                                <asp:ListItem Text="DIVORCIADO" Value="D"></asp:ListItem>
                                                                <asp:ListItem Text="SEPARADO" Value="X"></asp:ListItem>
                                                                <asp:ListItem Text="UNIÓN LIBRE" Value="U"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            1er Apellido:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtApellidoPat" runat="server" CssClass="Form_TextBox" MaxLength="60"
                                                                Width="90px">
                                                            </asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                            <cc1:FilteredTextBoxExtender ID="fteApellidoPat" runat="server" TargetControlID="txtApellidoPat"
                                                                ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnñopqrstuvwxyzÁÉÍÓÚÜáéíóúü"
                                                                Enabled="True">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            2do Apellido:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtApellidoMat" runat="server" CssClass="Form_TextBox" MaxLength="60"
                                                                Width="90px"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                            <cc1:FilteredTextBoxExtender ID="fteApellidoMat" runat="server" TargetControlID="txtApellidoMat"
                                                                ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnñopqrstuvwxyzÁÉÍÓÚÜáéíóúü"
                                                                Enabled="True">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </td>
                                                    </tr>
                                                    <tr style="border: 0;">
                                                        <td class="Form_TextoDer">
                                                            Fec. Nacimiento:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtFecNacimiento" runat="server" Width="90px" CssClass="Form_TextBox">
                                                            </asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                            <cc1:MaskedEditExtender ID="meeFecNacimiento" runat="server" TargetControlID="txtFecNacimiento"
                                                                Mask="99/99/9999" MaskType="Date">
                                                            </cc1:MaskedEditExtender>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Teléfono 1:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtTelefono1" runat="server" MaxLength="15" Width="90px" CssClass="Form_TextBox"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                            <cc1:FilteredTextBoxExtender ID="fteTelDomicio1" runat="server" TargetControlID="txtTelefono1"
                                                                ValidChars="0123456789*-" Enabled="True">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Teléfono 2:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtTelefono2" runat="server" MaxLength="15" Width="90px" CssClass="Form_TextBox"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="fteTelefono2" runat="server" TargetControlID="txtTelefono2"
                                                                ValidChars="0123456789*-" Enabled="True">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Email:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtEmail" runat="server" CssClass="Form_TextBox" Width="90px"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr style="border: 0;">
                                                        <td class="Form_TextoDer">
                                                            Mz/Lte/Nro:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtMzLteNro" runat="server" MaxLength="100" CssClass="Form_TextBox"
                                                                Width="90px">
                                                            </asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Apto/Int:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtAptoInt" runat="server" MaxLength="100" CssClass="Form_TextBox"
                                                                Width="90px">                                    
                                                            </asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Prefijo:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:DropDownList ID="ddlPrefijo" runat="server" Width="90px">
                                                                <asp:ListItem Text="CALLE" Value="CALLE"></asp:ListItem>
                                                                <asp:ListItem Text="AVENIDA" Value="AV"></asp:ListItem>
                                                                <asp:ListItem Text="JIRÓN" Value="JR"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Urbanización
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtUrbanizacion" runat="server" CssClass="Form_TextBox" MaxLength="15"
                                                                Width="90px"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr style="border: 0;">
                                                        <td class="Form_TextoDer">
                                                            Departamento:
                                                        </td>
                                                        <td class="Form_TextoIzq" style="width: 120px;">
                                                            <asp:UpdatePanel ID="upDepartamento" runat="server">
                                                                <ContentTemplate>
                                                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                                        <tr style="border: 0;">
                                                                            <td style="width: 110px;">
                                                                                <asp:DropDownList ID="ddlDepartamento" runat="server" Width="105px" AutoPostBack="True"
                                                                                    OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td>
                                                                                <span class="Form_TextoObligatorio">*</span>
                                                                            </td>
                                                                            <td style="width: 10px;">
                                                                                <asp:UpdateProgress ID="upsDepartamento" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upDepartamento">
                                                                                    <ProgressTemplate>
                                                                                        <asp:Image ID="imgLoadDep" runat="server" SkinID="sknImgLoading" Width="5px" Height="5px" />
                                                                                    </ProgressTemplate>
                                                                                </asp:UpdateProgress>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Provincia:
                                                        </td>
                                                        <td class="Form_TextoIzq" style="width: 120px;">
                                                            <asp:UpdatePanel ID="upProvincia" runat="server">
                                                                <ContentTemplate>
                                                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                                        <tr style="border: 0;">
                                                                            <td style="width: 110px">
                                                                                <asp:DropDownList ID="ddlProvincia" runat="server" Width="105px" AutoPostBack="True"
                                                                                    OnSelectedIndexChanged="ddlProvincia_SelectedIndexChanged">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td>
                                                                                <span class="Form_TextoObligatorio">*</span>
                                                                            </td>
                                                                            <td style="width: 10px;">
                                                                                <asp:UpdateProgress ID="upsProvincia" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upProvincia">
                                                                                    <ProgressTemplate>
                                                                                        <asp:Image ID="imgLoadProv" runat="server" SkinID="sknImgLoading" Width="5px" Height="5px" />
                                                                                    </ProgressTemplate>
                                                                                </asp:UpdateProgress>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="ddlDepartamento" EventName="selectedIndexChanged" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Distrito:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:UpdatePanel ID="upDistrito" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:DropDownList ID="ddlDistrito" runat="server" Width="105px">
                                                                    </asp:DropDownList>
                                                                    <span class="Form_TextoObligatorio">*</span>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="ddlProvincia" EventName="selectedIndexChanged" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr style="border: 0;">
                                                        <td class="Form_TextoDer">
                                                            Dirección:
                                                        </td>
                                                        <td class="Form_TextoIzq" colspan="2">
                                                            <asp:TextBox ID="txtDireccion" runat="server" CssClass="Form_TextBox" MaxLength="300"
                                                                Width="180px"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Referencias:
                                                        </td>
                                                        <td class="Form_TextoDer" colspan="2">
                                                            1ra:<asp:TextBox ID="txtReferencia" runat="server" CssClass="Form_TextBox" MaxLength="195"
                                                                Width="140px"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td class="Form_TextoDer" colspan="2">
                                                            2da:
                                                            <asp:TextBox ID="txtReferencia2" runat="server" CssClass="Form_TextBox" MaxLength="195"
                                                                Width="140px"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div style="display: none;">
                                                    <asp:RequiredFieldValidator ID="rfvInformador" runat="server" Text="*" ControlToValidate="ddlInformador"
                                                        Display="None" ErrorMessage="Seleccione Informador." ForeColor="Transparent"
                                                        SetFocusOnError="true" InitialValue="-1" ValidationGroup="valCliente">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvOficina" runat="server" Text="*" ControlToValidate="ddlOficina"
                                                        Display="None" ErrorMessage="Seleccione Oficina." ForeColor="Transparent"
                                                        SetFocusOnError="true" InitialValue="-1" ValidationGroup="valCliente">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvFinVigencia" runat="server" ControlToValidate="txtFinVigencia"
                                                        ValidationGroup="valCliente" Display="None" ErrorMessage="Fin Vigencia es obligatorio."
                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvNombre1" runat="server" ControlToValidate="txtNombre1"
                                                        ValidationGroup="valCliente" Display="None" ErrorMessage="1er nombre es obligatorio."
                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvNroDocumento" runat="server" ControlToValidate="txtNroDocumento"
                                                        ValidationGroup="valCliente" Display="None" ErrorMessage="Nro. documento obligatorio."
                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvApellidoPat" runat="server" ControlToValidate="txtApellidoPat"
                                                        ValidationGroup="valCliente" Display="None" ErrorMessage="1er apellido es obligatorio."
                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvApellidoMat" runat="server" ControlToValidate="txtApellidoMat"
                                                        ValidationGroup="valCliente" Display="None" ErrorMessage="2do apellido es obligatorio."
                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvFecNacimiento" runat="server" ControlToValidate="txtFecNacimiento"
                                                        ValidationGroup="valCliente" Display="None" ErrorMessage="Fecha de nacimiento es obligatorio">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvDireccion" runat="server" ControlToValidate="txtDireccion"
                                                        ValidationGroup="valCliente" Display="None" ErrorMessage="Dirección es obligatorio."
                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvRefrencia" runat="server" ControlToValidate="txtReferencia"
                                                        ValidationGroup="valCliente" Display="None" ErrorMessage="Referencia 1 es obligatorio."
                                                        SetFocusOnError="True">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvRefrencia2" runat="server" ControlToValidate="txtReferencia2"
                                                        ValidationGroup="valCliente" Display="None" ErrorMessage="Referencia 2 es obligarotio."
                                                        SetFocusOnError="true">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvMzLteNro" runat="server" ControlToValidate="txtMzLteNro"
                                                        ValidationGroup="valCliente" Display="None" ErrorMessage="Mz/Lte/Nro es obligatorio."
                                                        SetFocusOnError="true">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvAptoInt" runat="server" ControlToValidate="txtAptoInt"
                                                        ValidationGroup="valCliente" Display="None" ErrorMessage="Apto/Int es obligatorio."
                                                        SetFocusOnError="true">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvUrbanizacion" runat="server" ControlToValidate="txtUrbanizacion"
                                                        ValidationGroup="valCliente" Display="None" ErrorMessage="Urbanización es obligatorio."
                                                        SetFocusOnError="True">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvTelefono1" runat="server" ControlToValidate="txtTelefono1"
                                                        ValidationGroup="valCliente" Display="None" ErrorMessage="Teléfono es obligatorio."
                                                        SetFocusOnError="true">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvDepartamento" runat="server" Text="*" ControlToValidate="ddlDepartamento"
                                                        Display="None" ErrorMessage="Seleccione departamento." ForeColor="Transparent"
                                                        SetFocusOnError="true" InitialValue="-1" ValidationGroup="valCliente">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvProvincia" runat="server" Text="*" ControlToValidate="ddlProvincia"
                                                        Display="None" ErrorMessage="Seleccione provincia." ForeColor="Transparent" SetFocusOnError="true"
                                                        InitialValue="-1" ValidationGroup="valCliente">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvDistrito" runat="server" ControlToValidate="ddlDistrito"
                                                        Display="None" ErrorMessage="Seleccione distrito." ForeColor="Transparent" InitialValue="-1"
                                                        SetFocusOnError="true" Text="*" ValidationGroup="valCliente">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                                                        Display="None" ErrorMessage="Email es obligatorio." SetFocusOnError="true" ValidationGroup="valCliente">
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                            </dxp:PanelContent>
                                        </PanelCollection>
                                    </dxrp:ASPxRoundPanel>
                                    <br />
                                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" Width="100%" HeaderText="DATOS DEL VEHÍCULO">
                                        <PanelCollection>
                                            <dxp:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True">
                                                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                    <tr style="border: 0;">
                                                        <td class="Form_TextoDer">
                                                            Marca:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtMarca" runat="server" CssClass="Form_TextBoxDisable" Width="90px"
                                                                ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Modelo:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtModelo" runat="server" CssClass="Form_TextBoxDisable" Width="90px"
                                                                ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Año Fab.:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtAnio" runat="server" CssClass="Form_TextBoxDisable" Width="90px"
                                                                ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Nro Motor:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtNroMotor" runat="server" MaxLength="30" CssClass="Form_TextBox"
                                                                Width="90px"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr style="border: 0;">
                                                        <td class="Form_TextoDer">
                                                            Nro Placa:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtNroPlaca" runat="server" CssClass="Form_TextBox" Width="90px"
                                                                MaxLength="20"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                            <cc1:FilteredTextBoxExtender ID="fteNroPlaca" runat="server" TargetControlID="txtNroPlaca"
                                                                ValidChars="1234567890-abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ"
                                                                Enabled="True">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Color:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtColor" runat="server" CssClass="Form_TextBox" Width="90px" MaxLength="25">
                                                            </asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                            <cc1:FilteredTextBoxExtender ID="fteColor" runat="server" TargetControlID="txtColor"
                                                                ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnñopqrstuvwxyz">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Tipo Vehículo:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtTipVehiculo" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Nro. Asientos:</td>
                                                        <td>
                                                            <asp:TextBox ID="txtNroAsientos" runat="server" CssClass="Form_TextBox" 
                                                                Width="90px" MaxLength="1"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span></td>
                                                    </tr>
                                                    <tr style="border: 0;">
                                                        <td class="Form_TextoDer">
                                                            Nro. Chasis:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <cc1:FilteredTextBoxExtender ID="fteNroAsientos" runat="server" TargetControlID="txtNroAsientos"
                                                                ValidChars="1234567890" Enabled="True">
                                                            </cc1:FilteredTextBoxExtender>
                                                            <asp:TextBox ID="txtNroChasis" runat="server" CssClass="Form_TextBox" 
                                                                MaxLength="30" Width="90px"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Valor Vehículo:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtMontoAsegurado" runat="server" 
                                                                CssClass="Form_TextBoxMontoDisable" ReadOnly="True" Width="90px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            &nbsp;</td>
                                                        <td class="Form_TextoIzq">
                                                            &nbsp;</td>
                                                        <td class="Form_TextoDer">
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div style="display: none;">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtNroMotor"
                                                        SetFocusOnError="True" Display="None" ErrorMessage="Ingrese el nro. motor." ValidationGroup="valCliente"></asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvNroPlaca" runat="server" ControlToValidate="txtNroPlaca"
                                                        SetFocusOnError="True" Display="None" ErrorMessage="Nro. placa es obligatorio"
                                                        ValidationGroup="valCliente"></asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvColor" runat="server" ControlToValidate="txtColor"
                                                        SetFocusOnError="True" Display="None" ErrorMessage="Color del vehículo es obligatorio"
                                                        ValidationGroup="valCliente"></asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvNroAsientos" runat="server" ControlToValidate="txtNroAsientos"
                                                        SetFocusOnError="True" Display="None" ErrorMessage="Nro. de asientos es obligatorio."
                                                        ValidationGroup="valCliente">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvNroChasis" runat="server" ControlToValidate="txtNroChasis"
                                                        SetFocusOnError="true" Display="None" ErrorMessage="Nro. de chasis es obligatorio."
                                                        ValidationGroup="valCliente">
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                            </dxp:PanelContent>
                                        </PanelCollection>
                                    </dxrp:ASPxRoundPanel>
                                    <br />
                                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel3" runat="server" Width="100%" HeaderText=" INFORMACIÓN DEL PLAN">
                                        <PanelCollection>
                                            <dxp:PanelContent ID="PanelContent3" runat="server" SupportsDisabledAttribute="True">
                                                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                    <tr style="border: 0;">
                                                        <td class="Form_TextoDer">
                                                            Plan:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:DropDownList ID="ddlPlan" runat="server" AutoPostBack="True" 
                                                                OnSelectedIndexChanged="ddlPlan_SelectedIndexChanged" Width="95px">
                                                            </asp:DropDownList>
                                                            <span class="Form_TextoObligatorio">*</span></td>
                                                        <td class="Form_TextoDer">
                                                            Frecuencia:</td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:DropDownList ID="ddlFrecuencia" runat="server" AutoPostBack="True" 
                                                                OnSelectedIndexChanged="ddlFrecuencia_SelectedIndexChanged" Width="95px">
                                                            </asp:DropDownList>
                                                            <span class="Form_TextoObligatorio">*</span></td>
                                                        <td class="Form_TextoDer">
                                                            Moneda:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:DropDownList ID="ddlMonPrima" runat="server" Width="95px">
                                                            </asp:DropDownList>
                                                            <span class="Form_TextoObligatorio">*</span></td>
                                                        <td class="Form_TextoDer">
                                                            Prima Total:</td>
                                                        <td>
                                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:TextBox ID="txtPrimaTot" runat="server" 
                                                                        CssClass="Form_TextBoxMontoDisable" ReadOnly="true" Width="90px"></asp:TextBox>
                                                                </ContentTemplate>
                                                                <triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="ddlPlan" 
                                                                        EventName="SelectedIndexChanged" />
                                                                    <asp:AsyncPostBackTrigger ControlID="ddlFrecuencia" 
                                                                        EventName="SelectedIndexChanged" />
                                                                </triggers>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                    <tr style="border: 0;">
                                                        <td class="Form_TextoDer">
                                                            Prima Cobrar:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:UpdatePanel ID="upCotPrimCertificado" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:TextBox ID="txtPrimCob" runat="server" CssClass="Form_TextBoxMontoDisable" 
                                                                        ReadOnly="true" Width="90px"></asp:TextBox>
                                                                    <span class="Form_TextoObligatorio">*</span>
                                                                </ContentTemplate>
                                                                <triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="ddlPlan" 
                                                                        EventName="SelectedIndexChanged" />
                                                                    <asp:AsyncPostBackTrigger ControlID="ddlFrecuencia" 
                                                                        EventName="SelectedIndexChanged" />
                                                                </triggers>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            &nbsp;</td>
                                                        <td class="Form_TextoIzq">
                                                            &nbsp;</td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div style="display: none;">
                                                    <asp:RequiredFieldValidator ID="rfvPlan" runat="server" ControlToValidate="ddlPlan"
                                                        Display="None" ErrorMessage="Seleccione el plan." ForeColor="Transparent" InitialValue="-1"
                                                        SetFocusOnError="true" Text="*" ValidationGroup="valCliente">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvFrecuencia" runat="server" Text="*" ControlToValidate="ddlFrecuencia"
                                                        Display="None" ErrorMessage="Seleccione frecuencia de pago." ForeColor="Transparent"
                                                        InitialValue="-1" SetFocusOnError="True" ValidationGroup="valCliente"></asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvMonPrima" runat="server" Text="*" ControlToValidate="ddlMonPrima"
                                                        Display="None" ErrorMessage="Seleccione moneda prima." ForeColor="Transparent"
                                                        SetFocusOnError="True" InitialValue="-1" ValidationGroup="valCliente"></asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvPrimTotal" runat="server" Text="*" ControlToValidate="txtPrimCob"
                                                        Display="None" ErrorMessage="No existe monto de prima." ForeColor="Transparent"
                                                        SetFocusOnError="True" ValidationGroup="valCliente"></asp:RequiredFieldValidator>
                                                </div>
                                            </dxp:PanelContent>
                                        </PanelCollection>
                                    </dxrp:ASPxRoundPanel>
                                    <br />
                                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel4" runat="server" Width="100%" HeaderText="INFORMACIÓN DE INSPECCIÓN">
                                        <PanelCollection>
                                            <dxp:PanelContent ID="PanelContent4" runat="server" SupportsDisabledAttribute="True">
                                                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                    <tr style="border: 0;">
                                                        <td class="Form_TextoIzq">
                                                            <asp:CheckBox ID="CheckBox1" runat="server" Text="¿Dirección de cliente?" Visible="false" />
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Dirección:
                                                        </td>
                                                        <td class="Form_TextoIzq" colspan="2">
                                                            <asp:TextBox ID="txtDirEntrega" CssClass="Form_TextBox" runat="server" Width="180px"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                        </td>
                                                        <td class="Form_TextoIzq" colspan="3">
                                                        </td>
                                                    </tr>
                                                    <tr style="border: 0;">
                                                        <td>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Departamento:
                                                        </td>
                                                        <td class="Form_TextoIzq" style="width: 120px;">
                                                            <asp:UpdatePanel ID="upDepEntrega" runat="server">
                                                                <ContentTemplate>
                                                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                                        <tr style="border: 0;">
                                                                            <td style="width: 110px;">
                                                                                <asp:DropDownList ID="ddlDepEntrega" runat="server" Width="105px" AutoPostBack="True"
                                                                                    OnSelectedIndexChanged="ddlDepEntrega_SelectedIndexChanged">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td>
                                                                                <span class="Form_TextoObligatorio">*</span>
                                                                            </td>
                                                                            <td style="width: 10px;">
                                                                                <asp:UpdateProgress ID="upsDepEntrega" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upDepEntrega">
                                                                                    <ProgressTemplate>
                                                                                        <asp:Image ID="imgLoadDepEnt" runat="server" SkinID="sknImgLoading" Width="5px" Height="5px" />
                                                                                    </ProgressTemplate>
                                                                                </asp:UpdateProgress>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Provincia:
                                                        </td>
                                                        <td class="Form_TextoIzq" style="width: 120px;">
                                                            <asp:UpdatePanel ID="upProvEntrega" runat="server">
                                                                <ContentTemplate>
                                                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                                        <tr style="border: 0;">
                                                                            <td style="width: 110px;">
                                                                                <asp:DropDownList ID="ddlProvEntrega" runat="server" Width="105px" AutoPostBack="True"
                                                                                    OnSelectedIndexChanged="ddlProvEntrega_SelectedIndexChanged">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td>
                                                                                <span class="Form_TextoObligatorio">*</span>
                                                                            </td>
                                                                            <td style="width: 10px;">
                                                                                <asp:UpdateProgress ID="upsProvEntrega" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upProvEntrega">
                                                                                    <ProgressTemplate>
                                                                                        <asp:Image ID="imgLoadProvEnt" runat="server" SkinID="sknImgLoading" Width="5px"
                                                                                            Height="5px" />
                                                                                    </ProgressTemplate>
                                                                                </asp:UpdateProgress>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="ddlDepEntrega" EventName="selectedIndexChanged" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Distrito:
                                                        </td>
                                                        <td class="Form_TextoIzq" colspan="2">
                                                            <asp:UpdatePanel ID="upDistEntrega" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:DropDownList ID="ddlDistEntrega" runat="server" Width="105px">
                                                                    </asp:DropDownList>
                                                                    <span class="Form_TextoObligatorio">*</span>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="ddlProvEntrega" EventName="selectedIndexChanged" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div style="display: none;">
                                                    
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Text="*" ControlToValidate="txtDirEntrega"
                                                        Display="None" ErrorMessage="Ingrese la dirección para la entrega de la póliza." ForeColor="Transparent"
                                                        SetFocusOnError="true" ValidationGroup="valCliente">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvDepEntrega" runat="server" Text="*" ControlToValidate="ddlDepEntrega"
                                                        Display="None" ErrorMessage="Seleccione el departamento para la entrega de la póliza." ForeColor="Transparent"
                                                        SetFocusOnError="true" InitialValue="-1" ValidationGroup="valCliente">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvProvEntrega" runat="server" Text="*" ControlToValidate="ddlProvEntrega"
                                                        Display="None" ErrorMessage="Seleccione la provincia para la entrega de la póliza." ForeColor="Transparent"
                                                        SetFocusOnError="true" InitialValue="-1" ValidationGroup="valCliente">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="rfvDistEntrega" runat="server" ControlToValidate="ddlDistEntrega"
                                                        Display="None" ErrorMessage="Seleccione el distrito para la entrega de la póliza." ForeColor="Transparent"
                                                        InitialValue="-1" SetFocusOnError="true" Text="*" ValidationGroup="valCliente">
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                            </dxp:PanelContent>
                                        </PanelCollection>
                                    </dxrp:ASPxRoundPanel>
                                    <br />
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr style="border: 0;">
                                            <td class="Form_TextoIzq">
                                                <span class="Form_TextoObligatorio">&nbsp;(*)Campos Obligatorios</span>
                                            </td>
                                            <td class="Form_TextoDer">
                                                <asp:Button ID="btnExpedir" runat="server" Text="Expedir" CausesValidation="true"
                                                    ValidationGroup="valCliente" OnClick="btnExpedir_Click" ToolTip="Click para expedir el certificado." />
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:ValidationSummary ID="vsCliente" runat="server" ValidationGroup="valCliente"
                                        ShowMessageBox="true" ShowSummary="false" />
                                </dxw:ContentControl>
                            </ContentCollection>
                        </dxtc:TabPage>
                    </TabPages>
                </dxtc:ASPxPageControl>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <div style="display: none;">
        <asp:HiddenField ID="hfAnio" runat="server" />
        <asp:HiddenField ID="hfValVahiculo" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hfIdMarca" runat="server" />
        <asp:HiddenField ID="hfDesMarca" runat="server" />
        <asp:HiddenField ID="hfIdModelo" runat="server" />
        <asp:HiddenField ID="hfDesModelo" runat="server" />
        <asp:HiddenField ID="hdTimon" runat="server" />
        <asp:HiddenField ID="hfScoring" runat="server" />
        <asp:HiddenField ID="hfPorcen" runat="server" />
        <asp:HiddenField ID="hfIdProducto" runat="server" />
        <asp:HiddenField ID="hfValorOriginal" runat="server" />
    </div>
    <dxlp:ASPxLoadingPanel ID="dlpCotizacion" runat="server" Modal="true" ClientInstanceName="LoadingPanel"
        Text="Procesando&amp;hellip;">
    </dxlp:ASPxLoadingPanel>
</asp:Content>
