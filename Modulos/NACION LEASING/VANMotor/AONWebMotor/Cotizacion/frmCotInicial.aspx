<%@ Page Title="Cotizaci�n" Language="C#" MasterPageFile="~/Master/Principal.Master"
    AutoEventWireup="true" CodeBehind="frmCotInicial.aspx.cs" Inherits="AONWebMotor.Cotizacion.frmCotInicial"
    EnableEventValidation="false" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script language="javascript" type="text/javascript">
        function FormatMoney(num) {
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                num = num.substring(0, num.length - (4 * i + 3)) + ',' +
                num.substring(num.length - (4 * i + 3));
            return (((sign) ? '' : '-') + num + '.' + cents);
        }
        function LimpiarForm(Busq) {
            document.getElementById('<%=ddlClase.ClientID %>').value = "0";
            document.getElementById('<%=ddlMarca.ClientID %>').value = "0";
            document.getElementById('<%=hfIdModelo.ClientID %>').value = "";
            LimpiarCombo(document.getElementById('<%=ddlModelo.ClientID %>'));
            document.getElementById('<%=txtValVehiculo.ClientID %>').value = "0.00";
            document.getElementById('<%=ddlAniFabricacion.ClientID %>').value = "0";            

            document.getElementById('<%=txtPriNombre.ClientID %>').value = "";
            document.getElementById('<%=txtSegNombre.ClientID %>').value = "";
            document.getElementById('<%=txtApePaterno.ClientID %>').value = "";
            document.getElementById('<%=txtApeMaterno.ClientID %>').value = "";
            document.getElementById('<%=ddlTipDocumento.ClientID %>').value = "L"
            document.getElementById('<%=ddlDepartamento.ClientID %>').value = "5115";
            document.getElementById('<%=hfProvincia.ClientID %>').value = "511501";
            document.getElementById('<%=hfDistrito.ClientID %>').value = "511501001";
            document.getElementById('<%=txtDireccion.ClientID %>').value = "";
            document.getElementById('<%=txtTelefono.ClientID %>').value = "";
            document.getElementById('<%=txtCorreo.ClientID %>').value = "";

            //Buscar por placa
            if (Busq == 1) {
                document.getElementById('<%=txtNroDocumento.ClientID %>').value = "";
            }

            //Buscar por Nro. Documento
            if (Busq == 2) {
                document.getElementById('<%=txtNroPlaca.ClientID %>').value = "";
            }

            document.getElementById('<%=btnRefreshClear.ClientID %>').click();
        }
        function LimpiarCombo(ctrl) {
            var len = ctrl.options.length;
            for (i = len - 1; i >= 0; i--) {
                ctrl.options.remove(i);
            }
        }
    </script>

    <script language="javascript" type="text/javascript">
        function ClienteAsincrono(e) {
            var evt = e ? e : window.event;
            if (evt.keyCode == 13) {
                var nroDocumento = document.getElementById('<%=txtNroDocumento.ClientID %>').value;

                if (nroDocumento != "") {
                    ObtenerCliente(nroDocumento);
                    return false;
                }
                else {
                    return false;
                }
            }
        }
        function ObtenerCliente(pNroDocumento) {
            var cliente = PageMethods.ObtenerCliente(pNroDocumento, onSucceedCliente, OnErrorCliente);
        }
        function onSucceedCliente(result) {
            var arrValores = result.split('*');
            if (result != "") {
                document.getElementById('<%=txtPriNombre.ClientID %>').value = Encoder.htmlDecode(arrValores[0]);
                document.getElementById('<%=txtSegNombre.ClientID %>').value = Encoder.htmlDecode(arrValores[1]);
                document.getElementById('<%=txtApePaterno.ClientID %>').value = Encoder.htmlDecode(arrValores[2]);
                document.getElementById('<%=txtApeMaterno.ClientID %>').value = Encoder.htmlDecode(arrValores[3]);
                document.getElementById('<%=ddlTipDocumento.ClientID %>').value = Encoder.htmlDecode(arrValores[4]);
                document.getElementById('<%=txtNroDocumento.ClientID %>').value = Encoder.htmlDecode(arrValores[5]);
                document.getElementById('<%=ddlDepartamento.ClientID %>').value = Encoder.htmlDecode(arrValores[6].substring(0, 4));
                document.getElementById('<%=hfProvincia.ClientID %>').value = Encoder.htmlDecode(arrValores[6].substring(0, 6));
                document.getElementById('<%=hfDistrito.ClientID %>').value = Encoder.htmlDecode(arrValores[6]);
                document.getElementById('<%=txtDireccion.ClientID %>').value = Encoder.htmlDecode(arrValores[7]);
                document.getElementById('<%=ddlClase.ClientID %>').value = Encoder.htmlDecode(arrValores[8]);
                document.getElementById('<%=ddlMarca.ClientID %>').value = Encoder.htmlDecode(arrValores[9]);
                document.getElementById('<%=hfIdModelo.ClientID %>').value = Encoder.htmlDecode(arrValores[10]);
                document.getElementById('<%=ddlAniFabricacion.ClientID %>').value = Encoder.htmlDecode(arrValores[11]);
                document.getElementById('<%=txtValVehiculo.ClientID %>').value = Encoder.htmlDecode(arrValores[12]);
                document.getElementById('<%=txtTelefono.ClientID %>').value = Encoder.htmlDecode(arrValores[13]);
                document.getElementById('<%=txtCorreo.ClientID %>').value = Encoder.htmlDecode(arrValores[14]);
                document.getElementById('<%=btnRefresh.ClientID %>').click();
            }
            else {
                LimpiarForm(2);
                alert('No se encontr� el cliente.');
            }
        }
        function OnErrorCliente(result) {
            LimpiarForm(2);
            alert('No se encontr� el cliente.');
        }
    </script>

    <script language="javascript" type="text/javascript">
        function ClientexNroPlacaAsincrono(e) {
            var evt = e ? e : window.event;
            if (evt.keyCode == 13) {
                var nroPlaca = document.getElementById('<%=txtNroPlaca.ClientID %>').value;

                if (nroPlaca != "") {
                    ObtenerClientexNroPlaca(nroPlaca);
                    return false;
                }
                else {
                    return false;
                }
            }
        }
        function ObtenerClientexNroPlaca(pNroPlaca) {
            var cliente = PageMethods.ObtenerClietexNroPlaca(pNroPlaca, onSucceedClientexNroPlaca, OnErrorClientexNroPlaca);
        }
        function onSucceedClientexNroPlaca(result) {
            var arrValores = result.split('*');
            if (result != "") {
                document.getElementById('<%=txtPriNombre.ClientID %>').value = Encoder.htmlDecode(arrValores[0]);
                document.getElementById('<%=txtSegNombre.ClientID %>').value = Encoder.htmlDecode(arrValores[1]);
                document.getElementById('<%=txtApePaterno.ClientID %>').value = Encoder.htmlDecode(arrValores[2]);
                document.getElementById('<%=txtApeMaterno.ClientID %>').value = Encoder.htmlDecode(arrValores[3]);
                document.getElementById('<%=ddlTipDocumento.ClientID %>').value = Encoder.htmlDecode(arrValores[4]);
                document.getElementById('<%=txtNroDocumento.ClientID %>').value = Encoder.htmlDecode(arrValores[5]);
                document.getElementById('<%=ddlDepartamento.ClientID %>').value = Encoder.htmlDecode(arrValores[6].substring(0, 4));
                document.getElementById('<%=hfProvincia.ClientID %>').value = Encoder.htmlDecode(arrValores[6].substring(0, 6));
                document.getElementById('<%=hfDistrito.ClientID %>').value = Encoder.htmlDecode(arrValores[6]);
                document.getElementById('<%=txtDireccion.ClientID %>').value = Encoder.htmlDecode(arrValores[7]);
                document.getElementById('<%=ddlClase.ClientID %>').value = Encoder.htmlDecode(arrValores[8]);
                document.getElementById('<%=ddlMarca.ClientID %>').value = Encoder.htmlDecode(arrValores[9]);
                document.getElementById('<%=hfIdModelo.ClientID %>').value = Encoder.htmlDecode(arrValores[10]);
                document.getElementById('<%=ddlAniFabricacion.ClientID %>').value = Encoder.htmlDecode(arrValores[11]);
                document.getElementById('<%=txtValVehiculo.ClientID %>').value = Encoder.htmlDecode(arrValores[12]);
                document.getElementById('<%=txtTelefono.ClientID %>').value = Encoder.htmlDecode(arrValores[13]);
                document.getElementById('<%=txtCorreo.ClientID %>').value = Encoder.htmlDecode(arrValores[14]);
                document.getElementById('<%=btnRefresh.ClientID %>').click();
            }
            else {
                LimpiarForm(1);
                alert('No se encontr� el cliente.');
            }
        }
        function OnErrorClientexNroPlaca(result) {
            LimpiarForm(1);
            alert('No se encontr� el cliente.');
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td class="Form_SubTitulo" colspan="6">
                            DATOS DEL CLIENTE
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer" style="width: 120px;">
                            Primer Nombre: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtPriNombre" runat="server" CssClass="Form_TextBox" Width="120px"
                                Text="SILVANA">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvPriNombre" runat="server" Text="*" ControlToValidate="txtPriNombre"
                                Display="None" ErrorMessage="Ingrese Primer Nombre." ForeColor="Transparent"
                                SetFocusOnError="true" ValidationGroup="Cotizacion">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Segundo Nombre:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtSegNombre" runat="server" CssClass="Form_TextBox" Width="120px"
                                Text="PAOLA">
                            </asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            Tipo Documento:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlTipDocumento" runat="server" Width="150px">
                                <asp:ListItem Text="DNI" Value="0"  ></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Apellido Paterno: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtApePaterno" runat="server" CssClass="Form_TextBox" Width="120px"
                                Text="RODRIGUEZ" >
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvApePaterno" runat="server" Text="*" ControlToValidate="txtApePaterno"
                                Display="None" ErrorMessage="Ingrese Apellido Paterno" ForeColor="Transparent"
                                SetFocusOnError="true" ValidationGroup="Cotizacion">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Apllido Materno:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtApeMaterno" runat="server" CssClass="Form_TextBox" Width="120px"
                                Text="ORTEGA">
                            </asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            Nro Documento: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtNroDocumento" runat="server" Width="90px" CssClass="Form_TextBox"
                                Text="44264581" >
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvNroDocumento" runat="server" Text="*" ControlToValidate="txtNroDocumento"
                                Display="None" ErrorMessage="Ingrese Nro. Documento." ForeColor="Transparent"
                                SetFocusOnError="true" ValidationGroup="Cotizacion">
                            </asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="ftxtNroDocumento" runat="server" TargetControlID="txtNroDocumento"
                                ValidChars="0123456789">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Departamento:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlDepartamento" runat="server" Width="150px" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged">
                                <asp:ListItem Text="LIMA" Value="0"  ></asp:ListItem>                                
                            </asp:DropDownList>
                        </td>
                        <td class="Form_TextoDer">
                            Provincia:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlProvincia" runat="server" Width="150px" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlProvincia_SelectedIndexChanged">
                                 <asp:ListItem Text="LIMA" Value="0"  ></asp:ListItem>                                                                    
                            </asp:DropDownList>
                        </td>
                        <td class="Form_TextoDer">
                            Distrito:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlDistrito" runat="server" Width="145px">
                                 <asp:ListItem Text="SANTIAGO DE SURCO" Value="0"  ></asp:ListItem>                                
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Direcci�n: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtDireccion" runat="server" CssClass="Form_TextBox" Width="150px"
                                Text="AV. CAMINOS DEL INCA 612" >
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvDireccion" runat="server" Text="*" ControlToValidate="txtDireccion"
                                Display="None" ErrorMessage="Ingrese Direcci�n." ForeColor="Transparent" SetFocusOnError="true"
                                ValidationGroup="Cotizacion">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Tel�fono:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtTelefono" runat="server" CssClass="Form_TextBox" Width="145px" Text="2528929" >
                            </asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="ftxtTelefono" runat="server" TargetControlID="txtTelefono"
                                ValidChars="1234567890-*">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                        <td class="Form_TextoDer">
                            Email:
                        </td>
                        <td>
                            <asp:TextBox ID="txtCorreo" runat="server" CssClass="Form_TextBox" Width="145px" Text="silrod@gmail.com" >
                            </asp:TextBox>
                            <asp:RegularExpressionValidator ID="revCorreo" runat="server" ControlToValidate="txtCorreo"
                                Text="*" Display="None" SetFocusOnError="true" ErrorMessage="Email tiene formato incorrecto."
                                ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Cotizacion">
                            </asp:RegularExpressionValidator>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <br />
    <table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <table style="width: 100%;" class="Form_Fondo" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="Form_SubTitulo" colspan="6">
                            DATOS DEL VEH�CULO
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Nro. Placa:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtNroPlaca" runat="server" CssClass="Form_TextBoxDisable" Width="120px" Text="OPG-45"  ></asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            Clase: 
                        </td>
                        <td>
                            <asp:TextBox ID="txtClase" runat="server" CssClass="Form_TextBoxDisable" Width="120px" Text="SEDAN" >
                            </asp:TextBox>
                            <asp:DropDownList ID="ddlClase" runat="server" Width="200px" AutoPostBack="True"
                                Style="display: none;" OnSelectedIndexChanged="ddlClase_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvClase" runat="server" Text="*" ControlToValidate="ddlClase"
                                Display="None" ErrorMessage="Seleccione Clase de Veh�culo." ForeColor="Transparent"
                                SetFocusOnError="true" InitialValue="0" ValidationGroup="Cotizacion">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Marca: 
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtMarca" runat="server" CssClass="Form_TextBoxDisable" Text="AUDI" Width="120px"  ></asp:TextBox>
                            <asp:DropDownList ID="ddlMarca" runat="server" Width="200px" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlMarca_SelectedIndexChanged" Style="display: none;">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvMarca" runat="server" Text="*" ControlToValidate="ddlMarca"
                                Display="None" ErrorMessage="Seleccione Marca de Veh�culo." ForeColor="Transparent"
                                SetFocusOnError="true" InitialValue="0" ValidationGroup="Cotizacion">
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Modelo: 
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtModelo" runat="server" CssClass="Form_TextBoxDisable" Text="A4 1.8 TFSI" Width="120px"   ></asp:TextBox>
                            <asp:DropDownList ID="ddlModelo" runat="server" Width="120px" Style="display: none;">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvModelo" runat="server" Text="*" ControlToValidate="ddlModelo"
                                Display="None" ErrorMessage="Seleccione Modelo de Veh�culo." ForeColor="Transparent"
                                SetFocusOnError="true" InitialValue="0" ValidationGroup="Cotizacion">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            A�o de Fabricaci�n:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtAnio" runat="server" CssClass="Form_TextBoxDisable" Width="120px" Text="2009" ></asp:TextBox>
                            <asp:DropDownList ID="ddlAniFabricacion" runat="server" Width="120px" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlAniFabricacion_SelectedIndexChanged" Style="display: none;">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvAniFabricacion" runat="server" Text="*" ControlToValidate="ddlAniFabricacion"
                                Display="None" ErrorMessage="Seleccione A�o de Fabricaci�n del Veh�culo." ForeColor="Transparent"
                                SetFocusOnError="true" InitialValue="0" ValidationGroup="Cotizacion">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            &nbsp;</td>
                        <td class="Form_TextoIzq">
                            <asp:RequiredFieldValidator ID="rfvValVehiculo" runat="server" Text="*" ControlToValidate="txtValVehiculo"
                                Display="None" ErrorMessage="Ingrese Valor del Veh�culo." ForeColor="Transparent"
                                SetFocusOnError="true" ValidationGroup="Cotizacion">
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Cambio Valor Veh�culo:</td>
                        <td>
                            <asp:DropDownList ID="ddlSimbolo" runat="server" Width ="50px" >
                                <asp:ListItem Text="+" Value="0"></asp:ListItem>
                                <asp:ListItem Text="-" Value="1"></asp:ListItem>
                            </asp:DropDownList> 
                            <asp:DropDownList ID="ddlValor" runat="server" Width="50px" >
                                <asp:ListItem Text="10%" Value="10"></asp:ListItem>
                                <asp:ListItem Text="9%" Value="9"></asp:ListItem>
                            </asp:DropDownList> 
                        </td>
                        <td class="Form_TextoDer">
                            Valor Veh�culo(US$):</td>
                        <td>
                            <asp:TextBox ID="txtValVehiculo" runat="server" CssClass="Form_TextBoxDisable" style="text-align:right;" Width="120px"
                                Text="25,000.00" ReadOnly="true" onBlur="this.value=FormatMoney(this.value);">
                            </asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="ftxtValVehiculo" runat="server" TargetControlID="txtValVehiculo"
                                ValidChars="0123456789.,">
                            </cc1:FilteredTextBoxExtender>
                        </td>                        
                        <td class="Form_TextoDer">
                            &nbsp;</td>
                        <td class="Form_TextoIzq">
                            &nbsp;</td>
                    </tr>                    
                    <tr>
                        <td class="Form_TextoDer">
                            Tim�n Cambiado</td>
                        <td>
                                        <asp:RadioButton ID="rbtnTimCambiadoSi" runat="server" Text="Si" GroupName="TimCambiado" />
                                        <asp:RadioButton ID="rbtnTimCambiadoNo" runat="server" Text="No" GroupName="TimCambiado"
                                            Checked="true" />
                        </td>
                        <td class="Form_TextoDer">
                            Veh�culo Nuevo:</td>
                        <td>
                                        <asp:RadioButton ID="rbtnNuevoSi" runat="server" Text="Si" GroupName="Nuevo" />
                                        <asp:RadioButton ID="rbtnNuevoNo" runat="server" Text="No" GroupName="Nuevo" Checked="true" />
                        </td>
                        <td class="Form_TextoDer">
                            Requiere GPS:</td>
                        <td class="Form_TextoIzq">
                                        <asp:RadioButton ID="rbtnGpsSi" runat="server" Text="Si" GroupName="GPS" />
                                        <asp:RadioButton ID="rbtnGpsNo" runat="server" Text="No" GroupName="GPS" Checked="true" />
                        </td>
                    </tr>                                                 
                </table>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <br />
    <table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <table style="width: 100%;" class="Form_Fondo" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="Form_SubTitulo" colspan="6">
                            DATOS DE LA COTIZACI�N
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Nro. Cotizaci�n:
                        </td>    
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtNroCotizacion" runat="server" CssClass="Form_TextBoxDisable" Text="000125" Width="120px"  >
                            </asp:TextBox>
                        </td>    
                        <td class="Form_TextoDer">
                            Tasa:
                        </td>    
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtTasa" runat="server" CssClass="Form_TextBoxDisable" Text="3.64" Width="120px"  style="text-align:right;" >
                            </asp:TextBox> 
                        </td>    
                        <td class="Form_TextoDer">
                            Frecuencia:
                        </td>    
                        <td class="Form_TextoIzq">
                            <asp:DropDownList id="ddlFrecuencia" runat="server" Width="120px" >
                                <asp:ListItem Text="SELECCIONE" Value="1" ></asp:ListItem>
                            </asp:DropDownList>
                        </td>    
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Plan:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList id="ddlMedioPago" runat="server" Width="120px" >
                                <asp:ListItem Text="SELECCIONE" Value="1" ></asp:ListItem>
                            </asp:DropDownList>                             
                        </td>
                        <td class="Form_TextoDer">
                            Moneda:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList id="ddlMoneda" runat="server" Width="120px" >
                                <asp:ListItem Text="SELECCIONE" Value="1" ></asp:ListItem>
                            </asp:DropDownList> 
                        </td>
                        <td class="Form_TextoDer">
                            Prima:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtPrima" runat="server" Width="120px" CssClass="Form_TextBoxDisable" Text="909.00" style="text-align:right;"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Medio de Pago:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList id="ddlMedPago"  runat="server" Width="120px" >
                                <asp:ListItem Text="SELECCIONE" Value="1" ></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="Form_TextoDer">
                            Fecha Vencimiento:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox id="txtFecVecimiento" runat="server" CssClass="Form_TextBox" Width="120px"></asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            Periodo Vigencia:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList id="ddlPerVigencia" runat="server" Width="120px" >
                                <asp:ListItem Text="SELECCIONE" Value="-1" ></asp:ListItem>
                                <asp:ListItem Text="ANUAL" Value="1" ></asp:ListItem>
                                <asp:ListItem Text="BIANUAL" Value="2" ></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Descuento:</td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox id="txtPorcDscto" runat="server" Width="40px" CssClass="Form_TextBoxDisable" Text="10%"></asp:TextBox>&nbsp;
                            <asp:TextBox id="txtMontDscto" runat="server" Width="60px" CssClass="Form_TextBoxDisable" Text="$101.00" style="text-align:right;"></asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            Recargo:</td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox id="TextBox1" runat="server" Width="40px" CssClass="Form_TextBoxDisable" Text=""></asp:TextBox>&nbsp;
                            <asp:TextBox id="TextBox2" runat="server" Width="60px" CssClass="Form_TextBoxDisable" Text="$0.00" style="text-align:right;"></asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            &nbsp;</td>
                        <td class="Form_TextoIzq">
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <div class="Form_RegButtonDer">
                    <asp:Button ID="btnCotizar" runat="server" Text="Acepta" OnClick="btnCotizar_Click"
                        ValidationGroup="Cotizacion" />
                    <asp:Button id="btnNoAcepta" runat="server" Text="No Acepta"/>    
                    <asp:Button id="Button1" runat="server" Text="Motivos" Visible="false"/> 
                    <asp:Button id="Button2" runat="server" Text="No Se Encontro" Visible="false" />       
                    <asp:Button ID="btnImprimir" runat="server" Text="Imprimir" CausesValidation="false" Visible="false"  
                        Enabled="false" OnClientClick="return showModalPopup('bmpeCotizacion');" />
                    <asp:Button ID="btnEnvMail" runat="server" Text="Enviar Mail" OnClientClick="showModalFocus('bmpeMail','ctl00_ContentPlaceHolder1_txtEmail'); return false; " />
                    <asp:Button ID="btnSiguiente" runat="server" Text="Siguiente" Enabled="false" Visible="false"  OnClick="btnSiguiente_Click" />
                    <span class="Form_TextoObligatorio">&nbsp;(*)Campos Obligatorios</span>
                </div>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <div style="display: none;">
        <asp:HiddenField ID="hfIdCotizacion" runat="server" />
        <asp:HiddenField ID="hfNomArchivo" runat="server" />
        <asp:HiddenField ID="hfCliente" runat="server" />
        <asp:Button ID="btnRefresh" runat="server" Text="Modelo" Style="display: none;" OnClick="btnRefresh_Click" />
        <asp:Button ID="btnRefreshClear" runat="server" Text="Modelo" Style="display: none;"
            OnClick="btnRefreshClear_Click" />
        <asp:HiddenField ID="hfIdModelo" runat="server" />
        <asp:HiddenField ID="hfProvincia" runat="server" />
        <asp:HiddenField ID="hfDistrito" runat="server" />
    </div>
    <asp:ValidationSummary ID="vsCotizacion" runat="server" ShowMessageBox="true" ShowSummary="false"
        ValidationGroup="Cotizacion" />
    <asp:Panel ID="pnCotizacion" runat="server" Width="800px" Height="600px" CssClass="Modal_Panel"
        Style="display: none;">
        <div class="Modal_Head" style="width: 100%;">
            <div id="div1" style="float: left;">
                Cotizaci�n
            </div>
            <div id="div2" style="float: right;">
                <img alt="Click para cerrar." class="Modal_CloseButton" onclick="hideModalPopup('bmpeCotizacion')"
                    style="cursor: hand;" />
            </div>
        </div>
        <div class="Modal_Body" style="height: 95%;">
            <rsweb:ReportViewer ID="rvCotizacion" runat="server" Width="100%" Height="92%" Font-Names="Verdana"
                Font-Size="8pt">
                <LocalReport ReportPath="rdlc\rdlcCotizacion.rdlc">
                    <DataSources>
                        <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="BECotizacion" />
                    </DataSources>
                </LocalReport>
            </rsweb:ReportViewer>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="Obtener"
                TypeName="AONAffinity.Business.Logic.BLCotizacion">
                <SelectParameters>
                    <asp:ControlParameter ControlID="hfIdCotizacion" DefaultValue="" Name="pnIdCotizacion"
                        PropertyName="Value" Type="Decimal" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </div>
    </asp:Panel>
    <asp:Label ID="lblPopCCotizacion" runat="server" Style="display: none;"></asp:Label>
    <cc1:ModalPopupExtender ID="mpeCotizacion" runat="server" Enabled="true" TargetControlID="lblPopCCotizacion"
        BehaviorID="bmpeCotizacion" PopupControlID="pnCotizacion" BackgroundCssClass="Modal_Background">
    </cc1:ModalPopupExtender>
    <asp:Panel ID="pnMail" runat="server" Width="400px" CssClass="Modal_Panel" Style="display: none;">
        <div class="Modal_Head" style="width: 100%;">
            <div id="divHeadTitulo" style="float: left;">
                Enviar Mail
            </div>
            <div id="divHeadCerrar" style="float: right;">
                <img alt="Click para cerrar." class="Modal_CloseButton" onclick="hideModalPopup('bmpeMail')"
                    style="cursor: hand;" />
            </div>
        </div>
        <div id="divBodyMail" class="Modal_Body">
            <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="Form_TextoDer">
                        Email: <span class="Form_TextoObligatorio">*</span>
                    </td>
                    <td class="Form_TextoIzq">
                        <asp:TextBox ID="txtEmail" runat="server" Width="250px" CssClass="Form_TextBox"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                            Display="None" SetFocusOnError="true" ErrorMessage="Ingrese mail." ValidationGroup="Email">
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                            Display="None" SetFocusOnError="true" ErrorMessage="Email tiene formato incorrecto."
                            ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Email">
                        </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="Form_TextoDer">
                        Cc:
                    </td>
                    <td class="Form_TextoIzq">
                        <asp:TextBox ID="txtEmailCc" runat="server" Width="250px" CssClass="Form_TextBox"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revEmailCc" runat="server" ControlToValidate="txtEmailCc"
                            Display="None" SetFocusOnError="true" ErrorMessage="Email de copia tiene formato incorrecto."
                            ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Email">
                        </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="Form_TextoIzq" colspan="2">
                        <span class="Form_TextoObligatorio">&nbsp;(*)Campos Obligatorios</span>
                    </td>
                </tr>
            </table>
            <br />
            <asp:UpdateProgress ID="upsBusqOficina" runat="server" AssociatedUpdatePanelID="upMail"
                DisplayAfter="0">
                <ProgressTemplate>
                    <asp:Image ID="imgLoadOficina" runat="server" SkinID="sknImgLoading" Width="16px"
                        Height="16px" />
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div style="width: 100%; text-align: center;">
                <asp:UpdatePanel ID="upMail" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnEnviar" runat="server" Text="Enviar" ValidationGroup="Email" CausesValidation="true"
                            OnClick="btnEnviar_Click" />
                        <br />
                        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnEnviar" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <div>
                <asp:ValidationSummary ID="vsMail" runat="server" CssClass="Form_ValSumary" ValidationGroup="Email" />
            </div>
        </div>
    </asp:Panel>
    <asp:Label ID="lblPopMail" runat="server" Style="display: none;"></asp:Label>
    <cc1:ModalPopupExtender ID="mpeMail" runat="server" Enabled="true" TargetControlID="lblPopMail"
        BehaviorID="bmpeMail" PopupControlID="pnMail" BackgroundCssClass="Modal_Background">
    </cc1:ModalPopupExtender>
</asp:Content>
