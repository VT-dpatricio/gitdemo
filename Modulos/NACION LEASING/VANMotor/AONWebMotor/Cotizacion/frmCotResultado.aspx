<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true"
    CodeBehind="frmCotResultado.aspx.cs" Inherits="AONWebMotor.Cotizacion.frmCotResultado" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td class="Seq_Girs" style="height:15px;">
                Cotizaci�n
            </td>
            <td class="Seq_Red" style="height:15px;">
                Resultado Cotizaci�n
            </td>
            <td class="Seq_Girs" style="height:15px;">
                Informaci�n del Veh�culo
            </td>
            <td class="Seq_Girs" style="height:15px;">
                Registro de Certifiado
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <div style="width: 48%; float: left;">
                    <table class="Form_SubTitulo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="Form_SubTitulo">
                                DATOS DEL CLIENTE
                            </td>
                        </tr>
                    </table>
                    <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="Form_TextoIzq" style="width: 40%">
                                Tipo Documento:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:Label ID="lblTipDocumento" runat="server">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoIzq" style="width: 40%">
                                Nro Documento:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:Label ID="lblNroDocumentio" runat="server">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoIzq" style="width: 40%">
                                Nombres y Apellidos:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:Label ID="lblNomApe" runat="server">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoIzq" style="width: 40%">
                                Direcci�n:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:Label ID="lblDireccion" runat="server">
                                </asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="width: 48%; float: right;">
                    <table class="Form_SubTitulo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="Form_SubTitulo">
                                DATOS DEL VEH�CULO
                            </td>
                        </tr>
                    </table>
                    <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="Form_TextoIzq" style="width: 50%">
                                Clase:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:Label ID="lblClase" runat="server">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoIzq">
                                Marca:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:Label ID="lblMarca" runat="server">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoIzq">
                                Modelo:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:Label ID="lblModelo" runat="server">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoIzq">
                                A�o de Fabricacion:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:Label ID="lblAnio" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoIzq">
                                Suma Asegurada $:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:Label ID="lblSuma" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoIzq">
                                Plan:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:Label ID="lblPlan" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <div style="width: 48%; float: left;">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="Form_SubTitulo">
                                INFORMACI�N DE PRIMA
                            </td>
                        </tr>
                    </table>
                    <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="Form_TextoIzq" style="width: 50%;">
                                Prima:
                            </td>
                            <td class="Form_TextoDer">
                                316.00
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoIzq" style="width: 50%;">
                                SubTotal:
                            </td>
                            <td class="Form_TextoDer">
                                330.00
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoIzq" style="width: 50%;">
                                Derecho de Emisi�n:
                            </td>
                            <td class="Form_TextoDer">
                                9.90
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoIzq" style="width: 50%;">
                                IGV:
                            </td>
                            <td class="Form_TextoDer">
                                61.18
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoIzq" style="width: 50%;">
                                Total:
                            </td>
                            <td class="Form_TextoDer">
                                $461.08
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="width: 48%; float: right;">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="Form_SubTitulo">
                                FORMAS DE PAGO
                            </td>
                        </tr>
                    </table>
                    <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="Form_TextoIzq" style="width: 50%;">
                                Contado:
                            </td>
                            <td class="Form_TextoDer">
                                $461.08
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoIzq" style="width: 50%;">
                                Cargo en cuenta en 10 cuotas
                            </td>
                            <td class="Form_TextoDer">
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer" style="width: 50%;">
                                1ra
                            </td>
                            <td class="Form_TextoDer">
                                $43.00
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer" style="width: 50%;">
                                2da
                            </td>
                            <td class="Form_TextoDer">
                                $43.00
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer" style="width: 50%;">
                                3ra
                            </td>
                            <td class="Form_TextoDer">
                                $43.00
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer" style="width: 50%;">
                                4ta
                            </td>
                            <td class="Form_TextoDer">
                                $43.00
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer" style="width: 50%;">
                                5ta
                            </td>
                            <td class="Form_TextoDer">
                                $43.00
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer" style="width: 50%;">
                                6ta
                            </td>
                            <td class="Form_TextoDer">
                                $43.00
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer" style="width: 50%;">
                                7ma
                            </td>
                            <td class="Form_TextoDer">
                                $43.00
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer" style="width: 50%;">
                                8va
                            </td>
                            <td class="Form_TextoDer">
                                $43.00
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer" style="width: 50%;">
                                9na
                            </td>
                            <td class="Form_TextoDer">
                                $43.00
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer" style="width: 50%;">
                                10ma
                            </td>
                            <td class="Form_TextoDer">
                                $43.44
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer" style="width: 50%;">
                                Total Financiado
                            </td>
                            <td class="Form_TextoDer">
                                $434.40
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <div class="Form_RegButtonDer">
                    <asp:Button ID="btnImprimir" runat="server" Text="Imprimir" OnClick="btnImprimir_Click"
                        CausesValidation="false" />
                    <asp:Button ID="btnMail" runat="server" Text="Enviar Mail" OnClientClick="showModalFocus('bmpeMail','ctl00_ContentPlaceHolder1_txtEmail'); return false; " />
                    <asp:Button ID="btnRegresar" runat="server" Text="Regresar" />
                    <asp:Button ID="btnContinuar" runat="server" Text="Continuar" OnClick="btnContinuar_Click"
                        CausesValidation="false" />
                </div>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hfFecCotizacion" runat="server" />
    <asp:HiddenField ID="hfNomArchivo" runat="server" />
    <asp:Panel ID="pnMail" runat="server" Width="400px" CssClass="Modal_Panel" Style="display: none;">
        <div class="Modal_Head" style="width: 100%;">
            <div id="divHeadTitulo" style="float: left;">
                Enviar Mail
            </div>
            <div id="divHeadCerrar" style="float: right;">
                <img alt="Click para cerrar." class="Modal_CloseButton" onclick="hideModalPopup('bmpeMail')"
                    style="cursor: hand;" />
            </div>
        </div>
        <div id="divBodyMail" class="Modal_Body">
            <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="Form_TextoDer">
                        Email: <span class="Form_TextoObligatorio">*</span>
                    </td>
                    <td class="Form_TextoIzq">
                        <asp:TextBox ID="txtEmail" runat="server" Width="250px" CssClass="Form_TextBox"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                            Display="None" SetFocusOnError="true" ErrorMessage="Ingrese mail." ValidationGroup="Email">
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                            Display="None" SetFocusOnError="true" ErrorMessage="Email tiene formato incorrecto."
                            ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Email">
                        </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="Form_TextoDer">
                        Cc:
                    </td>
                    <td class="Form_TextoIzq">
                        <asp:TextBox ID="txtEmailCc" runat="server" Width="250px" CssClass="Form_TextBox"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revEmailCc" runat="server" ControlToValidate="txtEmailCc"
                            Display="None" SetFocusOnError="true" ErrorMessage="Email de copia tiene formato incorrecto."
                            ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Email">
                        </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="Form_TextoIzq" colspan="2">
                        <span class="Form_TextoObligatorio">&nbsp;(*)Campos Obligatorios</span>
                    </td>
                </tr>
            </table>
            <br />
            <asp:UpdateProgress ID="upsBusqOficina" runat="server" AssociatedUpdatePanelID="upMail"
                DisplayAfter="0">
                <ProgressTemplate>
                    <asp:Image ID="imgLoadOficina" runat="server" SkinID="sknImgLoading" Width="16px"
                        Height="16px" />
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div style="width: 100%; text-align: center;">
                <asp:UpdatePanel ID="upMail" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnEnviar" runat="server" Text="Enviar" ValidationGroup="Email" OnClick="btnEnviar_Click" />
                        <br />
                        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnEnviar" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <div>
                <asp:ValidationSummary ID="vsMail" runat="server" CssClass="Form_ValSumary" ValidationGroup="Email" />
            </div>
        </div>
    </asp:Panel>
    <asp:Label ID="lblPopMail" runat="server" Style="display: none;"></asp:Label>
    <cc1:ModalPopupExtender ID="mpeMail" runat="server" Enabled="true" TargetControlID="lblPopMail"
        BehaviorID="bmpeMail" PopupControlID="pnMail" BackgroundCssClass="Modal_Background">
    </cc1:ModalPopupExtender>
</asp:Content>
