﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessLogic;
using AONAffinity.Motor.BusinessEntity; 

namespace AONWebMotor.Cotizacion
{
    public partial class frmBuscarCot : PageBase
    {
        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    this.CargarFormulario();
                }
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, "Se presentó un problema al cargar el formulario. " + ex.Message, false);
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.BuscarCotizacion();
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, "Se presentó un problema al buscar la cotización. " + ex.Message, false);
            }            
        }

        protected void gvCotizacion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try 
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes.Add("OnMouseOut", "this.className = this.orignalclassName;");
                    e.Row.Attributes.Add("OnMouseOver", "this.orignalclassName = this.className;this.className = 'GridWiew_SelectedRow';");

                    ImageButton ibtnSelect = (ImageButton)e.Row.FindControl("ibtnSelect");
                    BECotizacion objBECotizacion = (BECotizacion)(e.Row.DataItem);

                    if (ibtnSelect != null)
                    {
                        ibtnSelect.CommandArgument = objBECotizacion.IdPeriodo + "@" + objBECotizacion.NroCotizacion + "@" + objBECotizacion.IdProducto;
                    }
                }
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, "Se presentó un problema al consultar la información del la cotizacion. " + ex.Message, false);
            }            
        }

        protected void gvCotizacion_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try 
            {
                switch (e.CommandName)
                {
                    case "Select":
                        String[] cArrValores = e.CommandArgument.ToString().Split('@');
                        Response.Redirect("../" + UrlPagina.CertificadoExpedir.Substring(2) + "?idPer=" + cArrValores[0] + "&nroCot=" + cArrValores[1] + "&idProd=" + cArrValores[2]);
                        break;
                }
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }            
        }

        protected void gvCotizacion_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try 
            {
                this.gvCotizacion.PageIndex = e.NewPageIndex;
                this.BuscarCotizacion();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }            
        }
        #endregion

        #region Métodos
        /// <summary>
        /// Permite cargar el formulario.
        /// </summary>
        private void CargarFormulario()
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            this.CargarParametros();

            this.CargarCabecera();
        }


        private void CargarParametros() 
        {
            if (String.IsNullOrEmpty(Request.QueryString["tipo"])) 
            {
                this.hfTipo.Value = Request.QueryString["tipo"].ToString();
            }
        }


        /// <summary>
        /// Permite cargar la cabecera de la grilla de consulta.
        /// </summary>
        private void CargarCabecera()
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/


            if (this.hfTipo.Value == String.Empty) 
            {
                if (this.gvCotizacion.Rows.Count == 0)
                {
                    List<BECotizacion> lstBECotizacion = new List<BECotizacion>();
                    BECotizacion objBECotizacion = new BECotizacion();
                    lstBECotizacion.Add(objBECotizacion);
                    this.CargarGridView(this.gvCotizacion, lstBECotizacion);
                    this.gvCotizacion.Rows[0].Visible = false;
                }
            }
            
            //Tipo Activacion
            if (this.hfTipo.Value == "3") 
            {
                
            }
        }

        /// <summary>
        /// Permite buscar la información de la cotización.
        /// </summary>
        private void BuscarCotizacion()
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
            ---------------------------------------------------------------------------*/

            this.lblResultado.Visible = false;
            BLCotizacion objBLCotizacion = new BLCotizacion();

            if (this.ddlTipoBusq.SelectedValue == "0")
            {
                this.CargarGridView(this.gvCotizacion, objBLCotizacion.ListarxNroDocumento(this.txtDNI.Text.Trim()));
            }
            else
            {
                this.CargarGridView(this.gvCotizacion, objBLCotizacion.ListarxNomApe(this.txtApePat.Text.Trim(), this.txtApeMat.Text.Trim(), this.txtPriNom.Text.Trim(), this.txtSegNom.Text.Trim()));
            }

            if (this.gvCotizacion.Rows.Count == 0)
            {
                this.CargarCabecera();
                this.lblResultado.Visible = true;
            }
        }
        #endregion
    }
}
