﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using iTextSharp.text;
//using iTextSharp.text.pdf;   
using AONAffinity.Resources;
using AONAffinity.Business.Logic;
using AONAffinity.Business.Entity;
using AONAffinity.Business.Entity.BDJanus;
using AONAffinity.Business.Entity.BDMotor;
    
namespace AONWebMotor.Cotizacion
{
    public partial class frmCotResultado : System.Web.UI.Page
    {

        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (!String.IsNullOrEmpty(Request.QueryString["idCot"]))
                    {
                        BLCotizacion objBLCotizacion = new BLCotizacion();
                        BECotizacion objBECotizacion = objBLCotizacion.Obtener(Convert.ToDecimal(Request.QueryString["idCot"]));

                        if (objBECotizacion != null)
                        {
                            //Datos del cliente
                            this.lblTipDocumento.Text = objBECotizacion.IdTipoDocumento;
                            this.lblNroDocumentio.Text = objBECotizacion.NroDocumento;
                            this.lblNomApe.Text = objBECotizacion.PriNombre + " " + objBECotizacion.SegNombre + " " + objBECotizacion.ApePaterno + " " + objBECotizacion.ApeMaterno;
                            this.lblDireccion.Text = objBECotizacion.Direccion;

                            //Datos del vehículo
                            this.lblClase.Text = objBECotizacion.DesClase;
                            this.lblMarca.Text = objBECotizacion.DesMarca;
                            this.lblModelo.Text = objBECotizacion.DesModelo;
                            this.lblAnio.Text = objBECotizacion.IdAnio.ToString();
                            /*if (objBECotizacion.TimonCambiado == true) { this.lblTimon.Text = "SI"; } else { this.lblTimon.Text = "NO"; }*/
                            this.lblSuma.Text = objBECotizacion.ValorVehiculo.ToString();
                            this.lblPlan.Text = objBECotizacion.IdPlan;

                            //Valores Hidden
                            this.hfFecCotizacion.Value = objBECotizacion.FechaCreacion.ToShortDateString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnContinuar_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("../" + UrlPagina.VehiculoRegDatoEsp.Substring(2) + "?idCot=" + Request.QueryString["idCot"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                String cMsj = "";
                DirectoryInfo directorioPdf = new DirectoryInfo(AppSettings.DirCotizacion);

                if (!directorioPdf.Exists)
                {
                    cMsj = "No existe el directorio de cotizaciones.";
                    throw new Exception(cMsj);
                }

                String cArchivo = "";
                String cPak = "";
                if (this.hfNomArchivo.Value == String.Empty)
                {
                    BLCotizacion objBLCotizacion = new BLCotizacion();
                    this.hfNomArchivo.Value = objBLCotizacion.GenerarPDF(Convert.ToInt32(Request.QueryString["idCot"]));
                    cPak = AppSettings.DirCotizacion + this.hfNomArchivo.Value;
                    cArchivo = this.hfNomArchivo.Value;
                    //System.Diagnostics.Process.Start(AppSettings.DirCotizacion + this.hfNomArchivo.Value);                    
                }
                cPak = AppSettings.DirCotizacion + this.hfNomArchivo.Value;
                cArchivo = this.hfNomArchivo.Value;

                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + cArchivo);
                Response.Flush();
                Response.WriteFile(cPak);
                Response.End();

                /*else 
                {
                    System.Diagnostics.Process.Start(AppSettings.DirCotizacion + this.hfNomArchivo.Value);    
                } */
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                String cMsj = "";
                DirectoryInfo directorioPdf = new DirectoryInfo(AppSettings.DirCotizacion);
                BLCotizacion objBLCotizacion = new BLCotizacion();

                if (!directorioPdf.Exists)
                {
                    cMsj = "No existe el directorio de cotizaciones.";
                    throw new Exception(cMsj);
                }

                if (this.hfNomArchivo.Value == String.Empty)
                {
                    this.hfNomArchivo.Value = objBLCotizacion.GenerarPDF(Convert.ToInt32(Request.QueryString["idCot"]));
                }

                String cRutaPdf = AppSettings.DirCotizacion + this.hfNomArchivo.Value;
                String cTituloCorreo = String.Empty;
                String cMensajeCorreo = String.Empty;

                BLParametro objBLParametro = new BLParametro();
                List<BEParametro> lstBEParametro = objBLParametro.Listar(AppSettings.CodParTitMailCot);

                if (lstBEParametro != null)
                {
                    cTituloCorreo = lstBEParametro[0].ValorString;
                }

                lstBEParametro = objBLParametro.Listar(AppSettings.CodParMsjMailCot);

                if (lstBEParametro != null)
                {
                    String[] cArrValores = new String[2];
                    cArrValores[0] = this.lblNomApe.Text.Trim();
                    cArrValores[1] = this.hfFecCotizacion.Value;

                    cMensajeCorreo = Funciones.RemplazarValores(lstBEParametro[0].ValorString, cArrValores);
                }

                BLMail objBLMail = new BLMail();
                objBLMail.Enviar(AppSettings.Correo, this.txtEmail.Text.Trim(), this.txtEmailCc.Text.Trim(), cTituloCorreo, cRutaPdf, cMensajeCorreo);
                this.lblMensaje.Text = "Se envió correo con cotización adjunta.";

            }
            catch (Exception ex)
            {
                this.lblMensaje.Text = ex.Message;
            }
        }
        #endregion        

        #region Metodos
        private void CrearPDF() 
        {

        }

        private void EnviarMail() 
        {

        }
        #endregion               
    }
}
