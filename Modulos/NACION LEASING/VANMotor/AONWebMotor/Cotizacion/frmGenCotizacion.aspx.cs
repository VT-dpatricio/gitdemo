﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Library.Resources;  
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessLogic;
using AONAffinity.Motor.BusinessEntity; 

namespace AONWebMotor.Cotizacion
{
    /// <summary>
    /// Formulario que permite generar las cotizaciones masivas.
    /// </summary>
    public partial class frmGenCotizacion : PageBase
    {
        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.:2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.:____-__-__
            ---------------------------------------------------------------------------*/
            try
            {
                if (!Page.IsPostBack)
                {
                    this.CargarFormulario();                    
                }
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }            
        }

        protected void ddlTipProceso_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.ddlTipProceso.SelectedValue != "-1")
                {
                    this.CargarProcesos();
                }
                else
                {
                    this.gvProceso.DataSource = null;
                    this.gvProceso.DataBind();
                    this.CargarCabeceraGvProceso();
                }
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void gvProceso_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                BLCotizacion objBLCotizacion = new BLCotizacion();
                String[] arrCValores = e.CommandArgument.ToString().Split('@');
                Int32 nIdproceso =  Convert.ToInt32(arrCValores[0]);
                String cRutaLog = arrCValores[1];
                String cArchivoLog = "LogCotizacion_" + nIdproceso + ".txt"; 

                switch (e.CommandName)
                {
                       
                    case "GenerarLog":                        
                        List<String> lstcLog = objBLCotizacion.GenerarLog(nIdproceso);
                        objBLCotizacion.EscribirErrores(lstcLog, cRutaLog, cArchivoLog);
                        Response.Redirect("../" + UrlPagina.DownloadFile.Substring(2) + "?archivo=" + cRutaLog + cArchivoLog);  
                        break;
                    case "Cotizar":
                        BLCliente objBLCliente = new BLCliente();
                        List<BECliente> lstBECliente = objBLCliente.ListarxProceso(Convert.ToInt32(arrCValores[0]));
                        this.ProcesarFiltro(lstBECliente);
                        this.ProcesarCotizacion(nIdproceso);
                        BEProceso objBEProceso = new BEProceso();
                        objBEProceso.IdProceso = nIdproceso;
                        objBEProceso.ArchivoLogCotizacion = cArchivoLog;
                        objBEProceso.Cotizado = true;
                        objBEProceso.UsuarioModificacion = Session[NombreSession.Usuario].ToString();
                        BLProceso objBLProceso = new BLProceso();
                        objBLProceso.ActualizarCotizacion(objBEProceso);
                        this.CargarProcesos();
                        break;
                }
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void gvProceso_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow) 
                {
                    e.Row.Attributes.Add("OnMouseOut", "this.className = this.orignalclassName;");
                    e.Row.Attributes.Add("OnMouseOver", "this.orignalclassName = this.className;this.className = 'GridWiew_SelectedRow';");

                    BEProceso objBEProceso = (BEProceso)(e.Row.DataItem);
                    ImageButton ibtnLog = (ImageButton)e.Row.FindControl("ibtnLog");
                    ImageButton ibtnCotizar = (ImageButton)e.Row.FindControl("ibtnCotizar");
                    Label lblFecProceso = (Label)e.Row.FindControl("lblFecProceso");

                    if (ibtnLog != null) 
                    {
                        ibtnLog.CommandArgument = objBEProceso.IdProceso.ToString() + "@" + objBEProceso.RutaArchivoLogCotizacion;
                    }
                    
                    if (ibtnCotizar != null) 
                    {
                        if (objBEProceso.Cotizado)
                        {
                            ibtnCotizar.Visible = false;
                        }
                        else 
                        {
                            ibtnCotizar.CommandArgument = objBEProceso.IdProceso.ToString() + "@" + objBEProceso.RutaArchivoLogCotizacion;
                        }
                    }

                    if (objBEProceso.FecProceso != NullTypes.FechaNull) 
                    {
                        if (lblFecProceso != null) 
                        {
                            lblFecProceso.Text = objBEProceso.FecProceso.ToShortDateString();    
                        }
                    }
                }
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void btnGenerar_Click(object sender, EventArgs e)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.:2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.:____-__-__
            ---------------------------------------------------------------------------*/

            try
            {
                //Obtener los clientes por cotizar.
                BLCliente objBLCliente = new BLCliente();
                List<BECliente> lstBECliente = objBLCliente.ListarxCotizar();

                //Si existe cliente por cotizar, se ejecutara primero el 
                //filtro de validación y luego la cotización.
                if (lstBECliente != null)
                {
                    //this.ProcesarFiltro(lstBECliente);
                    //this.ProcesarCotizacion(15);
                }
                else
                {
                    this.MostrarMensaje(this.Controls, "No existe clientes para cotizar.", false);                    
                }
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);                
            }
        }
        #endregion                     

        #region Metodos Privados
        private void CargarFormulario() 
        {
            this.CargarFuncionesJS();
            this.CargarTipoProceso();
            this.CargarCabeceraGvProceso();
        }
        private void CargarTipoProceso() 
        {
            BLTipoProceso objBLTipoProceso = new BLTipoProceso();
            this.CargarDropDownList(this.ddlTipProceso, "IdTipoProceso", "Descripcion", objBLTipoProceso.ListarProcesoCotizar(), true);     
        }

        /// <summary>
        /// Permite cargar funciones JavaScript a los controles.
        /// </summary>
        private void CargarFuncionesJS()
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.:2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.:____-__-__
            ---------------------------------------------------------------------------*/

            //this.btnGenerar.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de generar las cotizaciones?')== false) return false;");  
        }

        /// <summary>
        /// Permite ejecutar el filtro de validación.
        /// </summary>
        /// <param name="pLstBECliente">Lista de cliente por cotizar.</param>
        private void ProcesarFiltro(List<BECliente> pLstBECliente) 
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.:2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.:____-__-__
            ---------------------------------------------------------------------------*/

            BLFiltro objBLFiltro = new BLFiltro();
            objBLFiltro.ValidarVehiculoAsegurables(pLstBECliente, Session[NombreSession.Usuario].ToString());
        }

        /// <summary>
        /// Permite ejecutar el proceso de cotizaciones.
        /// </summary>
        //private void ProcesarCotizacion(Int32 pnIdProceso)
        //{
        //    /*---------------------------------------------------------------------------
        //     * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.:2011-08-26
        //     * Usr Mod.: _______________________                 Fecha Mod.:____-__-__
        //    ---------------------------------------------------------------------------*/

        //    BLCliente objBLCliente = new BLCliente();
        //    List<BECliente> lstBECliente = objBLCliente.ListarxProceso(pnIdProceso);
        //    BLCotizacion objBLCotizacion = new BLCotizacion();
        //    Int32 nResult = objBLCotizacion.Generar(lstBECliente, Session[NombreSession.Usuario].ToString());
             
        //    this.MostrarMensaje(this.Controls, "Se generaron " + nResult.ToString() + " cotizaciones. ", false);             
        //}


        private void ProcesarCotizacion(Int32 pnIdProceso)
        {
            /*---------------------------------------------------------------------------
             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.:2011-08-26
             * Usr Mod.: _______________________                 Fecha Mod.:____-__-__
            ---------------------------------------------------------------------------*/

            BLCliente objBLCliente = new BLCliente();
            List<BECliente> lstBECliente = objBLCliente.ListarxProceso(pnIdProceso);
            BLCotizacion objBLCotizacion = new BLCotizacion();
            Int32 nResult = objBLCotizacion.Generar(pnIdProceso, Session[NombreSession.Usuario].ToString());

            this.MostrarMensaje(this.Controls, "Se generaron " + nResult.ToString() + " cotizaciones. ", false);
        }


        private void CargarProcesos()
        {
            BLProceso objBLProceso = new BLProceso();
            this.CargarGridView(this.gvProceso, objBLProceso.Listar(Convert.ToInt32(this.ddlTipProceso.SelectedValue), true));
            this.CargarCabeceraGvProceso();
        }


        private void CargarCabeceraGvProceso()
        {
            if (this.gvProceso.Rows.Count == 0)
            {
                List<BEProceso> lstBEProceso = new List<BEProceso>();
                BEProceso objBEProceso = new BEProceso();
                lstBEProceso.Add(objBEProceso);
                this.CargarGridView(this.gvProceso, lstBEProceso);
                this.gvProceso.Rows[0].Visible = false;
            }
        }
        #endregion        

        

        
    }
}
