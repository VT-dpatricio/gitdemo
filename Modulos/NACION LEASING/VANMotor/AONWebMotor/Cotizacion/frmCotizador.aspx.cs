﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Motor.BusinessLogic;
using AONAffinity.Motor.BusinessEntity;
//using AONAffinity.Motor.Resources;
using AONAffinity.Motor.Resources.Constantes;
using System.Collections;
using System.Configuration;
using System.Xml;

namespace AONWebMotor.Cotizacion
{
    public partial class frmCotizador : PageBase
    {

        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!ValidarAcceso(Page.AppRelativeVirtualPath))
                {
                    Response.Redirect("~/Login.aspx", false);
                }

                if (ValidarEstadoClaveUsuario())
                {
                    Response.Redirect("~/Seguridad/CambiarClave.aspx", false);
                }
                this.CargarFormulario();
            }
        }

        protected void btnCotizar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cotizar();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void ddlMarca_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.CargarModeloxMarca();
            }
            catch (Exception ex)
            {

            }
        }

        protected void ibtnRegistrar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (this.hfPlanes.Value == String.Empty)
                {
                    this.MostrarMensaje(this.Controls, "Debe seleccionar al menos un plan.", false);
                    return;
                }

                this.pnlResultado.CssClass = "Form_Oculto";
                this.pnlRegistro.CssClass = "Form_Visible";
                this.txtMarca.Text = this.hfDesMarca.Value;
                this.txtModelo.Text = this.hfDesModelo.Value;
                this.txtAnio.Text = this.hfAnio.Value;
                this.txtValorVeh.Text = String.Format("{0:#,#0.00}", Convert.ToDecimal(this.hfValor.Value));
                this.MostarPlanesEscogidos();
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnResRegresar_Click(object sender, EventArgs e)
        {
            this.pnlResultado.CssClass = "Form_Oculto";
            this.pnlInicio.CssClass = "Form_Visible";
            this.ReiniciarFormulario();
        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            try
            {
                this.RegistrarCotizacion();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void btnInicar_Click(object sender, EventArgs e)
        {
            try
            {
                this.ReiniciarFormulario();
                LimpiarRegistro();
                this.hfPlanes.Value = "";
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void btnAtras_Click(object sender, EventArgs e)
        {
            try
            {
                this.pnlRegistro.CssClass = "Form_Oculto";
                this.pnlResultado.CssClass = "Form_Visible";
                this.hfPlanes.Value = "";
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }
        #endregion

        #region Metodos
        private void CargarFormulario()
        {
            this.CargarParametro();
            this.CargarMarca();
            this.CargarModelo();
            this.CargarAnio();
            this.CargarOpciones();
            this.CargarListaAsegurador();
            this.CargarFuncionesJS();
            this.CargarTipoDocumento();
        }

        private void CargarParametro()
        {
            //if (!String.IsNullOrEmpty(Request.QueryString["TipoCot"]))
            //    this.hfTipoCot.Value = Request.QueryString["TipoCot"];

            //if (!String.IsNullOrEmpty(Request.QueryString["idSponsor"]))
            //    this.hfIdSponsor.Value = Request.QueryString["idSponsor"];

            //if (!String.IsNullOrEmpty(Request.QueryString["idPais"]))
            //    this.hfIdPais.Value = Request.QueryString["idPais"];

            //if (!String.IsNullOrEmpty(Request.QueryString["idProd"]))
            //    this.hfIdProd.Value = Request.QueryString["idProd"];

            //if (!String.IsNullOrEmpty(Request.QueryString["idGrupProd"]))
            //    this.hfIdGrupProd.Value = Request.QueryString["idGrupProd"];


                this.hfTipoCot.Value = "2";
                this.hfIdSponsor.Value = "8";
                this.hfIdPais.Value = "51";
                this.hfIdProd.Value = "0";
                this.hfIdGrupProd.Value = "1";
            //?TipoCot=2&idProd=0&idSponsor=8&idGrupProd=1&idPais=51

        }

        private void CargarTipoDocumento()
        {
            BLTipoDocumento objBETipoDoc = new BLTipoDocumento();
            this.CargarDropDownList(this.ddlTipoDoc, "IdTipoDocumento", "Nombre", objBETipoDoc.Seleccionar(IDEntidad()), false);
            ddlTipoDoc.SelectedValue = "CI";
        }

        private void CargarMarca()
        {
            BLMarca objBEMarca = new BLMarca();
            this.CargarDropDownList(this.ddlMarca, "IdMarca", "Descripcion", objBEMarca.Listar(true), true);
        }

        private void CargarModelo()
        {
            BLModelo objBLModelo = new BLModelo();
            ViewState[Lista.lstModelo] = objBLModelo.Listar(true);
        }

        private void CargarAnio()
        {
            BLProducto objBLProducto = new BLProducto();
            this.CargarDropDownList(this.ddlAnio, "", "", objBLProducto.ListarAnios(Convert.ToInt32(Producto.Generico)), false);
            DateTime today = DateTime.Today;
            ddlAnio.SelectedValue = today.ToString("yyyy");
        }

        private void CargarModeloxMarca()
        {
            List<BEModelo> lstModelo = new List<BEModelo>();

            if (ViewState[Lista.lstModelo] != null)
            {
                lstModelo = (List<BEModelo>)(ViewState[Lista.lstModelo]);

                BLModelo objBLModelo = new BLModelo();
                this.CargarDropDownList(this.ddlModelo, "IdModelo", "Descripcion", objBLModelo.ListarxMarca(lstModelo, Convert.ToInt32(this.ddlMarca.SelectedValue)), true);
            }
        }

        private void CargarOpciones()
        {
            if (this.hfIdGrupProd.Value == String.Empty)
            {
                return;
            }

            BLDescuentoRecargo objBLDescuentoRecargo = new BLDescuentoRecargo();

            this.CargarDropDownList(this.ddlDescuento, "IdGrupoOpcion", "Condicion", objBLDescuentoRecargo.ListarxGrupoProducto(Convert.ToInt32(this.hfIdGrupProd.Value), TipoOpcion.Descuento, true), false);
            this.CargarDropDownList(this.ddlRecargo, "IdGrupoOpcion", "Condicion", objBLDescuentoRecargo.ListarxGrupoProducto(Convert.ToInt32(this.hfIdGrupProd.Value), TipoOpcion.Recargo, true), false);
            this.CargarDropDownList(this.ddlAdicionales, "IdGrupoOpcion", "Condicion", objBLDescuentoRecargo.ListarxGrupoProducto(Convert.ToInt32(this.hfIdGrupProd.Value), TipoOpcion.Adicional, true), false);

        }

        private void CargarListaAsegurador()
        {
            if (this.hfIdGrupProd.Value == String.Empty)
            {
                return;
            }

            BLAsegurador objBLAsegurador = new BLAsegurador();
            List<BEAsegurador> lstBEAsegurador = objBLAsegurador.ListarxGrupoProducto(Convert.ToInt32(this.hfIdGrupProd.Value), true);

            if (lstBEAsegurador != null)
            {
                //this.CrearJS();
                ViewState[Lista.lstAsegurador] = lstBEAsegurador;

                StringBuilder sbHtml = new StringBuilder();
                sbHtml.Append(@"<table>");
                sbHtml.Append(@"<tr>");
                foreach (BEAsegurador objBEAsegurador in lstBEAsegurador)
                {
                    sbHtml.Append(@"<td>");
                    sbHtml.Append("<img src=" + (char)(34) + objBEAsegurador.RutaLogo + (char)(34) + "/>");
                    sbHtml.Append(@"</td>");
                }
                sbHtml.Append(@"</tr>");
                sbHtml.Append(@"<tr>");
                foreach (BEAsegurador objBEAsegurador in lstBEAsegurador)
                {
                    sbHtml.Append(@"<td  style=" + (char)(34) + "text-align:center" + (char)(34) + ">");
                    //sbHtml.Append("<input type=" + (char)(34) + "checkbox" + (char)(34) + " id=" + (char)(34) + "cbxAseg" + objBEAsegurador.IdAsegurador.ToString() + (char)(34) + " runat=" + (char)(34) + "server" + (char)(34) + " clientidmode=" + (char)(34) + "Static" + (char)(34) + " onclick=" + (char)(34) + "asegurador(this, " +"'" + objBEAsegurador.IdAsegurador.ToString() + "');" + (char)(34) + "/>");
                    sbHtml.Append("<input type=" + (char)(34) + "checkbox" + (char)(34) + " id=" + (char)(34) + "cbxAseg" + objBEAsegurador.IdAsegurador.ToString() + (char)(34) + " runat=" + (char)(34) + "server" + (char)(34) + " clientidmode=" + (char)(34) + "Static" + (char)(34) + " onclick=" + (char)(34) + "return Asegurador('" + objBEAsegurador.IdAsegurador.ToString() + "');" + (char)(34) + "/>");
                    sbHtml.Append(@"</td>");
                }
                sbHtml.Append(@"</tr>");
                sbHtml.Append(@"</table>");
                dvAsegurador.InnerHtml = sbHtml.ToString();
            }
        }

        private void CargarFuncionesJS()
        {
            this.btnCotizar.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de realizar la cotización?')== false) return false;");
            this.btnRegistrar.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de registrar la cotización?')== false) return false;");
            this.ibtnRegistrar.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de registrar la cotización?')== false) return false;");
            this.btnResRegresar.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de regresar a la opción anterior?')== false) return false;");
            this.btnAtras.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de regresar a la opción anterior?')== false) return false;");
            this.btnInicar.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de iniciar una nueva cotización?')== false) return false;");
        }

        private void Cotizar()
        {
            if (this.hfAseguradoras.Value == String.Empty)
            {
                this.MostrarMensaje(this.Controls, "Debe seleccionar al menos una aseguradora.", false);
                return;
            }

            this.ActualizarValorHidden();
            //String cAsegurador = this.ObtenerAsegurador();
            Int32 nIdProducto = Convert.ToInt32(this.hfIdProd.Value);
            Decimal nValorVeh = Decimal.Round(Convert.ToDecimal(this.txtValor.Text.Trim()), 2);
            Int32 nIdMarca = Convert.ToInt32(this.ddlMarca.SelectedValue);
            Int32 nIdModelo = Convert.ToInt32(this.ddlModelo.SelectedValue);
            Int32 nAnioFab = Convert.ToInt32(this.ddlAnio.SelectedValue);
            Int32 nIdGrupProd = Convert.ToInt32(this.hfIdGrupProd.Value);
            Int32 nIdSponsor = Convert.ToInt32(this.hfIdSponsor.Value);
            Int32 nTipoCot = Convert.ToInt32(this.hfTipoCot.Value);

            BLCotizacion objBLCotizacion = new BLCotizacion();
            List<BECotizacion> lstBECotizacion = objBLCotizacion.GenerarCotizacion(nTipoCot, nIdProducto, nIdSponsor, nIdGrupProd,
                                                                                   nValorVeh, nIdMarca, nIdModelo, nAnioFab, false, false,
                                                                                   false, 0, 0,
                                                                                   Convert.ToInt32(this.ddlDescuento.SelectedValue), Convert.ToInt32(this.ddlRecargo.SelectedValue), Convert.ToInt32(this.ddlAdicionales.SelectedValue), this.hfAseguradoras.Value,
                                                                                   String.Empty, String.Empty, String.Empty, String.Empty, String.Empty,
                                                                                   String.Empty, String.Empty, String.Empty, String.Empty, String.Empty);

            ViewState[Lista.lstCotizacion] = lstBECotizacion;

            if (lstBECotizacion != null)
            {
                //StringBuilder sbHtml = new StringBuilder();

                //Int32 nTD = 1;
                //Int32 nIdProdAnt = lstBECotizacion[0].IdProducto;
                //Int32 nIdProd = 0;
                //sbHtml.Append("<table border=" + (char)(34) + "0" + (char)(34) + " cellpadding=" + (char)(34) + "0" + (char)(34) + "cellspacing=" + (char)(34) + "0" + (char)(34) + "style=" + (char)(34) + "width:98%" + (char)(34) + ">");

                //List<String> lstIdProd = new List<String>();

                this.CrearTablaResultado(lstBECotizacion);
                //this.CrearDetalleResultado(lstBECotizacion, lstIdProd);


                //foreach (BECotizacion objBECotizacion in lstBECotizacion)
                //{

                    


                    //nIdProd = objBECotizacion.IdProducto;

                    //if (nTD == 1) {
                    //    sbHtml.Append("<tr>");
                    //}

                    //sbHtml.Append("<td>");
                    ////sbHtml.Append("<img src=" + (char)(34) + objBECotizacion.RutaLogoAseg + (char)(34) + " class=" + (char)(34) + "ps_Logo" + (char)(34) + " />");
                    //sbHtml.Append("<table border=" + (char)(34) + "0" + (char)(34) + " cellpadding=" + (char)(34) + "0" + (char)(34) + "cellspacing=" + (char)(34) + "0" + (char)(34) + "Style=" + (char)(34) + "border-style:groove; border-width:thin;" + (char)(34) + ">");
                    ////sbHtml.Append("<tr><td class=" + (char)(34) + "ps_Titulo" + (char)(34) + ">" + objBECotizacion.DesProducto + "</td></tr>");
                    //sbHtml.Append("<tr><td class=" + (char)(34) + "ps_Titulo" + (char)(34) + ">" + objBECotizacion.DesCategoria + " " + objBECotizacion.SeguroPlan + " " + "<input type=" + (char)(34) + "checkbox" + (char)(34) + " id=" + (char)(34) + "cbxPlan" + objBECotizacion.Correlativo.ToString() + (char)(34) + " runat=" + (char)(34) + "server" + (char)(34) + "clientidmode=" + (char)(34) + "Static" + (char)(34) + "onclick=" + (char)(34) + "return Plan('" + objBECotizacion.Correlativo.ToString() + "')" + (char)(34) + "/></td></tr>");
                    //sbHtml.Append("<tr><td>" + "Cap. Aseg.: " + String.Format("{0:#,#0.00}", objBECotizacion.ValVehiculo) + "</td></tr>");

                    ////if (objBECotizacion.TasaPorc != 0)
                    ////{
                    ////    sbHtml.Append("<tr><td>" + "Tasa: " + Decimal.Round(objBECotizacion.TasaPorc, 2).ToString() + "%" + "</td></tr>");
                    ////}

                    ////if (objBECotizacion.PorcDescuento != 0 || objBECotizacion.PorcRecargo != 0)
                    ////{
                    ////    sbHtml.Append("<tr><td>" + "Costo: " + String.Format("{0:#,#0.00}", objBECotizacion.CostoAnual) + "" + "</td></tr>");
                    ////}

                    ////if (objBECotizacion.PorcDescuento != 0)
                    ////{
                    ////    sbHtml.Append("<tr><td>" + "Dcto: " + Decimal.Round(objBECotizacion.PorcDescuento, 2).ToString() + "%" + "</td></tr>");
                    ////    sbHtml.Append("<tr><td>" + "Monto Dcto: " + String.Format("{0:#,#0.00}", objBECotizacion.ImporteDescuento) + "" + "</td></tr>");
                    ////}
                    ////if (objBECotizacion.PorcRecargo != 0)
                    ////{
                    ////    sbHtml.Append("<tr><td>" + "Rcgo: " + String.Format("{0:#,#0.00}", objBECotizacion.PorcRecargo) + "%" + "</td></tr>");
                    ////    sbHtml.Append("<tr><td>" + "Monto Rcgo: " + String.Format("{0:#,#0.00}", objBECotizacion.ImporteRecargo) + "" + "</td></tr>");
                    ////}
                    ////sbHtml.Append("<tr><td>" + "Premio Anual: " + String.Format("{0:#,#0.00}", objBECotizacion.PrimaAnual) + "</td></tr>");
                    //sbHtml.Append("<tr><td>" + "Premio Mensual: " + String.Format("{0:#,#0.00}", objBECotizacion.PrimaAnualMensual) + "</td></tr>");
                    ////sbHtml.Append("<tr><td><input type=" + (char)(34) + "checkbox" + (char)(34) + " id=" + (char)(34) + "cbxPlan" + objBECotizacion.Correlativo.ToString() + (char)(34) + " runat=" + (char)(34) + "server" + (char)(34) + "clientidmode=" + (char)(34) + "Static" + (char)(34) + "onclick=" + (char)(34) + "return Plan('" + objBECotizacion.Correlativo.ToString() + "')" + (char)(34) + "/></tr></td>");
                    //sbHtml.Append("</table>");
                    //sbHtml.Append("</td>");

                    //if (nTD == 4)
                    //{
                    //    sbHtml.Append("</tr>");
                    //    nTD = 0;
                    //}

                    //nTD++;
                //}

                //sbHtml.Append("</table>");

                //this.dvResultado.InnerHtml = sbHtml.ToString();

                this.txtResMarca.Text = this.hfDesMarca.Value;
                this.txtResModelo.Text = this.hfDesModelo.Value;
                this.txtResAnio.Text = this.hfAnio.Value;
                this.txtResValor.Text = String.Format("{0:#,#0.00}", Convert.ToDecimal(this.hfValor.Value));


                this.pnlInicio.CssClass = "Form_Oculto";
                this.pnlResultado.CssClass = "Form_Visible";
            }
            else
            {
                this.MostrarMensaje(this.Controls, "No se encontró cotización para el vehículo ingresado", false);
            }
        }

        private void ActualizarValorHidden()
        {
            this.hfIdMarca.Value = this.ddlMarca.SelectedValue.ToString();
            this.hfDesMarca.Value = this.ddlMarca.SelectedItem.Text;
            this.hfIdModelo.Value = this.ddlModelo.SelectedValue.ToString();
            this.hfDesModelo.Value = this.ddlModelo.SelectedItem.Text;
            this.hfAnio.Value = this.ddlAnio.SelectedValue;
            this.hfValor.Value = this.txtValor.Text.Trim();
        }

        private void CrearTablaResultado(List<BECotizacion> lstBECotizacion) 
        {
            if (lstBECotizacion.Count == 0) { return; }

            Int32 nConta = 0;
            Int32 nIdProductoAnt = 0;
            List<Int32> lstIdProd = new List<Int32>();
            StringBuilder sbHtml = new StringBuilder();
            sbHtml.Append("<table border=" + (char)(34) + "0" + (char)(34) + " cellpadding=" + (char)(34) + "0" + (char)(34) + "cellspacing=" + (char)(34) + "0" + (char)(34) + "style=" + (char)(34) + "width:98%" + (char)(34) + ">");
            sbHtml.Append("<tr>");
            
            foreach (BECotizacion objBECotizacion in lstBECotizacion) 
            {
                if (nIdProductoAnt != objBECotizacion.IdProducto) 
                {
                    sbHtml.Append("<td>");
                    sbHtml.Append("<img src=" + (char)(34) + objBECotizacion.RutaLogoAseg + (char)(34) + " class=" + (char)(34) + "ps_Logo" + (char)(34) + " />");
                    sbHtml.Append("</td>");

                    lstIdProd.Add(objBECotizacion.IdProducto);
                }

                nIdProductoAnt = objBECotizacion.IdProducto;
            }
            sbHtml.Append("</tr>");
            sbHtml.Append("<tr>");

            nConta = 0;
            while (nConta < lstIdProd.Count)  
            {
                sbHtml.Append("<td style=" + (char)(34) + "vertical-align:top;" + (char)(34) + ">");
                sbHtml.Append(this.CrearDetalleResultado(lstBECotizacion, lstIdProd[nConta], lstIdProd.Count));
                sbHtml.Append("</td>");

                nConta++;
            }

            sbHtml.Append("</tr>");
            sbHtml.Append("</table>");
            this.dvResultado.InnerHtml = sbHtml.ToString();
        }

        private StringBuilder CrearDetalleResultado(List<BECotizacion> lstBECotizacion, Int32 pnIdProducto, Int32 pnCantProd) 
        {
            StringBuilder sbHtml = new StringBuilder();

            if (pnCantProd == 1) 
            {
                Int32 nTD = 1;

                sbHtml.Append("<table border=" + (char)(34) + "0" + (char)(34) + " cellpadding=" + (char)(34) + "0" + (char)(34) + "cellspacing=" + (char)(34) + "0" + (char)(34) + "style=" + (char)(34) + "width:98%" + (char)(34) + ">");

                foreach (BECotizacion objBECotizacion in lstBECotizacion)
                {
                    if (nTD == 1) { sbHtml.Append("<tr>"); }

                    sbHtml.Append("<td>");
                    sbHtml.Append("<table class=" + (char)(34) + "cot_Detalle" + (char)(34) + "border=" + (char)(34) + "0" + (char)(34) + " cellpadding=" + (char)(34) + "0" + (char)(34) + "cellspacing=" + (char)(34) + "0" + (char)(34) + "Style=" + (char)(34) + "border-style:groove; border-width:thin;" + (char)(34) + ">");
                    sbHtml.Append("<tr><td class=" + (char)(34) + "ps_Titulo" + (char)(34) + ">" + objBECotizacion.DesCategoria + " " + objBECotizacion.SeguroPlan + " " + "<input type=" + (char)(34) + "checkbox" + (char)(34) + " id=" + (char)(34) + "cbxPlan" + objBECotizacion.Correlativo.ToString() + (char)(34) + " runat=" + (char)(34) + "server" + (char)(34) + "clientidmode=" + (char)(34) + "Static" + (char)(34) + "onclick=" + (char)(34) + "return Plan('" + objBECotizacion.Correlativo.ToString() + "')" + (char)(34) + "/></td></tr>");
                    sbHtml.Append("<tr><td>" + "Tasa: " + Decimal.Round(objBECotizacion.TasaPorc, 2).ToString() + "%" + "</td></tr>");
                    sbHtml.Append("<tr><td>" + "Dcto: " + Decimal.Round(objBECotizacion.PorcDescuento, 2).ToString() + "%" + "</td></tr>");
                    sbHtml.Append("<tr><td>" + "Cap. Aseg.: " + String.Format("{0:#,#0.00}", objBECotizacion.ValVehiculo) + "</td></tr>");
                    sbHtml.Append("<tr><td>" + "Premio Anual: " + String.Format("{0:#,#0.00}", objBECotizacion.PrimaAnual) + "</td></tr>");
                    if (objBECotizacion.PrimaAnualMensual==0){
                       sbHtml.Append("<tr><td>" + "Premio Mensual:  Contado</td></tr>");
                    } else {
                       sbHtml.Append("<tr><td>" + "Premio Mensual: " + String.Format("{0:#,#0.00}", objBECotizacion.PrimaAnualMensual) + "</td></tr>");
                    }
                        if (!objBECotizacion.DesPromocion.Equals(""))
                    {
                        sbHtml.Append("<tr><td>" + "OBS: " + objBECotizacion.DesPromocion + "</td></tr>");
                    }
                    else {
                        sbHtml.Append("<tr><td>&nbsp</td></tr>");
                    }

                    
                    sbHtml.Append("</table>");
                    sbHtml.Append("</td>");

                    if (nTD == 3) {
                        sbHtml.Append("</tr>");
                        nTD = 0;
                    }

                    nTD++;
                }
                sbHtml.Append("</table>");
            }

            if (pnCantProd > 1) 
            {
                sbHtml.Append("<table border=" + (char)(34) + "0" + (char)(34) + " cellpadding=" + (char)(34) + "0" + (char)(34) + "cellspacing=" + (char)(34) + "0" + (char)(34) + ">");
                foreach (BECotizacion objBECotizacion in lstBECotizacion)
                {
                    if (objBECotizacion.IdProducto == pnIdProducto)
                    {
                        sbHtml.Append("<tr>");
                        sbHtml.Append("<td Style=" + (char)(34) + "width:50%; height:60px" + (char)(34) +">");
                        sbHtml.Append("<table class=" + (char)(34) + "cot_Detalle" + (char)(34) + "border=" + (char)(34) + "0" + (char)(34) + " cellpadding=" + (char)(34) + "0" + (char)(34) + "cellspacing=" + (char)(34) + "0" + (char)(34) + ">");
                        sbHtml.Append("<tr><td class=" + (char)(34) + "ps_Titulo" + (char)(34) + ">" + objBECotizacion.DesCategoria + " " + objBECotizacion.SeguroPlan + " " + "<input type=" + (char)(34) + "checkbox" + (char)(34) + " id=" + (char)(34) + "cbxPlan" + objBECotizacion.Correlativo.ToString() + (char)(34) + " runat=" + (char)(34) + "server" + (char)(34) + "clientidmode=" + (char)(34) + "Static" + (char)(34) + "onclick=" + (char)(34) + "return Plan('" + objBECotizacion.Correlativo.ToString() + "')" + (char)(34) + "/></td></tr>");
                        sbHtml.Append("<tr><td>" + "Tasa: " + Decimal.Round(objBECotizacion.TasaPorc, 2).ToString() + "%" + "</td></tr>");
                        sbHtml.Append("<tr><td>" + "Dcto: " + Decimal.Round(objBECotizacion.PorcDescuento, 2).ToString() + "%" + "</td></tr>");
                        sbHtml.Append("<tr><td>" + "Cap. Aseg.: " + String.Format("{0:#,#0.00}", objBECotizacion.ValVehiculo) + "</td></tr>");
                        sbHtml.Append("<tr><td>" + "Premio Anual: " + String.Format("{0:#,#0.00}", objBECotizacion.PrimaAnual) + "</td></tr>");

                        if (objBECotizacion.PrimaAnualMensual == 0)
                        {
                            sbHtml.Append("<tr><td>" + "Premio Mensual:  Contado</td></tr>");
                        }
                        else
                        {
                            sbHtml.Append("<tr><td>" + "Premio Mensual: " + String.Format("{0:#,#0.00}", objBECotizacion.PrimaAnualMensual) + "</td></tr>");
                        }
                        
                        if (!objBECotizacion.DesPromocion.Equals(""))
                        {
                            sbHtml.Append("<tr><td>" + "OBS: " + objBECotizacion.DesPromocion + "</td></tr>");
                        }                        else
                        {
                            sbHtml.Append("<tr><td>&nbsp</td></tr>");
                        }
                        sbHtml.Append("</table>");
                        sbHtml.Append("</td>");
                        sbHtml.Append("</tr>");
                    }
                }
            }

            sbHtml.Append("</table>");

            return sbHtml;
        }

        private void RegistrarCotizacion()
        {
            List<BECotizacion> lstBECotizacion = (List<BECotizacion>)(ViewState[Lista.lstCotizacion]);

            if (lstBECotizacion != null)
            {
                BECotizacion objBECotizacion = new BECotizacion();

                objBECotizacion.PriNombre = this.txtNombre1.Text.Trim().ToUpper();
                objBECotizacion.SegNombre = this.txtNombre2.Text.Trim().ToUpper();
                objBECotizacion.ApePaterno = this.txtApellido1.Text.Trim().ToUpper();
                objBECotizacion.ApeMaterno = this.txtApellido2.Text.Trim().ToUpper();
                objBECotizacion.IdTipoDocumento = this.ddlTipoDoc.SelectedValue;
                objBECotizacion.NroDocumento = this.txtNroDocumento.Text.Trim();
                objBECotizacion.TelDomicilio1 = this.txtTelefono.Text.Trim();
                objBECotizacion.TelMovil1 = this.txtCelular.Text.Trim();
                objBECotizacion.Email1 = this.txtEmail.Text.Trim();

                objBECotizacion.IdMarca = Convert.ToInt32(this.hfIdMarca.Value);
                objBECotizacion.IdModelo = Convert.ToInt32(this.hfIdModelo.Value);
                objBECotizacion.AnioFab = Convert.ToInt32(this.hfAnio.Value);
                objBECotizacion.ValOriVehiculo = Convert.ToDecimal(this.hfValor.Value);
                objBECotizacion.NroPlaca = this.txtNroPlaca.Text.Trim();
                objBECotizacion.UsuarioCreacion = Session[AONAffinity.Motor.Resources.NombreSession.Usuario].ToString();

                objBECotizacion.FecNacimiento = AONAffinity.Resources.NullTypes.FechaNull;
                objBECotizacion.FecIniVigPoliza = AONAffinity.Resources.NullTypes.FechaNull;
                objBECotizacion.FecFinVigPoliza = AONAffinity.Resources.NullTypes.FechaNull;
                objBECotizacion.FecValidez = AONAffinity.Resources.NullTypes.FechaNull;
                objBECotizacion.Sexo = AONAffinity.Resources.NullTypes.CadenaNull;
                objBECotizacion.Direccion = AONAffinity.Resources.NullTypes.CadenaNull;
                objBECotizacion.TelDomicilio2 = AONAffinity.Resources.NullTypes.CadenaNull;
                objBECotizacion.TelDomicilio3 = AONAffinity.Resources.NullTypes.CadenaNull;
                objBECotizacion.TelMovil2 = AONAffinity.Resources.NullTypes.CadenaNull;
                objBECotizacion.TelMovil3 = AONAffinity.Resources.NullTypes.CadenaNull;
                objBECotizacion.TelOficina1 = AONAffinity.Resources.NullTypes.CadenaNull;
                objBECotizacion.TelOficina2 = AONAffinity.Resources.NullTypes.CadenaNull;
                objBECotizacion.TelOficina3 = AONAffinity.Resources.NullTypes.CadenaNull;
                objBECotizacion.Email2 = AONAffinity.Resources.NullTypes.CadenaNull;
                objBECotizacion.NroMotor = AONAffinity.Resources.NullTypes.CadenaNull;
                objBECotizacion.NroSerie = AONAffinity.Resources.NullTypes.CadenaNull;
                objBECotizacion.Color = AONAffinity.Resources.NullTypes.CadenaNull;
                objBECotizacion.DirInspeccion = AONAffinity.Resources.NullTypes.CadenaNull;
                objBECotizacion.FecHorInspeccion = AONAffinity.Resources.NullTypes.CadenaNull;

                List<BECotizacionDetalle> lstBECotizacionDetalle = new List<BECotizacionDetalle>();


                String[] arrplan = this.hfPlanes.Value.Split(',');
                Int32 nConta = 0;

                while (nConta <= arrplan.Length - 1)
                {
                    foreach (BECotizacion objBECotizacion_Temp in lstBECotizacion)
                    {
                        objBECotizacion.IdProducto = objBECotizacion_Temp.IdProducto;

                        if (objBECotizacion_Temp.Correlativo.ToString() == arrplan[nConta])
                        {
                            BECotizacionDetalle objBECotizacionDetalle = new BECotizacionDetalle();
                            objBECotizacionDetalle.Correlativo = objBECotizacion_Temp.Correlativo;
                            objBECotizacionDetalle.IdProducto = objBECotizacion_Temp.IdProducto;
                            objBECotizacionDetalle.ValorAsegurado = objBECotizacion_Temp.ValVehiculo;
                            objBECotizacionDetalle.IdTasa = objBECotizacion_Temp.IdTasa;
                            objBECotizacionDetalle.ImpDescuento = objBECotizacion_Temp.ImporteDescuento;
                            objBECotizacionDetalle.ImpRecargo = objBECotizacion_Temp.ImporteRecargo;
                            objBECotizacionDetalle.PrimaCosto = objBECotizacion_Temp.CostoAnual;
                            objBECotizacionDetalle.PrimaTotal = objBECotizacion_Temp.PrimaAnual;
                            objBECotizacionDetalle.PrimaMensual = objBECotizacion_Temp.PrimaAnualMensual;
                            objBECotizacionDetalle.MonedaPrima = string.Empty;
                            objBECotizacionDetalle.OpcionPlan = objBECotizacion_Temp.SeguroPlan;
                            objBECotizacionDetalle.Elegido = false;

                            //Nuevos
                            objBECotizacionDetalle.IdCategoria = objBECotizacion_Temp.IdCategoria;
                            objBECotizacionDetalle.IdDescuento = objBECotizacion_Temp.IdDescuento;
                            objBECotizacionDetalle.IdRecargo = objBECotizacion_Temp.IdRecargo;
                            objBECotizacionDetalle.IdAdicional = objBECotizacion_Temp.IdAdicional;
                            objBECotizacionDetalle.MontoAdicional = objBECotizacion_Temp.MontoAdicional;
                            objBECotizacionDetalle.PagoContado = objBECotizacion_Temp.PagoContado;

                            objBECotizacionDetalle.PorcRecargo=objBECotizacion_Temp.PorcRecargo;
                            objBECotizacionDetalle.PorcDescuento = objBECotizacion_Temp.PorcDescuento;
                            objBECotizacionDetalle.Tasa = objBECotizacion_Temp.Tasa;
                            objBECotizacionDetalle.PorcTasa = objBECotizacion_Temp.TasaPorc;


                            objBECotizacionDetalle.UsuarioCreacion = Session[AONAffinity.Motor.Resources.NombreSession.Usuario].ToString();

                            lstBECotizacionDetalle.Add(objBECotizacionDetalle);
                        }
                    }

                    nConta++;
                }

                BLCotizacion objBLCotizacion = new BLCotizacion();
                Decimal nroCotizacion = objBLCotizacion.Regitrar(objBECotizacion, lstBECotizacionDetalle);

                String m_script;
                Guid m_Guid;

                if (nroCotizacion > 0)
                {
                    this.MostrarMensaje(this.Controls, "Se registró la cotización con exito", false);
                    this.btnGenerarPDF.Disabled = false;
                    this.btnGenerarPDF.Attributes.Add("onclick", "javascript:MostrarPCPrint('" + "frmPrint.aspx" + "','" + nroCotizacion + "');");
                    
                    m_script = " MostrarPCPrint('" + "frmPrint.aspx" + "','" + nroCotizacion + "'); ";
                    this.EjecutarJavaScript(this.Controls, m_script);

                    this.LimpiarRegistro();
                    this.btnRegistrar.Enabled = false;
                    this.btnAtras.Enabled = false;
                    this.CargarGridView(this.gvSeleccion, null);
                }
            }
        }

        private void LimpiarRegistro()
        {
            this.txtNombre1.Text = String.Empty;
            this.txtNombre2.Text = String.Empty;
            this.txtNroDocumento.Text = String.Empty;
            this.txtApellido1.Text = String.Empty;
            this.txtApellido2.Text = String.Empty;
            this.txtTelefono.Text = String.Empty;
            this.txtCelular.Text = String.Empty;
            this.txtEmail.Text = String.Empty;
            this.txtNroPlaca.Text = String.Empty;
            this.txtMarca.Text = String.Empty;
            this.txtModelo.Text = String.Empty;
            this.txtAnio.Text = String.Empty;
            this.txtValorVeh.Text = String.Empty;
            this.ddlAdicionales.SelectedIndex = 0;
            this.ddlAnio.SelectedIndex = 0;
            this.ddlDescuento.SelectedIndex = 0;
            this.ddlMarca.SelectedIndex = 0;
            this.ddlModelo.SelectedIndex = 0;
            this.ddlRecargo.SelectedIndex = 0;
            this.ddlTipoDoc.SelectedIndex = 0;
            ViewState[Lista.lstCotizacion] = null;
            this.btnGenerarPDF.Disabled = true;
            this.btnGenerarPDF.Attributes.Add("onclick", "javascript:return false;");

        }

        private void ReiniciarFormulario()
        {
            ViewState[Lista.lstCotizacion] = null;

            this.hfIdMarca.Value = String.Empty;
            this.hfDesMarca.Value = String.Empty;
            this.hfIdModelo.Value = String.Empty;
            this.hfDesModelo.Value = String.Empty;
            this.hfAnio.Value = String.Empty;
            this.hfValor.Value = String.Empty;
            this.hfAseguradoras.Value = String.Empty;

            this.txtNombre1.Text = String.Empty;
            this.txtNombre2.Text = String.Empty;
            this.txtNroDocumento.Text = String.Empty;
            this.txtApellido1.Text = String.Empty;
            this.txtApellido2.Text = String.Empty;
            this.txtTelefono.Text = String.Empty;
            this.txtEmail.Text = String.Empty;

            this.txtMarca.Text = String.Empty;
            this.txtModelo.Text = String.Empty;
            this.txtAnio.Text = String.Empty;
            this.txtValorVeh.Text = String.Empty;

            this.ddlMarca.SelectedIndex = 0;
            this.ddlModelo.SelectedIndex = 0;
            this.ddlAnio.SelectedIndex = 0;
            this.ddlRecargo.SelectedIndex = 0;
            this.ddlDescuento.SelectedIndex = 0;
            this.ddlAdicionales.SelectedIndex = 0;
            this.txtValor.Text = String.Empty;

            this.btnRegistrar.Enabled = true;
            this.btnAtras.Enabled = true;
            this.hfPlanes.Value = "";

            this.pnlRegistro.CssClass = "Form_Oculto";
            this.pnlResultado.CssClass = "Form_Oculto";
            this.pnlInicio.CssClass = "Form_Visible";
        }

        private void MostarPlanesEscogidos()
        {
            String[] arrplan = this.hfPlanes.Value.Split(',');
            Int32 nConta = 0;

            List<BECotizacion> lstBECotizacion = (List<BECotizacion>)(ViewState[Lista.lstCotizacion]);

            List<BECotizacion> lstBECotizacion_tmp = new List<BECotizacion>();

            while (nConta <= arrplan.Length - 1)
            {
                foreach (BECotizacion objBECotizacion_Temp in lstBECotizacion)
                {
                    if (objBECotizacion_Temp.Correlativo.ToString() == arrplan[nConta])
                    {
                        lstBECotizacion_tmp.Add(objBECotizacion_Temp);
                    }
                }

                nConta++;
            }
            //this.CargarGridView(this.gvSeleccion, null);
            this.CargarGridView(this.gvSeleccion, lstBECotizacion_tmp);
        }
        #endregion

        protected void gvSeleccion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow) {
               if(e.Row.Cells[5].Text == "0,00"){
                   e.Row.Cells[5].Text = "Contado";
               }
                //Label lblMontoMensual = (Label)e.Row.Cells[5].Controls[1];
                //lblMontoMensual.Text = "sss";
            
            }
        }

        #region slide
        //sbHtml.Append("<div class=" + (char)(34) + "ps_album" + (char)(34) + " style=" + (char)(34) + "opacity:0;" + (char)(34) + ">");

        //sbHtml.Append("<img src=" + (char)(34) + objBECotizacion.RutaLogoAseg + (char)(34) + " class=" + (char)(34) + "ps_Logo" + (char)(34) + " />");
        //sbHtml.Append("<div class=" + (char)(34) + "ps_desc" + (char)(34) + ">");
        //sbHtml.Append("<table border=" + (char)(34) + "0" + (char)(34) + " cellpadding=" + (char)(34) + "0" + (char)(34) + "cellspacing=" + (char)(34) + "0" + (char)(34) + ">");
        //sbHtml.Append("<tr><td class=" + (char)(34) + "ps_Titulo" + (char)(34) + ">" + objBECotizacion.DesProducto + "</td></tr>");
        //sbHtml.Append("<tr><td class=" + (char)(34) + "ps_Titulo" + (char)(34) + ">" + objBECotizacion.DesCategoria + " " + objBECotizacion.SeguroPlan + "</td></tr>");
        //sbHtml.Append("<tr><td>" + "Capital Asegurado: " + String.Format("{0:#,#0.00}", objBECotizacion.ValVehiculo) + "</td></tr>");

        //if (objBECotizacion.TasaPorc != 0)
        //{
        //    sbHtml.Append("<tr><td>" + "Tasa: " + Decimal.Round(objBECotizacion.TasaPorc, 2).ToString() + "%" + "</td></tr>");
        //}

        //if (objBECotizacion.PorcDescuento != 0 || objBECotizacion.PorcRecargo != 0)
        //{
        //    sbHtml.Append("<tr><td>" + "Costo: " + String.Format("{0:#,#0.00}", objBECotizacion.CostoAnual) + "" + "</td></tr>");
        //}

        //if (objBECotizacion.PorcDescuento != 0)
        //{
        //    sbHtml.Append("<tr><td>" + "Dcto: " + Decimal.Round(objBECotizacion.PorcDescuento, 2).ToString() + "%" + "</td></tr>");
        //    sbHtml.Append("<tr><td>" + "Monto Dcto: " + String.Format("{0:#,#0.00}", objBECotizacion.ImporteDescuento) + "" + "</td></tr>");
        //}
        //if (objBECotizacion.PorcRecargo != 0)
        //{
        //    sbHtml.Append("<tr><td>" + "Rcgo: " + String.Format("{0:#,#0.00}", objBECotizacion.PorcRecargo) + "%" + "</td></tr>");
        //    sbHtml.Append("<tr><td>" + "Monto Rcgo: " + String.Format("{0:#,#0.00}", objBECotizacion.ImporteRecargo) + "" + "</td></tr>");
        //}
        //sbHtml.Append("<tr><td>" + "Premio Anual: " + String.Format("{0:#,#0.00}", objBECotizacion.PrimaAnual) + "</td></tr>");
        //sbHtml.Append("<tr><td>" + "Premio Mensual: " + String.Format("{0:#,#0.00}", objBECotizacion.PrimaAnualMensual) + "</td></tr>");
        ////sbHtml.Append("<tr><td class=" + (char)(34) + "ps_TdBoton" + (char)(34) + "><input id=" + (char)(34) + "" + (char)(34) + " type=" + (char)(34) + "button" + (char)(34) + "name=" + (char)(34) + "Contratar" + (char)(34) + "value=" + (char)(34) + "Contratar" + (char)(34) + "/></td></tr>");
        //sbHtml.Append("<input type=" + (char)(34) + "checkbox" + (char)(34) + " id=" + (char)(34) + "cbxPlan" + objBECotizacion.Correlativo.ToString() + (char)(34) + " runat=" + (char)(34) + "server" + (char)(34) + "clientidmode=" + (char)(34) + "Static" + (char)(34) + "onclick=" + (char)(34) + "return Plan('" + objBECotizacion.Correlativo.ToString() + "')" + (char)(34) + "/>");
        //sbHtml.Append("</table>");
        //sbHtml.Append("</div>");
        //sbHtml.Append("</div>");
        #endregion

        //Soluciona problema de compatibilidad en IE7
        protected override void Render(HtmlTextWriter writer)
        {
            Control meta = Header.FindControl(DevExpress.Web.ASPxClasses.Internal.RenderUtils.IE8CompatibilityMetaID);
            if (meta != null) meta.Parent.Controls.Remove(meta);

            base.Render(writer);
        }





        protected void ibtnBuscarCliente_Click(object sender, ImageClickEventArgs e)
        {
            ArrayList ListaClientes = BuscarClientes(ddlTipoDoc.SelectedValue, txtNroDocumento.Text.Trim());
            if (ListaClientes.Count == 0)
            {
                //No se encontró ningún cliente
                ScriptManager.RegisterStartupScript(this.Page, base.GetType(), "Mensaje", "alert('No se encontró ningún cliente con el N° Documento " + txtNroDocumento.Text.Trim() + ".');", true);
            }
            else if (ListaClientes.Count == 1)
            {
                //Se encontró un cliente
                BEClienteWS oBECLiente = (BEClienteWS)ListaClientes[0];
                oBECLiente = BuscarCliente(oBECLiente.NroCliente, true);
                SetClienteEncontradoA(oBECLiente);
            }
            else
            {
                //Asegurado
                gvSeleccionarCliente.DataSource = ListaClientes;
                gvSeleccionarCliente.DataBind();
                ScriptManager.RegisterStartupScript(this.Page, base.GetType(), "mSeleccionarCliente", "SeleccionarCliente()", true);
            }


        }

        private void SetClienteEncontradoA(BEClienteWS oBECLiente)
        {            
            if (oBECLiente.CodRetorno == "0")
            {
                txtApellido1.Text = oBECLiente.Apellido1;
                txtApellido2.Text = oBECLiente.Apellido2;
                txtNombre1.Text = oBECLiente.Nombre1;
                txtNombre2.Text = oBECLiente.Nombre2;                                
                txtTelefono.Text = oBECLiente.Telefono;
                txtCelular.Text = oBECLiente.Celular;
                txtEmail.Text = oBECLiente.Email;  
               if (oBECLiente.EnRecuperacion)
                   ScriptManager.RegisterStartupScript(this.Page, base.GetType(), "Mensaje", "alert('Cliente en recuperación.');", true);
            }
            else
            {                
                ScriptManager.RegisterStartupScript(this.Page, base.GetType(), "Mensaje", "alert('No se encontró ningún cliente con el N° Documento: " + txtNroDocumento.Text.Trim() + ".');", true);
              
            }
        }


        private ArrayList BuscarClientes(string IDTipoDocumento, string NroDocumento)
        {
            ArrayList ListaCliente = new ArrayList();
            BEClienteWS oBECliente = default(BEClienteWS);
            BLListaEquivalencia oBLEquivalencia = new BLListaEquivalencia();
            Int32 pIDProducto = 5800;
            IDTipoDocumento = oBLEquivalencia.getValorEquivalencia(pIDProducto, "TipoDocumento", IDTipoDocumento).ValorEquivalencia;
            string UsuarioWS = ConfigurationManager.AppSettings["UsuarioWS"].ToString();
            string ClaveWS = ConfigurationManager.AppSettings["ClaveWS"].ToString();
            //---CLIENTES--------------------------
            ITAU_PY_CLIENTES.Clientes2WsService WSCliente = new ITAU_PY_CLIENTES.Clientes2WsService();
            string pBuscarCliente = SetBuscarCliente(IDTipoDocumento, NroDocumento);
            string RespCliente = WSCliente.getClientes(UsuarioWS, ClaveWS, pBuscarCliente);
            XmlDocument xmlRespCliente = new XmlDocument();
            xmlRespCliente.LoadXml(RespCliente);
            ////xmlRespCliente.Load("http://localhost:4457/WSxml/getClientes.xml");
            XmlNode rNodeCliente = xmlRespCliente["retorno"];
            if ((rNodeCliente.Attributes["codRetorno"].Value == "0"))
            {
                XmlNode Clientes = rNodeCliente["xml"]["clientes"];
                foreach (XmlNode nCliente in Clientes)
                {
                    oBECliente = new BEClienteWS();
                    var _with1 = nCliente;
                    oBECliente.NroCliente = nCliente.Attributes["nroCliente"].Value.Trim();
                    //
                    oBECliente.Nombre1 = nCliente.Attributes["apellido"].Value.Trim() + " " + nCliente.Attributes["nombre"].Value.Trim();
                    oBECliente.NroDocumento = nCliente.Attributes["nroDocumentoCompleto"].Value.Trim();
                    string fechaNac = _with1.Attributes["fechaNacimiento"].Value;
                    oBECliente.FechaNacimiento = fechaNac.Substring(8, 2) + "/" + fechaNac.Substring(5, 2) + "/" + fechaNac.Substring(0, 4);
                    ListaCliente.Add(oBECliente);
                }
            }
            return ListaCliente;
        }
        
        private BEClienteWS BuscarCliente(string pNroCliente, bool pValCliente)
        {
            BEClienteWS oBECliente = new BEClienteWS();
            oBECliente.CodRetorno = "-1";
           //BLListaEquivalencia oBLEquivalencia = new BLListaEquivalencia();
            string UsuarioWS = ConfigurationManager.AppSettings["UsuarioWS"].ToString();
            string ClaveWS = ConfigurationManager.AppSettings["ClaveWS"].ToString();
            //---CLIENTES--------------------------
            ITAU_PY_CLIENTES.Clientes2WsService WSCliente = new ITAU_PY_CLIENTES.Clientes2WsService();
            string pBuscarDatosCliente = SetBuscarDatosCliente(pNroCliente);
            string RespDatosCliente = WSCliente.getCliente(UsuarioWS, ClaveWS, pBuscarDatosCliente);
            XmlDocument xmlRespDatosCliente = new XmlDocument();
            xmlRespDatosCliente.LoadXml(RespDatosCliente);
            ////xmlRespDatosCliente.Load("http://localhost:4457/WSxml/getCliente.xml");
            XmlNode rNodeCliente = xmlRespDatosCliente["retorno"];
            if ((rNodeCliente.Attributes["codRetorno"].Value == "0"))
            {
                oBECliente.CodRetorno = "0";
                //--Datos Del Cliente
                oBECliente.NroCliente = pNroCliente;
                XmlNode DatosCliente = rNodeCliente["xml"]["clientes"]["cliente"];
                var _with1 = DatosCliente;
                string[] aApellido = SepararApellido(_with1.Attributes["apellido"].Value.Trim());
                oBECliente.Apellido1 = aApellido.Length > 0 ? aApellido[0].ToString() : "";
                oBECliente.Apellido2 = aApellido.Length > 1 ? aApellido[1].ToString() : "";
                string[] aNombre = SepararApellido(_with1.Attributes["nombre"].Value.Trim());
                oBECliente.Nombre1 = aNombre.Length > 0 ? aNombre[0].ToString() : "";
                oBECliente.Nombre2 = aNombre.Length > 1 ? aNombre[1].ToString() : "";


                //Teléfonos------------------
                string RespTelfCliente = WSCliente.getClienteTelefonos(UsuarioWS, ClaveWS, pBuscarDatosCliente);
                XmlDocument xmlTelfCliente = new XmlDocument();
                xmlTelfCliente.LoadXml(RespTelfCliente);
                ////xmlTelfCliente.Load("http://localhost:4457/WSxml/getClienteTelefonos.xml");
                if ((xmlTelfCliente["retorno"].Attributes["codRetorno"].Value == "0"))
                {
                    //---
                    XmlNode Telefonos = xmlTelfCliente["retorno"]["xml"]["clienteTelefonos"];
                    foreach (XmlNode nTelefono in Telefonos)
                    {
                        //Teléfono-----tipoMedioId(1=Teléfono,2=Celular)   categoriaCd(P=Particular, L=Laboral)
                        if (nTelefono.Attributes["tipoMedioId"].Value == "1" & nTelefono.Attributes["categoriaCd"].Value == "P")
                        {
                            oBECliente.Telefono = nTelefono.Attributes["medio"].Value;
                        }
                        //Otro Teléfono Laboral-----
                        if (nTelefono.Attributes["tipoMedioId"].Value == "1" & nTelefono.Attributes["categoriaCd"].Value == "L")
                        {
                            oBECliente.OtroTelefono = nTelefono.Attributes["medio"].Value;
                        }
                        //Celular-----
                        if (nTelefono.Attributes["tipoMedioId"].Value == "2" & nTelefono.Attributes["categoriaCd"].Value == "P")
                        {
                            oBECliente.Celular = nTelefono.Attributes["medio"].Value;
                        }
                    }

                }
                //BCtxtEmail.Text = Cliente.datosContacto(0).email.Trim
                
                
                if (pValCliente)
                {
                    //Servicio que recupera el comportamiento del cliente------
                    string RespCliRecuperacion = WSCliente.getClienteComportamiento(UsuarioWS, ClaveWS, pBuscarDatosCliente);
                    XmlDocument xmlCliRecuperacion = new XmlDocument();
                    xmlCliRecuperacion.LoadXml(RespCliRecuperacion);
                    ////xmlCliRecuperacion.Load("http://localhost:4457/WSxml/getClienteComportamiento.xml");
                    if ((xmlCliRecuperacion["retorno"].Attributes["codRetorno"].Value == "0"))
                    {
                        //---
                        XmlNode ClienteComportamiento = xmlCliRecuperacion["retorno"]["xml"]["clienteComportamiento"];
                        if (ClienteComportamiento.Attributes["recuperaciones"].Value.Trim() == "S")
                        {
                            oBECliente.EnRecuperacion = true;
                        }
                        else
                        {
                            oBECliente.EnRecuperacion = false;
                        }
                    }
                }

            }
            return oBECliente;
        }

        private string[] SepararApellido(string pApellido)
        {
            ArrayList aApellido = new ArrayList();
            string[] tabla = pApellido.Split(new Char[] { ' ' });            
            return tabla;
        }

        private string SetBuscarCliente(string IDTipoDoc, string NroDocumento)
        {
            XmlDocument Doc = new XmlDocument();
            XmlElement DocRoot = Doc.CreateElement("params");
            Doc.AppendChild(DocRoot);
            DocRoot.SetAttribute("canal", "37");
            DocRoot.SetAttribute("pgmcaller", "Venta");
            DocRoot.SetAttribute("estacion", "10.82.11.107");
            DocRoot.SetAttribute("usuario", "ilopez");
            DocRoot.SetAttribute("tipoDocumentoId", IDTipoDoc);
            DocRoot.SetAttribute("nroDocumento", NroDocumento);
            DocRoot.SetAttribute("nombre", "");
            DocRoot.SetAttribute("apellidoRazon", "");
            return Doc.OuterXml;
        }
        
        private string SetBuscarDatosCliente(string NroCliente)
        {
            XmlDocument Doc = new XmlDocument();
            XmlElement DocRoot = Doc.CreateElement("params");
            Doc.AppendChild(DocRoot);
            DocRoot.SetAttribute("canal", "2");
            DocRoot.SetAttribute("pgmcaller", "consulta");
            DocRoot.SetAttribute("estacion", "10.82.11.107");
            DocRoot.SetAttribute("usuario", "ilopez");
            DocRoot.SetAttribute("nroCliente", NroCliente);
            return Doc.OuterXml;
        }

        protected void gvSeleccionarCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            string pNroCliente = gvSeleccionarCliente.SelectedDataKey["NroCliente"].ToString();
            BEClienteWS oBECLiente = default(BEClienteWS);

            //Asegurado
            oBECLiente = BuscarCliente(pNroCliente, true);
            SetClienteEncontradoA(oBECLiente);      
           
            gvSeleccionarCliente.SelectedIndex = -1;
            gvSeleccionarCliente.DataSource = null;
            gvSeleccionarCliente.DataBind();

            ScriptManager.RegisterStartupScript(this.Page, base.GetType(), "MsgCS", "CerrarSelCliente()", true);
        }

    }


}