﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using VAN.Common.BL;
using System.Web.Security;
using VAN.Common.BE;

namespace AONWebMotor.Seguridad 
{
    public partial class CambiarClave : PaginaBase
    {
        protected void Page_Load(object sender, System.EventArgs e)
        {

            if (!IsPostBack)
            {
                if (!ValidarAcceso(Page.AppRelativeVirtualPath)){
                    Response.Redirect("~/Login.aspx", false);
                }
                try
                {

                    if (string.IsNullOrEmpty(Session["IDUsuarioDominio"].ToString())) {
                        if (Convert.ToBoolean(Session["PrimerLogin"])){
                            lblEstadoClave.Text = "Por favor actualice la contraseña";
                        }
                        if (Convert.ToBoolean(Session["ClaveCaducada"])){
                            lblEstadoClave.Text = "Contraseña caducada, Por favor actualice la contraseña";
                        }
                        //----Cargar Parámetros de Seguridad
                        IList pListParametro = (IList)Session["SeguridadPar"];
                        BLSeguridadConfig oBLCSeguridad = new BLSeguridadConfig();

                        //Longitud de caracteres mínima para la contraseña (Número)
                        Int32 pLongMinClave = Convert.ToInt32(oBLCSeguridad.Parametro("CONTRASE02", pListParametro));
                        if (pLongMinClave == 0)
                        {
                            //0 No válida la longitud
                            revLongitudNuevaC.Enabled = false;
                        }
                        else
                        {
                            revLongitudNuevaC.Enabled = true;
                            revLongitudNuevaC.ValidationExpression = "[\\S\\s]{" + pLongMinClave.ToString() + ",50}";
                            revLongitudNuevaC.ErrorMessage = "La nueva contraseña debe tener una longitud mínima de " + pLongMinClave.ToString() + " caracteres";
                        }

                        //Contraseña Segura
                        revSeguridadClave.Enabled = Convert.ToBoolean(Int16.Parse(oBLCSeguridad.Parametro("CONTRASE03", pListParametro)));

                        //Modificar la Clave solo una vez al día
                        if (Convert.ToBoolean(Int16.Parse(oBLCSeguridad.Parametro("CONTRASE06", pListParametro))))
                        {
                            BLSeguridadEvento oBLSegEvento = new BLSeguridadEvento();
                            if (oBLSegEvento.ClaveModificadaHoy(User.Identity.Name))
                            {
                                MultiView1.ActiveViewIndex = 2;
                            }
                        }
                    }else{
                        lblMensajeUsu.Text = "Esta opción no se encuentra disponible para usuarios de Dominio.";
                         MultiView1.ActiveViewIndex = 2;
                    }


                }
                catch (Exception ex)
                {
                    Response.Redirect("~/Login.aspx", false);
                }


            }
        }

        protected void btnCambiar_Click(object sender, EventArgs e)
        {
            MembershipUser u = Membership.GetUser(User.Identity.Name);
            try
            {
                if (u.ChangePassword(txtClaveActual.Text, txtNuevaClave.Text))
                {
                    //Dim NClave As String = FormsAuthentication.HashPasswordForStoringInConfigFile(txtNuevaClave.Text, "md5") '.GetHashCode()
                    //Registro Cambio de Contraseña --------------
                    BESeguridadEvento oBESegEvento = new BESeguridadEvento();
                    BLSeguridadEvento oBLSegEvento = new BLSeguridadEvento();
                    oBESegEvento.IDSistema = IDSistema();
                    oBESegEvento.IDUsuario = User.Identity.Name;
                    oBESegEvento.IDTipoEvento = 3;
                    //Cambio de Contraseña
                    oBESegEvento.Host = Request.UserHostAddress;
                    //oBESegEvento.Detalle = u.
                    oBLSegEvento.Insertar(oBESegEvento);
                    if (Convert.ToBoolean(Session["PrimerLogin"]))
                    {
                        oBESegEvento.IDTipoEvento = 1;
                        //Registra Primer Login
                        oBLSegEvento.Insertar(oBESegEvento);
                    }

                    MultiView1.ActiveViewIndex = 1;
                    Session["PrimerLogin"] = false;
                    Session["ClaveCaducada"] = false;
                    lblEstadoClave.Text = "";
                }
                else
                {
                    Msg.Text = "Contraseña actual incorrecta";
                }
            }
            catch (Exception ex)
            {
                Msg.Text = "Ocurrió un error: " + Server.HtmlEncode(ex.Message) + ". Por favor vuelva a intentar.";
            }
        }

        protected void btnContinuar_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Default.aspx", false);
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            txtClaveActual.Text = "";
            txtNuevaClave.Text = "";
            txtNuevaClaveConfir.Text = "";
        }
    }
}