<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true"
    CodeBehind="frmExpedir.aspx.cs" Inherits="AONWebMotor.Tareas.frmExpedir" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Controles/MsgBox.ascx" TagName="MsgBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <script language="javascript" type="text/javascript">
        function SolicitarCuenta()
        {
            if (document.getElementById('<%=cbxSolCuenta.ClientID %>').checked) {
                document.getElementById('<%=ddlMedPago.ClientID %>').disabled = false;
                document.getElementById('<%=txtNroCuenta.ClientID %>').disabled = false;
                document.getElementById('<%=ddlMonCuenta.ClientID %>').disabled = false;
                document.getElementById('<%=txtFecVencimiento.ClientID %>').disabled = false;                                    
            }
            else {
                document.getElementById('<%=ddlMedPago.ClientID %>').disabled = true;
                document.getElementById('<%=txtNroCuenta.ClientID %>').disabled = true;
                document.getElementById('<%=ddlMonCuenta.ClientID %>').disabled = true;
                document.getElementById('<%=txtFecVencimiento.ClientID %>').disabled = true;                 
            }
        }        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="Form_SubTitulo" colspan="6">
                            Opciones
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer" style="width: 150px">
                            Cambiar Valor Veh�culo:
                        </td>
                        <td class="Form_TextoIzq" style="width: 150px">
                            <asp:DropDownList ID="ddlSigno" runat="server" Width="40px">
                                <asp:ListItem Text="" Value="-1"></asp:ListItem>
                                <asp:ListItem Text="+" Value="+"></asp:ListItem>
                                <asp:ListItem Text="-" Value="-"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvSigno" runat="server" Text="*" ControlToValidate="ddlSigno"
                                Display="None" ErrorMessage="Seleccione signo de porcentaje" SetFocusOnError="true"
                                InitialValue="-1" ValidationGroup="CambiarValor">
                            </asp:RequiredFieldValidator>
                            <asp:DropDownList ID="ddlProcentaje" runat="server" Width="40px">
                                <asp:ListItem Text="" Value="-1"></asp:ListItem>
                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                <asp:ListItem Text="1" Value="1"></asp:ListItem>
                            </asp:DropDownList>
                            %
                            <asp:RequiredFieldValidator ID="rfvProcentaje" runat="server" Text="*" ControlToValidate="ddlProcentaje"
                                Display="None" ErrorMessage="Seleccione porcentaje" SetFocusOnError="true" InitialValue="-1"
                                ValidationGroup="CambiarValor">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:Button ID="btnCambiarValVeh" runat="server" Text="Cambiar" ValidationGroup="CambiarValor"
                                OnClick="btnCambiarValVeh_Click" />
                            <asp:Button ID="btnValorOri" runat="server" Text="Valor Original" OnClick="btnValorOri_Click" />
                        </td>
                        <td class="Form_TextoDer">
                        </td>
                        <td class="Form_TextoIzq">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                <div style="width: 100%; height: 380px; overflow: auto;">
                    <rsweb:ReportViewer ID="rvVenta" runat="server" ShowPrintButton="False" Width="100%"
                        Font-Names="Verdana" Font-Size="8pt" Height="350px" ZoomPercent="130" ShowExportControls="False"
                        ShowZoomControl="False">
                        <LocalReport ReportPath="rdlc\rdlcScriptVenta.rdlc">
                            <DataSources>
                                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="BECotizacion" />
                            </DataSources>
                        </LocalReport>
                    </rsweb:ReportViewer>
                </div>
                <br />
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td>
                            <div class="Form_RegButtonDer">
                                <asp:Button ID="btnAceptar" runat="server" Text="Acepta" OnClientClick="return showModalPopup('bmpeCotCertificado');" />
                                <asp:Button ID="btnImprimir" runat="server" Text="Imprimir" Enabled="true"  OnClientClick="return showModalPopup('bmpCotDocCertificado');" />
                                <asp:Button ID="btnCotRechazo" runat="server" Text="No Acepta" OnClientClick="showModalFocus('bmpeCotRechazo','ctl00_ContentPlaceHolder1_ddlTipMotRechazo'); return false;" />
                                <asp:Button ID="btnCotMail" runat="server" Text="Enviar Mail" OnClientClick="showModalFocus('bmpeCotMail','ctl00_ContentPlaceHolder1_txtEmail'); return false;" />
                                <asp:Button ID="btnNoEncontro" runat="server" Text="No se Contact�" />
                            </div>
                        </td>
                    </tr>
                </table>
                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="Obtener"
                    TypeName="AONAffinity.Business.Logic.BLCotizacion">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="1" Name="pnIdPeriodo" Type="Int32"></asp:Parameter>
                        <asp:Parameter DefaultValue="103" Name="pnNroCotizacion" Type="Decimal" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <uc1:MsgBox ID="msgBox" runat="server" />
    <asp:ValidationSummary ID="vsCambiarValor" runat="server" ValidationGroup="CambiarValor"
        ShowMessageBox="true" ShowSummary="false" />
    <div style="display:none ;">
        <asp:HiddenField ID="hfIdCertificado" runat="server" />
    </div>            
    <!--Popup Cotizacion Mail-->
    <asp:Panel ID="pnMail" runat="server" Width="400px" CssClass="Modal_Panel" Style="display: none;"
        DefaultButton="btnEnvCotMail">
        <div class="Modal_Head" style="width: 100%;">
            <div id="divHeadTitulo" style="float: left;">
                Enviar Mail
            </div>
            <div id="divHeadCerrar" style="float: right;">
                <img alt="Click para cerrar." class="Modal_CloseButton" onclick="hideModalPopup('bmpeCotMail');"
                    style="cursor: hand;" />
            </div>
        </div>
        <div id="divBodyMail" class="Modal_Body">
            <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="Form_TextoDer">
                        Email: <span class="Form_TextoObligatorio">*</span>
                    </td>
                    <td class="Form_TextoIzq">
                        <asp:TextBox ID="txtEmail" runat="server" Width="300px" CssClass="Form_TextBox"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                            Display="None" SetFocusOnError="true" ErrorMessage="Ingrese mail." ValidationGroup="Email">
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                            Display="None" SetFocusOnError="true" ErrorMessage="Email tiene formato incorrecto."
                            ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Email">
                        </asp:RegularExpressionValidator>
                    </td>
                    <td class="Form_TextoIzq" style="width: 16px">
                        <asp:UpdateProgress ID="upsCotMail" runat="server" AssociatedUpdatePanelID="upCotMail"
                            DisplayAfter="0">
                            <ProgressTemplate>
                                <asp:Image ID="imgLoadCotMail" runat="server" SkinID="sknImgLoading" Width="16px"
                                    Height="16px" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
                <tr>
                    <td class="Form_TextoDer">
                        Cc:
                    </td>
                    <td class="Form_TextoIzq">
                        <asp:TextBox ID="txtEmailCc" runat="server" Width="300px" CssClass="Form_TextBox"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revEmailCc" runat="server" ControlToValidate="txtEmailCc"
                            Display="None" SetFocusOnError="true" ErrorMessage="Email de copia tiene formato incorrecto."
                            ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Email">
                        </asp:RegularExpressionValidator>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="Form_TextoIzq" colspan="3">
                        <span class="Form_TextoObligatorio">&nbsp;(*)Campos Obligatorios</span>
                    </td>
                </tr>
            </table>
            <br />
            <div style="width: 100%; text-align: center;">
                <asp:UpdatePanel ID="upCotMail" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnEnvCotMail" runat="server" Text="Enviar" ValidationGroup="Email"
                            CausesValidation="true" OnClick="btnEnvCotMail_Click" />
                        <br />
                        <asp:Label ID="lblResCotMail" runat="server" ForeColor="Red"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnEnvCotMail" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <div>
                <asp:ValidationSummary ID="vsMail" runat="server" CssClass="Form_ValSumary" ValidationGroup="Email" />
            </div>
        </div>
    </asp:Panel>
    <asp:Label ID="lblCotMail" runat="server" Style="display: none;"></asp:Label>
    <cc1:ModalPopupExtender ID="mpeCotMail" runat="server" Enabled="true" TargetControlID="lblCotMail"
        BehaviorID="bmpeCotMail" PopupControlID="pnMail" BackgroundCssClass="Modal_Background">
    </cc1:ModalPopupExtender>
    <!--Popup Cotizacion Rechazo-->
    <asp:Panel ID="pnCotRechazo" runat="server" Width="400px" CssClass="Modal_Panel"
        Style="display: none;" DefaultButton="btnRegCotRechazo">
        <div class="Modal_Head" style="width: 100%;">
            <div id="div1" style="float: left;">
                Registrar Motivo
            </div>
            <div id="div2" style="float: right;">
                <img alt="Click para cerrar." class="Modal_CloseButton" onclick="hideModalPopup('bmpeCotRechazo')"
                    style="cursor: hand;" />
            </div>
        </div>
        <div id="div3" class="Modal_Body">
            <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="Form_TextoDer">
                        Tipo:
                    </td>
                    <td class="Form_TextoIzq">
                        <asp:DropDownList ID="ddlTipMotRechazo" runat="server" Width="200px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvTipMotRechazo" runat="server" ErrorMessage="Seleccione el tipo de rechazo."
                            Display="None" ControlToValidate="ddlTipMotRechazo" SetFocusOnError="true" InitialValue="-1"
                            ValidationGroup="CotizacionRechazo"></asp:RequiredFieldValidator>
                    </td>
                    <td class="Form_TextoIzq">
                        <asp:UpdateProgress ID="upsCotRechazo" runat="server" AssociatedUpdatePanelID="upCotRechazo"
                            DisplayAfter="0">
                            <ProgressTemplate>
                                <asp:Image ID="imgLoadOficina1" runat="server" SkinID="sknImgLoading" Width="16px"
                                    Height="16px" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
                <tr>
                    <td class="Form_TextoDer" style="vertical-align: top;">
                        Motivo:
                    </td>
                    <td class="Form_TextoIzq" colspan="2">
                        <asp:TextBox ID="txtMotRechazo" runat="server" Width="320px" TextMode="MultiLine"
                            MaxLength="350" CssClass="Form_TextBox" Height="100px"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <br />
            <div style="width: 100%; text-align: center;">
                <asp:UpdatePanel ID="upCotRechazo" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnRegCotRechazo" runat="server" Text="Registrar" CausesValidation="true"
                            ValidationGroup="CotizacionRechazo" OnClick="btnRegCotRechazo_Click" />
                        <br />
                        <asp:Label ID="lblResCotRechazo" runat="server" ForeColor="Red"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnRegCotRechazo" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <div>
                <asp:ValidationSummary ID="vsCotRechazo" runat="server" CssClass="Form_ValSumary"
                    ValidationGroup="CotizacionRechazo" ShowMessageBox="false" ShowSummary="true" />
            </div>
        </div>
    </asp:Panel>
    <asp:Label ID="lblCotRechazo" runat="server" Style="display: none;"></asp:Label>
    <cc1:ModalPopupExtender ID="mpeCotRechazo" runat="server" Enabled="true" TargetControlID="lblCotRechazo"
        BehaviorID="bmpeCotRechazo" PopupControlID="pnCotRechazo" BackgroundCssClass="Modal_Background">
    </cc1:ModalPopupExtender>
    <!--Popup Cotizacion Certificado-->
    <asp:Panel ID="pnCertificado" runat="server" Width="900px" CssClass="Modal_Panel"
        DefaultButton="btnExpedir" Style="display: none;">
        <div class="Modal_Head" style="width: 100%;">
            <div id="div4" style="float: left;">
                Registrar Certificado
            </div>
            <div id="div5" style="float: right;">
                <img alt="Click para cerrar." class="Modal_CloseButton" onclick="hideModalPopup('bmpeCotCertificado');"
                    style="cursor: hand;" />
            </div>
        </div>
        <div id="div6" class="Modal_Body">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td class="Form_tdBorde">
                    </td>
                    <td>
                        <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                            <tr>
                                <td class="Form_SubTitulo" colspan="6">
                                    IMFORMACI�N DE PAGO
                                </td>
                            </tr>
                            <tr>
                                <td class="Form_TextoDer" style="width: 110px;">
                                    Plan: <span class="Form_TextoObligatorio">*</span>
                                </td>
                                <td class="Form_TextoIzq">
                                    <asp:DropDownList ID="ddlPlan" runat="server" Width="150px" OnSelectedIndexChanged="ddlPlan_SelectedIndexChanged"
                                        AutoPostBack="true">
                                    </asp:DropDownList>                                                                                                                                                   
                                    <asp:RequiredFieldValidator ID="rfvPlan" runat="server" Text="*" ControlToValidate="ddlPlan"
                                        Display="None" ErrorMessage="Seleccione plan." ForeColor="Transparent" SetFocusOnError="true"
                                        InitialValue="-1" ValidationGroup="CotizacionCertificado" >
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td class="Form_TextoDer">
                                    Periodo de Pago: <span class="Form_TextoObligatorio">*</span>
                                </td>
                                <td class="Form_TextoIzq">
                                    <asp:DropDownList ID="ddlFrecuencia" runat="server" Width="150px" 
                                        onselectedindexchanged="ddlFrecuencia_SelectedIndexChanged" AutoPostBack="true" >
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvFrecuencia" runat="server" Text="*" ControlToValidate="ddlFrecuencia"
                                        Display="None" ErrorMessage="Seleccione frecuencia." ForeColor="Transparent"
                                        InitialValue="-1" SetFocusOnError="true" ValidationGroup="CotizacionCertificado">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td class="Form_TextoDer">
                                    Moneda Prima: <span class="Form_TextoObligatorio">*</span>
                                </td>
                                <td class="Form_TextoIzq">
                                    <asp:DropDownList ID="ddlMonPrima" runat="server" Width="150px">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvMonPrima" runat="server" Text="*" ControlToValidate="ddlMonPrima"
                                        Display="None" ErrorMessage="Seleccione moneda prima." ForeColor="Transparent"
                                        SetFocusOnError="true" InitialValue="-1" ValidationGroup="CotizacionCertificado">
                                    </asp:RequiredFieldValidator>
                                </td>
                                                        
                            </tr>
                            <tr>
                                <td class="Form_TextoDer">
                                    Valor de Cuota: <span class="Form_TextoObligatorio">*</span>
                                </td>
                                <td class="Form_TextoIzq">
                                    <asp:UpdatePanel ID="upCotPrimCertificado" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtPrimTotal" runat="server" CssClass="Form_TextBoxMontoDisable" Width="100px"
                                                ReadOnly="true" ></asp:TextBox>                        
                                            <cc1:FilteredTextBoxExtender ID="ftxtPrimTotal" runat="server" TargetControlID="txtPrimTotal"
                                                ValidChars="1234567890,.">
                                            </cc1:FilteredTextBoxExtender>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlPlan" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="ddlFrecuencia" EventName="SelectedIndexChanged" />                                            
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:RequiredFieldValidator id="rfvPrimTotal" runat="server" Text="*" ControlToValidate="txtPrimTotal"
                                        Display="None" ErrorMessage="No existe monto de prima." ForeColor="Transparent" 
                                        SetFocusOnError ="true" ValidationGroup="CotizacionCertificado"></asp:RequiredFieldValidator>                                                                                    
                                </td>            
                                <td></td>
                                <td>
                                    <asp:CheckBox ID="cbxSolCuenta" runat="server" Text="Solicitar nro. de cuenta:" onclick="SolicitarCuenta();" /> 
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>                                          
                                <td class="Form_TextoDer" >
                                    Medio de Pago: <span class="Form_TextoObligatorio">*</span>
                                </td>
                                <td class="Form_TextoIzq">
                                    <asp:DropDownList ID="ddlMedPago" runat="server" Width="120px" Enabled="false" >
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvMedPago" runat="server" Text="*" ControlToValidate="ddlMedPago"
                                        Display="None" ErrorMessage="Seleccione medio de pago." ForeColor="Transparent" 
                                        SetFocusOnError="true" InitialValue="-1" ValidationGroup="CotizacionCertificadoOpc" Enabled="false" >
                                    </asp:RequiredFieldValidator>                                    
                                </td>
                                <td class="Form_TextoDer">
                                    Nro. Cuenta: <span class="Form_TextoObligatorio">*</span>
                                </td>
                                <td class="Form_TextoIzq">
                                    <asp:TextBox ID="txtNroCuenta" runat="server" CssClass="Form_TextBox" Width="116px" Enabled="false" ></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvNroCuenta" runat="server" ControlToValidate="txtNroCuenta"
                                        SetFocusOnError="true" Display="None" ErrorMessage="Nro. cuenta obligatorio."
                                        Text="*" ValidationGroup="CotizacionCertificadoOpc" >
                                    </asp:RequiredFieldValidator>
                                    <cc1:FilteredTextBoxExtender id="fteMonCuenta" runat="server" TargetControlID="txtNroCuenta"
                                        ValidChars="0123456789" ></cc1:FilteredTextBoxExtender>
                                </td>
                                <td class="Form_TextoDer">
                                    Moneda Cuenta: <span class="Form_TextoObligatorio">*</span>
                                </td>
                                <td class="Form_TextoIzq">
                                    <asp:DropDownList ID="ddlMonCuenta" runat="server" Width="120px" Enabled="false" >
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvMonCuenta" runat="server" Text="*" ControlToValidate="ddlMonCuenta"
                                        Display="None" ErrorMessage="Seleccione moneda cuenta." ForeColor="Transparent"
                                        SetFocusOnError="true" InitialValue="-1" ValidationGroup="CotizacionCertificadoOpc" >
                                    </asp:RequiredFieldValidator>                                    
                                </td>                                
                            </tr>
                            <tr>
                                <td class="Form_TextoDer">
                                    Fecha Vencimiento:
                                </td>
                                <td class="Form_TextoIzq">
                                    <asp:TextBox ID="txtFecVencimiento" runat="server" CssClass="Form_TextBox" Width="60px" Enabled ="false" >
                                    </asp:TextBox> 
                                    <cc1:MaskedEditExtender ID="meeFecVencimiento" runat="server" Mask="99/99/999" MaskType="Date"
                                        TargetControlID="txtFecVencimiento"  >
                                    </cc1:MaskedEditExtender>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                        <br />
                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                            <tr>
                                <td>
                                    <asp:UpdatePanel ID="upExpedir" runat="server" >
                                        <ContentTemplate>
                                            <asp:Button ID="btnExpedir" runat="server" Text="Expedir" 
                                                ValidationGroup="CotizacionCertificado" onclick="btnExpedir_Click"/>
                                            <br />
                                            <br /> 
                                            <asp:Label ID="lblMsjExpedir" runat="server" ForeColor="Red"></asp:Label>                                                        
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="btnExpedir" EventName="Click"   />
                                        </Triggers>
                                    </asp:UpdatePanel>                                                                                                                                    
                                </td>
                            </tr>
                        </table>
                        <br />
                        <uc1:MsgBox ID="msgCertificado" runat="server" />
                        <asp:ValidationSummary ID="vsCertificado" runat="server" ShowMessageBox="false" ShowSummary="true"
                            CssClass="Form_ValSumary" ValidationGroup="CotizacionCertificado"  />      
                        <asp:ValidationSummary ID="vsCertificadoOpc" runat="server" ShowMessageBox="false" ShowSummary="true"
                            CssClass="Form_ValSumary" ValidationGroup="CotizacionCertificadoOpc" />                                                            
                    </td>
                    <td class="Form_tdBorde">
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:Label ID="lblCotCertificado" runat="server" Style="display: none;"></asp:Label>
    <cc1:ModalPopupExtender ID="mpeCotCertificado" runat="server" Enabled="true" TargetControlID="lblCotCertificado"
        BehaviorID="bmpeCotCertificado" PopupControlID="pnCertificado" BackgroundCssClass="Modal_Background">
    </cc1:ModalPopupExtender>
    <!--Popup Documento Certificado-->
    <asp:Panel ID="pnDocCertificado" runat="server" Width="700px" CssClass="Modal_Panel"
            style="display:none;" DefaultButton="btnEnvCotMail">
        <div class="Modal_Head" style="width: 100%;">
            <div id="div7" style="float: left;">
                Imprimir Certificado
            </div>
            <div id="div8" style="float: right;">
                <img alt="Click para cerrar." class="Modal_CloseButton" onclick="hideModalPopup('bmpCotDocCertificado');"
                    style="cursor: hand;" />
            </div>
        </div>
        <div id="div9" class="Modal_Body" style="height:580px;">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height: ">
                <tr>
                    <td class="Form_tdBorde">
                    </td>
                    <td>
                        <rsweb:ReportViewer ID="rvCertificado" runat="server" Width="100%" Height="550px"
                            Font-Names="Verdana" Font-Size="8pt">
                            <LocalReport ReportPath="rdlc\rdlcCertificadoSegVeh.rdlc">
                                <DataSources>
                                    <rsweb:ReportDataSource DataSourceId="odsCertificado" Name="BECertificado" />
                                </DataSources>
                            </LocalReport>
                        </rsweb:ReportViewer>
                        <asp:ObjectDataSource ID="odsCertificado" runat="server" SelectMethod="Obtener"
                            TypeName="AONAffinity.Business.Logic.BLReporte">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="5300-1000" Name="pcIdCertificado" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </td>
                    <td class="Form_tdBorde">
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:Label ID="lblCotDocCertificado" runat="server" Style="display: none;"></asp:Label>
    <cc1:ModalPopupExtender ID="mpeCotDocCertificado" runat="server" Enabled="true" TargetControlID="lblCotDocCertificado"
        BehaviorID="bmpCotDocCertificado" PopupControlID="pnDocCertificado" BackgroundCssClass="Modal_Background">
    </cc1:ModalPopupExtender>
</asp:Content> 