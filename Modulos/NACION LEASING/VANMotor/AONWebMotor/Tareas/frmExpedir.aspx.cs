﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessLogic;
using AONAffinity.Library.Resources;
using AONAffinity.Library.BusinessEntity.BDMotor;   

namespace AONWebMotor.Tareas
{
    public partial class frmExpedir : PageBase
    {
        #region Variables
        private static AONAffinity.Business.Entity.BDJanus.BEUsuario objBEUsuario = new AONAffinity.Business.Entity.BDJanus.BEUsuario();
        private static BECotizacion objBECotizacion = new BECotizacion();
        private static String cIdInformador;
        private static Int32 nIdOficina;
        private static String cIdMoneda = "USD";
        #endregion

        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    this.CargarInfoUsuario();
                    this.CargarInformadorOficina();
                    this.CargarCotizacion();
                    this.CargarFuncionesJS();
                    this.CargarMotivoRechazo();                    
                    this.CargarPlan();
                    this.CargarFrecuencia();
                    this.CargarMonedaPrima();
                    this.CargarMedioPago();                    
                }
            }
            catch (Exception ex) 
            {
                this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
            }            
        }

        protected void btnCambiarValVeh_Click(object sender, EventArgs e)
        {
            try
            {
                BECotizacion objBECotizacion = new BECotizacion();

                objBECotizacion.IdPeriodo = 1;                  //O_O
                objBECotizacion.NroCotizacion = 100;            //O_O
                objBECotizacion.CamPorcentaje = Convert.ToDecimal(this.ddlSigno.SelectedValue + this.ddlProcentaje.SelectedValue);
                objBECotizacion.UsuarioModificacion = Session[NombreSession.Usuario].ToString();

                BLCotizacion objBLCotizacion = new BLCotizacion();

                if (objBLCotizacion.ActualizarValVeh(objBECotizacion) > 0)
                {
                    this.rvVenta.LocalReport.Refresh();
                    this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Success, "Se modificó el valor del vehículo.", ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
                }
                else
                {
                    throw new Exception("Error al cambiar el valor del vehículo.");
                }
            }
            catch (Exception ex)
            {
                this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
            }
        }

        protected void btnValorOri_Click(object sender, EventArgs e)
        {
            try
            {
                BECotizacion objBECotizacion = new BECotizacion();

                objBECotizacion.IdPeriodo = 1;
                objBECotizacion.NroCotizacion = 100;                
                objBECotizacion.UsuarioModificacion = Session[NombreSession.Usuario].ToString();

                BLCotizacion objBLCotizacion = new BLCotizacion();

                if (objBLCotizacion.RevertirValVeh(objBECotizacion) > 0)
                {
                    this.rvVenta.LocalReport.Refresh();
                    this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Success, "Se revirtio el valor del vehículo.", ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
                }
                else 
                {
                    throw new Exception("Error al revertrir el valor del vehiculo.");
                }
            }
            catch (Exception ex) 
            {
                this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
            }
        }
        #endregion
           
        #region Metodos Privados
        private void CargarCotizacion() 
        {
            BLCotizacion objBLCotizacion = new BLCotizacion();
            objBECotizacion = objBLCotizacion.Obtener(1, 100); //O_O 

            if (objBECotizacion == null) 
            {
                throw new Exception("No existe cotizacion a procesar.");
            }
        }

        private void CargarFuncionesJS() 
        {
            this.btnCambiarValVeh.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de cambiar el valor del vehículo?')== false) return false;");
            this.btnValorOri.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de revertir el valor del vehículo?')== false) return false;");
            //Popup Mail Cotizacion
            this.btnEnvCotMail.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de enviar la cotización por email?')== false) return false;");
            //Popup Rechazo Cotizacion
            this.btnRegCotRechazo.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de registrar el rechazo de la cotización?')== false) return false;");
            //Popup Certificado Cotización
            this.btnExpedir.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de expedir el certificado?')== false) return false;");
        }

        private void CargarMotivoRechazo() 
        {
            BLTipoRechazoCotizacion objBLTipoRechazoCotizacion = new BLTipoRechazoCotizacion();
            this.CargarDropDownList(this.ddlTipMotRechazo, "IdTipRechazo", "Descripcion", objBLTipoRechazoCotizacion.Listar(true), true);   
        }

        private void CargarInfoUsuario() 
        {
            AONAffinity.Motor.BusinessLogic.BDJanus.BLUsuario objBLUsuario = new AONAffinity.Motor.BusinessLogic.BDJanus.BLUsuario();
            objBEUsuario = objBLUsuario.Obtener(Session[NombreSession.Usuario].ToString());  
        }

        private void CargarInformadorOficina() 
        {
            if (objBEUsuario.IdInformador == NullTypes.CadenaNull) 
            {
                throw new Exception("El usuario no tiene asignado un código de informador.");
            }

            AONAffinity.Motor.BusinessLogic.BDJanus.BLInformador objBLInformador = new AONAffinity.Motor.BusinessLogic.BDJanus.BLInformador();
            AONAffinity.Business.Entity.BDJanus.BEInformador objBEInformador = objBLInformador.Obtener(objBEUsuario.IdInformador);

            if (objBEInformador == null)
            {
                throw new Exception("El usuario no tiene asignado un código de informador.");
            }

            if (objBEInformador.IdOficina == NullTypes.IntegerNull) 
            {
                throw new Exception("No existe una oficina asignada al informador.");
            }

            cIdInformador = objBEInformador.IdInformador;
            nIdOficina = objBEInformador.IdOficina;
        }

        private void CargarPlan() 
        {
            AONAffinity.Motor.BusinessLogic.BDJanus.BLOpcion objBLOpcion = new AONAffinity.Motor.BusinessLogic.BDJanus.BLOpcion();
            this.CargarDropDownList(this.ddlPlan, "OPCION", "OPCION", objBLOpcion.Listar(AppSettings.CodigoProducto), true);

            //Si cotización solo tiene tasa anual, solo se muestra plan anual.
            if (objBECotizacion.TasaAnual != NullTypes.DecimalNull && objBECotizacion.TasaBianual == NullTypes.DecimalNull) 
            {
                this.ddlPlan.SelectedValue = ValorConstante.PlanAnual;
                this.ddlPlan.Enabled = false;
            }

            //Si cotización solo tiene tasa bianual, solo se muestra plan bianual.
            if (objBECotizacion.TasaAnual == NullTypes.DecimalNull && objBECotizacion.TasaBianual != NullTypes.DecimalNull) 
            {
                this.ddlPlan.SelectedValue = ValorConstante.PlanBianual;
                this.ddlPlan.Enabled = false;
            }
        }

        private void CargarFrecuencia()
        {
            AONAffinity.Motor.BusinessLogic.BDJanus.BLFrecuencia objBLFrecuencia = new AONAffinity.Motor.BusinessLogic.BDJanus.BLFrecuencia();
            this.CargarDropDownList(this.ddlFrecuencia, "IdFrecuencia", "Nombre", objBLFrecuencia.Listar(AppSettings.CodigoProducto), true);
        }

        private void CargarMonedaPrima()
        {
            AONAffinity.Motor.BusinessLogic.BDJanus.BLMoneda objBLMoneda = new AONAffinity.Motor.BusinessLogic.BDJanus.BLMoneda();
            List<AONAffinity.Library.BusinessEntity.BDJanus.BEMoneda> lstBEMoneda = objBLMoneda.Listar(AppSettings.CodigoProducto);                       

            if (lstBEMoneda != null) 
            {
                if (lstBEMoneda.Count > 1)
                {
                    this.CargarDropDownList(this.ddlMonPrima, "IdMoneda", "nombre", lstBEMoneda, true);
                }
                else 
                {
                    this.CargarDropDownList(this.ddlMonPrima, "IdMoneda", "nombre", lstBEMoneda, false);
                }
            }
        }

        public void CargarMedioPago()
        {
            
        }
        #endregion        

        #region Popup Cotizacion Rechazo
        protected void btnRegCotRechazo_Click(object sender, EventArgs e)
        {
            try 
            {
                this.lblResCotRechazo.Text = String.Empty;

                BECotizacion objBECotizacion = new BECotizacion();
                objBECotizacion.IdPeriodo = 1;
                objBECotizacion.NroCotizacion = 100;
                objBECotizacion.IdTipRechazo = Convert.ToInt32(this.ddlTipMotRechazo.SelectedValue);
                objBECotizacion.ObsTipRechazo = this.txtMotRechazo.Text.Trim().ToUpper();
                objBECotizacion.UsrRegRechazo = Session[NombreSession.Usuario].ToString();

                BLCotizacion objBLCotizacion = new BLCotizacion();

                if (objBLCotizacion.ActualizarRechazo(objBECotizacion) > 0)
                {
                    this.lblResCotRechazo.Text = "Se registró el motivo de rechazo de la cotización.";
                }
                else
                {
                    throw new Exception("Error al registrar el rechazo de la cotización.");
                }
            }
            catch (Exception ex) 
            {
                this.lblResCotRechazo.Text = ex.Message.ToString();
            }
        }
        #endregion        

        #region Popup Cotizacion Mail
        protected void btnEnvCotMail_Click(object sender, EventArgs e)
        {
            try 
            {
                this.lblResCotMail.Text = String.Empty;

                BLGeneraArchivo objBLGeneraArchivo = new BLGeneraArchivo();
                BLCotizacion objBLCotizacion = new BLCotizacion();
                BECotizacion objBECotizacion = objBLCotizacion.Obtener(1, 100);
                String cArchivo = objBLGeneraArchivo.CotizacionPDF(objBECotizacion, objBEUsuario);
                String cMail = "Sr.(a): " + objBECotizacion.PriNombre + " " + objBECotizacion.SegNombre + " " + objBECotizacion.ApePaterno + " " + objBECotizacion.ApeMaterno + " <br /> ";
                cMail = cMail + "Le adjuntamos la cotizacion de seguro vehicular  realizada el día: " + DateTime.Now.ToShortDateString();
                if (cArchivo != null) 
                {
                    BLMail objBLMail = new BLMail();
                    objBLMail.Enviar(this.txtEmail.Text.Trim(), this.txtEmailCc.Text.Trim(), "COTIZACION VEHICULAR", cMail, cArchivo);
                    this.lblResCotMail.Text = "- La cotización fué enviada al cliente.";
                }                
            }
            catch (Exception ex) 
            {
                this.lblResCotMail.Text = ex.Message.ToString();
            }
        }
        #endregion        

        #region Popup Cotizacion Certificado
        protected void ddlPlan_SelectedIndexChanged(object sender, EventArgs e) 
        {
            this.ddlFrecuencia_SelectedIndexChanged(sender, e); 
        }

        protected void ddlFrecuencia_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlFrecuencia.SelectedValue != "-1") 
            {
                if (this.ddlPlan.SelectedValue == ValorConstante.PlanAnual)
                {
                    if (objBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.DESCUENTO) || objBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.RECARGO))
                    {
                        if (this.ddlFrecuencia.SelectedValue == Convert.ToInt32(ValorConstante.TipoFrecuencia.PAGO_MENSUAL).ToString())
                        {
                            this.txtPrimTotal.Text = (objBECotizacion.CalcPrimaAnualC == NullTypes.DecimalNull ? String.Empty : Decimal.Round((objBECotizacion.CalcPrimaAnualC / 12), 2).ToString());
                        }
                        else
                        {
                            this.txtPrimTotal.Text = (objBECotizacion.CalcPrimaAnualC == NullTypes.DecimalNull ? String.Empty : Decimal.Round(objBECotizacion.CalcPrimaAnualC, 2).ToString());
                        }

                    }
                    else
                    {
                        this.txtPrimTotal.Text = (objBECotizacion.CalcPrimaAnual == NullTypes.DecimalNull ? String.Empty : Decimal.Round((objBECotizacion.CalcPrimaAnual / 12), 2).ToString());
                    }
                }
                if (this.ddlPlan.SelectedValue == ValorConstante.PlanBianual)
                {
                    if (objBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.DESCUENTO) || objBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.RECARGO))
                    {
                        if (this.ddlFrecuencia.SelectedValue == Convert.ToInt32(ValorConstante.TipoFrecuencia.PAGO_MENSUAL).ToString())
                        {
                            this.txtPrimTotal.Text = (objBECotizacion.CalcPrimaBianualC == NullTypes.DecimalNull ? String.Empty : Decimal.Round((objBECotizacion.CalcPrimaBianualC / 24), 2).ToString());
                        }
                        else
                        {
                            this.txtPrimTotal.Text = (objBECotizacion.CalcPrimaBianualC == NullTypes.DecimalNull ? String.Empty : Decimal.Round(objBECotizacion.CalcPrimaBianualC, 2).ToString());
                        }
                    }
                    else
                    {
                        this.txtPrimTotal.Text = (objBECotizacion.CalcPrimaBianual == NullTypes.DecimalNull ? String.Empty : Decimal.Round((objBECotizacion.CalcPrimaBianual / 24), 2).ToString());
                    }
                }
            }            
        }

        protected void btnExpedir_Click(object sender, EventArgs e)
        {
            try
            {
                this.lblMsjExpedir.Text = String.Empty;

                if (this.cbxSolCuenta.Checked)
                {
                    this.ValidaExpedirOpcional();
                }

                AONAffinity.Business.Entity.BDJanus.BECertificado objBECertificado = new AONAffinity.Business.Entity.BDJanus.BECertificado();
                objBECertificado = this.CargarCertificado();

                List<AONAffinity.Business.Entity.BDJanus.BEAsegurado> lstObjBEAsegurado = new List<AONAffinity.Business.Entity.BDJanus.BEAsegurado>();
                lstObjBEAsegurado = this.CargarAsegurado(); 

                AONAffinity.Motor.BusinessLogic.BDJanus.BLCertificado objBLCertificado = new AONAffinity.Motor.BusinessLogic.BDJanus.BLCertificado();
                List<AONAffinity.Business.Entity.BDJanus.BEExpedir> lstBEExpedir = objBLCertificado.BLExpedirCertificado(objBECertificado, lstObjBEAsegurado, null, null, null, "");

                if (lstBEExpedir[0].Codigo == 1)
                {                    
                    this.hfIdCertificado.Value = lstBEExpedir[0].Descripcion;
                    this.btnExpedir.Enabled = false;
                    this.btnImprimir.Enabled = true;
                    this.mpeCotCertificado.Hide();
                    this.rvCertificado.LocalReport.Refresh();
                    this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Success, "Se expeidio el certificado con exito.", ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho); 
                }
                else 
                {
                    this.msgCertificado.Show(AONWebMotor.controles.MsgBox.MessageType.Error, "Se produjo un error al expedir el certificado. <br/> " + lstBEExpedir[0].Descripcion, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho); 
                }                
            }
            catch (Exception ex)
            {
                this.msgCertificado.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho); 
            }
        }

        #region Métodos Privados
        private void ValidaExpedirOpcional()
        {
            Boolean bError = false;
            String cMensaje = String.Empty;
            this.lblMsjExpedir.Text = String.Empty;

            if (this.ddlMedPago.SelectedValue == "-1")
            {
                bError = true;
                cMensaje = "- Seleccione el medio de pago." + " <br /> ";
            }
            if (this.txtNroCuenta.Text == String.Empty)
            {
                bError = true;
                cMensaje = cMensaje + "- Ingrese el nro de cuenta." + " <br /> ";
            }
            if (this.ddlMonCuenta.SelectedValue == "-1")
            {
                bError = true;
                cMensaje = cMensaje + "- Seleccione la moneda de la cuenta." + " <br /> ";
            }

            if (bError == true)
            {
                throw new Exception(cMensaje);
            }
        }
        #endregion

        #region Funciones Privadas
        private AONAffinity.Business.Entity.BDJanus.BECertificado CargarCertificado() 
        {
            AONAffinity.Business.Entity.BDJanus.BECertificado objBECertificado = new AONAffinity.Business.Entity.BDJanus.BECertificado();

            if (objBECotizacion != null) 
            {
                //objBECertificado.IdCertificado = "5300-100";            //O_O
                objBECertificado.IdProducto = AppSettings.CodigoProducto;
                //objBECertificado.NumCertificado = 100;                  //O_O
                objBECertificado.Opcion = this.ddlPlan.SelectedValue;
                objBECertificado.IdFrecuencia = Convert.ToInt32(this.ddlFrecuencia.SelectedValue);
                objBECertificado.IdOficina = nIdOficina;
                objBECertificado.IdInformador = cIdInformador;
                objBECertificado.IdMedioPago = (this.cbxSolCuenta.Checked ? this.ddlMedPago.SelectedValue : "NA");
                objBECertificado.IdEstadoCertificado = 1;
                objBECertificado.MontoAsegurado = objBECotizacion.ValVehiculo;
                objBECertificado.PrimaBruta = this.ObtenerPrimaTotal();
                objBECertificado.Iva = 0;
                objBECertificado.PrimaTotal = this.ObtenerPrimaTotal();
                objBECertificado.PrimaCobrar = this.ObtenerPrimaCobrar();
                objBECertificado.NumeroCuenta = (this.cbxSolCuenta.Checked ? this.txtNroCuenta.Text.Trim() : "000000000000"); 
                objBECertificado.Vencimiento = (this.cbxSolCuenta.Checked ? Convert.ToDateTime(this.txtFecVencimiento.Text.Trim()) : NullTypes.FechaNull);
                objBECertificado.Vigencia = DateTime.Now;
                objBECertificado.Puntos = NullTypes.IntegerNull;
                objBECertificado.IdMotivoAnulacion = NullTypes.IntegerNull;
                objBECertificado.Consistente = false;
                objBECertificado.PuntosAnual = NullTypes.IntegerNull;
                objBECertificado.IdCiudad = objBECotizacion.IdCiudad;
                objBECertificado.Direccion = objBECotizacion.Direccion;
                objBECertificado.Telefono = objBECotizacion.TelDomicilio1;
                objBECertificado.Nombre1 = objBECotizacion.PriNombre;
                objBECertificado.Nombre2 = objBECotizacion.SegNombre;
                objBECertificado.Apellido1 = objBECotizacion.ApePaterno;
                objBECertificado.Apellido2 = objBECotizacion.ApeMaterno;
                objBECertificado.CcCliente = objBECotizacion.NroDocumento;
                objBECertificado.IdTipoDocumento = objBECotizacion.IdTipoDocumento;
                objBECertificado.Digitacion = DateTime.Now;
                objBECertificado.Incentivo = NullTypes.DecimalNull;
                objBECertificado.SaldoIncentivo = NullTypes.DecimalNull;
                objBECertificado.UsuarioCreacion = Session[NombreSession.Usuario].ToString();
                objBECertificado.Venta = DateTime.Now;
                objBECertificado.FinVigencia = this.ObtenerFinVigencia();
                objBECertificado.IdMonedaPrima = this.ddlMonPrima.SelectedValue;
                objBECertificado.IdMonedaCobro = cIdMoneda;
                objBECertificado.AfiliadoHasta = NullTypes.FechaNull;                
            }

            return objBECertificado;
        }

        private List<AONAffinity.Business.Entity.BDJanus.BEAsegurado> CargarAsegurado()
        {
            List<AONAffinity.Business.Entity.BDJanus.BEAsegurado> lstBEAsegurado = new List<AONAffinity.Business.Entity.BDJanus.BEAsegurado>();

            if (objBECotizacion != null)
            {
                AONAffinity.Business.Entity.BDJanus.BEAsegurado objBEAsegurado = new AONAffinity.Business.Entity.BDJanus.BEAsegurado();

                //objBEAsegurado.IdCertificado = "5300-100";
                objBEAsegurado.Consecutivo = 1;
                objBEAsegurado.IdTipodocumento = objBECotizacion.IdTipoDocumento;
                objBEAsegurado.Ccaseg = objBECotizacion.NroDocumento;
                objBEAsegurado.Nombre1 = objBECotizacion.PriNombre;
                objBEAsegurado.Nombre2 = objBECotizacion.SegNombre;
                objBEAsegurado.Apellido1 = objBECotizacion.ApePaterno;
                objBEAsegurado.Apellido2 = objBECotizacion.ApeMaterno;
                objBEAsegurado.Direccion = objBECotizacion.Direccion;
                objBEAsegurado.Telefono = objBECotizacion.TelDomicilio1;
                objBEAsegurado.IdCiudad = objBECotizacion.IdCiudad;
                objBEAsegurado.FechaNacimiento = objBECotizacion.FecNacimiento;
                objBEAsegurado.IdParentesco = 2; //MISMO CONTRATANTE O_O
                objBEAsegurado.Activo = true;

                lstBEAsegurado.Add(objBEAsegurado); 
            }

            return lstBEAsegurado;
        }

        public Decimal ObtenerPrimaTotal() {
            Decimal nPrimaTot = 0;

            if (this.ddlPlan.SelectedValue == ValorConstante.PlanAnual) 
            {
                if (objBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.DESCUENTO) || objBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.RECARGO))
                {
                    nPrimaTot = Decimal.Round(objBECotizacion.CalcPrimaAnualC, 2);
                }
                else
                {
                    nPrimaTot = Decimal.Round(objBECotizacion.CalcPrimaAnual, 2);
                }
            }

            if (this.ddlPlan.SelectedValue == ValorConstante.PlanBianual) 
            {
                if (objBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.DESCUENTO) || objBECotizacion.Condicion == Convert.ToInt32(ValorConstante.CondicionCotizacion.RECARGO))
                {
                    nPrimaTot = Decimal.Round(objBECotizacion.CalcPrimaBianualC, 2);
                }
                else
                {
                    nPrimaTot = Decimal.Round(objBECotizacion.CalcPrimaBianual, 2);
                }
            }                

            return nPrimaTot;
        }

        public Decimal ObtenerPrimaCobrar() 
        {
            Decimal nPrimaTot = 0;

            //Si selecciono pago unico, caso contratio pago mensual.
            if (this.ddlFrecuencia.SelectedValue == (ValorConstante.TipoFrecuencia.PAGO_UNICO).ToString()) 
            {
                nPrimaTot = this.ObtenerPrimaTotal();
            }
            else
            {
                //Si plan es anual, la prima a cobrar es la prima total / 12,
                //caso plan es bianual, la prima a cobrar es la total / 24.
                if (this.ddlPlan.SelectedValue == ValorConstante.PlanAnual)
                {
                    nPrimaTot = this.ObtenerPrimaTotal() / 12;
                }
                else 
                {
                    nPrimaTot = this.ObtenerPrimaTotal() / 24;
                }
            }

            return nPrimaTot;
        }

        public DateTime ObtenerFinVigencia() 
        {
            DateTime dFinVigencia = DateTime.Now;

            if (this.ddlPlan.SelectedValue == ValorConstante.PlanAnual)
            {
                dFinVigencia = dFinVigencia.AddYears(1);
                dFinVigencia = dFinVigencia.AddDays(-1); 
            }
            else 
            {
                dFinVigencia = dFinVigencia.AddYears(2);
                dFinVigencia = dFinVigencia.AddDays(-1);
            }

            return dFinVigencia;
        }
        #endregion

        
        #endregion
    }
}