﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AONWebMotor.Tareas
{
    public partial class frmPendiente : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) 
            {
                this.load();
            }
        }

        public void load() 
        {
            List<Cliente> lstCliente = new List<Cliente>();
            Cliente objCliente = new Cliente();
            objCliente.Nombre1 = "SILVANA";
            objCliente.Nombre2 = "PAOLA";
            objCliente.Apellido1 = "RODRIGUEZ";
            objCliente.Apellido2 = "ORTEGA";
            objCliente.DNI = "44246708"; 
            objCliente.Telefono = "2563256";
            objCliente.Direccion = "AV. MIGUEL GRAU 123 - BARRANCO - LIMA";
            objCliente.Marca = "TOYOTA";
            objCliente.Modelo = "COROLLA"; 
            lstCliente.Add(objCliente);

            Cliente objCliente1 = new Cliente();
            objCliente1.Nombre1 = "JOLSE";
            objCliente1.Nombre2 = "JHONATAN";
            objCliente1.Apellido1 = "LOPEZ";
            objCliente1.Apellido2 = "BARRENECHEA";
            objCliente1.DNI = "44653256"; 
            objCliente1.Telefono = "2356563";
            objCliente1.Direccion = "AV. JAVIER PRADO ESTE 1256 - SAN LUIS - LIMA";
            objCliente1.Marca = "VOLVO";
            objCliente1.Modelo = "S40"; 
             
            lstCliente.Add(objCliente1);

            Cliente objCliente2 = new Cliente();
            objCliente2.Nombre1 = "PAMELA";
            objCliente2.Nombre2 = "";
            objCliente2.Apellido1 = "AGUILAR";
            objCliente2.Apellido2 = "CANESSA";
            objCliente2.DNI = "44512365"; 
            objCliente2.Telefono = "2528929";
            objCliente2.Direccion = "AV. CAMINOS DEL INCA - SURCO - LIMA";
            objCliente2.Marca = "CHEVROLET";
            objCliente2.Modelo = "CRUZE"; 
            lstCliente.Add(objCliente2);

            Cliente objCliente3 = new Cliente();
            objCliente3.Nombre1 = "LUIS";
            objCliente3.Nombre2 = "ANGEL";
            objCliente3.Apellido1 = "GUZMAN";
            objCliente3.Apellido2 = "VILLA";
            objCliente3.DNI = "77455561"; 
            objCliente3.Telefono = "2456892";
            objCliente3.Direccion = "AV. ANGAMOS - SURQUILLO - LIMA";
            objCliente3.Marca = "TOYOTA";
            objCliente3.Modelo = "YARIS"; 
            lstCliente.Add(objCliente3);

            this.GridView1.DataSource = lstCliente;
            this.GridView1.DataBind();

  
        }
    }

    public class Cliente 
    {
        private String nombre1;
        private String nombre2;
        private String apellido1;
        private String apellido2;
        private String dni;
        private String telefono;
        private String direccion;
        private String marca;
        private String modelo;

        public String Nombre1
        {
            get { return nombre1; }
            set { nombre1 = value; }
        }

        public String Nombre2
        {
            get { return nombre2; }
            set { nombre2 = value; }
        }

        public String Apellido1
        {
            get { return apellido1; }
            set { apellido1 = value; }
        }
        public String Apellido2
        {
            get { return apellido2; }
            set { apellido2 = value; }
        }
        public String DNI 
        {
            get { return dni; }
            set { dni = value; }
        }

        public String Telefono
        {
            get { return telefono; }
            set { telefono = value; }
        }

        public String Direccion 
        {
            get { return direccion; }
            set { direccion = value; }
        }

        public String Marca 
        {
            get { return marca; }
            set { marca = value; }
        }

        public String Modelo
        {
            get { return modelo; }
            set { modelo = value; }
        }
    }
}
