﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true"
    CodeBehind="frmPendiente.aspx.cs" Inherits="AONWebMotor.Tareas.frmPendiente" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="Form_SubTitulo" colspan="6">
                            CLIENTE A CONTACTAR
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Cliente:
                        </td>
                        <td class="Form_TextoIzq" colspan="3">
                            <asp:TextBox ID="txtNom1" runat="server" CssClass="Form_TextBoxDisable" Width="400px"
                                Text="SILVANA PAOLA RODRIGUEZ ORTEGA"  >
                            </asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            Nro. Documento:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtDNI" runat="server" CssClass="Form_TextBoxDisable"
                                Text="44264581" >
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Teléfono Fijo:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtTelFijo" runat="server" CssClass="Form_TextBoxDisable" Width="120px"
                             Text="2528929" ></asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            Teléfono Movil:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtTelMovil" runat="server" CssClass="Form_TextBoxDisable" Width="120px"
                                Text="993648132"></asp:TextBox>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Email:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="TextBox1" runat="server" CssClass="Form_TextBoxDisable" Width="200px"
                                    Text="silrod@gmail.com">                            
                            </asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            Dirección:
                        </td>
                        <td class="Form_TextoIzq" colspan="2">
                            <asp:TextBox ID="TextBox2" runat="server" CssClass="Form_TextBoxDisable" Width="200px"
                                Text="AV. CAMINOS DEL INCA 612" >
                            </asp:TextBox>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Departamento:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtDep" runat="server" Width="120px" CssClass="Form_TextBoxDisable"
                                Text="LIMA" ></asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            Provincia:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="TextBox3" runat="server" Width="120px" CssClass="Form_TextBoxDisable"
                                Text="LIMA"></asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            Distrito:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="TextBox4" runat="server" Width="120px" CssClass="Form_TextBoxDisable"
                                Text="SANTIAGO DE SURCO"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <br />
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="Form_SubTitulo" colspan="6">
                            DATOS VEHÍCULO
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Nro Placa:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="TextBox5" runat="server" Width="120px" CssClass="Form_TextBoxDisable"
                                Text="OPG-45"></asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            Marca:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="TextBox6" runat="server" Width="120px" CssClass="Form_TextBoxDisable"
                                Text="AUDI"></asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            Modelo:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="TextBox7" runat="server" Width="120px" CssClass="Form_TextBoxDisable"
                                Text="A4 1.8  TFSI"></asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            Año:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="TextBox8" runat="server" Width="60px" CssClass="Form_TextBoxDisable"
                                Text="2009"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <br />
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="Form_SubTitulo" colspan="8">
                            DATOS DE DESCUENTO
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8" style="text-align:center;" >
                            <asp:Label id="lblMsj" runat="server" Text="CLIENTE TIENE DESCUENTO...!" 
                                Font-Bold="True" Font-Size="Small" ForeColor="#CC3300"   ></asp:Label>
                        </td> 
                    </tr>                    
                    <tr>                       
                        <td class="Form_TextoDer" >
                        </td>
                        <td class="Form_TextoIzq" >
                        </td>
                        <td></td>
                        <td></td>
                         <td class="Form_TextoDer" >
                            % Tasa:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="TextBox10" runat="server" Width="80px" CssClass="Form_TextBoxDisable" Text="4.04" style="text-align:right;"></asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            Prima Anual $:</td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox id="TextBox11" runat="server" Width="80px" CssClass="Form_TextBoxDisable" Text="1,010.00" style="text-align:right;"></asp:TextBox>
                        </td>
                    </tr>                    
                    <tr>                       
                        <td class="Form_TextoDer" >
                            Con Descuento:
                        </td>
                        <td class="Form_TextoIzq" >
                            <asp:TextBox ID="txtPorDescto" runat="server" Text="10%" CssClass="Form_TextBoxDisable" Width="40px" ></asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            Monto Descuento $:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="TextBox12" runat="server" Text="101.00" CssClass="Form_TextBoxDisable" Width="80px" style="text-align:right;" ></asp:TextBox>
                        </td>
                        
                         <td class="Form_TextoDer">
                            % Tasa:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="TextBox9" runat="server" Width="80px" CssClass="Form_TextBoxDisable" Text="3.64" style="text-align:right;"></asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            Prima Anual $:</td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox id="txtprima" runat="server" Width="80px" CssClass="Form_TextBoxDisable" Text="909.00" style="text-align:right;" ></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <br />
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td>
                            <div class="Form_RegButtonDer">
                                <asp:Button ID="btnContinuar" runat="server" Text="Continuar" />                                
                                <asp:Button ID="btnNoEncontro" runat="server" Text="No se Contactó"/>
                            </div>
                        </td>
                    </tr>
                </table>           
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Style="display: none;"
                    SkinID="sknGridView">
                    <Columns>
                        <asp:BoundField HeaderText="Primer Nombre" DataField="Nombre1" />
                        <asp:BoundField DataField="Nombre2" HeaderText="Segundo Nombre" />
                        <asp:BoundField DataField="Apellido1" HeaderText="Apellido Paterno" />
                        <asp:BoundField DataField="Apellido2" HeaderText="Apellido Materno" />
                        <asp:BoundField DataField="DNI" HeaderText="Nro. Doc. Identidad" />
                        <asp:BoundField HeaderText="Telefonos" DataField="Telefono" />
                        <asp:BoundField DataField="Direccion" HeaderText="Dirección" />
                        <asp:BoundField DataField="Marca" HeaderText="Marca" />
                        <asp:BoundField DataField="Modelo" HeaderText="Marca" />
                    </Columns>
                </asp:GridView>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
</asp:Content>
