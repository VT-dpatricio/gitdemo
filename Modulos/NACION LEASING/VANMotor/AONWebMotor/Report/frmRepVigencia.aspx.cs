﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AONWebMotor.Report
{
    public partial class frmRepVigencia : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) 
            {
                Page.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            }
        }

        protected void btnVer_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt16(this.txtNroMeses.Text) > 0 && Convert.ToInt16(this.txtNroMeses.Text) <= 12)
                {
                    /*ObjectDataSource dataSource = new ObjectDataSource();

                    dataSource.TypeName = "AONAffinity.Business.Logic.BLReporte";
                    dataSource.SelectMethod = "ListarPorVencer";
                    dataSource.SelectParameters.Add("pnNroMeses", System.Data.DbType.Int32, txtNroMeses.Text);
                    dataSource.SelectParameters.Add("pnIdPproducto", System.Data.DbType.Int32, "5300");

                    Microsoft.Reporting.WebForms.ReportDataSource obj = new Microsoft.Reporting.WebForms.ReportDataSource("BECertificado", dataSource);
                    this.rvReporte.LocalReport.ReportPath = @"rdlc\rdlcPolizaPorVencer.rdlc";
                    this.rvReporte.LocalReport.DataSources.Add(obj);*/
                    this.rvReporte.LocalReport.Refresh();                                        
                }                
            }
            catch (Exception ex) 
            {
                throw ex;
            }
        }
    }
}
