<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true"
    CodeBehind="frmRepVigencia.aspx.cs" Inherits="AONWebMotor.Report.frmRepVigencia" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
    
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>    
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>
        Reporte Polizas por Vencer
    </h1>
    <table border="0" cellpadding="0" cellspacing="0" style="width:100%" >
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <asp:Panel ID="pnFiltro" runat="server" DefaultButton="btnVer" Width="100%" >
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width:100%">
                    <tr>
                        <td class="Form_TextoDer" style="width:100px"  >
                            Nro. Meses:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtNroMeses" runat="server" CssClass="Form_TextBox" MaxLength="2">
                            </asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="ftxtNroMeses" runat="server" TargetControlID="txtNroMeses" 
                                ValidChars="1234567890"></cc1:FilteredTextBoxExtender>
                        </td>
                        <td class="Form_TextoDer">
                            <asp:Button ID="btnVer" runat="server" Text="Ver" onclick="btnVer_Click" />
                        </td>                        
                    </tr>
                </table>
                </asp:Panel>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width:100%" >
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <div id="divReport" runat="server" style="overflow:auto; width:100%; height:390px" >
                    <rsweb:ReportViewer ID="rvReporte" runat="server" Width="100%" height="360px" 
                        Font-Names="Verdana" Font-Size="8pt" >
                        <LocalReport ReportPath="rdlc\rdlcPolizaPorVencer.rdlc">
                            <DataSources>
                                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="BECertificado" />
                            </DataSources>
                        </LocalReport>
                    </rsweb:ReportViewer>                    
                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
                        SelectMethod="ListarPorVencer" TypeName="AONAffinity.Business.Logic.BLReporte">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="txtNroMeses" DefaultValue="-1" 
                                Name="pnNroMeses" PropertyName="Text" Type="Int32" />
                            <asp:Parameter DefaultValue="5300" Name="pnIdPproducto" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </div>  
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    
</asp:Content>
