﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessLogic;
using AONAffinity.Motor.BusinessEntity;

namespace AONWebMotor.Mantenimiento
{
    public partial class frmMantMarca : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!ValidarAcceso(Page.AppRelativeVirtualPath))
                {
                    Response.Redirect("~/Login.aspx", false);
                }

                if (ValidarEstadoClaveUsuario())
                {
                    Response.Redirect("~/Seguridad/CambiarClave.aspx", false);
                }

                this.BuscarMarca();
            }
        }


        protected void gvMarca_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvMarca.PageIndex = e.NewPageIndex;
            gvMarca.EditIndex = -1;
            this.BuscarMarca();
        }

        private void BuscarMarca()
        {
            BLMarca objBLMarca = new BLMarca();
            this.CargarGridView(this.gvMarca, objBLMarca.Buscar(this.txtDescripcion.Text.Trim(), null));            
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            this.BuscarMarca();
        }

        protected void ibtnGrabar_Click(object sender, ImageClickEventArgs e)
        {
            BLMarca oBL = new BLMarca();
            BEMarca oBE = new BEMarca();            
            oBE.IdMarca = Int32.Parse(((Label)gvMarca.Rows[gvMarca.EditIndex].Cells[0].Controls[1]).Text.Trim());
            oBE.Descripcion = ((TextBox)gvMarca.Rows[gvMarca.EditIndex].Cells[1].Controls[1]).Text;
            oBE.CodExterno = ((TextBox)gvMarca.Rows[gvMarca.EditIndex].Cells[2].Controls[1]).Text;
            oBE.EstadoRegistro = ((CheckBox)gvMarca.Rows[gvMarca.EditIndex].Cells[3].Controls[1]).Checked;
            oBE.UsuarioCreacion = Session[AONAffinity.Motor.Resources.NombreSession.Usuario].ToString();
            if (oBE.IdMarca == 0)
            {
                oBL.Insertar(oBE);
            }
            else
            {
                oBL.Actualizar(oBE);
            }
            gvMarca.EditIndex = -1;
            BuscarMarca();
        }

        protected void gvMarca_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvMarca.EditIndex = -1;
            BuscarMarca();
        }

        protected void gvMarca_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvMarca.EditIndex = e.NewEditIndex;
            BuscarMarca();
        }

        protected void gvMarca_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            PintarNuevoRegistro(e, 1);
        }
    }
}