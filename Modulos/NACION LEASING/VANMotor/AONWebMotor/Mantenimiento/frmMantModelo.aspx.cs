﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Motor.Resources;
using AONAffinity.Motor.BusinessLogic;
using AONAffinity.Motor.BusinessEntity;

namespace AONWebMotor.Mantenimiento
{
    public partial class frmMantModelo : PageBase
    {
        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!ValidarAcceso(Page.AppRelativeVirtualPath))
                {
                    Response.Redirect("~/Login.aspx", false);
                }

                if (ValidarEstadoClaveUsuario())
                {
                    Response.Redirect("~/Seguridad/CambiarClave.aspx", false);
                }

                this.CargarMarca();
                ddlMarca.SelectedIndex = 0;
                BuscarModelo();
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.BuscarModelo();
            }
            catch (Exception ex)
            {
            }
        }
        #endregion

    
        private void CargarMarca()
        {
            BLMarca objBLMarca = new BLMarca();
            this.CargarDropDownList(this.ddlMarca, "IdMarca", "Descripcion", objBLMarca.Listar(true), false);
        }

        private void BuscarModelo()
        {
            /*if (this.ddlMarca.SelectedValue != "-1")
            {*/
                BLModelo objBLModelo = new BLModelo();
                this.CargarGridView(this.gvModelo, objBLModelo.ListarxMarcaDesc(Convert.ToInt32(this.ddlMarca.SelectedValue), this.txtDescripcion.Text.Trim(), null));
            /*}*/
        }

        protected void ibtnGrabar_Click(object sender, ImageClickEventArgs e)
        {
            BLModelo oBL = new BLModelo();
            BEModelo oBE = new BEModelo();
            oBE.IdMarca = Int32.Parse(ddlMarca.SelectedValue);
            oBE.IdModelo = Int32.Parse(((Label)gvModelo.Rows[gvModelo.EditIndex].Cells[0].Controls[1]).Text.Trim());            
            oBE.Descripcion = ((TextBox)gvModelo.Rows[gvModelo.EditIndex].Cells[1].Controls[1]).Text;
            oBE.IdClase = Int32.Parse(((DropDownList)gvModelo.Rows[gvModelo.EditIndex].Cells[2].Controls[1]).SelectedValue);
            oBE.IdTipoVehiculo = Int32.Parse(((DropDownList)gvModelo.Rows[gvModelo.EditIndex].Cells[3].Controls[1]).SelectedValue);            
            oBE.CodExterno = ((TextBox)gvModelo.Rows[gvModelo.EditIndex].Cells[4].Controls[1]).Text;
            oBE.Asegurable = ((CheckBox)gvModelo.Rows[gvModelo.EditIndex].Cells[5].Controls[1]).Checked;
            oBE.EstadoRegistro = ((CheckBox)gvModelo.Rows[gvModelo.EditIndex].Cells[6].Controls[1]).Checked;
            oBE.UsuarioCreacion = Session[AONAffinity.Motor.Resources.NombreSession.Usuario].ToString();
            if (oBE.IdModelo == 0)
            {
                oBL.Insertar(oBE);
            }
            else
            {
                oBL.Actualizar(oBE);
            }
            gvModelo.EditIndex = -1;
            BuscarModelo();
        }

        protected void gvModelo_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvModelo.PageIndex = e.NewPageIndex;
            gvModelo.EditIndex = -1;
            BuscarModelo();
        }

        protected void gvModelo_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvModelo.EditIndex = -1;
            BuscarModelo();
        }

        protected void gvModelo_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvModelo.EditIndex = e.NewEditIndex;
            BuscarModelo();
            DropDownList ddlClase = ((DropDownList)gvModelo.Rows[e.NewEditIndex].Cells[2].Controls[1]);
            String idClase = ((Label)gvModelo.Rows[e.NewEditIndex].Cells[2].Controls[2]).Text;
            DropDownList ddlTipoVehiculo = ((DropDownList)gvModelo.Rows[e.NewEditIndex].Cells[3].Controls[1]);
            String idTipoVehiculo = ((Label)gvModelo.Rows[e.NewEditIndex].Cells[3].Controls[2]).Text;
            //Cargar Clase
            BLClase oBLClase = new BLClase();            
            ddlClase.DataSource = oBLClase.Listar(null);
            ddlClase.DataTextField = "Descripcion"; 
            ddlClase.DataValueField = "IdClase";
            ddlClase.DataBind();
            ddlClase.SelectedValue = idClase;
            //Cargar TipoVeh
            BLTipoVehiculo oBLTipoVeh = new BLTipoVehiculo();
            ddlTipoVehiculo.DataSource = oBLTipoVeh.Listar(null);
            ddlTipoVehiculo.DataTextField = "Descripcion"; 
            ddlTipoVehiculo.DataValueField = "IdTipoVehiculo";
            ddlTipoVehiculo.DataBind();
            ddlTipoVehiculo.SelectedValue = idTipoVehiculo;
           
        }

        protected void gvModelo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            PintarNuevoRegistro(e, 1);
        }

        protected void ddlMarca_SelectedIndexChanged(object sender, EventArgs e)
        {
            BuscarModelo();
        }

        
    }
}