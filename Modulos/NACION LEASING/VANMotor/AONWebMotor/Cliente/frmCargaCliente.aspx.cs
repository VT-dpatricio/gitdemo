﻿using System;
using System.IO; 
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Resources;
using AONAffinity.Motor.BusinessLogic;
using AONAffinity.Business.Entity.BDMotor;
using AONAffinity.Motor.Resources;  

namespace AONWebMotor.Cliente
{
    public partial class frmCargaCliente : PageBase
    {
        #region Variables
        private Boolean bRegActivo = true;
        #endregion

        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {                    
                    this.CargarTipoProceso();
                    this.CargarFuncionesJS();
                    this.CargarCabeceraGvProceso();                    
                }
            }
            catch (Exception ex)
            {
                this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
            }
        }

        protected void btnCargar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!this.fuTrama.HasFile)
                {
                    this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Warning, "Seleccione el archivo a cargar.", ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
                    this.fuTrama.Focus();
                }
                else
                {
                    this.ValidarDirectorioArchivo();
                    this.ValidarArchivo();

                    BEProceso objBEProceso = new BEProceso();
                    objBEProceso.Descripcion = this.ddlTipProceso.SelectedItem.Text;
                    objBEProceso.Archivo = this.fuTrama.FileName;
                    objBEProceso.IdTipProceso = Convert.ToInt32(this.ddlTipProceso.SelectedValue);
                    objBEProceso.UsuarioCreacion = Session[NombreSession.Usuario].ToString();

                    BLProceso objBLProceso = new BLProceso();

                    if (objBLProceso.Insertar(objBEProceso) > 0)
                    {
                        String cArchivoTrama =  AppSettings.DirectorioTramaCli + fuTrama.FileName;   //UrlPagina.DirTramaCliente.Substring(2) + fuTrama.FileName;
                        //cArchivoTrama = Server.MapPath(cArchivoTrama);
                        this.fuTrama.SaveAs(cArchivoTrama);
                        this.CargarProcesos();
                        this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Success, "El archivo fue cargado al servidor en unos momentos sera procesado.", ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
                    }
                    else
                    {
                        throw new Exception("Error al cargar el archivo.");
                    }
                }
            }
            catch (Exception ex)
            {
                this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
            }
        }

        protected void ddlTipProceso_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.ddlTipProceso.SelectedValue != "-1")
                {
                    this.CargarProcesos();
                }
                else 
                {
                    this.gvProceso.DataSource = null;
                    this.gvProceso.DataBind();
                    this.CargarCabeceraGvProceso();
                }
            }
            catch (Exception ex)
            {
                this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
            }
        }

        protected void gvProceso_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes.Add("OnMouseOut", "this.className = this.orignalclassName;");
                    e.Row.Attributes.Add("OnMouseOver", "this.orignalclassName = this.className;this.className = 'GridWiew_SelectedRow';");

                    LinkButton lbtnArchivo = (LinkButton)e.Row.FindControl("lbtnArchivo");
                    LinkButton lbtnArchivoLog = (LinkButton)e.Row.FindControl("lbtnArchivoLog");                    
                    Label lblFecProceso = (Label)e.Row.FindControl("lblFecProceso");
                    Label lblRegTotal = (Label)e.Row.FindControl("lblRegTotal");
                    Label lblRegTotalVal = (Label)e.Row.FindControl("lblRegTotalVal");
                    Label lblRegTotalErr = (Label)e.Row.FindControl("lblRegTotalErr");
                    //ImageButton ibtnLog = (ImageButton)e.Row.FindControl("ibtnLog");  
                    ImageButton ibtnDarBaja = (ImageButton)e.Row.FindControl("ibtnDarBaja");
                    BEProceso objBEProceso = (BEProceso)(e.Row.DataItem);

                    if (lbtnArchivo != null)
                    {
                        lbtnArchivo.Text = objBEProceso.Archivo;
                        lbtnArchivo.Attributes.Add("href", "../" + AppSettings.DirectorioTramaCli + objBEProceso.Archivo);
                        lbtnArchivo.Attributes.Add("target", "_blank");                        
                    }                    

                    if (lblRegTotal != null && objBEProceso.RegTotal != NullTypes.IntegerNull) 
                    {
                        lblRegTotal.Text = objBEProceso.RegTotal.ToString();
                    }

                    if (lblRegTotalVal != null && objBEProceso.RegTotalVal != NullTypes.IntegerNull) 
                    {
                        lblRegTotalVal.Text = objBEProceso.RegTotalVal.ToString();
                    }

                    if (lblRegTotalErr != null && objBEProceso.RegTotalErr != NullTypes.IntegerNull) 
                    {
                        lblRegTotalErr.Text = objBEProceso.RegTotalErr.ToString();
                    }

                    if (lbtnArchivoLog != null && lblFecProceso != null)
                    {                        
                        if (objBEProceso.Procesado == true)
                        {
                            ibtnDarBaja.Enabled = false;
                            lbtnArchivoLog.Text = objBEProceso.ArchivoLog;
                            lbtnArchivoLog.Attributes.Add("href", "../" + AppSettings.DirectorioTramaCliLog + objBEProceso.ArchivoLog);
                            lbtnArchivoLog.Attributes.Add("target", "_blank");            
                            lblFecProceso.Text = objBEProceso.FecProceso.ToShortDateString();
                        }
                        else 
                        {
                            if (lblRegTotal != null) { lblRegTotal.Text = String.Empty; }
                            if (lblRegTotalVal != null) { lblRegTotalVal.Text = String.Empty; }
                            if (lblRegTotalErr != null) { lblRegTotalErr.Text = String.Empty; }
                            lbtnArchivoLog.Visible = false; 
                        }
                    }

                    if (ibtnDarBaja != null)
                    {
                        if (objBEProceso.Procesado == true)
                        {
                            ibtnDarBaja.Visible = false;
                        }
                        else
                        {
                            ibtnDarBaja.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de dar de baja el archivo?')== false) return false;");
                            ibtnDarBaja.CommandArgument = objBEProceso.IdProceso.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
            }
        }

        protected void gvProceso_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                switch (e.CommandName)
                {
                    case "DarBaja":
                        BEProceso objBEProceso = new BEProceso();
                        objBEProceso.IdProceso = Convert.ToInt32(e.CommandArgument);
                        objBEProceso.UsuarioModificacion = Session[NombreSession.Usuario].ToString();
                        objBEProceso.EstadoRegistro = false;

                        BLProceso objBLProceso = new BLProceso();

                        if (objBLProceso.ActualizarEstado(objBEProceso) > 0)
                        {
                            this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Success, "El archivo se dio de baja.", ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
                            this.CargarProcesos();
                        }
                        else
                        {
                            throw new Exception("Error al dar de baja el archivo.");
                        }

                        break;
                }
            }
            catch (Exception ex)
            {
                this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
            }
        }

        protected void gvProceso_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                this.gvProceso.PageIndex = e.NewPageIndex;
                this.CargarProcesos();
            }
            catch (Exception ex)
            {
                this.msgBox.Show(AONWebMotor.controles.MsgBox.MessageType.Error, ex.Message, ValorConstante.MsgBoxAlto, ValorConstante.MsgBoxAncho);
            }
        }
        #endregion

        #region Métodos Privados
        private void CargarTipoProceso()
        {
            BLTipoProceso objBLTipoProceso = new BLTipoProceso();
            this.CargarDropDownList(this.ddlTipProceso, "IdTipoProceso", "Descripcion", objBLTipoProceso.Listar(this.bRegActivo), true);
        }

        private void CargarFuncionesJS()
        {
            this.btnCargar.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de cargar el archivo?')== false) return false;");
        }

        private void CargarCabeceraGvProceso()
        {
            if (this.gvProceso.Rows.Count == 0)
            {
                List<BEProceso> lstBEProceso = new List<BEProceso>();
                BEProceso objBEProceso = new BEProceso();
                lstBEProceso.Add(objBEProceso);
                this.CargarGridView(this.gvProceso, lstBEProceso);
                this.gvProceso.Rows[0].Visible = false;
            }
        }

        private void CargarProcesos()
        {
            BLProceso objBLProceso = new BLProceso();
            this.CargarGridView(this.gvProceso, objBLProceso.Listar(Convert.ToInt32(this.ddlTipProceso.SelectedValue), true));
            this.CargarCabeceraGvProceso();
        }

        private void ValidarDirectorioArchivo()
        {
            String cUrlDirectorio = "../" + UrlPagina.DirTramaCliente.Substring(2);
            DirectoryInfo objDirectorio = new DirectoryInfo(Server.MapPath(cUrlDirectorio)); 

            if (!objDirectorio.Exists)
            {
                throw new Exception("El direcctorio no existe.");
            }

            String cUrlFile = "../" + AppSettings.DirectorioTramaCli + fuTrama.FileName;
            FileInfo objFileInfo = new FileInfo(Server.MapPath(cUrlFile));

            if (objFileInfo.Exists)
            {
                throw new Exception("El archivo ya existe en el directorio.");
            }
        }

        private void ValidarArchivo()
        {
            BLTipoProceso objBLTipoProceso = new BLTipoProceso();
            BETipoProceso objBETipoProceso = objBLTipoProceso.Obtener(Convert.ToInt32(this.ddlTipProceso.SelectedValue));

            if (objBETipoProceso != null)
            {
                String cExtencion = System.IO.Path.GetExtension(this.fuTrama.FileName).ToLower();

                if (cExtencion != ".txt")
                {
                    throw new Exception("El archivo no tiene formato correcto, debe ser: .txt");
                }

                String cFecha = "AAAAMMDD.txt";
                String cArchivoCargar = this.fuTrama.FileName;
                String cArchivoPatron = objBETipoProceso.NombreArchivo;

                cArchivoCargar = cArchivoCargar.Remove(cArchivoCargar.Length - cFecha.Length, cFecha.Length);
                cArchivoPatron = cArchivoPatron.Remove(cArchivoPatron.Length - cFecha.Length, cFecha.Length);

                if (cArchivoCargar != cArchivoPatron)
                {
                    throw new Exception("El nombre del archivo es incorrecto debe tener el formato: " + objBETipoProceso.NombreArchivo);
                }

                if (objBETipoProceso.NombreArchivo.Length != this.fuTrama.FileName.ToString().Length)
                {
                    throw new Exception("El nombre del archivo es incorrecto debe tener el formato: " + objBETipoProceso.NombreArchivo);
                }
            }
        }
        #endregion        
    }
}    
