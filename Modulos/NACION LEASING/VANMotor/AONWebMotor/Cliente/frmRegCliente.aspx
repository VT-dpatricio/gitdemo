﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true"
    CodeBehind="frmRegCliente.aspx.cs" Inherits="AONWebMotor.Cliente.frmRegCliente" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../controles/MsgBox.ascx" TagName="MsgBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script language="javascript" type="text/javascript">
        function LimpiarForm(Busq) {
            document.getElementById('<%=ddlClase.ClientID %>').value = "0";
            document.getElementById('<%=ddlMarca.ClientID %>').value = "0";
            document.getElementById('<%=hfIdModelo.ClientID %>').value = "";
            document.getElementById('<%=ddlAniFabricacion.ClientID %>').value = "0";

            document.getElementById('<%=txtPriNombre.ClientID %>').value = "";
            document.getElementById('<%=txtSegNombre.ClientID %>').value = "";
            document.getElementById('<%=txtApePaterno.ClientID %>').value = "";
            document.getElementById('<%=txtApeMaterno.ClientID %>').value = "";
            document.getElementById('<%=ddlTipDocumento.ClientID %>').value = "L"
            document.getElementById('<%=ddlDepartamento.ClientID %>').value = "5115";
            document.getElementById('<%=hfProvincia.ClientID %>').value = "511501";
            document.getElementById('<%=hfDistrito.ClientID %>').value = "511501001";
            document.getElementById('<%=txtDireccion.ClientID %>').value = "";
            document.getElementById('<%=txtTelefono.ClientID %>').value = "";
            document.getElementById('<%=txtCorreo.ClientID %>').value = "";

            //Buscar por placa
            if (Busq == 1) {
                document.getElementById('<%=txtNroDocumento.ClientID %>').value = "";
            }

            //Buscar por Nro. Documento
            if (Busq == 2) {
                document.getElementById('<%=txtNroPlaca.ClientID %>').value = "";
            }

            document.getElementById('<%=btnRefreshClear.ClientID %>').click();
        }
        
        function ClientexNroPlacaAsincrono(e) {
            var evt = e ? e : window.event;
            if (evt.keyCode == 13) {
                var nroPlaca = document.getElementById('<%=txtNroPlaca.ClientID %>').value;

                if (nroPlaca != "") {
                    ObtenerClientexNroPlaca(nroPlaca);
                    return false;
                }
                else {
                    return false;
                }
            }
        }
        function ObtenerClientexNroPlaca(pNroPlaca) {
            var cliente = PageMethods.ObtenerClietexNroPlaca(pNroPlaca, onSucceedClientexNroPlaca, OnErrorClientexNroPlaca);
        }
        function onSucceedClientexNroPlaca(result) {
            var arrValores = result.split('*');
            if (result != "") {
                document.getElementById('<%=txtPriNombre.ClientID %>').value = Encoder.htmlDecode(arrValores[0]);
                document.getElementById('<%=txtSegNombre.ClientID %>').value = Encoder.htmlDecode(arrValores[1]);
                document.getElementById('<%=txtApePaterno.ClientID %>').value = Encoder.htmlDecode(arrValores[2]);
                document.getElementById('<%=txtApeMaterno.ClientID %>').value = Encoder.htmlDecode(arrValores[3]);
                document.getElementById('<%=ddlTipDocumento.ClientID %>').value = Encoder.htmlDecode(arrValores[4]);
                document.getElementById('<%=txtNroDocumento.ClientID %>').value = Encoder.htmlDecode(arrValores[5]);
                document.getElementById('<%=ddlDepartamento.ClientID %>').value = Encoder.htmlDecode(arrValores[6].substring(0, 4));
                document.getElementById('<%=hfProvincia.ClientID %>').value = Encoder.htmlDecode(arrValores[6].substring(0, 6));
                document.getElementById('<%=hfDistrito.ClientID %>').value = Encoder.htmlDecode(arrValores[6]);
                document.getElementById('<%=txtDireccion.ClientID %>').value = Encoder.htmlDecode(arrValores[7]);
                document.getElementById('<%=ddlClase.ClientID %>').value = Encoder.htmlDecode(arrValores[8]);
                document.getElementById('<%=ddlMarca.ClientID %>').value = Encoder.htmlDecode(arrValores[9]);
                document.getElementById('<%=hfIdModelo.ClientID %>').value = Encoder.htmlDecode(arrValores[10]);
                document.getElementById('<%=ddlAniFabricacion.ClientID %>').value = Encoder.htmlDecode(arrValores[11]);
                document.getElementById('<%=txtTelefono.ClientID %>').value = Encoder.htmlDecode(arrValores[13]);
                document.getElementById('<%=txtCorreo.ClientID %>').value = Encoder.htmlDecode(arrValores[14]);

                document.getElementById('<%=ddlClase.ClientID %>').value = 2;
                document.getElementById('<%=btnRefresh.ClientID %>').click();
            }
            else {
                LimpiarForm(1);
                alert('No se encontró el cliente.');
            }
        }
        function OnErrorClientexNroPlaca(result) {
            LimpiarForm(1);
            alert('No se encontró el cliente.');
        }
        function OnColorSelected(pCod, pDes) {
            document.getElementById('<%=hfCodColor.ClientID %>').value = pCod;
            document.getElementById('<%=txtColor.ClientID %>').value = Encoder.htmlDecode(pDes);
            document.getElementById('<%=hfDesColor.ClientID %>').value = Encoder.htmlDecode(pDes);
            document.getElementById('<%=txtColor.ClientID %>').focus();
            hideModalPopup('bmpeBuscarColor');
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="Form_SubTitulo" colspan="6">
                            DATOS DEL CLIENTE
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer" style="width: 110px;">
                            Primer Nombre:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtPriNombre" runat="server" CssClass="Form_TextBox" Width="120px">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvPriNombre" runat="server" ControlToValidate="txtPriNombre"
                                Text="*" ErrorMessage="Primer nombre obligatorio." Display="None" SetFocusOnError="true"
                                ValidationGroup="Certificado"></asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Segundo Nombre:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtSegNombre" runat="server" CssClass="Form_TextBox" Width="120px">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSegNombre"
                                Text="*" ErrorMessage="Segundo nombre obligatorio." Display="None" SetFocusOnError="true"
                                ValidationGroup="Certificado"></asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Tipo Documento:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlTipDocumento" runat="server" Width="180px">
                                <asp:ListItem Text="SELECCIONE" Value="0"></asp:ListItem>
                                <asp:ListItem Text="DNI" Value="L"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvPlan" runat="server" Text="*" ControlToValidate="ddlTipDocumento"
                                Display="None" ErrorMessage="Seleccione tipo de documento." ForeColor="Transparent"
                                SetFocusOnError="true" InitialValue="0" ValidationGroup="Certificado">
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Apellido Paterno:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtApePaterno" runat="server" CssClass="Form_TextBox" Width="120px">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvApePaterno" runat="server" ControlToValidate="txtApePaterno"
                                Text="*" ErrorMessage="Apellido paterno obligatorio." Display="None" SetFocusOnError="true"
                                ValidationGroup="Certificado"></asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Apellido Materno:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtApeMaterno" runat="server" CssClass="Form_TextBox" Width="120px">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtApeMaterno"
                                Text="*" ErrorMessage="Apellido materno obligatorio." Display="None" SetFocusOnError="true"
                                ValidationGroup="Certificado"></asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Nro. Documento:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtNroDocumento" runat="server" Width="90px" CssClass="Form_TextBox" MaxLength="8" >
                            </asp:TextBox>
                            <cc1:FilteredTextBoxExtender id="fteNroDocumento" runat="server" TargetControlID="txtNroDocumento" ValidChars="0123456789"></cc1:FilteredTextBoxExtender>
                            <asp:RequiredFieldValidator ID="rfvNroDocumento" runat="server" ControlToValidate="txtNroDocumento"
                                Text="*" ErrorMessage="Nro Documento obligatorio." Display="None" SetFocusOnError="true"
                                ValidationGroup="Certificado"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Departamento:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlDepartamento" runat="server" Width="140px" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged">
                                <asp:ListItem Text="SELECCIONE" Value="-1"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Text="*"
                                ControlToValidate="ddlDepartamento" Display="None" ErrorMessage="Seleccione departamento."
                                ForeColor="Transparent" SetFocusOnError="true" InitialValue="0" ValidationGroup="Certificado">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Provincia:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlProvincia" runat="server" Width="140px" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlProvincia_SelectedIndexChanged">
                                <asp:ListItem Text="SELECCIONE" Value="-1"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Text="*"
                                ControlToValidate="ddlProvincia" Display="None" ErrorMessage="Seleccione provincia."
                                ForeColor="Transparent" SetFocusOnError="true" InitialValue="0" ValidationGroup="Certificado">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Distrito:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlDistrito" runat="server" Width="140px">
                                <asp:ListItem Text="SELECCIONE" Value="-1"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Text="*"
                                ControlToValidate="ddlDistrito" Display="None" ErrorMessage="Seleccione distrito."
                                ForeColor="Transparent" SetFocusOnError="true" InitialValue="0" ValidationGroup="Certificado">
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Direccción:
                        </td>
                        <td class="Form_TextoIzq" colspan="2">
                            <asp:TextBox ID="txtDireccion" runat="server" CssClass="Form_TextBox" Width="200px">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtNroDocumento"
                                Text="*" ErrorMessage="Dirección obligatorio." Display="None" SetFocusOnError="true"
                                ValidationGroup="Certificado"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                        </td>
                        <td class="Form_TextoDer">
                            Email: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtCorreo" runat="server" CssClass="Form_TextBox" Width="180px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Teléfono Fijo:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtTelefono" runat="server" CssClass="Form_TextBox"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender id="fteTelefono" runat="server" TargetControlID="txtTelefono" ValidChars="*-1234567890" ></cc1:FilteredTextBoxExtender>
                            <asp:RequiredFieldValidator ID="rfvTelefono" runat="server" ControlToValidate="txtTelefono"
                                Text="*" ErrorMessage="Teléfono fijo obligatorio." Display="None" SetFocusOnError="true"
                                ValidationGroup="Certificado"></asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Teléfono Movil:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtTelMovil" runat="server" CssClass="Form_TextBox">
                            </asp:TextBox>
                            <cc1:FilteredTextBoxExtender id="fteTelMovil" runat="server" TargetControlID="txtTelMovil" ValidChars="*-1234567890" >
                            </cc1:FilteredTextBoxExtender>
                            <asp:RequiredFieldValidator ID="rfvTelMovil" runat="server" ControlToValidate="txtTelMovil"
                                Text="*" ErrorMessage="Teléfono movil obligatorio." Display="None" SetFocusOnError="true"
                                ValidationGroup="Certificado"></asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Teléfono Oficina:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtTelOficina" runat="server" CssClass="Form_TextBox">
                            </asp:TextBox>
                            <cc1:FilteredTextBoxExtender id="fteTelOficina" runat="server" TargetControlID="txtTelOficina" ValidChars="*-1234567890" >
                            </cc1:FilteredTextBoxExtender>
                            <asp:RequiredFieldValidator ID="rfvTelOficina" runat="server" ControlToValidate="txtTelOficina"
                                Text="*" ErrorMessage="Teléfono oficina obligatorio." Display="None" SetFocusOnError="true"
                                ValidationGroup="Certificado"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Es Traslado:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlTraslado" runat="server" Width="120px">
                                <asp:ListItem Text="SELECCIONE" Value="0"></asp:ListItem>
                                <asp:ListItem Text="SI" Value="1"></asp:ListItem>
                                <asp:ListItem Text="NO" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" Text="*"
                                ControlToValidate="ddlTraslado" Display="None" ErrorMessage="Seleccione si es traslado."
                                ForeColor="Transparent" SetFocusOnError="true" InitialValue="0" ValidationGroup="Certificado">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Fecha Nacimiento:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtFecNacimiento" runat="server" CssClass="Form_TextBox"></asp:TextBox>
                            <cc1:MaskedEditExtender id="meeFecNacimiento" runat="server" TargetControlID="txtFecNacimiento" Mask="99/99/9999" MaskType="Date">
                            </cc1:MaskedEditExtender>
                            <asp:RequiredFieldValidator ID="rfvFecNacimiento" runat="server" ControlToValidate="txtFecNacimiento"
                                Text="*" ErrorMessage="Fecha nacimiento obligatorio." Display="None" SetFocusOnError="true"
                                ValidationGroup="Certificado"></asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Edad: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtEdad" runat="server" CssClass="Form_TextBox" MaxLength="2" ></asp:TextBox>
                            <cc1:FilteredTextBoxExtender id="fteEdad" runat="server" TargetControlID="txtEdad" ValidChars="1234567890" >
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                </table>
                <br />
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td colspan="6" class="Form_SubTitulo">
                            DATOS DEL VEHÍCULO
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer" style="width: 80px;">
                            Nro. Placa:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtNroPlaca" runat="server" CssClass="Form_TextBoxCod" Width="120px"
                                MaxLength="15"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvNroPlaca" runat="server" ControlToValidate="txtNroPlaca"
                                SetFocusOnError="true" Text="*" Display="None" ErrorMessage="Nro. placa obligatorio."
                                ValidationGroup="Certificado">
                            </asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="fteNroPlaca" runat="server" TargetControlID="txtNroPlaca"
                                ValidChars="ABCDEFGHIJKMNÑOPQRSTUVWXYZabcdefghijkmnñopqrstuvwxyz -0123456789">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                        <td class="Form_TextoDer">
                            Clase:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlClase" runat="server" Width="120px">
                                <asp:ListItem Text="SELECCIONE" Value="-1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="Form_TextoDer">
                            Marca:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlMarca" runat="server" Width="120px" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlMarca_SelectedIndexChanged">
                                <asp:ListItem Text="SELECCIONE" Value="-1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Modelo:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlModelo" runat="server" Width="120px">
                                <asp:ListItem Text="SELECCIONE" Value="-1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="Form_TextoDer">
                            Año:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlAniFabricacion" runat="server" Width="120px">
                                <asp:ListItem Text="SELECCIONE" Value="-1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="Form_TextoDer">
                            Carrocería: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlTipCarroceria" runat="server" Width="120px">
                                <asp:ListItem Text="SELECCIONE" Value="-1"></asp:ListItem>
                            </asp:DropDownList>                            
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Color:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtColor" runat="server" CssClass="Form_TextBoxCod" Width="125px"
                                ReadOnly="true" onkeyup="showPopupKey('bmpeBuscarColor','ctl00_ContentPlaceHolder1_ddlColorBase');">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvCodColor" runat="server" ControlToValidate="txtColor"
                                Display="None" ErrorMessage="Color obligatorio." Text="*" ValidationGroup="Certificado"></asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoDer">
                            Nro. Pasajeros: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtNroPasjeros" runat="server" CssClass="Form_TextBox" Width="60px"
                                MaxLength="2">
                            </asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="fteNroCarroceria" runat="server" TargetControlID="txtNroPasjeros"
                                ValidChars="0123456789">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                        <td class="Form_TextoDer">
                            Nro. Motor: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtNroMotor" runat="server" CssClass="Form_TextBox" Width="120px"
                                MaxLength="15"></asp:TextBox>                            
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Combustible: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlTipCombustible" runat="server" Width="120px">
                                <asp:ListItem Text="SELECCIONE" Value="0"></asp:ListItem>
                            </asp:DropDownList>                            
                        </td>
                        <td class="Form_TextoDer">
                            Mercancia: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlTipMercancia" runat="server" Width="120px">
                                <asp:ListItem Text="SELECCIONE" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="Form_TextoDer">
                            Nro. Chasis: <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtNroChasis" runat="server" CssClass="Form_TextBox" Width="120px"
                                MaxLength="15"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <br />
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td colspan="6" class="Form_SubTitulo">
                            DATOS ADICIONALES
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Es Timon Cambiado:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlTimonCambiodo" runat="server" Width="120px">
                                <asp:ListItem Text="SELECCIONE" Value="-1"></asp:ListItem>
                                <asp:ListItem Text="SI" Value="1"></asp:ListItem>
                                <asp:ListItem Text="NO" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="Form_TextoDer">
                            Requiere GPS:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="DropDownList1" runat="server" Width="120px">
                                <asp:ListItem Text="SELECCIONE" Value="-1"></asp:ListItem>
                                <asp:ListItem Text="SI" Value="1"></asp:ListItem>
                                <asp:ListItem Text="NO" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Nro de Siniestros:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtNroSiniestro" runat="server" CssClass="Form_TextBoxCod" Width="120px"
                                onkeyup="showPopupKey('bmpeAgregarSiniestro','ctl00_ContentPlaceHolder1_ddlSiniestro');"></asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            Nro. Vig. Concecutivas:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtNroVigencia" runat="server" CssClass="Form_TextBoxCod" Width="120px"
                                onkeyup="showPopupKey('bmpeAgregarVigencia','ctl00_ContentPlaceHolder1_ddlAnioVigencia');"></asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            Última Vigencia:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtUltimaVig" runat="server" CssClass="Form_TextBox" Width="120px">
                            </asp:TextBox>
                            <cc1:MaskedEditExtender id="meeUltVigencia" runat="server" TargetControlID="txtUltimaVig" Mask="99/99/999" MaskType="Date">
                            </cc1:MaskedEditExtender>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <div class="Form_RegButtonDer">
                    <asp:Button ID="btnGuaVenta" runat="server" Text="Grabar" ValidationGroup="Certificado"
                        OnClick="btnGuaVenta_Click" />
                    <asp:Button ID="btnSiniestros" runat="server" Text="Siniestros" OnClientClick="showModalFocus('bmpeAgregarSiniestro','ctl00_ContentPlaceHolder1_ddlSiniestro'); return false; " />
                    <asp:Button ID="btnVigencias" runat="server" Text="Vigencias" OnClientClick="showModalFocus('bmpeAgregarVigencia','ctl00_ContentPlaceHolder1_ddlAnioVigencia'); return false; " />
                    <span class="Form_TextoObligatorio">&nbsp;(*)Campos no Obligatorios</span>
                </div>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <div style="display: none;">
        <asp:HiddenField ID="hfIdCotizacion" runat="server" />
        <asp:HiddenField ID="hfNomArchivo" runat="server" />
        <asp:HiddenField ID="hfCliente" runat="server" />
        <asp:Button ID="btnRefresh" runat="server" Text="Modelo" CausesValidation="false"
            Style="display: none;" OnClick="btnRefresh_Click" />
        <asp:Button ID="btnRefreshClear" runat="server" Text="Modelo" Style="display: none;"
            OnClick="btnRefreshClear_Click" CausesValidation="false" />
        <asp:HiddenField ID="hfIdModelo" runat="server" />
        <asp:HiddenField ID="hfProvincia" runat="server" />
        <asp:HiddenField ID="hfDistrito" runat="server" />
        <asp:HiddenField ID="hfCodColor" runat="server" />
        <asp:HiddenField ID="hfDesColor" runat="server" />
    </div>
    <asp:ValidationSummary ID="vsCertificado" runat="server" ShowMessageBox="true" ShowSummary="false"
        ValidationGroup="Certificado" />
    <uc1:MsgBox ID="msgBox" runat="server" />    
    <!--Buscar Color-->
    <asp:Panel ID="pnBuscarColor" runat="server" Width="500px" CssClass="Modal_Panel" Style="display: none;"
        DefaultButton="btnBusqColor">
        <div class="Modal_Head" style="width: 100%;">
            <div id="div3" style="float: left;">
                Buscar Color
            </div>
            <div id="div4" style="float: right;">
                <img alt="Click para cerrar." class="Modal_CloseButton" onclick="hideModalPopup('bmpeBuscarColor')"
                    style="cursor: hand;" />
            </div>
        </div>        
        <div class="Modal_Body">
            <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="Form_TextoIzq">
                        Descripción:
                    </td>
                    <td class="Form_TextoIzq">
                        <asp:DropDownList ID="ddlColorBase" runat="server" Width="150px">
                        </asp:DropDownList>
                    </td>
                    <td class="Form_TextoIzq" style="width: 16px;">
                        <asp:UpdateProgress ID="upsBusqColor" runat="server" AssociatedUpdatePanelID="upBusqColor"
                            DisplayAfter="0">
                            <ProgressTemplate>
                                <asp:Image ID="imgLoadColor" runat="server" SkinID="sknImgLoading" Width="16px" Height="16px" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                    <td class="Form_TextoDer">
                        <asp:UpdatePanel ID="upBusqColor" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnBusqColor" runat="server" Text="Buscar" OnClick="btnBusqColor_Click"
                                    Width="80px" CausesValidation="false"   />
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnBusqColor" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
            <br />
            <div style="overflow: auto; height: 200px;">
                <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="gvBusqColor" runat="server" SkinID="sknGridView" AutoGenerateColumns="false"
                            Width="98%" OnRowDataBound="gvBusqColor_RowDataBound">
                            <Columns>
                                <asp:BoundField DataField="IdColorOpcion" HeaderText="Código">
                                    <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Descripcion" HeaderText="Color">
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="gvBusqColor" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <br />
        </div>
    </asp:Panel>
    <asp:Label ID="lblBuscarColor" runat="server" Style="display: none;"></asp:Label>
    <cc1:ModalPopupExtender ID="mpeBuscarColor" runat="server" Enabled="true" TargetControlID="lblBuscarColor"
        BehaviorID="bmpeBuscarColor" PopupControlID="pnBuscarColor" BackgroundCssClass="Modal_Background">
    </cc1:ModalPopupExtender>
    <!--Agregar Siniestro-->
    <asp:Panel ID="pnAgregarSiniestro" runat="server" Width="500px" CssClass="Modal_Panel" Style="display: none;"
        DefaultButton="btnAgrSiniestro" >
        <div class="Modal_Head" style="width: 100%;">
            <div id="divHeadTitulo" style="float: left;">
                Agregar Siniestros
            </div>
            <div id="divHeadCerrar" style="float: right;">
                <img alt="Click para cerrar." class="Modal_CloseButton" onclick="hideModalPopup('bmpeAgregarSiniestro')"
                    style="cursor: hand;" />
            </div>
        </div>
        <div class="Modal_Body">
            <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="Form_TextoIzq">
                        Siniestro:
                    </td>
                    <td class="Form_TextoDer">
                        <asp:DropDownList ID="ddlSiniestro" runat="server" Width="120px">
                            <asp:ListItem Text="SELECCIONE" Value="0"></asp:ListItem>
                            <asp:ListItem Text="CHOQUE" Value="0"></asp:ListItem>
                            <asp:ListItem Text="PERDIDA TOTAL" Value="0"></asp:ListItem>
                            <asp:ListItem Text="ROBO PARCIAL" Value="0"></asp:ListItem>
                            <asp:ListItem Text="ROBO TOTAL" Value="0"></asp:ListItem>
                            <asp:ListItem Text="RESPONSABILIDAD CIVIL" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td class="Form_TextoIzq">
                        Fecha Sinietro:
                    </td>
                    <td class="Form_TextoDer">
                        <asp:TextBox ID="FecSiniestro" runat="server" Width="80px">
                        </asp:TextBox>
                        <cc1:MaskedEditExtender ID="meeFecSiniestro" runat="server"  TargetControlID="FecSiniestro" Mask="99/99/9999" MaskType="Date">
                        </cc1:MaskedEditExtender>
                    </td>
                    <td class="Form_TextoDer">
                        <asp:Button ID="btnAgrSiniestro" runat="server" Text="Agregar" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="Modal_Body" style="overflow: auto; height: 200px;">
            <asp:UpdatePanel ID="upAgreSiniestro" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvAgrSiniestro" runat="server" SkinID="sknGridView" AutoGenerateColumns="false"
                        Width="98%">
                        <Columns>
                            <asp:BoundField DataField="Desc" HeaderText="Sinietro">
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Fec" HeaderText="Fecha">
                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Quitar">
                                <ItemTemplate>
                                    <asp:ImageButton ID="ibtnAnular" runat="server" SkinID="sknIbtnIcoAnular" CommandName="Quitar" />
                                </ItemTemplate>
                                <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                <ItemStyle Width="50px" HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="gvAgrSiniestro" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </asp:Panel>
    <asp:Label ID="lblAgregarSiniestro" runat="server" Style="display: none;"></asp:Label>
    <cc1:ModalPopupExtender ID="mpeAgregarSiniestro" runat="server" Enabled="true" TargetControlID="lblAgregarSiniestro"
        BehaviorID="bmpeAgregarSiniestro" PopupControlID="pnAgregarSiniestro" BackgroundCssClass="Modal_Background">
    </cc1:ModalPopupExtender>
    <!--Agregar Vigencia-->
    <asp:Panel ID="pnAgregarVigencia" runat="server" Width="500px" CssClass="Modal_Panel" Style="display: none;"
        DefaultButton="btnAgrVigencia">
        <div class="Modal_Head" style="width: 100%;">
            <div id="div1" style="float: left;">
                Agregar Vigencias
            </div>
            <div id="div2" style="float: right;">
                <img alt="Click para cerrar." class="Modal_CloseButton" onclick="hideModalPopup('bmpeAgregarVigencia')"
                    style="cursor: hand;" />
            </div>
        </div>
        <div class="Modal_Body">
            <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="Form_TextoIzq">
                        Año:
                    </td>
                    <td class="Form_TextoDer">
                        <asp:DropDownList ID="ddlAnioVigencia" runat="server" Width="120px">
                            <asp:ListItem Text="SELECCIONE" Value="0"></asp:ListItem>
                            <asp:ListItem Text="2010" Value="0"></asp:ListItem>
                            <asp:ListItem Text="2009" Value="0"></asp:ListItem>
                            <asp:ListItem Text="2008" Value="0"></asp:ListItem>
                            <asp:ListItem Text="2007" Value="0"></asp:ListItem>
                            <asp:ListItem Text="2006" Value="0"></asp:ListItem>
                            <asp:ListItem Text="2005" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td class="Form_TextoIzq">
                        Fecha Vigencia:
                    </td>
                    <td class="Form_TextoDer">
                        <asp:TextBox ID="txtFecVigencia" runat="server" Width="60px">
                        </asp:TextBox>
                        <cc1:MaskedEditExtender id="meeFecVigencia" runat="server" TargetControlID="txtFecVigencia" Mask="99/99/9999" MaskType="Date" >
                        </cc1:MaskedEditExtender>
                    </td>
                    <td class="Form_TextoIzq">
                        Nro. Póliza:
                    </td>
                    <td class="Form_TextoDer">
                        <asp:TextBox ID="txtNroProliza" runat="server" Width="80px">
                        </asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="fteNroProliza" runat="server" TargetControlID="txtNroProliza" ValidChars="1234567890-">
                        </cc1:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td class="Form_TextoDer">
                        <asp:Button ID="btnAgrVigencia" runat="server" Text="Agregar" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="Modal_Body" style="overflow: auto; height: 200px;">
            <asp:UpdatePanel ID="upGvAgrVigencia" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvAgrVigencia" runat="server" SkinID="sknGridView" AutoGenerateColumns="false"
                        Width="98%">
                        <Columns>
                            <asp:BoundField DataField="Desc" HeaderText="Año">
                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Poliza" HeaderText="Poliza">
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>                            
                            <asp:BoundField DataField="Fec" HeaderText="Fecha">
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Quitar">
                                <ItemTemplate>
                                    <asp:ImageButton ID="ibtnAnular" runat="server" SkinID="sknIbtnIcoAnular" CommandName="Quitar" />
                                </ItemTemplate>
                                <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                <ItemStyle Width="50px" HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="gvAgrVigencia" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </asp:Panel>
    <asp:Label ID="lblAgregarVigencia" runat="server" Style="display: none;"></asp:Label>
    <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" Enabled="true" TargetControlID="lblAgregarVigencia"
        BehaviorID="bmpeAgregarVigencia" PopupControlID="pnAgregarVigencia" BackgroundCssClass="Modal_Background">
    </cc1:ModalPopupExtender>
</asp:Content>
