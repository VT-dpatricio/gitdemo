﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true" CodeBehind="frmCargar.aspx.cs" Inherits="AONWebMotor.Trama.frmCargar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="Form_SubTitulo" colspan="6">
                            CARGAR INFORMACIÓN MASIVA
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td class="Form_TextoDer">
                            Tipo de Proceso:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlTipProceso" runat="server" Width="200px" 
                                AutoPostBack="True" onselectedindexchanged="ddlTipProceso_SelectedIndexChanged">
                            </asp:DropDownList>      
                            <asp:RequiredFieldValidator ID="rfvTipProceso" runat="server" Text="*" ControlToValidate="ddlTipProceso"
                                Display="None" ErrorMessage="Seleccione tipo de proceso." 
                                SetFocusOnError="true" InitialValue="-1" ValidationGroup="CargarArchivo">
                            </asp:RequiredFieldValidator>                      
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td class="Form_TextoDer">
                            Archivo:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:FileUpload ID="fuTrama" runat="server" />
                            <asp:Button ID="btnCargar" runat="server" Text="Cargar" ValidationGroup="CargarArchivo" 
                                onclick="btnCargar_Click" />
                        </td>
                        <td class="Form_TextoIzq">
                        </td>
                        <td>
                        </td>
                    </tr>                    
                </table>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <a href="~/Controles/MsgBox.ascx">~/Controles/MsgBox.ascx</a>
</asp:Content>
