﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Business.Entity.BDJanus;
using AONAffinity.Business.Entity.BDMotor;    
using AONAffinity.Business.Logic;
using AONAffinity.Resources;    

namespace AONWebMotor.Cliente
{
    public partial class frmRegCliente : PageBase 
    {
        #region Variables Privadas
        /// <summary>
        /// Variable que indica los registros
        /// de estado activo.
        /// </summary>
        private Boolean bEstadoReg = true;

        /// <summary>
        ///Variable de codigo de departamento por
        ///defecto Lima.
        /// </summary>
        private Int32 nCodDepartamento = 5115;

        /// <summary>
        /// Variable de codigo de tipo de documento
        /// por defecto DNI.
        /// </summary>
        private String cCodDocIdentidad = "L";

        private Int32 nIdMercancia = 3;
        #endregion        


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) 
            {
                /*this.load();
                this.CargarDepartamento();
                this.CargarCiudad();
                this.CargarProvincia();
                this.CargarDistrito();
                this.CargarClase();
                this.CargarMarca();
                this.CargarModelo();
                 
                this.CargarAnio();
                this.CargarFuncionesJS();
                this.CargarColor();
                this.CargarCabeceraGvBusqColor();
                this.CargarTipoCarroceria();
                this.CargarTipoConbustible();
                this.CargarTipoMercancia();*/
                /*List<BEModelo> lstBEModelo = ((List<BEModelo>)(ViewState[Constantes.ListaModelo]));
                BLModelo objBLModelo = new BLModelo();
                this.CargarDropDownList(this.ddlModelo, "IdModelo", "Descripcion", objBLModelo.ListarxMarca(lstBEModelo, Convert.ToInt32(this.ddlMarca.SelectedValue), Convert.ToInt32(this.ddlClase.SelectedValue)), false);*/
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try
            {
                this.CargarModelo();
                this.ddlModelo.SelectedValue = this.hfIdModelo.Value;
                this.CargarProvincia();
                this.ddlProvincia.SelectedValue = this.hfProvincia.Value;
                this.CargarDistrito();
                this.ddlDistrito.SelectedValue = this.hfDistrito.Value;
                this.txtNroPlaca.Focus();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void btnRefreshClear_Click(object sender, EventArgs e)
        {
            /*
            * CREADO POR:              Gary Porras Paraguay
            * FECHA DE CREACION:       2011-05-16
            * MODIFICADO POR:          
            * FECHA DE MODIFICACION:   
            */

            try
            {
                this.CargarProvincia();
                this.ddlProvincia.SelectedValue = this.hfProvincia.Value;
                this.CargarDistrito();
                this.ddlDistrito.SelectedValue = this.hfDistrito.Value;
                //this.rbtnTimCambiadoNo.Checked = true;
                //this.rbtnNuevoNo.Checked = true;
                //this.rbtnGpsNo.Checked = true;
                this.txtNroPlaca.Focus();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        private void load() 
        {
            List<Siniestro> lstSiniestro = new List<Siniestro>();
            Siniestro objSiniestro = new Siniestro();

            objSiniestro.Desc = "CHOQUE";
            objSiniestro.Fec = "15/08/2011";

            lstSiniestro.Add(objSiniestro);

            this.gvAgrSiniestro.DataSource = lstSiniestro;
            this.gvAgrSiniestro.DataBind();

            List<Siniestro> lstSiniestro1 = new List<Siniestro>();
            Siniestro objSiniestro1 = new Siniestro();

            objSiniestro1.Desc = "2010";
            objSiniestro1.Poliza = "201045256";
            objSiniestro1.Fec = "05/04/2010";

            lstSiniestro1.Add(objSiniestro1);

            this.gvAgrVigencia.DataSource = lstSiniestro1;
            this.gvAgrVigencia.DataBind();
        }

        #region Métodos Privados
        /// <summary>
        /// Permite cargar las clases de vehículos.
        /// </summary>
        private void CargarClase()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLClase objBLClase = new BLClase();
            this.CargarDropDownList(ddlClase, "IdClase", "Descripcion", objBLClase.Listar(this.bEstadoReg), true);
        }

        /// <summary>
        /// Permite cargar las marcas de vehículos
        /// </summary>
        private void CargarMarca()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLMarca objBLMarca = new BLMarca();
            this.CargarDropDownList(this.ddlMarca, "idMarca", "descripcion", objBLMarca.Listar(this.bEstadoReg), true);
        }

        /// <summary>
        /// Permite limpiar los items del control ddlModelo.
        /// </summary>
        private void LimpiarDropDownListModelo()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            List<BEModelo> lstBEModelo = null;
            this.CargarDropDownList(this.ddlModelo, "IdModelo", "Descripcion", lstBEModelo, true);

            if (this.hfIdModelo.Value != String.Empty)
            {
                this.ddlModelo.SelectedValue = this.hfIdModelo.Value; 
            }
        }

        /// <summary>
        /// Permite cargar todos los modelos de vehiculos y almacenar 
        /// la información en un ViewState.
        /// </summary>
        private void CargarModelo()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLModelo objBLModelo = new BLModelo();
            List<BEModelo> lstBEModelo = objBLModelo.Listar(this.bEstadoReg);
            ViewState[Constantes.ListaModelo] = lstBEModelo;
        }

        /// <summary>
        /// Permite cargar los años de los modelos.
        /// </summary>
        private void CargarAnio()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLAnio objBLAnio = new BLAnio();
            this.CargarDropDownList(this.ddlAniFabricacion, "IdAnio", "descripcion", objBLAnio.Listar(this.bEstadoReg), true);
        }

        /// <summary>
        /// Permite cargar el tipo de documento de identidad.
        /// </summary>
        private void CargarTipoDocumento()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLTipoDocumento objBLTipoDocumento = new BLTipoDocumento();
            this.CargarDropDownList(this.ddlTipDocumento, "idTipoDocumento", "nombre", objBLTipoDocumento.Obtener(), false);

            if (this.ddlTipDocumento.Items.Count > 0)
            {
                this.ddlTipDocumento.SelectedValue = this.cCodDocIdentidad;
            }
        }

        /// <summary>
        /// Permite cargar los departamentos.
        /// </summary>
        private void CargarDepartamento()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLDepartamento objBLDepartamento = new BLDepartamento();
            this.CargarDropDownList(this.ddlDepartamento, "idDepartamento", "nombre", objBLDepartamento.Listar(Constantes.CodPeru), false);

            if (this.ddlDepartamento.Items.Count > 0)
            {
                this.ddlDepartamento.SelectedValue = this.nCodDepartamento.ToString();
            }
        }

        /// <summary>
        /// Permite cargar todas las ciudaddes y almacenar
        /// la información en un ViewState.
        /// </summary>
        private void CargarCiudad()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLCiudad objBLCiudad = new BLCiudad();
            List<BECiudad> lstBECiudad = objBLCiudad.Listar();
            ViewState[Constantes.ListaCiudad] = lstBECiudad;
        }

        /// <summary>
        /// Permite cargar las provincias por departamento.
        /// </summary>
        private void CargarProvincia()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:                
             */

            List<BECiudad> lstBECiudad = ((List<BECiudad>)(ViewState[Constantes.ListaCiudad]));
            BLCiudad objBLCiudad = new BLCiudad();
            this.CargarDropDownList(this.ddlProvincia, "IdProvincia", "Provincia", objBLCiudad.Listar(Convert.ToInt32(this.ddlDepartamento.SelectedValue), lstBECiudad), false);
        }

        /// <summary>
        /// Permite cargar los distritos por provincia.
        /// </summary>
        private void CargarDistrito()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            List<BECiudad> lstBECiudad = ((List<BECiudad>)(ViewState[Constantes.ListaCiudad]));
            BLCiudad objBLCiudad = new BLCiudad();
            this.CargarDropDownList(this.ddlDistrito, "idCiudad", "nombre", objBLCiudad.Listar(Convert.ToInt32(this.ddlDepartamento.SelectedValue), Convert.ToInt32(this.ddlProvincia.SelectedValue), lstBECiudad), false);
        }

        /// <summary>
        /// Permite cargar funciones JavaScript a los controles del
        /// formulario.
        /// </summary>
        private void CargarFuncionesJS()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            this.btnGuaVenta.Attributes.Add("OnClick", "javascript:if(confirm('¿Está seguro de registrar los datos del cliente?')== false) return false;");
            //this.txtNroDocumento.Attributes.Add("onkeypress", "return ClienteAsincrono(event);");
            this.txtNroPlaca.Attributes.Add("onkeypress", "return ClientexNroPlacaAsincrono(event);");
        }
        private void CargarTipoCarroceria()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-11
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLCarroceria objBLCarroceria = new BLCarroceria();
            this.CargarDropDownList(this.ddlTipCarroceria, "IdCarroceria", "Descripcion", objBLCarroceria.Listar(true), true);
        }

        /// <summary>
        /// Permite cargar el tipo de combustible.
        /// </summary>
        private void CargarTipoConbustible()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-11
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLConbustible objBLConbustible = new BLConbustible();
            this.CargarDropDownList(this.ddlTipCombustible, "IdCombustible", "Descripcion", objBLConbustible.Listar(this.bEstadoReg), true);
        }

        /// <summary>
        /// Permite cargar el tipo de mercancia.
        /// </summary>
        private void CargarTipoMercancia()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-11
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            BLMercancia objBLMercancia = new BLMercancia();
            this.CargarDropDownList(this.ddlTipMercancia, "IdMercancia", "Descripcion", objBLMercancia.Listar(this.bEstadoReg), false);

            if (this.ddlTipMercancia.Items.Count > 0)
            {
                this.ddlTipMercancia.SelectedValue = this.nIdMercancia.ToString();
            }
        }
        #endregion        

        #region
        /// <summary>
        /// Permite obtener la información de un cliente
        /// por nro de placa.
        /// </summary>
        /// <param name="pcNroPlaca"></param>
        /// <returns></returns>
        [System.Web.Services.WebMethod]
        public static String ObtenerClietexNroPlaca(String pcNroPlaca)
        {
            String cResult = "";

            BLCliente objBLCliente = new BLCliente();
            BECliente objBECliente = objBLCliente.ObtenerxNroPlaca(pcNroPlaca);

            if (objBECliente != null)
            {
                cResult = objBECliente.PriNombre + "*";
                cResult = cResult + objBECliente.SegNombre + "*";
                cResult = cResult + objBECliente.ApePaterno + "*";
                cResult = cResult + objBECliente.ApeMaterno + "*";
                cResult = cResult + objBECliente.IdTipoDocumento + "*";
                cResult = cResult + objBECliente.NroDocumento + "*";
                cResult = cResult + objBECliente.idCiudad + "*";
                cResult = cResult + objBECliente.Direccion + "*";
                cResult = cResult + objBECliente.IdClase + "*";
                cResult = cResult + objBECliente.IdMarca + "*";
                cResult = cResult + objBECliente.IdModelo + "*";
                //cResult = cResult + objBECliente.IdAnio + "*";
                cResult = cResult + objBECliente.ValorVehiculo + "*";
                cResult = cResult + objBECliente.TelDomicilio + "*";
                cResult = cResult + objBECliente.Email;
            }

            return cResult;
        }
        #endregion

        protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try
            {
                this.CargarProvincia();
                this.CargarDistrito();
                this.ddlProvincia.Focus();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void ddlProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try
            {
                this.CargarDistrito();
                this.ddlDistrito.Focus();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void ddlMarca_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
            * CREADO POR:              Gary Porras Paraguay
            * FECHA DE CREACION:       2011-05-16
            * MODIFICADO POR:          
            * FECHA DE MODIFICACION:   
            */

            try
            {
                List<BEModelo> lstBEModelo = ((List<BEModelo>)(ViewState[Constantes.ListaModelo]));
                BLModelo objBLModelo = new BLModelo();
                this.CargarDropDownList(this.ddlModelo, "IdModelo", "Descripcion", objBLModelo.ListarxMarca(lstBEModelo, Convert.ToInt32(this.ddlMarca.SelectedValue), Convert.ToInt32(this.ddlClase.SelectedValue)), false);

                if (this.hfIdModelo.Value != String.Empty) 
                {
                    this.ddlModelo.SelectedValue = hfIdModelo.Value;
                }
                this.ddlModelo.Focus();
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void btnGuaVenta_Click(object sender, EventArgs e)
        {
            try 
            {
                this.MostrarMensaje(this.Controls, "Se registro la información del cliente", false);
            }
            catch (Exception ex) 
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);                
            }
        }

        protected void btnBusqColor_Click(object sender, EventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try
            {
                if (this.ddlColorBase.SelectedValue != "0")
                {
                    BLColorOpcion objBLColorOpcion = new BLColorOpcion();
                    this.gvBusqColor.DataSource = objBLColorOpcion.Listar(Convert.ToInt32(this.ddlColorBase.SelectedValue), true);
                    this.gvBusqColor.DataBind();
                }
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        protected void gvBusqColor_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-06-14
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes.Add("OnMouseOut", "this.className = this.orignalclassName;");
                    e.Row.Attributes.Add("OnMouseOver", "this.orignalclassName = this.className;this.className = 'GridWiew_SelectedRow';");
                    e.Row.Attributes.Add("onclick", "javascript:OnColorSelected('" + e.Row.Cells[0].Text + "','" + e.Row.Cells[1].Text + "')");
                }
            }
            catch (Exception ex)
            {
                this.MostrarMensaje(this.Controls, ex.Message, false);
            }
        }

        #region Metodos Privados Popup Color
        /// <summary>
        /// Permite cargar el color.
        /// </summary>
        private void CargarColor()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:                
             */

            BLColor objBLColor = new BLColor();
            this.CargarDropDownList(this.ddlColorBase, "IdColor", "Descripcion", objBLColor.Listar(this.bEstadoReg), true);
        }

        /// <summary>
        /// Permite cargar la cabera el el GridView color.
        /// </summary>
        public void CargarCabeceraGvBusqColor()
        {
            /*
             * CREADO POR:              Gary Porras Paraguay
             * FECHA DE CREACION:       2011-05-16
             * MODIFICADO POR:          
             * FECHA DE MODIFICACION:   
             */

            if (this.gvBusqColor.Rows.Count == 0)
            {
                BEColorOpcion objBEColorOpcion = new BEColorOpcion();
                List<BEColorOpcion> lstBEColorOpcion = new List<BEColorOpcion>();
                lstBEColorOpcion.Add(objBEColorOpcion);
                this.gvBusqColor.DataSource = lstBEColorOpcion;
                this.gvBusqColor.DataBind();
                this.gvBusqColor.Rows[0].Visible = false;
            }
        }        
        #endregion             
    }

    public class Siniestro
    {
        private String desc;
        private String fec;
        private String poliza;

        public String Desc 
        {
            get { return desc; }
            set { desc = value; }
        }

        public String Fec 
        {
            get { return fec; }
            set { fec = value; }
        }

        public String Poliza 
        {
            get { return poliza; }
            set { poliza = value; }
        }
    }
}
