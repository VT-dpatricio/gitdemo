﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true" CodeBehind="frmPagTarjeta.aspx.cs" Inherits="AONWebMotor.Pago.frmPagTarjeta" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>
        Pago con Tarjeta de Crédito
    </h1>
    <table border="0" cellpadding="0" cellspacing="0" style="height: 100%; width: 100%;">
        <tr>
            <td>
                Tipo Tarjeta:
            </td>
            <td>
                <asp:DropDownList id="ddlTipTarjeta" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                Nro. Tarjeta:
            </td>
            <td>
                <asp:TextBox id="txtNroTarjeta" runat ="server">
                </asp:TextBox>
            </td>
            <td></td>
            <td></td>            
        </tr>
        <tr>
            <td>
                Fecha Vencimiento:
            </td>
            <td>
                <asp:TextBox id="txtFecVencimiento" runat="server">
                </asp:TextBox>
            </td>
            <td>
                Tipo Id Titular:
            </td>
            <td>
                <asp:DropDownList id="ddlTipTitular" runat="server">
                </asp:DropDownList>
            </td>
            <td></td>
            <td></td>            
        </tr>
        <tr>
            <td>
                Nro. Id Titular:
            </td>
            <td>
                <asp:TextBox id="txtNroIdTitular" runat="server">
                </asp:TextBox>
            </td>
            <td>
                Nombre Titular:
            </td>
            <td>
                <asp:TextBox id="txtNomTitular" runat="server">
                </asp:TextBox>
            </td>
            <td></td>
            <td></td>            
        </tr>
        <tr>
            <td>
                Valor Prima:
            </td>
            <td>
                <asp:TextBox id="txtValPrima" runat="server">
                </asp:TextBox>
            </td>
            <td>
                IVA:
            </td>
            <td>
                <asp:TextBox id="txtIva" runat="server">
                </asp:TextBox>
            </td>
            <td></td>
            <td></td>            
        </tr>
        <tr>
            <td>
                Valor Pago:
            </td>
            <td>
                <asp:TextBox id="txtValPago" runat="server">
                </asp:TextBox>
            </td>
            <td>
                Nro. Autorización:
            </td>
            <td>
               <asp:TextBox id="txtNroAutorizacion" runat="server">
               </asp:TextBox>
            </td>
            <td></td>
            <td></td>            
        </tr>
        <tr>
            <td>
                Fecha Pago:
            </td>
            <td>
                <asp:TextBox id="txtFecPago" runat="server">
                </asp:TextBox>
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>            
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="height: 100%; width: 100%;">
        <tr>
            <td>
                <asp:Button id="btnGuardar" runat="server" Text="Guardar" />
                <asp:Button id="btnPrincipal" runat="server" Text="Principal" />
                <asp:Button id="btnPanTareas" runat="server" Text="Panel Tareas" />
                <asp:Button id="btnAnterior" runat="server" Text="Anterior" />
                <asp:Button id="btnSiguiente" runat="server" Text="Siguiente" />
            </td>
        </tr>
    </table> 
</asp:Content>
