﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true" CodeBehind="frmPagCreFinanciera.aspx.cs" Inherits="AONWebMotor.Pago.frmPagCreFinanciera" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>
        Pago con Crédito Financiera
    </h1>
    <table border="0" cellpadding="0" cellspacing="0" style="height:100%; width:100%;">
        <tr>
            <td>
                Cia de Financiera:
            </td>
            <td>
                <asp:DropDownList id="ddlCiaFinanciera" runat ="server">
                </asp:DropDownList> 
            </td>
            <td>
                Nro. Crédito:
            </td>
            <td>
                <asp:TextBox id="txtNroCredito" runat="server">
                </asp:TextBox>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Valor Crédito
            </td>
            <td>
                <asp:TextBox id="txtValCredito" runat="server" >
                </asp:TextBox>
            </td>
            <td>
                Cuota Inicial:
            </td>
            <td>
                <asp:TextBox id="txtCuoInicial" runat="server">
                </asp:TextBox>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Cuota Periódica:
            </td>
            <td>
                <asp:TextBox id="txtCuoPeriodica" runat="server">
                </asp:TextBox>
            </td>
            <td>
                Tarjeta Fidelización
            </td>
            <td>
                <asp:TextBox id="txtTarFidelizacion" runat="server">
                </asp:TextBox>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                Datos Pago Cuota Inicial
            </td>            
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Banco:
            </td>
            <td>
                <asp:DropDownList id="ddlBanco" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                Sucursal:
            </td>
            <td>
                <asp:TextBox id="txtSucursal" runat="server">
                </asp:TextBox>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Nro. Comprobate:
            </td>
            <td>
                <asp:TextBox id="txtNroComprobante" runat="server">
                </asp:TextBox>
            </td>
            <td>
                Fecha Pago:
            </td>
            <td>
                <asp:TextBox id="txtFecPago" runat="server">
                </asp:TextBox>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>
                Valor Pagado:
            </td>
            <td>
                <asp:TextBox id="txtValPagado" runat="server"></asp:TextBox>
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>        
    </table> 
    <br />
    <table>
        <tr>
            <td>
                <asp:Button id="btnGuardar" runat="server" Text="Guardar" />
                <asp:Button id="btnRegresar" runat="server" Text="Regresar" />
                <asp:Button id="btnPrincipal" runat="server" Text="Principal" />
                <asp:Button id="btnPanTarea" runat="server" Text="Panel Tareas" />
                <asp:Button id="btnAnterior" runat="server" Text="Anterior" />
                <asp:Button id="btnSiguiente" runat="server" Text="Siguiente" />
                <br />
                <br />
            </td>
        </tr>
    </table> 
    <br />  
    <div style="border-style: #E11B22; border-width: #E11B22; border-color: #E11B22;">
    
        gary
    </div> 
</asp:Content>
