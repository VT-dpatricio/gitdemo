﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true" CodeBehind="frmPagEfectivo.aspx.cs" Inherits="AONWebMotor.Pago.frmPagCreEfectivo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>
        Pago en Efectivo
    </h1>
    <table border="0" cellpadding="0" cellspacing="0" style="height: 100%; width: 100%;">
        <tr>
            <td>
                Valor Prima:
            </td>
            <td>
                <asp:TextBox id="txtValPrima" runat="server">
                </asp:TextBox>
            </td>
            <td>
                IVA:
            </td>
            <td>
                <asp:TextBox id="txtIva" runat="server">
                </asp:TextBox>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Valor Pago:
            </td>
            <td>
                <asp:TextBox id="txtValPago" runat="server">
                </asp:TextBox>
            </td>
            <td>
                Nro. Consignación:
            </td>
            <td>
                <asp:TextBox id="txtNroConsignacion" runat="server">
                </asp:TextBox>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Banco:
            </td>
            <td>
                <asp:DropDownList id="ddlBanco" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                Sucursal:
            </td>
            <td>
                <asp:TextBox id="txtSucursal" runat="server"></asp:TextBox>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Fecha Pago:
            </td>
            <td>
                <asp:TextBox id="txtFecPago" runat="server">
                </asp:TextBox>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="height: 100%; width: 100%;">
        <tr>
            <td>
                <asp:Button id="btnGuardar" runat="server" Text="Guardar" />
                <asp:Button id="btnPrincipal" runat="server" Text="Principal" />
                <asp:Button id="btnPanTareas" runat="server" Text="Panel Tareas" />
                <asp:Button id="btnAnterior" runat="server" Text="Anterior" />
                <asp:Button id="btnSiguiente" runat="server" Text="Siguiente" />
            </td>
        </tr>
    </table>            
</asp:Content>
