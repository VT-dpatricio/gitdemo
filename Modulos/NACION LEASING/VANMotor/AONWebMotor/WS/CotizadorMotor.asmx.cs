﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using AONAffinity.Motor.BusinessLogic;
using AONAffinity.Motor.BusinessEntity;

namespace AONWebMotor.WS
{
    /// <summary>
    /// Summary description for CotizadorMotor
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class CotizadorMotor : System.Web.Services.WebService
    {

        [WebMethod]
        public List<BECotizacion> GenerarCotizacion(Int32 pnTipoCot, Int32 pnIdProducto, Int32 pnIdSponsor, Int32 pnIdGrupProd,
                                                    Decimal pnValorVehiculo, Int32 pnIdMarca, Int32 pnIdModelo, Int32 pnAnioFab, Boolean pbTimonCamb, Boolean pbGPS,
                                                    Boolean pbScoring, Decimal pnPorcentValVeh, Int32 pnIdCiudad,
                                                    Int32 pnIdDescuento, Int32 pnIdRecargo, Int32 pnIdAdicional, String pcIdAseguradores,
                                                    String pcParam1, String pcParam2, String pcParam3, String pcParam4, String pcParam5,
                                                    String pcParam6, String pcParam7, String pcParam8, String pcParam9, String pcParam10)
        {
            BLCotizacion objBLCotizacion = new BLCotizacion();
            List<BECotizacion> lstBECotizacion = objBLCotizacion.GenerarCotizacion(pnTipoCot, pnIdProducto, pnIdSponsor, pnIdGrupProd, pnValorVehiculo, pnIdMarca, pnIdModelo, pnAnioFab, pbTimonCamb, pbGPS,
                                                                                    pbScoring, pnPorcentValVeh, pnIdCiudad,
                                                                                    pnIdDescuento, pnIdRecargo, pnIdAdicional, pcIdAseguradores,
                                                                                    pcParam1, pcParam2, pcParam3, pcParam4, pcParam5,
                                                                                    pcParam6, pcParam7, pcParam8, pcParam9, pcParam10);

            if (lstBECotizacion == null)
            {
                lstBECotizacion = new List<BECotizacion>();
            }

            return lstBECotizacion;
        }

        public List<BECotizacionDetalle> ObtenerDetalle(Decimal pnNroCotizacion) 
        {
            BLCotizacion objBLCotizacion = new BLCotizacion();
            List<BECotizacionDetalle> lstBECotizacionDetalle = objBLCotizacion.ObtenerDetalle(pnNroCotizacion);

            if (lstBECotizacionDetalle == null) 
            {
                lstBECotizacionDetalle = new List<BECotizacionDetalle>();
            }

            return lstBECotizacionDetalle;
        } 
    }
}
