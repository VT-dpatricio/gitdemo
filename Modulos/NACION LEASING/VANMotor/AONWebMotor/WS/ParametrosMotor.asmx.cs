﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using AONAffinity.Motor.BusinessLogic;
using AONAffinity.Motor.BusinessEntity;

namespace AONWebMotor.WS
{
    /// <summary>
    /// Descripción breve de ParametrosMotor
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio Web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class ParametrosMotor : System.Web.Services.WebService
    {

        [WebMethod]
        public BEProducto ObtenerProducto(Int32 pnIdProducto)
        {
            BLProducto objBLProducto = new BLProducto();
            BEProducto objBEProducto = objBLProducto.Obtener(pnIdProducto);

            if (objBEProducto == null)
            {
                objBEProducto = new BEProducto();
            }

            return objBEProducto;
        }

        [WebMethod]
        public List<BEProductoMarca> ListarMarcaxProducto(Int32 pnIdProducto) 
        {
            BLProductoMarca objBLProductoMarca = new BLProductoMarca();
            List<BEProductoMarca> lstBEProductoMarca = objBLProductoMarca.ListarxProducto(pnIdProducto, true);

            if (lstBEProductoMarca == null) 
            {
                lstBEProductoMarca = new List<BEProductoMarca>();
            }

            return lstBEProductoMarca;
        }

        [WebMethod]
        public List<BEModelo> ListarxModeloxMarcaTipo(Int32 pnIdProducto, Int32 pnIdMarca, Int32 pnIdTipoVehiculo) 
        {            
            BLModelo objBLModelo = new BLModelo();
            List<BEModelo> lstBEModelo = objBLModelo.ListarxMarcaTipo(pnIdProducto, pnIdMarca, pnIdTipoVehiculo, true);

            if (lstBEModelo == null) 
            {
                lstBEModelo = new List<BEModelo>();
            }

            return lstBEModelo;
        }  

        [WebMethod]
        public List<String> ListarAniosxProducto(Int32 pnIdProducto)
        {
            BLProducto objBLProducto = new BLProducto();
            List<String> lstAnios = objBLProducto.ListarAnios(pnIdProducto);

            if (lstAnios == null)
            {
                lstAnios = new List<String>();
            }

            return lstAnios;
        }

        [WebMethod]
        public List<String> ListarNroAsientos(Int16 pnMaxNroAsientos)
        {
            BLProducto objBLProducto = new BLProducto();
            List<String> lstBEParametro = objBLProducto.ListarNroAsientos(pnMaxNroAsientos);

            if (lstBEParametro == null)
            {
                lstBEParametro = new List<String>();
            }

            return objBLProducto.ListarNroAsientos(pnMaxNroAsientos);
        } 

        [WebMethod]
        public List<BEProductoTipoVehiculo> ListarTipoVehiculoxProducto(Int32 pnIdProducto) 
        {
            BLProductoTipoVehiculo objBLProductoTipoVehiculo = new BLProductoTipoVehiculo();
            List<BEProductoTipoVehiculo> lstBEProductoTipoVehiculo = objBLProductoTipoVehiculo.ListarxProducto(pnIdProducto, true);

            if (lstBEProductoTipoVehiculo == null)
            {
                lstBEProductoTipoVehiculo = new List<BEProductoTipoVehiculo>();
            }

            return lstBEProductoTipoVehiculo;
        }

        [WebMethod]        
        public List<BEProductoUsoVehiculo> ListarUsoVehiculoxProducto(Int32 pnIdProducto)
        {
            BLProductoUsoVehiculo objBLProductoUsoVehiculo = new BLProductoUsoVehiculo ();
            List<BEProductoUsoVehiculo>  lstBEProductoUsoVehiculo = objBLProductoUsoVehiculo.ListarxProducto(pnIdProducto, true);

            if(lstBEProductoUsoVehiculo == null)
            {
                lstBEProductoUsoVehiculo = new List<BEProductoUsoVehiculo>();
            }

            return lstBEProductoUsoVehiculo ;
        }
 
        [WebMethod]
        public List<BEProductoColor> ListarColorxProducto(Int32 pnIdProducto)  
        {
            BLProductoColor objBLProductoColor = new BLProductoColor();
            List<BEProductoColor> lstBEProductoColor = objBLProductoColor.ListarxProducto(pnIdProducto, true);
            
            if (lstBEProductoColor == null) 
            {
                lstBEProductoColor = new List<BEProductoColor>();
            }

            return lstBEProductoColor;
        }

        [WebMethod]
        public BETarifa ObtenerTarifa(Int32 pnIdProducto, Int32 pnIdModelo, Int32 pnAnioFab, Int16 pnNroAsientos) 
        {
            BLTarifa objBLTarifa = new BLTarifa();
            BETarifa objBETarifa = objBLTarifa.Obtener(pnIdProducto, pnIdModelo, pnAnioFab, pnNroAsientos);

            if (objBETarifa == null) 
            {
                objBETarifa = new BETarifa();
                objBETarifa.IdTarifa = -1;
            }

            return objBETarifa;
        }

        [WebMethod]
        public BEProductoTipoCanal ObtenerCanalxProducto(Int32 pnIdProducto, Int32 pnIdCanal) 
        {
            BLProductoTipoCanal objBLProductoTipoCanal = new BLProductoTipoCanal();
            BEProductoTipoCanal objBEProductoTipoCanal = objBLProductoTipoCanal.Obtener(pnIdProducto, pnIdCanal);

            if (objBEProductoTipoCanal == null) 
            {
                objBEProductoTipoCanal = new BEProductoTipoCanal();                 
            }

            return objBEProductoTipoCanal;
        }

        [WebMethod]
        public BECategoriaModelo ObtenerCategoriaxModelo(Int32 pnIdProducto, Int32 pnIdModelo) 
        {
            BLCategoriaModelo objBLCategoriaModelo = new BLCategoriaModelo();
            BECategoriaModelo objBECategoriaModelo = objBLCategoriaModelo.Obtener(pnIdProducto, pnIdModelo);

            if (objBECategoriaModelo == null) 
            {
                objBECategoriaModelo = new BECategoriaModelo();
            }

            return objBECategoriaModelo;
        }


        [WebMethod]
        public List<BERespuesta> ValidarVehiculo(Int32 pnIdProducto, Int32 pnIdModelo, Boolean pbTimonOriginal, Decimal pnValorVehiculo, Int32 pnAnioFab, Int16 pnNroAsientos, Int32 pnidTipoVehiculo, Int32 pnIdUsoVehiculo, Boolean pbGPS, Boolean pbGas, Int32 pnIdCiudad, Int32 pnIdCiudadInsp)
        {
            BLCotizacion objBLCotizacion = new BLCotizacion();
            List<BERespuesta> lstBERespuesta = objBLCotizacion.ValidarVehiculo(pnIdProducto, pnIdModelo, pbTimonOriginal, pnValorVehiculo, pnAnioFab, pnNroAsientos, pnidTipoVehiculo, pnIdUsoVehiculo, pbGPS, pbGas, pnIdCiudad, pnIdCiudadInsp);

            if (lstBERespuesta == null)
            {
                lstBERespuesta = new List<BERespuesta>();
            }

            return lstBERespuesta;
        }

        [WebMethod]
        public BEProductoPoliza ObtenerNroPorliza(Int32 pnIdProducto, String pcOpcionPlan, String pcParametro) 
        {
            BLProductoPoliza objBLProductoPoliza = new BLProductoPoliza();
            BEProductoPoliza objBEProductoPoliza = objBLProductoPoliza.Obtener(pnIdProducto, pcOpcionPlan, pcParametro);

            if (objBEProductoPoliza == null) 
            {
                objBEProductoPoliza = new BEProductoPoliza();
            }

            return objBEProductoPoliza;
        }

        [WebMethod]
        public List<BERespuesta> ValidarNumeroCertificado(Int32 pnIdProducto, Int32 pnIdCanal, Decimal pnNumCertificado) 
        {
            AONAffinity.Motor.BusinessLogic.Bais.BLCertificado objBLCertificado = new AONAffinity.Motor.BusinessLogic.Bais.BLCertificado();
            List<BERespuesta> lstBERespuesta = objBLCertificado.ValidarNumCertificado(pnIdProducto, pnIdCanal, pnNumCertificado);

            if (lstBERespuesta == null) 
            {
                lstBERespuesta = new List<BERespuesta>();
            }

            return lstBERespuesta;
        }  
    }
}
