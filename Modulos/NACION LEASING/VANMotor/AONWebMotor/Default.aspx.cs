﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AONAffinity.Resources;
using AONAffinity.Motor.Resources;  
using AONAffinity.Motor.BusinessLogic.Bais;
using AONAffinity.Motor.BusinessEntity.Bais;  

namespace AONWebMotor.Master
{
    public partial class WebForm1 : PaginaBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (ValidarEstadoClaveUsuario())
                {
                    Response.Redirect("~/Seguridad/CambiarClave.aspx", false);                    
                }
                            
                try
                {
                    lblNombreUsuario.Text = Session["NombreUsuario"].ToString();
                }
                catch (Exception ex)
                {
                    Response.Redirect("~/Login.aspx");
                }

            }
            
            //try
            //{
            //    if (!Page.IsPostBack)
            //    {
            //        this.CargarFormulario();
            //    }
            //}
            //catch (Exception ex) 
            //{
            //    this.MostrarMensaje(this.Controls, "Se presentó un problema al cargar el formulario." + ex.Message, false);
            //}
        }

        #region Métodos
        public void CargarFormulario() 
        {
            Page.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            this.CargarDatoUsuario();
        }

        public void CargarDatoUsuario() 
        {
            BLUsuario objBLUsuario = new BLUsuario();
            BEUsuario objBEUsuario = objBLUsuario.Obtener(Session[NombreSession.Usuario].ToString());

            if (objBEUsuario != null) 
            {
                this.lblNombreUsuario.Text = objBEUsuario.Nombre1 + " " + objBEUsuario.Apellido1 + ","; 
            }            
        }
        #endregion
    }
}
