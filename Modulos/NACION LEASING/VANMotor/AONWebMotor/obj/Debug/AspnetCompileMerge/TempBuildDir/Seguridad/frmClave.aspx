﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true"
    CodeBehind="frmClave.aspx.cs" Inherits="AONWebMotor.Seguridad.frmClave" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="Form_SubTitulo" colspan="6">
                            CAMBIAR CLAVE
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                        </td>
                        <td class="Form_TextoIzq">
                        </td>
                        <td class="Form_TextoDer">
                            Clave Actual:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="TextBox1" runat="server" CssClass="Form_TextBox"></asp:TextBox>
                            <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoDer">
                        </td>
                        <td class="Form_TextoIzq">
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                        </td>
                        <td class="Form_TextoIzq">
                        </td>
                        <td class="Form_TextoDer">
                            Nueva Clave:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="TextBox2" runat="server" CssClass="Form_TextBox"></asp:TextBox>
                            <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoDer">
                        </td>
                        <td class="Form_TextoIzq">
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                        </td>
                        <td class="Form_TextoIzq">
                        </td>
                        <td class="Form_TextoDer">
                            Repetir Nueva 
                            Clave:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="TextBox3" runat="server" CssClass="Form_TextBox"></asp:TextBox>
                            <span class="Form_TextoObligatorio">*</span>
                        </td>
                        <td class="Form_TextoDer">
                        </td>
                        <td class="Form_TextoIzq">
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                        </td>
                        <td class="Form_TextoIzq">
                        </td>
                        <td class="Form_TextoDer">
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:Button ID="btnUsuario" runat="server" Text="Cambiar" />
                        </td>
                        <td class="Form_TextoDer">
                        </td>
                        <td class="Form_TextoIzq">
                        </td>
                    </tr>                   
                </table>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
</asp:Content>
