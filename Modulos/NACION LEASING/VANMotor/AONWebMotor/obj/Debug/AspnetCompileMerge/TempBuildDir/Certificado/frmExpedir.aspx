﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true"
    CodeBehind="frmExpedir.aspx.cs" Inherits="AONWebMotor.Certificado.frmExpedir" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Controles/MsgBox.ascx" TagName="MsgBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
        function SolicitarCuenta() {
            if (document.getElementById('<%=cbxSolCuenta.ClientID %>').checked) {
                document.getElementById('<%=ddlMedPago.ClientID %>').disabled = false;
                document.getElementById('<%=txtNroCuenta.ClientID %>').disabled = false;
                document.getElementById('<%=ddlMonCuenta.ClientID %>').disabled = false;
                document.getElementById('<%=txtFecVencimiento.ClientID %>').disabled = false;
            }
            else {
                document.getElementById('<%=ddlMedPago.ClientID %>').disabled = true;
                document.getElementById('<%=txtNroCuenta.ClientID %>').disabled = true;
                document.getElementById('<%=ddlMonCuenta.ClientID %>').disabled = true;
                document.getElementById('<%=txtFecVencimiento.ClientID %>').disabled = true;
            }
        }
        function ValidaTipoMedioPago(objCombo) {
            if (objCombo.value == 'TA') {
                document.getElementById('<%=txtFecVencimiento.ClientID %>').disabled = false;
            }
            else if (objCombo.value == 'TM') {
                document.getElementById('<%=txtFecVencimiento.ClientID %>').disabled = false;
            }
            else if (objCombo.value == 'TV') {
                document.getElementById('<%=txtFecVencimiento.ClientID %>').disabled = false;
            }
            else {
                document.getElementById('<%=txtFecVencimiento.ClientID %>').disabled = true;
            }
        }        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="Form_SubTitulo" colspan="6">
                            OPCIONES
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer" style="width: 150px">
                            Cambiar Valor Vehículo:
                        </td>
                        <td class="Form_TextoIzq" style="width: 50px">
                            <asp:DropDownList ID="ddlSigno" runat="server" Width="40px" AutoPostBack="True" OnSelectedIndexChanged="ddlSigno_SelectedIndexChanged">
                                <asp:ListItem Text="" Value="-1"></asp:ListItem>
                                <asp:ListItem Text="+" Value="+"></asp:ListItem>
                                <asp:ListItem Text="-" Value="-"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvSigno" runat="server" Text="*" ControlToValidate="ddlSigno"
                                Display="None" ErrorMessage="Seleccione signo de porcentaje" SetFocusOnError="true"
                                InitialValue="-1" ValidationGroup="CambiarValor">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:UpdatePanel ID="upSigno" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlPorcentaje" runat="server" Width="40px">
                                    </asp:DropDownList>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlSigno" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                        <td>
                            <asp:Button ID="btnCambiarValVeh" runat="server" Text="Cambiar" ValidationGroup="CambiarValor"
                                OnClick="btnCambiarValVeh_Click" />
                            <asp:Button ID="btnValorOri" runat="server" Text="Valor Original" OnClick="btnValorOri_Click" />
                        </td>
                        <td>
                        </td>
                        <td style="text-align: right;">
                            <asp:Button ID="btnVerAgenda" runat="server" Text="Ver Agenda" Visible="false" OnClientClick="return showModalPopup('bmpeVerAgenda');" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                <div style="width: 100%; height: 380px; overflow: auto;">
                    <rsweb:ReportViewer ID="rvCotizacion" runat="server" ShowPrintButton="False" Width="100%"
                        Font-Names="Verdana" Font-Size="8pt" Height="350px" ZoomPercent="130" ShowExportControls="False"
                        ShowZoomControl="False" ShowRefreshButton="False" BackColor="#CCCCCC">
                        <LocalReport ReportPath="rdlc\rdlcScriptVenta.rdlc">
                            <DataSources>
                                <rsweb:ReportDataSource DataSourceId="odsCotizacion" Name="BECotizacion" />
                            </DataSources>
                        </LocalReport>
                    </rsweb:ReportViewer>
                    <asp:ObjectDataSource ID="odsCotizacion" runat="server" SelectMethod="ObtenerSpeech"
                        TypeName="AONAffinity.Motor.BusinessLogic.BLCotizacion">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="hfIdPeriodo" DefaultValue="0" Name="pnIdPeriodo"
                                PropertyName="Value" Type="Int32" />
                            <asp:ControlParameter ControlID="hfNroCotizacion" DefaultValue="0" Name="pnNroCotizacion"
                                PropertyName="Value" Type="Decimal" />
                            <asp:ControlParameter ControlID="hfSaludo" DefaultValue="" Name="pcSaludo" PropertyName="Value"
                                Type="String" />
                            <asp:ControlParameter ControlID="hfCond" DefaultValue="" Name="pcCondicion" PropertyName="Value"
                                Type="String" />
                            <asp:ControlParameter ControlID="hfConf" DefaultValue="" Name="pcConfirmacion" PropertyName="Value"
                                Type="String" />
                            <asp:ControlParameter ControlID="hfValVehiculo" DefaultValue="" Name="pcValVehiculo"
                                PropertyName="Value" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </div>
                <br />
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td>
                            <div class="Form_RegButtonDer">
                                <asp:Button ID="btnAceptar" runat="server" Text="Acepta" OnClick="btnAceptar_Click" />
                                <asp:Button ID="btnImprimir" runat="server" Text="Imprimir" Visible="false" OnClientClick="return showModalPopup('bmpCotDocCertificado');" />
                                <asp:Button ID="btnCotRechazo" runat="server" Text="Opciones" OnClientClick="return showModalPopup('bmpeCotRechazo');" />
                                <asp:Button ID="btnCotMail" runat="server" Text="Enviar Mail" OnClientClick="return showModalPopup('bmpeCotMail');" />
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <uc1:MsgBox ID="msgBox" runat="server" />
    <asp:ValidationSummary ID="vsCambiarValor" runat="server" ValidationGroup="CambiarValor"
        ShowMessageBox="true" ShowSummary="false" />
    <div style="display: none;">
        <asp:HiddenField ID="hfIdCertificado" runat="server" />
        <asp:HiddenField ID="hfIdPeriodo" runat="server" />
        <asp:HiddenField ID="hfNroCotizacion" runat="server" />
        <asp:HiddenField ID="hfSaludo" runat="server" />
        <asp:HiddenField ID="hfCond" runat="server" />
        <asp:HiddenField ID="hfConf" runat="server" />
        <asp:HiddenField ID="hfValVehiculo" runat="server" />
        <asp:HiddenField ID="hfEstForm" runat="server" />
        <asp:HiddenField ID="hfIdProducto" runat="server" />
        <asp:HiddenField ID="hfDatoCliente" runat="server" />
        <asp:HiddenField ID="hfDatoVehiculo" runat="server" />
        <asp:Button ID="btnPostBack" runat="server" Text="PostBack" OnClick="btnPostBack_Click" />
    </div>
    <!--Popup Cotizacion Mail-->
    <asp:Panel ID="pnMail" runat="server" Width="400px" CssClass="Modal_Panel" DefaultButton="btnEnvCotMail"
        Style="display: none">
        <div class="Modal_Head" style="width: 100%;">
            <div id="divHeadTitulo" style="float: left;">
                Enviar Mail
            </div>
            <div id="divHeadCerrar" style="float: right;">
                <img alt="Click para cerrar." class="Modal_CloseButton" onclick="hideModalPopup('bmpeCotMail');"
                    style="cursor: hand;" />
            </div>
        </div>
        <div id="divBodyMail" class="Modal_Body">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="Form_TextoIzq" style="width: 60px;">
                        <asp:Image ID="imgSpeechMail" runat="server" SkinID="sknImgUsuario" />
                    </td>
                    <td class="Form_Nota">
                        <asp:Label ID="lblSpeechMail" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            <br />
            <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="Form_TextoDer">
                        Email: <span class="Form_TextoObligatorio">*</span>
                    </td>
                    <td class="Form_TextoIzq">
                        <asp:TextBox ID="txtEmail" runat="server" Width="300px" CssClass="Form_TextBox"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                            Display="None" SetFocusOnError="true" ErrorMessage="Ingrese mail." ValidationGroup="Email">
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                            Display="None" SetFocusOnError="true" ErrorMessage="Email tiene formato incorrecto."
                            ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Email">
                        </asp:RegularExpressionValidator>
                    </td>
                    <td class="Form_TextoIzq" style="width: 16px">
                        <asp:UpdateProgress ID="upsCotMail" runat="server" AssociatedUpdatePanelID="upCotMail"
                            DisplayAfter="0">
                            <ProgressTemplate>
                                <asp:Image ID="imgLoadCotMail" runat="server" SkinID="sknImgLoading" Width="16px"
                                    Height="16px" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
                <tr>
                    <td class="Form_TextoDer">
                        Cc:
                    </td>
                    <td class="Form_TextoIzq">
                        <asp:TextBox ID="txtEmailCc" runat="server" Width="300px" CssClass="Form_TextBox"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revEmailCc" runat="server" ControlToValidate="txtEmailCc"
                            Display="None" SetFocusOnError="true" ErrorMessage="Email de copia tiene formato incorrecto."
                            ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Email">
                        </asp:RegularExpressionValidator>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="Form_TextoIzq" colspan="3">
                    </td>
                </tr>
            </table>
            <br />
            <div style="width: 100%; text-align: center;">
                <asp:UpdatePanel ID="upCotMail" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnEnvCotMail" runat="server" Text="Enviar" ValidationGroup="Email"
                            CausesValidation="true" OnClick="btnEnvCotMail_Click" />
                        <br />
                        <asp:Label ID="lblResCotMail" runat="server" ForeColor="Red"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnEnvCotMail" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <div>
                <span class="Form_TextoObligatorio">&nbsp;(*)Campos Obligatorios</span>
                <asp:ValidationSummary ID="vsMail" runat="server" ValidationGroup="Email" ShowMessageBox="true"
                    ShowSummary="false" />
            </div>
        </div>
    </asp:Panel>
    <asp:Label ID="lblCotMail" runat="server" Style="display: none;"></asp:Label>
    <cc1:ModalPopupExtender ID="mpeCotMail" runat="server" Enabled="true" TargetControlID="lblCotMail"
        BehaviorID="bmpeCotMail" PopupControlID="pnMail" BackgroundCssClass="Modal_Background">
    </cc1:ModalPopupExtender>
    <!--Popup Cotizacion Rechazo-->
    <asp:Panel ID="pnCotRechazo" runat="server" Width="450px" CssClass="Modal_Panel"
        Style="display: none;">
        <div class="Modal_Head" style="width: 100%;">
            <div id="div1" style="float: left;">
                Opciones
            </div>
            <div id="div2" style="float: right;">
                <img alt="Click para cerrar." class="Modal_CloseButton" onclick="hideModalPopup('bmpeCotRechazo')"
                    style="cursor: hand;" />
            </div>
        </div>
        <div id="div3" class="Modal_Body">
            <cc1:TabContainer ID="tcopcion" runat="server" ActiveTabIndex="0">
                <cc1:TabPanel ID="tpOpcion1" runat="server">
                    <HeaderTemplate>
                        Rechazo Cotización
                    </HeaderTemplate>
                    <ContentTemplate>
                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td class="Form_TextoIzq" style="width: 60px;">
                                    <asp:Image ID="imgSpeechRec" runat="server" SkinID="sknImgUsuario" />
                                </td>
                                <td class="Form_Nota">
                                    <asp:Label ID="lblSpeechRec" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td class="Form_TextoDer">
                                    Tipo: <span class="Form_TextoObligatorio">*</span>
                                </td>
                                <td class="Form_TextoIzq">
                                    <asp:DropDownList ID="ddlTipMotRechazo" runat="server" Width="315px">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvTipMotRechazo" runat="server" ErrorMessage="Seleccione el tipo de rechazo."
                                        Display="None" ControlToValidate="ddlTipMotRechazo" SetFocusOnError="True" InitialValue="-1"
                                        ValidationGroup="CotizacionRechazo"></asp:RequiredFieldValidator>
                                </td>
                                <td class="Form_TextoIzq">
                                </td>
                            </tr>
                            <tr>
                                <td class="Form_TextoDer" style="vertical-align: top;">
                                    Motivo:
                                </td>
                                <td class="Form_TextoIzq" colspan="2">
                                    <asp:TextBox ID="txtMotRechazo" runat="server" Width="320px" TextMode="MultiLine"
                                        MaxLength="350" CssClass="Form_TextBox" Height="100px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <div style="width: 100%; text-align: center;">
                            <asp:Button ID="btnRegCotRechazo" runat="server" Text="Registrar" ValidationGroup="CotizacionRechazo"
                                OnClick="btnRegCotRechazo_Click" />
                        </div>
                        <br />
                        <span class="Form_TextoObligatorio">&nbsp;(*)Campos Obligatorios</span>
                        <uc1:MsgBox ID="msgRechazo" runat="server" />
                        <asp:ValidationSummary ID="vsCotRechazo" runat="server" ValidationGroup="CotizacionRechazo"
                            ShowMessageBox="True" ShowSummary="False" />
                    </ContentTemplate>
                </cc1:TabPanel>
                <cc1:TabPanel ID="tpOpcion2" runat="server">
                    <HeaderTemplate>
                        Agendar Cotización
                    </HeaderTemplate>
                    <ContentTemplate>
                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td class="Form_TextoIzq" style="width: 60px;">
                                    <asp:Image ID="imgSpeechAge" runat="server" SkinID="sknImgUsuario" />
                                </td>
                                <td class="Form_Nota">
                                    <asp:Label ID="lblSpeechAge" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td class="Form_TextoDer">
                                    Fecha: <span class="Form_TextoObligatorio">*</span>
                                </td>
                                <td class="Form_TextoIzq">
                                    <asp:TextBox ID="txtFechaAge" runat="server" Width="120px" CssClass="Form_TextBox"></asp:TextBox>
                                    <cc1:MaskedEditExtender ID="meeFecAge" runat="server" TargetControlID="txtFechaAge"
                                        Mask="99/99/9999" MaskType="Date" Enabled="True" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                        CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                        CultureTimePlaceholder="">
                                    </cc1:MaskedEditExtender>
                                    <asp:RegularExpressionValidator ID="revFechaAge" runat="server" ControlToValidate="txtFechaAge"
                                        Display="None" SetFocusOnError="True" ErrorMessage="Fecha tiene formato incorrecto."
                                        ValidationExpression="(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"
                                        ValidationGroup="Agenda"></asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator ID="rfvFecha" runat="server" ControlToValidate="txtFechaAge"
                                        ErrorMessage="Fecha Obligatorio." SetFocusOnError="True" Display="None" ValidationGroup="Agenda"></asp:RequiredFieldValidator>
                                </td>
                                <td class="Form_TextoDer">
                                    Hora: <span class="Form_TextoObligatorio">*</span>
                                </td>
                                <td class="Form_TextoIzq">
                                    <asp:TextBox ID="txtHoraAge" runat="server" Width="80px" CssClass="Form_TextBox"></asp:TextBox>
                                    <cc1:MaskedEditExtender ID="meeHoraAge" runat="server" TargetControlID="txtHoraAge"
                                        Mask="99:99" MaskType="Time" AcceptAMPM="True" Enabled="True" CultureAMPMPlaceholder=""
                                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                                        CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="">
                                    </cc1:MaskedEditExtender>
                                    <asp:RequiredFieldValidator ID="rfvHoraAge" runat="server" ControlToValidate="txtHoraAge"
                                        ErrorMessage="Hora obligatorio." SetFocusOnError="True" Display="None" ValidationGroup="Agenda"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="Form_TextoDer" style="vertical-align: top;">
                                    Detalle:
                                </td>
                                <td class="Form_TextoIzq" colspan="3">
                                    <asp:TextBox ID="txtObsAgenda" runat="server" Width="320px" TextMode="MultiLine"
                                        MaxLength="300" CssClass="Form_TextBox" Height="100px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <div style="width: 100%; text-align: center;">
                        </div>
                        <br />
                        <span class="Form_TextoObligatorio">&nbsp;(*)Campos Obligatorios</span>
                        <uc1:MsgBox ID="msgAgenda" runat="server" />
                        <asp:ValidationSummary ID="vsAgenda" runat="server" ValidationGroup="Agenda" ShowMessageBox="True"
                            ShowSummary="False" />
                    </ContentTemplate>
                </cc1:TabPanel>
            </cc1:TabContainer>
        </div>
    </asp:Panel>
    <asp:Label ID="lblCotRechazo" runat="server" Style="display: none;"></asp:Label>
    <cc1:ModalPopupExtender ID="mpeCotRechazo" runat="server" Enabled="true" TargetControlID="lblCotRechazo"
        BehaviorID="bmpeCotRechazo" PopupControlID="pnCotRechazo" BackgroundCssClass="Modal_Background">
    </cc1:ModalPopupExtender>
    <!--Popup Cotizacion Certificado-->
    <asp:Panel ID="pnCertificado" runat="server" Width="900px" CssClass="Modal_Panel"
        Style="display: none;">
        <div class="Modal_Head" style="width: 100%;">
            <div id="div4" style="float: left;">
                Registrar Certificado
            </div>
            <div id="div5" style="float: right;">
                <img alt="Click para cerrar." class="Modal_CloseButton" onclick="hideModalPopup('bmpeCotCertificado');"
                    style="cursor: hand;" />
            </div>
        </div>
        <div id="div6" class="Modal_Body">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td class="Form_tdBorde">
                    </td>
                    <td>
                        <cc1:TabContainer ID="tbCotizacion" runat="server" ActiveTabIndex="0">
                            <cc1:TabPanel ID="tpCliente" runat="server">
                                <HeaderTemplate>
                                    Datos del Cliente
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="Form_TextoIzq" style="width: 60px;">
                                                <asp:Image ID="imgSpeechCli" runat="server" SkinID="sknImgUsuario" />
                                            </td>
                                            <td class="Form_Nota">
                                                <asp:Label ID="lblSpecchCli" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="Form_SubTitulo" colspan="6">
                                                INFORMACIÓN DEL CLIENTE
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Form_TextoDer">
                                                Nombres: <span class="Form_TextoObligatorio">*</span>
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:TextBox ID="txtNombre1" runat="server" CssClass="Form_TextBox" MaxLength="60"
                                                    Width="120px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvNombre1" runat="server" ControlToValidate="txtNombre1"
                                                    ValidationGroup="ActCliente" Display="None" ErrorMessage="Nombre Obligatorio."
                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="fteNombre1" runat="server" Enabled="True" TargetControlID="txtNombre1"
                                                    ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnñopqrstuvwxyzÁÉÍÓÚÜáéíóúü">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:TextBox ID="txtNombre2" runat="server" CssClass="Form_TextBox" MaxLength="60"
                                                    Width="120px"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="fteNombre2" runat="server" Enabled="True" TargetControlID="txtNombre2"
                                                    ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnñopqrstuvwxyzÁÉÍÓÚÜáéíóúü">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td class="Form_TextoDer">
                                                Apellidos <span class="Form_TextoObligatorio">*</span>
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:TextBox ID="txtApellidoPat" runat="server" CssClass="Form_TextBox" MaxLength="60"
                                                    Width="120px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvApellidoPat" runat="server" ControlToValidate="txtApellidoPat"
                                                    ValidationGroup="ActCliente" Display="None" ErrorMessage="Apellido Paterno Obligatorio."
                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="fteApellidoPat" runat="server" Enabled="True" TargetControlID="txtApellidoPat"
                                                    ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnñopqrstuvwxyzÁÉÍÓÚÜáéíóúü">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:TextBox ID="txtApellidoMat" runat="server" CssClass="Form_TextBox" MaxLength="60"
                                                    Width="120px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvApellidoMat" runat="server" ControlToValidate="txtApellidoMat"
                                                    ValidationGroup="ActCliente" Display="None" ErrorMessage="Apellido Materno Obligatorio."
                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="fteApellidoMat" runat="server" Enabled="True" TargetControlID="txtApellidoMat"
                                                    ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnñopqrstuvwxyzÁÉÍÓÚÜáéíóúü">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Form_TextoDer">
                                                Tipo y Nro. Doc.: <span class="Form_TextoObligatorio">*</span>
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:DropDownList ID="ddlTipDocumento" runat="server" Width="120px">
                                                    <asp:ListItem Text="DNI" Value="L"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:TextBox ID="txtNroDocumento" runat="server" CssClass="Form_TextBox" MaxLength="8"
                                                    Width="120px"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="fteNroDocumento" runat="server" Enabled="True" TargetControlID="txtNroDocumento"
                                                    ValidChars="0123456789">
                                                </cc1:FilteredTextBoxExtender>
                                                <asp:RequiredFieldValidator ID="rfvNroDocumento" runat="server" ControlToValidate="txtNroDocumento"
                                                    ValidationGroup="ActCliente" Display="None" ErrorMessage="Nro. Documento Obligatorio."
                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                            </td>
                                            <td class="Form_TextoDer">
                                                Dirección: <span class="Form_TextoObligatorio">*</span>
                                            </td>
                                            <td class="Form_TextoIzq" colspan="2">
                                                <asp:TextBox ID="txtDireccion" runat="server" MaxLength="300" Width="260px" CssClass="Form_TextBox"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvDireccion" runat="server" ControlToValidate="txtDireccion"
                                                    ValidationGroup="ActCliente" Display="None" ErrorMessage="Dirección Obligatorio."
                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Form_TextoDer">
                                                Departamento: <span class="Form_TextoObligatorio">*</span>
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:UpdatePanel ID="upDepartamento" runat="server">
                                                    <ContentTemplate>
                                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                            <tr>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlDepartamento" runat="server" Width="125px" AutoPostBack="True"
                                                                        OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="rfvDepartamento" runat="server" Text="*" ControlToValidate="ddlDepartamento"
                                                                        Display="None" ErrorMessage="Seleccione departamento." ForeColor="Transparent"
                                                                        SetFocusOnError="true" InitialValue="-1" ValidationGroup="ActCliente">
                                                                    </asp:RequiredFieldValidator>
                                                                </td>
                                                                <td>
                                                                    <asp:UpdateProgress ID="upsDepartamento" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upDepartamento">
                                                                        <ProgressTemplate>
                                                                            <asp:Image ID="imgLoadDep" runat="server" SkinID="sknImgLoading" Width="14px" Height="14px" />
                                                                        </ProgressTemplate>
                                                                    </asp:UpdateProgress>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td class="Form_TextoDer">
                                                Provincia: <span class="Form_TextoObligatorio">*</span>
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:UpdatePanel ID="upProvincia" runat="server">
                                                    <ContentTemplate>
                                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                            <tr>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlProvincia" runat="server" Width="125px" AutoPostBack="True"
                                                                        OnSelectedIndexChanged="ddlProvincia_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="rfvProvincia" runat="server" Text="*" ControlToValidate="ddlProvincia"
                                                                        Display="None" ErrorMessage="Seleccione provincia." ForeColor="Transparent" SetFocusOnError="true"
                                                                        InitialValue="-1" ValidationGroup="ActCliente">
                                                                    </asp:RequiredFieldValidator>
                                                                </td>
                                                                <td>
                                                                    <asp:UpdateProgress ID="upsProvincia" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upProvincia">
                                                                        <ProgressTemplate>
                                                                            <asp:Image ID="imgLoadProv" runat="server" SkinID="sknImgLoading" Width="14px" Height="14px" />
                                                                        </ProgressTemplate>
                                                                    </asp:UpdateProgress>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddlDepartamento" EventName="selectedIndexChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td class="Form_TextoDer">
                                                Distrito: <span class="Form_TextoObligatorio">*</span>
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:UpdatePanel ID="upDistrito" runat="server">
                                                    <ContentTemplate>
                                                        <asp:DropDownList ID="ddlDistrito" runat="server" Width="125px">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="rfvDistrito" runat="server" ControlToValidate="ddlDistrito"
                                                            Display="None" ErrorMessage="Seleccione distrito." ForeColor="Transparent" InitialValue="-1"
                                                            SetFocusOnError="true" Text="*" ValidationGroup="ActCliente">
                                                        </asp:RequiredFieldValidator>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddlProvincia" EventName="selectedIndexChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Form_TextoDer">
                                                Tel. Domicilio:
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:TextBox ID="txtTelDomicio1" runat="server" MaxLength="15" Width="120px" CssClass="Form_TextBox"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="fteTelDomicio1" runat="server" Enabled="True" TargetControlID="txtTelDomicio1"
                                                    ValidChars="0123456789*-">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td class="Form_TextoDer">
                                                Tel. Domicilio:
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:TextBox ID="txtTelDomicio2" runat="server" MaxLength="15" Width="120px" CssClass="Form_TextBox"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="fteTelDomicio2" runat="server" Enabled="True" TargetControlID="txtTelDomicio2"
                                                    ValidChars="0123456789*-">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td class="Form_TextoDer">
                                                Tel. Domicilio:
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:TextBox ID="txtTelDomicio3" runat="server" MaxLength="15" Width="120px" CssClass="Form_TextBox"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="fteTelDomicio3" runat="server" Enabled="True" TargetControlID="txtTelDomicio3"
                                                    ValidChars="0123456789*-">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Form_TextoDer">
                                                Tel. Movil:
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:TextBox ID="txtTelMovil1" runat="server" MaxLength="15" Width="120px" CssClass="Form_TextBox"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="fteTelMovil1" runat="server" Enabled="True" TargetControlID="txtTelMovil1"
                                                    ValidChars="0123456789*-">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td class="Form_TextoDer">
                                                Tel. Movil:
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:TextBox ID="txtTelMovil2" runat="server" MaxLength="15" Width="120px" CssClass="Form_TextBox"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="fteTelMovil2" runat="server" Enabled="True" TargetControlID="txtTelMovil2"
                                                    ValidChars="0123456789*-">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td class="Form_TextoDer">
                                                Tel. Movil:
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:TextBox ID="txtTelMovil3" runat="server" MaxLength="15" Width="120px" CssClass="Form_TextBox"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="fteTelMovil3" runat="server" Enabled="True" TargetControlID="txtTelMovil3"
                                                    ValidChars="0123456789*-">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Form_TextoDer">
                                                Tel. Oficina:
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:TextBox ID="txtTelOficina1" runat="server" MaxLength="15" Width="120px" CssClass="Form_TextBox"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="ftxTelOficina1" runat="server" Enabled="True" TargetControlID="txtTelOficina1"
                                                    ValidChars="0123456789*-">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td class="Form_TextoDer">
                                                Tel. Oficina:
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:TextBox ID="txtTelOficina2" runat="server" MaxLength="15" Width="120px" CssClass="Form_TextBox"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="fteTelOficina2" runat="server" Enabled="True" TargetControlID="txtTelOficina2"
                                                    ValidChars="0123456789*-">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td class="Form_TextoDer">
                                                Tel. Oficina:
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:TextBox ID="txtTelOficina3" runat="server" MaxLength="15" Width="120px" CssClass="Form_TextBox"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="fteTelOficina1" runat="server" Enabled="True" TargetControlID="txtTelOficina3"
                                                    ValidChars="0123456789*-">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Form_TextoDer">
                                                E-Mail:
                                            </td>
                                            <td class="Form_TextoIzq" colspan="2">
                                                <asp:TextBox ID="txtEmail1" runat="server" CssClass="Form_TextBox" MaxLength="50"
                                                    Width="260px"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="revEmail1" runat="server" ControlToValidate="txtEmail1"
                                                    Display="None" ErrorMessage="Email tiene formato incorrecto." SetFocusOnError="True"
                                                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="ActCliente"></asp:RegularExpressionValidator>
                                            </td>
                                            <td class="Form_TextoDer">
                                                E-Mail:
                                            </td>
                                            <td class="Form_TextoIzq" colspan="2">
                                                <asp:TextBox ID="txtEmail2" runat="server" CssClass="Form_TextBox" MaxLength="50"
                                                    Width="260px"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="revEmail2" runat="server" ControlToValidate="txtEmail2"
                                                    Display="None" ErrorMessage="Email tiene formato incorrecto." SetFocusOnError="True"
                                                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="ActCliente"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <asp:Panel ID="pnInspeccion" runat="server">
                                        <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td class="Form_SubTitulo" colspan="6">
                                                    Información para Inspección Vehícular
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Form_TextoDer" style="width: 100px;">
                                                    Fecha:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtFecInsp" runat="server" CssClass="Form_TextBox" Width="60px"></asp:TextBox>
                                                    <cc1:MaskedEditExtender ID="meeFecInsp" runat="server" CultureAMPMPlaceholder=""
                                                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                                                        CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                                                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFecInsp">
                                                    </cc1:MaskedEditExtender>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Hora:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtHoraInsp" runat="server" CssClass="Form_TextBox" Width="60px"></asp:TextBox>
                                                    <cc1:MaskedEditExtender ID="meeHora" runat="server" AcceptAMPM="True" CultureAMPMPlaceholder=""
                                                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                                                        CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                                                        Enabled="True" Mask="99:99" MaskType="Time" TargetControlID="txtHoraInsp">
                                                    </cc1:MaskedEditExtender>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    Dirección:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtDireccionInsp" runat="server" CssClass="Form_TextBox" Width="350px"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:UpdatePanel ID="upActCliente" runat="server">
                                        <ContentTemplate>
                                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                <tr>
                                                    <td>
                                                        <span class="Form_TextoObligatorio">&nbsp;(*)Campos Obligatorios</span>
                                                    </td>
                                                    <td>
                                                        <asp:UpdateProgress ID="upsActCliente" runat="server" AssociatedUpdatePanelID="upActCliente"
                                                            DisplayAfter="0">
                                                            <ProgressTemplate>
                                                                <asp:Image ID="imgLoadActCliente" runat="server" SkinID="sknImgLoading" Width="16px"
                                                                    Height="16px" /></ProgressTemplate>
                                                        </asp:UpdateProgress>
                                                    </td>
                                                    <td class="Form_TextoDer">
                                                        <asp:Button ID="btnActuCliente" runat="server" Text="Actualizar" ValidationGroup="ActCliente"
                                                            OnClick="btnActuCliente_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="btnActuCliente" EventName="click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:ValidationSummary ID="vsDatoCliente" runat="server" ValidationGroup="ActCliente"
                                        ShowMessageBox="True" ShowSummary="False" />
                                    <uc1:MsgBox ID="msgCliente" runat="server" />
                                </ContentTemplate>
                            </cc1:TabPanel>
                            <cc1:TabPanel ID="tpVehiculo" runat="server" EnableViewState="false">
                                <HeaderTemplate>
                                    Datos del Vehículo
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="Form_TextoIzq" style="width: 60px;">
                                                <asp:Image ID="imgSpeechVeh" runat="server" SkinID="sknImgUsuario" />
                                            </td>
                                            <td class="Form_Nota">
                                                <asp:Label ID="lblSpecchVeh" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="Form_SubTitulo" colspan="6">
                                                INFORMACIÓN DEL VEHÍCULO
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Form_TextoDer">
                                                Marca:
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:TextBox ID="txtMarca" runat="server" CssClass="Form_TextBoxDisable" Width="130px"
                                                    ReadOnly="True"></asp:TextBox>
                                            </td>
                                            <td class="Form_TextoDer">
                                                Modelo:
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:TextBox ID="txtModelo" runat="server" CssClass="Form_TextBoxDisable" Width="130px"
                                                    ReadOnly="True"></asp:TextBox>
                                            </td>
                                            <td class="Form_TextoDer">
                                                Año Fab.:
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:TextBox ID="txtAnioFab" runat="server" CssClass="Form_TextBoxDisable" Width="130px"
                                                    ReadOnly="True"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Form_TextoDer">
                                                Nro Motor:
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:TextBox ID="txtNroMotor" runat="server" CssClass="Form_TextBoxDisable" Width="130px"
                                                    ReadOnly="True"></asp:TextBox>
                                            </td>
                                            <td class="Form_TextoDer">
                                                Nro Placa: <span class="Form_TextoObligatorio">*</span>
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:TextBox ID="txtNroPlaca" runat="server" CssClass="Form_TextBox" Width="130px"
                                                    MaxLength="20"></asp:TextBox><cc1:FilteredTextBoxExtender ID="fteNroPlaca" runat="server"
                                                        TargetControlID="txtNroPlaca" ValidChars="123456789-abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ"
                                                        Enabled="True">
                                                    </cc1:FilteredTextBoxExtender>
                                                <asp:RequiredFieldValidator ID="rfvNroPlaca" runat="server" ControlToValidate="txtNroPlaca"
                                                    SetFocusOnError="True" Display="None" ErrorMessage="Nro. Placa Obligatorio" ValidationGroup="ActVehiculo"></asp:RequiredFieldValidator>
                                            </td>
                                            <td class="Form_TextoDer">
                                                Color: <span class="Form_TextoObligatorio">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtColor" runat="server" CssClass="Form_TextBox" Width="130px" MaxLength="25"></asp:TextBox><cc1:FilteredTextBoxExtender
                                                    ID="fteColor" runat="server" TargetControlID="txtColor" ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnñopqrstuvwxyz"
                                                    Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                                <asp:RequiredFieldValidator ID="rfvColor" runat="server" ControlToValidate="txtColor"
                                                    SetFocusOnError="True" Display="None" ErrorMessage="Color Obligatorio" ValidationGroup="ActVehiculo"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Form_TextoDer">
                                                Tipo Vehículo:
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:TextBox ID="txtTipVehiculo" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="True"
                                                    Width="130px"></asp:TextBox>
                                            </td>
                                            <td class="Form_TextoDer">
                                                Clase Vehículo:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtClaseVehiculo" runat="server" CssClass="Form_TextBoxDisable"
                                                    ReadOnly="True" Width="130px"></asp:TextBox>
                                            </td>
                                            <td class="Form_TextoDer">
                                                Nro. Asientos: <span class="Form_TextoObligatorio">*</span>
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:TextBox ID="txtNroAsientos" runat="server" CssClass="Form_TextBox" Width="130px"
                                                    MaxLength="1"></asp:TextBox><cc1:FilteredTextBoxExtender ID="fteNroAsientos" runat="server"
                                                        TargetControlID="txtNroAsientos" ValidChars="1234" Enabled="True">
                                                    </cc1:FilteredTextBoxExtender>
                                                <asp:RequiredFieldValidator ID="rfvNroAsientos" runat="server" ControlToValidate="txtNroAsientos"
                                                    SetFocusOnError="True" Display="None" ErrorMessage="Nro. Asientos Obligatorio"
                                                    ValidationGroup="ActVehiculo"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Form_TextoDer">
                                                Nro. Chasis: <span class="Form_TextoObligatorio">*</span>
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:TextBox ID="txtNroChasis" runat="server" CssClass="Form_TextBox" Width="130px"></asp:TextBox>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <asp:UpdatePanel ID="upActVehiculo" runat="server">
                                        <ContentTemplate>
                                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                <tr>
                                                    <td>
                                                        <span class="Form_TextoObligatorio">&nbsp;(*)Campos Obligatorios</span>
                                                    </td>
                                                    <td class="Form_TextoDer">
                                                        <asp:UpdateProgress ID="upsActVehiculo" runat="server" AssociatedUpdatePanelID="upActVehiculo"
                                                            DisplayAfter="0">
                                                            <ProgressTemplate>
                                                                <asp:Image ID="imgLoadActVehiculo" runat="server" SkinID="sknImgLoading" Width="16px"
                                                                    Height="16px" /></ProgressTemplate>
                                                        </asp:UpdateProgress>
                                                    </td>
                                                    <td class="Form_TextoDer">
                                                        <asp:Button ID="btnActVehiculo" runat="server" Text="Actualizar" ValidationGroup="ActVehiculo"
                                                            OnClick="btnActVehiculo_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="btnActVehiculo" EventName="click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:ValidationSummary ID="vsActVehiculo" runat="server" ValidationGroup="ActVehiculo"
                                        ShowMessageBox="True" ShowSummary="False" />
                                    <uc1:MsgBox ID="msgVehiculo" runat="server" />
                                </ContentTemplate>
                            </cc1:TabPanel>
                            <cc1:TabPanel ID="tbMedioPago" runat="server">
                                <HeaderTemplate>
                                    Datos del Medio de Pago y Plan
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="Form_TextoIzq" style="width: 60px;">
                                                <asp:Image ID="imgSpeechMedPag" runat="server" SkinID="sknImgUsuario" />
                                            </td>
                                            <td class="Form_Nota">
                                                <asp:Label ID="lblSpecchMedPag" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                        <tr>
                                            <td class="Form_SubTitulo" colspan="6">
                                                DIRECCION DE ENTREGA
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Form_TextoDer">
                                                Dirección <span class="Form_TextoObligatorio">*</span>
                                            </td>
                                            <td class="Form_TextoIzq" colspan="5">
                                                <asp:TextBox ID="txtDirEntrega" runat="server" Width="350px" MaxLength="250" CssClass="Form_TextBox"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="dd" runat="server" ControlToValidate="txtDirEntrega"
                                                    ValidationGroup="CotizacionCertificado" ErrorMessage="Ingrese la dirección de entrega."
                                                    SetFocusOnError="true" Display="None"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Form_TextoDer">
                                                Departamento: <span class="Form_TextoObligatorio">*</span>
                                            </td>
                                            <td>
                                                <asp:UpdatePanel ID="upDepEntrega" runat="server">
                                                    <ContentTemplate>
                                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                            <tr>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlDepEntrega" runat="server" Width="125px" AutoPostBack="True"
                                                                        OnSelectedIndexChanged="ddlDepEntrega_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="rfvDepEntrega" runat="server" Text="*" ControlToValidate="ddlDepEntrega"
                                                                        Display="None" ErrorMessage="Seleccione departamento de entrega." ForeColor="Transparent"
                                                                        SetFocusOnError="true" InitialValue="-1" ValidationGroup="CotizacionCertificado">
                                                                    </asp:RequiredFieldValidator>
                                                                </td>
                                                                <td style="width: 16px">
                                                                    <asp:UpdateProgress ID="upsDepEntrega" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upDepEntrega">
                                                                        <ProgressTemplate>
                                                                            <asp:Image ID="imgLoadDep" runat="server" SkinID="sknImgLoading" Width="14px" Height="14px" />
                                                                        </ProgressTemplate>
                                                                    </asp:UpdateProgress>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td class="Form_TextoDer">
                                                Provincia: <span class="Form_TextoObligatorio">*</span>
                                            </td>
                                            <td>
                                                <asp:UpdatePanel ID="upProvEntrega" runat="server">
                                                    <ContentTemplate>
                                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                            <tr>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlProvEntrega" runat="server" Width="125px" AutoPostBack="True"
                                                                        OnSelectedIndexChanged="ddlProvEntrega_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="rfvProvEntrega" runat="server" Text="*" ControlToValidate="ddlProvEntrega"
                                                                        Display="None" ErrorMessage="Seleccione provincia de entrega." ForeColor="Transparent"
                                                                        SetFocusOnError="true" InitialValue="-1" ValidationGroup="CotizacionCertificado">
                                                                    </asp:RequiredFieldValidator>
                                                                </td>
                                                                <td>
                                                                    <asp:UpdateProgress ID="upsProvEntrega" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upProvEntrega">
                                                                        <ProgressTemplate>
                                                                            <asp:Image ID="imgLoadProv" runat="server" SkinID="sknImgLoading" Width="14px" Height="14px" />
                                                                        </ProgressTemplate>
                                                                    </asp:UpdateProgress>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddlDepEntrega" EventName="selectedIndexChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td class="Form_TextoDer">
                                                Distrito: <span class="Form_TextoObligatorio">*</span>
                                            </td>
                                            <td>
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:DropDownList ID="ddlDistEntrega" runat="server" Width="125px">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="rfvDistEntrega" runat="server" ControlToValidate="ddlDistEntrega"
                                                            Display="None" ErrorMessage="Seleccione distrito de entrega." ForeColor="Transparent"
                                                            InitialValue="-1" SetFocusOnError="true" Text="*" ValidationGroup="CotizacionCertificado">
                                                        </asp:RequiredFieldValidator>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddlProvEntrega" EventName="selectedIndexChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;
                                        display: none;">
                                        <tr>
                                            <td class="Form_SubTitulo" colspan="6">
                                                IMFORMACIÓN DEL MEDIO DE PAGO
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Form_TextoIzq" colspan="6">
                                                <asp:CheckBox ID="cbxSolCuenta" runat="server" Text="Solicitar nro. de cuenta:" onclick="SolicitarCuenta();" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Form_TextoDer">
                                                Medio de Pago: <span class="Form_TextoObligatorio">*</span>
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:UpdatePanel ID="upMedPago" runat="server">
                                                    <ContentTemplate>
                                                        <asp:DropDownList ID="ddlMedPago" runat="server" Width="125px" AutoPostBack="True"
                                                            OnSelectedIndexChanged="ddlMedPago_SelectedIndexChanged" onchange="ValidaTipoMedioPago(this);">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="rfvMedPago" runat="server" Text="*" ControlToValidate="ddlMedPago"
                                                            Display="None" ErrorMessage="Seleccione medio de pago." ForeColor="Transparent"
                                                            SetFocusOnError="True" InitialValue="-1" ValidationGroup="CotizacionCertificadoOpc"
                                                            Enabled="False"></asp:RequiredFieldValidator></ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddlMedPago" EventName="selectedIndexChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td class="Form_TextoDer">
                                                Nro. Cuenta: <span class="Form_TextoObligatorio">*</span>
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:TextBox ID="txtNroCuenta" runat="server" CssClass="Form_TextBox" Width="130px"
                                                    MaxLength="50"></asp:TextBox><asp:RequiredFieldValidator ID="rfvNroCuenta" runat="server"
                                                        ControlToValidate="txtNroCuenta" SetFocusOnError="True" Display="None" ErrorMessage="Nro. cuenta obligatorio."
                                                        Text="*" ValidationGroup="CotizacionCertificadoOpc"></asp:RequiredFieldValidator><cc1:FilteredTextBoxExtender
                                                            ID="fteMonCuenta" runat="server" TargetControlID="txtNroCuenta" ValidChars="0123456789"
                                                            Enabled="True">
                                                        </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td class="Form_TextoDer">
                                                Moneda Cuenta: <span class="Form_TextoObligatorio">*</span>
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:UpdatePanel ID="upMonCuenta" runat="server">
                                                    <ContentTemplate>
                                                        <asp:DropDownList ID="ddlMonCuenta" runat="server" Width="125px">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="rfvMonCuenta" runat="server" Text="*" ControlToValidate="ddlMonCuenta"
                                                            Display="None" ErrorMessage="Seleccione moneda cuenta." ForeColor="Transparent"
                                                            SetFocusOnError="True" InitialValue="-1" ValidationGroup="CotizacionCertificadoOpc"></asp:RequiredFieldValidator></ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddlMedPago" EventName="selectedIndexChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Form_TextoDer">
                                                Fecha Vencimiento:
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:TextBox ID="txtFecVencimiento" runat="server" CssClass="Form_TextBox" Width="60px"
                                                    Enabled="False"></asp:TextBox><cc1:MaskedEditExtender ID="meeFecVencimiento" runat="server"
                                                        Mask="99/99/9999" MaskType="Date" TargetControlID="txtFecVencimiento" CultureAMPMPlaceholder=""
                                                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                                                        CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                                                        Enabled="True">
                                                    </cc1:MaskedEditExtender>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="Form_SubTitulo" colspan="6">
                                                INFORMACIÓN DEL PLAN
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Form_TextoDer">
                                                Plan: <span class="Form_TextoObligatorio">*</span>
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:DropDownList ID="ddlPlan" runat="server" Width="125px" OnSelectedIndexChanged="ddlPlan_SelectedIndexChanged"
                                                    AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="Form_TextoDer">
                                                Periodo de Pago: <span class="Form_TextoObligatorio">*</span>
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:DropDownList ID="ddlFrecuencia" runat="server" Width="125px" OnSelectedIndexChanged="ddlFrecuencia_SelectedIndexChanged"
                                                    AutoPostBack="True">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="rfvFrecuencia" runat="server" Text="*" ControlToValidate="ddlFrecuencia"
                                                    Display="None" ErrorMessage="Seleccione frecuencia." ForeColor="Transparent"
                                                    InitialValue="-1" SetFocusOnError="True" ValidationGroup="CotizacionCertificado"></asp:RequiredFieldValidator>
                                            </td>
                                            <td class="Form_TextoDer">
                                                Moneda Prima: <span class="Form_TextoObligatorio">*</span>
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:DropDownList ID="ddlMonPrima" runat="server" Width="125px">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="rfvMonPrima" runat="server" Text="*" ControlToValidate="ddlMonPrima"
                                                    Display="None" ErrorMessage="Seleccione moneda prima." ForeColor="Transparent"
                                                    SetFocusOnError="True" InitialValue="-1" ValidationGroup="CotizacionCertificado"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Form_TextoDer">
                                                Valor de Cuota: <span class="Form_TextoObligatorio">*</span>
                                            </td>
                                            <td class="Form_TextoIzq">
                                                <asp:UpdatePanel ID="upCotPrimCertificado" runat="server">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtPrimTotal" runat="server" CssClass="Form_TextBoxMontoDisable"
                                                            Width="100px" ReadOnly="true"></asp:TextBox><cc1:FilteredTextBoxExtender ID="ftxtPrimTotal"
                                                                runat="server" TargetControlID="txtPrimTotal" ValidChars="1234567890,.">
                                                            </cc1:FilteredTextBoxExtender>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddlPlan" EventName="SelectedIndexChanged" />
                                                        <asp:AsyncPostBackTrigger ControlID="ddlFrecuencia" EventName="SelectedIndexChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                                <asp:RequiredFieldValidator ID="rfvPrimTotal" runat="server" Text="*" ControlToValidate="txtPrimTotal"
                                                    Display="None" ErrorMessage="No existe monto de prima." ForeColor="Transparent"
                                                    SetFocusOnError="True" ValidationGroup="CotizacionCertificado"></asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="Form_TextoIzq">
                                                <span class="Form_TextoObligatorio">&nbsp;(*)Campos Obligatorios</span>
                                            </td>
                                            <td class="Form_TextoDer">
                                                <asp:Button ID="btnExpedir" runat="server" Text="Expedir" ValidationGroup="CotizacionCertificado"
                                                    OnClick="btnExpedir_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                    <uc1:MsgBox ID="msgCertificado" runat="server" />
                                    <asp:ValidationSummary ID="vsCertificado" runat="server" ShowMessageBox="True" ShowSummary="False"
                                        ValidationGroup="CotizacionCertificado" />
                                    <asp:ValidationSummary ID="vsCertificadoOpc" runat="server" ShowMessageBox="True"
                                        ShowSummary="False" ValidationGroup="CotizacionCertificadoOpc" />
                                </ContentTemplate>
                            </cc1:TabPanel>
                        </cc1:TabContainer>
                    </td>
                    <td class="Form_tdBorde">
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:Label ID="lblCotCertificado" runat="server" Style="display: none;"></asp:Label>
    <cc1:ModalPopupExtender ID="mpeCotCertificado" runat="server" Enabled="true" TargetControlID="lblCotCertificado"
        BehaviorID="bmpeCotCertificado" PopupControlID="pnCertificado" BackgroundCssClass="Modal_Background"
        PopupDragHandleControlID="pnCertificado">
    </cc1:ModalPopupExtender>
    <!--Popup Documento Certificado-->
    <asp:Panel ID="pnDocCertificado" runat="server" Width="700px" CssClass="Modal_Panel"
        Style="display: none;" DefaultButton="btnEnvCotMail">
        <div class="Modal_Head" style="width: 100%;">
            <div id="div7" style="float: left;">
                Imprimir Certificado
            </div>
            <div id="div8" style="float: right;">
                <img alt="Click para cerrar." class="Modal_CloseButton" onclick="hideModalPopup('bmpCotDocCertificado');"
                    style="cursor: hand;" />
            </div>
        </div>
        <div id="div9" class="Modal_Body" style="height: 480px;">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="Form_TextoIzq" style="width: 60px;">
                        <asp:Image ID="imgSpeechBen" runat="server" SkinID="sknImgUsuario" />
                    </td>
                    <td>
                        <asp:Panel ID="pnSpeechCert" runat="server" ScrollBars="Vertical" Width="100%" Height="100px">
                            <table>
                                <tr>
                                    <td class="Form_Nota">
                                        <asp:Label ID="lblSpeechBen" runat="server"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblSpeechCob" runat="server"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblSpeechCoor" runat="server"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblSpeechDesp" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <br />
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="Form_tdBorde">
                    </td>
                    <td>
                        <%--<rsweb:ReportViewer ID="rvCertificado" runat="server" Width="100%" Height="350px"
                            Font-Names="Verdana" Font-Size="8pt" ShowRefreshButton="False">
                            <LocalReport ReportPath="rdlc\rdlcCertificadoSegVeh.rdlc">
                                <DataSources>
                                    <rsweb:ReportDataSource DataSourceId="odsCertificado" Name="BECertificado" />
                                    <rsweb:ReportDataSource DataSourceId="odsVehiculo" Name="BEVehiculo" />
                                    <rsweb:ReportDataSource DataSourceId="odsCliente" Name="BECliente" />
                                </DataSources>
                            </LocalReport>
                        </rsweb:ReportViewer>--%>
                        <asp:ObjectDataSource ID="odsCliente" runat="server" SelectMethod="ObtenerxCertificado"
                            TypeName="AONAffinity.Motor.BusinessLogic.BLCliente">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfIdCertificado" DefaultValue="5251-3" Name="pcIdCertificado"
                                    PropertyName="Value" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource ID="odsVehiculo" runat="server" SelectMethod="ObtenerxCertificado"
                            TypeName="AONAffinity.Motor.BusinessLogic.BLVehiculo">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfIdCertificado" DefaultValue="0" Name="pcIdCertificado"
                                    PropertyName="Value" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource ID="odsCertificado" runat="server" SelectMethod="Obtener" TypeName="AONAffinity.Motor.BusinessLogic.BLReporte">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfIdCertificado" DefaultValue="0" Name="pcIdCertificado"
                                    PropertyName="Value" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </td>
                    <td class="Form_tdBorde">
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:Label ID="lblCotDocCertificado" runat="server" Style="display: none;"></asp:Label>
    <cc1:ModalPopupExtender ID="mpeCotDocCertificado" runat="server" Enabled="true" TargetControlID="lblCotDocCertificado"
        BehaviorID="bmpCotDocCertificado" PopupControlID="pnDocCertificado" BackgroundCssClass="Modal_Background">
    </cc1:ModalPopupExtender>
    <!--Popup Ver Agenda-->
    <asp:Panel ID="pnVerAgenda" runat="server" Width="500px " CssClass="Modal_Panel"
        Style="display: none;">
        <div class="Modal_Head" style="width: 100%;">
            <div id="divCabVerAgenda" style="float: left;">
                Ver Agenda
            </div>
            <div id="div11" style="float: right;">
                <img alt="Click para cerrar." class="Modal_CloseButton" onclick="hideModalPopup('bmpeVerAgenda');"
                    style="cursor: hand;" />
            </div>
        </div>
        <div id="div12" class="Modal_Body" style="height: 160px;">
            <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="Form_SubTitulo" colspan="4">
                        Información de Cotización en Agenda
                    </td>
                </tr>
                <tr>
                    <td class="Form_TextoDer">
                        Fecha y Hora:
                    </td>
                    <td class="Form_TextoIzq">
                        <asp:TextBox ID="txtFecHora" runat="server" Width="60px" ReadOnly="true" CssClass="Form_TextBoxDisable"></asp:TextBox>
                    </td>
                    <td class="Form_TextoDer">
                        Informador:
                    </td>
                    <td class="Form_TextoIzq">
                        <asp:TextBox ID="txtInformador" runat="server" Width="120px" ReadOnly="true" CssClass="Form_TextBoxDisable"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="Form_TextoDer" style="vertical-align: top;">
                        Observaciones:
                    </td>
                    <td class="Form_TextoIzq" colspan="3">
                        <asp:TextBox ID="txtObsAgeCot" runat="server" ReadOnly="true" TextMode="MultiLine"
                            Height="80px" Width="320px" CssClass="Form_TextBoxDisable"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:Label ID="lblVerAgenda" runat="server" Style="display: none;"></asp:Label>
    <cc1:ModalPopupExtender ID="mpeVerAgenda" runat="server" Enabled="true" TargetControlID="lblVerAgenda"
        BehaviorID="bmpeVerAgenda" PopupControlID="pnVerAgenda" BackgroundCssClass="Modal_Background"
        PopupDragHandleControlID="divCabVerAgenda">
    </cc1:ModalPopupExtender>
</asp:Content>
