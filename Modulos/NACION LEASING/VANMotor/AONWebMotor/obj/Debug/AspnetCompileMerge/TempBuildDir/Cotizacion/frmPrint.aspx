﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmPrint.aspx.cs" Inherits="AONWebMotor.Cotizacion.frmPrint" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width:99%; height:99%;" >
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"
            InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt"
            Width="100%" Height="550px"  ShowZoomControl="false" >
            <LocalReport ReportPath="rdlc\ProductoGrupo\rdlCotizacionGrupoProd_1.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="odsCotDetalle" Name="dsCotizacionDetalle" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
    </div>
    <asp:ObjectDataSource ID="odsCotDetalle" runat="server" SelectMethod="ObtenerDetalle"
        TypeName="AONAffinity.Motor.BusinessLogic.BLCotizacion">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="120" Name="pnNroCotizacion" QueryStringField="idCot"
                Type="Decimal" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    </form>
</body>
</html>
