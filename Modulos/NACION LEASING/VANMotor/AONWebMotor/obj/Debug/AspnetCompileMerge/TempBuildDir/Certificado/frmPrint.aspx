﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmPrint.aspx.cs" Inherits="AONWebMotor.Certificado.frmPrint" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 18px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
         <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>                                
                <div style="width: 100%; height:95%; overflow:auto;" >
                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Width="100%" Height="450px"     
                        Font-Size="8pt" InteractiveDeviceInfos="(Colección)" WaitMessageFont-Names="Verdana"
                        WaitMessageFont-Size="14pt" BackColor="#CCCCCC">
                        <LocalReport ReportPath="rdlc\Rimac\CertificadoRIMAC.rdlc">
                            <DataSources>
                                <rsweb:ReportDataSource DataSourceId="odsCertificado" Name="BECertificado" />
                                <rsweb:ReportDataSource DataSourceId="odsVehiculo" Name="BEVehiculo" />
                                <rsweb:ReportDataSource DataSourceId="odsCliente" Name="BECliente" />
                            </DataSources>
                        </LocalReport>
                    </rsweb:ReportViewer>
                    <asp:ObjectDataSource ID="odsCliente" runat="server" 
                        SelectMethod="ObtenerxCertificado" 
                        TypeName="AONAffinity.Motor.BusinessLogic.BLCliente">
                        <SelectParameters>
                            <asp:QueryStringParameter DefaultValue="0" Name="pcIdCertificado" 
                                QueryStringField="idCert" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="odsVehiculo" runat="server" 
                        SelectMethod="ObtenerxCertificado" 
                        TypeName="AONAffinity.Motor.BusinessLogic.BLVehiculo">
                        <SelectParameters>
                            <asp:QueryStringParameter DefaultValue="0" Name="pcIdCertificado" 
                                QueryStringField="idCert" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="odsCertificado" runat="server" SelectMethod="Obtener" 
                        TypeName="AONAffinity.Motor.BusinessLogic.Bais.BLCertificado" >
                        <SelectParameters>
                            <asp:QueryStringParameter DefaultValue="0" Name="pcIdCertificado" 
                                QueryStringField="idCert" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:HiddenField ID="hfIdCertificado" runat="server" />
                </div>
            </td>
            <td class="style1">
            </td>
        </tr>
    </table>
    </div>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true" EnableScriptGlobalization="true">
    </asp:ScriptManager>
    </form>
</body>
</html>
