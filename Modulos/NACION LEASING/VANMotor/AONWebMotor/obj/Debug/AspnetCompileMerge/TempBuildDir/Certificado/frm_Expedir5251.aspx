﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true"
    CodeBehind="frm_Expedir5251.aspx.cs" Inherits="AONWebMotor.Certificado.frm_Expedir5251" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <asp:Panel ID="pnCliente" runat="server">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        
                    </table>
                    <br />
                    <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="Form_SubTitulo" colspan="8">
                                INFORMACIÓN DEL CLIENTE
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoIzq" style="width: 60px;">
                                <asp:Image ID="imgSpeechCli" runat="server" SkinID="sknImgUsuario" />
                            </td>
                            <td class="Form_TextoIzq" colspan="7">
                                <asp:Label ID="lblSpecchCli" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer">
                                1er Nombre:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:TextBox ID="txtNombre1" runat="server" CssClass="Form_TextBox" MaxLength="60"
                                    Width="100px"></asp:TextBox>
                                <span class="Form_TextoObligatorio">*</span>
                                <cc1:FilteredTextBoxExtender ID="fteNombre1" runat="server" TargetControlID="txtNombre1"
                                    ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnñopqrstuvwxyzÁÉÍÓÚÜáéíóúü"
                                    Enabled="True">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td class="Form_TextoDer">
                                2do Nombre:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:TextBox ID="txtNombre2" runat="server" CssClass="Form_TextBox" MaxLength="60"
                                    Width="100px"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="fteNombre2" runat="server" TargetControlID="txtNombre2"
                                    ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnñopqrstuvwxyzÁÉÍÓÚÜáéíóúü"
                                    Enabled="True">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td class="Form_TextoDer">
                                Tipo Doc.:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:DropDownList ID="ddlTipDocumento" runat="server" Width="100px">
                                    <asp:ListItem Text="DNI" Value="L"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="Form_TextoDer">
                                Nro. Doc.:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:TextBox ID="txtNroDocumento" runat="server" CssClass="Form_TextBox" MaxLength="8"
                                    Width="100px"></asp:TextBox>
                                <span class="Form_TextoObligatorio">*</span>
                                <cc1:FilteredTextBoxExtender ID="fteNroDocumento" runat="server" TargetControlID="txtNroDocumento"
                                    ValidChars="0123456789" Enabled="True">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer">
                                1er Apellido:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:TextBox ID="txtApellidoPat" runat="server" CssClass="Form_TextBox" MaxLength="60"
                                    Width="100px">
                                </asp:TextBox>
                                <span class="Form_TextoObligatorio">*</span>
                                <cc1:FilteredTextBoxExtender ID="fteApellidoPat" runat="server" TargetControlID="txtApellidoPat"
                                    ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnñopqrstuvwxyzÁÉÍÓÚÜáéíóúü"
                                    Enabled="True">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td class="Form_TextoDer">
                                2do Apellido:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:TextBox ID="txtApellidoMat" runat="server" CssClass="Form_TextBox" MaxLength="60"
                                    Width="100px"></asp:TextBox>
                                <span class="Form_TextoObligatorio">*</span>
                                <cc1:FilteredTextBoxExtender ID="fteApellidoMat" runat="server" TargetControlID="txtApellidoMat"
                                    ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnñopqrstuvwxyzÁÉÍÓÚÜáéíóúü"
                                    Enabled="True">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td class="Form_TextoDer">
                                Sexo:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:DropDownList ID="ddlSexo" runat="server" Width="100px">
                                    <asp:ListItem Text="MÁSCULINO" Value="M"></asp:ListItem>
                                    <asp:ListItem Text="FEMENINO" Value="F"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="Form_TextoDer">
                                Estado Civil:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:DropDownList ID="ddlEstadoCiv" runat="server" Width="105px">
                                    <asp:ListItem Text="SOLTERO" Value="S"></asp:ListItem>
                                    <asp:ListItem Text="CASADO" Value="C"></asp:ListItem>
                                    <asp:ListItem Text="VIUDO" Value="V"></asp:ListItem>
                                    <asp:ListItem Text="DIVORCIADO" Value="D"></asp:ListItem>
                                    <asp:ListItem Text="SEPARADO" Value="X"></asp:ListItem>
                                    <asp:ListItem Text="UNIÓN LIBRE" Value="U"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer">
                                Fec. Nacimiento:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:TextBox ID="txtFecNacimiento" runat="server" Width="100px" CssClass="Form_TextBox">
                                </asp:TextBox>
                                <span class="Form_TextoObligatorio">*</span>
                                <cc1:MaskedEditExtender ID="meeFecNacimiento" runat="server" TargetControlID="txtFecNacimiento"
                                    Mask="99/99/9999" MaskType="Date">
                                </cc1:MaskedEditExtender>
                            </td>
                            <td class="Form_TextoDer">
                                Dirección:
                            </td>
                            <td class="Form_TextoIzq" colspan="3">
                                <asp:TextBox ID="txtDireccion" runat="server" MaxLength="300" Width="290px" CssClass="Form_TextBox">
                                </asp:TextBox>
                                <span class="Form_TextoObligatorio">*</span>
                            </td>
                            <td class="Form_TextoDer">
                                Prefijo:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:DropDownList ID="ddlPrefijo" runat="server" Width="105px">
                                    <asp:ListItem Text="CALLE" Value="Calle"></asp:ListItem>
                                    <asp:ListItem Text="AVENIDA" Value="Av"></asp:ListItem>
                                    <asp:ListItem Text="JIRÓN" Value="Jr"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer">
                                Mz/Lte/Nro:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:TextBox ID="txtMzLteNro" runat="server" MaxLength="100" CssClass="Form_TextBox"
                                    Width="100px">
                                </asp:TextBox>
                                <span class="Form_TextoObligatorio">*</span>
                            </td>
                            <td class="Form_TextoDer">
                                Apto/Int:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:TextBox ID="txtAptoInt" runat="server" MaxLength="100" CssClass="Form_TextBox"
                                    Width="100px">                                    
                                </asp:TextBox>
                                <span class="Form_TextoObligatorio">*</span>
                            </td>
                            <td class="Form_TextoDer">
                                Urbanización:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:TextBox ID="txtUrbanizacion" runat="server" MaxLength="15" CssClass="Form_TextBox"
                                    Width="100px">
                                </asp:TextBox>
                                <span class="Form_TextoObligatorio">*</span>
                            </td>
                            <td class="Form_TextoDer">
                            </td>
                            <td class="Form_TextoIzq">
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer">
                                Referencia 1:
                            </td>
                            <td class="Form_TextoIzq" colspan="3">
                                <asp:TextBox ID="txtReferencia" runat="server" MaxLength="195" CssClass="Form_TextBox"
                                    Width="200px">
                                </asp:TextBox>
                                <span class="Form_TextoObligatorio">*</span>
                            </td>
                            <td class="Form_TextoDer">
                                Referencia 2:
                            </td>
                            <td class="Form_TextoIzq" colspan="3">
                                <asp:TextBox ID="txtReferencia2" runat="server" MaxLength="195" CssClass="Form_TextBox"
                                    Width="200px">
                                </asp:TextBox>
                                <span class="Form_TextoObligatorio">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer">
                                Departamento:
                            </td>
                            <td class="Form_TextoIzq" style="width: 125px;">
                                <asp:UpdatePanel ID="upDepartamento" runat="server">
                                    <ContentTemplate>
                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                            <tr>
                                                <td style="width: 100px;">
                                                    <asp:DropDownList ID="ddlDepartamento" runat="server" Width="105px" AutoPostBack="True"
                                                        OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 5px;">
                                                    <span class="Form_TextoObligatorio">*</span>
                                                </td>
                                                <td style="width: 10px;">
                                                    <asp:UpdateProgress ID="upsDepartamento" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upDepartamento">
                                                        <ProgressTemplate>
                                                            <asp:Image ID="imgLoadDep" runat="server" SkinID="sknImgLoading" Width="10px" Height="10px" />
                                                        </ProgressTemplate>
                                                    </asp:UpdateProgress>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="Form_TextoDer">
                                Provincia:
                            </td>
                            <td class="Form_TextoIzq" style="width: 125px;">
                                <asp:UpdatePanel ID="upProvincia" runat="server">
                                    <ContentTemplate>
                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                            <tr>
                                                <td style="width: 100px;">
                                                    <asp:DropDownList ID="ddlProvincia" runat="server" Width="105px" AutoPostBack="True"
                                                        OnSelectedIndexChanged="ddlProvincia_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 5px;">
                                                    <span class="Form_TextoObligatorio">*</span>
                                                </td>
                                                <td style="width: 10px;">
                                                    <asp:UpdateProgress ID="upsProvincia" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upProvincia">
                                                        <ProgressTemplate>
                                                            <asp:Image ID="imgLoadProv" runat="server" SkinID="sknImgLoading" Width="10px" Height="10px" />
                                                        </ProgressTemplate>
                                                    </asp:UpdateProgress>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlDepartamento" EventName="selectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                            <td class="Form_TextoDer">
                                Distrito:
                            </td>
                            <td class="Form_TextoIzq" colspan="3">
                                <asp:UpdatePanel ID="upDistrito" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlDistrito" runat="server" Width="205px">
                                        </asp:DropDownList>
                                        <span class="Form_TextoObligatorio">*</span>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlProvincia" EventName="selectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer">
                                Teléfono 1:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:TextBox ID="txtTelefono1" runat="server" MaxLength="15" Width="100px" CssClass="Form_TextBox"></asp:TextBox>
                                <span class="Form_TextoObligatorio">*</span>
                                <cc1:FilteredTextBoxExtender ID="fteTelDomicio1" runat="server" TargetControlID="txtTelefono1"
                                    ValidChars="0123456789*-" Enabled="True">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td class="Form_TextoDer">
                                Teléfono 2:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:TextBox ID="txtTelefono2" runat="server" MaxLength="15" Width="100px" CssClass="Form_TextBox"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="fteTelefono2" runat="server" TargetControlID="txtTelefono2"
                                    ValidChars="0123456789*-" Enabled="True">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td class="Form_TextoDer">
                                Email:
                            </td>
                            <td class="Form_TextoIzq" colspan="3">
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="Form_TextBox" Width="200px"></asp:TextBox>
                                <span class="Form_TextoObligatorio">*</span>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="Form_SubTitulo" colspan="8">
                                INFORMACIÓN DEL VEHÍCULO
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer">
                                Marca:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:TextBox ID="txtMarca" runat="server" CssClass="Form_TextBoxDisable" Width="100px"
                                    ReadOnly="true"></asp:TextBox>
                            </td>
                            <td class="Form_TextoDer">
                                Modelo:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:TextBox ID="txtModelo" runat="server" CssClass="Form_TextBoxDisable" Width="100px"
                                    ReadOnly="true"></asp:TextBox>
                            </td>
                            <td class="Form_TextoDer">
                                Año Fab.:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:TextBox ID="txtAnioFab" runat="server" CssClass="Form_TextBoxDisable" Width="100px"
                                    ReadOnly="true"></asp:TextBox>
                            </td>
                            <td class="Form_TextoDer">
                                Nro Motor:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:TextBox ID="txtNroMotor" runat="server" CssClass="Form_TextBoxDisable" Width="100px"
                                    ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer">
                                Nro Placa:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:TextBox ID="txtNroPlaca" runat="server" CssClass="Form_TextBox" Width="100px"
                                    MaxLength="20"></asp:TextBox>
                                <span class="Form_TextoObligatorio">*</span>
                                <cc1:FilteredTextBoxExtender ID="fteNroPlaca" runat="server" TargetControlID="txtNroPlaca"
                                    ValidChars="123456789-abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ"
                                    Enabled="True">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td class="Form_TextoDer">
                                Color:
                            </td>
                            <td>
                                <asp:TextBox ID="txtColor" runat="server" CssClass="Form_TextBox" Width="100px" MaxLength="25">
                                </asp:TextBox>
                                <span class="Form_TextoObligatorio">*</span>
                                <cc1:FilteredTextBoxExtender ID="fteColor" runat="server" TargetControlID="txtColor"
                                    ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZ abcdefghijklmnñopqrstuvwxyz">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td class="Form_TextoDer">
                                Tipo Vehículo:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:TextBox ID="txtTipVehiculo" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                    Width="100px"></asp:TextBox>
                            </td>
                            <td class="Form_TextoDer">
                                Clase Vehículo:
                            </td>
                            <td>
                                <asp:TextBox ID="txtClaseVehiculo" runat="server" CssClass="Form_TextBoxDisable"
                                    ReadOnly="true" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer">
                                Nro. Asientos:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:TextBox ID="txtNroAsientos" runat="server" CssClass="Form_TextBox" Width="100px"
                                    MaxLength="1">
                                </asp:TextBox>
                                <span class="Form_TextoObligatorio">*</span>
                                <cc1:FilteredTextBoxExtender ID="fteNroAsientos" runat="server" TargetControlID="txtNroAsientos"
                                    ValidChars="1234567890" Enabled="True">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td class="Form_TextoDer">
                                Nro. Chasis:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:TextBox ID="txtNroChasis" runat="server" CssClass="Form_TextBox" Width="100px">
                                </asp:TextBox>
                                <span class="Form_TextoObligatorio">*</span>
                            </td>
                            <td class="Form_TextoDer">
                            </td>
                            <td class="Form_TextoIzq">
                            </td>
                            <td class="Form_TextoDer">
                            </td>
                            <td class="Form_TextoIzq">
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="Form_TextoIzq">
                                <span class="Form_TextoObligatorio">&nbsp;(*)Campos Obligatorios</span>
                            </td>
                            <td class="Form_TextoDer">
                                <asp:Button ID="btnSiguiente" runat="server" Text="Siguiente" Width="90px" Enabled="false"
                                    OnClick="btnSiguiente_Click" />
                                <asp:Button ID="btnAcualizar" runat="server" Text="Actualizar" Width="90px" ValidationGroup="valCliente"
                                    OnClick="btnAcualizar_Click" />
                            </td>
                        </tr>
                    </table>
                    <asp:ValidationSummary ID="vsCliente" runat="server" ValidationGroup="valCliente"
                        ShowMessageBox="true" ShowSummary="false" />
                    <div style="display: none;">
                        <asp:RequiredFieldValidator ID="rfvNombre1" runat="server" ControlToValidate="txtNombre1"
                            ValidationGroup="valCliente" Display="None" ErrorMessage="1er nombre es obligatorio."
                            SetFocusOnError="True"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="rfvNroDocumento" runat="server" ControlToValidate="txtNroDocumento"
                            ValidationGroup="valCliente" Display="None" ErrorMessage="Nro. documento obligatorio."
                            SetFocusOnError="True"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="rfvApellidoPat" runat="server" ControlToValidate="txtApellidoPat"
                            ValidationGroup="valCliente" Display="None" ErrorMessage="1er apellido es obligatorio."
                            SetFocusOnError="True"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="rfvApellidoMat" runat="server" ControlToValidate="txtApellidoMat"
                            ValidationGroup="valCliente" Display="None" ErrorMessage="2do apellido es obligatorio."
                            SetFocusOnError="True"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="rfvFecNacimiento" runat="server" ControlToValidate="txtFecNacimiento"
                            ValidationGroup="valCliente" Display="None" ErrorMessage="Fecha de nacimiento es obligatorio">
                        </asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="rfvDireccion" runat="server" ControlToValidate="txtDireccion"
                            ValidationGroup="valCliente" Display="None" ErrorMessage="Dirección es obligatorio."
                            SetFocusOnError="True"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="rfvRefrencia" runat="server" ControlToValidate="txtReferencia"
                            ValidationGroup="valCliente" Display="None" ErrorMessage="Referencia 1 es obligatorio."
                            SetFocusOnError="True">
                        </asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="rfvRefrencia2" runat="server" ControlToValidate="txtReferencia2"
                            ValidationGroup="valCliente" Display="None" ErrorMessage="Referencia 2 es obligarotio."
                            SetFocusOnError="true">
                        </asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="rfvMzLteNro" runat="server" ControlToValidate="txtMzLteNro"
                            ValidationGroup="valCliente" Display="None" ErrorMessage="Mz/Lte/Nro es obligatorio."
                            SetFocusOnError="true">
                        </asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="rfvAptoInt" runat="server" ControlToValidate="txtAptoInt"
                            ValidationGroup="valCliente" Display="None" ErrorMessage="Apto/Int es obligatorio."
                            SetFocusOnError="true">
                        </asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="rfvUrbanizacion" runat="server" ControlToValidate="txtUrbanizacion"
                            ValidationGroup="valCliente" Display="None" ErrorMessage="Urbanización es obligatorio."
                            SetFocusOnError="True">
                        </asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="rfvTelefono1" runat="server" ControlToValidate="txtTelefono1"
                            ValidationGroup="valCliente" Display="None" ErrorMessage="Teléfono es obligatorio."
                            SetFocusOnError="true">
                        </asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="rfvNroPlaca" runat="server" ControlToValidate="txtNroPlaca"
                            SetFocusOnError="True" Display="None" ErrorMessage="Nro. placa es obligatorio"
                            ValidationGroup="valCliente"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="rfvColor" runat="server" ControlToValidate="txtColor"
                            SetFocusOnError="True" Display="None" ErrorMessage="Color del vehículo es obligatorio"
                            ValidationGroup="valCliente"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="rfvNroAsientos" runat="server" ControlToValidate="txtNroAsientos"
                            SetFocusOnError="True" Display="None" ErrorMessage="Nro. de asientos es obligatorio."
                            ValidationGroup="valCliente">
                        </asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="rfvNroChasis" runat="server" ControlToValidate="txtNroChasis"
                            SetFocusOnError="true" Display="None" ErrorMessage="Nro. de chasis es obligatorio."
                            ValidationGroup="valCliente">
                        </asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="rfvDepartamento" runat="server" Text="*" ControlToValidate="ddlDepartamento"
                            Display="None" ErrorMessage="Seleccione departamento." ForeColor="Transparent"
                            SetFocusOnError="true" InitialValue="-1" ValidationGroup="valCliente">
                        </asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="rfvProvincia" runat="server" Text="*" ControlToValidate="ddlProvincia"
                            Display="None" ErrorMessage="Seleccione provincia." ForeColor="Transparent" SetFocusOnError="true"
                            InitialValue="-1" ValidationGroup="valCliente">
                        </asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="rfvDistrito" runat="server" ControlToValidate="ddlDistrito"
                            Display="None" ErrorMessage="Seleccione distrito." ForeColor="Transparent" InitialValue="-1"
                            SetFocusOnError="true" Text="*" ValidationGroup="valCliente">
                        </asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                            Display="None" ErrorMessage="Email es obligatorio." SetFocusOnError="true" ValidationGroup="valCliente">
                        </asp:RequiredFieldValidator>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnCertificado" runat="server" Visible="false">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        
                    </table>
                    <br />
                    <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td class="Form_SubTitulo" colspan="6">
                                DIRECCIÓN DE ENTREGA
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoIzq" style="width: 60px;">
                                <asp:Image ID="imgSpeechVeh" runat="server" SkinID="sknImgUsuario" />
                            </td>
                            <td class="Form_TextoIzq" colspan="5" >
                                <asp:Label ID="lblSpecchVeh" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer" style="width: 107px; vertical-align: top;">
                                Dirección
                            </td>
                            <td class="Form_TextoIzq" colspan="5">
                                <asp:TextBox ID="txtDirEntrega" runat="server" Width="350px" MaxLength="250" CssClass="Form_TextBox"
                                    Height="40px" TextMode="MultiLine"></asp:TextBox>
                                <span class="Form_TextoObligatorio">*</span>
                                <asp:RequiredFieldValidator ID="rdvDirEntrega" runat="server" ControlToValidate="txtDirEntrega"
                                    ValidationGroup="valExpedir" ErrorMessage="Dirección de entrega es obligatorio."
                                    SetFocusOnError="true" Display="None"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer">
                                Departamento:
                            </td>
                            <td style="width: 150px;">
                                <asp:UpdatePanel ID="upDepEntrega" runat="server">
                                    <ContentTemplate>
                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                            <tr>
                                                <td style="width: 100px;">
                                                    <asp:DropDownList ID="ddlDepEntrega" runat="server" Width="105px" AutoPostBack="True"
                                                        OnSelectedIndexChanged="ddlDepEntrega_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <span class="Form_TextoObligatorio">*</span>
                                                    <asp:RequiredFieldValidator ID="rfvDepEntrega" runat="server" Text="*" ControlToValidate="ddlDepEntrega"
                                                        Display="None" ErrorMessage="Seleccione departamento de entrega." ForeColor="Transparent"
                                                        SetFocusOnError="true" InitialValue="-1" ValidationGroup="valExpedir">
                                                    </asp:RequiredFieldValidator>
                                                </td>
                                                <td style="width: 10px;">
                                                    <asp:UpdateProgress ID="upsDepEntrega" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upDepEntrega">
                                                        <ProgressTemplate>
                                                            <asp:Image ID="imgLoadDepEnt" runat="server" SkinID="sknImgLoading" Width="10px"
                                                                Height="10px" />
                                                        </ProgressTemplate>
                                                    </asp:UpdateProgress>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="Form_TextoDer" style="width: 64px;">
                                Provincia:
                            </td>
                            <td style="width: 150px;">
                                <asp:UpdatePanel ID="upProvEntrega" runat="server">
                                    <ContentTemplate>
                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                            <tr>
                                                <td style="width: 100px;">
                                                    <asp:DropDownList ID="ddlProvEntrega" runat="server" Width="105px" AutoPostBack="True"
                                                        OnSelectedIndexChanged="ddlProvEntrega_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <span class="Form_TextoObligatorio">*</span>
                                                    <asp:RequiredFieldValidator ID="rfvProvEntrega" runat="server" Text="*" ControlToValidate="ddlProvEntrega"
                                                        Display="None" ErrorMessage="Seleccione provincia de entrega." ForeColor="Transparent"
                                                        SetFocusOnError="true" InitialValue="-1" ValidationGroup="valExpedir">
                                                    </asp:RequiredFieldValidator>
                                                </td>
                                                <td style="width: 10px;">
                                                    <asp:UpdateProgress ID="upsProvEntrega" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upProvEntrega">
                                                        <ProgressTemplate>
                                                            <asp:Image ID="imgLoadProvEnt" runat="server" SkinID="sknImgLoading" Width="10px"
                                                                Height="10px" />
                                                        </ProgressTemplate>
                                                    </asp:UpdateProgress>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlDepEntrega" EventName="selectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                            <td class="Form_TextoDer" style="width: 80px">
                                Distrito:
                            </td>
                            <td>
                                <asp:UpdatePanel ID="upDistEntrega" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlDistEntrega" runat="server" Width="205px">
                                        </asp:DropDownList>
                                        <span class="Form_TextoObligatorio">*</span>
                                        <asp:RequiredFieldValidator ID="rfvDistEntrega" runat="server" ControlToValidate="ddlDistEntrega"
                                            Display="None" ErrorMessage="Seleccione distrito de entrega." ForeColor="Transparent"
                                            InitialValue="-1" SetFocusOnError="true" Text="*" ValidationGroup="valExpedir">
                                        </asp:RequiredFieldValidator>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlProvEntrega" EventName="selectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer">
                            </td>
                            <td colspan="5">
                                <asp:Label ID="lblInspeccion" runat="server">                                    
                                </asp:Label> 
                            </td>                            
                        </tr>
                    </table>
                    <br />
                    <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="Form_SubTitulo" colspan="6">
                                INFORMACIÓN DEL PLAN
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoIzq" style="width: 60px;">
                                <asp:Image ID="Image1" runat="server" SkinID="sknImgUsuario" />
                            </td>
                            <td class="Form_TextoIzq" colspan="5">
                                <asp:Label ID="lblSpechPlan" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer" style="width: 107px">
                                Plan:
                            </td>
                            <td class="Form_TextoIzq" style="width: 150px;">
                                <asp:DropDownList ID="ddlPlan" runat="server" Width="105px" OnSelectedIndexChanged="ddlPlan_SelectedIndexChanged"
                                    AutoPostBack="True">
                                </asp:DropDownList>
                                <span class="Form_TextoObligatorio">*</span>
                            </td>
                            <td class="Form_TextoDer" style="width: 64px;">
                                Frecuencia:
                            </td>
                            <td class="Form_TextoIzq" style="width: 150px;">
                                <asp:DropDownList ID="ddlFrecuencia" runat="server" Width="105px" OnSelectedIndexChanged="ddlFrecuencia_SelectedIndexChanged"
                                    AutoPostBack="True">
                                </asp:DropDownList>
                                <span class="Form_TextoObligatorio">*</span>
                                <asp:RequiredFieldValidator ID="rfvFrecuencia" runat="server" Text="*" ControlToValidate="ddlFrecuencia"
                                    Display="None" ErrorMessage="Seleccione frecuencia de pago." ForeColor="Transparent"
                                    InitialValue="-1" SetFocusOnError="True" ValidationGroup="valExpedir"></asp:RequiredFieldValidator>
                            </td>
                            <td class="Form_TextoDer" style="width: 80px;">
                                Moneda Prima:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:DropDownList ID="ddlMonPrima" runat="server" Width="205px">
                                </asp:DropDownList>
                                <span class="Form_TextoObligatorio">*</span>
                                <asp:RequiredFieldValidator ID="rfvMonPrima" runat="server" Text="*" ControlToValidate="ddlMonPrima"
                                    Display="None" ErrorMessage="Seleccione moneda prima." ForeColor="Transparent"
                                    SetFocusOnError="True" InitialValue="-1" ValidationGroup="valExpedir"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="Form_TextoDer">
                                Valor de Cuota:
                            </td>
                            <td class="Form_TextoIzq">
                                <asp:UpdatePanel ID="upCotPrimCertificado" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtPrimTotal" runat="server" CssClass="Form_TextBoxMontoDisable"
                                            Width="100px" ReadOnly="true"></asp:TextBox>
                                        <span class="Form_TextoObligatorio">*</span>
                                        <cc1:FilteredTextBoxExtender ID="ftxtPrimTotal" runat="server" TargetControlID="txtPrimTotal"
                                            ValidChars="1234567890,.">
                                        </cc1:FilteredTextBoxExtender>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlPlan" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="ddlFrecuencia" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                <asp:RequiredFieldValidator ID="rfvPrimTotal" runat="server" Text="*" ControlToValidate="txtPrimTotal"
                                    Display="None" ErrorMessage="No existe monto de prima." ForeColor="Transparent"
                                    SetFocusOnError="True" ValidationGroup="valExpedir"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="Form_TextoIzq">
                                <span class="Form_TextoObligatorio">&nbsp;(*)Campos Obligatorios</span>
                            </td>
                            <td class="Form_TextoDer">
                                <asp:Button ID="btnFinalizar" runat="server" Text="Finalizar" Width="90px" CausesValidation="true"
                                    Enabled="false" onclick="btnFinalizar_Click" />
                                <asp:Button ID="btnImprimir" runat="server" Text="Imprimir" Width="90px" CausesValidation="true"
                                    Enabled="false" OnClientClick="return showModalPopup('bmpCotDocCertificado');" />
                                <asp:Button ID="btnExpedir" runat="server" Text="Expedir" Width="90px" CausesValidation="true"
                                    ValidationGroup="valExpedir" OnClick="btnExpedir_Click" />
                            </td>
                        </tr>
                    </table>
                    <asp:ValidationSummary ID="vsExpedir" runat="server" ShowMessageBox="true" ShowSummary="false"
                        ValidationGroup="valExpedir" />
                    <asp:HiddenField ID="hfIdPeriodo" runat="server" />
                    <asp:HiddenField ID="hfNroCotizacion" runat="server" />
                    <asp:HiddenField ID="hfIdCertificado" runat="server" />
                    <asp:HiddenField ID="hfIdProducto" runat="server" />
                </asp:Panel>
                <asp:Panel ID="pnCondiciones" runat="server" Visible="false">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td>
                                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td class="Form_SubTitulo" colspan="2">
                                            PRINCIPALES EXCLUSIONES
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 50px; vertical-align:top;" >
                                            <asp:Image ID="Image2" runat="server" SkinID="sknImgUsuario" />
                                        </td>
                                        <td CssClass="Form_Nota" style="vertical-align:top;" >                                            
                                            <asp:Label ID="lblExcluciones" runat="server" ></asp:Label>                                                                                
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td class="Form_SubTitulo" colspan="2">
                                            IMPORTANTE
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 50px; vertical-align:top;" >
                                            <asp:Image ID="Image3" runat="server" SkinID="sknImgUsuario" />
                                        </td>
                                        <td CssClass="Form_Nota" style="vertical-align:top;" >                                            
                                            <asp:Label ID="lblImportante" runat="server" ></asp:Label>                                                                                
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td class="Form_SubTitulo" colspan="2">
                                            DESPEDIDA
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 50px; vertical-align:top;" >
                                            <asp:Image ID="Image4" runat="server" SkinID="sknImgUsuario" />
                                        </td>
                                        <td CssClass="Form_Nota" style="vertical-align:top;" >                                            
                                            <asp:Label ID="lblDespedida" runat="server" ></asp:Label>                                                                                
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br />
                </asp:Panel>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnDocCertificado" runat="server" Width="700px" CssClass="Modal_Panel"
        Style="display: none;">
        <div class="Modal_Head" style="width: 100%;">
            <div id="div7" style="float: left;">
                Imprimir Certificado
            </div>
            <div id="div8" style="float: right;">
                <img alt="Click para cerrar." class="Modal_CloseButton" onclick="hideModalPopup('bmpCotDocCertificado');"
                    style="cursor: hand;" />
            </div>
        </div>
        <div id="div9" class="Modal_Body" style="height: 480px;">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="Form_TextoIzq" style="width: 60px;">
                        <asp:Image ID="imgSpeechBen" runat="server" SkinID="sknImgUsuario" />
                    </td>
                    <td>
                        <asp:Panel ID="pnSpeechCert" runat="server" ScrollBars="Vertical" Width="100%" Height="100px">
                            <table>
                                <tr>
                                    <td class="Form_Nota">
                                        <asp:Label ID="lblSpeechBen" runat="server"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblSpeechCob" runat="server"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblSpeechCoor" runat="server"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblSpeechDesp" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <br />
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="Form_tdBorde">
                    </td>
                    <td>
                        <rsweb:ReportViewer ID="rwCertificado" runat="server" BackColor="#CCCCCC" Font-Names="Verdana"
                            Font-Size="8pt" InteractiveDeviceInfos="(Colección)" WaitMessageFont-Names="Verdana"
                            WaitMessageFont-Size="14pt" Width="98%">
                            <LocalReport ReportPath="rdlc\Rimac\CertificadoRIMAC.rdlc">
                                <DataSources>
                                    <rsweb:ReportDataSource DataSourceId="obsCertificado" Name="BECertificado" />
                                    <rsweb:ReportDataSource DataSourceId="odsVehiculo" Name="BEVehiculo" />
                                    <rsweb:ReportDataSource DataSourceId="odsCliente" Name="BECliente" />
                                </DataSources>
                            </LocalReport>
                        </rsweb:ReportViewer>
                        <asp:ObjectDataSource ID="obsCertificado" runat="server" SelectMethod="Obtener" TypeName="AONAffinity.Motor.BusinessLogic.Bais.BLCertificado">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfIdCertificado" DefaultValue="0" Name="pcIdCertificado"
                                    PropertyName="Value" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource ID="odsCliente" runat="server" SelectMethod="ObtenerxCertificado"
                            TypeName="AONAffinity.Motor.BusinessLogic.BLCliente">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfIdCertificado" DefaultValue="0" Name="pcIdCertificado"
                                    PropertyName="Value" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource ID="odsVehiculo" runat="server" SelectMethod="ObtenerxCertificado"
                            TypeName="AONAffinity.Motor.BusinessLogic.BLVehiculo">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfIdCertificado" DefaultValue="0" Name="pcIdCertificado"
                                    PropertyName="Value" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </td>
                    <td class="Form_tdBorde">
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:Label ID="lblCotDocCertificado" runat="server" Style="display: none;"></asp:Label>
    <cc1:ModalPopupExtender ID="mpeCotDocCertificado" runat="server" Enabled="true" TargetControlID="lblCotDocCertificado"
        BehaviorID="bmpCotDocCertificado" PopupControlID="pnDocCertificado" BackgroundCssClass="Modal_Background">
    </cc1:ModalPopupExtender>
</asp:Content>
