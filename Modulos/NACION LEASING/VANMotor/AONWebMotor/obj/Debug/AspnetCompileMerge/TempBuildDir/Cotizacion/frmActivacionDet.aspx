﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmActivacionDet.aspx.cs"
    Inherits="AONWebMotor.Cotizacion.ActivacionDetalle" %>

<%@ Register Assembly="DevExpress.Web.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>
<%@ Register assembly="DevExpress.Web.v8.3" namespace="DevExpress.Web.ASPxRoundPanel" tagprefix="dxrp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Detalle Cotización</title>
</head>
<body style="width: 670px;">
    <form id="form1" runat="server" style="width: 100%;">
    <h1>
        Detalle Cotización</h1>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <br />
    <table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <dxrp:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" Width="100%" HeaderText="Cotización">
                    <PanelCollection>
                        <dxrp:PanelContent ID="PanelContent1" runat="server" supportsdisabledattribute="True">
                            <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td class="Form_TextoDer">
                                        Nro Cotización:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtNtoCot" runat="server" SkinID="txtReadOnly" ReadOnly="true"
                                            Width="70px">
                                        </asp:TextBox>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Cliente:
                                    </td>
                                    <td class="Form_TextoIzq" colspan="3">
                                        <asp:TextBox ID="txtCliente" runat="server" SkinID="txtReadOnly" ReadOnly="true" Width="200px">
                                        </asp:TextBox>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Nro. Doc.
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtDocumento" runat="server" SkinID="txtReadOnly" ReadOnly="true"
                                            Width="70px">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="Form_TextoDer">
                                        Marca:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtMarca" runat="server" SkinID="txtReadOnly" ReadOnly="true"
                                            Width="70px">
                                        </asp:TextBox>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Modelo:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtModelo" runat="server" SkinID="txtReadOnly" ReadOnly="true" Width="70px">
                                        </asp:TextBox>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Año:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtAnio" runat="server" SkinID="txtReadOnly" ReadOnly="true"
                                            Width="50px">
                                        </asp:TextBox>
                                    </td>
                                    <td class="Form_TextoDer">
                                        Valor:
                                    </td>
                                    <td class="Form_TextoIzq">
                                        <asp:TextBox ID="txtValor" runat="server" SkinID="txtReadOnly" ReadOnly="true"
                                            Width="70px">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </dxrp:PanelContent>
                    </PanelCollection>
                </dxrp:ASPxRoundPanel>
                <br />
                
                <div style="width: 100%; overflow: auto; text-align: center;">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                             <asp:GridView ID="gvDetalle" runat="server" SkinID="sknGridView" Width="98%" EnableModelValidation="True"
                        OnRowCommand="gvDetalle_RowCommand" 
                        OnRowDataBound="gvDetalle_RowDataBound" AllowPaging="True" 
                        onpageindexchanging="gvDetalle_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="Correlativo">
                                <ItemTemplate>
                                    <asp:Label ID="lblCorrelativo" runat="server" Text='<%# Bind("Correlativo") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Correlativo") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="DesAsegurador" HeaderText="Aseguradora">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DesProducto" HeaderText="Producto" />
                            <asp:BoundField DataField="PrimaMensual" HeaderText="Prima Mensual" 
                                DataFormatString="{0:#,#0.00}" />
                            <asp:BoundField DataField="PrimaTotal" HeaderText="Prima Total" 
                                DataFormatString="{0:#,#0.00}" />
                            <asp:TemplateField HeaderText="Estado">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbxEstado" runat="server" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnEditar" runat="server" CommandName="Editar" SkinID="sknImgEditar" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <br />
                <asp:Label ID="lblResultado" runat="server" Text="No se encontraron resultados."
                    Visible="false" CssClass="Form_MsjLabel"></asp:Label>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
