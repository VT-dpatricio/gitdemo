﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true" CodeBehind="frmMantModelo.aspx.cs" Inherits="AONWebMotor.Mantenimiento.frmMantModelo" %>


<%@ Register src="~/Controles/Cargando.ascx" tagname="Cargando" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
             <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscar">
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="Form_SubTitulo" colspan="6">
                            MANTENIMIENTO MODELO
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer" style="width: 58px">
                            Marca:
                        </td>
                        <td class="Form_TextoIzq" style="width: 150px">
                            <asp:DropDownList id="ddlMarca" runat="server" Width="150px" 
                                AutoPostBack="True" onselectedindexchanged="ddlMarca_SelectedIndexChanged" >
                                
                            </asp:DropDownList>
                        </td>
                        <td class="Form_TextoDer" style="width: 50px">
                            Buscar:
                        </td>
                        <td class="Form_TextoIzq" style="width: 264px">
                            <asp:TextBox ID="txtDescripcion" runat="server" Width="250px"  ></asp:TextBox>
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" 
                                onclick="btnBuscar_Click"/> 
                        </td>
                        <td></td>
                    </tr>
                </table>
              </asp:Panel>
                <br />
                <div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView ID="gvModelo" runat="server" AllowPaging="True" 
                                EnableModelValidation="True" onpageindexchanging="gvModelo_PageIndexChanging" 
                                PageSize="15" SkinID="sknGridView" Width="100%" 
                                onrowcancelingedit="gvModelo_RowCancelingEdit" 
                                onrowdatabound="gvModelo_RowDataBound" onrowediting="gvModelo_RowEditing">
                                <Columns>
                                    <asp:TemplateField HeaderText="idModelo" Visible="False">
                                        <EditItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("idModelo") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("idModelo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Modelo">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Descripcion") %>' Width="200px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                ControlToValidate="TextBox1" ErrorMessage="Ingrese el Modelo" 
                                                SetFocusOnError="True" ValidationGroup="vgGrabar">*</asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>                                             
                                        </ItemTemplate>
                                        <ItemStyle Width="220px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Clase">
                                        <EditItemTemplate>                                            
                                            <asp:DropDownList ID="ddlClase" runat="server" Width="150px">
                                            </asp:DropDownList><asp:Label ID="lblidClase" runat="server" Text='<%# Bind("idClase") %>' Visible="false"></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("DesClase") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="150px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tipo">
                                        <EditItemTemplate>                                            
                                            <asp:DropDownList ID="ddlTipoVehiculo" runat="server" Width="200px">
                                            </asp:DropDownList><asp:Label ID="lblidTipoVehiculo" runat="server" Text='<%# Bind("idTipoVehiculo") %>' Visible="false"></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("DesTipoVehiculo") %>'></asp:Label>
                                            
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cod. Externo">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("CodExterno") %>' Width="70px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("CodExterno") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="75px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Asegurable">
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="CheckBox1" runat="server" 
                                                Checked='<%# Bind("Asegurable") %>' />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Asegurable") %>' 
                                                Enabled="false" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="70px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Estado">
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="CheckBox2" runat="server" 
                                                Checked='<%# Bind("EstadoRegistro") %>' />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBox2" runat="server" 
                                                Checked='<%# Bind("EstadoRegistro") %>' Enabled="false" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                                    </asp:TemplateField>
                                     <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ibtnModificar" runat="server" CommandName="Edit" ToolTip="Modificar"
                                                ImageUrl="~/Img/editar.gif" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:ImageButton ID="ibtnGrabar" runat="server" ImageUrl="~/Img/Save.gif" ToolTip="Grabar" 
                                                onclick="ibtnGrabar_Click"  ValidationGroup="vgGrabar"/>
                                            &nbsp;<asp:ImageButton ID="ibtnCancelar" runat="server" CommandName="cancel" CausesValidation="false" 
                                                ImageUrl="~/Img/b_cancelar.gif" ToolTip="Cancelar" />
                                        </EditItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="40px" />
                                    </asp:TemplateField>



                                </Columns>
                                <EmptyDataTemplate>
                                    No se encontraron registros...
                                </EmptyDataTemplate>
                                <HeaderStyle Height="20px" />
                                <RowStyle Height="25px" />
                            </asp:GridView>
                               <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                ShowMessageBox="True" ShowSummary="false" ValidationGroup="vgGrabar" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlMarca" 
                                EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>

                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0">
                        <ProgressTemplate>
                            <uc1:Cargando ID="Cargando1" runat="server" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
</asp:Content>
