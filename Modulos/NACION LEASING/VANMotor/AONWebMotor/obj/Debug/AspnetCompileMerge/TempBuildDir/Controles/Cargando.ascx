﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Cargando.ascx.cs" Inherits="AONWebMotor.Controles.Cargando" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Panel ID="PCar" runat="server" Width="144px">
<table class="divLoading" style="width: 121px; margin-left: 0px; z-index:1000000000;">
    <tr>
        <td>
            <asp:Image ID="Image1" runat="server" ImageUrl="~/Img/loading.gif" /></td>
        <td colspan="2" style="width: 183px">
            Procesando...</td>
    </tr>
</table>
</asp:Panel>

<asp:AlwaysVisibleControlExtender ID="avCargando" runat="server" HorizontalSide="Center" VerticalSide="Middle" TargetControlID="PCar">
</asp:AlwaysVisibleControlExtender>
