﻿<%@ Page Title="Activación de Cotización" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true"
    CodeBehind="frmActivacion.aspx.cs" Inherits="AONWebMotor.Cotizacion.frmActivacion" %>

<%@ Register Assembly="DevExpress.Web.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxrp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="DevExpress.Web.v8.3" namespace="DevExpress.Web.ASPxRoundPanel" tagprefix="dxrp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
        function pageLoad() {
            this.ValidarFiltro();
        }
        function ValidarFiltro() {
            if (document.getElementById('<%=ddlTipoBusq.ClientID %>').value == 0) {
                document.getElementById('<%=lblNro.ClientID %>').style.display = '';
                document.getElementById('<%=txtDNI.ClientID %>').style.display = '';

                document.getElementById('<%=lblNroPlaca.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtNroPlaca.ClientID %>').style.display = 'none';
            }
            if (document.getElementById('<%=ddlTipoBusq.ClientID %>').value == 2) {
                document.getElementById('<%=txtDNI.ClientID %>').style.display = 'none';
                document.getElementById('<%=lblNro.ClientID %>').style.display = 'none';

                document.getElementById('<%=lblNroPlaca.ClientID %>').style.display = '';
                document.getElementById('<%=txtNroPlaca.ClientID %>').style.display = '';

                document.getElementById('<%=lblNro.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtDNI.ClientID %>').style.display = 'none';
            }
        }

        function MostrarPCDetalle(idCot, cli, nroDoc, marca, modelo, anio, valor) {
            pcCertificado.contentUrl = 'frmActivacionDet.aspx' + '?idCot=' + idCot + '&cli=' + cli + '&nroDoc=' + nroDoc + '&marca=' + marca + '&modelo=' + modelo + '&anio=' + anio + '&valor=' + valor;
            pcCertificado.Show();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>
        Activación de Cotización</h1>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td>
                            <dxrp:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" Width="100%" HeaderText="Filtros de Búsqueda">
                                <PanelCollection>
                                    <dxrp:PanelContent ID="PanelContent1" runat="server" supportsdisabledattribute="True">
                                        <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td class="Form_TextoDer">
                                                    Buscar Por:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:DropDownList ID="ddlTipoBusq" runat="server" Width="180px" onchange="return ValidarFiltro();">
                                                        <asp:ListItem Text="DOCUMENTO" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="N° CHAPA" Value="2"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    <asp:Label ID="lblNro" runat="server" Text="N° Documento:" MaxLength="50"></asp:Label>
                                                    <asp:Label ID="lblNroPlaca" runat="server" Text="N° Chapa:" MaxLength="50" Style="display: none;"></asp:Label>
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtDNI" runat="server"  MaxLength="20" Width="100px"></asp:TextBox>
                                                    <asp:TextBox ID="txtNroPlaca" runat="server" MaxLength="20"
                                                        Style="display: none;" Width="100px"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="ftxtDNI" runat="server" TargetControlID="txtDNI"
                                                        ValidChars="-" FilterType="Numbers, UppercaseLetters, LowercaseLetters, Custom">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                    </dxrp:PanelContent>
                                </PanelCollection>
                            </dxrp:ASPxRoundPanel>
                        </td>
                        <td style="vertical-align: top;" class="Form_TextoDer">
                            <br />
                            <br />
                            <br />
                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click" />
                        </td>
                    </tr>
                </table>
                <br />
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div style="text-align: center; width:100%">
                            <asp:GridView ID="gvCotizacion" runat="server" SkinID="sknGridView" Width="98%"
                                AllowPaging="True" EnableModelValidation="True" OnRowCommand="gvCotizacion_RowCommand"
                                OnRowDataBound="gvCotizacion_RowDataBound" OnPageIndexChanging="gvCotizacion_PageIndexChanging">
                                <Columns>
                                    <asp:BoundField DataField="NroCotizacion" HeaderText="Nro Cotización">
                                        <HeaderStyle Width="80px" />
                                        <ItemStyle Width="80px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NombreApellido" HeaderText="Cliente">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NroDocumento" HeaderText="Nro. Documento">
                                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Nro Rodaje" DataField="NroPlaca">
                                        <HeaderStyle Width="80px" HorizontalAlign="Center" />
                                        <ItemStyle Width="80px" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <a href="javascript:MostrarPCDetalle('<%#Eval("NroCotizacion")%>','<%#Eval("NombreApellido")%>','<%#Eval("NroDocumento")%>','<%#Eval("DesMarca")%>','<%#Eval("DesModelo")%>','<%#Eval("AnioFab")%>','<%#Eval("ValOriVehiculo")%>')"
                                                style="">
                                                <img src="../App_Themes/Imagenes/Ico/edit.gif" alt="" style="border:0px;" />
                                            </a>
                                        </ItemTemplate>
                                        <HeaderStyle Width="50px" />
                                        <ItemStyle Width="50px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DesMarca" HeaderText="Marca" Visible="False" />
                                    <asp:BoundField DataField="DesModelo" HeaderText="Modelo" Visible="False" />
                                    <asp:BoundField DataField="AnioFab" HeaderText="Anio" Visible="False" />
                                    <asp:BoundField DataField="ValOriVehiculo" HeaderText="Valor" Visible="False">
                                        <HeaderStyle HorizontalAlign="Center" Width="80px" />
                                        <ItemStyle HorizontalAlign="Center" Width="80px" />
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:Label ID="lblResultado" runat="server" Text="No se encontraron resultados."
                    Visible="false" CssClass="Form_MsjLabel"></asp:Label>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <dxpc:ASPxPopupControl ID="pcCertificado" runat="server" AllowDragging="True" AllowResize="True"
        CloseAction="CloseButton" ContentUrl="frmActivacionDet.aspx" ClientInstanceName="pcCertificado"
        EnableViewState="False" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ShowFooter="False" Width="700px" Height="500px" LoadContentViaCallback="OnFirstShow"
        HeaderText="" EnableHierarchyRecreation="True" BackColor="Gray" Modal="true"
        ModalBackgroundStyle-Opacity="80" ModalBackgroundStyle-BackColor="Black">
    </dxpc:ASPxPopupControl>
</asp:Content>
