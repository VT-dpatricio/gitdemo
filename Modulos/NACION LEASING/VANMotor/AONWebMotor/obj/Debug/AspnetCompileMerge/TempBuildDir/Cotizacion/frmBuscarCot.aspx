﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true"
    CodeBehind="frmBuscarCot.aspx.cs" Inherits="AONWebMotor.Cotizacion.frmBuscarCot" %>

<%@ Register Assembly="DevExpress.Web.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxrp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
        function pageLoad() {
            this.ValidarFiltro();
        }
        function ValidarFiltro() {
            if (document.getElementById('<%=ddlTipoBusq.ClientID %>').value == 0) {
                document.getElementById('<%=lblNro.ClientID %>').style.display = '';
                document.getElementById('<%=txtDNI.ClientID %>').style.display = '';

                document.getElementById('<%=lblNroPlaca.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtNroPlaca.ClientID %>').style.display = 'none';

                document.getElementById('<%=lblApePat.ClientID %>').style.display = 'none';
                document.getElementById('<%=lblApeMat.ClientID %>').style.display = 'none';
                document.getElementById('<%=lblPriNom.ClientID %>').style.display = 'none';
                document.getElementById('<%=lblSegNom.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtApePat.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtApeMat.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtPriNom.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtSegNom.ClientID %>').style.display = 'none';
            }
            if (document.getElementById('<%=ddlTipoBusq.ClientID %>').value == 1) {
                document.getElementById('<%=txtDNI.ClientID %>').style.display = 'none';
                document.getElementById('<%=lblNro.ClientID %>').style.display = 'none';

                document.getElementById('<%=lblNroPlaca.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtNroPlaca.ClientID %>').style.display = 'none';

                document.getElementById('<%=lblApePat.ClientID %>').style.display = '';
                document.getElementById('<%=lblApeMat.ClientID %>').style.display = '';
                document.getElementById('<%=lblPriNom.ClientID %>').style.display = '';
                document.getElementById('<%=lblSegNom.ClientID %>').style.display = '';
                document.getElementById('<%=txtApePat.ClientID %>').style.display = '';
                document.getElementById('<%=txtApeMat.ClientID %>').style.display = '';
                document.getElementById('<%=txtPriNom.ClientID %>').style.display = '';
                document.getElementById('<%=txtSegNom.ClientID %>').style.display = '';
            }
            if (document.getElementById('<%=ddlTipoBusq.ClientID %>').value == 2) {
                document.getElementById('<%=txtDNI.ClientID %>').style.display = 'none';
                document.getElementById('<%=lblNro.ClientID %>').style.display = 'none';

                document.getElementById('<%=lblNroPlaca.ClientID %>').style.display = '';
                document.getElementById('<%=txtNroPlaca.ClientID %>').style.display = '';

                document.getElementById('<%=txtDNI.ClientID %>').style.display = 'none';
                document.getElementById('<%=lblApePat.ClientID %>').style.display = 'none';
                document.getElementById('<%=lblApeMat.ClientID %>').style.display = 'none';
                document.getElementById('<%=lblPriNom.ClientID %>').style.display = 'none';
                document.getElementById('<%=lblSegNom.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtApePat.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtApeMat.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtPriNom.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtSegNom.ClientID %>').style.display = 'none';
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Buscar Cotización</h1>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td>
                            <dxrp:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" Width="100%" HeaderText="Filtros de Búsqueda">
                                <PanelCollection>
                                    <dxrp:PanelContent ID="PanelContent1" runat="server" supportsdisabledattribute="True">
                                        <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td class="Form_TextoDer">
                                                    Buscar Por:
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:DropDownList ID="ddlTipoBusq" runat="server" Width="180px" onchange="return ValidarFiltro();">
                                                        <asp:ListItem Text="DOCUMENTO IDENTIDAD" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="NOMBRE Y APELLIDOS" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="NRO RODAJE/PLACA" Value="2"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    <asp:Label ID="lblNro" runat="server" Text="Nro. Documento:" MaxLength="50"></asp:Label>
                                                    <asp:Label ID="lblNroPlaca" runat="server" Text="Nro Rodaje/Placa:" MaxLength="50"
                                                        Style="display: none;"></asp:Label>
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtDNI" runat="server" CssClass="Form_TextBox" MaxLength="8" Width="100px"></asp:TextBox>
                                                    <asp:TextBox ID="txtNroPlaca" runat="server" CssClass="Form_TextBox" MaxLength="12"
                                                        Style="display: none;" Width="100px"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="ftxtDNI" runat="server" TargetControlID="txtDNI"
                                                        ValidChars="0123456789">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Form_TextoDer">
                                                    <asp:Label ID="lblApePat" runat="server" Text="Apellido Paterno:" MaxLength="50"
                                                        Style="display: none;"></asp:Label>
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtApePat" runat="server" CssClass="Form_TextBox" MaxLength="50"
                                                        Width="100px" Style="display: none;"></asp:TextBox>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    <asp:Label ID="lblApeMat" runat="server" Text="Apellido Materno:" MaxLength="50"
                                                        Style="display: none;"></asp:Label>
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtApeMat" runat="server" CssClass="Form_TextBox" MaxLength="50"
                                                        Width="100px" Style="display: none;"></asp:TextBox>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Form_TextoDer">
                                                    <asp:Label ID="lblPriNom" runat="server" Text="Primer Nombre:" Style="display: none;"></asp:Label>
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtPriNom" runat="server" CssClass="Form_TextBox" Style="display: none;"
                                                        Width="100px"></asp:TextBox>
                                                </td>
                                                <td class="Form_TextoDer">
                                                    <asp:Label ID="lblSegNom" runat="server" Text="Segundo Nombre:" Style="display: none;"></asp:Label>
                                                </td>
                                                <td class="Form_TextoIzq">
                                                    <asp:TextBox ID="txtSegNom" runat="server" CssClass="Form_TextBox" Style="display: none;"
                                                        Width="100px"></asp:TextBox>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                    </dxrp:PanelContent>
                                </PanelCollection>
                            </dxrp:ASPxRoundPanel>
                        </td>
                        <td style="vertical-align: top;" class="Form_TextoDer">
                            <br />
                            <br />
                            <br />
                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click" />
                        </td>
                    </tr>
                </table>
                <br />
                <div id="dvTipo1" runat="server" style="text-align: center; overflow: auto; height: 95%;">
                    <asp:GridView ID="gvCotizacion" runat="server" SkinID="sknGridView" Width="100%"
                        AllowPaging="True" OnPageIndexChanging="gvCotizacion_PageIndexChanging" OnRowCommand="gvCotizacion_RowCommand"
                        OnRowDataBound="gvCotizacion_RowDataBound">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ibtnSelect" runat="server" CommandName="Select" SkinID="sknIbtnSiguiente" />
                                </ItemTemplate>
                                <HeaderStyle Width="25px" />
                                <ItemStyle HorizontalAlign="Center" Width="25px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="IdPeriodo" HeaderText="Periodo" Visible="False" />
                            <asp:BoundField DataField="NroCotizacion" HeaderText="Nro Cotización" Visible="False" />
                            <asp:BoundField DataField="PeriodoCotizacion" HeaderText="Cotización">
                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DesProducto" HeaderText="Producto">
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NombreApellido" HeaderText="Cliente">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NroDocumento" HeaderText="Nro. Documento">
                                <HeaderStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TelefonoDomicilio" HeaderText="Tel. Domicilio">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TelefonoMovil" HeaderText="Tel. Movil">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TelefonoOficina" HeaderText="Tel. Oficina">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </div>
                <asp:Label ID="lblResultado" runat="server" Text="No se encontraron resultados."
                    Visible="false" CssClass="Form_MsjLabel"></asp:Label>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hfTipo" runat="server" />
</asp:Content>
