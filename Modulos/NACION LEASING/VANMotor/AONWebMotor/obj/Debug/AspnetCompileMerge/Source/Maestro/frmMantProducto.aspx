﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/NewPrincipal.Master" AutoEventWireup="true" CodeBehind="frmMantProducto.aspx.cs" Inherits="AONWebMotor.Maestro.frmMatProducto" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v8.3" Namespace="DevExpress.Web.ASPxGridView"
    TagPrefix="dxwgv" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v8.3" Namespace="DevExpress.Web.ASPxRoundPanel"
    TagPrefix="dxrp" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="DevExpress.Web.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v8.3" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v8.3" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v8.3" Namespace="DevExpress.Web.ASPxPanel"
    TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v8.3" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Mantenimiento Producto</h1>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <dxtc:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="2" 
                    Width="100%">
                        <TabPages>
                            <dxtc:TabPage Name="tpCertificado" Text="Producto">
                                <ContentCollection>
                                    <dxw:ContentControl ID="ContentControl1" runat="server" SupportsDisabledAttribute="True">
                                        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" Width="100%" HeaderText="DATOS DEL PRODUCTO">
                                            <PanelCollection>
                                                <dxp:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" class="Form_Fondo">
                                                    <tr>
                                                        <td class="Form_TextoDer">
                                                            Producto:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtProducto" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="120px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Nro. Certificado:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtNroCertificado" runat="server" CssClass="Form_TextBoxDisable"
                                                                ReadOnly="true" Width="90px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Inicio Vigencia:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtIniVigencia" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="60px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Fin Vigencia:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtFinVigencia" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="60px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Form_TextoDer">
                                                            Estado:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="TextBox7" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Monto Asegurado:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtMontoAseg" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Fecha Venta:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtFecVenta" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="60px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Digitación:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtFecDigitacion" runat="server" 
                                                                CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="60px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Form_TextoDer">
                                                            Informador:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="TextBox3" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="120px"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Oficina:
                                                        </td>
                                                        <td class="Form_TextoIzq" colspan="2">
                                                            <asp:TextBox ID="TextBox4" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                        </td>
                                                    </tr>
                                                </table>
                                                </dxp:PanelContent>
                                            </PanelCollection>
                                        </dxrp:ASPxRoundPanel>
                                    </dxw:ContentControl>
                                </ContentCollection>
                            </dxtc:TabPage>
                            <dxtc:TabPage Name="tpCertificado" Text="Producto Marca">
                                <ContentCollection>
                                    <dxw:ContentControl ID="ContentControl2" runat="server" SupportsDisabledAttribute="True">
                                        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" Width="100%" HeaderText="MARCAS DEL PRODUCTO">
                                            <PanelCollection>
                                                <dxp:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True">
                                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" class="Form_Fondo">
                                                    <tr>
                                                        <td class="Form_TextoDer">
                                                            Código Producto:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="TextBox1" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Producto:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="TextBox2" runat="server" CssClass="Form_TextBoxDisable"
                                                                ReadOnly="true" Width="250px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">                                                            
                                                        </td>
                                                        <td class="Form_TextoIzq">                                                            
                                                        </td>
                                                        <td class="Form_TextoDer">                                                            
                                                        </td>
                                                        <td class="Form_TextoIzq">                                                            
                                                        </td>
                                                    </tr>                                                                                                                                                            
                                                </table>
                                                <br />
                                                <div style="text-align: center; overflow: auto; height: 95%;">
                                                    <asp:GridView ID="gvMarca" runat="server" EnableModelValidation="True" 
                                                        SkinID="sknGridView" AllowPaging="True" AutoGenerateColumns="False" 
                                                        OnPageIndexChanging="gvMarca_PageIndexChanging" 
                                                        OnRowCommand="gvMarca_RowCommand" OnRowDataBound="gvMarca_RowDataBound">
                                                        <Columns>
                                                            <asp:BoundField DataField="idMarca" HeaderText="Id">
                                                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Descripcion" HeaderText="Descripción">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="Código Externo">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("CodExterno") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtCodExterno" runat="server" CssClass="Form_TextBox" 
                                                                        Width="80px"></asp:TextBox>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="100px" />
                                                                <ItemStyle Width="100px" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="UsuarioCreacion" HeaderText="Usuario Cre.">
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="FechaCreacion" HeaderText="Fec. Creación">
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="Usuario Mod.">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="TextBox4" runat="server" 
                                                                        Text='<%# Bind("UsuarioModificacion") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblUsrMod" runat="server" 
                                                                        Text='<%# Bind("UsuarioModificacion") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Fec. Modificación">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="TextBox5" runat="server" 
                                                                        Text='<%# Bind("FechaModificacion") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFecMod" runat="server" 
                                                                        Text='<%# Bind("FechaModificacion") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Activo">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("EstadoRegistro") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="cbxEstado" runat="server" />
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Editar">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="ibtnEditar" runat="server" CommandName="Editar" 
                                                                        SkinID="sknImgEditar" ToolTip="Click para editar." />
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>                                                                                                   
                                                </dxp:PanelContent>
                                            </PanelCollection>
                                        </dxrp:ASPxRoundPanel>
                                    </dxw:ContentControl>
                                </ContentCollection>
                            </dxtc:TabPage>
                            <dxtc:TabPage  Name="tpCertificado" Text="Producto Categoria">
                                <ContentCollection>
                                    <dxw:ContentControl ID="ContentControl3" runat="server" SupportsDisabledAttribute="True">
                                        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel3" runat="server" Width="100%" HeaderText="CATEGORIAS DEL PRODUCTO">
                                            <PanelCollection> 
                                                <dxp:PanelContent>
                                                     
                                                </dxp:PanelContent>
                                            </PanelCollection>                                            
                                        </dxrp:ASPxRoundPanel>                                        
                                    </dxw:ContentControl>                                        
                                </ContentCollection>
                            </dxtc:TabPage>
                        </TabPages>
                </dxtc:ASPxPageControl>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
</asp:Content>
