﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true" CodeBehind="frmMantModelo.aspx.cs" Inherits="AONWebMotor.Modelo.frmMantModelo" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="Form_SubTitulo" colspan="6">
                            MANTENIMIENTO MODELO
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Marca:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList id="ddlMarca" runat="server" Width="150px" >
                                
                            </asp:DropDownList>
                        </td>
                        <td class="Form_TextoDer">
                            Descripción:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtDescripcion" runat="server" CssClass="Form_TextBox" Width="250px"  ></asp:TextBox>
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" 
                                onclick="btnBuscar_Click"/> 
                        </td>
                        <td></td>
                    </tr>
                </table>
                <br />
                <div>
                    <asp:GridView ID="gvModelo" runat="server" SkinID="sknGridView" Width="100%"  >
                        <Columns>
                            <asp:BoundField DataField="Descripcion" HeaderText="Modelo" />
                            <asp:BoundField DataField="DesClase" HeaderText="Clase" />
                            <asp:BoundField DataField="DesTipoVehiculo" HeaderText="Tipo" />
                            <asp:BoundField DataField="Asegurable" HeaderText="Asegurable" />
                            <asp:BoundField DataField="CodExterno" HeaderText="Cod. Externo" />
                            <asp:BoundField DataField="UsuarioCreacion" HeaderText="Usr. Creación" />
                            <asp:BoundField DataField="FechaCreacion" HeaderText="Fec. Creación" />
                        </Columns>
                    </asp:GridView>
                </div>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
</asp:Content>
