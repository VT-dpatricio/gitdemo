﻿<%@ Page Title="Cotizador Seguro Vehícular" Language="C#" MasterPageFile="~/Master/Principal.Master"
    AutoEventWireup="true" CodeBehind="frmCotizador.aspx.cs" Inherits="AONWebMotor.Cotizacion.frmCotizador" %>

<%@ Register Assembly="DevExpress.Web.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v8.3" Namespace="DevExpress.Web.ASPxRoundPanel"
    TagPrefix="dxrp" %>
<%@ Register src="~/Controles/Cargando.ascx" tagname="Cargando" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../JS/jquery.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../JS2/jquery-ui.css" />
    <script src="../JS2/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../JS2/jquery-ui.js" type="text/javascript"></script>
    <script src="../JS2/jquery-imask-min.js" type="text/javascript"></script>
    <script src="../JS2/jquery.blockUI.js" type="text/javascript"></script> 
    <link rel="stylesheet" href="../resources/demos/style.css" />

    <script type="text/javascript">

        $(function () {
            $('#<%= txtValor.ClientID %>').iMask({
                type: 'number',
                decDigits: 2,
                decSymbol: ',',
                groupSymbol: '.'
            });
        });
    </script>



    <script language="javascript" type="text/jscript">
        function MostrarPCPrint(ruta, idCot) {
            pcCertificado.contentUrl = ruta + '?idCot=' + idCot;
            pcCertificado.Show();
        }

        function Plan(idPlan) {
            var hfPlan = 'ctl00_ContentPlaceHolder1_hfPlanes';

            if ($get('cbxPlan' + idPlan).checked) {

                if ($get(hfPlan).value == '') {
                    $get(hfPlan).value = idPlan;
                }
                else {
                    $get(hfPlan).value = $get(hfPlan).value + ',' + idPlan;
                }
            }
            else {
                if ($get(hfPlan).value != '') {
                    var valores = $get(hfPlan).value;
                    var arr = valores.split(',');

                    $get(hfPlan).value = '';

                    for (var i = 0; i < arr.length; i++) {
                        if (arr[i] != idPlan) {
                            if ($get(hfPlan).value == '') {
                                $get(hfPlan).value = arr[i];
                            }
                            else if ($get(hfPlan).value != '') {
                                $get(hfPlan).value = $get(hfPlan).value + ',' + arr[i];
                            }
                        }
                    }
                }
            }
        }
        function Asegurador(idAseg) {
            var hfAseg = 'ctl00_ContentPlaceHolder1_hfAseguradoras';

            if ($get('cbxAseg' + idAseg).checked) {

                if ($get(hfAseg).value == '') {
                    $get(hfAseg).value = idAseg;
                }
                else {
                    $get(hfAseg).value = $get(hfAseg).value + ',' + idAseg;
                }
            }
            else {
                if ($get(hfAseg).value != '') {
                    var valores = $get(hfAseg).value;
                    var arr = valores.split(',');

                    $get(hfAseg).value = '';

                    for (var i = 0; i < arr.length; i++) {
                        if (arr[i] != idAseg) {
                            if ($get(hfAseg).value == '') {
                                $get(hfAseg).value = arr[i];
                            }
                            else if ($get(hfAseg).value != '') {
                                $get(hfAseg).value = $get(hfAseg).value + ',' + arr[i];
                            }
                        }
                    }
                }
            }
        }

        function TextboxMonto(id) {
            //var num = document.getElementById(id);
            var txtvalue = id.value;
            var result;
            if (txtvalue.indexOf = '.' == -1) {
                result = txtvalue.substring(0, 6);
                result = parseFloat(result).toFixed(2);
            }
            else {
                result = parseFloat(txtvalue).toFixed(2);
            }
            if (result == 'NaN') {
                id.value = '0.00';
                return false;
            }
            else {
                result = result;
                id.value = result;
                return false;
            }
        }

        function SeleccionarCliente() {
            $.blockUI({ message: $('#frmSeleccionarCliente'),
                css: {
                    left: ($(window).width() - 700) / 2 + 'px',
                    width: '700px',
                    height: '260px',
                    border: '4px solid #750000'
                }
               ,
                growlCSS: {
                    cursor: 'pointer'
                }
            });
            $('#frmSeleccionarCliente').parent().appendTo($("#aspnetForm"));
        }

        function CerrarSelCliente() {
            $('#frmSeleccionarCliente').parent().appendTo($("body"));
            $.unblockUI();
            return false;
        }
    </script>
    <style type="text/css">
        span.reference
        {
            position: fixed;
            top: 10px;
            right: 10px;
            font-size: 9px;
        }
        span.reference a
        {
            /*color: #aaa;*/
            text-decoration: none;
            text-transform: uppercase;
            margin-left: 10px;
        }
        span.reference a:hover
        {
            /*color: #ddd;*/
        }
        h1.title
        {
            text-indent: -9000px;
            background: transparent url(title.png) no-repeat top left;
            width: 640px;
            height: 52px;
            position: absolute;
            top: 15px;
            left: 15px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>
        Cotizador Seguro Vehícular</h1>
    <asp:Panel ID="pnlInicio" runat="server" CssClass="Form_Visible">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td class="Form_tdBorde">
                </td>
                <td>
                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" Width="100%" HeaderText="Datos del Vehículo">
                        <PanelCollection>
                            <dxrp:PanelContent ID="PanelContent7" runat="server" supportsdisabledattribute="True">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" class="Form_Fondo">
                                    <tr>
                                        <td class="Form_TextoDer">
                                            Marca:
                                        </td>
                                        <td class="Form_TextoIzq">
                                            <asp:UpdatePanel ID="upMarca" runat="server">
                                                <ContentTemplate>
                                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                        <tr style="border: 0;">
                                                            <td style="width: 110px;">
                                                                <asp:DropDownList ID="ddlMarca" runat="server" Width="110px" AutoPostBack="True"
                                                                    OnSelectedIndexChanged="ddlMarca_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <span class="Form_TextoObligatorio">*</span>
                                                            </td>
                                                            <td style="width: 15px">
                                                                <asp:UpdateProgress ID="upsMarca" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upMarca">
                                                                    <ProgressTemplate>
                                                                        <asp:Image ID="imgLoadD" runat="server" SkinID="sknImgLoading" Width="10px" Height="10px" />
                                                                    </ProgressTemplate>
                                                                </asp:UpdateProgress>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td class="Form_TextoDer">
                                            Modelo:
                                        </td>
                                        <td class="Form_TextoIzq">
                                            <asp:UpdatePanel ID="upModelo" runat="server">
                                                <ContentTemplate>
                                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                        <tr style="border: 0;">
                                                            <td style="width: 110px;">
                                                                <asp:DropDownList ID="ddlModelo" runat="server" Width="200px">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <span class="Form_TextoObligatorio">*</span>
                                                            </td>
                                                            <td style="width: 15px">
                                                                <asp:UpdateProgress ID="upsProv" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="upModelo">
                                                                    <ProgressTemplate>
                                                                        <asp:Image ID="imgLoadP" runat="server" SkinID="sknImgLoading" Width="10px" Height="10px" />
                                                                    </ProgressTemplate>
                                                                </asp:UpdateProgress>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="ddlMarca" EventName="selectedIndexChanged" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td class="Form_TextoDer">
                                            Año:
                                        </td>
                                        <td class="Form_TextoIzq">
                                            <asp:DropDownList ID="ddlAnio" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Form_TextoDer">
                                            Valor:
                                        </td>
                                        <td class="Form_TextoIzq">
                                            <asp:TextBox ID="txtValor" runat="server" Width="90px"
                                                MaxLength="13" ></asp:TextBox>
                                            <span class="Form_TextoObligatorio">*</span>
                                            <cc1:FilteredTextBoxExtender ID="ftxteValor" runat="server" TargetControlID="txtValor"
                                                ValidChars="1234567890,.">
                                            </cc1:FilteredTextBoxExtender>
                                        </td>
                                    </tr>
                                </table>
                                <div style="display: none;">
                                    <asp:RequiredFieldValidator ID="rfvMarca" runat="server" ControlToValidate="ddlMarca"
                                        Display="None" ErrorMessage="Seleccione la marca del vehículo" ForeColor="Transparent"
                                        InitialValue="-1" SetFocusOnError="true" Text="*" ValidationGroup="valCotizar">
                                    </asp:RequiredFieldValidator>
                                    <asp:RequiredFieldValidator ID="rfvModelo" runat="server" ControlToValidate="ddlModelo"
                                        Display="None" ErrorMessage="Seleccione el modelo del vehículo." ForeColor="Transparent"
                                        InitialValue="-1" SetFocusOnError="true" Text="*" ValidationGroup="valCotizar">
                                    </asp:RequiredFieldValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Text="*"
                                        ControlToValidate="txtValor" Display="None" ErrorMessage="Ingrese el valor del vehículo."
                                        ForeColor="Transparent" SetFocusOnError="true" ValidationGroup="valCotizar">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </dxrp:PanelContent>
                        </PanelCollection>
                    </dxrp:ASPxRoundPanel>
                    <br />
                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel5" runat="server" Width="100%" HeaderText="Opciones de Cotización">
                        <PanelCollection>
                            <dxrp:PanelContent ID="PanelContent4" runat="server" supportsdisabledattribute="True">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" class="Form_Fondo">
                                    <tr>
                                        <td class="Form_TextoDer">
                                            Descuento:
                                        </td>
                                        <td class="Form_TextoIzq">
                                            <asp:DropDownList ID="ddlDescuento" runat="server" Width="600px">
                                            </asp:DropDownList>
                                        </td>
                                        
                                        
                                    </tr>
                                    <tr>
                                        <td class="Form_TextoDer">
                                            Recargo:
                                        </td>
                                        <td class="Form_TextoIzq">
                                            <asp:DropDownList ID="ddlRecargo" runat="server" Width="600px">
                                            </asp:DropDownList>
                                        </td>
                                     </tr>
                                     <tr>
                                        <td class="Form_TextoDer">
                                            Adicionales:
                                        </td>
                                        <td class="Form_TextoIzq">
                                            <asp:DropDownList ID="ddlAdicionales" runat="server" Width="600px">
                                            </asp:DropDownList>
                                        </td>
                                     </tr>

                                </table>
                            </dxrp:PanelContent>
                        </PanelCollection>
                    </dxrp:ASPxRoundPanel>
                    <br />
                    <div id="dvAsegurador" runat="server" style="width: 100%; text-align: center;">
                    </div>
                    <br />
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="Form_TextoIzq">
                                <span class="Form_TextoObligatorio">&nbsp;(*)Campos Obligatorios</span>
                            </td>
                            <td class="Form_TextoDer">
                                <asp:Button ID="btnCotizar" runat="server" Text="Cotizar" ValidationGroup="valCotizar"
                                    Width="90px" ToolTip="Click para generar cotización." OnClick="btnCotizar_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="Form_tdBorde">
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="vsCotizar" runat="server" ValidationGroup="valCotizar"
            ShowMessageBox="true" ShowSummary="false" />
    </asp:Panel>
    <asp:Panel ID="pnlRegistro" runat="server" CssClass="Form_Oculto" Style="vertical-align: top;
        width: 100%">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td class="Form_tdBorde">
                </td>
                <td>
                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel3" runat="server" Width="100%" HeaderText="Datos del Cliente">
                        <PanelCollection>
                            <dxrp:PanelContent ID="PanelContent2" runat="server" supportsdisabledattribute="True">
                                <asp:UpdatePanel ID="upDatoCliente" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%" class="Form_Fondo">
                                    <tr>
                                        <td class="Form_TextoDer">
                                            Tipo Documento:
                                        </td>
                                        <td class="Form_TextoIzq">
                                            <asp:DropDownList ID="ddlTipoDoc" runat="server" Width="95px">                                                
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Form_TextoDer">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                ControlToValidate="txtNroDocumento" ErrorMessage="Ingrese el N° Documento" 
                                                SetFocusOnError="True" ValidationGroup="BuscarCliente">*</asp:RequiredFieldValidator>
                                            N° Documento:
                                        </td>
                                        <td class="Form_TextoIzq">
                                            <asp:TextBox ID="txtNroDocumento" runat="server" Width="90px"
                                                MaxLength="20"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="fteNroDoc" runat="server" TargetControlID="txtNroDocumento"
                                                ValidChars="-" FilterType="Numbers, UppercaseLetters, LowercaseLetters, Custom">
                                            </cc1:FilteredTextBoxExtender>
                                            &nbsp;<asp:ImageButton ID="ibtnBuscarCliente" runat="server" 
                                                ImageUrl="~/Img/Icons/ver.gif" OnClick="ibtnBuscarCliente_Click" 
                                                style="width: 16px" ValidationGroup="BuscarCliente" />
                                        </td>
                                        <td class="Form_TextoDer">
                                            Teléfono:
                                        </td>
                                        <td class="Form_TextoIzq">
                                            <asp:TextBox ID="txtTelefono" runat="server" Width="90px"
                                                MaxLength="20"></asp:TextBox>
                                            <span class="Form_TextoObligatorio">*</span>
                                            <cc1:FilteredTextBoxExtender ID="fteTelefono" runat="server" TargetControlID="txtTelefono"
                                                ValidChars="1234567890-">
                                            </cc1:FilteredTextBoxExtender>
                                        </td>
                                        <td class="Form_TextoDer">
                                            Célular:
                                        </td>
                                        <td class="Form_TextoIzq">
                                            <asp:TextBox ID="txtCelular" runat="server"  Width="90px"
                                                MaxLength="20"></asp:TextBox>
                                            <span class="Form_TextoObligatorio">*</span>
                                            <cc1:FilteredTextBoxExtender ID="fteCelular" runat="server" TargetControlID="txtCelular"
                                                ValidChars="1234567890-">
                                            </cc1:FilteredTextBoxExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Form_TextoDer">
                                            Primer Nombre:
                                        </td>
                                        <td class="Form_TextoIzq">
                                            <asp:TextBox ID="txtNombre1" runat="server" Width="90px"
                                                MaxLength="50"></asp:TextBox>
                                            <span class="Form_TextoObligatorio">*</span>
                                        </td>
                                        <td class="Form_TextoDer">
                                            Segundo Nombre:
                                        </td>
                                        <td class="Form_TextoIzq">
                                            <asp:TextBox ID="txtNombre2" runat="server" Width="90px"
                                                MaxLength="50"></asp:TextBox>
                                        </td>
                                        <td class="Form_TextoDer">
                                            E-Mail:
                                        </td>
                                        <td class="Form_TextoIzq">
                                            <asp:TextBox ID="txtEmail" runat="server" Width="90px" MaxLength="50"></asp:TextBox>
                                            <span class="Form_TextoObligatorio">*</span>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Form_TextoDer">
                                            Primer Apellido:
                                        </td>
                                        <td class="Form_TextoIzq">
                                            <asp:TextBox ID="txtApellido1" runat="server" Width="90px"
                                                MaxLength="50"></asp:TextBox>
                                            <span class="Form_TextoObligatorio">*</span>
                                        </td>
                                        <td class="Form_TextoDer">
                                            Segundo Apellido:
                                        </td>
                                        <td class="Form_TextoIzq">
                                            <asp:TextBox ID="txtApellido2" runat="server" Width="90px"
                                                MaxLength="50"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                                <asp:RequiredFieldValidator ID="rfvNombre1" runat="server" ErrorMessage="Ingrese el primer nombre."
                                    Text="*" ForeColor="Transparent" Display="Dynamic" ValidationGroup="regCotizacion"
                                    SetFocusOnError="true" ControlToValidate="txtNombre1"></asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="rfvApellido1" runat="server" ErrorMessage="Ingrese el primer apellido."
                                    Text="*" ForeColor="Transparent" Display="Dynamic" ValidationGroup="regCotizacion"
                                    SetFocusOnError="true" ControlToValidate="txtApellido1"></asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="rfvTelefono" runat="server" ErrorMessage="Ingrese el nro de teléfono."
                                    Text="*" ForeColor="Transparent" Display="Dynamic" ValidationGroup="regCotizacion"
                                    SetFocusOnError="true" ControlToValidate="txtTelefono"></asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="rfvCelular" runat="server" ErrorMessage="Ingrese el nro. de célular."
                                    Text="*" ForeColor="Transparent" Display="Dynamic" ValidationGroup="regCotizacion"
                                    SetFocusOnError="true" ControlToValidate="txtCelular"></asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="Ingrese el correo correo electronico."
                                    Text="*" ForeColor="Transparent" Display="Dynamic" ValidationGroup="regCotizacion"
                                    SetFocusOnError="true" ControlToValidate="txtEmail"></asp:RequiredFieldValidator>
                            
                                    </ContentTemplate>
                                    <triggers>
                                        <asp:AsyncPostBackTrigger ControlID="gvSeleccionarCliente" 
                                            EventName="SelectedIndexChanged" />
                                    </triggers>
                                </asp:UpdatePanel>
                                
                               



                            <div id="frmSeleccionarCliente" style=" width:700px;height:250px;padding-top:0px; margin:auto; display:none"> 
     <asp:UpdatePanel ID="upSelCliente" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            
                          <table class="TablaMarco" style="width: 100%">
                                <tr>
                                    <td class="TablaTitulo">
                                        Seleccionar Cliente                                      
                                        </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width: 100%">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvSeleccionarCliente" runat="server" AutoGenerateColumns="False" 
                                                        EnableModelValidation="True" AllowPaging="False" 
                                                        DataKeyNames="NroCliente" SkinID="sknGridView" 
                                                        onselectedindexchanged="gvSeleccionarCliente_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:BoundField DataField="NroCliente" HeaderText="N° Cliente">
                                                            <ItemStyle Width="100px" HorizontalAlign="Center" />
                                                            </asp:BoundField>

                                                            <asp:BoundField DataField="NroDocumento" HeaderText="N° Documento">
                                                            <ItemStyle Width="130px" HorizontalAlign="Center" />
                                                            </asp:BoundField>

                                                            <asp:BoundField DataField="Nombre1" HeaderText="Apellidos y Nombres" HtmlEncode="False" >
                                                            <ItemStyle Width="300px" HorizontalAlign="Left"/>
                                                            </asp:BoundField>

                                                            <asp:BoundField DataField="FechaNacimiento" HeaderText="Fecha Nac.">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>                                                            
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="ibtnSelCobertura" runat="server" 
                                                                        ImageUrl="~/Img/seleccionar.gif" CausesValidation="False" 
                                                                        CommandName="Select" />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                                   No se encontraron datos...
                                                        </EmptyDataTemplate>
                                                        <RowStyle Height="25px" />                                                      
                                                    </asp:GridView>
                                                   
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td>
                                                        <input style="width: 64px; height:22px;" id="btnCanCC" onmouseover="this.className='btnStyleSobre';" class="btnStyle" onmouseout="this.className='btnStyle';" onclick="CerrarSelCliente()" type="button" value="Cancelar" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <Triggers>                            
                            <asp:AsyncPostBackTrigger ControlID="ibtnBuscarCliente" EventName="Click" />
                        </Triggers>
                     </asp:UpdatePanel>
                     <asp:UpdateProgress ID="upProgSelCliente" runat="server" DisplayAfter="0">
                        <ProgressTemplate>
                            <uc1:Cargando ID="CargandoSelCli" runat="server" />
                            
                        </ProgressTemplate>
                     </asp:UpdateProgress>
       
</div>


                            
                            
                            </dxrp:PanelContent>
                        </PanelCollection>
                    </dxrp:ASPxRoundPanel>
                </td>
                <td class="Form_tdBorde">
                </td>
            </tr>
        </table>
        <br />
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td class="Form_tdBorde">
                </td>
                <td>
                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel4" runat="server" Width="100%" HeaderText="Datos del Vehículo">
                        <PanelCollection>
                            <dxrp:PanelContent ID="PanelContent3" runat="server" supportsdisabledattribute="True">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%" class="Form_Fondo">
                                    <tr>
                                        <td class="Form_TextoDer" style="width: 90px">
                                            Marca:
                                        </td>
                                        <td class="Form_TextoIzq">
                                            <asp:TextBox ID="txtMarca" runat="server" SkinID="txtReadOnly" ReadOnly="true"
                                                Width="120px"></asp:TextBox>
                                        </td>
                                        <td class="Form_TextoDer">
                                            Modelo:
                                        </td>
                                        <td class="Form_TextoIzq">
                                            <asp:TextBox ID="txtModelo" runat="server" SkinID="txtReadOnly" ReadOnly="true"
                                                Width="120px"></asp:TextBox>
                                        </td>
                                        <td class="Form_TextoDer">
                                            Año:
                                        </td>
                                        <td class="Form_TextoIzq">
                                            <asp:TextBox ID="txtAnio" runat="server" SkinID="txtReadOnly" ReadOnly="true"
                                                Width="90px"></asp:TextBox>
                                        </td>
                                        <td class="Form_TextoDer">
                                            Valor:
                                        </td>
                                        <td class="Form_TextoIzq">
                                            <asp:TextBox ID="txtValorVeh" runat="server" SkinID="txtReadOnly"
                                                ReadOnly="true" Width="90px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Form_TextoDer" style="width: 90px">
                                            Chapa:
                                        </td>
                                        <td class="Form_TextoIzq">
                                            <asp:TextBox ID="txtNroPlaca" runat="server" Width="120px"></asp:TextBox>
                                            <span class="Form_TextoObligatorio">*</span>
                                            <asp:RequiredFieldValidator ID="rfvPlaca" runat="server" ErrorMessage="Ingrese el N° Chapa"
                                                Text="*" ForeColor="Transparent" Display="Dynamic" ValidationGroup="regCotizacion"
                                                SetFocusOnError="true" ControlToValidate="txtNroPlaca"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </dxrp:PanelContent>
                        </PanelCollection>
                    </dxrp:ASPxRoundPanel>
                    <br />
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td style="text-align: left;">
                                <span class="Form_TextoObligatorio">&nbsp;(*)Campos Obligatorios</span>
                            </td>
                            <td style="text-align: right;">
                                <asp:Button ID="btnInicar" runat="server" Text="Inicio" ToolTip="Click para iniciar la cotización."
                                    OnClick="btnInicar_Click" />
                                <asp:Button ID="btnAtras" runat="server" Text="Regresar" ToolTip="Click para retroceder."
                                    OnClick="btnAtras_Click" />
                                <input title="Click para generar archivo PDF." type="button" id="btnGenerarPDF" runat="server"
                                    causesvalidation="False" value="Generar PDF" disabled />
                                <asp:Button ID="btnRegistrar" runat="server" Text="Registar" OnClick="btnRegistrar_Click"
                                    ToolTip="Click para registrar la cotización." ValidationGroup="regCotizacion" />
                            </td>
                        </tr>
                    </table>
                    <dxpc:ASPxPopupControl ID="pcCertificado" runat="server" AllowDragging="True" AllowResize="True"
                        CloseAction="CloseButton" ContentUrl="frmPrint.aspx" ClientInstanceName="pcCertificado"
                        EnableViewState="False" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
                        ShowFooter="False" Width="700px" Height="600px" LoadContentViaCallback="OnFirstShow"
                        HeaderText="Cotización" EnableHierarchyRecreation="True" BackColor="Gray" Modal="true"
                        ModalBackgroundStyle-Opacity="80" ModalBackgroundStyle-BackColor="Black">
                    </dxpc:ASPxPopupControl>
                </td>
                <td class="Form_tdBorde">
                </td>
            </tr>
        </table>
        <br />
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td class="Form_tdBorde">
                </td>
                <td>
                    <div style="width: 100%; overflow: auto;">
                        <asp:GridView ID="gvSeleccion" runat="server" EnableModelValidation="True" 
                            SkinID="sknGridView" onrowdatabound="gvSeleccion_RowDataBound">
                            <Columns>
                                <asp:BoundField DataField="DesAsegurador" HeaderText="Aseguradora" />
                                <asp:BoundField DataField="DesCategoria" HeaderText="Producto" />
                                <asp:BoundField DataField="SeguroPlan" HeaderText="Plan" />
                                <asp:BoundField DataField="ValVehiculo" DataFormatString="{0:#,#0.00}" 
                                    HeaderText="Capital Asegurado" ItemStyle-HorizontalAlign="Right" >
                                <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                                <asp:BoundField DataField="PrimaAnual" DataFormatString="{0:#,#0.00}" 
                                    HeaderText="Prima Anual" ItemStyle-HorizontalAlign="Right" >
                                <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                                <asp:BoundField DataField="PrimaAnualMensual" DataFormatString="{0:#,#0.00}" 
                                    HeaderText="Prima Mensual" ItemStyle-HorizontalAlign="Right" >
                                <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                            </Columns>
                            <RowStyle Height="20px" />
                        </asp:GridView>
                    </div>
                </td>
                <td class="Form_tdBorde">
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="vsRegistro" runat="server" ValidationGroup="regCotizacion"
            ShowMessageBox="true" ShowSummary="false" />
    </asp:Panel>
    <asp:Panel ID="pnlResultado" runat="server" CssClass="Form_Oculto" Style="vertical-align: top;
        width: 100%">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td class="Form_tdBorde">
                </td>
                <td>
                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel6" runat="server" Width="100%" HeaderText="Datos del Vehículo">
                        <PanelCollection>
                            <dxrp:PanelContent ID="PanelContent5" runat="server" supportsdisabledattribute="True">
                                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td class="Form_TextoDer" style="width: 90px">
                                            Marca:
                                        </td>
                                        <td class="Form_TextoIzq">
                                            <asp:TextBox ID="txtResMarca" runat="server" SkinID="txtReadOnly" ReadOnly="true"
                                                Width="120px"></asp:TextBox>
                                        </td>
                                        <td class="Form_TextoDer">
                                            Modelo:
                                        </td>
                                        <td class="Form_TextoIzq">
                                            <asp:TextBox ID="txtResModelo" runat="server" SkinID="txtReadOnly" ReadOnly="true"
                                                Width="120px"></asp:TextBox>
                                        </td>
                                        <td class="Form_TextoDer">
                                            Año:
                                        </td>
                                        <td class="Form_TextoIzq">
                                            <asp:TextBox ID="txtResAnio" runat="server" SkinID="txtReadOnly" ReadOnly="true"
                                                Width="90px"></asp:TextBox>
                                        </td>
                                        <td class="Form_TextoDer">
                                            Valor:
                                        </td>
                                        <td class="Form_TextoIzq">
                                            <asp:TextBox ID="txtResValor" runat="server" SkinID="txtReadOnly"
                                                ReadOnly="true" Width="90px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </dxrp:PanelContent>
                        </PanelCollection>
                    </dxrp:ASPxRoundPanel>
                </td>
                <td class="Form_TextoDer">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="vertical-align: top;">
                                <asp:ImageButton ID="ibtnRegistrar" runat="server" SkinID="sknIbtnRegistar" CssClass="Form_ButtonRed"
                                    OnClick="ibtnRegistrar_Click" />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: bottom;">
                                <asp:Button ID="btnResRegresar" runat="server" Text="Regresar" OnClick="btnResRegresar_Click"
                                    ToolTip="Click para regresar al página inicial." />
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="Form_tdBorde">
                </td>
            </tr>
        </table>
        <br />
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td class="Form_tdBorde">
                </td>
                <td style="vertical-align: top;">
                    <div id="dvResultado" runat="server" clientidmode="Static" style="border-style: groove;
                        border-width: thin; text-align: center; vertical-align: top; width: 98%; height: 360px;
                        overflow: auto;">
                    </div>
                    <%--<div id="ps_slider" class="ps_slider" style="vertical-align: top; margin-top: 5;">
                        <a class="prev disabled"></a><a class="next disabled"></a>
                        <div id="ps_albums" runat="server" clientidmode="Static" style="text-align: center;
                            vertical-align: top;"></div>
                    </div>
                    <div id="ps_overlay" class="ps_overlay" style="display: none;">
                    </div>
                    <a id="ps_close" class="ps_close" style="display: none;"></a>
                    <div id="ps_container" class="ps_container" style="display: none;">
                        <a id="ps_next_photo" class="ps_next_photo" style="display: none;"></a>
                    </div>--%>
                </td>
                <td class="Form_tdBorde">
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:UpdateProgress ID="upGeneral" runat="server" DisplayAfter="0">
                        <ProgressTemplate>
                            <uc1:Cargando ID="CargandoGeneral" runat="server" />
                            
                        </ProgressTemplate>
                     </asp:UpdateProgress>

    





    <asp:HiddenField ID="hfIdMarca" runat="server" />
    <asp:HiddenField ID="hfDesMarca" runat="server" />
    <asp:HiddenField ID="hfIdModelo" runat="server" />
    <asp:HiddenField ID="hfDesModelo" runat="server" />
    <asp:HiddenField ID="hfAnio" runat="server" />
    <asp:HiddenField ID="hfValor" runat="server" />
    <asp:HiddenField ID="hfTipoCot" runat="server" />
    <asp:HiddenField ID="hfIdPais" runat="server" />
    <asp:HiddenField ID="hfIdProd" runat="server" />
    <asp:HiddenField ID="hfIdGrupProd" runat="server" />
    <asp:HiddenField ID="hfIdSponsor" runat="server" />
    <asp:HiddenField ID="hfAseguradoras" runat="server" />
    <asp:HiddenField ID="hfPlanes" runat="server" />



</asp:Content>
