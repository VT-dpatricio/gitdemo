﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true" CodeBehind="frmMantMarca.aspx.cs" Inherits="AONWebMotor.Mantenimiento.frmMantMarca" %>

<%@ Register src="~/Controles/Cargando.ascx" tagname="Cargando" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>

             <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscar">
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="Form_SubTitulo" colspan="4">
                            MANTENIMIENTO MARCA
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer" style="width: 38px">
                            Buscar:
                        </td>
                        <td class="Form_TextoIzq" style="width: 264px">
                            <asp:TextBox ID="txtDescripcion" runat="server" Width="250px"  ></asp:TextBox>
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" 
                                onclick="btnBuscar_Click"/> 
                        </td>
                        <td></td>
                    </tr>
                </table>
               
                </asp:Panel>

                <br />
                <div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView ID="gvMarca" runat="server" AllowPaging="True" 
                                EnableModelValidation="True" onpageindexchanging="gvMarca_PageIndexChanging" 
                                onrowcancelingedit="gvMarca_RowCancelingEdit" onrowediting="gvMarca_RowEditing" 
                                PageSize="15" SkinID="sknGridView" Width="100%" 
                                onrowdatabound="gvMarca_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="idMarca" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("idMarca") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("idMarca") %>'></asp:Label>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Marca">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Descripcion") %>' 
                                                Width="590px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                ControlToValidate="TextBox1" ErrorMessage="Ingrese la Marca" 
                                                SetFocusOnError="True" ValidationGroup="vgGrabar">*</asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cod. Externo">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("CodExterno") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("CodExterno") %>' 
                                                Width="100px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemStyle Width="115px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Estado">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBox1" runat="server" 
                                                Checked='<%# Bind("EstadoRegistro") %>' Enabled="false" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="CheckBox1" runat="server" 
                                                Checked='<%# Bind("EstadoRegistro") %>' />
                                        </EditItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ibtnModificar" runat="server" CommandName="Edit"  ToolTip="Modificar"
                                                ImageUrl="~/Img/editar.gif" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:ImageButton ID="ibtnGrabar" runat="server" ImageUrl="~/Img/Save.gif" ValidationGroup="vgGrabar" ToolTip="Grabar"
                                                onclick="ibtnGrabar_Click" />
                                            &nbsp;<asp:ImageButton ID="ibtnCancelar" runat="server" ToolTip="Cancelar" CommandName="cancel" CausesValidation="false" 
                                                ImageUrl="~/Img/b_cancelar.gif" />
                                        </EditItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="40px" />
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    No se encontraron registros...
                                </EmptyDataTemplate>
                                <HeaderStyle Height="20px" />
                                <RowStyle Height="25px" />
                            </asp:GridView>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                ShowMessageBox="True" ValidationGroup="vgGrabar" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>

           
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0">
                        <ProgressTemplate>
                            <uc1:Cargando ID="Cargando1" runat="server" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
</asp:Content>
