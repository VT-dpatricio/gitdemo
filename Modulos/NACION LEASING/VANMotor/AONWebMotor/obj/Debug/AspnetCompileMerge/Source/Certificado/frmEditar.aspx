﻿<%@ Page Title="Editar Certificado" Language="C#" MasterPageFile="~/Master/Principal.Master"
    AutoEventWireup="true" CodeBehind="frmEditar.aspx.cs" Inherits="AONWebMotor.Certificado.frmEditar" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v8.3" Namespace="DevExpress.Web.ASPxRoundPanel"
    TagPrefix="dxrp" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="DevExpress.Web.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v8.3" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v8.3" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v8.3" Namespace="DevExpress.Web.ASPxPanel"
    TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v8.3, Version=8.3.4.0, Culture=neutral, PublicKeyToken=5377c8e3b72b4073"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <dxtc:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" 
                    Width="100%">
                    <TabPages>
                        <dxtc:TabPage Name="tpCertificado" Text="Producto">
                            <ContentCollection>
                                <dxw:ContentControl ID="ContentControl1" runat="server" SupportsDisabledAttribute="True">
                                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" Width="100%" HeaderText="DATOS DEL PRODUCTO">
                                        <PanelCollection>
                                            <dxp:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" class="Form_Fondo">
                                                    <tr>
                                                        <td class="Form_TextoDer">
                                                            Producto:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtProducto" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="120px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Nro. Certificado:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtNroCertificado" runat="server" CssClass="Form_TextBoxDisable"
                                                                ReadOnly="true" Width="90px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Inicio Vigencia:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtIniVigencia" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="60px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Fin Vigencia:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtFinVigencia" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="60px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Form_TextoDer">
                                                            Estado:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="TextBox7" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Monto Asegurado:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtMontoAseg" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Fecha Venta:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtFecVenta" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="60px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Digitación:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtFecDigitacion" runat="server" 
                                                                CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="60px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Form_TextoDer">
                                                            Informador:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="TextBox3" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="120px"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Oficina:
                                                        </td>
                                                        <td class="Form_TextoIzq" colspan="2">
                                                            <asp:TextBox ID="TextBox4" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </dxp:PanelContent>
                                        </PanelCollection>
                                    </dxrp:ASPxRoundPanel>
                                    <br />
                                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" Width="100%" HeaderText="DATOS DEL TITULAR">
                                        <PanelCollection>
                                            <dxp:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True">
                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" class="Form_Fondo">
                                                    <tr>
                                                        <td class="Form_TextoDer">
                                                            Pri. Nombre:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtNombre1" runat="server" CssClass="Form_TextBox" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Seg. Nombre:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtNombre2" runat="server" CssClass="Form_TextBox" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Tipo Doc.:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="TextBox12" runat="server" CssClass="Form_TextBox" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Nro. Documento:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtCcCliente" runat="server" CssClass="Form_TextBox" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Form_TextoDer">
                                                            Ape. Paterno:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtApellido1" runat="server" CssClass="Form_TextBox" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Ape. Materno:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtApellido2" runat="server" CssClass="Form_TextBox" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Dirección:
                                                        </td>
                                                        <td class="Form_TextoIzq" colspan="3">
                                                            <asp:TextBox ID="txtDireccion" runat="server" CssClass="Form_TextBox" ReadOnly="true"
                                                                Width="304px"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Form_TextoDer">
                                                            Teléfono:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtTelefono" runat="server" CssClass="Form_TextBox" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Departamento:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="TextBox19" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Provincia:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="TextBox20" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Distrito:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="TextBox17" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </dxp:PanelContent>
                                        </PanelCollection>
                                    </dxrp:ASPxRoundPanel>
                                    <br />
                                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel3" runat="server" Width="100%" HeaderText="DATOS DEL PLAN Y MEDIO DE PAGO ">
                                        <PanelCollection>
                                            <dxp:PanelContent ID="PanelContent3" runat="server" SupportsDisabledAttribute="True">
                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" class="Form_Fondo">
                                                    <tr>
                                                        <td class="Form_TextoDer">
                                                            Plan:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtPlan" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Frecuencia:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtFrecuencia" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Prima Cobrar:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtPrimaCob" runat="server" CssClass="Form_TextBoxMontoDisable" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Prima Total:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="txtPrimaTot" runat="server" CssClass="Form_TextBoxMontoDisable" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Form_TextoDer">
                                                            Moneda Prima:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="TextBox29" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Medio de Pago:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="TextBox26" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Nro. Tarjeta/Cta:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="TextBox27" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                            <span class="Form_TextoObligatorio">*</span>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Moneda Cta:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="TextBox28" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </dxp:PanelContent>
                                        </PanelCollection>
                                    </dxrp:ASPxRoundPanel>
                                    <br />
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr style="border: 0;">
                                            <td class="Form_TextoIzq">
                                                <span class="Form_TextoObligatorio">&nbsp;(*)Campos Obligatorios</span>
                                            </td>
                                            <td class="Form_TextoDer">
                                                <asp:Button ID="Button1" runat="server" Text="Activar" CausesValidation="true" Width="90px" />
                                                <asp:Button ID="btnAnular" runat="server" Text="Anular" CausesValidation="true" Width="90px" />
                                                <asp:Button ID="btnExpedir" runat="server" Text="Actualizar" CausesValidation="true"
                                                    Width="90px" />
                                            </td>
                                        </tr>
                                    </table>
                                </dxw:ContentControl>
                            </ContentCollection>
                        </dxtc:TabPage>
                        <dxtc:TabPage Name="tpInfoCertificado" Text="Info. Adicional Certificado">
                            <ContentCollection>
                                <dxw:ContentControl ID="ContentControl2" runat="server" SupportsDisabledAttribute="True">
                                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel5" runat="server" Width="100%" HeaderText="DATOS ADICIONALES">
                                        <PanelCollection>
                                            <dxp:PanelContent ID="PanelContent5" runat="server" SupportsDisabledAttribute="True">
                                                <div style="text-align: center; overflow: auto; height: 95%;">
                                                    <asp:GridView ID="gvCotizacion" runat="server" SkinID="sknGridView" Width="100%"
                                                        AllowPaging="True" EnableModelValidation="True" ShowHeader="False" >
                                                        <Columns>
                                                            <asp:BoundField HeaderText="idCertificado" Visible="False" />
                                                            <asp:BoundField DataField="numCertificado" HeaderText="Campo">
                                                                <ItemStyle Width="80px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DesProducto" HeaderText="Información">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="ibtnSelect" runat="server" CommandName="Select" 
                                                                        SkinID="sknImgEditar" />
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="25px" />
                                                                <ItemStyle HorizontalAlign="Center" Width="25px" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </dxp:PanelContent>
                                        </PanelCollection>
                                    </dxrp:ASPxRoundPanel>
                                    <br />
                                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel4" runat="server" Width="100%" HeaderText="AUDITORIA DEL CERTIFICADO">
                                        <PanelCollection>
                                            <dxp:PanelContent ID="PanelContent4" runat="server" SupportsDisabledAttribute="True">
                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" class="Form_Fondo">
                                                    <tr>
                                                        <td class="Form_TextoDer">
                                                            Usuario Creación:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="TextBox39" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="80px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Usuario Modificación:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="TextBox40" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="80px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Fecha Modificación.
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="TextBox41" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="60px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Fecha Anulación:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="TextBox5" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="60px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </dxp:PanelContent>
                                        </PanelCollection>
                                    </dxrp:ASPxRoundPanel>
                                </dxw:ContentControl>
                            </ContentCollection>
                        </dxtc:TabPage>
                        <dxtc:TabPage Name="tpVehiculo" Text="Vehículo Asegurado">
                            <ContentCollection>
                                <dxw:ContentControl ID="ContentControl3" runat="server" SupportsDisabledAttribute="True">
                                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel6" runat="server" Width="100%" HeaderText="DATOS DEL VEHICULO">
                                        <PanelCollection>
                                            <dxp:PanelContent ID="PanelContent6" runat="server" SupportsDisabledAttribute="True">
                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" class="Form_Fondo">
                                                    <tr>
                                                        <td class="Form_TextoDer">
                                                            Marca:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="TextBox6" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Modelo:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="TextBox30" runat="server" CssClass="Form_TextBoxDisable"
                                                                ReadOnly="true" Width="90px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Tipo:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="TextBox31" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            Clase:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="TextBox32" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Form_TextoDer">
                                                            Año Fabricación:
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                            <asp:TextBox ID="TextBox33" runat="server" CssClass="Form_TextBoxDisable" ReadOnly="true"
                                                                Width="90px"></asp:TextBox>
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                            &nbsp;</td>
                                                        <td class="Form_TextoIzq">
                                                            &nbsp;</td>
                                                        <td class="Form_TextoDer">
                                                            &nbsp;</td>
                                                        <td class="Form_TextoIzq">
                                                            &nbsp;</td>
                                                        <td class="Form_TextoDer">
                                                            &nbsp;</td>
                                                        <td class="Form_TextoIzq">
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Form_TextoDer">
                                                            &nbsp;</td>
                                                        <td class="Form_TextoIzq">
                                                            &nbsp;</td>
                                                        <td class="Form_TextoDer">
                                                            &nbsp;</td>
                                                        <td class="Form_TextoIzq" colspan="2">
                                                            &nbsp;</td>
                                                        <td class="Form_TextoIzq">
                                                        </td>
                                                        <td class="Form_TextoDer">
                                                        </td>
                                                        <td class="Form_TextoIzq">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </dxp:PanelContent>
                                        </PanelCollection>
                                    </dxrp:ASPxRoundPanel>
                                </dxw:ContentControl>
                            </ContentCollection>
                        </dxtc:TabPage>
                    </TabPages>
                </dxtc:ASPxPageControl>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
</asp:Content>
