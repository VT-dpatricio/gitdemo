﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/NewPrincipal.Master" AutoEventWireup="true"
    CodeBehind="frmConfigEstructura.aspx.cs" Inherits="AONWebMotor.Trama.frmConfigEstructura" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="Form_SubTitulo" colspan="6">
                            CONFIGURACIÓN DE ESTRUCTURA DE ARCHIVO
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Tipo Proceso:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlTipProceso" runat="server" Width="250px" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlTipProceso_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Tipo Archivo:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList ID="ddlTipoArchivo" runat="server" Width="250px" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlTipoArchivo_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                <br />
                <div style="text-align: center; overflow: auto; height: 95%;">
                    <asp:GridView ID="gvProceso" runat="server" SkinID="sknGridView" AutoGenerateColumns="False"
                        AllowPaging="True" EnableModelValidation="True" OnPageIndexChanging="gvProceso_PageIndexChanging"
                        OnRowDataBound="gvProceso_RowDataBound" Style="text-align: left" 
                        onrowcommand="gvProceso_RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="Orden">
                                <ItemTemplate>
                                    <asp:Label ID="lblOrden" runat="server" Text='<%# Bind("Orden") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("Orden") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tipo Dato">
                                <ItemTemplate>
                                    <asp:DropDownList ID="dllTipoDato" runat="server" Width="80px">
                                    </asp:DropDownList>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("TipoDato") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Valor Fijo">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtValorFijo" runat="server" CssClass="Form_TextBox" Width="80px"></asp:TextBox>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("ValorFijo") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Campo">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtCampo" runat="server" CssClass="Form_TextBox" Width="200px"></asp:TextBox>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Nombre") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Inicio">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtInicio" runat="server" CssClass="Form_TextBox" Width="50px"></asp:TextBox>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Inicio") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Longitud">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtLongitud" runat="server" CssClass="Form_TextBox" Width="50px"></asp:TextBox>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Longitud") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Obligatorio">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbxObligatorio" runat="server" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Obligatorio") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="80px" />
                                <ItemStyle HorizontalAlign="Center" Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Opciones">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnActualizar" runat="server" CommandName="Actualizar" 
                                        SkinID="sknImgEditar" ToolTip="Permite actualizar el registro seleccionado" />
                                    <asp:ImageButton ID="ibtnQuitar" runat="server" SkinID="sknIbtnIcoAnular" 
                                        ToolTip="Permite eliminar el registro seleccionado" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <asp:Label ID="lblResultado" runat="server" Text="No se encontraron resultados."
                    Visible="false" CssClass="Form_MsjLabel"></asp:Label>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
</asp:Content>
