﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Principal.Master" AutoEventWireup="true" CodeBehind="frmConsultaImp.aspx.cs" Inherits="AONWebMotor.Certificado.frmConsultaImp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
        function pageLoad() {
            this.ValidarFiltro();
        }
        function ValidarFiltro() {
            if (document.getElementById('<%=ddlTipoBusq.ClientID %>').value == 0) {
                document.getElementById('<%=txtNro.ClientID %>').style.display = '';

                document.getElementById('<%=lblApePat.ClientID %>').style.display = 'none';
                document.getElementById('<%=lblApeMat.ClientID %>').style.display = 'none';
                document.getElementById('<%=lblPriNom.ClientID %>').style.display = 'none';
                document.getElementById('<%=lblSegNom.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtApePat.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtApeMat.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtPriNom.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtSegNom.ClientID %>').style.display = 'none';
            }
            if (document.getElementById('<%=ddlTipoBusq.ClientID %>').value == 1) {
                document.getElementById('<%=txtNro.ClientID %>').style.display = 'none';

                document.getElementById('<%=lblApePat.ClientID %>').style.display = '';
                document.getElementById('<%=lblApeMat.ClientID %>').style.display = '';
                document.getElementById('<%=lblPriNom.ClientID %>').style.display = '';
                document.getElementById('<%=lblSegNom.ClientID %>').style.display = '';
                document.getElementById('<%=txtApePat.ClientID %>').style.display = '';
                document.getElementById('<%=txtApeMat.ClientID %>').style.display = '';
                document.getElementById('<%=txtPriNom.ClientID %>').style.display = '';
                document.getElementById('<%=txtSegNom.ClientID %>').style.display = '';
            }
            if (document.getElementById('<%=ddlTipoBusq.ClientID %>').value == 2) {
                document.getElementById('<%=txtNro.ClientID %>').style.display = '';

                document.getElementById('<%=lblApePat.ClientID %>').style.display = 'none';
                document.getElementById('<%=lblApeMat.ClientID %>').style.display = 'none';
                document.getElementById('<%=lblPriNom.ClientID %>').style.display = 'none';
                document.getElementById('<%=lblSegNom.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtApePat.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtApeMat.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtPriNom.ClientID %>').style.display = 'none';
                document.getElementById('<%=txtSegNom.ClientID %>').style.display = 'none';
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td class="Form_tdBorde">
            </td>
            <td>
                <table class="Form_Fondo" border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="Form_SubTitulo" colspan="6">
                            IMPRIMIR CERTIFICADO
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            Buscar Por:
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:DropDownList id="ddlTipoBusq" runat="server" Width="250px" onchange="return ValidarFiltro();" >
                                <asp:ListItem Text="NRO DOCUMENTO" Value="0"></asp:ListItem>
                                <asp:ListItem Text="NOMBRE Y APELLIDOS" Value="1"></asp:ListItem>
                                <asp:ListItem Text="NRO CERTIFICADO" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="Form_TextoDer">
                            <asp:TextBox ID="txtNro" runat="server" CssClass="Form_TextBox" MaxLength="8"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="ftxtDNI" runat="server" TargetControlID="txtNro" ValidChars="0123456789" ></cc1:FilteredTextBoxExtender>
                        </td>
                        <td class="Form_TextoIzq">                            
                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" 
                                onclick="btnBuscar_Click" />
                        </td>
                        <td>                             
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            <asp:Label ID="lblApePat" runat="server" Text="Apellido Paterno:" MaxLength="50" style="display:none;"></asp:Label>    
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtApePat" runat="server" CssClass="Form_TextBox" MaxLength="50" style="display:none;"></asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">
                            <asp:Label ID="lblApeMat" runat="server" Text="Apellido Materno:" MaxLength="50" style="display:none;"></asp:Label>                                                            
                        </td>
                        <td class="Form_TextoIzq">                            
                            <asp:TextBox ID="txtApeMat" runat="server" CssClass="Form_TextBox" MaxLength="50" style="display:none;"></asp:TextBox>
                        </td>
                        <td>                            
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="Form_TextoDer">
                            <asp:Label ID="lblPriNom" runat="server" Text="Primer Nombre:" style="display:none;" ></asp:Label>
                        </td>
                        <td class="Form_TextoIzq">
                            <asp:TextBox ID="txtPriNom" runat="server" CssClass="Form_TextBox" style="display:none;"></asp:TextBox>
                        </td>
                        <td class="Form_TextoDer">                                            
                            <asp:Label ID="lblSegNom" runat="server" Text="Segundo Nombre:" style="display:none;" ></asp:Label>            
                        </td>
                        <td class="Form_TextoIzq">                            
                            <asp:TextBox ID="txtSegNom" runat="server" CssClass="Form_TextBox" style="display:none;"></asp:TextBox>
                        </td>
                        <td>                            
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                <br />
                <div style="text-align:center; overflow:auto; height:95%;" >
                    <asp:GridView ID="gvCotizacion" runat="server" SkinID="sknGridView" 
                        Width="100%" AllowPaging="True" EnableModelValidation="True" 
                        onrowdatabound="gvCotizacion_RowDataBound" 
                        onpageindexchanging="gvCotizacion_PageIndexChanging" 
                        onrowcommand="gvCotizacion_RowCommand"  >
                        <Columns>
                            <asp:BoundField HeaderText="idCertificado" 
                                Visible="False" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ibtnSelect" runat="server" CommandName="Select" 
                                        SkinID="sknIbtnSiguiente" />
                                </ItemTemplate>
                                <HeaderStyle Width="25px" />
                                <ItemStyle HorizontalAlign="Center" Width="25px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="numCertificado" HeaderText="Nro. Certificado" >
                            <ItemStyle Width="80px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DesProducto" HeaderText="Producto">
                                <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="200px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NombreApellidos" HeaderText="Cliente" >
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" Width="200px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DesTipoDocumento" HeaderText="Tipo Documento" >
                                <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="CcCliente" HeaderText="Nro. Documento" >
                                <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DesEstadoCertificado" HeaderText="Estado">
                            <HeaderStyle HorizontalAlign="Center" Width="60px" />
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                    <br />                    
                </div>
                <asp:Label id="lblResultado" runat ="server" Text="No se encontraron resultados." Visible="false" CssClass="Form_MsjLabel"></asp:Label>
            </td>
            <td class="Form_tdBorde">
            </td>
        </tr>
    </table>
</asp:Content>
