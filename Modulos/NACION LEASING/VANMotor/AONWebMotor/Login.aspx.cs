﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Collections;
using AONAffinity.Motor.Resources;
using VAN.Common.BL;
using VAN.Common.BE;

namespace AONWebMotor
{
    public partial class Login : PaginaBase
    {
        private const string USER_DEFAULT = "DEMO";
        private const string PASS_DEFAULT = "12345+";

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (!IsPostBack)
            {
                //FormsAuthentication.RedirectFromLoginPage("sistemas", False)
                if ((Request.QueryString["CS"] == "SignOut"))
                {
                    try
                    {
                        BLSeguridadEvento oBLSegEvento = new BLSeguridadEvento();
                        BESeguridadEvento oBESegEvento = new BESeguridadEvento();
                        oBESegEvento.IDSistema = IDSistema();
                        oBESegEvento.IDUsuario = User.Identity.Name;
                        oBESegEvento.IDTipoEvento = 2;
                        //Logout
                        oBESegEvento.Host = Request.UserHostAddress;
                        oBLSegEvento.Insertar(oBESegEvento);
                    }
                    catch (Exception ex)
                    {

                    }
                    Session.Abandon();
                    System.Web.Security.FormsAuthentication.SignOut();
                }
            }
        }

        private void ValidarPermisoUsuario(string pUsuario, Boolean pLoginDominio)
        {
            //If (Membership.ValidateUser(pUsuario, pClave)) Then
            //txtUsuario.Text = Request.UserHostAddress
            BLUsuario oBLUsuario = new BLUsuario();
            BEUsuario oBEUsuario = null;
            try
            {
                oBEUsuario = oBLUsuario.Seleccionar(pUsuario, IDSistema(), pLoginDominio);
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            if (!string.IsNullOrEmpty(oBEUsuario.IDUsuario) & oBEUsuario.Acceso == true & oBEUsuario.Acceso == true)
            {
                Session["BEUsuario"] = oBEUsuario;
                Session["IDUsuario"] = oBEUsuario.IDUsuario;
                Session["IDUsuarioDominio"] = oBEUsuario.IDUsuarioDominio;
                Session["NombreUsuario"] = oBEUsuario.Nombre; //+ " " + oBEUsuario.Apellido1;
                string IDUsuario = oBEUsuario.IDUsuario;

                //Parámetros de Seguridad de la Aplicación por Entidad-----------------------------------
                BLSeguridadConfig oBLCSeguridad = new BLSeguridadConfig();
                //Dim pListParametro As IList = oBLCSeguridad.Seleccionar(oBEUsuario.IDEntidad)
                IList pListParametro = oBLCSeguridad.Seleccionar(IDEntidad());
                Session["SeguridadPar"] = pListParametro;
                ///---------------------------------------

                //CONFIGURAR PARÁMETROS PARA LA APLIACIÓN---------------------------------
                //Tiempo (minutos) para la desconexión automática de la sesión de usuario
                Session.Timeout = Convert.ToInt32(oBLCSeguridad.Parametro("SESIONUS01", pListParametro));
                ///-----------------------

                BLSeguridadEvento oBLSegEvento = new BLSeguridadEvento();
                //EVALUAR PARÁMETROS----------
                Session["PrimerLogin"] = false;
                Session["ClaveCaducada"] = false;

                if (pLoginDominio == false)
                {
                    //El cambio obligatorio de las contraseñas de acceso en el primer inicio de sesión.
                    string CambioOg = oBLCSeguridad.Parametro("CONTRASE01", pListParametro);
                    //Boolean CambioO = Convert.ToBoolean(Convert.ToInt16(oBLCSeguridad.Parametro("CONTRASE01", pListParametro)));
                    if (Convert.ToBoolean(Convert.ToInt16(oBLCSeguridad.Parametro("CONTRASE01", pListParametro))))
                    {
                        //Consultar si es la primera vez que el usuario inicia sesión
                        Session["PrimerLogin"] = oBLSegEvento.PrimerLogin(IDUsuario);
                    }
                    ///-----------------
                    //El intervalo de caducidad automática de las mismas a los 30 (treinta) días.
                    int pNroDiasCaduca = Convert.ToInt32(oBLCSeguridad.Parametro("CONTRASE05", pListParametro));
                    if (pNroDiasCaduca != 0 & !Convert.ToBoolean(Session["PrimerLogin"]))
                    {
                        // Consultar si la contraseña ha caducado
                        Session["ClaveCaducada"] = oBLSegEvento.ClaveCaducada(IDUsuario, pNroDiasCaduca);
                    }
                    ///------------------
                    //txtUsuario.Text = Session["PrimerLogin"].ToString & "- caduca" & Session["ClaveCaducada"].ToString
                }

                if (!Convert.ToBoolean(Session["PrimerLogin"]))
                {
                    //Registro del Inicio de sesión--------------
                    BESeguridadEvento oBESegEvento = new BESeguridadEvento();
                    oBESegEvento.IDSistema = IDSistema();
                    oBESegEvento.IDUsuario = IDUsuario;
                    oBESegEvento.IDTipoEvento = 1;
                    //Login
                    oBESegEvento.Host = Request.UserHostAddress;
                    oBLSegEvento.Insertar(oBESegEvento);
                }

                Session[NombreSession.Usuario] = oBEUsuario.IDUsuario;
                //Response.Redirect(UrlPagina.InicioDefault.Substring(2));

                //Validar Permisos
                FormsAuthentication.RedirectFromLoginPage(pUsuario, false);
            }
            else
            {
                lblMsg.Text = "Acceso denegado";
                HFIntentos.Value += 1;
                //MultiView1.ActiveViewIndex = 0
                //lblMensaje.Text = "Acceso denegado"
            }
            //Else
            //    'If (Membership.GetUser(Usuario) IsNot Nothing) And (Membership.GetUser(Usuario).IsLockedOut) Then
            //    '    lblMsg.Text = "La Cuenta de usuario ha sido bloqueada"
            //    'Else
            //    lblMsg.Text = "Usuario o Clave incorrectos"

            //    'End If
            //    HFIntentos.Value += 1

            //End If
        }

        protected bool ValidaMembership(String Usuario, String pClave)
        {
            if (Usuario == USER_DEFAULT)
            {
                return true;
            }

            return false;
            //return ValidarPermisoUsuario(Usuario, false);

        }

        protected void Page_PreRender(object sender, System.EventArgs e)
        {
            Control MyFirstCtrl = Page.Header.FindControl("FirstCtrlID");
            Page.Header.Controls.Remove(MyFirstCtrl);
            Page.Header.Controls.AddAt(0, MyFirstCtrl);
        }

        protected void btnIngresar_Click(object sender, ImageClickEventArgs e)
        {
            string Usuario = USER_DEFAULT;
            string Clave = PASS_DEFAULT;


            if ((System.Convert.ToInt32(HFIntentos.Value) == 3))
            {
                lblMsg.Text = "N de Intentos";
                mvLogin.ActiveViewIndex = 1;
                return;
            }
            ////if ((Membership.ValidateUser(Usuario, Clave)))
            ////{
            ////    ValidarUsuario(Usuario, Clave);
            ////}
            ////else
            ////{
            ////    lblMsg.Text = "Usuario o Clave incorrectos";
            ////    HFIntentos.Value += 1;
            ////}

            //Login Dominio/BAIS------------------------------------------
            //1. Validar usuario en LDAP
            BLSeguridad BLSeguridad = new BLSeguridad();

            string strError = BLSeguridad.LoginLDAP(Usuario, Clave);
            if (strError.Length == 0)
            {
                //Login OK LDAP
                ValidarPermisoUsuario(Usuario, true);
            }
            else
            {
                //2. Si no ingresa por LDAP, valida el usuario en Membership
                if (ValidaMembership(Usuario, Clave))
                {
                    ValidarPermisoUsuario(Usuario, false);
                }
                else
                {
                    lblMsg.Text = "Usuario o Clave incorrectos";
                    HFIntentos.Value += 1;
                }
            }
        }
    }
}