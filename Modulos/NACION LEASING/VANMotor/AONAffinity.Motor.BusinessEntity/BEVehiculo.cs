﻿using System;
using System.Collections.Generic;
using System.Text;
using AONAffinity.Library.Resources;

namespace AONAffinity.Motor.BusinessEntity
{
    public class BEVehiculo
    {
        #region Campos
        private String clase;
        private String marca;
        private String modelo;
        private String tipo;
        private String nroChasis;
        private String nroMotor;
        private Int16 anioFab;
        private String placa;
        private String uso;
        private String color;
        #endregion

        #region Propiedades
        public String Clase 
        {
            get { return this.clase; }
            set { this.clase = value; }
        }

        public String Marca 
        {
            get { return this.marca; }
            set { this.marca = value; }
        }

        public String Modelo 
        {
            get { return this.modelo; }
            set { this.modelo = value; }
        }

        public String Tipo 
        {
            get { return this.tipo; }
            set { this.tipo = value; }
        }

        public String NroChasis 
        {
            get { return this.nroChasis; }
            set { this.nroChasis = value; }
        }

        public String NroMotor 
        {
            get { return this.nroMotor; }
            set { this.nroMotor = value; }
        }

        public Int16 AnioFab 
        {
            get { return this.anioFab; }
            set { this.anioFab = value; }
        }

        public String Placa 
        {
            get { return this.placa; }
            set { this.placa = value; }
        }

        public String Uso 
        {
            get { return this.uso; }
            set { this.uso = value; }
        }

        public String Color 
        {
            get { return this.color; }
            set { this.color = value; }
        }
        #endregion

        #region Campos Consulta
        private Int32 idinfasegplaca;
        private Int32 idinfasegmotor;
        private Int32 idinfasegserie;
        #endregion

        #region Propiedades Consulta
        public Int32 IdInfasegPlaca
        {
            get { return this.idinfasegplaca; }
            set { this.idinfasegplaca = value; }
        }

        public Int32 IdInfoAsegMotor
        {
            get { return this.idinfasegmotor; }
            set { this.idinfasegmotor = value; }
        }

        public Int32 IdInfAsegSerie
        {
            get { return this.idinfasegserie; }
            set { this.idinfasegserie = value; }
        }
        #endregion
    }
}
