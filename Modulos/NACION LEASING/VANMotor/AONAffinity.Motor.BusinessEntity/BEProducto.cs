﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    [Serializable]
    public class BEProducto : BEAuditoria
    {
        #region Campos
        private Int32 idproducto;
        private String descripcion;
        private Int32 idasegurador;
        private Int32 idsponsor;
        private Int32 maxantiguedad;
        private Decimal maxvalorvehiculo;
        private Boolean siniestro;
        private Decimal montoSiniestro;
        private Int32 maxCantSiniestro;
        private Boolean? timoncambiado;
        private Decimal porcemision;
        private Boolean cambiovalvehiculo;
        private Int16 maxporcmenos;
        private Int16 maxporcmas;
        private Int32 minantiguedad;
        private Int16 maxnroasientos; 
        #endregion

        #region Propiedades
        public Int32 IdProducto
        {
            get { return this.idproducto; }
            set { this.idproducto = value; }
        }

        public String Descripcion
        {
            get { return this.descripcion; }
            set { this.descripcion = value; }
        }

        public Int32 IdAsegurador
        {
            get { return this.idasegurador; }
            set { this.idasegurador = value; }
        }

        public Int32 IdSponsor
        {
            get { return this.idsponsor; }
            set { this.idsponsor = value; }
        }

        public Int32 MaxAntiguedad
        {
            get { return this.maxantiguedad; }
            set { this.maxantiguedad = value; }
        }

        public Decimal MaxValorVehiculo
        {
            get { return this.maxvalorvehiculo; }
            set { this.maxvalorvehiculo = value; }
        }

        public Boolean Siniestro 
        {
            get { return this.siniestro; }
            set { this.siniestro = value; }
        }

        public Decimal MontoSiniestro 
        {
            get { return this.montoSiniestro; }
            set { this.montoSiniestro = value ;}
        }

        public Int32 MaxCantSiniestro 
        {
            get { return this.maxCantSiniestro; }
            set { this.maxCantSiniestro = value; }
        }

        public Boolean? TimonCambiado 
        {
            get { return this.timoncambiado; }
            set { this.timoncambiado = value; }
        }

        public Decimal PorcEmision 
        {
            get { return this.porcemision; }
            set { this.porcemision = value; }
        }

        public Boolean CambioValVehiculo 
        {
            get { return this.cambiovalvehiculo; }
            set { this.cambiovalvehiculo = value; }
        }

        public Int16 MaxPorcMenos 
        {
            get { return this.maxporcmenos; }
            set { this.maxporcmenos = value; }
        }

        public Int16 MaxPorcMas 
        {
            get { return this.maxporcmas; }
            set { this.maxporcmas = value; }
        }

        public Int32 MinAntiguedad 
        {
            get { return this.minantiguedad; }
            set { this.minantiguedad = value; }
        }

        public Int16 MaxNroAsientos 
        {
            get { return this.maxnroasientos; }
            set { this.maxnroasientos = value; }
        }
        #endregion 

        #region Campos Consulta
        private Int32 aniofab;
        #endregion

        #region Propiedades Consulta
        public Int32 AnioFab 
        {
            get { return this.aniofab; }
            set { this.aniofab = value; }
        }
        #endregion
    }
}
