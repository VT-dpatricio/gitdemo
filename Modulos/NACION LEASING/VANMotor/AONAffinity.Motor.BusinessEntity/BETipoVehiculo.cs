﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    public class BETipoVehiculo : BEAuditoria 
    {
        #region Campos
        private Int32 idtipovehiculo;
        private String descripcion;
        private String codexterno;
        #endregion

        #region Propiedades
        public Int32 IdTipoVehiculo
        {
            get { return this.idtipovehiculo; }
            set { this.idtipovehiculo = value; }
        }

        public String Descripcion
        {
            get { return this.descripcion; }
            set { this.descripcion = value; }
        }

        public String CodExterno
        {
            get { return this.codexterno; }
            set { this.codexterno = value; }
        }
        #endregion
    }
}
