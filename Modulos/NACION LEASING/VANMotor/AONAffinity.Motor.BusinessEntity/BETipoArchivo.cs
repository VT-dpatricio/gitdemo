﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    [Serializable] 
    public class BETipoArchivo : BEAuditoria
    {
        #region Campos
        private Int32 idtipoarchivo;
	    private String descripcion;
        private String extencion;
        private Int32 longitud;
        #endregion

        #region Propiedades
        public Int32 IdTipoArchivo 
        {
            get { return this.idtipoarchivo; }
            set { this.idtipoarchivo = value; }
        }

        public String Descripcion 
        {
            get { return this.descripcion; }
            set { this.descripcion = value; } 
        }

        public String Extencion 
        {
            get { return this.extencion; }
            set { this.extencion = value; }
        }

        public Int32 Longitud 
        {
            get { return this.longitud; }
            set { this.longitud = value; }
        }
        #endregion

        #region Campos Consulta
        private String nombrearchivo;
        private String descproceso;
        private Int32 idtipoproceso;
        #endregion

        #region Propiedades Consulta
        public String NombreArchivo 
        {
            get { return this.nombrearchivo; }
            set { this.nombrearchivo = value; }
        }

        public String DescProceso 
        {
            get { return this.descproceso; }
            set { this.descproceso = value; }
        }

        public Int32 IdTipoProceso
        {
            get { return this.idtipoproceso; }
            set { this.idtipoproceso = value; }
        }
        #endregion
    }
}
