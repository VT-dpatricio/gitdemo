﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AONAffinity.Library.Resources;

namespace AONAffinity.Motor.BusinessEntity
{
    public class BEDescuentoRecargo : BEAuditoria
    {
        #region Campos
        private Int32 iddsctorcrgo;
        private Decimal porcentaje;
        private String tipo;
        private Int32 minvalor;
        private Int32 maxvalor;
        private String condicion;
        #endregion

        #region Propiedades
        public Int32 IdDsctoRcrgo
        {
            get { return iddsctorcrgo; }
            set { iddsctorcrgo = value; }
        }

        public Decimal Porcentaje
        {
            get { return porcentaje; }
            set { porcentaje = value; }
        }

        public String Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }

        public Int32 MinValor
        {
            get { return minvalor; }
            set { minvalor = value; }
        }

        public Int32 MaxValor
        {
            get { return maxvalor; }
            set { maxvalor = value; }
        }

        public String Condicion
        {
            get { return condicion; }
            set { condicion = value; }
        }
        #endregion

        #region Campos Extends
        private Int32 idgrupoopcion;
        #endregion

        #region Propiedades Extends
        public Int32 IdGrupoOpcion 
        {
            get { return this.idgrupoopcion; }
            set { this.idgrupoopcion = value; }
        }
        #endregion
    }
}
