﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AONAffinity.Library.Resources;

namespace AONAffinity.Motor.BusinessEntity
{
    /// <summary>
    /// Clase entidad del negocio que define la tabla Cliente.
    /// </summary>
    [Serializable]
    public class BECliente : BEAuditoria
    {
        #region Campos
        private String nromotor;
        private String nrocertificado;
        private Int32 codasegurador;
        private String apepaterno;
        private String apematerno;
        private String prinombre;
        private String segnombre;
        private String idtipodocumento;
        private String nrodocumento;
        private String sexo;
        private DateTime fecnacimiento;
        private Int32 idciudad;
        private String direccion;
        private String teldomicilio1;
        private String teldomicilio2;
        private String teldomicilio3;
        private String telmovil1;
        private String telmovil2;
        private String telmovil3;
        private String teloficina1;
        private String teloficina2;
        private String teloficina3;
        private String email1;
        private String email2;
        private DateTime fecvigencia;
        private DateTime fecfinvigencia;
        private Int32 idmarca;
        private Int32 idmodelo;
        private String nroplaca;
        private Int32 anioFab;
        private Decimal valorvehiculo;
        private String nroserie;
        private String color;
        private Int32 idusovehiculo;
        private Int32 idclase;
        private Int32 nroasientos;
        private Boolean estimoncambiado;
        private Boolean esblindado;
        private Boolean reqGPS;
        private Int32 idsponsor;
        private Boolean esasegurable;
        private Int32 idproceso;
        private Int32 idestadocliente;
        #endregion

        #region Propiedades
        public String NroMotor
        {
            get { return this.nromotor; }
            set { this.nromotor = value; }
        }

        public String NroCertificado
        {
            get { return this.nrocertificado; }
            set { this.nrocertificado = value; }
        }

        public Int32 CodAsegurador
        {
            get { return this.codasegurador; }
            set { this.codasegurador = value; }
        }

        public String ApePaterno
        {
            get { return this.apepaterno; }
            set { this.apepaterno = value; }
        }

        public String ApeMaterno
        {
            get { return this.apematerno; }
            set { this.apematerno = value; }
        }

        public String PriNombre
        {
            get { return this.prinombre; }
            set { this.prinombre = value; }
        }

        public String SegNombre
        {
            get { return this.segnombre; }
            set { this.segnombre = value; }
        }

        public String IdTipoDocumento
        {
            get { return this.idtipodocumento; }
            set { this.idtipodocumento = value; }
        }

        public String NroDocumento
        {
            get { return this.nrodocumento; }
            set { this.nrodocumento = value; }
        }

        public String Sexo
        {
            get { return this.sexo; }
            set { this.sexo = value; }
        }

        public DateTime FecNacimiento
        {
            get { return this.fecnacimiento; }
            set { this.fecnacimiento = value; }
        }

        public Int32 idCiudad
        {
            get { return this.idciudad; }
            set { this.idciudad = value; }
        }

        public String Direccion
        {
            get { return direccion; }
            set { direccion = value; }
        }

        public String TelDomicilio1
        {
            get { return this.teldomicilio1; }
            set { this.teldomicilio1 = value; }
        }

        public String TelDomicilio2
        {
            get { return this.teldomicilio2; }
            set { this.teldomicilio2 = value; }
        }

        public String TelDomicilio3
        {
            get { return this.teldomicilio3; }
            set { this.teldomicilio3 = value; }
        }

        public String TelMovil1
        {
            get { return this.telmovil1; }
            set { this.telmovil1 = value; }
        }

        public String TelMovil2
        {
            get { return this.telmovil2; }
            set { this.telmovil2 = value; }
        }

        public String TelMovil3
        {
            get { return this.telmovil3; }
            set { this.telmovil3 = value; }
        }

        public String TelOficina1
        {
            get { return teloficina1; }
            set { teloficina1 = value; }
        }

        public String TelOficina2
        {
            get { return teloficina2; }
            set { teloficina2 = value; }
        }

        public String TelOficina3
        {
            get { return teloficina3; }
            set { teloficina3 = value; }
        }

        public String Email1
        {
            get { return email1; }
            set { email1 = value; }
        }

        public String Email2
        {
            get { return email2; }
            set { email2 = value; }
        }

        public DateTime FecVigencia
        {
            get { return this.fecvigencia; }
            set { this.fecvigencia = value; }
        }

        public DateTime FecFinVigencia
        {
            get { return this.fecfinvigencia; }
            set { this.fecfinvigencia = value; }
        }

        public Int32 IdMarca
        {
            get { return this.idmarca; }
            set { this.idmarca = value; }
        }

        public Int32 IdModelo
        {
            get { return this.idmodelo; }
            set { idmodelo = value; }
        }

        public String NroPlaca
        {
            get { return this.nroplaca; }
            set { this.nroplaca = value; }
        }

        public Int32 AnioFab
        {
            get { return anioFab; }
            set { anioFab = value; }
        }

        public Decimal ValorVehiculo
        {
            get { return this.valorvehiculo; }
            set { this.valorvehiculo = value; }
        }

        public String NroSerie
        {
            get { return this.nroserie; }
            set { this.nroserie = value; }
        }

        public String Color
        {
            get { return this.color; }
            set { this.color = value; }
        }

        public Int32 IdUsoVehiculo
        {
            get { return this.idusovehiculo; }
            set { this.idusovehiculo = value; }
        }

        public Int32 IdClase
        {
            get { return this.idclase; }
            set { this.idclase = value; }
        }

        public Int32 NroAsientos
        {
            get { return this.nroasientos; }
            set { this.nroasientos = value; }
        }

        public Boolean EsTimonCambiado
        {
            get { return this.estimoncambiado; }
            set { this.estimoncambiado = value; }
        }

        public Boolean EsBlindado
        {
            get { return this.esblindado; }
            set { this.esblindado = value; }
        }

        public Boolean ReqGPS
        {
            get { return this.reqGPS; }
            set { this.reqGPS = value; }
        }

        public Int32 IdSponsor
        {
            get { return this.idsponsor; }
            set { this.idsponsor = value; }
        }

        public Boolean EsAsegurable
        {
            get { return this.esasegurable; }
            set { this.esasegurable = value; }
        }

        public Int32 IdProceso
        {
            get { return this.idproceso; }
            set { this.idproceso = value; }
        }

        public Int32 IdEstadoCliente
        {
            get { return this.idestadocliente; }
            set { this.idestadocliente = value; }
        }
        #endregion

        #region Campos Consulta
        private Int32 cantsiniestro1anio;
        private Int32 cantsiniestro2anio;
        private Int32 cantsiniestro3anio;
        private Int32 cantsiniestro4anio;
        private Int32 cantsiniestro5anio;
        private DateTime fecinicio;
        private DateTime fecfin;
        private String nropoliza;
        private String codextmarca;
        private String codextmodelo;
        private String codextsubmodelo;        
        private String codexttipvehiculo;
        private String codextusovehiculo;
        #endregion

        #region Propiedades Consulta
        public Int32 CantSiniestro1Anio
        {
            get { return cantsiniestro1anio; }
            set { cantsiniestro1anio = value; }
        }

        public Int32 CantSiniestro2Anio
        {
            get { return cantsiniestro2anio; }
            set { cantsiniestro2anio = value; }
        }

        public Int32 CantSiniestro3Anio
        {
            get { return cantsiniestro3anio; }
            set { cantsiniestro3anio = value; }
        }

        public Int32 CantSiniestro4Anio
        {
            get { return cantsiniestro4anio; }
            set { cantsiniestro4anio = value; }
        }

        public Int32 CantSiniestro5Anio
        {
            get { return cantsiniestro5anio; }
            set { cantsiniestro5anio = value; }
        }

        public DateTime FecInicio
        {
            get { return fecinicio; }
            set { fecinicio = value; }
        }

        public DateTime FecFin
        {
            get { return fecfin; }
            set { fecfin = value; }
        }

        public String NombreApellido
        {
            get { return this.prinombre + " " + this.segnombre + " " + this.apepaterno + " " + this.apematerno; }
        }

        public String NroPoliza
        {
            get { return this.nropoliza; }
            set { this.nropoliza = value; }
        }

        public String CodExtMarca 
        {
            get { return this.codextmarca; }
            set { this.codextmarca = value; }
        }

        public String CodExtModelo
        {
            get { return this.codextmodelo; }
            set { this.codextmodelo = value; }
        }

        public String CodExtSubModelo
        {
            get { return this.codextsubmodelo; }
            set { this.codextsubmodelo = value; }
        }

        public String CodExtTipVehiculo
        {
            get { return this.codexttipvehiculo; }
            set { this.codexttipvehiculo = value; }
        }

        public String CodExtUsoVehiculo 
        {
            get { return this.codextusovehiculo; }
            set { this.codextusovehiculo = value; }
        }
        #endregion
    }
}
