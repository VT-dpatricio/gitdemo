﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    public class BETipoRechazoCotizacion : BEAuditoria
    {
        #region Campos
        private Int32 idtiprechazo;
        private String descripcion;
        #endregion

        #region Propiedades
        public Int32 IdTipRechazo
        {
            get { return idtiprechazo; }
            set { idtiprechazo = value; }
        }

        public String Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }
        #endregion
    }
}
