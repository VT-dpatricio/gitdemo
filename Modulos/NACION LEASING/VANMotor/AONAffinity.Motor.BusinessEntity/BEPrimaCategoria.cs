﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AONAffinity.Library.Resources;

namespace AONAffinity.Motor.BusinessEntity
{
    public class BEPrimaCategoria : BEAuditoria
    {
        #region Campos
        private Int32 idcategoria;
        private Int32 consecutivo;
        private Decimal montoprima;
        private DateTime fecinivigencia;
        private DateTime fecfinvigencia;
        #endregion

        #region Propiedades
        public Int32 IdCategoria
        {
            get { return idcategoria; }
            set { idcategoria = value; }
        }

        public Int32 Consecutivo
        {
            get { return consecutivo; }
            set { consecutivo = value; }
        }

        public Decimal MontoPrima
        {
            get { return montoprima; }
            set { montoprima = value; }
        }

        public DateTime FecIniVigencia
        {
            get { return fecinivigencia; }
            set { fecinivigencia = value; }
        }

        public DateTime FecFinVigencia
        {
            get { return fecfinvigencia; }
            set { fecfinvigencia = value; }
        }
        #endregion       
    }
}
