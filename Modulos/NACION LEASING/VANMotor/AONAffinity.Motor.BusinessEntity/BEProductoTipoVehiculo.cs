﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    [Serializable]
    public class BEProductoTipoVehiculo : BEAuditoria
    {
        #region Campos
        private Int32 idproducto;
        private Int32 idtipovehiculo;
        private String codexterno;
        #endregion

        #region Propiedades
        public Int32 IdProducto
        {
            get { return this.idproducto; }
            set { this.idproducto = value; }
        }

        public Int32 IdTipoVehiculo
        {
            get { return this.idtipovehiculo; }
            set { this.idtipovehiculo = value; }
        }

        public String CodExterno
        {
            get { return this.codexterno; }
            set { this.codexterno = value; }
        }
        #endregion

        #region CamposConsulta
        private String descripcion;
        #endregion

        #region Propiedades
        public String Descripcion
        {
            get { return this.descripcion; }
            set { this.descripcion = value; }
        }
        #endregion
    }
}
