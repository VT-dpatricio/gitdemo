﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AONAffinity.Library.Resources;

namespace AONAffinity.Motor.BusinessEntity
{
    /// <summary>
    /// Clase de entidad del negocio que referencia a la tabla Categoria.
    /// </summary>
    [Serializable]
    public class BECategoria : BEAuditoria 
    {
        #region Campos
        private Int32 idcategoria;
        private Int32 idcategoriapad;
        private String descategoria;
	    private Decimal montoprima;
        private String idmoneda;
        private Int32 minantiguedad;
        private Int32 idproducto;
        private String codexterno;
        #endregion

        #region Propiedades
        /// <summary>
        /// Código de categoría.
        /// </summary>
        public Int32 IdCategoria 
        {
            get { return this.idcategoria; }
            set { this.idcategoria = value; } 
        }

        /// <summary>
        /// Código de categoría padre.
        /// </summary>
        public Int32 IdCategoriaPad 
        {
            get { return this.idcategoriapad; }
            set { this.idcategoriapad = value; }
        }

        /// <summary>
        /// Descripción de categoría.
        /// </summary>
        public String DesCategoria 
        {
            get { return this.descategoria; }
            set { this.descategoria = value; }
        }

        /// <summary>
        /// Monto de prima de categoría.
        /// </summary>
        public Decimal MontoPrima 
        {
            get { return this.montoprima; }
            set { this.montoprima = value; }
        }

        /// <summary>
        /// Código de moneda.
        /// </summary>
        public String IdMoneda 
        {
            get { return this.idmoneda; }
            set { this.idmoneda = value; }
        }

        public Int32 MinAntiguedad 
        {
            get { return this.minantiguedad; }
            set { this.minantiguedad = value; }
        }

        public Int32 IdProducto
        {
            get { return this.idproducto; }
            set { this.idproducto = value; }
        }

        public String CodExterno 
        {
            get { return this.codexterno; }
            set { this.codexterno = value; }
        }
        #endregion
    }
}