﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    public class BEProductoPoliza : BEAuditoria
    {
        #region Campos
        private Int32 idproducto;
        private String nropoliza;
        private String opcionplan;
        private String parametro;
        #endregion

        #region Propiedades
        public Int32 IdProducto 
        {
            get { return this.idproducto; }
            set { this.idproducto = value; }
        }

        public String NroPoliza
        {
            get { return this.nropoliza; }
            set { this.nropoliza = value; }
        }

        public String OpcionPlan
        {
            get { return this.opcionplan; }
            set { this.opcionplan = value; }
        }

        public String Parametro 
        {
            get { return this.parametro; }
            set { this.parametro = value; }
        }
        #endregion
    }
}
