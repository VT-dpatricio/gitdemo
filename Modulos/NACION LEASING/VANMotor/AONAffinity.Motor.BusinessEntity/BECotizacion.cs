﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AONAffinity.Library.Resources;

namespace AONAffinity.Motor.BusinessEntity
{
    /// <summary>
    /// Clase entidad que define la tabla Cotización.
    /// </summary>
    [Serializable]
    public class BECotizacion : BEAuditoria
    {
        #region Campos
        private Int32 idpromocion;
        private String despromocion;
        private Int32 idperiodo;
        private Decimal nrocotizacion;
        private String apepaterno;
        private String apematerno;
        private String prinombre;
        private String segnombre;
        private String idtipodocumento;
        private String nrodocumento;
        private String sexo;
        private DateTime fecnacimiento;
        private Int32 idciudad;
        private String direccion;
        private String teldomicilio1;
        private String teldomicilio2;
        private String teldomicilio3;
        private String telmovil1;
        private String telmovil2;
        private String telmovil3;
        private String teloficina1;
        private String teloficina2;
        private String teloficina3;
        private String email1;
        private String email2;
        private DateTime fecinivigpoliza;
        private DateTime fecfinvigpoliza;
        private Int32 idmarca;
        private Int32 idmodelo;
        private Int32 idclase;
        private String nroplaca;
        private Int32 aniofab;
        private String nromotor;
        private String nroserie;
        private String color;
        private Int32 idusovehiculo;
        private Int32 nroasientos;
        private Boolean estimoncambiado;
        private Boolean reqgps;
        private Decimal valorivehiculo;
        private Decimal valvehiculo;
        private Boolean camvalvehiculo;
        private Decimal camporcentaje;
        private Decimal primaanual;
        private Decimal tasaanual;
        private Decimal primabianual;
        private Decimal tasabianual;
        private Int32 condicion;
        private Decimal porcentajec;
        private Decimal primaanualc;
        private Decimal tasaanualc;
        private Decimal primabianualc;
        private Decimal tasabianualc;
        private Decimal valortea;
        private Int32 idestado;
        private Boolean reqinspeccion;
        private String dirinspeccion;
        private String fechorinspeccion;
        private DateTime fecvalidez;
        private Int32 idcategoria;
        private Int32 cansiniestro1anio;
        private Decimal monsiniestro1anio;
        private Int32 cansiniestro2anio;
        private Decimal monsiniestro2anio;
        private Int32 cansiniestro3anio;
        private Decimal monsiniestro3anio;
        private Int32 cansiniestro4anio;
        private Decimal monsiniestro4anio;
        private Int32 cansiniestro5anio;
        private Decimal monsiniestro5anio;
        private Int32 idtiprechazo;
        private String obstiprechazo;
        private String usrregrechazo;
        private DateTime fecregrechazo;
        private Int32 codsponsor;
        private Int32 idproducto;
        private Int32 idtipovehiculo;
        private Int32 idAsegurador;
        private String desAsegurador;
        private String desCategoria;
        private String seguroplan;
        private Decimal tasa;
        private Decimal tasaporc;
        private Decimal costoanual;
        private Decimal porcrecargo;
        private Decimal importerecargo;
        private Decimal porcdescuento;
        private Decimal importedescuento;
        private Int32 idtipo;
        private String desTipo;
        private String rutalogoaseg;
        private Int32 correlativo;

        private Int32 idDescuento;
        private Int32 idRecargo;
        private Int32 idAdicional;
        private decimal montoAdicional;
        private Boolean pagoContado;

        #endregion

        #region Propiedades
        /*
         * idpromocion;
        private String despromocion;
         */

        public Int32 IdDescuento
        {
            get { return this.idDescuento; }
            set { this.idDescuento = value; }
        }

        public Int32 IdRecargo
        {
            get { return this.idRecargo; }
            set { this.idRecargo = value; }
        }

        public Int32 IdAdicional
        {
            get { return this.idAdicional; }
            set { this.idAdicional = value; }
        }

        public decimal MontoAdicional
        {
            get { return this.montoAdicional; }
            set { this.montoAdicional = value; }
        }

        public Boolean PagoContado
        {
            get { return this.pagoContado; }
            set { this.pagoContado = value; }
        }


        public Int32 IdPromocion 
        {
            get { return this.idpromocion; }
            set { this.idpromocion = value; }
        }

        public String DesPromocion
        {
            get { return this.despromocion; }
            set { this.despromocion = value; }
        }

        public Int32 Correlativo 
        {
            get { return this.correlativo; }
            set { this.correlativo = value; }
        }

        public String RutaLogoAseg
        {
            get { return this.rutalogoaseg; }
            set { this.rutalogoaseg = value; }
        }
        public String DesTipo 
        {
            get { return this.desTipo; }
            set { this.desTipo = value; }
        }

        public Int32 IdTipo 
        {
            get { return this.idtipo; }
            set { this.idtipo = value; }
        }
        public Decimal ImporteDescuento
        {
            get { return this.importedescuento; }
            set { this.importedescuento = value; }
        }

        public Decimal PorcDescuento
        {
            get { return this.porcdescuento; }
            set { this.porcdescuento = value; }
        }
        public Decimal ImporteRecargo
        {
            get { return this.importerecargo; }
            set { this.importerecargo = value; }
        }
        public Decimal PorcRecargo
        {
            get { return this.porcrecargo; }
            set { this.porcrecargo = value; }
        }
        public Decimal CostoAnual
        {
            get { return this.costoanual; }
            set { this.costoanual = value; }
        }
        public Decimal TasaPorc
        {
            get { return this.tasaporc; }
            set { this.tasaporc = value; }
        }
        public Decimal Tasa
        {
            get { return this.tasa; }
            set { this.tasa = value; }
        }
        public String SeguroPlan
        {
            get { return this.seguroplan; }
            set { this.seguroplan = value; }
        }
        public String DesCategoria
        {
            get { return this.desCategoria; }
            set { this.desCategoria = value; }
        }
        public String DesAsegurador
        {
            get { return this.desAsegurador; }
            set { this.desAsegurador = value; }
        }
        public Int32 IdAsegurador
        {
            get { return this.idAsegurador; }
            set { this.idAsegurador = value; }
        }
        public Int32 IdPeriodo
        {
            get { return idperiodo; }
            set { idperiodo = value; }
        }

        public Decimal NroCotizacion
        {
            get { return nrocotizacion; }
            set { nrocotizacion = value; }
        }

        public String ApePaterno
        {
            get { return apepaterno; }
            set { apepaterno = value; }
        }

        public String ApeMaterno
        {
            get { return apematerno; }
            set { apematerno = value; }
        }

        public String PriNombre
        {
            get { return prinombre; }
            set { prinombre = value; }
        }

        public String SegNombre
        {
            get { return segnombre; }
            set { segnombre = value; }
        }

        public String IdTipoDocumento
        {
            get { return idtipodocumento; }
            set { idtipodocumento = value; }
        }

        public String NroDocumento
        {
            get { return nrodocumento; }
            set { nrodocumento = value; }
        }

        public String Sexo
        {
            get { return sexo; }
            set { sexo = value; }
        }

        public DateTime FecNacimiento
        {
            get { return fecnacimiento; }
            set { fecnacimiento = value; }
        }

        public Int32 IdCiudad
        {
            get { return idciudad; }
            set { idciudad = value; }
        }

        public String Direccion
        {
            get { return direccion; }
            set { direccion = value; }
        }

        public String TelDomicilio1
        {
            get { return teldomicilio1; }
            set { teldomicilio1 = value; }
        }

        public String TelDomicilio2
        {
            get { return teldomicilio2; }
            set { teldomicilio2 = value; }
        }

        public String TelDomicilio3
        {
            get { return teldomicilio3; }
            set { teldomicilio3 = value; }
        }

        public String TelMovil1
        {
            get { return telmovil1; }
            set { telmovil1 = value; }
        }

        public String TelMovil2
        {
            get { return telmovil2; }
            set { telmovil2 = value; }
        }

        public String TelMovil3
        {
            get { return telmovil3; }
            set { telmovil3 = value; }
        }

        public String TelOficina1
        {
            get { return teloficina1; }
            set { teloficina1 = value; }
        }

        public String TelOficina2
        {
            get { return teloficina2; }
            set { teloficina2 = value; }
        }

        public String TelOficina3
        {
            get { return teloficina3; }
            set { teloficina3 = value; }
        }

        public String Email1
        {
            get { return email1; }
            set { email1 = value; }
        }

        public String Email2
        {
            get { return email2; }
            set { email2 = value; }
        }

        public DateTime FecIniVigPoliza
        {
            get { return fecinivigpoliza; }
            set { fecinivigpoliza = value; }
        }

        public DateTime FecFinVigPoliza
        {
            get { return fecfinvigpoliza; }
            set { fecfinvigpoliza = value; }
        }

        public Int32 IdMarca
        {
            get { return idmarca; }
            set { idmarca = value; }
        }

        public Int32 IdModelo
        {
            get { return idmodelo; }
            set { idmodelo = value; }
        }

        public Int32 IdClase
        {
            get { return idclase; }
            set { idclase = value; }
        }

        public String NroPlaca
        {
            get { return nroplaca; }
            set { nroplaca = value; }
        }

        public Int32 AnioFab
        {
            get { return aniofab; }
            set { aniofab = value; }
        }

        public String NroMotor
        {
            get { return nromotor; }
            set { nromotor = value; }
        }

        public String NroSerie
        {
            get { return nroserie; }
            set { nroserie = value; }
        }

        public String Color
        {
            get { return color; }
            set { color = value; }
        }

        public Int32 IdUsoVehiculo
        {
            get { return idusovehiculo; }
            set { idusovehiculo = value; }
        }

        public Int32 NroAsientos
        {
            get { return nroasientos; }
            set { nroasientos = value; }
        }

        public Boolean EsTimonCambiado
        {
            get { return estimoncambiado; }
            set { estimoncambiado = value; }
        }

        public Boolean ReqGPS
        {
            get { return reqgps; }
            set { reqgps = value; }
        }

        public Decimal ValOriVehiculo
        {
            get { return valorivehiculo; }
            set { valorivehiculo = value; }
        }

        public Decimal ValVehiculo
        {
            get { return valvehiculo; }
            set { valvehiculo = value; }
        }

        public Boolean CamValVehiculo
        {
            get { return camvalvehiculo; }
            set { camvalvehiculo = value; }
        }

        public Decimal CamPorcentaje
        {
            get { return camporcentaje; }
            set { camporcentaje = value; }
        }

        public Decimal PrimaAnual
        {
            get { return primaanual; }
            set { primaanual = value; }
        }

        public Decimal TasaAnual
        {
            get { return tasaanual; }
            set { tasaanual = value; }
        }

        public Decimal PrimaBianual
        {
            get { return primabianual; }
            set { primabianual = value; }
        }

        public Decimal TasaBianual
        {
            get { return tasabianual; }
            set { tasabianual = value; }
        }

        public Int32 Condicion
        {
            get { return condicion; }
            set { condicion = value; }
        }

        public Decimal PorcentajeC
        {
            get { return porcentajec; }
            set { porcentajec = value; }
        }

        public Decimal PrimaAnualC
        {
            get { return primaanualc; }
            set { primaanualc = value; }
        }

        public Decimal TasaAnualC
        {
            get { return tasaanualc; }
            set { tasaanualc = value; }
        }

        public Decimal PrimaBianualC
        {
            get { return primabianualc; }
            set { primabianualc = value; }
        }

        public Decimal TasaBianualC
        {
            get { return tasabianualc; }
            set { tasabianualc = value; }
        }

        public Decimal ValorTEA
        {
            get { return this.valortea; }
            set { this.valortea = value; }
        }

        public Int32 IdEstado
        {
            get { return idestado; }
            set { idestado = value; }
        }

        public Boolean ReqInspeccion
        {
            get { return reqinspeccion; }
            set { reqinspeccion = value; }
        }

        public String DirInspeccion
        {
            get { return dirinspeccion; }
            set { dirinspeccion = value; }
        }

        public String FecHorInspeccion
        {
            get { return fechorinspeccion; }
            set { fechorinspeccion = value; }
        }

        public DateTime FecValidez
        {
            get { return fecvalidez; }
            set { fecvalidez = value; }
        }

        public Int32 IdCategoria
        {
            get { return this.idcategoria; }
            set { this.idcategoria = value; }
        }

        public Int32 CanSiniestro1Anio
        {
            get { return cansiniestro1anio; }
            set { cansiniestro1anio = value; }
        }

        public Decimal MonSiniestro1Anio
        {
            get { return monsiniestro1anio; }
            set { monsiniestro1anio = value; }
        }

        public Int32 CanSiniestro2Anio
        {
            get { return cansiniestro2anio; }
            set { cansiniestro2anio = value; }
        }

        public Decimal MonSiniestro2Anio
        {
            get { return monsiniestro2anio; }
            set { monsiniestro2anio = value; }
        }

        public Int32 CanSiniestro3Anio
        {
            get { return cansiniestro3anio; }
            set { cansiniestro3anio = value; }
        }

        public Decimal MonSiniestro3Anio
        {
            get { return monsiniestro3anio; }
            set { monsiniestro3anio = value; }
        }

        public Int32 CanSiniestro4Anio
        {
            get { return cansiniestro4anio; }
            set { cansiniestro4anio = value; }
        }

        public Decimal MonSiniestro4Anio
        {
            get { return monsiniestro4anio; }
            set { monsiniestro4anio = value; }
        }

        public Int32 CanSiniestro5Anio
        {
            get { return cansiniestro5anio; }
            set { cansiniestro5anio = value; }
        }

        public Decimal MonSiniestro5Anio
        {
            get { return monsiniestro5anio; }
            set { monsiniestro5anio = value; }
        }

        public Int32 IdTipRechazo
        {
            get { return this.idtiprechazo; }
            set { this.idtiprechazo = value; }
        }

        public String ObsTipRechazo
        {
            get { return this.obstiprechazo; }
            set { this.obstiprechazo = value; }
        }

        public String UsrRegRechazo
        {
            get { return this.usrregrechazo; }
            set { this.usrregrechazo = value; }
        }

        public DateTime FecRegRechazo
        {
            get { return this.fecregrechazo; }
            set { this.fecregrechazo = value; }
        }

        public Int32 CodSponsor
        {
            get { return this.codsponsor; }
            set { this.codsponsor = value; }
        }

        public Int32 IdProducto
        {
            get { return this.idproducto; }
            set { this.idproducto = value; }
        }

        public Int32 IdTipoVehiculo
        {
            get { return this.idtipovehiculo; }
            set { this.idtipovehiculo = value; }
        }
        #endregion

        #region Campos Consulta
        private String distrito;
        private String provincia;
        private String departamento;
        private String desclase;
        private String desmarca;
        private String desmodelo;
        private String desusovehiculo;
        private String desestado;
        private Decimal calcprimaanual;
        private Decimal calcprimabianual;
        private Decimal calcprimaanualc;
        private Decimal calcprimabianualc;
        private String valorstring1;
        private String valorstring2;
        private String valorstring3;
        private String valorstring4;
        private String desproducto;
        private String telefonodomicilio;
        private String telefonomovil;
        private String telefonooficina;
        private String destipovehiculo;
        private Decimal primaanualmensual;
        private Decimal primabianualmensual;
        private Int32 cantVigencias;
        private String asesor;
        private Int32 idtasa;
        private Decimal valortasa;
        #endregion

        #region Propiedades Consulta
        public String PeriodoCotizacion
        {
            get { return this.idperiodo + "-" + this.NroCotizacion; }
        }

        public String NombreApellido
        {
            get { return this.prinombre + " " + this.segnombre + " " + this.apepaterno + " " + this.apematerno; }
        }

        public String TelefonoDomicilio
        {
            get
            {
                if (this.teldomicilio1 != NullTypes.CadenaNull)
                {
                    this.telefonodomicilio = this.teldomicilio1;
                }

                if (this.teldomicilio2 != NullTypes.CadenaNull)
                {
                    this.telefonodomicilio = this.telefonodomicilio + " / " + this.teldomicilio2;
                }

                if (this.teldomicilio3 != NullTypes.CadenaNull)
                {
                    this.telefonodomicilio = this.telefonodomicilio + " / " + this.teldomicilio3;
                }

                return telefonodomicilio;
            }
        }

        public String TelefonoMovil
        {
            get
            {
                if (this.telmovil1 != NullTypes.CadenaNull)
                {
                    this.telefonomovil = this.telmovil1;
                }

                if (this.telmovil2 != NullTypes.CadenaNull)
                {
                    this.telefonomovil = this.telefonomovil + " / " + this.telmovil2;
                }

                if (this.telmovil3 != NullTypes.CadenaNull)
                {
                    this.telefonomovil = this.telefonomovil + " / " + this.telmovil3;
                }

                return this.telefonomovil;
            }
        }

        public String TelefonoOficina
        {
            get
            {
                if (this.teloficina1 != NullTypes.CadenaNull)
                {
                    this.telefonooficina = this.teloficina1;
                }

                if (this.teloficina2 != NullTypes.CadenaNull)
                {
                    this.telefonooficina = this.telefonooficina + " / " + this.teloficina2;
                }

                if (this.teloficina3 != NullTypes.CadenaNull)
                {
                    this.telefonooficina = this.telefonooficina + " / " + this.teloficina3;
                }

                return telefonooficina;
            }
        }

        public String Distrito
        {
            get { return distrito; }
            set { distrito = value; }
        }

        public String Provincia
        {
            get { return provincia; }
            set { provincia = value; }
        }

        public String Departamento
        {
            get { return departamento; }
            set { departamento = value; }
        }

        /// <summary>
        /// Descripción de clase del vehículo.
        /// </summary>        
        public String DesClase
        {
            get { return desclase; }
            set { desclase = value; }
        }

        /// <summary>
        /// Descripción de marca del vehículo.
        /// </summary>
        public String DesMarca
        {
            get { return desmarca; }
            set { desmarca = value; }
        }

        /// <summary>
        /// Descripción de modelo del vahiculo.
        /// </summary>
        public String DesModelo
        {
            get { return desmodelo; }
            set { desmodelo = value; }
        }

        public String DesUsoVehiculo
        {
            get { return desusovehiculo; }
            set { desusovehiculo = value; }
        }

        public String DesEstado
        {
            get { return desestado; }
            set { desestado = value; }
        }

        public String DesProducto
        {
            get { return this.desproducto; }
            set { this.desproducto = value; }
        }

        public String DesTipoVehiculo
        {
            get { return this.destipovehiculo; }
            set { this.destipovehiculo = value; }
        }

        public Decimal CalcPrimaAnual
        {
            get { return this.PrimaAnualMensual; }

        }

        //public Decimal CalcPrimaBianual
        //{
        //    get
        //    {
        //        if (this.TasaBianual != NullTypes.DecimalNull)
        //        {
        //            this.calcprimabianual = this.ValVehiculo * (this.TasaBianual / 100);

        //            if (this.valortea != NullTypes.DecimalNull)
        //            {

        //            }
        //        }
        //        else
        //        {
        //            this.calcprimabianual = NullTypes.DecimalNull;
        //        }

        //        return this.calcprimabianual;
        //    }

        //    set { this.calcprimabianual = value; }
        //}

        //public Decimal CalcPrimaAnualC
        //{
        //    get
        //    {
        //        if (this.TasaAnualC != NullTypes.DecimalNull)
        //        {
        //            this.calcprimaanualc = this.ValVehiculo * (this.TasaAnualC / 100);
        //        }
        //        else
        //        {
        //            this.calcprimaanualc = NullTypes.DecimalNull;
        //        }

        //        return calcprimaanualc;
        //    }
        //    set { calcprimaanualc = value; }
        //}

        //public Decimal CalcPrimaBianualC
        //{
        //    get
        //    {
        //        if (this.TasaBianual != NullTypes.DecimalNull)
        //        {
        //            this.calcprimabianualc = this.ValVehiculo * (this.TasaBianualC / 100);
        //        }
        //        else
        //        {
        //            this.calcprimabianualc = NullTypes.DecimalNull;
        //        }

        //        return this.calcprimabianualc;
        //    }
        //    set { this.calcprimabianualc = value; }

        //}

        public String ValorString1
        {
            get { return this.valorstring1; }
            set { this.valorstring1 = value; }
        }

        public String ValorString2
        {
            get { return this.valorstring2; }
            set { this.valorstring2 = value; }
        }

        public String ValorString3
        {
            get { return this.valorstring3; }
            set { this.valorstring3 = value; }
        }

        public String ValorString4
        {
            get { return this.valorstring4; }
            set { this.valorstring4 = value; }
        }

        public Decimal PrimaAnualMensual
        {
            get { return this.primaanualmensual; }
            set { this.primaanualmensual = value; }
        }

        public Decimal PrimaBianualMensual
        {
            get { return this.primabianualmensual; }
            set { this.primabianualmensual = value; }
        }

        public Int32 CantVigencias 
        {
            get { return this.cantVigencias; }
            set { this.cantVigencias = value; }
        }

        public String Asesor 
        {
            get { return this.asesor; }
            set { this.asesor = value; } 
        }

        public Int32 IdTasa 
        {
            get { return this.idtasa; }
            set { this.idtasa = value; }
        }

        public Decimal ValorTasa
        {
            get { return this.valortasa; }
            set { this.valortasa = value; }
        }
        #endregion
    }
}
