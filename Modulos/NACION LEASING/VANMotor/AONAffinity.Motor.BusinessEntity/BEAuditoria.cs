﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    /// <summary>
    /// Clase de entidad del negocio que referncia a los campos auditables.
    /// </summary>
    [Serializable]
    public class BEAuditoria
    {
        #region Atributos
        private DateTime fechacreacion;
        private DateTime fechamodificacion;
        private String usuariocreacion;
        private String usuariomodificacion;
        private Boolean estadoregistro;
        #endregion

        #region Propiedades
        /// <summary>
        /// Fecha de creacion del registro.
        /// </summary>
        public DateTime FechaCreacion
        {
            get { return fechacreacion; }
            set { fechacreacion = value; }
        }

        /// <summary>
        /// Fecha de modificación del registro.
        /// </summary>
        public DateTime FechaModificacion
        {
            get { return fechamodificacion; }
            set { fechamodificacion = value; }
        }

        /// <summary>
        /// Usuario creación del registro.
        /// </summary>
        public String UsuarioCreacion
        {
            get { return usuariocreacion; }
            set { usuariocreacion = value; }
        }

        /// <summary>
        /// Usuario modificador del Registro.
        /// </summary>
        public String UsuarioModificacion
        {
            get { return usuariomodificacion; }
            set { usuariomodificacion = value; }
        }

        /// <summary>
        /// Estado del registro.
        /// </summary>
        public Boolean EstadoRegistro
        {
            get { return estadoregistro; }
            set { estadoregistro = value; }
        }
        #endregion
    }
}
