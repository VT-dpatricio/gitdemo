﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AONAffinity.Library.Resources;

namespace AONAffinity.Motor.BusinessEntity
{
    /// <summary>
    /// Clase entidad que define la tabla Modelo.
    /// </summary>
    [Serializable]
    public class BEModelo : BEAuditoria
    {
        #region Constructor
        /// <summary>
        /// Constructor de BEModelo.
        /// </summary>
        public BEModelo()
        {
            this.idmodelo=0;
            this.CodExterno = NullTypes.CadenaNull;
            this.UsuarioModificacion = NullTypes.CadenaNull;
            this.FechaModificacion = NullTypes.FechaNull;
        }
        #endregion

        #region campos
        private Int32 idmodelo;
        private Int32 idmarca;
        private String descripcion;
        private Int32 idclase;
        private String codexterno;
        private Boolean asegurable;
        private Int32 idtipovehiculo;
        #endregion

        #region Propiedades
        /// <summary>
        /// Código de modelo.
        /// </summary>
        public Int32 IdModelo
        {
            get { return this.idmodelo; }
            set { this.idmodelo = value; }
        }

        /// <summary>
        /// Código de marca.
        /// </summary>
        public Int32 IdMarca
        {
            get { return this.idmarca; }
            set { this.idmarca = value; }
        }

        /// <summary>
        /// Descripción de modelo.
        /// </summary>
        public String Descripcion
        {
            get { return this.descripcion; }
            set { this.descripcion = value; }
        }

        /// <summary>
        /// Código de clase.
        /// </summary>
        public Int32 IdClase
        {
            get { return this.idclase; }
            set { this.idclase = value; }
        }

        public String CodExterno
        {
            get { return this.codexterno; }
            set { this.codexterno = value; }
        }

        public Boolean Asegurable
        {
            get { return this.asegurable; }
            set { this.asegurable = value; }
        }

        public Int32 IdTipoVehiculo
        {
            get { return this.idtipovehiculo; }
            set { this.idtipovehiculo = value; }
        }
        #endregion

        #region Campos Consulta
        private String desmarca;
        private String desclase;
        private String destipovehiculo;
        private String codextmarca;
        #endregion

        #region Propiedades Consulta
        /// <summary>
        /// Descripción de marca.
        /// </summary>
        public String DesMarca
        {
            get { return this.desmarca; }
            set { this.desmarca = value; }
        }

        public String DesClase
        {
            get { return this.desclase; }
            set { this.desclase = value; }
        }

        public String DesTipoVehiculo
        {
            get { return this.destipovehiculo; }
            set { this.destipovehiculo = value; }
        }

        public String CodExtMarca
        {
            get { return this.codextmarca; }
            set { this.codextmarca = value; }
        }
        #endregion
    }
}
