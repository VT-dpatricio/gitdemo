﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    /// <summary>
    /// Clase de entidad del negócio, hace referencia a la tabla ProductoEmail.
    /// </summary>
    public class BEProductoEmail : BEAuditoria 
    {
        #region Campos
        private Int32 idproducto;
        private String emailfrom;
        private String emailcc;
        private String emailcco;
        private String emailsubject;
        private String emailbody;
        private String emailpws;
        private String emailsmtp;
        private Int32 emailport;        
        private String rutaarchivo;
        private String rutaimgasegurador;
        private String rutaimgsponsor;
        #endregion

        #region Propiedades
        public Int32 IdProducto 
        {
            get { return this.idproducto; }
            set { this.idproducto = value; }
        }
        
        public String EmailFrom 
        {
            get { return this.emailfrom; }
            set { this.emailfrom = value; }
        }

        public String EmailCC 
        {
            get { return this.emailcc; }
            set { this.emailcc = value; }
        }

        public String EmailCCO 
        {
            get { return this.emailcco; }
            set { this.emailcco = value; }
        }

        public String EmailSubject 
        {
            get { return this.emailsubject; }
            set { this.emailsubject = value; }
        }

        public String EmailBody
        {
            get { return this.emailbody; }
            set { this.emailbody = value; }
        }

        public String EmailPws
        {
            get { return this.emailpws; }
            set { this.emailpws = value; }
        }

        public String EmailSMTP 
        {
            get { return this.emailsmtp; }
            set { this.emailsmtp = value; }
        }

        public Int32 EmailPort 
        {
            get { return this.emailport; }
            set { this.emailport = value; }
        }

        public String RutaArchivo 
        {
            get { return this.rutaarchivo; }
            set { this.rutaarchivo = value; }
        }

        public String RutaImgAsegurador 
        {
            get { return this.rutaimgasegurador; }
            set { this.rutaimgasegurador = value; }
        }

        public String RutaImgSponsor
        {
            get { return this.rutaimgsponsor; }
            set { this.rutaimgsponsor = value; }
        }
        #endregion
    }
}