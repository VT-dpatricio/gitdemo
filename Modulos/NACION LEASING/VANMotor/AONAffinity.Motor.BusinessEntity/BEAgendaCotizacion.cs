﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AONAffinity.Library.Resources;

namespace AONAffinity.Motor.BusinessEntity
{
    /// <summary>
    /// Clase de entidad del negocio que referencia a la tabla AgendaCotizacion.
    /// </summary>
    [Serializable]
    public class BEAgendaCotizacion : BEAuditoria 
    {
        #region Campos
        private Int32 idperiodo;
	    private Decimal nrocotizacion;
	    private DateTime fechaagenda;
	    private String codinformador;
	    private String observaciones;	    
        #endregion

        #region Propiedades
        /// <summary>
        /// Código de periodo.
        /// </summary>
        public Int32 IdPeriodo 
        {
            get { return this.idperiodo; }
            set { this.idperiodo = value; }
        }

        /// <summary>
        /// Nro. de cotización.
        /// </summary>
        public Decimal NroCotizacion 
        {
            get { return this.nrocotizacion; }
            set { this.nrocotizacion = value; }
        }

        /// <summary>
        /// Fecha de agenda.
        /// </summary>
        public DateTime FechaAgenda 
        {
            get { return this.fechaagenda; }
            set { this.fechaagenda = value; }
        }

        /// <summary>
        /// Código de informador.
        /// </summary>
        public String CodInformador 
        {
            get { return this.codinformador; }
            set { this.codinformador = value; }
        }

        /// <summary>
        /// Observaciones de la agenda.
        /// </summary>
        public String Observaciones 
        {
            get { return this.observaciones; }
            set { this.observaciones = value; }
        }
        #endregion
    }
}