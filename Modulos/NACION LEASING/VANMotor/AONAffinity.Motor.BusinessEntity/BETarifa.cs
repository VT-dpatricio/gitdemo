﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    public class BETarifa : BEAuditoria
    {
        #region Campos
        private Int32 idtarifa;
        private Int32 idproducto;
        private Int32 idcategoria;
        private Int32 idmodelo;
        private Int16 nroasientos;
        private Int32 aniofab;
        private Decimal valorvehiculo;
        private DateTime fecinivigencia;
        private DateTime fecfinvigencia;
        #endregion

        #region Propiedades
        public Int32 IdTarifa
        {
            get { return this.idtarifa; }
            set { this.idtarifa = value; }
        }

        public Int32 IdProducto
        {
            get { return this.idproducto; }
            set { this.idproducto = value; }
        }

        public Int32 IdCategoria
        {
            get { return this.idcategoria; }
            set { this.idcategoria = value; }
        }

        public Int32 IdModelo
        {
            get { return this.idmodelo; }
            set { this.idmodelo = value; }
        }

        public Int16 NroAsientos
        {
            get { return this.nroasientos; }
            set { this.nroasientos = value; }
        }

        public Int32 AnioFab
        {
            get { return this.aniofab; }
            set { this.aniofab = value; }
        }

        public Decimal ValorVehiculo
        {
            get { return this.valorvehiculo; }
            set { this.valorvehiculo = value; }
        }

        public DateTime FecIniVigencia
        {
            get { return this.fecinivigencia; }
            set { this.fecinivigencia = value; }
        }

        public DateTime FecFinVigencia
        {
            get { return this.fecfinvigencia; }
            set { this.fecfinvigencia = value; }
        }
        #endregion

        #region Antiguo
        #region Campos
        //private Int32 idmodelo;
        //private Int32 idanio;
        //private Decimal valorvehiculo;
        //private DateTime fecvigencia;
        #endregion

        #region Propiedades
        //public Int32 IdModelo
        //{
        //    get { return idmodelo; }
        //    set { idmodelo = value; }
        //}

        //public Int32 IdAnio
        //{
        //    get { return idanio; }
        //    set { idanio = value; }
        //}

        //public Decimal ValorVehiculo
        //{
        //    get { return valorvehiculo; }
        //    set { valorvehiculo = value; }
        //}

        //public DateTime FecVigencia
        //{
        //    get { return fecvigencia; }
        //    set { fecvigencia = value; }
        //}
        #endregion
        #endregion        
    }
}
