﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    [Serializable]
    public class BECotizacionDetalle : BEAuditoria
    {
        Int32 idpromocion;
        String despromocion;
        Int32 correlativo;
        Decimal nrocotizacion;
        Int32 idpoducto;
        Int32 idmarca;
        Int32 idmodelo;
        Int32 anioFab;
        Decimal valorasegurado;
        Int32 idtasa;
        Decimal impdescuento;
        Decimal imprecargo;
        Decimal porcdescuento;
        Decimal porcrecargo;
        Decimal primacosto;
        Decimal primatotal;
        Decimal primamensual;
        String monedaprima;
        String opcionplan;
        Boolean elegido;
        String nroplaca;

        //
        String desproducto;
        String desasegurador;
        String rutalogo;
        String prinombre;
        String segnombre;
        String apepaterno;
        String apematerno;
        String idtipodocumento;
        String nrodocumento;
        String desmarca;
        String desmodelo;
        Decimal valorvehiculo;
        Decimal porctasa;

        private Int32 idDescuento;
        private Int32 idRecargo;
        private Int32 idAdicional;
        private decimal montoAdicional;
        private Boolean pagoContado;
        private Int32 idcategoria;
        Decimal tasa;

        public Decimal Tasa
        {
            get { return this.tasa; }
            set { this.tasa = value; }
        }

        public Int32 IdCategoria
        {
            get { return this.idcategoria; }
            set { this.idcategoria = value; }
        }

        public Int32 IdDescuento
        {
            get { return this.idDescuento; }
            set { this.idDescuento = value; }
        }

        public Int32 IdRecargo
        {
            get { return this.idRecargo; }
            set { this.idRecargo = value; }
        }

        public Int32 IdAdicional
        {
            get { return this.idAdicional; }
            set { this.idAdicional = value; }
        }

        public decimal MontoAdicional
        {
            get { return this.montoAdicional; }
            set { this.montoAdicional = value; }
        }

        public Boolean PagoContado
        {
            get { return this.pagoContado; }
            set { this.pagoContado = value; }
        }


        public Int32 IdPromocion
        {
            get { return this.idpromocion; }
            set { this.idpromocion = value; }
        }

        public String DesPromocion
        {
            get { return this.despromocion; }
            set { this.despromocion = value; }
        }

        public String DesProducto
        {
            get { return this.desproducto; }
            set { this.desproducto = value; }
        }
        public String DesAsegurador
        {
            get { return this.desasegurador; }
            set { this.desasegurador = value; }
        }
        public String RutaLogo
        {
            get { return this.rutalogo; }
            set { this.rutalogo = value; }
        }

        public String PriNombre
        {
            get { return this.prinombre; }
            set { this.prinombre = value; }
        }
        public String SegNombre
        {
            get { return this.segnombre; }
            set { this.segnombre = value; }
        }

        public String ApePaterno
        {
            get { return this.apepaterno; }
            set { this.apepaterno = value; }
        }

        public String ApeMaterno
        {
            get { return this.apematerno; }
            set { this.apematerno = value; }
        }

        public String IdTipoDocumento
        {
            get { return this.idtipodocumento; }
            set { this.idtipodocumento = value; }
        }

        public String NroDocumento
        {
            get { return this.nrodocumento; }
            set { this.nrodocumento = value; }
        }

        public String DesMarca
        {
            get { return this.desmarca; }
            set { this.desmarca = value; }
        }

        public String DesModelo
        {
            get { return this.desmodelo; }
            set { this.desmodelo = value; }
        }

        public Decimal ValorVehiculo 
        {
            get { return this.valorvehiculo; }
            set { this.valorvehiculo = value; }
        }

        public Decimal PorcTasa 
        {
            get { return this.porctasa; }
            set { this.porctasa = value; }
        }

        //
        public Int32 Correlativo 
        {
            get { return this.correlativo; }
            set { this.correlativo = value; }
        }

        public Decimal NroCotizacion 
        {
            get { return this.nrocotizacion; }
            set { this.nrocotizacion = value; }
        }

        public Int32 IdProducto
        {
            get { return this.idpoducto; }
            set { this.idpoducto = value; }
        }

        public Int32 IdMarca
        {
            get { return this.idmarca; }
            set { this.idmarca = value; }
        }

        public Int32 IdModelo
        {
            get { return this.idmodelo; }
            set { this.idmodelo = value; }
        }

        public Int32 AnioFab
        {
            get { return this.anioFab; }
            set { this.anioFab = value; }
        }

        public Decimal ValorAsegurado
        {
            get { return this.valorasegurado; }
            set { this.valorasegurado = value; }
        }

        public Int32 IdTasa
        {
            get { return this.idtasa; }
            set { this.idtasa = value; }
        }

        public Decimal ImpDescuento
        {
            get { return this.impdescuento; }
            set { this.impdescuento = value; }
        }

        public Decimal ImpRecargo
        {
            get { return this.imprecargo; }
            set { this.imprecargo = value; }
        }      

        public Decimal PorcDescuento
        {
            get { return this.porcdescuento; }
            set { this.porcdescuento = value; }
        }

        public Decimal PorcRecargo
        {
            get { return this.porcrecargo; }
            set { this.porcrecargo = value; }
        }

        public Decimal PrimaCosto
        {
            get { return this.primacosto; }
            set { this.primacosto = value; }
        }

        public Decimal PrimaTotal
        {
            get { return this.primatotal; }
            set { this.primatotal = value; }
        }

        public Decimal PrimaMensual
        {
            get { return this.primamensual; }
            set { this.primamensual = value; }
        }

        public String MonedaPrima 
        {
            get { return this.monedaprima; }
            set { this.monedaprima = value; }
        }

        public String OpcionPlan 
        {
            get { return this.opcionplan; }
            set { this.opcionplan = value; }
        }

        public Boolean Elegido 
        {
            get { return this.elegido; }
            set { this.elegido = value; }
        }

        public String NroPlaca 
        {
            get { return this.nroplaca; }
            set { this.nroplaca = value; }
        }
    }
}
