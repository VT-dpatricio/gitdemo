﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    [Serializable] 
    public class BEProductoTipoCanal : BEAuditoria 
    {
        #region Campos
        private Int32 idproducto;
	    private Int32 idtipocanal;
        private String codexterno;
        #endregion

        #region Propiedades
        public Int32 IdProducto 
        {
            get { return this.idproducto; }
            set { this.idproducto = value ;}
        }

        public Int32 IdTipoCanal
        {
            get { return this.idtipocanal; }
            set { this.idtipocanal = value; }
        }

        public String CodExterno
        {
            get { return this.codexterno; }
            set { this.codexterno = value; }
        }
        #endregion

        #region Campos Consulta
        private String descripcion;
        #endregion

        #region Propiedades Consulta
        public String Descripcion 
        {
            get { return this.descripcion; }
            set { this.descripcion = value; }
        }
        #endregion
    }
}
