﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    [Serializable]
    public class BERespuesta
    {
        #region Campos
        private Int32 idrespuesta;
        private String descripcion;
        #endregion

        #region Propiedades
        public Int32 IdRespuesta 
        {
            get { return this.idrespuesta; }
            set { this.idrespuesta = value; }
        }

        public String Descripcion
        {
            get { return this.descripcion; }
            set { this.descripcion = value; }
        }
        #endregion
    }
}
