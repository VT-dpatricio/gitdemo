﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    /// <summary>
    /// Clase de entidad del negocio de la tabla ValorEquivalencia.
    /// </summary>
    [Serializable]
    public class BEValorEquivalencia : BEAuditoria 
    {
        #region Campos
        private Int32 idtipoproceso;
	    private Int32 idtipoarchivo;
	    private Int32 orden;
	    private String valor;
	    private String equivalencia;	    
        #endregion

        #region Propiedades
        /// <summary>
        /// Id tipo proceso.
        /// </summary>
        public Int32 IdTipoProceso
        {
            get { return this.idtipoproceso; }
            set { this.idtipoproceso = value; }
        }

        /// <summary>
        /// Id tipo archivo.
        /// </summary>
        public Int32 IdTipoArchivo 
        {
            get { return this.idtipoarchivo; }
            set { this.idtipoarchivo = value; } 
        }

        /// <summary>
        /// Nro. orden de campo.
        /// </summary>
        public Int32 Orden 
        {
            get { return this.orden; }
            set { this.orden = value; }
        }

        /// <summary>
        /// Valor de campo.
        /// </summary>
        public String Valor
        {
            get { return this.valor; }
            set { this.valor = value; }
        }

        /// <summary>
        /// Valor equivalente.
        /// </summary>
        public String Equivalencia 
        {
            get { return this.equivalencia; }
            set { this.equivalencia = value; }
        }
        #endregion
    }
}
