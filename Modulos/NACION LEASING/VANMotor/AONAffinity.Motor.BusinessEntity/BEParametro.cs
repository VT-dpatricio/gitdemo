﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AONAffinity.Library.Resources;

namespace AONAffinity.Motor.BusinessEntity
{
    /// <summary>
    /// Clase entidad que define la tabla Parametro.
    /// </summary>
    [Serializable] 
    public class BEParametro : BEAuditoria
    {
        #region Campos
        private Int32 idparametro;
        private String descripcion;
        private Int32 idproducto;
        private Int32 valornumeroent;
        private Decimal valornumerodec;
        private String valorcadena;
        private DateTime valorfecha;
        private Nullable<Boolean> valorbool;
        #endregion

        #region Propiedades
        /// <summary>
        /// Código de parámetro.
        /// </summary>
        public Int32 IdParametro
        {
            get { return idparametro; }
            set { idparametro = value; }
        }

        /// <summary>
        /// Descripción del parámetro.
        /// </summary>
        public String Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        /// <summary>
        /// Código de producto.
        /// </summary>
        public Int32 IdProducto
        {
            get { return idproducto; }
            set { idproducto = value; }
        }

        /// <summary>
        /// Valor nro. entero.
        /// </summary>
        public Int32 ValorNumeroEnt
        {
            get { return valornumeroent; }
            set { valornumeroent = value; }
        }

        /// <summary>
        /// Valor nro. decimal.
        /// </summary>
        public Decimal ValorNumeroDec
        {
            get { return valornumerodec; }
            set { valornumerodec = value; }
        }

        /// <summary>
        /// Valor cadena.
        /// </summary>
        public String ValorCadena
        {
            get { return valorcadena; }
            set { valorcadena = value; }
        }

        /// <summary>
        /// Valor fecha.
        /// </summary>
        public DateTime ValorFecha
        {
            get { return valorfecha; }
            set { valorfecha = value; }
        }

        public Nullable<Boolean> ValorBool
        {
            get { return this.valorbool; }
            set { this.valorbool = value; }
        }
        #endregion
    }
}
