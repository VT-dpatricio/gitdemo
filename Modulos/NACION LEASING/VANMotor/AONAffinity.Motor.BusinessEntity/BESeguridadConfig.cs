﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    public class BESeguridadConfig:BEBase
    {
        private string _iDParametro = string.Empty;
        private int _iDEntidad = 0;

        private string _valor = string.Empty;
        public string IDParametro
        {
            get { return this._iDParametro; }
            set { this._iDParametro = value; }
        }

        public int IDEntidad
        {
            get { return _iDEntidad; }
            set { _iDEntidad = value; }
        }

        public string Valor
        {
            get { return this._valor; }
            set { this._valor = value; }
        }
    }
}
