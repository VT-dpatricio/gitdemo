﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AONAffinity.Library.Resources; 

namespace AONAffinity.Motor.BusinessEntity
{
    [Serializable]
    public class BEProceso : BEAuditoria
    {
        #region Campos
        private Int32 idproceso;
        private String descripcion;
        private String archivo;
        private Boolean procesado;
        private String archivolog;
        private DateTime fecpoceso;
        private Int32 idtipproceso;
        private Int32 regtotal;
        private Int32 regtotalval;
        private Int32 regtotalerr;
        private Int32 nroobservaciones;
        private String rutaarchivologcotizacion;
        private String archivologcotizacion;
        private Boolean cotizado;
        private Int32 idProducto;
        #endregion

        #region Propiedades
        public Int32 IdProceso
        {
            get { return this.idproceso; }
            set { this.idproceso = value; }
        }

        public String Descripcion
        {
            get { return this.descripcion; }
            set { this.descripcion = value; }
        }

        public String Archivo
        {
            get { return this.archivo; }
            set { this.archivo = value; }
        }

        public Boolean Procesado
        {
            get { return this.procesado; }
            set { this.procesado = value; }
        }

        public String ArchivoLog
        {
            get { return this.archivolog; }
            set { this.archivolog = value; }
        }

        public DateTime FecProceso
        {
            get { return this.fecpoceso; }
            set { this.fecpoceso = value; }
        }

        public Int32 IdTipProceso
        {
            get { return this.idtipproceso; }
            set { this.idtipproceso = value; }
        }

        public Int32 RegTotal
        {
            get { return this.regtotal; }
            set { this.regtotal = value; }
        }

        public Int32 RegTotalVal
        {
            get { return this.regtotalval; }
            set { this.regtotalval = value; }
        }

        public Int32 RegTotalErr
        {
            get { return regtotalerr; }
            set { regtotalerr = value; }
        }

        public Int32 NroObservaciones
        {
            get { return this.nroobservaciones; }
            set { this.nroobservaciones = value; }
        }

        public String RutaArchivoLogCotizacion
        {
            get { return this.rutaarchivologcotizacion; }
            set { this.rutaarchivologcotizacion = value; }
        }

        public String ArchivoLogCotizacion
        {
            get { return this.archivologcotizacion; }
            set { this.archivologcotizacion = value; }
        }

        public Boolean Cotizado
        {
            get { return this.cotizado; }
            set { this.cotizado = value; }
        }
        #endregion

        #region Campos Consulta
        private String destipproceso;
        private String rutaarchivo;
        private String rutaarchivolog;
        private String rutaarchivoexport;
        private Boolean generaexport;
        private String spexport;
        #endregion

        #region Propiedades Consulta
        public String DesProcesado
        {
            get
            {
                if (procesado == true)
                {
                    return "SI";
                }
                else { return "NO"; }
            }
        }

        public String DesTipProceso
        {
            get { return destipproceso; }
            set { destipproceso = value; }
        }

        public String RutaArchivo
        {
            get { return this.rutaarchivo; }
            set { this.rutaarchivo = value; }
        }

        public String RutaArchivoLog
        {
            get { return this.rutaarchivolog; }
            set { this.rutaarchivolog = value; }
        }

        public String RutaArchivoExport
        {
            get { return this.rutaarchivoexport; }
            set { this.rutaarchivoexport = value; }
        }

        public Boolean GeneraExport
        {
            get { return this.generaexport; }
            set { this.generaexport = value; }
        }

        public String SpExport
        {
            get { return this.spexport; }
            set { this.spexport = value; }
        }

        public Int32 IdProducto 
        {
            get { return this.idProducto; }
            set { this.idProducto = value; }
        }
        #endregion
    }
}
