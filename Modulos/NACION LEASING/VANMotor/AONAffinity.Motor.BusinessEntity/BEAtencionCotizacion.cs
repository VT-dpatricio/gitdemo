﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AONAffinity.Library.Resources;

namespace AONAffinity.Motor.BusinessEntity
{
    /// <summary>
    /// Clase de entidad del negocio que referencia a la tabla AtencionCotizacion.
    /// </summary>
    public class BEAtencionCotizacion : BEAuditoria
    {
        #region Campos
        private Int32 idperiodo;
	    private Decimal nrocotizacion;
	    private String codinformador;
        private DateTime fechaatencion;
        #endregion

        #region Propiedades
        public Int32 IdPeriodo 
        {
            get { return this.idperiodo; }
            set { this.idperiodo = value; }
        }

        public Decimal NroCotizacion 
        {
            get { return this.nrocotizacion; }
            set { this.nrocotizacion = value; }
        }

        public String CodInformador 
        {
            get { return this.codinformador; }
            set { this.codinformador = value; }
        }

        public DateTime FechaAtencion 
        {
            get { return this.fechaatencion; }
            set { this.fechaatencion = value; }
        }
        #endregion

        #region Campos Consulta
        private Int32 tipo;
        #endregion

        #region Propiedades Consulta
        public Int32 Tipo 
        {
            get { return tipo; }
            set { tipo = value; }
        }
        #endregion
    }
}
