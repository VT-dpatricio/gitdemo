﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    [Serializable]
    public class BEProductoMarca : BEAuditoria
    {
        #region Campos
        private Int32 idproducto;
        private Int32 idmarca;
        private String codexterno;
        #endregion

        #region Propiedades
        public Int32 IdProducto
        {
            get { return this.idproducto; }
            set { this.idproducto = value; }
        }

        public Int32 IdMarca
        {
            get { return this.idmarca; }
            set { this.idmarca = value; }
        }

        public String CodExterno
        {
            get { return this.codexterno; }
            set { this.codexterno = value; }
        }
        #endregion

        #region CamposConsulta
        private String descripcion;
        #endregion

        #region Propiedades
        public String Descripcion
        {
            get { return this.descripcion; }
            set { this.descripcion = value; }
        }
        #endregion
    }
}
