﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    public class BESeguridadEvento: BEBase
    {
        private int _iDEvento = 0;
        private int _iDSistema = 0;
        private string _iDUsuario = string.Empty;
        private int _iDTipoEvento = 0;
        private System.DateTime _fechaRegistro = System.DateTime.Now;
        private string _detalle = string.Empty;

        private string _host = string.Empty;
        public int IDEvento
        {
            get { return this._iDEvento; }
            set { this._iDEvento = value; }
        }

        public int IDSistema
        {
            get { return this._iDSistema; }
            set { this._iDSistema = value; }
        }

        public string IDUsuario
        {
            get { return this._iDUsuario; }
            set { this._iDUsuario = value; }
        }

        public int IDTipoEvento
        {
            get { return this._iDTipoEvento; }
            set { this._iDTipoEvento = value; }
        }

        public System.DateTime FechaRegistro
        {
            get { return this._fechaRegistro; }
            set { this._fechaRegistro = value; }
        }

        public string Detalle
        {
            get { return this._detalle; }
            set { this._detalle = value; }
        }

        public string Host
        {
            get { return this._host; }
            set { this._host = value; }
        }

    }
}
