﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    public class BEClase : BEAuditoria 
    {
        #region Campos
        private Int32 idclase;
        private String descripcion;
        private String codexterno;
        #endregion

        #region Propiedades
        public Int32 IdClase
        {
            get { return this.idclase; }
            set { this.idclase = value; }
        }

        public String Descripcion
        {
            get { return this.descripcion; }
            set { this.descripcion = value; }
        }

        public String CodExterno
        {
            get { return this.codexterno; }
            set { this.codexterno = value; }
        }
        #endregion
    }
}
