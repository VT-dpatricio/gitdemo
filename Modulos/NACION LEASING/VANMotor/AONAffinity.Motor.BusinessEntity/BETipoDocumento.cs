﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    public class BETipoDocProducto: BEBase
    {
        #region Campos
        private Int32 idproducto;
        private String idtipodocumento;
        private String nombre;
        #endregion

        #region Propiedades
        /// <summary>
        /// Codigo del producto
        /// </summary>
        public Int32 IdProducto
        {
            get { return idproducto; }
            set { idproducto = value; }
        }
        /// <summary>
        /// Codigo del tipo de documento
        /// </summary>
        public String IdTipoDocumento
        {
            get { return idtipodocumento; }
            set { idtipodocumento = value; }
        }
        /// <summary>
        /// Nombre del tipo de documento
        /// </summary>
        public String Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        #endregion
    }
}
