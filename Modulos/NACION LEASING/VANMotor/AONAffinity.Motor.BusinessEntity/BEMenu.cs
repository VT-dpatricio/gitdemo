﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AONAffinity.Library.Resources;

namespace AONAffinity.Motor.BusinessEntity
{
    [Serializable]
    public class BEMenu : BEAuditoria
    {
        #region Campos
        private Int32 idmenu;
        private String nombre;
        private Int32 nodpadre;
        private String ruta;
        #endregion

        #region Propiedades
        public Int32 IdMenu
        {
            get { return this.idmenu; }
            set { this.idmenu = value; }
        }

        public String Nombre
        {
            get { return this.nombre; }
            set { this.nombre = value; }
        }

        public Int32 NodPadre
        {
            get { return this.nodpadre; }
            set { this.nodpadre = value; }
        }

        public String Ruta
        {
            get { return this.ruta; }
            set { this.ruta = value; }
        }
        #endregion
    }
}
