﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AONAffinity.Library.Resources;

namespace AONAffinity.Motor.BusinessEntity
{
    /// <summary>
    /// Clase de entidad del negocio que referencia  a la tabla VigenciaSiniestro.
    /// </summary>
    [Serializable]
    public class BEVigenciaSiniestro : BEAuditoria
    {
        #region Campos
        private String nromotor;
        private Int32 cantsiniestro1anio;
        private Int32 cantsiniestro2anio;
        private Int32 cantsiniestro3anio;
        private Int32 cantsiniestro4anio;
        private Int32 cantsiniestro5anio;
        private Decimal montosiniestro1anio;
        private Decimal montosiniestro2anio;
        private Decimal montosiniestro3anio;
        private Decimal montosiniestro4anio;
        private Decimal montosiniestro5anio;
        private DateTime fecinivigencia;
        private DateTime fecfinvigencia;        
        #endregion

        #region Propiedades
        /// <summary>
        /// Nro. motor.
        /// </summary>
        public String NroMotor 
        {
            get { return nromotor; }
            set { nromotor = value; }
        }

        /// <summary>
        /// Cantidad de siniestros a 1 anio.
        /// </summary>
        public Int32 CantSiniestro1Anio 
        {
            get { return cantsiniestro1anio; }
            set { cantsiniestro1anio = value; } 
        }

        /// <summary>
        /// Cantidad de siniestros a 2 anios.
        /// </summary>
        public Int32 CantSiniestro2Anio 
        {
            get { return cantsiniestro2anio; }
            set { cantsiniestro2anio = value; }
        }

        /// <summary>
        /// Cantidad de siniestros a 3 anios.
        /// </summary>
        public Int32 CantSiniestro3Anio 
        {
            get { return cantsiniestro3anio; }
            set { cantsiniestro3anio = value; }
        }

        /// <summary>
        /// Cantidad de siniestros a 4 anios.
        /// </summary>
        public Int32 CantSiniestro4Anio 
        {
            get { return cantsiniestro4anio; }
            set { cantsiniestro4anio = value; }
        }

        /// <summary>
        /// Cantidad de siniestros a 5 anios.
        /// </summary>
        public Int32 CantSiniestro5Anio 
        {
            get { return cantsiniestro5anio; }
            set { cantsiniestro5anio = value; }
        }

        /// <summary>
        /// Monto de siniestro a 1 anio.
        /// </summary>
        public Decimal MontoSiniestro1Anio 
        {
            get { return this.montosiniestro1anio; }
            set { this.montosiniestro1anio = value; }
        }

        /// <summary>
        /// Monto de siniestro a 2 anios.
        /// </summary>
        public Decimal MontoSiniestro2Anio 
        {
            get { return this.montosiniestro2anio; }
            set { this.montosiniestro2anio = value; }
        }

        /// <summary>
        /// Monto de siniestro a 3 anios.
        /// </summary>
        public Decimal MontoSiniestro3Anio 
        {
            get { return this.montosiniestro3anio; }
            set { this.montosiniestro3anio = value; }
        }

        /// <summary>
        /// Monto de siniestro a 4 anios.
        /// </summary>
        public Decimal MontoSiniestro4Anio 
        {
            get { return this.montosiniestro4anio; }
            set { this.montosiniestro4anio = value; }
        }

        /// <summary>
        /// Monto de siniestro a 5 anios.
        /// </summary>
        public Decimal MontoSiniestro5Anio 
        {
            get { return this.montosiniestro5anio; }
            set { this.montosiniestro5anio = value; }
        }

        /// <summary>
        /// Fecha de inicio de vigencia.
        /// </summary>
        public DateTime FecIniVigencia 
        {
            get { return fecinivigencia; }
            set { fecinivigencia = value; }
        }

        /// <summary>
        /// Fecha de fin de vigencia.
        /// </summary>
        public DateTime FecFinVigencia 
        {
            get { return fecfinvigencia; }
            set { fecfinvigencia = value; }
        }
        #endregion

        #region Campos Consulta
        private Int32 cantvigencia;
        #endregion

        #region Propiedades Consulta
        /// <summary>
        /// Cantidad de vigencias acumuladas.
        /// </summary>
        public Int32 CantVigencia 
        {
            get { return cantvigencia; }
            set { cantvigencia = value; }
        }
        #endregion

    }
}
