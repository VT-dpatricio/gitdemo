﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    public class BEUsuarioBW: BEBase
    {
        public string Apellido
        {
            get { return this._apellido; }
            set { this._apellido = value; }
        }

        public string Buscar
        {
            get { return this._buscar; }
            set { this._buscar = value; }
        }

        public string Clave
        {
            get { return this._clave; }
            set { this._clave = value; }
        }

        public string DescripcionCargo
        {
            get { return this._descripcionCargo; }
            set { this._descripcionCargo = value; }
        }

        public string DescripcionRol
        {
            get { return this._descripcionRol; }
            set { this._descripcionRol = value; }
        }

        public string DNI
        {
            get { return this._dNI; }
            set { this._dNI = value; }
        }

        public bool Eliminado
        {
            get { return this._eliminado; }
            set { this._eliminado = value; }
        }

        public string Email
        {
            get { return this._email; }
            set { this._email = value; }
        }

        public bool Acceso
        {
            get { return _acceso; }
            set { _acceso = value; }
        }

        public bool Estado
        {
            get { return this._estado; }
            set { this._estado = value; }
        }

        public int IDCargo
        {
            get { return this._iDCargo; }
            set { this._iDCargo = value; }
        }

        public int IDRol
        {
            get { return this._iDRol; }
            set { this._iDRol = value; }
        }

        public string IDUsuario
        {
            get { return this._iDUsuario; }
            set { this._iDUsuario = value; }
        }

        public string Nombre
        {
            get { return this._nombre; }
            set { this._nombre = value; }
        }

        public string Usuario
        {
            get { return this._usuario; }
            set { this._usuario = value; }
        }

        public int IDEntidad
        {
            get { return this._iDEntidad; }
            set { this._iDEntidad = value; }
        }

        public string IDInformador
        {
            get { return this._iDInformador; }
            set { this._iDInformador = value; }
        }

        public string Apellido1
        {
            get { return this._apellido1; }
            set { this._apellido1 = value; }
        }

        public string Apellido2
        {
            get { return this._apellido2; }
            set { this._apellido2 = value; }
        }

        public string Nombre1
        {
            get { return this._nombre1; }
            set { this._nombre1 = value; }
        }

        public string Nombre2
        {
            get { return this._nombre2; }
            set { this._nombre2 = value; }
        }

        public int IDOficina
        {
            get { return this._iDOficina; }
            set { this._iDOficina = value; }
        }

        public int IDCanal
        {
            get { return this._iDCanal; }
            set { this._iDCanal = value; }
        }

        // Fields
        private string _apellido = string.Empty;
        private string _buscar;
        private string _clave = string.Empty;
        private string _descripcionCargo = string.Empty;
        private string _descripcionRol = string.Empty;
        private string _dNI = string.Empty;
        private bool _eliminado = true;

        private string _email = string.Empty;
        //Private _iDCargo As Integer = 0
        private int _iDRol = 0;
        private string _iDUsuario = string.Empty;
        private string _nombre = string.Empty;

        private string _usuario = string.Empty;
        private string _iDInformador = string.Empty;
        private string _apellido1 = string.Empty;
        private string _apellido2 = string.Empty;
        private string _nombre1 = string.Empty;

        private string _nombre2 = string.Empty;
        //--
        private int _iDEntidad = 0;
        private int _iDOficina = 0;
        //--
        private int _iDCargo = 0;
        private int _iDCanal = 0;
        //--
        private bool _estado = true;
        private bool _acceso = false;
    }
}
