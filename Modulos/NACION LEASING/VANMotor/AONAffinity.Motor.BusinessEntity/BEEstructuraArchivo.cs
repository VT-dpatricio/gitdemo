﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    [Serializable]
    public class BEEstructuraArchivo : BEAuditoria
    {
        #region Campos
        private Int32 idtipoproceso;
        private Int32 orden;
        private String tipodato;
        private String valorfijo;
        private String nombre;
        private Int32 inicio;
        private Int32 longitud;
        private Boolean obligatorio;
        private Int32 idtipoarchivo;
        #endregion

        #region Propiedades
        public Int32 IdTipoProceso
        {
            get { return this.idtipoproceso; }
            set { this.idtipoproceso = value; }
        }

        public Int32 Orden
        {
            get { return this.orden; }
            set { this.orden = value; }
        }

        public String TipoDato
        {
            get { return this.tipodato; }
            set { this.tipodato = value; }
        }

        public String ValorFijo
        {
            get { return this.valorfijo; }
            set { this.valorfijo = value; }
        }

        public String Nombre
        {
            get { return this.nombre; }
            set { this.nombre = value; }
        }

        public Int32 Inicio
        {
            get { return this.inicio; }
            set { this.inicio = value; }
        }

        public Int32 Longitud
        {
            get { return this.longitud; }
            set { this.longitud = value; }
        }

        public Boolean Obligatorio
        {
            get { return this.obligatorio; }
            set { this.obligatorio = value; }
        }

        public Int32 IdTipoArchivo 
        {
            get { return this.idtipoarchivo; }
            set { this.idtipoarchivo = value; }
        }
        #endregion
    }
}
