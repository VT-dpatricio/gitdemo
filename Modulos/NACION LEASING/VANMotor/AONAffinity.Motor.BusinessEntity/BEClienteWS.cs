﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    public class BEClienteWS: BEBase
    {
        #region "Campos"
        private string _CodRetorno;
        private string _NroCliente;
        private string _Nombre1;
        private string _Nombre2;
        private string _Apellido1;
        private string _Apellido2;
        private string _NroDocumento;
        private string _FechaNacimiento;
        private string _Sexo;
        private string _IDEstadoCivil;
        private string _EstadoCivil;
        private string _Nacionalidad;
        private string _LugarNacimiento;
        private string _Profesion;
        private string _IDCiudad;
        private string _Ciudad;
        private string _Direccion;
        private string _Telefono;
        private string _OtroTelefono;
        private string _Celular;
        private string _Email;
        private bool _EnRecuperacion = false;
        #endregion        

        #region "Propiedades"

        public bool EnRecuperacion
        {
            get { return _EnRecuperacion; }
            set { _EnRecuperacion = value; }
        }

        public string CodRetorno
        {
            get { return _CodRetorno; }
            set { _CodRetorno = value; }
        }

        public string NroCliente
        {
            get { return _NroCliente; }
            set { _NroCliente = value; }
        }

        public string NroDocumento
        {
            get { return _NroDocumento; }
            set { _NroDocumento = value; }
        }

        public string Nombre1
        {
            get { return _Nombre1; }
            set { _Nombre1 = value; }
        }

        public string Nombre2
        {
            get { return _Nombre2; }
            set { _Nombre2 = value; }
        }

        public string Apellido1
        {
            get { return _Apellido1; }
            set { _Apellido1 = value; }
        }

        public string Apellido2
        {
            get { return _Apellido2; }
            set { _Apellido2 = value; }
        }

        public string FechaNacimiento
        {
            get { return _FechaNacimiento; }
            set { _FechaNacimiento = value; }
        }

        public string Sexo
        {
            get { return _Sexo; }
            set { _Sexo = value; }
        }

        public string IDEstadoCivil
        {
            get { return _IDEstadoCivil; }
            set { _IDEstadoCivil = value; }
        }

        public string EstadoCivil
        {
            get { return _EstadoCivil; }
            set { _EstadoCivil = value; }
        }

        public string Nacionalidad
        {
            get { return _Nacionalidad; }
            set { _Nacionalidad = value; }
        }

        public string LugarNacimiento
        {
            get { return _LugarNacimiento; }
            set { _LugarNacimiento = value; }
        }

        public string Profesion
        {
            get { return _Profesion; }
            set { _Profesion = value; }
        }

        public string IDCiudad
        {
            get { return _IDCiudad; }
            set { _IDCiudad = value; }
        }

        public string Ciudad
        {
            get { return _Ciudad; }
            set { _Ciudad = value; }
        }

        public string Direccion
        {
            get { return _Direccion; }
            set { _Direccion = value; }
        }

        public string Telefono
        {
            get { return _Telefono; }
            set { _Telefono = value; }
        }

        public string OtroTelefono
        {
            get { return _OtroTelefono; }
            set { _OtroTelefono = value; }
        }

        public string Celular
        {
            get { return _Celular; }
            set { _Celular = value; }
        }

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        #endregion

    }
}
