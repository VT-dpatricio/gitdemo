﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    [Serializable]
    public class BETipoDato
    {
        #region Campos
        private String idTipoDato;
        private String descripcion;
        #endregion

        #region Propiedades
        public String IdTipoDato 
        {
            get { return this.idTipoDato; }
            set { this.idTipoDato = value; }
        }

        public String Descripcion 
        {
            get { return this.descripcion; }
            set { this.descripcion = value; }
        }
        #endregion
    }
}
