﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity.Bais
{
    public class BEFrecuencia
    {
        #region Atributos
        private Int32 idfrecuencia;
        private String nombre;
        private Int32 idproducto;
        private String opcion;
        #endregion

        #region Propiedades
        /// <summary>
        /// Código de frecuencia.
        /// </summary>
        public Int32 IdFrecuencia
        {
            get { return idfrecuencia; }
            set { idfrecuencia = value; }
        }

        /// <summary>
        /// Nombre de frecuencia.
        /// </summary>
        public String Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        /// <summary>
        /// Código de producto.
        /// </summary>
        public Int32 IdProducto
        {
            get { return idproducto; }
            set { idproducto = value; }
        }

        /// <summary>
        /// Nombre de opcion o plan.
        /// </summary>
        public String Opcion
        {
            get { return opcion; }
            set { opcion = value; }
        }
        #endregion

        #region Campos Consulta
        private Int32 idplanes;
        #endregion

        #region Propiedades Consulta
        public Int32 IdPlanes
        {
            get { return idplanes; }
            set { this.idplanes = value; }
        }
        #endregion
    }
}
