﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity.Bais
{
    public class BEOficina
    {
        #region Campos
        private Int32 idoficina;
        private Int32 codoficina;
        private Int32 idciudad;
        private String nombre;
        private String idgerente;
        private String gerente;
        private String idregional;
        private String tipo;
        private String ur;
        private String ccgerentezona;
        private String gerentezona;
        private Int32 informadores;
        private String centroinformacion;
        private String zona;
        private Int32 iddivision;
        private Boolean estado;
        #endregion

        #region Propiedades
        /// <summary>
        /// Código de oficina.
        /// </summary>
        public Int32 IdOficina
        {
            get { return idoficina; }
            set { idoficina = value; }
        }

        /// <summary>
        /// Código externo de oficina.
        /// </summary>
        public Int32 CodOficina
        {
            get { return codoficina; }
            set { codoficina = value; }
        }

        /// <summary>
        /// Código de ubigeo.
        /// </summary>
        public Int32 idCiudad
        {
            get { return idciudad; }
            set { idciudad = value; }
        }

        /// <summary>
        /// Nombre de oficina.
        /// </summary>
        public String Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        /// <summary>
        /// Código de gerente de oficina.
        /// </summary>
        public String IdGerente
        {
            get { return idgerente; }
            set { idgerente = value; }
        }

        /// <summary>
        /// Nombre gerente oficina.
        /// </summary>
        public String Gerente
        {
            get { return gerente; }
            set { gerente = value; }
        }

        /// <summary>
        /// Código de regional.
        /// </summary>
        public String IdRegional
        {
            get { return idregional; }
            set { idregional = value; }
        }

        /// <summary>
        /// Tipo de oficina.
        /// </summary>
        public String Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Ur
        {
            get { return ur; }
            set { ur = value; }
        }

        /// <summary>
        /// Nro documento identidad de gerente de oficina.
        /// </summary>
        public String CcGerenteZona
        {
            get { return ccgerentezona; }
            set { ccgerentezona = value; }

        }

        /// <summary>
        /// Gerente de zona.
        /// </summary>
        public String GerenteZona
        {
            get { return gerentezona; }
            set { gerentezona = value; }
        }

        /// <summary>
        /// Nro. de informadores.
        /// </summary>
        public Int32 Informadores
        {
            get { return informadores; }
            set { informadores = value; }
        }

        /// <summary>
        /// Centro de información.
        /// </summary>
        public String CentroInformacion
        {
            get { return centroinformacion; }
            set { centroinformacion = value; }
        }

        /// <summary>
        /// Zona de oficina.
        /// </summary>
        public String Zona
        {
            get { return zona; }
            set { zona = value; }
        }

        /// <summary>
        /// Código de division.
        /// </summary>
        public Int32 IdDivision
        {
            get { return iddivision; }
            set { iddivision = value; }
        }

        /// <summary>
        /// Estado de oficina.
        /// </summary>
        public Boolean Estado
        {
            get { return estado; }
            set { estado = value; }
        }
        #endregion          
    }
}
