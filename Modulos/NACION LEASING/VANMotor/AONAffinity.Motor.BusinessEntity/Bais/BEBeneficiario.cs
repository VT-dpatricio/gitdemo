﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity.Bais
{
    public class BEBeneficiario
    {
        #region Campos
        private Int32 idbeneficiario;
        private String idcertificado;
        private Int16 consecutivo;
        private String ccbenef;
        private String idtipodocumento;
        private Decimal porcentaje;
        private String nombre1;
        private String nombre2;
        private String apellido1;
        private String apellido2;
        private Int16 idparentesco;
        private Boolean activo;
        private String domicilio;
        private DateTime fechanacimiento;
        private String ocupacion;
        private String telefono;
        private String sexo;
        #endregion

        #region Propiedades
        public Int32 IdBeneficiario
        {
            get { return this.idbeneficiario; }
            set { this.idbeneficiario = value; }
        }

        public String IdCertificado
        {
            get { return this.idcertificado; }
            set { this.idcertificado = value; }
        }

        public Int16 Consecutivo
        {
            get { return this.consecutivo; }
            set { this.consecutivo = value; }
        }

        public String CcBenef
        {
            get { return this.ccbenef; }
            set { this.ccbenef = value; }
        }

        public String IdTipoDocumento
        {
            get { return this.idtipodocumento; }
            set { this.idtipodocumento = value; }
        }

        public Decimal Porcentaje
        {
            get { return this.porcentaje; }
            set { this.porcentaje = value; }
        }

        public String Nombre1
        {
            get { return this.nombre1; }
            set { this.nombre1 = value; }
        }

        public String Nombre2
        {
            get { return this.nombre2; }
            set { this.nombre2 = value; }
        }

        public String Apellido1
        {
            get { return this.apellido1; }
            set { this.apellido1 = value; }
        }

        public String Apellido2
        {
            get { return this.apellido2; }
            set { this.apellido2 = value; }
        }

        public Int16 IdParentesco
        {
            get { return this.idparentesco; }
            set { this.idparentesco = value; }
        }

        public Boolean Activo
        {
            get { return this.activo; }
            set { this.activo = value; }
        }

        public String Domicilio
        {
            get { return this.domicilio; }
            set { this.domicilio = value; }
        }

        public DateTime FechaNacimiento
        {
            get { return this.fechanacimiento; }
            set { this.fechanacimiento = value; }
        }

        public String Ocupacion
        {
            get { return this.ocupacion; }
            set { this.ocupacion = value; }
        }

        public String Telefono
        {
            get { return this.telefono; }
            set { this.telefono = value; }
        }

        public String Sexo
        {
            get { return this.sexo; }
            set { this.sexo = value; }
        }
        #endregion

        #region Campos Consulta
        private String nombredocumento;
        private String descparentesco;
        private String nombres;
        private String apellidos;
        private String nombrecompleto;
        private String nombreestado;
        #endregion

        #region Propiedades Consulta
        public String NombreDocumento
        {
            get { return this.nombredocumento; }
            set { this.nombredocumento = value; }
        }

        public String Nombres
        {
            get { return this.Nombre1 + " " + this.Nombre2; }
            set { this.nombres = value; }
        }

        public String Apellidos
        {
            get { return this.Apellido1 + " " + this.Apellido2; }
            set { this.apellidos = value; }
        }

        public String NombreCompleto
        {
            get { return Apellidos + " " + Nombres; }
            set { nombrecompleto = value; }
        }

        public String NombreEstado
        {
            get { return this.nombreestado; }
            set { this.nombreestado = value; }
        }

        public String DescParentesco
        {
            get { return this.descparentesco; }
            set { this.descparentesco = value; }
        }
        #endregion
    }
}
