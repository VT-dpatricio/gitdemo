﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity.Bais
{
    public class BEInfoProductoC
    {
        #region Campos
        private String idcertificado;
        private Int32 idinfoproducto;
        private Decimal valornum;
        private DateTime valordate;
        private String valorstring;
        private String nombre;
        #endregion

        #region Propiedades
        /// <summary>
        /// Código de certificado.
        /// </summary>
        public String IdCertificado
        {
            get { return this.idcertificado; }
            set { this.idcertificado = value; }
        }

        /// <summary>
        /// Código de infoproducto.
        /// </summary>
        public Int32 IdInfoProducto
        {
            get { return this.idinfoproducto; }
            set { this.idinfoproducto = value; }
        }

        /// <summary>
        /// Valor numerico.
        /// </summary>
        public Decimal ValorNum
        {
            get { return this.valornum; }
            set { this.valornum = value; }
        }

        /// <summary>
        /// Valor fecha.
        /// </summary>
        public DateTime ValorDate
        {
            get { return this.valordate; }
            set { this.valordate = value; }
        }

        /// <summary>
        /// Valor cadena.
        /// </summary>
        public String ValorString
        {
            get { return this.valorstring; }
            set { this.valorstring = value; }
        }

        /// <summary>
        /// Nombre valor.
        /// </summary>
        public String Nombre
        {
            get { return this.nombre; }
            set { this.nombre = value; }
        }
        #endregion 
    }
}
