﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity.Bais
{
    public class BEInformador
    {
        #region Campos
        private String idinformador;
        private String cc;
        private String nombre;
        private Int32 idoficina;
        private Int32 idcargo;
        private Boolean activo;
        private Int32 idcanal;
        #endregion

        #region Propiedades
        public String IdInformador
        {
            get { return this.idinformador; }
            set { this.idinformador = value; }
        }

        public String CC
        {
            get { return this.cc; }
            set { this.cc = value; }
        }

        public String Nombre
        {
            get { return this.nombre; }
            set { this.nombre = value; }
        }

        public Int32 IdOficina
        {
            get { return this.idoficina; }
            set { this.idoficina = value; }
        }

        public Int32 IdCargo
        {
            get { return this.idcargo; }
            set { this.idcargo = value; }
        }

        public Boolean Activo
        {
            get { return this.activo; }
            set { this.activo = value; }
        }

        public Int32 IdCanal
        {
            get { return this.idcanal; }
            set { this.idcanal = value; }
        }
        #endregion

        #region Campos Consulta
        private String nomoficina;
        #endregion

        #region Propiedades Consulta
        public String NomOficina
        {
            get { return this.nomoficina; }
            set { this.nomoficina = value; }
        }
        #endregion
    }
}
