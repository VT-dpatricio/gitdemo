﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity.Bais
{
    public class BEMoneda
    {
        #region Campos
        private String idmoneda;
        private String nombre;
        private String simbolo;
        #endregion

        #region Propiedades
        /// <summary>
        /// Código de moneda.
        /// </summary>
        public String IdMoneda
        {
            get { return idmoneda; }
            set { idmoneda = value; }
        }

        /// <summary>
        /// Nombre de moneda.
        /// </summary>
        public String Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        /// <summary>
        /// Simbolo de moneda.
        /// </summary>
        public String Simbolo
        {
            get { return simbolo; }
            set { simbolo = value; }
        }
        #endregion
    }
}
