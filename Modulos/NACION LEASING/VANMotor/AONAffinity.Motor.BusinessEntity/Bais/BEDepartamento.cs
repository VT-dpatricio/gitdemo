﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity.Bais
{
    public class BEDepartamento
    {
        #region Campos
        private Int32 iddepartamento;
        private Int32 idpais;
        private String nombre;
        #endregion

        #region Propiedades
        /// <summary>
        /// Código de departamento.
        /// </summary>
        public Int32 IdDepartamento
        {
            get { return iddepartamento; }
            set { iddepartamento = value; }
        }

        /// <summary>
        /// Código Pais.
        /// </summary>
        public Int32 IdPais
        {
            get { return idpais; }
            set { idpais = value; }
        }

        /// <summary>
        /// Nombre del departamento.
        /// </summary>
        public String Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        #endregion
    }
}
