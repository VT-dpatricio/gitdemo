﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity.Bais
{
    public class BEInfoAseguradoC
    {
        #region Atributos
        private String idcertificado;
        private Int32 idinfoasegurado;
        private Int16 consecutivo;
        private Decimal valornum;
        private DateTime valordate;
        private String valorstring;
        #endregion

        #region Propiedades
        public String IdCertificado
        {
            get { return idcertificado; }
            set { idcertificado = value; }
        }

        public Int32 IdInfoAsegurado
        {
            get { return idinfoasegurado; }
            set { idinfoasegurado = value; }
        }

        public Int16 Consecutivo
        {
            get { return consecutivo; }
            set { consecutivo = value; }
        }

        public Decimal ValorNum
        {
            get { return valornum; }
            set { valornum = value; }
        }

        public DateTime ValorDate
        {
            get { return valordate; }
            set { valordate = value; }
        }

        public String ValorString
        {
            get { return valorstring; }
            set { valorstring = value; }
        }
        #endregion

        #region Campos Consulta
        private String descripcion;
        private String nombre;
        #endregion

        #region Propiedades
        /// <summary>
        /// Descripció de infoAsegurado.
        /// </summary>
        public String Descripcion
        {
            get { return this.descripcion; }
            set { this.descripcion = value; }
        }

        /// <summary>
        /// Nombre de infoasegurado.
        /// </summary>
        public String Nombre
        {
            get { return this.nombre; }
            set { this.nombre = value; }
        }
        #endregion
    }
}
