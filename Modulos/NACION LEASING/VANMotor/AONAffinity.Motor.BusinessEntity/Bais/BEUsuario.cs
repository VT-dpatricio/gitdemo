﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity.Bais
{
    public class BEUsuario
    {
        #region Campos
        private String idusuario;
        private String apellido1;
        private String apellido2;
        private String nombre1;
        private String nombre2;
        private String idinformador;
        #endregion

        #region Propiedades
        public String IdUsuario
        {
            get { return this.idusuario; }
            set { this.idusuario = value; }
        }

        public String Apellido1
        {
            get { return this.apellido1; }
            set { this.apellido1 = value; }
        }

        public String Apellido2
        {
            get { return this.apellido2; }
            set { this.apellido2 = value; }
        }

        public String Nombre1
        {
            get { return this.nombre1; }
            set { this.nombre1 = value; }
        }

        public String Nombre2
        {
            get { return this.nombre2; }
            set { this.nombre2 = value; }
        }

        public String IdInformador
        {
            get { return this.idinformador; }
            set { this.idinformador = value; }
        }
        #endregion
    }
}
