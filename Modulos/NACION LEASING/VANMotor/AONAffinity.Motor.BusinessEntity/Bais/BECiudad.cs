﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity.Bais
{
    /// <summary>
    /// Clase entidad que define la tabla Ciudad.
    /// </summary>
    [Serializable]
    public class BECiudad
    {
        #region Campos
        private Int32 idciudad;
        private Int32 iddepartamento;
        private String nombre;
        private String provincia;
        #endregion

        #region Propiedades
        /// <summary>
        /// Código de ciudad.
        /// </summary>
        public Int32 IdCiudad
        {
            get { return idciudad; }
            set { idciudad = value; }
        }

        /// <summary>
        /// Código de departamento;
        /// </summary>
        public Int32 IdDepartamento
        {
            get { return iddepartamento; }
            set { iddepartamento = value; }
        }

        /// <summary>
        ///  Nombre de ciudad.
        /// </summary>
        public String Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        /// <summary>
        /// Nombre de provincia.
        /// </summary>
        public String Provincia
        {
            get { return provincia; }
            set { provincia = value; }
        }
        #endregion

        #region Campos Consulta
        private String idprovincia;
        #endregion

        #region Propiedades Consulta
        /// <summary>
        /// Código de provincia.
        /// </summary>
        public String IdProvincia
        {
            get { return idprovincia; }
            set { idprovincia = value; }
        }
        #endregion
    }
}
