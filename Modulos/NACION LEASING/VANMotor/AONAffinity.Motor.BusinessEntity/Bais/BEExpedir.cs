﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity.Bais
{
    public class BEExpedir
    {
        #region Campos
        private Int32 codigo;
        private String descripcion;
        #endregion

        #region Propiedades
        public Int32 Codigo
        {
            get { return codigo; }
            set { codigo = value; }
        }

        public String Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }
        #endregion
    }
}
