﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AONAffinity.Library.Resources;

namespace AONAffinity.Motor.BusinessEntity.Bais
{
    public class BECertificado
    {
        #region Constructor
        /// <summary>
        /// Inicializa los campos nulos con sus valores por defecto.
        /// </summary>
        public BECertificado()
        {
            this.primacobrar = NullTypes.DecimalNull;
            this.vencimiento = NullTypes.FechaNull;
            this.vigencia = NullTypes.FechaNull;
            this.puntos = NullTypes.IntegerNull;
            this.idmotivoanulacion = NullTypes.IntegerNull;
            this.nombre2 = NullTypes.CadenaNull;
            this.apellido2 = NullTypes.CadenaNull;
            this.incentivo = NullTypes.IntegerNull;
            this.saldoincentivo = NullTypes.IntegerNull;
            this.usuariomodificacion = NullTypes.CadenaNull;
            this.fechamodificacion = NullTypes.FechaNull;
            this.solicitudanulacion = NullTypes.FechaNull;
            this.digitacionanulacion = NullTypes.FechaNull;
            this.efectuaranulacion = NullTypes.FechaNull;
            this.finvigencia = NullTypes.FechaNull;
            this.afiliadohasta = NullTypes.FechaNull;
            this.idciclo = NullTypes.IntegerNull;
            this.diagenera = NullTypes.ShortNull;
            this.mesgenera = NullTypes.ShortNull;
        }
        #endregion

        #region Campos
        private String idcertificado;
        private Int32 idproducto;
        private Decimal numcertificado;
        private String opcion;
        private Int32 idfrecuencia;
        private Int32 idoficina;
        private String idinformador;
        private String idmediopago;
        private Int32 idestadocertificado;
        private Decimal montoasegurado;
        private Decimal primabruta;
        private Decimal iva;
        private Decimal primatotal;
        private Decimal primacobrar;
        private String numerocuenta;
        private DateTime vencimiento;
        private DateTime vigencia;
        private Int32 puntos;
        private Int32 idmotivoanulacion;
        private Boolean consistente;
        private Int32 puntosanual;
        private Int32 idciudad;
        private String direccion;
        private String telefono;
        private String nombre1;
        private String nombre2;
        private String apellido1;
        private String apellido2;
        private String cccliente;
        private String idtipodocumento;
        private DateTime digitacion;
        private Decimal incentivo;
        private Decimal saldoincentivo;
        private String usuariocreacion;
        private String usuariomodificacion;
        private DateTime fechamodificacion;
        private DateTime solicitudanulacion;
        private DateTime digitacionanulacion;
        private DateTime efectuaranulacion;
        private DateTime venta;
        private DateTime finvigencia;
        private String idmonedaprima;
        private String idmonedacobro;
        private DateTime afiliadohasta;
        private Int32 idciclo;
        private Int16 diagenera;
        private Int16 mesgenera;
        #endregion

        #region Propiedades
        /// <summary>
        /// Codigo del Certificado.
        /// NOT NULL
        /// </summary>
        public String IdCertificado
        {
            get { return idcertificado; }
            set { idcertificado = value; }
        }

        /// <summary>
        /// Codigo del Producto.
        /// NOT NULL
        /// </summary>
        public Int32 IdProducto
        {
            get { return idproducto; }
            set { idproducto = value; }
        }

        /// <summary>
        /// Numero del Certificado.
        /// NOT NULL
        /// </summary>
        public Decimal NumCertificado
        {
            get { return numcertificado; }
            set { numcertificado = value; }
        }

        /// <summary>
        /// Plan del Certificado.
        /// NOT NULL
        /// </summary>
        public String Opcion
        {
            get { return opcion; }
            set { opcion = value; }
        }

        /// <summary>
        /// Frecuencia de Pago.
        /// NOT NULL
        /// </summary>
        public Int32 IdFrecuencia
        {
            get { return idfrecuencia; }
            set { idfrecuencia = value; }
        }

        /// <summary>
        /// Codigo de Oficina.
        /// NOT NULL
        /// </summary>
        public Int32 IdOficina
        {
            get { return idoficina; }
            set { idoficina = value; }
        }

        /// <summary>
        /// Codigo de Vendedor.
        /// NOT NULL
        /// </summary>
        public String IdInformador
        {
            get { return idinformador; }
            set { idinformador = value; }
        }

        /// <summary>
        /// Codigo de Medio de Pago.
        /// NOT NULL
        /// </summary>
        public String IdMedioPago
        {
            get { return idmediopago; }
            set { idmediopago = value; }
        }

        /// <summary>
        /// Estado de Certificado.
        /// NOT NULL
        /// </summary>
        public Int32 IdEstadoCertificado
        {
            get { return idestadocertificado; }
            set { idestadocertificado = value; }
        }

        /// <summary>
        /// Monto Asegurado.
        /// NOT NULL
        /// </summary>
        public Decimal MontoAsegurado
        {
            get { return montoasegurado; }
            set { montoasegurado = value; }
        }

        /// <summary>
        /// Prima Bruta.
        /// NOT NULL
        /// </summary>
        public Decimal PrimaBruta
        {
            get { return primabruta; }
            set { primabruta = value; }
        }

        /// <summary>
        /// IVA.
        /// NOT NULL
        /// </summary>
        public Decimal Iva
        {
            get { return iva; }
            set { iva = value; }
        }

        /// <summary>
        /// Prima Total.
        /// NOT NULL
        /// </summary>
        public Decimal PrimaTotal
        {
            get { return primatotal; }
            set { primatotal = value; }
        }

        /// <summary>
        /// Prima a Cobrar.
        /// Si es NULL, enviar -1
        /// Obligatorio para BDJanus
        /// </summary>
        public Decimal PrimaCobrar
        {
            get { return primacobrar; }
            set { primacobrar = value; }
        }

        /// <summary>
        /// Numero de Cuenta/Tarjeta de Cargo.
        /// NOT NULL
        /// </summary>
        public String NumeroCuenta
        {
            get { return numerocuenta; }
            set { numerocuenta = value; }
        }

        /// <summary>
        /// Fecha de Vencimiento de la Tarjeta.
        /// Si es NULL, enviar 1900-01-01
        /// </summary>
        public DateTime Vencimiento
        {
            get { return vencimiento; }
            set { vencimiento = value; }
        }

        /// <summary>
        /// Inicio de Vigencia del Certificado.
        /// Si es NULL, enviar 1900-01-01
        /// </summary>
        public DateTime Vigencia
        {
            get { return vigencia; }
            set { vigencia = value; }
        }

        /// <summary>
        /// Puntos.
        /// Si es NULL, enviar -1
        /// </summary>
        public Int32 Puntos
        {
            get { return puntos; }
            set { puntos = value; }
        }

        /// <summary>
        /// Motivo de Anulacion.
        /// Si es NULL, enviar -1
        /// </summary>
        public Int32 IdMotivoAnulacion
        {
            get { return idmotivoanulacion; }
            set { idmotivoanulacion = value; }
        }

        /// <summary>
        /// Consistente.
        /// NOT NULL
        /// </summary>
        public Boolean Consistente
        {
            get { return consistente; }
            set { consistente = value; }
        }

        /// <summary>
        /// Puntos Anuales.
        /// NOT NULL
        /// </summary>
        public Int32 PuntosAnual
        {
            get { return puntosanual; }
            set { puntosanual = value; }
        }

        /// <summary>
        /// Codigo de Ciudad.
        /// NOT NULL
        /// </summary>
        public Int32 IdCiudad
        {
            get { return idciudad; }
            set { idciudad = value; }
        }

        /// <summary>
        /// Domicilio del Contratante.
        /// NOT NULL
        /// </summary>
        public String Direccion
        {
            get { return direccion; }
            set { direccion = value; }
        }

        /// <summary>
        /// Numero de Telefono del Contratante.
        /// NOT NULL
        /// </summary>
        public String Telefono
        {
            get { return telefono; }
            set { telefono = value; }
        }

        /// <summary>
        /// Primer Nombre del Contratante.
        /// NOT NULL
        /// </summary>
        public String Nombre1
        {
            get { return nombre1; }
            set { nombre1 = value; }
        }

        /// <summary>
        /// Segundo Nombre del Contratante.
        /// Si es NULL, Enviar "" o String.Empty
        /// </summary>
        public String Nombre2
        {
            get { return nombre2; }
            set { nombre2 = value; }
        }

        /// <summary>
        /// Apellido Paterno del Contratante.
        /// NOT NULL
        /// </summary>
        public String Apellido1
        {
            get { return apellido1; }
            set { apellido1 = value; }
        }

        /// <summary>
        /// Apellido Materno del Contratante.
        /// Si es NULL, Enviar "" o String.Empty
        /// </summary>
        public String Apellido2
        {
            get { return apellido2; }
            set { apellido2 = value; }
        }

        /// <summary>
        /// Numero de documento del Cliente.
        /// NOT NULL
        /// </summary>
        public String CcCliente
        {
            get { return cccliente; }
            set { cccliente = value; }
        }

        /// <summary>
        /// Codigo Tipo de Documento.
        /// NOT NULL
        /// </summary>
        public String IdTipoDocumento
        {
            get { return idtipodocumento; }
            set { idtipodocumento = value; }
        }

        /// <summary>
        /// Fecha de digitacion del certificado.
        /// NOT NULL
        /// </summary>
        public DateTime Digitacion
        {
            get { return digitacion; }
            set { digitacion = value; }
        }

        /// <summary>
        /// Incentivo.
        /// Si es NULL, Enviar -1
        /// </summary>
        public Decimal Incentivo
        {
            get { return incentivo; }
            set { incentivo = value; }
        }

        /// <summary>
        /// Saldo Incentivo.
        /// Si es NULL, Enviar -1
        /// </summary>
        public Decimal SaldoIncentivo
        {
            get { return saldoincentivo; }
            set { saldoincentivo = value; }
        }

        /// <summary>
        /// Usuario Digitador del Certificado.
        /// NOT NULL
        /// </summary>
        public String UsuarioCreacion
        {
            get { return usuariocreacion; }
            set { usuariocreacion = value; }
        }

        /// <summary>
        /// Usuario que Modifica el Certificado.
        /// Si es NULL, Enviar "" o String.Empty
        /// </summary>
        public String UsuarioModificacion
        {
            get { return usuariomodificacion; }
            set { usuariomodificacion = value; }
        }

        /// <summary>
        /// Fecha de Modificacion del Certificado.
        /// Si es NULL, Enviar 1900-01-01
        /// </summary>
        public DateTime FechaModificacion
        {
            get { return fechamodificacion; }
            set { fechamodificacion = value; }
        }

        /// <summary>
        /// Fecha en la que se solicitó la Anulacion.
        /// Si es NULL, Enviar 1900-01-01
        /// </summary>
        public DateTime SolicitudAnulacion
        {
            get { return solicitudanulacion; }
            set { solicitudanulacion = value; }
        }

        /// <summary>
        /// Fecha en la que se digita la Anulacion.
        /// Si es NULL, Enviar 1900-01-01
        /// </summary>
        public DateTime DigitacionAnulacion
        {
            get { return digitacionanulacion; }
            set { digitacionanulacion = value; }
        }

        /// <summary>
        /// Fecha en la que se efectua la Anulacion.
        /// Si es NULL, Enviar 1900-01-01
        /// </summary>
        public DateTime EfectuarAnulacion
        {
            get { return efectuaranulacion; }
            set { efectuaranulacion = value; }
        }

        /// <summary>
        /// Fecha de Venta del Certificado.
        /// NOT NULL
        /// </summary>
        public DateTime Venta
        {
            get { return venta; }
            set { venta = value; }
        }

        /// <summary>
        /// Fin de Vigencia del Certificado.
        /// Si es NULL, Enviar 1900-01-01
        /// </summary>
        public DateTime FinVigencia
        {
            get { return finvigencia; }
            set { finvigencia = value; }
        }

        /// <summary>
        /// Moneda de la Prima.
        /// NOT NULL.
        /// </summary>
        public String IdMonedaPrima
        {
            get { return idmonedaprima; }
            set { idmonedaprima = value; }
        }

        /// <summary>
        /// Moneda del Cobro.
        /// NOT NULL
        /// </summary>
        public String IdMonedaCobro
        {
            get { return idmonedacobro; }
            set { idmonedacobro = value; }
        }

        /// <summary>
        /// Fecha de Afiliacion.
        /// Si es NULL, Enviar 1900-01-01
        /// </summary>
        public DateTime AfiliadoHasta
        {
            get { return afiliadohasta; }
            set { afiliadohasta = value; }
        }

        /// <summary>
        /// Ciclo.
        /// Si es NULL, enviar -1.
        /// </summary>
        public Int32 IdCiclo
        {
            get { return idciclo; }
            set { idciclo = value; }
        }

        /// <summary>
        /// Dia Genera.
        /// Si es NULL, enviar -1.
        /// </summary>
        public Int16 DiaGenera
        {
            get { return diagenera; }
            set { diagenera = value; }
        }

        /// <summary>
        /// Mes Genera.
        /// Si es NULL, enviar -1.
        /// </summary>
        public Int16 MesGenera
        {
            get { return mesgenera; }
            set { mesgenera = value; }
        }
        #endregion

        #region Campos Consulta
        private String desproducto;
        private String nominformador;
        private Int32 codoficina;
        private String nomoficina;
        private String desmediopago;
        private String desfrecuencia;
        private String destipodocumento;
        private String nomciudad;
        private String nomprovincia;
        private String nomdepartamento;
        private String desestadocertificado;
        private String desmotivoanulacion;
        private String simbolomoneda;
        private String desmonedaprima;
        private String desmonedacobro;
        private DateTime fecnacimiento;
        private String sexo;
        private String nomapeconyugue;
        private String sexoconyugue;
        private DateTime fecnacimientoconyugue;
        private Decimal numpoliza;
        private String numpedido;
        private String nombrecliente;
        private String desoficina;
        private String desasegurador;
        //private String nropoliza;
        #endregion

        #region Propiedades Consulta
        /// <summary>
        /// Descripción del producto.
        /// </summary>
        public String DesProducto
        {
            get { return desproducto; }
            set { desproducto = value; }
        }

        /// <summary>
        /// Nombre del informador.
        /// </summary>
        public String NomInformador
        {
            get { return nominformador; }
            set { nominformador = value; }
        }

        /// <summary>
        /// Código de oficina.
        /// </summary>
        public Int32 CodOficina
        {
            get { return codoficina; }
            set { codoficina = value; }
        }

        /// <summary>
        /// Nombre de oficina.
        /// </summary>
        public String NomOficina
        {
            get { return nomoficina; }
            set { nomoficina = value; }
        }

        /// <summary>
        /// Descripción del médio de pago.
        /// </summary>
        public String DesMedioPago
        {
            get { return desmediopago; }
            set { desmediopago = value; }
        }

        /// <summary>
        /// Descripción de frecuencia.
        /// </summary>
        public String DesFrecuencia
        {
            get { return desfrecuencia; }
            set { desfrecuencia = value; }
        }

        /// <summary>
        /// Descripción del tipo de documento.
        /// </summary>
        public String DesTipoDocumento
        {
            get { return destipodocumento; }
            set { destipodocumento = value; }
        }

        /// <summary>
        /// Nombre de ciudad.
        /// </summary>
        public String NomCiudad
        {
            get { return nomciudad; }
            set { nomciudad = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String NomProvincia
        {
            get { return this.nomprovincia; }
            set { this.nomprovincia = value; }
        }

        /// <summary>
        /// Nombre de departamento.
        /// </summary>
        public String NomDepartamento
        {
            get { return nomdepartamento; }
            set { nomdepartamento = value; }
        }

        /// <summary>
        /// Descripción del estado del certificado.
        /// </summary>
        public String DesEstadoCertificado
        {
            get { return desestadocertificado; }
            set { desestadocertificado = value; }
        }

        /// <summary>
        /// Descripción del motivo de anulación.
        /// </summary>
        public String DesMotivoAnulacion
        {
            get { return desmotivoanulacion; }
            set { desmotivoanulacion = value; }
        }

        /// <summary>
        /// Simbolo de moneda.
        /// </summary>
        public String SimboloMoneda
        {
            get { return simbolomoneda; }
            set { simbolomoneda = value; }
        }

        /// <summary>
        /// Descripción de moneda prima.
        /// </summary>
        public String DesMonedaPrima
        {
            get { return desmonedaprima; }
            set { desmonedaprima = value; }
        }

        /// <summary>
        /// Descripción de moneda de cobro.
        /// </summary>
        public String DesMonedaCobro
        {
            get { return desmonedacobro; }
            set { desmonedacobro = value; }
        }

        public DateTime FecNacimiento
        {
            get { return this.fecnacimiento; }
            set { this.fecnacimiento = value; }
        }

        public String Sexo
        {
            get { return this.sexo; }
            set { this.sexo = value; }
        }

        public String NomApeConyugue
        {
            get { return this.nomapeconyugue; }
            set { this.nomapeconyugue = value; }
        }

        public String SexoConyugue
        {
            get { return this.sexoconyugue; }
            set { this.sexoconyugue = value; }
        }

        public DateTime FecNacimientoConyugue
        {
            get { return this.fecnacimientoconyugue; }
            set { this.fecnacimientoconyugue = value; }
        }

        public Decimal NumPoliza
        {
            get { return this.numpoliza; }
            set { this.numpoliza = value; }
        }

        public String NumPedido
        {
            get { return this.numpedido; }
            set { this.numpedido = value; }
        }

        public String NombreCliente
        {
            get { return this.nombrecliente; }
            set { this.nombrecliente = value; }
        }

        public String DesOficina
        {
            get { return this.desoficina; }
            set { this.desoficina = value; }
        }

        public String DesAsegurador
        {
            get { return this.desasegurador; }
            set { this.desasegurador = value; }
        }

        //public String NroPoliza
        //{
        //    get { return this.nropoliza; }
        //    set { this.nropoliza = value; }
        //}

        public String NombreApellidos 
        {
            get { return this.nombre1 + " " + this.nombre2 + " " + this.apellido1 + " " + this.apellido2; }
        }
        #endregion
    }
}
