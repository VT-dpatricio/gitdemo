﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity.Bais
{
    public class BEOpcionPoliza : BEAuditoria
    {
        #region Campos
        private Int32 idproducto;
	    private String opcion;
	    private Int32 idfrecuencia;
	    private String idmonedaprima;
	    private String idtipodocumento;
	    private String nropoliza;	    
        #endregion

        #region Propiedades
        public Int32 IdProducto 
        {
            get { return this.idproducto; }
            set { this.idproducto = value; }
        }

        public String Opcion
        {
            get { return this.opcion; }
            set { this.opcion = value; }
        }

        public Int32 IdFrecuencia
        {
            get { return this.idfrecuencia; }
            set { this.idfrecuencia = value; }
        }

        public String IdMonedaPrima
        {
            get { return this.idmonedaprima; }
            set { this.idmonedaprima = value; }
        }

        public String IdTipoDocumento
        {
            get { return this.idtipodocumento; }
            set { this.idtipodocumento = value; }
        }

        public String NroPoliza 
        {
            get { return this.nropoliza; }
            set { this.nropoliza = value; }
        }        
        #endregion
    }
}
