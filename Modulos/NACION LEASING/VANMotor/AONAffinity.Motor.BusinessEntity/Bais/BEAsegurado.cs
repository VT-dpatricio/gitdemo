﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity.Bais
{
    public class BEAsegurado
    {
        #region Constructor
        /// <summary>
        /// Inicializa los campos nulos con sus valores por defecto.
        /// </summary>
        public BEAsegurado()
        {
            nombre2 = String.Empty;
            apellido2 = String.Empty;
            direccion = String.Empty;
            telefono = String.Empty;
            idciudad = -1;
            fechanacimiento = DateTime.Parse("1900-01-01");
            idparentesco = -1;
        }
        #endregion

        #region Campos
        private String idcertificado;
        private Int16 consecutivo;
        private String idtipodocumento;
        private String ccaseg;
        private String nombre1;
        private String nombre2;
        private String apellido1;
        private String apellido2;
        private String direccion;
        private String telefono;
        private Int32 idciudad;
        private DateTime fechanacimiento;
        private Int16 idparentesco;
        private Boolean activo;
        private String nombres;
        private String apellidos;
        private String nombrecompleto;
        private String nombreestado;
        #endregion

        #region CamposConsulta
        private String nombretipodocumento;
        private Decimal primaasegurado;
        private String planasegurado;
        private String desdistrito;
        private String desprovincia;
        private String desdepartamento;
        private String ocupacion;
        private String celular;
        private String estadocivil;
        private String sexo;
        private String email;
        #endregion

        #region Campos Adicionales
        private Int32 idfrecuencia;
        private String idmonedaprima;
        private String opcion;
        private Decimal montoasegurado;
        #endregion

        #region PropiedadesConsulta
        public String Email
        {
            get { return this.email; }
            set { this.email = value; }
        }

        public String Sexo
        {
            get { return this.sexo; }
            set { this.sexo = value; }
        }

        public String Ocupacion
        {
            get { return this.ocupacion; }
            set { this.ocupacion = value; }
        }

        public String Celular
        {
            get { return this.celular; }
            set { this.celular = value; }
        }

        public String EstadoCivil
        {
            get { return this.estadocivil; }
            set { this.estadocivil = value; }
        }

        public String DesDistrito
        {
            get { return this.desdistrito; }
            set { this.desdistrito = value; }
        }

        public String DesProvincia
        {
            get { return this.desprovincia; }
            set { this.desprovincia = value; }
        }

        public String DesDepartamento
        {
            get { return this.desdepartamento; }
            set { this.desdepartamento = value; }
        }

        /// <summary>
        /// Nombre del tipo de documento
        /// </summary>
        public String NombreTipoDocumento
        {
            get { return nombretipodocumento; }
            set { nombretipodocumento = value; }
        }

        /// <summary>
        /// Prima del Asegurado
        /// </summary>
        public Decimal PrimaAsegurado
        {
            get { return primaasegurado; }
            set { primaasegurado = value; }
        }

        /// <summary>
        /// Plan del Asegurado
        /// </summary>
        public String PlanAsegurado
        {
            get { return planasegurado; }
            set { planasegurado = value; }
        }

        /// <summary>
        /// Nombres Concatenados del Asegurado
        /// </summary>
        public String Nombres
        {
            get { return Nombre1 + " " + Nombre2; }
            set { nombres = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Apellidos
        {
            get { return Apellido1 + " " + Apellido2; }
            set { apellidos = value; }
        }
        public String NombreCompleto
        {
            get { return Apellidos + " " + Nombres; }
            set { nombrecompleto = value; }
        }
        public String NombreEstado
        {
            get { return nombreestado; }
            set { nombreestado = value; }
        }
        #endregion

        #region Propiedades
        public Boolean Activo
        {
            get { return activo; }
            set { activo = value; }
        }

        public String IdCertificado
        {
            get { return idcertificado; }
            set { idcertificado = value; }
        }

        public Int16 Consecutivo
        {
            get { return consecutivo; }
            set { consecutivo = value; }
        }

        public String IdTipodocumento
        {
            get { return idtipodocumento; }
            set { idtipodocumento = value; }
        }

        public String Ccaseg
        {
            get { return ccaseg; }
            set { ccaseg = value; }
        }

        public String Nombre1
        {
            get { return nombre1; }
            set { nombre1 = value; }
        }

        public String Nombre2
        {
            get { return nombre2; }
            set { nombre2 = value; }
        }

        public String Apellido1
        {
            get { return apellido1; }
            set { apellido1 = value; }
        }

        public String Apellido2
        {
            get { return apellido2; }
            set { apellido2 = value; }
        }

        public String Direccion
        {
            get { return direccion; }
            set { direccion = value; }
        }

        public String Telefono
        {
            get { return telefono; }
            set { telefono = value; }
        }

        public Int32 IdCiudad
        {
            get { return idciudad; }
            set { idciudad = value; }
        }

        public DateTime FechaNacimiento
        {
            get { return fechanacimiento; }
            set { fechanacimiento = value; }
        }

        public Int16 IdParentesco
        {
            get { return idparentesco; }
            set { idparentesco = value; }
        }

        #endregion

        #region Propiedades Adicionales
        public Int32 IdFrecuencia
        {
            get { return idfrecuencia; }
            set { idfrecuencia = value; }
        }

        public String IdMonedaPrima
        {
            get { return idmonedaprima; }
            set { idmonedaprima = value; }
        }

        public String Opcion
        {
            get { return opcion; }
            set { opcion = value; }
        }

        public Decimal MontoAsegurado
        {
            get { return montoasegurado; }
            set { montoasegurado = value; }
        }
        #endregion
    }
}
