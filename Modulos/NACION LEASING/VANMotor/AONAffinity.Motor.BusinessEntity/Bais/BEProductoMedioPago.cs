﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity.Bais
{
    public class BEProductoMedioPago
    {
        #region Campos
        private Int32 idproducto;
        private String idmediopago;
        private String idmonedacobro;
        private String codigocomercio;
        private String cuentarecaudadora;
        private String codigoarchivo;
        private String codigointerno;
        private String monedacuenta;
        private String servicio;
        private String origensolicitud;
        #endregion

        #region Propiedades
        /// <summary>
        /// Código de producto.
        /// </summary>
        public Int32 IdProducto
        {
            get { return idproducto; }
            set { idproducto = value; }
        }

        /// <summary>
        /// Código de medio de pago.
        /// </summary>
        public String IdMedioPago
        {
            get { return idmediopago; }
            set { idmediopago = value; }
        }

        /// <summary>
        /// Código de moneda de cobro.
        /// </summary>
        public String IdMonedaCobro
        {
            get { return idmonedacobro; }
            set { idmonedacobro = value; }
        }

        /// <summary>
        /// Código de comercio.
        /// </summary>
        public String CodigoComercio
        {
            get { return codigocomercio; }
            set { codigocomercio = value; }
        }

        /// <summary>
        /// Nro. cuenta recaudadora.
        /// </summary>
        public String CuentaRecaudadora
        {
            get { return cuentarecaudadora; }
            set { cuentarecaudadora = value; }
        }

        /// <summary>
        /// Código de archivo.
        /// </summary>
        public String CodigoArchivo
        {
            get { return codigoarchivo; }
            set { codigoarchivo = value; }
        }

        /// <summary>
        /// Código interno.
        /// </summary>
        public String CodigoInterno
        {
            get { return codigointerno; }
            set { codigointerno = value; }
        }

        /// <summary>
        /// Moneda de cuenta.
        /// </summary>
        public String MonedaCuenta
        {
            get { return monedacuenta; }
            set { monedacuenta = value; }
        }

        /// <summary>
        /// Descripción de servicio.
        /// </summary>
        public String Servicio
        {
            get { return servicio; }
            set { servicio = value; }
        }

        /// <summary>
        /// Origen de solicitud.
        /// </summary>
        public String OrigenSolicitud
        {
            get { return origensolicitud; }
            set { origensolicitud = value; }
        }
        #endregion

        #region Campos Consulta
        private String desmediopago;
        private String desMonedacobro;
        #endregion

        #region Propiedades Consulta
        /// <summary>
        /// Descripción del médio de pago.
        /// </summary>
        public String DesMedioPago
        {
            get { return desmediopago; }
            set { desmediopago = value; }
        }

        public String DesMonedacobro
        {
            get { return this.desMonedacobro; }
            set { this.desMonedacobro = value; }
        }
        #endregion
    }
}
