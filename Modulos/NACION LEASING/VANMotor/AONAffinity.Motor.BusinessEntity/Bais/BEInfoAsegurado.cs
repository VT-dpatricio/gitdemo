﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity.Bais
{
    [Serializable]
    public class BEInfoAsegurado
    {
        #region Atributos
        private Int32 idinfoasegurado;
        private Int32 idproducto;
        private String nombre;
        private String descripcion;
        private String idtipodato;
        private Boolean obligatorio;
        private Int32 minimo;
        private Int32 maximo;
        private Int16 numcaracteres;
        private String regex;
        private String formato;
        #endregion

        #region Propiedades
        public Int32 IdInfoAsegurado
        {
            get { return idinfoasegurado; }
            set { idinfoasegurado = value; }
        }

        public Int32 IdProducto
        {
            get { return idproducto; }
            set { idproducto = value; }
        }

        public String Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public String Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        public String IdTipoDato
        {
            get { return idtipodato; }
            set { idtipodato = value; }
        }

        public Boolean Obligatorio
        {
            get { return obligatorio; }
            set { obligatorio = value; }
        }

        public Int32 Minimo
        {
            get { return minimo; }
            set { minimo = value; }
        }

        public Int32 Maximo
        {
            get { return maximo; }
            set { maximo = value; }
        }

        public Int16 NumCaracteres
        {
            get { return numcaracteres; }
            set { numcaracteres = value; }
        }

        public String Regex
        {
            get { return regex; }
            set { regex = value; }
        }

        public String Formato
        {
            get { return formato; }
            set { formato = value; }
        }
        #endregion
    }
}
