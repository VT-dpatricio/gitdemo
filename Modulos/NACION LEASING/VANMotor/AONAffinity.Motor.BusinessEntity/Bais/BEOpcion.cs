﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity.Bais
{
    public class BEOpcion
    {
        #region Campos
        private Int32 idproducto;
        private String opcion;
        private Boolean activo;
        private Int32 puntos;
        private Decimal puntosvalor;
        #endregion

        #region Propiedades
        /// <summary>
        /// Código de producto.
        /// </summary>
        public Int32 IdProducto
        {
            get { return idproducto; }
            set { idproducto = value; }
        }

        /// <summary>
        /// Nombre de opción.
        /// </summary>
        public String Opcion
        {
            get { return opcion; }
            set { opcion = value; }
        }

        /// <summary>
        /// Estado activo de la opción.
        /// </summary>
        public Boolean Activo
        {
            get { return activo; }
            set { activo = value; }
        }

        /// <summary>
        /// Valor de puntos.
        /// </summary>
        public Int32 Puntos
        {
            get { return puntos; }
            set { puntos = value; }
        }

        /// <summary>
        /// Monto de puntos valor.
        /// </summary>
        public Decimal PuntosValor
        {
            get { return puntosvalor; }
            set { puntosvalor = value; }
        }
        #endregion
    }
}
