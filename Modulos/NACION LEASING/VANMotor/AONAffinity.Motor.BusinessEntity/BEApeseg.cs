﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AONAffinity.Library.Resources;

namespace AONAffinity.Motor.BusinessEntity
{
    /// <summary>
    /// 
    /// </summary>
    public class BEApeseg : BEAuditoria
    {
        #region Campos
        private Int32 idmodelo;
        private Int32 idanio;
        private Int32 idperiodo;
        private Decimal valorvehiculo;
        #endregion

        #region Propiedades
        public Int32 IdModelo 
        {
            get { return idmodelo; }
            set { idmodelo = value; }
        }

        public Int32 IdAnio 
        {
            get { return idanio; }
            set { idanio = value; }
        }

        public Int32 IdPeriodo 
        {
            get { return idperiodo; }
            set { idperiodo = value; }
        }

        public Decimal ValorVehiculo 
        {
            get { return valorvehiculo; }
            set { valorvehiculo = value; }
        }
        #endregion
    }
}
