﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AONAffinity.Library.Resources;

namespace AONAffinity.Motor.BusinessEntity
{
    /// <summary>
    /// Clase entidad que define la tabla Marca.
    /// </summary>
    public class BEMarca : BEAuditoria
    {
        #region Constructor
        /// <summary>
        /// Constructor de BEMarca.
        /// </summary>
        public BEMarca()
        {
            this.idmarca = 0;
            this.UsuarioModificacion = NullTypes.CadenaNull;    
            this.FechaModificacion = NullTypes.FechaNull;            
        }
        #endregion

        #region Campos
        private Int32 idmarca;
        private String descripcion;
        private String codexterno;
        #endregion

        #region Propiedades;
        /// <summary>
        /// Código de marca.
        /// </summary>
        public Int32 IdMarca 
        {
            get { return idmarca; }
            set { idmarca = value; }
        }

        /// <summary>
        /// Descripcion de marca.
        /// </summary>
        public String Descripcion 
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        public String CodExterno 
        {
            get { return this.codexterno; }
            set { this.codexterno = value; }
        }
        #endregion
    }
}
