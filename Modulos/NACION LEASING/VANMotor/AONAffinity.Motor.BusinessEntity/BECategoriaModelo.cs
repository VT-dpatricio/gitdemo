﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    public class BECategoriaModelo : BEAuditoria 
    {
        #region Campos
        private Int32 idcategoria;
        private Int32 idmodelo;
        private Int32 idproducto;
		private String codexterno;
        private String codsubmodelo;		   
        #endregion

        #region Propiedades
        public Int32 IdCategoria 
        {
            get { return this.idcategoria; }
            set { this.idcategoria = value; }
        }
        public Int32 IdModelo 
        {
            get { return this.idmodelo; }
            set { this.idmodelo = value; }
        }

        public Int32 IdIdproducto 
        {
            get { return this.idproducto; }
            set { this.idproducto = value; }
        }

        public String CodExterno 
        {
            get { return this.codexterno; }
            set { this.codexterno = value; }
        }

        public String CodSubModelo 
        {
            get { return this.codsubmodelo; }
            set { this.codsubmodelo = value; }
        }
        #endregion

        #region Campos Consulta
        private Boolean? timoncambiado;
        private String descmodelo;
        private String descsubmodelo;
        #endregion

        #region Propiedades Consulta
        public Boolean? TimonCambiado 
        {
            get { return this.timoncambiado; }
            set { this.timoncambiado = value; }
        }

        public String DescModelo 
        {
            get { return this.descmodelo; }
            set { this.descmodelo = value; }
        }

        public String DescSubModelo
        {
            get { return this.descsubmodelo; }
            set { this.descsubmodelo = value; }
        }
        #endregion
    }
}
