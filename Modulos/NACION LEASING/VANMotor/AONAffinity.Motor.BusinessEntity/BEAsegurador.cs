﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    [Serializable]
    public class BEAsegurador : BEAuditoria 
    {
        #region 
        private Int32 idasegurador;
        private String descripcion;
        private String rutalogo;
        #endregion


        #region Campos
        public Int32 IdAsegurador 
        {
            get { return this.idasegurador; }
            set { this.idasegurador = value; }
        }

        public String Descripcion
        {
            get { return this.descripcion; }
            set { this.descripcion = value; }
        }

        public String RutaLogo
        {
            get { return this.rutalogo; }
            set { this.rutalogo = value; }
        }
        #endregion
    }
}
