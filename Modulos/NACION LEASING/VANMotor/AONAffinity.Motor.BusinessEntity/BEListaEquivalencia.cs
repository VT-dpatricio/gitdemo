﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    public class BEListaEquivalencia: BEBase
    {
        private string _valorAON = string.Empty;
        private string _valorEquivalencia = string.Empty;

        private string _descripcion = string.Empty;
        private Int32 _idProducto = 0;

        private string _tabla = string.Empty;
        public string ValorAON
        {
            get { return _valorAON; }
            set { _valorAON = value; }
        }

        public string ValorEquivalencia
        {
            get { return _valorEquivalencia; }
            set { _valorEquivalencia = value; }
        }

        public string Descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; }
        }

        public Int32 IdProducto
        {
            get { return _idProducto; }
            set { _idProducto = value; }
        }

        public string Tabla
        {
            get { return _tabla; }
            set { _tabla = value; }
        }
    }
}
