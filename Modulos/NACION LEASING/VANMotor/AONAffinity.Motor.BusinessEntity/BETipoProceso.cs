﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    /// <summary>
    /// Clase de entidad del negocio de la tabla TipoProceso.
    /// </summary>
    [Serializable]
    public class BETipoProceso : BEAuditoria
    {
        #region Campos
        private Int32 idtipoproceso;
        private String descripcion;
        private String nombrearchivo;
        private Int32 idproducto;
        private String rutaarchivo;
        private String rutaarchivolog;
        private Int32 longitudtrama;
        #endregion

        #region Propiedades
        /// <summary>
        /// Id tipo de proceso.
        /// </summary>
        public Int32 IdTipoProceso
        {
            get { return idtipoproceso; }
            set { idtipoproceso = value; }
        }

        /// <summary>
        /// Descripción del proceso.
        /// </summary>
        public String Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        /// <summary>
        /// Nombre del archivo.
        /// </summary>
        public String NombreArchivo
        {
            get { return nombrearchivo; }
            set { nombrearchivo = value; }
        }

        /// <summary>
        /// Código de producto.
        /// </summary>
        public Int32 IdProducto
        {
            get { return this.idproducto; }
            set { this.idproducto = value; }
        }

        /// <summary>
        /// Ruta archivo cargado.
        /// </summary>
        public String RutaArchivo
        {
            get { return this.rutaarchivo; }
            set { this.rutaarchivo = value; }
        }

        /// <summary>
        /// Ruta archivo log.
        /// </summary>
        public String RutaArchivoLog
        {
            get { return this.rutaarchivolog; }
            set { this.rutaarchivolog = value; }
        }

        /// <summary>
        /// Longitud de trama.
        /// </summary>
        public Int32 LongitudTrama
        {
            get { return this.longitudtrama; }
            set { this.longitudtrama = value; }
        }
        #endregion
    }
}