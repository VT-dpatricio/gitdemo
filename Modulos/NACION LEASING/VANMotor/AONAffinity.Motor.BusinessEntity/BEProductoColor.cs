﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AONAffinity.Motor.BusinessEntity
{
    [Serializable]
    public class BEProductoColor : BEAuditoria
    {
        #region Campos
        private Int32 idproducto;
        private Int32 idcolor;
        private String codExterno;
        #endregion

        #region propiedades
        public Int32 IdProducto 
        {
            get { return this.idproducto; }
            set { this.idproducto = value; }
        }

        public Int32 IdColor 
        {
            get { return this.idcolor; }
            set { this.idcolor = value; }
        }

        public String CodExterno 
        {
            get { return this.codExterno; }
            set { this.codExterno = value; }
        }
        #endregion

        #region Campos Consulta
        private String descripcion;
        #endregion

        #region Propiedades Consulta
        public String Descripcion
        {
            get { return this.descripcion; }
            set { this.descripcion = value; }
        }
        #endregion
    }
}
