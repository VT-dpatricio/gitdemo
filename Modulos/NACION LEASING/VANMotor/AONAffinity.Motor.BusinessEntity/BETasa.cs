﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AONAffinity.Library.Resources;

namespace AONAffinity.Motor.BusinessEntity
{
    public class BETasa : BEAuditoria 
    {
        #region Campos
        private Int32 idperiodo;
        private Int32 idtipo;
        private Int32 idcategoria;
        private Int32 anioantiguedad;
        private Decimal valortasa;
        #endregion

        #region Propiedades
        public Int32 IdPeriodo 
        {
            get { return idperiodo; }
            set { idperiodo = value; }
        }

        public Int32 IdTipo 
        {
            get { return idtipo; }
            set { idtipo = value; }
        }

        public Int32 IdCategoria 
        {
            get { return idcategoria; }
            set { idcategoria = value; }
        }

        public Int32 AnioAntiguedad 
        {
            get { return anioantiguedad; }
            set { anioantiguedad = value; }
        }

        public Decimal ValorTasa 
        {
            get { return valortasa; }
            set { valortasa = value; }
        }
        #endregion

        #region Campos Consulta
        private Int32 idmodelo;
        private String descategoria;
        private Decimal montoprima;
        private String idmoneda;
        #endregion

        #region Propiedades Consulta
        public Int32 IdModelo 
        {
            get { return idmodelo; }
            set { idmodelo = value; }
        }

        public String DesCategoria 
        {
            get { return descategoria; }
            set { descategoria = value; }
        }

        public Decimal MontoPrima 
        {
            get { return this.montoprima; }
            set { this.montoprima = value; }
        }

        public String IdMoneda 
        {
            get { return this.idmoneda; }
            set { this.idmoneda = value; }
        }
        #endregion
    }
}
