Imports VAN.Consulta.BE
Imports VAN.Consulta.DL
Imports System
Imports System.Collections
Imports System.Configuration
Public Class BLImagen

    Public Function Seleccionar(ByVal pIDCertificado As String) As IList
        Dim oDL As New DLNTImagen
        Dim oBEI As New BEImagen
        'Dim ListaTemp As New ArrayList
        'Dim i As Integer
        Dim Lista As ArrayList
        'Dim UrlImg As String = ConfigurationManager.AppSettings("UrlImagen1").ToString
        oBEI.IdCertificado = pIDCertificado
        Lista = oDL.Seleccionar(oBEI)
        'For i = 0 To Lista.Count - 1
        '    Dim oBE As BEImagen = DirectCast(Lista.Item(i), BEImagen)
        '    'If (ValidarArchivoRemoto(UrlImg & oBE.RutaImagen)) Then
        '    oBE.RutaImagen = UrlImg & oBE.RutaImagen
        '    ListaTemp.Add(oBE)
        '    'End If
        'Next i
        Return Lista

    End Function

    Public Function Insertar(ByVal pIDEntidad As BEImagen) As Boolean
        Dim oDL As New DLTImagen
        Return oDL.Insertar(pIDEntidad)
    End Function

    ''' <summary>
    ''' Permite validar si existe archivo remoto.
    ''' </summary>
    ''' <param name="pcUrl">Direcci�n del archivo.</param>
    ''' <returns>True existe | False mo existe.</returns>
    ''' 

    Public Function ValidarArchivoRemoto(ByVal pcUrl As String) As Boolean
        'Dim obj As Net.HttpWebRequest = Net.HttpWebRequest.Create(pcUrl)
        Dim obj As Net.WebRequest = Net.WebRequest.Create(New Uri(pcUrl))
        Dim rpt As Boolean = False
        'obj.Method = "HEAD"
        Try
            'Dim resp As Net.HttpWebResponse = obj.GetResponse()
            Dim resp As Net.WebResponse = obj.GetResponse()
            rpt = True
        Catch ex As Net.WebException
            If (ex.Status = Net.WebExceptionStatus.ProtocolError) Then
                rpt = False
            End If
        End Try
        Return rpt
    End Function
End Class
