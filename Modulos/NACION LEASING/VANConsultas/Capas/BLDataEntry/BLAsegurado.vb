Imports VAN.Consulta.BE
Imports VAN.Consulta.DL
Imports System
Imports System.Collections
Public Class BLAsegurado

    Public Function Actualizar(ByVal pIDEntidad As BEAsegurado) As Boolean
        Dim oDL As New DLTAsegurado
        Return oDL.Actualizar(pIDEntidad)
    End Function

    Public Function Anular(ByVal pIDCertificado As String, ByVal pIDConsecutivo As Integer, ByVal pIdUsuarioMod As String, ByVal pObservacion As String) As Boolean
        Dim oDL As New DLTAsegurado
        Dim oBE As New BEAsegurado
        oBE.UsuarioModificacion = pIdUsuarioMod
        oBE.IdCertificado = pIDCertificado
        oBE.Observacion = pObservacion
        oBE.Consecutivo = pIDConsecutivo
        Return oDL.Anular(oBE)
    End Function

    Public Function ValidarNroDocumento(ByVal pIdCertificado As String, ByVal pIdProducto As Integer, ByVal pNroDocumento As String) As Boolean
        Dim oDL As New DLNTAsegurado
        Dim r As Boolean = False
        If oDL.ValidarNroDocumento(pIdCertificado, pIdProducto, pNroDocumento).CcAseg = "" Then
            r = True
        End If
        Return r
    End Function

    Public Function Seleccionar(ByVal pIDCertificado As String) As IList
        Dim oDL As New DLNTAsegurado
        Dim oBE As New BEAsegurado
        oBE.IdCertificado = pIDCertificado
        Return oDL.Seleccionar(oBE)
    End Function

    Public Function Seleccionar(ByVal pIDCertificado As String, ByVal pConsecutivo As Int16) As BEAsegurado
        Dim oDL As New DLNTAsegurado
        Return oDL.SeleccionarBE(pIDCertificado, pConsecutivo)
    End Function



End Class
