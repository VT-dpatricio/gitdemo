Imports VAN.Consulta.BE
Imports VAN.Consulta.DL
Imports System
Imports System.Collections
Public Class BLCertificado

    Public Function getNroCertificado(ByVal pIDProducto As Integer) As String
        Dim oDL As New DLNTCertificado
        Return oDL.getNroCertificado(pIDProducto).IdCertificado
    End Function

    Public Function Buscar(ByVal pIDFiltro As String, ByVal pIdUsuario As String, ByVal pIdEntidad As Int32, ByVal pIdProducto As Int32, ByVal pNumero As String, ByVal pApellido As String, ByVal pNombre As String, ByVal pFiltroAsegurado As Boolean) As IList
        Dim oDL As New DLNTCertificado
        Dim oBE As New BECertificado
        oBE.IDFiltro = pIDFiltro
        oBE.Usuario = pIdUsuario
        oBE.IDEntidad = pIdEntidad
        oBE.IdProducto = pIdProducto
        oBE.Numero = pNumero
        oBE.Apellido1 = pApellido
        oBE.Nombre1 = pNombre
        oBE.FiltroAsegurado = pFiltroAsegurado
        Return oDL.Seleccionar(oBE)
    End Function

    Public Function Seleccionar(ByVal pIDCertificado As String) As BECertificado
        Dim oDL As New DLNTCertificado
        Return oDL.SeleccionarCer(pIDCertificado)
    End Function

    Public Function Activar(ByVal pIDCertificado As String, ByVal pIdUsuarioMod As String, ByVal pObservacion As String) As Boolean
        Dim oDL As New DLTCertificado
        Dim oBE As New BECertificado
        oBE.UsuarioModificacion = pIdUsuarioMod
        oBE.IdCertificado = pIDCertificado
        oBE.Observacion = pObservacion
        Return oDL.Activar(oBE)
    End Function

    Public Function Actualizar(ByVal pBECertificado As BECertificado) As Boolean
        Dim oDL As New DLTCertificado
        Return oDL.Actualizar(pBECertificado)
    End Function

    Public Function Anular(ByVal pIDCertificado As String, ByVal pIdUsuarioMod As String, ByVal pObservacion As String, ByVal pIDMotivoAnulacion As Int32) As Boolean
        Dim oDL As New DLTCertificado
        Dim oBE As New BECertificado
        oBE.UsuarioModificacion = pIdUsuarioMod
        oBE.IdCertificado = pIDCertificado
        oBE.Observacion = pObservacion
        oBE.IdMotivoAnulacion = pIDMotivoAnulacion
        Return oDL.Anular(oBE)
    End Function

End Class
