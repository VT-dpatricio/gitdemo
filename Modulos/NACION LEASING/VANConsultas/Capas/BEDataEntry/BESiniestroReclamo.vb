Public Class BESiniestroReclamo
    Inherits BEBase
    Private _Letra As String
    Private _nroSolicitud As Int32
    Private _tipocaso As String
    Private _tramitesolicitado As String
    Private _medioingreso As String
    Private _estado As String
    Private _dictamen As String
    Private _fechaRegistro As String
    Private _fechaRespuesta As String
    Private _idCertificado As String

    Public Property IdCertificado() As String
        Get
            Return _idCertificado
        End Get
        Set(ByVal value As String)
            _idCertificado = value
        End Set
    End Property

    Public Property Letra() As String
        Get
            Return _Letra
        End Get
        Set(ByVal value As String)
            _Letra = value
        End Set
    End Property

    Public Property NroSolicitud() As Int32
        Get
            Return _nroSolicitud
        End Get
        Set(ByVal value As Int32)
            _nroSolicitud = value
        End Set
    End Property

    Public Property TipoCaso() As String
        Get
            Return _tipocaso
        End Get
        Set(ByVal value As String)
            _tipocaso = value
        End Set
    End Property

    Public Property TramiteSolicitado() As String
        Get
            Return _tramitesolicitado
        End Get
        Set(ByVal value As String)
            _tramitesolicitado = value
        End Set
    End Property

    Public Property MedioIngreso() As String
        Get
            Return _medioingreso
        End Get
        Set(ByVal value As String)
            _medioingreso = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return _estado
        End Get
        Set(ByVal value As String)
            _estado = value
        End Set
    End Property

    Public Property Dictamen() As String
        Get
            Return _dictamen
        End Get
        Set(ByVal value As String)
            _dictamen = value
        End Set
    End Property

    Public Property FechaRegistro() As String
        Get
            Return _fechaRegistro
        End Get
        Set(ByVal value As String)
            _fechaRegistro = value
        End Set
    End Property

    Public Property FechaRespuesta() As String
        Get
            Return _fechaRespuesta
        End Get
        Set(ByVal value As String)
            _fechaRespuesta = value
        End Set
    End Property

End Class
