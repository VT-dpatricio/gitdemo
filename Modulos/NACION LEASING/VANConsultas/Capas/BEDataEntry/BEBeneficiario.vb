Public Class BEBeneficiario
    Inherits BEBase
    Private _idBeneficiario As Int32
    Private _idCertificado As String
    Private _consecutivo As Int16
    Private _ccBenef As String
    Private _idTipoDocumento As String
    Private _porcentaje As Decimal
    Private _nombre1 As String
    Private _nombre2 As String
    Private _apellido1 As String
    Private _apellido2 As String
    Private _Parentesco As String
    Private _idParentesco As Int16
    Private _activo As Boolean
    Private _domicilio As String
    Private _fechaNacimiento As DateTime
    Private _ocupacion As String
    Private _telefono As String
    Private _sexo As String
    Private _tipoDocumento As String
    Private _estado As String
    Private _usuarioModificacion As String
    Private _observacion As String = String.Empty

    Public Property IdBeneficiario() As Int32
        Get
            Return _idBeneficiario
        End Get
        Set(ByVal value As Int32)
            _idBeneficiario = value
        End Set
    End Property

    Public Property IdCertificado() As String
        Get
            Return _idCertificado
        End Get
        Set(ByVal value As String)
            _idCertificado = value
        End Set
    End Property

    Public Property Consecutivo() As Int16
        Get
            Return _consecutivo
        End Get
        Set(ByVal value As Int16)
            _consecutivo = value
        End Set
    End Property

    Public Property CcBenef() As String
        Get
            Return _ccBenef
        End Get
        Set(ByVal value As String)
            _ccBenef = value
        End Set
    End Property

    Public Property IdTipoDocumento() As String
        Get
            Return _idTipoDocumento
        End Get
        Set(ByVal value As String)
            _idTipoDocumento = value
        End Set
    End Property

    Public Property Porcentaje() As Decimal
        Get
            Return _porcentaje
        End Get
        Set(ByVal value As Decimal)
            _porcentaje = value
        End Set
    End Property

    Public Property Nombre1() As String
        Get
            Return _nombre1
        End Get
        Set(ByVal value As String)
            _nombre1 = value
        End Set
    End Property

    Public Property Nombre2() As String
        Get
            Return _nombre2
        End Get
        Set(ByVal value As String)
            _nombre2 = value
        End Set
    End Property

    Public Property Apellido1() As String
        Get
            Return _apellido1
        End Get
        Set(ByVal value As String)
            _apellido1 = value
        End Set
    End Property

    Public Property Apellido2() As String
        Get
            Return _apellido2
        End Get
        Set(ByVal value As String)
            _apellido2 = value
        End Set
    End Property

    Public Property Parentesco() As String
        Get
            Return _Parentesco
        End Get
        Set(ByVal value As String)
            _Parentesco = value
        End Set
    End Property


    Public Property IdParentesco() As Int16
        Get
            Return _idParentesco
        End Get
        Set(ByVal value As Int16)
            _idParentesco = value
        End Set
    End Property

    Public Property Activo() As Boolean
        Get
            Return _activo
        End Get
        Set(ByVal value As Boolean)
            _activo = value
        End Set
    End Property

    Public Property Domicilio() As String
        Get
            Return _domicilio
        End Get
        Set(ByVal value As String)
            _domicilio = value
        End Set
    End Property

    Public Property FechaNacimiento() As DateTime
        Get
            Return _fechaNacimiento
        End Get
        Set(ByVal value As DateTime)
            _fechaNacimiento = value
        End Set
    End Property

    Public Property Ocupacion() As String
        Get
            Return _ocupacion
        End Get
        Set(ByVal value As String)
            _ocupacion = value
        End Set
    End Property

    Public Property Telefono() As String
        Get
            Return _telefono
        End Get
        Set(ByVal value As String)
            _telefono = value
        End Set
    End Property

    Public Property Sexo() As String
        Get
            Return _sexo
        End Get
        Set(ByVal value As String)
            _sexo = value
        End Set
    End Property

    Public Property TipoDocumento() As String
        Get
            Return _tipoDocumento
        End Get
        Set(ByVal value As String)
            _tipoDocumento = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return _estado
        End Get
        Set(ByVal value As String)
            _estado = value
        End Set
    End Property

    Public Property UsuarioModificacion() As String
        Get
            Return _usuarioModificacion
        End Get
        Set(ByVal value As String)
            _usuarioModificacion = value
        End Set
    End Property

    Public Property Observacion() As String
        Get
            Return _observacion
        End Get
        Set(ByVal value As String)
            _observacion = value
        End Set
    End Property

End Class
