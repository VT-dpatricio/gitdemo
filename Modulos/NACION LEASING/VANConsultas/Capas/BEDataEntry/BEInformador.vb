Public Class BEInformador
    Inherits BEBase
    Private _idInformador As String
    Private _nombre As String

    Public Property IdInformador() As String
        Get
            Return _idInformador
        End Get
        Set(ByVal value As String)
            _idInformador = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property


End Class
