Public Class BEMedioPago
    Inherits BEBase

    Private _idProducto As Int32
    Private _idMedioPago As String
    Private _nombre As String


    Public Property IdProducto() As Int32
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Int32)
            _idProducto = value
        End Set
    End Property

    Public Property IdMedioPago() As String
        Get
            Return _idMedioPago
        End Get
        Set(ByVal value As String)
            _idMedioPago = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

End Class
