﻿Imports VAN.Consulta.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLTSeguridadEvento
    Inherits DLBase
    Implements IDLT

    Public Function Actualizar(ByVal pEntidad As BE.BEBase) As Boolean Implements IDLT.Actualizar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Eliminar(ByVal pCodigo As Integer) As Boolean Implements IDLT.Eliminar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Insertar(ByVal pEntidad As BE.BEBase) As Boolean Implements IDLT.Insertar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SE_InsertarEvento", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm = Me.LlenarEstructura(pEntidad, cm, "I")
        Dim resultado As Boolean = False
        Try
            cn.Open()
            cm.ExecuteNonQuery()
            resultado = cm.Parameters("ReturnValue").Value()
            'resultado = cm.ExecuteNonQuery()
            'resultado = True
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return resultado
    End Function

    Public Function LlenarEstructura(ByVal pEntidad As BE.BEBase, ByVal pcm As System.Data.SqlClient.SqlCommand, ByVal TipoTransaccion As String) As System.Data.SqlClient.SqlCommand Implements IDLT.LlenarEstructura
        Dim oBE As BESeguridadEvento = DirectCast(pEntidad, BESeguridadEvento)
        If (TipoTransaccion = "I") Then
            pcm.Parameters.Add("@IDSistema", SqlDbType.Int).Value = oBE.IDSistema
            pcm.Parameters.Add("@IDUsuario", SqlDbType.VarChar, 50).Value = oBE.IDUsuario
            pcm.Parameters.Add("@IDTipoEvento", SqlDbType.Int).Value = oBE.IDTipoEvento
            pcm.Parameters.Add("@Detalle", SqlDbType.VarChar, 500).Value = oBE.Detalle
            pcm.Parameters.Add("@Host", SqlDbType.VarChar, 50).Value = oBE.Host
            pcm.Parameters.Add("@IDUsuarioAlta", SqlDbType.VarChar, 50).Value = oBE.UsuarioCreacion
            pcm.Parameters.Add("ReturnValue", SqlDbType.Bit).Direction = ParameterDirection.ReturnValue
        End If
        'If (TipoTransaccion = "A") Then
        'End If
        Return pcm
    End Function
End Class
