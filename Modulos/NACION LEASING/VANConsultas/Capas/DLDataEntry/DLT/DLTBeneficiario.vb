Imports VAN.Consulta.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLTBeneficiario
    Inherits DLBase
    Implements IDLT

    Public Function Actualizar(ByVal pEntidad As BE.BEBase) As Boolean Implements IDLT.Actualizar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SC_ActualizarBeneficiario", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm = Me.LlenarEstructura(pEntidad, cm, "Actualizar")
        Dim resultado As Boolean = False
        Try
            cn.Open()
            cm.ExecuteNonQuery()
            resultado = cm.Parameters("ReturnValue").Value()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return resultado
    End Function

    Public Function Eliminar(ByVal pCodigo As Integer) As Boolean Implements IDLT.Eliminar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Anular(ByVal pEntidad As BE.BEBase) As Boolean
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SC_AnularBeneficiario", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm = Me.LlenarEstructura(pEntidad, cm, "Anular")
        Dim resultado As Boolean = False
        Try
            cn.Open()
            cm.ExecuteNonQuery()
            resultado = cm.Parameters("ReturnValue").Value()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return resultado
    End Function

    Public Function Insertar(ByVal pEntidad As BE.BEBase) As Boolean Implements IDLT.Insertar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function LlenarEstructura(ByVal pEntidad As BE.BEBase, ByVal pcm As System.Data.SqlClient.SqlCommand, ByVal TipoTransaccion As String) As System.Data.SqlClient.SqlCommand Implements IDLT.LlenarEstructura
        Dim oBE As BEBeneficiario = DirectCast(pEntidad, BEBeneficiario)
        If (TipoTransaccion = "Actualizar") Then
            'pcm.Parameters.Add("@IdCertificado", SqlDbType.VarChar, 25).Value = oBE.IdCertificado
            pcm.Parameters.Add("@Domicilio", SqlDbType.VarChar, 250).Value = oBE.Domicilio
            pcm.Parameters.Add("@Telefono", SqlDbType.VarChar, 15).Value = oBE.Telefono
            pcm.Parameters.Add("@nombre1", SqlDbType.VarChar, 60).Value = oBE.Nombre1
            pcm.Parameters.Add("@nombre2", SqlDbType.VarChar, 60).Value = oBE.Nombre2
            pcm.Parameters.Add("@apellido1", SqlDbType.VarChar, 60).Value = oBE.Apellido1
            pcm.Parameters.Add("@apellido2", SqlDbType.VarChar, 60).Value = oBE.Apellido2
            pcm.Parameters.Add("@CcBenef", SqlDbType.VarChar, 20).Value = oBE.CcBenef 'Nro Documento
            pcm.Parameters.Add("@idTipoDocumento", SqlDbType.VarChar, 3).Value = oBE.IdTipoDocumento
            If oBE.Sexo = "0" Then
                pcm.Parameters.Add("@Sexo", SqlDbType.Char, 1).Value = DBNull.Value
            Else
                pcm.Parameters.Add("@Sexo", SqlDbType.Char, 1).Value = oBE.Sexo
            End If
            pcm.Parameters.Add("@porcentaje", SqlDbType.Decimal).Value = oBE.Porcentaje
            pcm.Parameters.Add("@IdParentesco", SqlDbType.SmallInt).Value = oBE.IdParentesco
            'If oBE.FechaNacimiento = Nothing Then
            '    pcm.Parameters.Add("@FechaNacimiento", SqlDbType.DateTime).Value = DBNull.Value
            'Else
            pcm.Parameters.Add("@FechaNacimiento", SqlDbType.DateTime).Value = oBE.FechaNacimiento
            'End If
            pcm.Parameters.Add("@Ocupacion", SqlDbType.VarChar, 80).Value = oBE.Ocupacion
        End If

        If (TipoTransaccion = "Anular") Then

        End If
        pcm.Parameters.Add("@IdCertificado", SqlDbType.VarChar, 25).Value = oBE.IdCertificado
        pcm.Parameters.Add("@IdBeneficiario", SqlDbType.Int).Value = oBE.IdBeneficiario
        pcm.Parameters.Add("@Consecutivo", SqlDbType.SmallInt).Value = oBE.Consecutivo
        pcm.Parameters.Add("ReturnValue", SqlDbType.Bit).Direction = ParameterDirection.ReturnValue
        pcm.Parameters.Add("@UsuarioModificacion", SqlDbType.VarChar, 50).Value = oBE.UsuarioModificacion
        pcm.Parameters.Add("@Observacion", SqlDbType.VarChar, 500).Value = oBE.Observacion


        Return pcm
    End Function

End Class
