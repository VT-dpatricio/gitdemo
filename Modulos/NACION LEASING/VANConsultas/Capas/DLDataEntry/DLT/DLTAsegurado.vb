Imports VAN.Consulta.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLTAsegurado
    Inherits DLBase
    Implements IDLT

    Public Function Actualizar(ByVal pEntidad As BE.BEBase) As Boolean Implements IDLT.Actualizar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SC_ActualizarAsegurado", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm = Me.LlenarEstructura(pEntidad, cm, "Actualizar")
        Dim resultado As Boolean = False
        Try
            cn.Open()
            cm.ExecuteNonQuery()
            resultado = cm.Parameters("ReturnValue").Value()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return resultado
    End Function

    Public Function Eliminar(ByVal pCodigo As Integer) As Boolean Implements IDLT.Eliminar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Anular(ByVal pEntidad As BE.BEBase) As Boolean
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SC_AnularAsegurado", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm = Me.LlenarEstructura(pEntidad, cm, "Anular")
        Dim resultado As Boolean = False
        Try
            cn.Open()
            cm.ExecuteNonQuery()
            resultado = cm.Parameters("ReturnValue").Value()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return resultado
    End Function

    Public Function Insertar(ByVal pEntidad As BE.BEBase) As Boolean Implements IDLT.Insertar
     Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function LlenarEstructura(ByVal pEntidad As BE.BEBase, ByVal pcm As System.Data.SqlClient.SqlCommand, ByVal TipoTransaccion As String) As System.Data.SqlClient.SqlCommand Implements IDLT.LlenarEstructura
        Dim oBE As BEAsegurado = DirectCast(pEntidad, BEAsegurado)
        If (TipoTransaccion = "Actualizar") Then
            pcm.Parameters.Add("@Direccion", SqlDbType.VarChar, 250).Value = oBE.Direccion
            pcm.Parameters.Add("@Telefono", SqlDbType.VarChar, 15).Value = oBE.Telefono
            pcm.Parameters.Add("@nombre1", SqlDbType.VarChar, 60).Value = oBE.Nombre1
            pcm.Parameters.Add("@nombre2", SqlDbType.VarChar, 60).Value = oBE.Nombre2
            pcm.Parameters.Add("@apellido1", SqlDbType.VarChar, 60).Value = oBE.Apellido1
            pcm.Parameters.Add("@apellido2", SqlDbType.VarChar, 60).Value = oBE.Apellido2
            pcm.Parameters.Add("@ccAseg", SqlDbType.VarChar, 20).Value = oBE.CcAseg 'Nro Documento
            pcm.Parameters.Add("@idTipoDocumento", SqlDbType.VarChar, 3).Value = oBE.IdTipoDocumento
            'pcm.Parameters.Add("@Sexo", SqlDbType.Char, 1).Value = oBE.Sexo
            'pcm.Parameters.Add("@EstadoCivil", SqlDbType.VarChar, 5).Value = oBE.EstadoCivil
            If oBE.IdCiudad = 0 Then
                pcm.Parameters.Add("@IdCiudad", SqlDbType.Int).Value = DBNull.Value
            Else
                pcm.Parameters.Add("@IdCiudad", SqlDbType.Int).Value = oBE.IdCiudad
            End If

            pcm.Parameters.Add("@IdParentesco", SqlDbType.SmallInt).Value = oBE.IdParentesco
            pcm.Parameters.Add("@FechaNacimiento", SqlDbType.SmallDateTime).Value = oBE.FechaNacimiento
        End If

        If (TipoTransaccion = "Anular") Then

        End If
        pcm.Parameters.Add("@IdCertificado", SqlDbType.VarChar, 25).Value = oBE.IdCertificado
        pcm.Parameters.Add("@Consecutivo", SqlDbType.Int).Value = oBE.Consecutivo
        pcm.Parameters.Add("ReturnValue", SqlDbType.Bit).Direction = ParameterDirection.ReturnValue
        pcm.Parameters.Add("@UsuarioModificacion", SqlDbType.VarChar, 50).Value = oBE.UsuarioModificacion
        pcm.Parameters.Add("@Observacion", SqlDbType.VarChar, 500).Value = oBE.Observacion
        Return pcm
    End Function

End Class
