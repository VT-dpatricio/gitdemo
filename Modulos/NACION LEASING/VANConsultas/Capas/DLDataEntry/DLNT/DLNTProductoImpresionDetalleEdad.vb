﻿Imports VAN.Consulta.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLNTProductoImpresionDetalleEdad
    Inherits DLBase
    Implements IDLNT

    Public Function Seleccionar(ByVal IdImpresionPadre As Integer, ByVal IDCertificado As String) As IList
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("BW_PRC_SEL_ImpresionDetalleEdad", cn)
        cm.CommandType = CommandType.StoredProcedure

        cm.Parameters.Add("@IDCertificado", SqlDbType.VarChar, 25).Value = IDCertificado
        cm.Parameters.Add("@IdProductoImpresionPadre", SqlDbType.Int).Value = IdImpresionPadre

        Dim oBE As BEProductoImpresionDetalleEdad
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BEProductoImpresionDetalleEdad
                oBE.EdadInicial = rd.GetInt32(rd.GetOrdinal("EdadInicial"))
                oBE.EdadFinal = rd.GetInt32(rd.GetOrdinal("EdadFinal"))
                oBE.Opcion = rd.GetString(rd.GetOrdinal("Opcion"))
                oBE.IDProductoDetalleEdad = rd.GetInt32(rd.GetOrdinal("IDProductoImpresionEdad"))
                oBE.IDProducto = rd.GetInt32(rd.GetOrdinal("IDProducto"))
                oBE.IDImpresion = rd.GetInt32(rd.GetOrdinal("IDImpresion"))
                oBE.Visualiza = rd.GetBoolean(rd.GetOrdinal("Visualiza"))
                oBE.Directorio = rd.GetString(rd.GetOrdinal("Directorio"))
                oBE.NombreArchivo = rd.GetString(rd.GetOrdinal("NombreArchivo"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function Seleccionar1() As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar1(pEntidad As BEBase) As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar1(pCodigo As Integer) As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(pCodigo As Integer) As BEBase Implements IDLNT.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function
End Class
