Imports VAN.Consulta.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLNTProducto
    Inherits DLBase
    Implements IDLNT

    Public Function Seleccionar(ByVal pEntidad As BEBase) As System.Collections.IList Implements IDLNT.Seleccionar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand()
        cm.CommandType = CommandType.StoredProcedure
        cm.Connection = cn
        Dim oBE As BEProducto = DirectCast(pEntidad, BEProducto)
        If oBE.Opcion = "SeleccionarActivos" Then
            cm.CommandText = "SC_SeleccionarProducto"
        Else
            cm.CommandText = "SC_SeleccionarProductoTodos"
        End If
        cm.Parameters.Add("@IDEntidad", SqlDbType.Int).Value = oBE.IDEntidad
        cm.Parameters.Add("@IDUsuario", SqlDbType.NVarChar, 50).Value = oBE.IDUsuario
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BEProducto
                oBE.IDProducto = rd.GetInt32(rd.GetOrdinal("idProducto"))
                oBE.Descripcion = rd.GetString(rd.GetOrdinal("nombre"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    

    Public Function Seleccionar() As System.Collections.IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As System.Collections.IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function


    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BE.BEBase Implements IDLNT.SeleccionarBE
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("BW_SeleccionarProducto", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@IDProducto", SqlDbType.Int).Value = pCodigo
        Dim oBE As New BEProducto
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            If rd.Read Then
                oBE.IDProducto = rd.GetInt32(rd.GetOrdinal("IDProducto"))
                oBE.IDEntidad = rd.GetInt32(rd.GetOrdinal("IDEntidad"))
                oBE.ReporteCertificado = rd.GetString(rd.GetOrdinal("ReporteCertificado"))
                oBE.ReporteSolicitud = rd.GetString(rd.GetOrdinal("ReporteSolicitud"))
                oBE.ReporteCondicion = rd.GetString(rd.GetOrdinal("ReporteCondicion"))
                oBE.ReporteDeclaracionJurada = rd.GetString(rd.GetOrdinal("ReporteDeclaracionJurada"))
                oBE.IDImpresionCertificado = rd.GetInt32(rd.GetOrdinal("IDImpresionCertificado"))
                oBE.IDImpresionSolicitud = rd.GetInt32(rd.GetOrdinal("IDImpresionSolicitud"))
                oBE.IDImpresionCondicion = rd.GetInt32(rd.GetOrdinal("IDImpresionCondicion"))
                oBE.IDImpresionDeclaracionJurada = rd.GetInt32(rd.GetOrdinal("IDImpresionDeclaracionJurada"))
            End If
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return oBE
    End Function
End Class
