Imports VAN.Consulta.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLNTAsegurado
    Inherits DLBase
    Implements IDLNT


    Public Function ValidarNroDocumento(ByVal pIdCertificado As String, ByVal pIdProducto As Integer, ByVal pNroDocumento As String) As BEAsegurado
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("DE_ValidarNroDocumento", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@IdCertificado", SqlDbType.VarChar, 25).Value = pIdCertificado
        cm.Parameters.Add("@CcAseg", SqlDbType.VarChar, 20).Value = pNroDocumento
        cm.Parameters.Add("@IdProducto", SqlDbType.Int).Value = pIdProducto
        Dim oBE As New BEAsegurado
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            If rd.Read Then
                oBE.CcAseg = rd.GetString(rd.GetOrdinal("CcAseg"))
            End If
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return oBE
    End Function

    Public Function Seleccionar() As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(ByVal pIDCertificado As String, ByVal pConsecutivo As Integer) As BEAsegurado
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SCAR_SeleccionarAseguradoxID", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@idCertificado", SqlDbType.VarChar, 25).Value = pIDCertificado
        cm.Parameters.Add("@consecutivo", SqlDbType.SmallInt).Value = pConsecutivo
        Dim oBE As New BEAsegurado
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            If rd.Read Then
                oBE.IdCertificado = rd.GetString(rd.GetOrdinal("IdCertificado"))
                oBE.Consecutivo = rd.GetInt16(rd.GetOrdinal("Consecutivo"))
                oBE.IdTipoDocumento = rd.GetString(rd.GetOrdinal("IdTipoDocumento"))
                oBE.Nombre1 = rd.GetString(rd.GetOrdinal("Nombre1"))
                oBE.Nombre2 = rd.GetString(rd.GetOrdinal("Nombre2"))
                oBE.Apellido1 = rd.GetString(rd.GetOrdinal("Apellido1"))
                oBE.Apellido2 = rd.GetString(rd.GetOrdinal("Apellido2"))
                oBE.CcAseg = rd.GetString(rd.GetOrdinal("CcAseg"))
                oBE.IdParentesco = rd.GetInt16(rd.GetOrdinal("IdParentesco"))
                oBE.Telefono = rd.GetString(rd.GetOrdinal("Telefono"))
                oBE.Direccion = rd.GetString(rd.GetOrdinal("Direccion"))
                oBE.IdCiudad = rd.GetInt32(rd.GetOrdinal("IdCiudad"))
                oBE.IDProvincia = rd.GetInt32(rd.GetOrdinal("IDProvincia"))
                oBE.IDDepartamento = rd.GetInt32(rd.GetOrdinal("IDDepartamento"))
                oBE.IDPais = rd.GetInt32(rd.GetOrdinal("IDPais"))
                oBE.IDPaisCer = rd.GetInt32(rd.GetOrdinal("IDPaisCer"))
                oBE.Ciudad = rd.GetString(rd.GetOrdinal("Ciudad"))
                oBE.Provincia = rd.GetString(rd.GetOrdinal("Provincia"))
                oBE.FechaNacimiento = rd.GetString(rd.GetOrdinal("FechaNacimiento"))
                'oBE.Sexo = rd.GetString(rd.GetOrdinal("Sexo"))
                oBE.Plan = rd.GetString(rd.GetOrdinal("Plan"))
                oBE.Prima = rd.GetDecimal(rd.GetOrdinal("Prima"))
                oBE.Activo = rd.GetBoolean(rd.GetOrdinal("Activo"))
                oBE.DirNumero = rd.GetString(rd.GetOrdinal("DirNumero"))
                oBE.DirPiso = rd.GetString(rd.GetOrdinal("DirPiso"))
                oBE.DirDpto = rd.GetString(rd.GetOrdinal("DirDpto"))
                oBE.DirCodPostal = rd.GetString(rd.GetOrdinal("DirCodPostal"))
                oBE.CodAreaTelefono = rd.GetString(rd.GetOrdinal("CodAreaTelefono"))
            End If
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return oBE
    End Function

    Public Function Seleccionar(ByVal pEntidad As BE.BEBase) As IList Implements IDLNT.Seleccionar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SC_SeleccionarAsegurado", cn)
        cm.CommandType = CommandType.StoredProcedure
        Dim oBE As BEAsegurado = DirectCast(pEntidad, BEAsegurado)
        cm.Parameters.Add("@IDCertificado", SqlDbType.VarChar, 25).Value = oBE.IdCertificado
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BEAsegurado
                oBE.IdCertificado = rd.GetString(rd.GetOrdinal("IdCertificado"))
                oBE.Consecutivo = rd.GetInt16(rd.GetOrdinal("Consecutivo"))
                oBE.Nombre1 = rd.GetString(rd.GetOrdinal("NombreApellido"))
                oBE.TipoDocumento = rd.GetString(rd.GetOrdinal("TipoDocumento"))
                oBE.CcAseg = rd.GetString(rd.GetOrdinal("CcAseg"))
                oBE.Direccion = rd.GetString(rd.GetOrdinal("Direccion"))
                oBE.Estado = rd.GetString(rd.GetOrdinal("Estado"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BEBase Implements IDLNT.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function

End Class
