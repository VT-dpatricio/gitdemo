Imports VAN.Consulta.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLNTBeneficiario
    Inherits DLBase
    Implements IDLNT

    Public Function Seleccionar() As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pEntidad As BE.BEBase) As IList Implements IDLNT.Seleccionar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SC_SeleccionarBeneficiario", cn)
        cm.CommandType = CommandType.StoredProcedure
        Dim oBE As BEBeneficiario = DirectCast(pEntidad, BEBeneficiario)
        cm.Parameters.Add("@IDCertificado", SqlDbType.VarChar, 25).Value = oBE.IdCertificado
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BEBeneficiario
                oBE.IdCertificado = rd.GetString(rd.GetOrdinal("IdCertificado"))
                oBE.IdBeneficiario = rd.GetInt32(rd.GetOrdinal("IdBeneficiario"))
                oBE.Consecutivo = rd.GetInt16(rd.GetOrdinal("Consecutivo"))
                oBE.Nombre1 = rd.GetString(rd.GetOrdinal("NombreApellido"))
                oBE.TipoDocumento = rd.GetString(rd.GetOrdinal("TipoDocumento"))
                oBE.CcBenef = rd.GetString(rd.GetOrdinal("CcBenef"))
                oBE.Domicilio = rd.GetString(rd.GetOrdinal("Domicilio"))
                oBE.Estado = rd.GetString(rd.GetOrdinal("Estado"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function


    Public Function SeleccionarBEN(ByVal pIdBeneficiario As Integer) As BEBeneficiario
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SC_SeleccionarBeneficiarioxID", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@idBeneficiario", SqlDbType.Int).Value = pIdBeneficiario
        Dim oBE As New BEBeneficiario
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            If rd.Read Then
                oBE.IdBeneficiario = rd.GetInt32(rd.GetOrdinal("IdBeneficiario"))
                oBE.IdCertificado = rd.GetString(rd.GetOrdinal("IdCertificado"))
                oBE.Consecutivo = rd.GetInt16(rd.GetOrdinal("Consecutivo"))
                oBE.IdTipoDocumento = rd.GetString(rd.GetOrdinal("IdTipoDocumento"))
                oBE.Nombre1 = rd.GetString(rd.GetOrdinal("Nombre1"))
                oBE.Nombre2 = rd.GetString(rd.GetOrdinal("Nombre2"))
                oBE.Apellido1 = rd.GetString(rd.GetOrdinal("Apellido1"))
                oBE.Apellido2 = rd.GetString(rd.GetOrdinal("Apellido2"))
                oBE.CcBenef = rd.GetString(rd.GetOrdinal("CcBenef"))
                oBE.IdParentesco = rd.GetInt16(rd.GetOrdinal("IdParentesco"))
                oBE.Telefono = rd.GetString(rd.GetOrdinal("Telefono"))
                oBE.Domicilio = rd.GetString(rd.GetOrdinal("Domicilio"))
                oBE.FechaNacimiento = rd.GetString(rd.GetOrdinal("FechaNacimiento"))
                oBE.Sexo = rd.GetString(rd.GetOrdinal("Sexo"))
                oBE.Ocupacion = rd.GetString(rd.GetOrdinal("Ocupacion"))
                oBE.Porcentaje = rd.GetDecimal(rd.GetOrdinal("Porcentaje"))
                oBE.Activo = rd.GetBoolean(rd.GetOrdinal("Activo"))
            End If
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return oBE
    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BEBase Implements IDLNT.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function
End Class
