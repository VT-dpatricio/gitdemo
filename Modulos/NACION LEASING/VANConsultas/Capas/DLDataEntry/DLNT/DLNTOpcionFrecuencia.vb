Imports VAN.Consulta.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLNTOpcionFrecuencia
    Inherits DLBase
    Implements IDLNT
    Public Function Seleccionar() As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pEntidad As BEBase) As IList Implements IDLNT.Seleccionar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("DE_SeleccionarOpcionFrecuencia", cn)

        Dim oBE As BEOpcionFrecuencia = DirectCast(pEntidad, BEOpcionFrecuencia)
        cm.Parameters.Add("@IdProducto", SqlDbType.Int).Value = oBE.IdProducto
        cm.Parameters.Add("@Opcion", SqlDbType.VarChar, 10).Value = oBE.Opcion
        cm.CommandType = CommandType.StoredProcedure
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BEOpcionFrecuencia
                oBE.IdFrecuencia = rd.GetInt32(rd.GetOrdinal("IdFrecuencia"))
                oBE.Nombre = rd.GetString(rd.GetOrdinal("Nombre"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BEBase Implements IDLNT.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function
End Class
