Imports System.IO
Imports VAN.Consulta.BE
Imports VAN.Consulta.BL
Partial Class SubirImagen
    Inherits System.Web.UI.Page
    Protected Sub btnCargarImagen_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCargarImagen.Click
        If Session("IDCertificado") Is Nothing Then
            lblMensaje.Text = "Por favor,vuelva a seleccionar el certificado en la b�squeda"
            Exit Sub
        End If
        Dim RutaImgServer As String = System.Configuration.ConfigurationManager.AppSettings("RutaImagen").ToString
        Dim sExt As String = String.Empty
        Dim sName As String = String.Empty
        Dim sIDCertificado As String = Session("IDCertificado").ToString
        Dim sNumCertificado As String = Session("NumCertificado").ToString
        Dim sIDProducto As String = Session("IDProducto").ToString
        Dim sRutPeriodoPro As String = ddlAnio.SelectedValue & "/" & ddlMes.SelectedItem.ToString & "/" & sIDProducto & "/"
        Dim sIDUsuario As String = Membership.GetUser().UserName
        Try
            If fuImagen.HasFile Then
                sName = fuImagen.FileName
                sExt = Path.GetExtension(sName)
                If ValidaExtension(sExt) Then
                    If (ComparaNombres(sNumCertificado, sName)) Then
                        If Not ExisteImagen(RutaImgServer & sRutPeriodoPro, sName) Then
                            fuImagen.SaveAs(RutaImgServer & sRutPeriodoPro & sName)
                            Dim oBEImagen As New BEImagen
                            Dim oBLImagen As New BLImagen
                            oBEImagen.IdCertificado = sIDCertificado
                            oBEImagen.NombreImagen = sName
                            oBEImagen.RutaImagen = sRutPeriodoPro & sName
                            oBEImagen.UsuarioCreacion = sIDUsuario
                            oBEImagen.Descripcion = "Sis. ConsultasAON"
                            oBLImagen.Insertar(oBEImagen)
                            lblMensaje.Text = ""
                            ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "act", "alert('Imagen grabada con �xito.');parent.ActListaImg();", True)
                        Else
                            lblMensaje.Text = "Existe una imagen en el servidor con el mismo nombre."
                        End If
                    Else
                        lblMensaje.Text = "El n�mero de certificado y el n�mero de la imagen no conciden"
                    End If

                Else
                    lblMensaje.Text = "El archivo seleccionado no es del tipo imagen."
                End If
            Else
                lblMensaje.Text = "Seleccione la imagen que desea subir."
            End If
        Catch ex As Exception
            lblMensaje.Text = "Ocurri� un error al cargar la imagen"
        End Try

    End Sub

    Private Function ValidaExtension(ByVal sExtension As String) As Boolean
        Select Case sExtension
            Case ".jpg", ".jpeg"
                Return True
            Case Else
                Return False
        End Select
    End Function

    Private Function ComparaNombres(ByVal pNumCertificado As String, ByVal pImg As String) As Boolean
        Dim numCertificado As String
        Dim numeros As Char()
        numCertificado = pImg.Replace(".JPG", "")
        numeros = numCertificado.ToCharArray()
        numCertificado = ""
        For Each num As Char In numeros
            If (IsNumeric(num)) Then
                numCertificado = numCertificado + num
            End If
        Next
        If (numCertificado.Trim = pNumCertificado) Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Function ExisteImagen(ByVal rutaServer As String, ByVal img As String) As Boolean
        Dim dirServ As DirectoryInfo = New DirectoryInfo(rutaServer)
        Dim files As FileInfo()
        Dim found As Boolean
        files = dirServ.GetFiles()
        found = False
        Try
            For Each file As FileInfo In files 'Buscamos el archivo
                If (file.Name = img) Then
                    found = True
                    Exit For
                End If
            Next
            Return found
        Catch ex As Exception
            Return False
        End Try

        Return False
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'Establece Permisos-----------------
            Dim oBLMenuO As New BLMenuOperacion
            Dim oBEOperacion As New BEOperacion
            Dim IDUsuario As String = Membership.GetUser().UserName
            oBEOperacion = oBLMenuO.Seleccionar(4, IDUsuario)
            pSubirImagen.Visible = oBEOperacion.SubirImagen
            ddlMes.SelectedValue = Now.Month
            ddlAnio.SelectedValue = Now.Year
        End If
    End Sub
End Class
