<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Consulta.aspx.vb" Inherits="Consulta" EnableEventValidation="false" culture="es-PE" uiCulture="es-PE"%>
<%@ Register Src="Cargando.ascx" TagName="Cargando" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script src="Script/jquery.simplemodal.1.4.2.min.js" type="text/javascript"></script> 
    <script src="Script/jquery.colorbox.js" type="text/javascript"></script> 
    <link href="css/colorbox.css" rel="stylesheet" />
<script type="text/jscript" language="javascript">
    function TabActive(value) {
        var CTab2 = $get('__tab_ctl00_ContentPlaceHolder1_TabConsulta_TabPanel2');
        var CTab3 = $get('__tab_ctl00_ContentPlaceHolder1_TabConsulta_TabPanel3');
        var CTab4 = $get('__tab_ctl00_ContentPlaceHolder1_TabConsulta_TabPanel4');
        var CTab5 = $get('__tab_ctl00_ContentPlaceHolder1_TabConsulta_TabPanel5');
        var CTab6 = $get('__tab_ctl00_ContentPlaceHolder1_TabConsulta_TabPanel6');
        var CTab7 = $get('__tab_ctl00_ContentPlaceHolder1_TabConsulta_TabPanel7');
        var CTab8 = $get('__tab_ctl00_ContentPlaceHolder1_TabConsulta_TabPanel8');
        var CTab9 = $get('__tab_ctl00_ContentPlaceHolder1_TabConsulta_TabPanel9');
        CTab2.disabled = value;
        CTab3.disabled = value;
        CTab4.disabled = value;
        CTab5.disabled = value;
        CTab6.disabled = value;
        CTab7.disabled = value;
        CTab8.disabled = value;
        CTab9.disabled = value;
        //document.getElementById('__tab_ctl00_ContentPlaceHolder1_TabConsulta_TabPanel8').style.display = 'none';
        //document.getElementById('__tab_ctl00_ContentPlaceHolder1_TabConsulta_TabPanel8').style.visibility = 'none';
        //document.getElementById('__tab_ctl00_ContentPlaceHolder1_TabConsulta_TabPanel8').disabled = true;
        //document.getElementById('ctl00_ContentPlaceHolder1_TabConsulta_TabPanel8_tab').style.display = 'none';
    }


    function ActListaImg() {
        Bt = $get("<%=btnActImagen.ClientID%>");
        
        Bt.click();
    }

    function EstadoTabIC(estado)
    {
        if (!estado) {
            $get('ctl00_ContentPlaceHolder1_TabConsulta_TabPanel8_tab').style.display = 'none';
        }
        else { $get('ctl00_ContentPlaceHolder1_TabConsulta_TabPanel8_tab').style.display = 'inline' }

    }

    function ValidaFVencimiento(oSrc, args) {
        var objddlMedioPago = document.getElementById('<%=ddlIPMedioPago.ClientID%>').value;
        if ((objddlMedioPago == "TV" || objddlMedioPago == "TA" || objddlMedioPago == "TM") && args.Value.trim() == "") {
            args.IsValid = false;
        }
        else {
            args.IsValid = true;
        }
    }

    function Imprimir(ID) {
        var lo_ventana;
        lsPagina = 'ImprimirEstCuenta.aspx?IDCer=' + ID;
        //lsPagina = 'ImprimirCer.aspx?IDCer='+ID;	    	    
        var posicion_x;
        var posicion_y;
        posicion_x = (screen.width / 2) - (960 / 2);
        posicion_y = (screen.height / 2) - (600 / 2);
        lo_ventana = window.open(lsPagina, 'Imprimir', 'top=' + posicion_y + ',left=' + posicion_x + ',toolbar=no,location=no,directories=no,menubar=no,status=no,resizable=yes,scrollbars=yes,width=960,height=600');
        lo_ventana.focus();
    }


    function ImprimirCer(ID, Tipo) {
        src = 'ImprimirCertificado.aspx?ID=' + ID + '&Tipo=' + Tipo;
        OpenDialog(src);

    }

    function OpenDialog(src) {
        var options = {
            iframe: true,
            fastIframe: true,
            href: src,
            width: 950,
            height: 482,
            initialWidth: 950,
            initialHeight: 482,
            close: "Cerrar",
            escKey: true
        }

        $.colorbox(options);
    }

    function Numero(e) {
        key = (document.all) ? e.keyCode : e.which;
        if (key < 48 || key > 57) {
            if (key == 8 || key == 0) {
                return true
            }
            else
                return false;
        }
    }

    function Imprimir(ID, Tipo) {
        src = 'Imprimir.aspx?ID=' + ID + '&Tipo=' + Tipo;
        OpenDialog(src);
    }

    function EmailPoliza() {
        var txtName = document.getElementById('<%=txtITEmail.ClientID%>');
        if (document.getElementById('ctl00_ContentPlaceHolder1_TabConsulta_TabPanel2_chk_envioMailPoliza').checked == true) {
            document.getElementById('ctl00_ContentPlaceHolder1_TabConsulta_TabPanel2_txtITEmail').disabled = false;
            document.getElementById("ctl00_ContentPlaceHolder1_TabConsulta_TabPanel2_txtITEmail").readOnly = false;
            txtName.className = 'txtTexto';
            document.getElementById('ctl00_ContentPlaceHolder1_TabConsulta_TabPanel2_hfEditarEmail').value = "S";
        }
        else {
            document.getElementById('ctl00_ContentPlaceHolder1_TabConsulta_TabPanel2_txtITEmail').disabled = true;
            document.getElementById("ctl00_ContentPlaceHolder1_TabConsulta_TabPanel2_txtITEmail").readOnly = true;
            txtName.className = 'txtTextoD';
            document.getElementById('ctl00_ContentPlaceHolder1_TabConsulta_TabPanel2_hfEditarEmail').value = "N";
        }

        Tb = $get("<%=CERbtnChangeMail.ClientID%>");
        Tb.click();
    }

</script> 
<style type="text/css">
    .style4
    {
        width: 82px;
    }
    .style6
    {
        text-align: left;
        color: #000;
        width: 80px;
    }
    .style7
    {
        width: 80px;
    }
    .style8
    {
        text-align: left;
        color: #000;
        width: 82px;
    }
    .style12
    {
    }
    .style13
    {
        text-align: left;
        color: #000;
        width: 550px;
	/*color : #660000;*/
    }
    .style16
    {
        text-align: left;
        color: #000;
        width: 24px;
    }
    .style17
    {
        width: 24px;
    }
    .style18
    {
        text-align: left;
        color: #000;
        width: 88px;
    }
    .style21
    {
        width: 88px;
    }
    .auto-style1 {
        width: 82px;
        height: 28px;
    }
    .auto-style2 {
        text-align: left;
        color: #000;
        width: 167px;
    }
    .auto-style3 {
        border: solid 1px #CCC;
        background-color: #EBEBEB;
        height: 172px;
    }
    .auto-style7 {
        text-align: right;
        font-size: 11px;
        width: 70px;
        height: 26px;
    }
    .auto-style8 {
        width: 100px;
        height: 26px;
    }
    .auto-style9 {
        text-align: right;
        font-size: 11px;
        width: 83px;
        height: 26px;
    }
    .auto-style10 {
        width: 206px;
        height: 26px;
    }
    .auto-style11 {
        width: 56px;
        height: 26px;
    }
    .auto-style12 {
        width: 164px;
        height: 26px;
    }
    .auto-style13 {
        height: 26px;
    }
    .auto-style14 {
        text-align: left;
        color: #000;
        width: 126px;
        height: 19px;
    }
    .auto-style15 {
        text-align: left;
        color: #000;
        height: 19px;
    }
    .auto-style16 {
        text-align: left;
        color: #000;
        width: 71px;
        height: 19px;
    }
    .auto-style17 {
        width: 126px;
    }
    .auto-style18 {
        text-align: left;
        color: #000;
        width: 126px;
    }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
    <br />
    <cc1:tabcontainer id="TabConsulta" runat="server" activetabindex="0" 
        OnClientActiveTabChanged="ActiveTabChanged" Width="100%" ScrollBars="Horizontal"><cc1:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel1"><HeaderTemplate>
B�squeda
</HeaderTemplate>
        <ContentTemplate>
                <asp:UpdatePanel id="upParBuscar" runat="server"><contenttemplate>
                       <TABLE style="WIDTH: 887px" class="MarcoTabla" cellSpacing=4>
                           <TBODY>
                               <TR>
                                   <TD class="auto-style7">
                                       <asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" __designer:wfdid="w155" SetFocusOnError="True" ControlToValidate="ddlEntidad" ValidationGroup="BUS" ErrorMessage="Seleccione una Entidad" InitialValue="0" Enabled="False">*</asp:RequiredFieldValidator>
                                       Entidad:</TD>
                                   <TD class="auto-style8">
                                       <asp:DropDownList id="ddlEntidad" runat="server"  ValidationGroup="BUS" OnSelectedIndexChanged="ddlEntidad_SelectedIndexChanged" Width="169px" AppendDataBoundItems="True" AutoPostBack="True">
                                           <asp:ListItem Enabled="False" Value="0">-- Seleccionar--</asp:ListItem>
                                       </asp:DropDownList>
                                   </TD>
                                   <TD class="auto-style9">Producto:</TD>
                                   <TD class="auto-style10">
                                       <asp:DropDownList id="ddlProducto" runat="server"  Width="197px" AppendDataBoundItems="True">
                                           <asp:ListItem Value="0">--Todos--</asp:ListItem>
                                       </asp:DropDownList>
                                   </TD>
                                   <TD class="auto-style11"></TD>
                                   <TD class="auto-style12">
                                       <asp:CheckBox id="chkAse" runat="server" __designer:wfdid="w158" Text="Mostrar Asegurado">
                                       </asp:CheckBox>
                                   </TD>
                                   <TD class="auto-style13"></TD>
                                   <tr>
                                       <td class="TilCeldaD" style="WIDTH: 70px">Buscar por:</td>
                                       <td style="WIDTH: 100px">
                                           <asp:DropDownList ID="ddlBuscarx" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBuscarx_SelectedIndexChanged" Width="169px">
                                               <asp:ListItem Selected="True" Value="NDocumento">N� Documento</asp:ListItem>
                                               <asp:ListItem Value="NCertificado">N� Certificado</asp:ListItem>
                                               <asp:ListItem Value="NCuenta">N� Cuenta</asp:ListItem>
                                               <asp:ListItem Value="NPlaca">N� Placa</asp:ListItem>
                                               <asp:ListItem Value="ApeNom">Apellidos y/o Nombres</asp:ListItem>
                                               <asp:ListItem Value="RAIZ">N� Raiz</asp:ListItem>
                                           </asp:DropDownList>
                                       </td>
                                       <td class="TilCeldaD" style="WIDTH: 83px">
                                           <asp:Label ID="lblBuscarx" runat="server" Text="N� Documento:"></asp:Label>
                                       </td>
                                       <td style="WIDTH: 206px">
                                           <asp:TextBox ID="txtParametro" runat="server" ValidationGroup="BUS" Width="186px"></asp:TextBox>
                                           <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  ControlToValidate="txtParametro" ErrorMessage="Ingrese el par�metro" SetFocusOnError="True" ValidationGroup="BUS">*</asp:RequiredFieldValidator>
                                       </td>
                                       <td class="TilCeldaD" style="WIDTH: 56px">
                                           <asp:Label ID="lblNombres" runat="server" Text="Nombres:" Visible="False"></asp:Label>
                                       </td>
                                       <td style="WIDTH: 164px">
                                           <asp:TextBox ID="txtNombres" runat="server" Visible="False" Width="186px"></asp:TextBox>
                                       </td>
                                       <td>
                                           <asp:Button ID="btnBuscar" runat="server" Text="Buscar" ValidationGroup="BUS" Width="54px"></asp:Button>
                                       </td>
                                   </tr>
                               </TR>
                           </TBODY>
                       </TABLE><asp:ValidationSummary id="ValidationSummary4" runat="server"  ValidationGroup="BUS" ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary> 
</contenttemplate>
</asp:UpdatePanel>


            <br />
            <asp:UpdatePanel id="upLista" runat="server" UpdateMode="Conditional"><contenttemplate>
<asp:GridView id="gvBusqueda" runat="server" OnSelectedIndexChanged="gvBusqueda_SelectedIndexChanged" Width="887px" OnPageIndexChanging="gvBusqueda_PageIndexChanging" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="IdCertificado,NumCertificado,IdProducto">
<RowStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" Height="23px"></RowStyle>
<Columns>
<asp:TemplateField HeaderText="N&#186; Certificado"><EditItemTemplate>
<asp:TextBox id="TextBox2" runat="server" Text='<%# Bind("NumCertificado") %>'></asp:TextBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="Label2" runat="server" SkinID="lblSF" Text='<%# Bind("NumCertificado") %>'></asp:Label> 
</ItemTemplate>

<ItemStyle HorizontalAlign="Center" Width="90px"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Producto"><EditItemTemplate>
<asp:TextBox id="TextBox1" runat="server" Text='<%# Bind("Producto") %>'></asp:TextBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="Label1" runat="server" SkinID="lblSF" Text='<%# Bind("Producto") %>'></asp:Label> 
</ItemTemplate>

<ItemStyle Width="150px"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Tipo Doc."><EditItemTemplate>
<asp:TextBox id="TextBox4" runat="server" Text='<%# Bind("TipoDocumento") %>'></asp:TextBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="Label4" runat="server" SkinID="lblSF" Text='<%# Bind("TipoDocumento") %>'></asp:Label> 
</ItemTemplate>

<ItemStyle HorizontalAlign="Center" Width="80px"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="N&#186; Doc."><EditItemTemplate>
<asp:TextBox id="TextBox5" runat="server" Text='<%# Bind("CcCliente") %>'></asp:TextBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="Label5" runat="server" SkinID="lblSF" Text='<%# Bind("CcCliente") %>'></asp:Label> 
</ItemTemplate>

<ItemStyle HorizontalAlign="Center" Width="80px"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Tipo"><EditItemTemplate>
<asp:TextBox id="TextBox6" runat="server" Text='<%# Bind("Tipo") %>'></asp:TextBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="Label6" runat="server" SkinID="lblSF" Text='<%# Bind("Tipo") %>'></asp:Label> 
</ItemTemplate>

<ItemStyle HorizontalAlign="Center" Width="110px"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Apellidos"><EditItemTemplate>
<asp:TextBox id="TextBox8" runat="server" Text='<%# Bind("Apellido1") %>'></asp:TextBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="Label8" runat="server" SkinID="lblSF" Text='<%# Bind("Apellido1") %>'></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Nombres"><EditItemTemplate>
<asp:TextBox id="TextBox7" runat="server" Text='<%# Bind("Nombre1") %>'></asp:TextBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="Label7" runat="server" SkinID="lblSF" Text='<%# Bind("Nombre1") %>'></asp:Label> 
</ItemTemplate>

<ItemStyle Width="130px"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Estado"><EditItemTemplate>
<asp:TextBox id="TextBox3" runat="server" Text='<%# Bind("Estado") %>'></asp:TextBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="Label3" runat="server" SkinID="lblSF" Text='<%# Bind("Estado") %>'></asp:Label> 
</ItemTemplate>

<ItemStyle HorizontalAlign="Center" Width="60px"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/img/seleccionar.gif" CausesValidation="False" Text="Select" CommandName="Select" ToolTip="Seleccionar"></asp:ImageButton> 
</ItemTemplate>

<ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
</asp:TemplateField>
</Columns>
<EmptyDataTemplate>
No se encontraron registros...
</EmptyDataTemplate>
</asp:GridView> 
</contenttemplate>
<triggers>
<asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
</triggers>
</asp:UpdatePanel>


        
</ContentTemplate>

</cc1:TabPanel>
<cc1:TabPanel runat="server" HeaderText="TabPanel2" ID="TabPanel2"><HeaderTemplate>
Certificado
</HeaderTemplate>
    <ContentTemplate>
        <asp:UpdatePanel id="upCertificado" runat="server" UpdateMode="Conditional">
            <contenttemplate>
<TABLE style="MARGIN: auto; WIDTH: 887px" cellSpacing=7 cellPadding=0 border=0>
                           <TBODY>
                               <TR>
                                   <TD class="MarcoTabla">
                                       <table style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0>
                                           <tr>
                                               <td class="TablaTitulo">Informaci�n del Titular</td>
                                           </tr>
                                       </table>
                                       <table style="width:100%;" cellspacing="4">
                                           <tr>
                                               <td class="CampoCelda">Tipo Documento</td>
                                               <td class="CampoCelda" style="width:100px;">N� Documento </td>
                                               <td class="CampoCelda" colspan="3">Apellido Paterno</td>
                                               <td class="CampoCelda" colspan="2" style="width:100px;">Apellido Materno</td>
                                               <td class="CampoCelda" style="width:100px;">Primer Nombre</td>
                                               <td class="CampoCelda">Segundo Nombre</td>
                                           </tr>
                                           <tr>
                                               <td>
                                                   <asp:DropDownList ID="ddlITTipoDoc" runat="server" CssClass="ddlDisable" Enabled="False" Width="114px">
                                                   </asp:DropDownList>
                                               </td>
                                               <td>
                                                   <asp:TextBox ID="txtITNroDoc" runat="server" ReadOnly="True" SkinID="txtDisable" Width="106px"></asp:TextBox>
                                               </td>
                                               <td colspan="3">
                                                   <asp:TextBox ID="txtITApellido1" runat="server" ReadOnly="True" SkinID="txtDisable" Width="150px"></asp:TextBox>
                                               </td>
                                               <td colspan="2">
                                                   <asp:TextBox ID="txtITApellido2" runat="server" ReadOnly="True" SkinID="txtDisable" Width="150px"></asp:TextBox>
                                               </td>
                                               <td>
                                                   <asp:TextBox ID="txtITNombre1" runat="server" ReadOnly="True" SkinID="txtDisable" Width="150px"></asp:TextBox>
                                               </td>
                                               <td>
                                                   <asp:TextBox ID="txtITNombre2" runat="server" ReadOnly="True" SkinID="txtDisable" Width="135px"></asp:TextBox>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td class="CampoCelda" colspan="2">Direccion Particular / Calle </td>
                                               <td class="CampoCelda">N�</td>
                                               <td class="CampoCelda">Piso</td>
                                               <td class="CampoCelda">Dpto.</td>
                                               <td class="CampoCelda">C. Postal</td>
                                               <td class="CampoCelda" colspan="3">Localidad / Provincia</td>
                                           </tr>
                                           <tr>
                                               <td colspan="2">
                                                   <asp:TextBox ID="txtITDirecPar" runat="server" ReadOnly="True" SkinID="txtDisable" Width="230px"></asp:TextBox>
                                               </td>
                                               <td>
                                                   <asp:TextBox ID="txtITDirNumero" runat="server" ReadOnly="True" SkinID="txtDisable" Width="50px"></asp:TextBox>
                                               </td>
                                               <td>
                                                   <asp:TextBox ID="txtITDirPiso" runat="server" ReadOnly="True" SkinID="txtDisable" Width="37px"></asp:TextBox>
                                               </td>
                                               <td>
                                                   <asp:TextBox ID="txtITDirDpto" runat="server" ReadOnly="True" SkinID="txtDisable" Width="37px"></asp:TextBox>
                                               </td>
                                               <td>
                                                   <asp:TextBox ID="txtITDirCPostal" runat="server" ReadOnly="True" SkinID="txtDisable" Width="50px"></asp:TextBox>
                                               </td>
                                               <td colspan="3">
                                                   <asp:TextBox ID="txtITDirUbigeo" runat="server" ReadOnly="True" SkinID="txtDisable" Width="400px"></asp:TextBox>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td class="CampoCelda" colspan="2">Email</td>
                                               <td class="CampoCelda">Cod. �rea</td>
                                               <td class="CampoCelda" colspan="2">N� Tel�fono</td>
                                               <td class="CampoCelda">Cod. �rea</td>
                                               <td class="CampoCelda">Otro Tel�fono</td>
                                               <td class="CampoCelda">Celular</td>
                                               <td></td>
                                           </tr>
                                           <tr>
                                               <td colspan="2">
                                                   <asp:TextBox ID="txtITEmail" runat="server" ReadOnly="True" SkinID="txtDisable" Width="230px"></asp:TextBox>
                                               </td>
                                               <td>
                                                   <asp:TextBox ID="txtITCAreaTelefono" runat="server" ReadOnly="True" SkinID="txtDisable" Width="50px"></asp:TextBox>
                                               </td>
                                               <td colspan="2">
                                                   <asp:TextBox ID="txtITTelefono" runat="server" ReadOnly="True" SkinID="txtDisable" Width="87px"></asp:TextBox>
                                               </td>
                                               <td>
                                                   <asp:TextBox ID="txtITCAreaOtroTelf" runat="server" ReadOnly="True" SkinID="txtDisable" Width="50px"></asp:TextBox>
                                               </td>
                                               <td>
                                                   <asp:TextBox ID="txtITOtroTelefono" runat="server" ReadOnly="True" SkinID="txtDisable" Width="87px"></asp:TextBox>
                                               </td>
                                               <td>
                                                   <asp:TextBox ID="txtITCelular" runat="server" ReadOnly="True" SkinID="txtDisable" Width="87px"></asp:TextBox>
                                               </td>
                                               <td></td>
                                           </tr>
                                           <tr>
                                               <td colspan="2">
                                                   <asp:CheckBox ID="chk_envioMailPoliza" runat="server" Checked="True" Enabled="False" onclick="EmailPoliza()" Text="Recibe p�liza v�a mail" />
                                               </td>
                                               <td>&nbsp;</td>
                                               <td colspan="2">&nbsp;</td>
                                               <td>&nbsp;</td>
                                               <td>&nbsp;</td>
                                               <td>&nbsp;</td>
                                               <td>&nbsp;</td>
                                           </tr>
                                       </table>
                                       <table border="0" cellpadding="0" cellspacing="0" style="WIDTH: 100%">
                                           <tbody>
                                               <tr>
                                                   <td class="TablaTitulo" colspan="5">Datos del Certificado </td>
                                               </tr>
                                           </tbody>
                                       </table>
                                       <table cellspacing="4" style="width:100%;" border="0" cellpadding="0">
                                           <tbody>
                                               <tr>
                                                   <td>N� Certificado BAIS</td>
                                                   <td colspan="3">Sucursal</td>
                                                   <td colspan="2">Vendedor</td>
                                                   <td>Estado</td>
                                                   <td>F. Venta</td>
                                                   <td>F. Digitaci�n</td>
                                               </tr>
                                               <tr>
                                                   <td>
                                                       <asp:TextBox ID="txtDCNroCertificado" runat="server" ReadOnly="True" SkinID="txtDisable" Width="123px"></asp:TextBox>
                                                   </td>
                                                   <td colspan="3">
                                                       <asp:TextBox ID="txtDCAgenciaNombre" runat="server" ReadOnly="True" SkinID="txtDisable" Width="210px"></asp:TextBox>
                                                   </td>
                                                   <td colspan="2">
                                                       <asp:TextBox ID="txtDCVendedorNombre" runat="server" ReadOnly="True" SkinID="txtDisable" Width="214px"></asp:TextBox>
                                                   </td>
                                                   <td>
                                                       <asp:DropDownList ID="ddlDCEstadoCer" runat="server" CssClass="ddlDisable" Enabled="False" Width="83px">
                                                       </asp:DropDownList>
                                                   </td>
                                                   <td style="WIDTH: 71px">
                                                       <asp:TextBox ID="txtDCFechaVenta" runat="server" ReadOnly="True" SkinID="txtDisable" Width="55px"></asp:TextBox>
                                                   </td>
                                                   <td>
                                                       <asp:TextBox ID="txtDCFechaDigitacion" runat="server" ReadOnly="True" SkinID="txtDisable" Width="102px"></asp:TextBox>
                                                   </td>
                                               </tr>
                                               <tr>
                                                   <td class="CampoCelda">N� Certificado Aseguradora</td>
                                                   <td class="CampoCelda" style="WIDTH: 92px">Inicio Vigencia</td>
                                                   <td class="CampoCelda" style="WIDTH: 63px">Fin Vigencia</td>
                                                   <td class="CampoCelda" style="WIDTH: 70px">Sol. Anulaci�n</td>
                                                   <td class="CampoCelda" style="WIDTH: 68px">Efectuar Anu.</td>
                                                   <td class="CampoCelda" style="WIDTH: 155px">Usuario Mod.</td>
                                                   <td class="CampoCelda" colspan="3">Motivo Anulaci�n<asp:RequiredFieldValidator ID="rfvddlDCMotAnulacion" runat="server" __designer:wfdid="w77" ControlToValidate="ddlDCMotAnulacion" Display="None" Enabled="False" ErrorMessage="Seleccione el Motivo de Anulaci�n" InitialValue="0" SetFocusOnError="True" ValidationGroup="CER">*</asp:RequiredFieldValidator>
                                                   </td>
                                               </tr>
                                               <tr>
                                                   <td class="auto-style17">
                                                       <asp:TextBox ID="txtDCNroCertificadoAseg" runat="server" SkinID="txtDisable" Width="123px"></asp:TextBox>
                                                   </td>
                                                   <td style="WIDTH: 92px">
                                                       <asp:TextBox ID="txtDCInicioVigencia" runat="server" ReadOnly="True" SkinID="txtDisable" Width="60px"></asp:TextBox>
                                                   </td>
                                                   <td style="WIDTH: 63px">
                                                       <asp:TextBox ID="txtDCFinVigencia" runat="server" ReadOnly="True" SkinID="txtDisable" Width="55px"></asp:TextBox>
                                                   </td>
                                                   <td style="WIDTH: 70px">
                                                       <asp:TextBox ID="txtDCSolAnulacion" runat="server" ReadOnly="True" SkinID="txtDisable" Width="63px"></asp:TextBox>
                                                   </td>
                                                   <td style="WIDTH: 68px">
                                                       <asp:TextBox ID="txtDCEfectuarAnu" runat="server" ReadOnly="True" SkinID="txtDisable" Width="59px"></asp:TextBox>
                                                   </td>
                                                   <td style="WIDTH: 155px">
                                                       <asp:TextBox ID="txtDCUsuarioMod" runat="server" ReadOnly="True" SkinID="txtDisable" Width="137px"></asp:TextBox>
                                                   </td>
                                                   <td colspan="3">
                                                       <asp:DropDownList ID="ddlDCMotAnulacion" runat="server" AppendDataBoundItems="True" CssClass="ddlDisable" Enabled="False" ValidationGroup="CER" Width="273px">
                                                       </asp:DropDownList>
                                                   </td>
                                               </tr>
                                               <tr>
                                                   <td class="CampoCelda">Ra�z</td>
                                                   <td class="CampoCelda" colspan="3">Observaciones</td>
                                                   <td colspan="2">
                                                       <asp:Label ID="lblEstadoVen" runat="server" ForeColor="#009933"></asp:Label>
                                                   </td>
                                                   <td></td>
                                                   <td style="WIDTH: 71px"></td>
                                                   <td></td>
                                               </tr>
                                               <tr>
                                                   <td class="CampoCelda" style="vertical-align: top;">
                                                       <asp:TextBox ID="txtDCNroCertificadoB" runat="server" SkinID="txtDisable" Width="123px"></asp:TextBox>
                                                   </td>
                                                   <td colspan="8">
                                                       <asp:TextBox ID="txtDCObs" runat="server" CssClass="txtTextoD" ReadOnly="True" Rows="2" SkinID="txtDisable" TextMode="MultiLine" Width="720px"></asp:TextBox>
                                                   </td>
                                               </tr>
                                           </tbody>
                                       </table>
                                       <table border="0" cellpadding="0" cellspacing="0" style="WIDTH: 100%">
                                           <tr>
                                               <td class="TablaTitulo">Informaci�n Adicional del Titular</td>
                                           </tr>
                                       </table>
                                       <table border="0" cellpadding="0" cellspacing="4" style="width:100%;">
                                           <tr>
                                               <td class="CampoCelda" style="width:117px;">Estado Civil</td>
                                               <td class="CampoCelda" style="width:107px;">Sexo</td>
                                               <td class="CampoCelda" style="width:70px;">Fecha Nac.<asp:CompareValidator ID="cvtxtITFechaNac" runat="server" __designer:wfdid="w86" ControlToValidate="txtITFechaNac" Display="None" Enabled="False" ErrorMessage="La Fecha de Nacimiento no contiene un formato v�lido." Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="CER">*</asp:CompareValidator>
                                               </td>
                                               <td class="CampoCelda" style="width:40px;">Edad</td>
                                               <td class="CampoCelda" style="width:100px;">N� Cuil/Cuit/CDI</td>
                                               <td class="CampoCelda" style="width:150px;">Lugar de Nacimiento</td>
                                               <td style="width:250px;"></td>
                                           </tr>
                                           <tr>
                                               <td style="width: 117px">
                                                   <asp:DropDownList ID="ddlITEstadoCivil" runat="server" AppendDataBoundItems="True" CssClass="ddlDisable" Enabled="False" Width="115px">
                                                   </asp:DropDownList>
                                               </td>
                                               <td style="width: 107px">
                                                   <asp:DropDownList ID="ddlITSexo" runat="server" CssClass="ddlDisable" Enabled="False" Width="105px">
                                                       <asp:ListItem Value="0">-------------</asp:ListItem>
                                                       <asp:ListItem Value="M">Masculino</asp:ListItem>
                                                       <asp:ListItem Value="F">Femenino</asp:ListItem>
                                                   </asp:DropDownList>
                                               </td>
                                               <td>
                                                   <asp:TextBox ID="txtITFechaNac" runat="server" ReadOnly="True" SkinID="txtDisable" Width="60px"></asp:TextBox>
                                               </td>
                                               <td>
                                                   <asp:TextBox ID="txtITEdad" runat="server" ReadOnly="True" SkinID="txtDisable" Width="37px"></asp:TextBox>
                                               </td>
                                               <td>
                                                   <asp:TextBox ID="txtITCUIT" runat="server" MaxLength="11" ReadOnly="True" SkinID="txtDisable" Width="90px"></asp:TextBox>
                                               </td>
                                               <td colspan="2">
                                                   <asp:TextBox ID="txtITNacimiento" runat="server" ReadOnly="True" SkinID="txtDisable" Width="400px"></asp:TextBox>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td class="CampoCelda" colspan="2" style="height: 17px;">Nacionalidad</td>
                                               <td class="CampoCelda" colspan="3" style="width:150px; height: 17px;">Profesi�n</td>
                                               <td id="lb_condIva" runat="server" class="CampoCelda" colspan="2" style="width:150px; height: 17px;">Condici�n IVA</td>
                                           </tr>
                                           <tr>
                                               <td colspan="2">
                                                   <asp:TextBox ID="txtITNacionalidad" runat="server"  ReadOnly="True" SkinID="txtDisable" Width="225px"></asp:TextBox>
                                               </td>
                                               <td colspan="3">
                                                   <asp:TextBox ID="txtITProfesion" runat="server"  ReadOnly="True" SkinID="txtDisable" Width="205px"></asp:TextBox>
                                               </td>
                                               <td colspan="2">
                                                   <asp:DropDownList ID="ddIA_CondicionIva" runat="server" CssClass="ddlDisable" Enabled="False" Width="250px">
                                                       <asp:ListItem Value="">Seleccione</asp:ListItem>
                                                       <asp:ListItem Value="Monotributo">Monotributo</asp:ListItem>
                                                       <asp:ListItem Value="Responsable Inscripto">Responsable Inscripto</asp:ListItem>
                                                   </asp:DropDownList>
                                               </td>
                                           </tr>
                                       </table>
                                       <table border="0" cellpadding="0" cellspacing="0" style="WIDTH: 100%">
                                           <tr>
                                               <td class="TablaTitulo">Domicilio del Riesgo Asegurado</td>
                                           </tr>
                                       </table>
                                       <table border="0" cellpadding="0" cellspacing="4" style="width:100%;">
                                           <tr>
                                               <td class="CampoCelda" style="width:228px;">Direccion Particular / Calle </td>
                                               <td class="CampoCelda">N�</td>
                                               <td class="CampoCelda">Piso</td>
                                               <td class="CampoCelda">Dpto.</td>
                                               <td class="CampoCelda">C. Postal</td>
                                               <td class="CampoCelda">Localidad / Provincia </td>
                                           </tr>
                                           <tr>
                                               <td style="width: 228px; height: 14px;">
                                                   <asp:TextBox ID="txtITDRADireccion" runat="server" ReadOnly="True" SkinID="txtDisable" Width="230px"></asp:TextBox>
                                               </td>
                                               <td style="height: 14px;">
                                                   <asp:TextBox ID="txtITDRADirNumero" runat="server" ReadOnly="True" SkinID="txtDisable" Width="50px"></asp:TextBox>
                                               </td>
                                               <td style="height: 14px">
                                                   <asp:TextBox ID="txtITDRADirPiso" runat="server"  ReadOnly="True" SkinID="txtDisable" Width="37px"></asp:TextBox>
                                               </td>
                                               <td style="height: 14px">
                                                   <asp:TextBox ID="txtITDRADirDpto" runat="server"  ReadOnly="True" SkinID="txtDisable" Width="37px"></asp:TextBox>
                                               </td>
                                               <td style="height: 14px">
                                                   <asp:TextBox ID="txtITDRADirCPostal" runat="server" ReadOnly="True" SkinID="txtDisable" Width="50px"></asp:TextBox>
                                               </td>
                                               <td style="height: 14px">
                                                   <asp:TextBox ID="txtITDRADirUbigeo" runat="server"  ReadOnly="True" SkinID="txtDisable" Width="400px"></asp:TextBox>
                                               </td>
                                           </tr>
                                       </table>
                                       <table border="0" cellpadding="0" cellspacing="0" style="WIDTH: 100%">
                                           <tbody>
                                               <tr>
                                                   <td class="TablaTitulo" colspan="5">Informaci�n de Pago</td>
                                               </tr>
                                           </tbody>
                                       </table>
                                       <table border="0" cellpadding="0" cellspacing="4" style="width: 100%">
                                           <tr>
                                               <td class="CampoCelda">Producto</td>
                                               <td class="CampoCelda">&nbsp;</td>
                                               <td class="CampoCelda">Plan</td>
                                               <td class="CampoCelda">&nbsp;</td>
                                               <td class="CampoCelda" style="width: 73px">Frecuencia</td>
                                               <td class="CampoCelda">Moneda Premio</td>
                                               <td class="CampoCelda">Premio</td>
                                               <td class="CampoCelda">Suma Aseg.</td>
                                           </tr>
                                           <tr>
                                               <td colspan="2">
                                                   <asp:TextBox ID="txtIPProducto" runat="server" ReadOnly="True" SkinID="txtDisable" Width="235px"></asp:TextBox>
                                               </td>
                                               <td class="style12" colspan="2">
                                                   <asp:TextBox ID="txtIPPlan" runat="server" ReadOnly="True" SkinID="txtDisable" Width="235px"></asp:TextBox>
                                               </td>
                                               <td style="width: 73px">
                                                   <asp:TextBox ID="txtIPFrecPago" runat="server" ReadOnly="True" SkinID="txtDisable" Width="70px"></asp:TextBox>
                                               </td>
                                               <td>
                                                   <asp:TextBox ID="txtIPModenaPrima" runat="server" ReadOnly="True" SkinID="txtDisable" Width="93px"></asp:TextBox>
                                               </td>
                                               <td>
                                                   <asp:TextBox ID="txtIPPrimaBruta" runat="server" ReadOnly="True" SkinID="txtDisable" Width="85px"></asp:TextBox>
                                               </td>
                                               <td>
                                                   <asp:TextBox ID="txtIPMontoAseg" runat="server" ReadOnly="True" SkinID="txtDisable" Width="85px"></asp:TextBox>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td class="CampoCelda">Medio Pago</td>
                                               <td class="CampoCelda">N�mero</td>
                                               <td class="CampoCelda">CBU</td>
                                               <td class="CampoCelda">&nbsp;</td>
                                               <td class="CampoCelda">&nbsp;</td>
                                               <td>&nbsp;</td>
                                               <td>&nbsp;</td>
                                               <td>&nbsp;</td>
                                           </tr>
                                           <tr>
                                               <td class="CampoCelda">
                                                   <asp:DropDownList ID="ddlIPMedioPago" runat="server" CssClass="ddlDisable" Enabled="False" Width="111px">
                                                   </asp:DropDownList>
                                               </td>
                                               <td class="CampoCelda">
                                                   <asp:TextBox ID="txtIPCatTarj" runat="server" ReadOnly="True" SkinID="txtDisable" Width="120px"></asp:TextBox>
                                               </td>
                                               <td class="CampoCelda">
                                                   <asp:TextBox ID="txtITTCBU" runat="server" ReadOnly="True" SkinID="txtDisable" Width="150px"></asp:TextBox>
                                               </td>
                                               <td class="CampoCelda">&nbsp;</td>
                                               <td style="width: 73px">&nbsp;</td>
                                               <td>&nbsp;</td>
                                               <td>&nbsp;</td>
                                               <td>&nbsp;</td>
                                           </tr>
                                       </table>
                                       <asp:HiddenField ID="hfEditarEmail" runat="server" />
                                       <asp:HiddenField ID="hfIDProducto" runat="server" />
                                       <asp:HiddenField ID="txtDCIDAgencia" runat="server"  />
                                       <asp:HiddenField ID="txtDCIDVendedor" runat="server"  />
                                       <asp:HiddenField ID="hfITIDCiudad" runat="server" />
                                       <asp:HiddenField ID="hfIDAIDCiudad" runat="server" />
                                       <asp:Button ID="CERbtnChangeMail" runat="server"  CausesValidation="False" onclick="CERbtnChangeMail_Click" CssClass="BotonOculto" Text="Change" Width="70px" />
                                       <asp:Button ID="CERbtnModificar" runat="server"  CausesValidation="False" onclick="CERbtnModificar_Click" Text="Modificar" Width="70px" />
                                       <asp:Button ID="CERbtnAnular" runat="server" CausesValidation="False" onclick="CERbtnAnular_Click" Text="Anular" Width="65px" />
                                       <asp:Button ID="CERbtnActivar" runat="server"  CausesValidation="False" onclick="CERbtnActivar_Click" Text="Activar" Width="64px" />
                                       <asp:Button ID="CERbtnGrabar" runat="server"  onclick="CERbtnGrabar_Click" Text="Grabar" ValidationGroup="CER" Width="61px" />
                                       &nbsp;<asp:Button ID="CERbtnCancelar" runat="server"  CausesValidation="False" onclick="CERbtnCancelar_Click" Text="Cancelar" Width="61px" />
                                       &nbsp;<asp:Button ID="CERbtnImprimir" runat="server" CausesValidation="False" Text="Imprimir" Width="65px" />
                                       &nbsp;<asp:HiddenField ID="hfOperacion" runat="server" />
                                       &nbsp;<asp:HiddenField ID="hfIDTipoProducto" runat="server" />
                                       &nbsp;<asp:HiddenField ID="hfCodAsegurador" runat="server" />
                                       &nbsp;<asp:ValidationSummary ID="ValidationSummary1" runat="server"  ShowMessageBox="True" ShowSummary="False" ValidationGroup="CER" />
                                       <cc1:MaskedEditExtender ID="meetxtITFechaNac" runat="server"  ClearTextOnInvalid="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtITFechaNac">
                                       </cc1:MaskedEditExtender>
                                   </TD>
                               </TR>
                           </TBODY>
                       </TABLE>
</contenttemplate>
            <triggers>
<asp:AsyncPostBackTrigger ControlID="btnActCertificado" EventName="Click"></asp:AsyncPostBackTrigger>
</triggers>
        </asp:UpdatePanel>
        <br />
        &nbsp;<asp:Button ID="btnActCertificado" runat="server" SkinID="BotonOculto" />
        
    
</ContentTemplate>

</cc1:TabPanel>
<cc1:TabPanel runat="server" HeaderText="TabPanel3" ID="TabPanel3"><HeaderTemplate>
Asegurados
</HeaderTemplate>
    <ContentTemplate>
        <asp:UpdatePanel ID="upAsegurados" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
<TABLE style="MARGIN: auto; WIDTH: 887px" id="TABLE1" cellSpacing=7 cellPadding=0 border=0>
    <TBODY><TR><TD colSpan=1>
        <asp:GridView id="gvAsegurado" runat="server" AutoGenerateColumns="False" DataKeyNames="IdCertificado,Consecutivo" OnSelectedIndexChanged="gvAsegurado_SelectedIndexChanged" Width="877px">
            <Columns>
                <asp:BoundField DataField="IdCertificado" Visible="False" HeaderText="IdCertificado"></asp:BoundField>
                <asp:BoundField DataField="Consecutivo" Visible="False" HeaderText="Consecutivo"></asp:BoundField>
                <asp:BoundField DataField="Nombre1" HeaderText="Nombre Completo">
                    <ItemStyle Width="280px"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="TipoDocumento" HeaderText="Tipo Documento">
                    <ItemStyle Width="110px" HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="ccAseg" HeaderText="N&#186; Documento">
                    <ItemStyle Width="110px" HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="Direccion" HeaderText="Direccion"></asp:BoundField>
                <asp:BoundField DataField="Estado" HeaderText="Estado">
                    <ItemStyle Width="80px" HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:TemplateField ShowHeader="False">
                    <ItemStyle Width="30px" HorizontalAlign="Center">
                    </ItemStyle>
                    <ItemTemplate>
                        <asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/img/seleccionar.gif" CausesValidation="False" Text="Select" CommandName="Select" ToolTip="Seleccionar">
                        </asp:ImageButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <BR />
        </TD>
        </TR><TR>
        
        <TD class="MarcoTabla" colspan="1">
        
             <table border="0" cellPadding="0" cellSpacing="0" style="WIDTH: 100%">
                 <tbody>
                     <tr>
                         <td class="TablaTitulo" colspan="5">
                             Datos del Asegurado</td>
                     </tr>
                 </tbody>
             </table>
             <table border="0" cellpadding="0" cellspacing="4" style="width:100%;">
                 <tbody>
                     <tr>
                         <td class="CampoCelda" colspan="2" style="HEIGHT: 13px">
                             Parentesco</td>
                         <td class="CampoCelda" colspan="2" style="HEIGHT: 13px">
                             Primer Nombre</td>
                         <td class="CampoCelda" colspan="2" style="HEIGHT: 13px">
                             Seg. Nombre</td>
                         <td class="CampoCelda" colspan="2" style="HEIGHT: 13px">
                             Ape. Paterno</td>
                         <td class="CampoCelda" style="WIDTH: 69px; HEIGHT: 13px">
                             Ape. Materno</td>
                         <td class="CampoCelda" style="HEIGHT: 13px">
                             Fecha Nac.<asp:CompareValidator ID="CompareValidator2" 
                  runat="server" ControlToValidate="ASEtxtFechaNac" 
                  ErrorMessage="Ingrese una Fecha Nacimiento v�lida" Operator="DataTypeCheck" 
                  SetFocusOnError="True" Type="Date" ValidationGroup="ASE">*</asp:CompareValidator>
                         </td>
                         <td class="CampoCelda" style="HEIGHT: 13px; width: 107px;">
                             Tipo Documento</td>
                         <td class="CampoCelda" style="HEIGHT: 13px">
                             N� Documento</td>
                     </tr>
                     <tr>
                         <td colspan="2">
                             <asp:DropDownList ID="ASEddlParentesco" runat="server" 
                  AppendDataBoundItems="True" CssClass="ddlDisable" Enabled="False" Width="106px">
                             </asp:DropDownList>
                         </td>
                         <td colspan="2">
                             <asp:TextBox ID="ASEtxtNombre1" runat="server" ReadOnly="True" 
                  SkinID="txtDisable" Width="95px"></asp:TextBox>
                         </td>
                         <td colspan="2">
                             <asp:TextBox ID="ASEtxtNombre2" runat="server" ReadOnly="True" 
                  SkinID="txtDisable" Width="107px"></asp:TextBox>
                         </td>
                         <td colspan="2">
                             <asp:TextBox ID="ASEtxtApellido1" runat="server" ReadOnly="True" 
                  SkinID="txtDisable" Width="107px"></asp:TextBox>
                         </td>
                         <td style="WIDTH: 69px">
                             <asp:TextBox ID="ASEtxtApellido2" runat="server" ReadOnly="True" 
                  SkinID="txtDisable" Width="107px"></asp:TextBox>
                         </td>
                         <td>
                             <asp:TextBox ID="ASEtxtFechaNac" runat="server" ReadOnly="True" 
                  SkinID="txtDisable" ValidationGroup="ASE" Width="59px"></asp:TextBox>
                         </td>
                         <td style="width: 107px">
                             <asp:DropDownList ID="ASEddlTipoDoc" runat="server" 
                  CssClass="ddlDisable" Enabled="False" Width="110px">
                             </asp:DropDownList>
                         </td>
                         <td>
                             <asp:TextBox ID="ASEtxtNroDoc" runat="server" ReadOnly="True" 
                              SkinID="txtDisable" Width="100px"></asp:TextBox>
                         </td>
                     </tr>
                     <tr>
                         <td class="CampoCelda" colspan="4">
                             Direcci�n / Calle</td>
                         <td class="CampoCelda" style="WIDTH: 17px">
                             N�mero</td>
                         <td class="CampoCelda">
                             Piso</td>
                         <td class="CampoCelda" style="WIDTH: 46px">
                             Dpto.</td>
                         <td class="CampoCelda" style="WIDTH: 46px">
                             C. Postal</td>
                         <td class="CampoCelda" colspan="4">
                             Localidad / Provincia</td>
                     </tr>
                     <tr>
                         <td class="CampoCelda" colspan="4">
                             <asp:TextBox ID="ASEtxtDireccion" runat="server" ReadOnly="True" 
                              SkinID="txtDisable" Width="205px"></asp:TextBox>
                         </td>
                         <td class="CampoCelda" style="WIDTH: 17px">
                             <asp:TextBox ID="ASEtxtDirNumero" runat="server" ReadOnly="True" 
                              SkinID="txtDisable" Width="45px"></asp:TextBox>
                         </td>
                         <td class="CampoCelda">
                             <asp:TextBox ID="ASEtxtDirPiso" runat="server" ReadOnly="True" 
                              SkinID="txtDisable" Width="45px"></asp:TextBox>
                         </td>
                         <td class="CampoCelda" style="WIDTH: 46px">
                             <asp:TextBox ID="ASEtxtDirDpto" runat="server" ReadOnly="True" 
                              SkinID="txtDisable" Width="45px"></asp:TextBox>
                         </td>
                         <td class="CampoCelda" style="WIDTH: 46px">
                             <asp:TextBox ID="ASEtxtDirCPostal" runat="server" ReadOnly="True" 
                              SkinID="txtDisable" Width="45px"></asp:TextBox>
                         </td>
                         <td class="CampoCelda" colspan="4">
                             <asp:TextBox ID="ASEtxtDirUbigeo" runat="server" ReadOnly="True" 
                              SkinID="txtDisable" Width="400px"></asp:TextBox>
                         </td>
                     </tr>
                     <tr>
                         <td class="CampoCelda" colspan="0" style="width: 20px">
                             Cod. �rea</td>
                         <td class="CampoCelda" colspan="2">
                             Tel�fono</td>
                         <td class="CampoCelda">
                             &nbsp;</td>
                         <td class="CampoCelda">
                             &nbsp;</td>
                         <td class="CampoCelda" style="WIDTH: 46px">
                             &nbsp;</td>
                         <td class="CampoCelda" style="WIDTH: 46px">
                             &nbsp;</td>
                         <td class="CampoCelda" style="WIDTH: 69px">
                             &nbsp;</td>
                         <td class="CampoCelda">
                             &nbsp;</td>
                         <td class="CampoCelda" style="width: 107px">
                             &nbsp;</td>
                         <td class="CampoCelda">
                             &nbsp;</td>
                     </tr>
                     <tr>
                         <td colspan="0" style="width: 20px">
                             <asp:TextBox ID="ASEtxtCAreaTelefono" runat="server" ReadOnly="True" 
                  SkinID="txtDisable" Width="59px"></asp:TextBox>
                         </td>
                         <td colspan="2">
                             <asp:TextBox ID="ASEtxtTelefono" runat="server" ReadOnly="True" 
                              SkinID="txtDisable" Width="70px"></asp:TextBox>
                         </td>
                         <td>
                             &nbsp;</td>
                         <td>
                             &nbsp;</td>
                         <td style="WIDTH: 46px">
                             &nbsp;</td>
                         <td style="WIDTH: 46px">
                             &nbsp;</td>
                         <td colspan="1">
                             &nbsp;</td>
                         <td>
                             &nbsp;</td>
                         <td style="width: 107px">
                             &nbsp;</td>
                         <td>
                             &nbsp;</td>
                     </tr>
                     <tr>
                         <td colspan="0" style="width: 20px">
                             &nbsp;</td>
                         <td colspan="0" style="width: 41px">
                             &nbsp;</td>
                         <td>
                             &nbsp;</td>
                         <td>
                             &nbsp;</td>
                         <td style="WIDTH: 46px">
                             &nbsp;</td>
                         <td style="WIDTH: 46px">
                             &nbsp;</td>
                         <td colspan="1">
                             &nbsp;</td>
                         <td>
                             &nbsp;</td>
                         <td style="width: 107px">
                             &nbsp;</td>
                         <td>
                             &nbsp;</td>
                     </tr>
                 </tbody>
             </table>
            </TD>
        </TR>
        <TR>
            <TD class="MarcoTabla" colSpan=1>
                <TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0>
                    <TBODY>
                        <TR>
                            <TD class="TablaTitulo" colSpan=5>
                                Informaci�n Adicional</TD>
                        </TR>
                    </TBODY>
                </TABLE>
                <TABLE style="WIDTH: 100%" cellSpacing=4 cellPadding=0 border=0>
                    <TBODY>
                        <TR>
                            <TD style="WIDTH: 216px; HEIGHT: 13px" class="CampoCelda">
                                Plan</TD>
                            <TD style="WIDTH: 81px; HEIGHT: 13px" class="CampoCelda">
                                &nbsp; Prima</TD>
                            <TD style="HEIGHT: 13px" class="CampoCelda">
                                Observaciones</TD>
                        </TR>
                        <TR>
                            <TD style="VERTICAL-ALIGN: top; WIDTH: 216px" class="CampoCelda">
                                <asp:TextBox id="ASEtxtPlan" runat="server" SkinID="txtDisable" Width="211px" ReadOnly="True"></asp:TextBox>
                            </TD>
                            <TD style="VERTICAL-ALIGN: top; WIDTH: 81px" class="CampoCelda">
                                <asp:TextBox id="ASEtxtPrima" runat="server" SkinID="txtDisable" Width="77px" ReadOnly="True"></asp:TextBox>
                            </TD>
                            <TD class="CampoCelda">
                                <asp:TextBox id="ASEtxtObservacion" runat="server" SkinID="txtDisable" Width="537px" ReadOnly="True" TextMode="MultiLine"></asp:TextBox>
                            </TD>
                        </TR>
                    </TBODY>
                </TABLE>
            </TD>
        </TR>
        <TR>
            <TD style="HEIGHT: 29px">
                &nbsp;<asp:Button ID="ASEbtnModificar" runat="server" CausesValidation="False" 
                    onclick="ASEbtnModificar_Click" Text="Modificar" Width="70px"></asp:Button>
                &nbsp;<asp:Button ID="ASEbtnAnular" runat="server" CausesValidation="False" 
                    onclick="ASEbtnAnular_Click" Text="Anular" Width="65px"></asp:Button>
                &nbsp;<asp:Button ID="ASEbtnGrabar" runat="server" onclick="ASEbtnGrabar_Click" 
                    Text="Grabar" ValidationGroup="ASE" Width="61px">
                </asp:Button>
                &nbsp;<asp:Button ID="ASEbtnCancelar" runat="server" CausesValidation="False" 
                    onclick="ASEbtnCancelar_Click" Text="Cancelar" Width="61px"></asp:Button>
                <asp:HiddenField ID="ASEhfOperacion" runat="server">
                </asp:HiddenField>
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" 
                    ShowMessageBox="True" ShowSummary="False" ValidationGroup="ASE">
                </asp:ValidationSummary>
                <cc1:MaskedEditExtender ID="ASEmeetxtFechaNac" runat="server" 
                    ClearTextOnInvalid="True" Mask="99/99/9999" MaskType="Date" 
                    TargetControlID="ASEtxtFechaNac">
                </cc1:MaskedEditExtender>
            </TD>
        </TR>
    </TBODY></TABLE>
</ContentTemplate>
            <triggers>
<asp:AsyncPostBackTrigger ControlID="btnActAsegurado" EventName="Click"></asp:AsyncPostBackTrigger>
</triggers>
        </asp:UpdatePanel><asp:Button ID="btnActAsegurado" runat="server" SkinID="BotonOculto" />
       
    
</ContentTemplate>

</cc1:TabPanel>

<cc1:TabPanel runat="server" HeaderText="TabPanel4" ID="TabPanel4">
    <HeaderTemplate>
Beneficiarios
</HeaderTemplate>
    <ContentTemplate>
        <asp:UpdatePanel id="upBeneficiario" runat="server" UpdateMode="Conditional">
            <contenttemplate>
                <TABLE style="MARGIN: auto; WIDTH: 887px" id="Table2" cellSpacing=7 cellPadding=0 border=0>
                    <TBODY><TR><TD colSpan=1>
                        <asp:GridView ID="gvBeneficiario" runat="server"  AutoGenerateColumns="False" DataKeyNames="IdBeneficiario,IdCertificado,Consecutivo" OnSelectedIndexChanged="gvBeneficiario_SelectedIndexChanged" Width="870px">
                            <Columns>
                                <asp:BoundField DataField="IdCertificado" HeaderText="IdCertificado" Visible="False"></asp:BoundField>
                                <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo" Visible="False"></asp:BoundField>
                                <asp:BoundField DataField="Nombre1" HeaderText="Nombre Completo">
                                    <ItemStyle Width="280px"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="TipoDocumento" HeaderText="Tipo Documento">
                                    <ItemStyle HorizontalAlign="Center" Width="110px"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ccBenef" HeaderText="N� Documento">
                                    <ItemStyle HorizontalAlign="Center" Width="110px"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="domicilio" HeaderText="Direccion"></asp:BoundField>
                                <asp:BoundField DataField="Estado" HeaderText="Estado">
                                    <ItemStyle HorizontalAlign="Center" Width="80px"></ItemStyle>
                                </asp:BoundField>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" CommandName="Select" ImageUrl="~/img/seleccionar.gif" Text="Select" ToolTip="Seleccionar">
                                        </asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                No se encontraron registros...
                            </EmptyDataTemplate>
                        </asp:GridView>
                        <br />
                        </TD>
                        </TR><TR>
        
                        <TD colSpan=1 class="MarcoTabla">
        
                             <table border="0" cellpadding="0" cellspacing="0" style="WIDTH: 100%">
                                 <tbody>
                                     <tr>
                                         <td class="TablaTitulo" colspan="5">Datos del Beneficiario</td>
                                     </tr>
                                 </tbody>
                             </table>
                             <table border="0" cellpadding="0" cellspacing="4" style="WIDTH: 100%">
                                 <tbody>
                                     <tr>
                                         <td class="CampoCelda" style="WIDTH: 97px; HEIGHT: 13px">Parentesco</td>
                                         <td class="CampoCelda" style="WIDTH: 93px; HEIGHT: 13px">Primer Nombre</td>
                                         <td class="CampoCelda" style="WIDTH: 118px; HEIGHT: 13px">Seg. Nombre</td>
                                         <td class="CampoCelda" style="WIDTH: 108px; HEIGHT: 13px">Ape. Paterno</td>
                                         <td class="CampoCelda" style="WIDTH: 122px; HEIGHT: 13px">Ape. Materno</td>
                                         <td class="CampoCelda" style="HEIGHT: 13px">Fecha Nac.<asp:CompareValidator ID="CompareValidator3" runat="server" __designer:wfdid="w285" ControlToValidate="BENtxtFechaNac" ErrorMessage="Ingrese una Fecha Nacimiento v�lida" Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="BEN">*</asp:CompareValidator>
                                         </td>
                                         <td class="CampoCelda" style="WIDTH: 49px; HEIGHT: 13px">Sexo</td>
                                         <td class="CampoCelda" style="HEIGHT: 13px">Tel�fono</td>
                                     </tr>
                                     <tr>
                                         <td style="WIDTH: 97px">
                                             <asp:DropDownList ID="BENddlParentesco" runat="server"  AppendDataBoundItems="True" CssClass="ddlDisable" Enabled="False" Width="106px">
                                             </asp:DropDownList>
                                         </td>
                                         <td style="WIDTH: 93px">
                                             <asp:TextBox ID="BENtxtNombre1" runat="server"  ReadOnly="True" SkinID="txtDisable" Width="95px"></asp:TextBox>
                                         </td>
                                         <td style="WIDTH: 118px">
                                             <asp:TextBox ID="BENtxtNombre2" runat="server"  ReadOnly="True" SkinID="txtDisable" Width="117px"></asp:TextBox>
                                         </td>
                                         <td style="WIDTH: 108px">
                                             <asp:TextBox ID="BENtxtApellido1" runat="server"  ReadOnly="True" SkinID="txtDisable" Width="121px"></asp:TextBox>
                                         </td>
                                         <td style="WIDTH: 122px">
                                             <asp:TextBox ID="BENtxtApellido2" runat="server"  ReadOnly="True" SkinID="txtDisable" Width="137px"></asp:TextBox>
                                         </td>
                                         <td>
                                             <asp:TextBox ID="BENtxtFechaNac" runat="server"  ReadOnly="True" SkinID="txtDisable" ValidationGroup="BEN" Width="59px"></asp:TextBox>
                                         </td>
                                         <td style="WIDTH: 49px">
                                             <asp:DropDownList ID="BENddlSexo" runat="server"  CssClass="ddlDisable" Enabled="False" Width="72px">
                                                 <asp:ListItem Value="0">-------------</asp:ListItem>
                                                 <asp:ListItem Value="M">Masculino</asp:ListItem>
                                                 <asp:ListItem Value="F">Femenino</asp:ListItem>
                                             </asp:DropDownList>
                                         </td>
                                         <td>
                                             <asp:TextBox ID="BENtxtTelefono" runat="server"  ReadOnly="True" SkinID="txtDisable" Width="60px"></asp:TextBox>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td class="CampoCelda" colspan="2" style="HEIGHT: 13px">Tipo Documento</td>
                                         <td class="CampoCelda" style="WIDTH: 118px; HEIGHT: 13px">N� Documento</td>
                                         <td class="CampoCelda" style="WIDTH: 108px; HEIGHT: 13px">Direcci�n</td>
                                         <td class="CampoCelda" style="WIDTH: 122px; HEIGHT: 13px"></td>
                                         <td class="CampoCelda" colspan="2" style="HEIGHT: 13px">Ocupaci�n</td>
                                         <td class="CampoCelda" style="HEIGHT: 13px">%Paticipaci�n</td>
                                     </tr>
                                     <tr>
                                         <td colspan="2">
                                             <asp:DropDownList ID="BENddlTipoDoc" runat="server"  CssClass="ddlDisable" Enabled="False" Width="216px">
                                             </asp:DropDownList>
                                         </td>
                                         <td style="WIDTH: 118px">
                                             <asp:TextBox ID="BENtxtNroDoc" runat="server"  ReadOnly="True" SkinID="txtDisable" Width="117px"></asp:TextBox>
                                         </td>
                                         <td colspan="2">
                                             <asp:TextBox ID="BENtxtDireccion" runat="server"  ReadOnly="True" SkinID="txtDisable" Width="272px"></asp:TextBox>
                                         </td>
                                         <td colspan="2">
                                             <asp:TextBox ID="BENtxtOcupacion" runat="server"  ReadOnly="True" SkinID="txtDisable" Width="134px"></asp:TextBox>
                                         </td>
                                         <td>
                                             <asp:TextBox ID="BENtxtPorcentaje" runat="server"  MaxLength="3" ReadOnly="True" SkinID="txtDisable" Width="60px"></asp:TextBox>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td class="CampoCelda" colspan="2">Observaciones</td>
                                         <td style="WIDTH: 118px"></td>
                                         <td colspan="2"></td>
                                         <td colspan="2"></td>
                                         <td></td>
                                     </tr>
                                     <tr>
                                         <td colspan="3">
                                             <asp:TextBox ID="BENtxtObservacion" runat="server"  ReadOnly="True" SkinID="txtDisable" TextMode="MultiLine" Width="339px"></asp:TextBox>
                                         </td>
                                         <td colspan="2"></td>
                                         <td colspan="2"></td>
                                         <td></td>
                                     </tr>
                                 </tbody>
                             </table>
                            </TD>
                        </TR>
                        <TR>
                            <TD>&nbsp;<asp:Button id="BENbtnModificar" onclick="BENbtnModificar_Click" runat="server" Width="70px"  Text="Modificar" CausesValidation="False"></asp:Button>
                                <asp:Button id="BENbtnAnular" onclick="BENbtnAnular_Click" runat="server" Width="65px"  Text="Anular" CausesValidation="False"></asp:Button>
                                <asp:Button id="BENbtnGrabar" onclick="BENbtnGrabar_Click" runat="server" Width="61px"  ValidationGroup="BEN" Text="Grabar"></asp:Button>
                                &nbsp;<asp:Button id="BENbtnCancelar" onclick="BENbtnCancelar_Click" runat="server" Width="61px"  Text="Cancelar" CausesValidation="False"></asp:Button>
                                <asp:HiddenField id="BENhfOperacion" runat="server" >
                                </asp:HiddenField>
                                <asp:ValidationSummary id="ValidationSummary3" runat="server"  ValidationGroup="BEN" ShowSummary="False" ShowMessageBox="True">
                                </asp:ValidationSummary>
                                <cc1:MaskedEditExtender id="BENmeetxtFechaNac" runat="server"  ClearTextOnInvalid="True" Mask="99/99/9999" MaskType="Date" TargetControlID="BENtxtFechaNac">
                                </cc1:MaskedEditExtender>
                            </TD>
                        </TR>
                    </TBODY></TABLE>
        </contenttemplate>
            <triggers>
                <asp:AsyncPostBackTrigger ControlID="btnActBeneficiario" EventName="Click"></asp:AsyncPostBackTrigger>
            </triggers>
        </asp:UpdatePanel>
         <asp:Button ID="btnActBeneficiario" runat="server" SkinID="BotonOculto" />
    
</ContentTemplate>

</cc1:TabPanel>

<cc1:TabPanel runat="server" HeaderText="TabPanel5" ID="TabPanel5">
    <HeaderTemplate>
Im�genes
</HeaderTemplate>
    <ContentTemplate>
        <asp:UpdatePanel id="upImagen" runat="server" UpdateMode="Conditional">
            <contenttemplate>
                <TABLE style="WIDTH: 100%" cellSpacing=4><TBODY><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 157px"><asp:ListBox id="lbImagen" runat="server" __designer:wfdid="w490" Width="155px" OnSelectedIndexChanged="lbImagen_SelectedIndexChanged1" AutoPostBack="True" Height="500px"></asp:ListBox></TD><TD style="VERTICAL-ALIGN: top"><DIV style="OVERFLOW: auto; WIDTH: 720px; HEIGHT: 500px"><asp:Image id="imgCer" runat="server" __designer:wfdid="w491" Width="700px" Height="800px" Visible="False"></asp:Image></DIV></TD></TR><TR><TD style="VERTICAL-ALIGN: top" colSpan=2><IFRAME style="WIDTH: 879px; HEIGHT: 84px" id="SubirImagen" name="SubirImagen" src="SubirImagen.aspx" frameBorder="0"></IFRAME></TD></TR></TBODY></TABLE>
            </contenttemplate>
            <triggers>
                <asp:AsyncPostBackTrigger ControlID="btnActImagen" EventName="Click"></asp:AsyncPostBackTrigger>
            </triggers>
        </asp:UpdatePanel>
        <asp:Button ID="btnActImagen" runat="server" SkinID="BotonOculto" />
    
</ContentTemplate>

</cc1:TabPanel>

<cc1:TabPanel runat="server" HeaderText="TabPanel6" ID="TabPanel6">
    <HeaderTemplate>
Cobros
</HeaderTemplate>
    <ContentTemplate>
        <asp:UpdatePanel id="upCobro" runat="server" UpdateMode="Conditional">
            <contenttemplate>
<asp:GridView id="gvCobro" runat="server" OnRowCommand="gvCobro_RowCommand" DataKeyNames="idCobro,idCertificado" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="gvCobro_PageIndexChanging" OnSelectedIndexChanged="gvCobro_SelectedIndexChanged" Width="891px" __designer:wfdid="w500"><Columns>
<asp:BoundField DataField="IdCertificado" HeaderText="IdCertificado" Visible="False"></asp:BoundField>
<asp:BoundField DataField="IdCobro" HeaderText="N&#186; Cuota">
<ItemStyle HorizontalAlign="Center" Width="70px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="MedioPago" HeaderText="Medio Pago">
<ItemStyle HorizontalAlign="Center" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="numeroCuenta" HeaderText="N&#186; Cuenta/Tarjeta">
<ItemStyle HorizontalAlign="Center" Width="200px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ValorMoneda" HeaderText="Valor Cobrado">
<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="FechaProceso" HeaderText="Fecha Cobro" HtmlEncode="False">
<ItemStyle HorizontalAlign="Center" Width="80px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="inicioPeriodo" DataFormatString="{0:d}" HeaderText="Inicio Periodo" HtmlEncode="False">
<ItemStyle HorizontalAlign="Center" Width="80px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="finPeriodo" DataFormatString="{0:d}" HeaderText="Fin Periodo" HtmlEncode="False">
<ItemStyle HorizontalAlign="Center" Width="80px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="EstadoCobro" HeaderText="Estado">
<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/img/ver.gif"  Text="Ver Historial de Cobros" ToolTip="Ver Historial de Cobros" CommandArgument='<%# "Historial" %>' CommandName="Select" CausesValidation="False"></asp:ImageButton> 
</ItemTemplate>

<ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField ShowHeader="False"><ItemTemplate>
<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="~/img/Grid/detalles.gif" Text="Ver Extorno" Visible='<%# IIF((Eval("Extorno").ToString() ="0"),false,true) %>' ToolTip="Ver Extorno" CommandArgument='<%# "Extorno" %>' CommandName="Select" CausesValidation="False"></asp:ImageButton> 
</ItemTemplate>

<ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
</asp:TemplateField>
</Columns>
<EmptyDataTemplate>
No se encontraron registros...
</EmptyDataTemplate>
</asp:GridView> <BR /><asp:GridView id="gvCobroLog" runat="server" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="gvCobroLog_PageIndexChanging" Width="891px" __designer:wfdid="w501" Caption="Historial del Proceso de Cobros"><Columns>
<asp:BoundField DataField="IdCertificado" Visible="False" HeaderText="IdCertificado"></asp:BoundField>
<asp:BoundField DataField="IdCobro" HeaderText="N&#186; Cuota">
<ItemStyle Width="70px" HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="MedioPago" HeaderText="Medio Pago">
<ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="numeroCuenta" HeaderText="N&#186; Cuenta/Tarjeta">
<ItemStyle Width="200px" HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ValorMoneda" HeaderText="Valor Cobrado">
<ItemStyle Width="100px" HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField HtmlEncode="False" DataFormatString="{0:d}" DataField="FechaCobro" HeaderText="Fecha Cobro">
<ItemStyle Width="90px" HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="Referencia" HeaderText="Referencia"></asp:BoundField>
<asp:BoundField DataField="EstadoCobro" HeaderText="Estado">
<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>
<EmptyDataTemplate>
No se encontraron registros...
</EmptyDataTemplate>
</asp:GridView> <asp:GridView id="gvExtorno" runat="server" AutoGenerateColumns="False"  Caption="Extorno"><Columns>
<asp:BoundField DataField="idCobro" HeaderText="N&#186; Cuota">
<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="monto" HeaderText="Monto">
<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="idmoneda" HeaderText="Moneda">
<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="tipoCambio" HeaderText="Tipo Cambio">
<ItemStyle Width="70px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="motivo" HeaderText="Motivo"></asp:BoundField>
<asp:BoundField DataField="fechaSolicitud" DataFormatString="{0:d}" HeaderText="Fecha Solicitud" HtmlEncode="False">
<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Fecha Extorno"><EditItemTemplate>
<asp:TextBox id="TextBox2" runat="server"  Text='<%# Bind("fechaExtorno") %>'></asp:TextBox>
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="Label2" runat="server"  Text='<%# Bind("fechaExtorno", "{0:d}") %>' Visible='<%# IIF((Eval("fechaExtorno", "{0:d}").ToString() ="01/01/1900"),false,true) %>'></asp:Label> 
</ItemTemplate>

<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Fecha Cierre"><EditItemTemplate>
<asp:TextBox id="TextBox1" runat="server"  Text='<%# Bind("fechaCierre") %>'></asp:TextBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="Label1" runat="server" Text='<%# Bind("fechaCierre", "{0:d}") %>' Visible='<%# IIF((Eval("fechaCierre", "{0:d}").ToString() ="01/01/1900"),false,true) %>'></asp:Label> 
</ItemTemplate>

<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="activo" HeaderText="Activo">
<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:BoundField>
</Columns>
</asp:GridView><BR /><asp:Button id="btnImprimirECuenta" onclick="btnImprimirECuenta_Click" runat="server" Width="157px"  Text="Imprimir Estado de Cuenta"></asp:Button> 
</contenttemplate>
            <triggers>
<asp:AsyncPostBackTrigger ControlID="btnActCobro" EventName="Click"></asp:AsyncPostBackTrigger>
</triggers>
        </asp:UpdatePanel>
        <asp:Button ID="btnActCobro" runat="server" SkinID="BotonOculto" />
       
    
</ContentTemplate>

</cc1:TabPanel>


<cc1:TabPanel ID="TabPanel7" runat="server" HeaderText="TabPanel7">
            <HeaderTemplate>
Siniestros o Reclamos
</HeaderTemplate>
            

<ContentTemplate>
                <asp:UpdatePanel id="upSimRec" runat="server" UpdateMode="Conditional">
                    <contenttemplate>
                        <br />
                        <asp:GridView ID="gvReclamo" runat="server" AutoGenerateColumns="False" DataKeyNames="NroTicket"
                            Caption="Reclamos" EnableModelValidation="True" Width="887px">
                            <Columns>
                                <asp:BoundField DataField="NroTicket" HeaderText="N� Ticket">
                                <ItemStyle HorizontalAlign="Center" Width="120px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="NroReclamo" HeaderText="N� Incidente">
                                <ItemStyle HorizontalAlign="Center" Width="120px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FechaReclamo" HeaderText="Fecha Incidente">
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FechaAtencion" HeaderText="Fecha Atenci�n">
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FechaFinAtencion" HeaderText="Fecha Cierre">
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="MotivoReclamo" HeaderText="Motivo Reclamo">
                                <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="EstadoReclamo" HeaderText="Estado">
                                <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>

                                <asp:TemplateField>
                                   <ItemStyle HorizontalAlign="Center" Width="60px"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/img/seleccionar.gif" CausesValidation="False" Text="Select" CommandName="Select" ToolTip="Seleccionar"></asp:ImageButton> 
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                            <EmptyDataTemplate>
                                No se encontraron registros...
                            </EmptyDataTemplate>
                        </asp:GridView>
                        <br />
                        <asp:Button ID="btnImpresionNota" runat="server" CausesValidation="False" Enabled="False" Height="25px" Text="Imprimir Nota Reclamo" Width="150px" />
                        <br />
                        <br />
                        <asp:GridView ID="gvSiniestro" runat="server" AutoGenerateColumns="False" DataKeyNames="NroTicket"
                            Caption="Siniestros" EnableModelValidation="True" Width="887px">
                            <Columns>
                                <asp:BoundField DataField="NroTicket" HeaderText="N� Ticket">
                                <ItemStyle HorizontalAlign="Center" Width="120px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="TipoSiniestro" HeaderText="Tipo Siniestro">
                                <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FechaSiniestro" HeaderText="Fecha Siniestro">
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Monto Denunciado">                                    
                                    <ItemTemplate>                                        
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("MontoSiniestro") %>'></asp:Label>
                                        <asp:Label ID="Label9" runat="server" Text='<%# Bind("IDMonedaMSiniestro") %>'></asp:Label> 
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="120px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Monto Pago">                                  
                                    <ItemTemplate>                                        
                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("MontoPago") %>'></asp:Label> 
                                        <asp:Label ID="Label10" runat="server" Text='<%# Bind("IDMonedaMPago") %>'></asp:Label> 
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="120px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="EstadoSiniestro" HeaderText="Estado">
                                <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField>
                                   <ItemStyle HorizontalAlign="Center" Width="60px"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/img/seleccionar.gif" CausesValidation="False" Text="Select" CommandName="Select" ToolTip="Seleccionar"></asp:ImageButton> 
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                            <EmptyDataTemplate>
                                No se encontraron registros...
                            </EmptyDataTemplate>
                        </asp:GridView>
                        <br />
                                                <asp:GridView ID="gvDatosAdicionales" runat="server" __designer:wfdid="w284" 
                                                    AutoGenerateColumns="False"  AllowPaging="True" Caption="Informaci�n Adicional"
                                                    EnableModelValidation="True"  Width="887px">
                                                    <Columns>
                                                        <asp:BoundField DataField="Nombre" HeaderText="Nombre">
                                                        <ItemStyle Width="280px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n">
                                                        <ItemStyle HorizontalAlign="Center" Width="110px" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <EmptyDataTemplate>
                                                        No se encontraron registros...
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
            </contenttemplate>
                    <triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnActSinRec" EventName="Click"></asp:AsyncPostBackTrigger>
                    </triggers>
                </asp:UpdatePanel>
                <asp:Button ID="btnActSinRec" runat="server" SkinID="BotonOculto" />
            
</ContentTemplate>
        

</cc1:TabPanel>

        <cc1:TabPanel ID="TabPanel9" runat="server" HeaderText="TabPanel7">
            <HeaderTemplate>
                Datos Adicionales
            
</HeaderTemplate>
            

<ContentTemplate>
                <asp:UpdatePanel id="upDataAditional" runat="server" UpdateMode="Conditional">
                    <contenttemplate>
                     <table border="0" cellpadding="0" cellspacing="0" style="width: 904px; margin: auto">
                            <tr>
                                <td>
                                <table border="0" cellpadding="0" cellspacing="0" style="WIDTH: 100%">
                                    <tbody>
                                        <tr>
                                            <td class="TablaTitulo">
                                                DATOS ADICIONALES</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table style="width: 100%">
                                    <tr>
                                        <td colspan="6">
                                            <asp:GridView ID="gvDataAditional" runat="server" 
                                                AutoGenerateColumns="False"  AllowPaging="True" 
                                                EnableModelValidation="True" Height="16px" Width="100%">
                                                <RowStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" Height="23px"></RowStyle>
                                                <Columns>
                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre">
                                                    <ItemStyle Width="280px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Valor" HeaderText="Descripci�n">
                                                    <ItemStyle HorizontalAlign="Center" Width="110px" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    No se encontraron registros...
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                               </td>
                            </tr>
                        </table>
                </contenttemplate>
                    <triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnActAditional" EventName="Click"></asp:AsyncPostBackTrigger>
                    </triggers>
                </asp:UpdatePanel>
                <asp:Button ID="btnActAditional" runat="server" SkinID="BotonOculto" />
            
</ContentTemplate>
        

</cc1:TabPanel>

 <!--Nuevo tab-->
 <cc1:TabPanel runat="server" HeaderText="TabPanel8" ID="TabPanel8" >
     <HeaderTemplate>
Integral de Comercio
</HeaderTemplate>
    <ContentTemplate>
        <asp:UpdatePanel id="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <contenttemplate>
                <table style="margin: auto; WIDTH: 907px" cellSpacing="7" cellPadding="0" border="0">
                        <tbody>
                            <tr>
                                <td class="auto-style3" colspan="1">
                                    <table border="0" cellpadding="0" cellspacing="0" style="WIDTH: 100%">
                                        <tbody>
                                            <tr>
                                                <td class="TablaTitulo" colspan="5">Informaci�n de Riesgo del Asegurado</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table border="0" cellpadding="0" cellspacing="4" style="width: 100%">
                                        <tr>
                                            <td class="CampoCelda">Ramo</td>
                                            <td width="50%" class="CampoCelda">Superficie Comercio (m2)</td>
                                        </tr>
                                        <tr>
                                            <td class="CampoCelda">
                                                <asp:DropDownList ID="ddlRamo" runat="server" AutoPostBack="True" Height="17px" OnSelectedIndexChanged="ddlBuscarx_SelectedIndexChanged" Width="391px">
                                                </asp:DropDownList>
                                            </td>
                                            <td width="50%">
                                                <asp:TextBox ID="tb_superficieComercio" runat="server" ReadOnly="True" SkinID="txtDisable" Width="120px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="CampoCelda" colspan="2">Detalle Bienes (Marca y Modelo. En caso de monitor detallar pulgadas)</td>
                                        </tr>
                                        <tr>
                                            <td class="CampoCelda" colspan="2">
                                                <asp:TextBox ID="tb_detalleBienes" runat="server" ReadOnly="True" SkinID="txtDisable" Width="877px" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <tr>
                                    <td style="HEIGHT: 29px">
                                        <asp:Button ID="PHogarbtnModificar" runat="server" CausesValidation="False" Text="Modificar" Width="70px" />
                                        <asp:Button ID="PHogarbtnGrabar" runat="server" Text="Grabar" ValidationGroup="ASE" Width="70px" />
                                        <asp:Button ID="PHogarbtnCancelar" runat="server" CausesValidation="False" Text="Cancelar" Width="70px" />
                                        <br />
                                        <asp:HiddenField ID="PHhfOperacion" runat="server" />
                                        <asp:HiddenField ID="PHhfInfoProducto" runat="server" />
                                    </td>
                                </tr>
                            
                        </tbody>
                    </table>
            </contenttemplate>
            <triggers>
                <asp:AsyncPostBackTrigger ControlID="btnActHogar" EventName="Click"></asp:AsyncPostBackTrigger>
            </triggers>
        </asp:UpdatePanel><asp:Button ID="btnActHogar" runat="server" SkinID="BotonOculto" />
      
    
</ContentTemplate>

</cc1:TabPanel>

</cc1:tabcontainer>
    <br />
    <br />
    <br />
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0">
        <ProgressTemplate>
            <uc1:Cargando ID="Cargando1" runat="server" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    &nbsp;&nbsp;
</asp:Content>

