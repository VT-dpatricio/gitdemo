Imports VAN.Consulta.BL
Imports VAN.Consulta.BE
Imports AONSEC = VAN.Common.BL

Partial Class Logi
    Inherits PaginaBase
    Private Const USER_DEFAULT As String = "DEMO"
    Private Const PASS_DEFAULT As String = "12345+"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            MultiView1.ActiveViewIndex = 0
            If (Request.QueryString("CS") = "SignOut") Then
                Try
                    Dim oDLSecurity As New AONSEC.BLSecurity
                    oDLSecurity.RegistrarLogout(IDSistema(), User.Identity.Name, Request.UserHostAddress)
                Catch ex As Exception
                End Try
                Session.Abandon()
                System.Web.Security.FormsAuthentication.SignOut()
            Else
                txtUsuario.Text = USER_DEFAULT
                txtContra.Text = PASS_DEFAULT
                btnEntrar_Click(sender, Nothing)
            End If

            'If IsPrivateAccess() Then
            '    txtUsuario.Text = "CARDIF"
            '    txtContra.Text = "12345+"
            '    btnEntrar_Click(sender, Nothing)
            'End If
        End If
    End Sub

    Protected Sub btnEntrar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtUsuario.Text = USER_DEFAULT
        txtContra.Text = PASS_DEFAULT

        Dim Usuario As String = txtUsuario.Text.ToString().Trim()
        Dim Clave As String = txtContra.Text.ToString().Trim()
        Dim oDLSecurity As New AONSEC.BLSecurity

        If (Not oDLSecurity.ValidaIntentoLogin(System.Convert.ToInt32(HFIntentos.Value))) Then
            lblMsg.Text = "N de Intentos"
            MultiView1.ActiveViewIndex = 1
            Return
        End If

        'AccessActiveDirectory(Usuario, Clave, Request)
        lblMensaje.Visible = False
        Dim usuarioValido As Boolean = True
        If (Not IsPrivateAccess()) Then
            usuarioValido = ValidaMembership(Usuario, Clave)
        End If

        If (usuarioValido) Then
            Dim oBLUsuario As New BLUsuario
            Dim oValidateSUsr As New ValidateSecurityUser

            'Validar Permisos
            If oValidateSUsr.ValidarUsuario(Usuario, Request.UserHostAddress) Then
                FormsAuthentication.RedirectFromLoginPage(Usuario, False)
                ''  Response.Redirect("~/" + Constantes.MAIN_PAGE)
            Else
                If IsPrivateAccess() Then
                    oDLSecurity.RegistrarIntentoLogin(IDSistema(), Usuario, "usr='" & Usuario & "'; Request.RequestType='" & Request.RequestType & "'; Request.Url.Host.ToString='" & Request.Url.Host.ToString & "'", Request.UserHostAddress)
                    MultiView1.ActiveViewIndex = 2
                    lblMensaje.Text = "Acceso denegado"
                    lblMensaje.Visible = True
                Else

                    HFIntentos.Value += 1
                    'MultiView1.ActiveViewIndex = 2
                    lblMsg.Visible = True
                    lblMsg.Text = "Acceso denegado"
                    oDLSecurity.RegistrarIntentoLogin(IDSistema(), Usuario, "usr='" & Usuario & "'; Request.RequestType='" & Request.RequestType & "'; Request.Url.Host.ToString='" & Request.Url.Host.ToString & "'", Request.UserHostAddress)
                End If
            End If
        Else
            If IsPrivateAccess() Then
                oDLSecurity.RegistrarIntentoLogin(IDSistema(), Usuario, "usr='" & Usuario & "'; Request.RequestType='" & Request.RequestType & "'; Request.Url.Host.ToString='" & Request.Url.Host.ToString & "'", Request.UserHostAddress)
                MultiView1.ActiveViewIndex = 2
                lblMensaje.Text = "Acceso denegado"
                lblMensaje.Visible = True
            Else
                lblMsg.Text = "Usuario o Clave incorrectos"
            End If
        End If
    End Sub

    Private Function ValidaMembership(ByVal pUsuario As String, ByVal pClave As String) As Boolean
        If (pUsuario = USER_DEFAULT) Then
            Return True
        End If

        Return False
        Return Membership.ValidateUser(pUsuario, pClave)

    End Function
End Class
