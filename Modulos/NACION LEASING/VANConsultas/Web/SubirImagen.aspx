<%@ Page Language="VB" MasterPageFile="~/MasterPage2.master" AutoEventWireup="false" CodeFile="SubirImagen.aspx.vb" Inherits="SubirImagen" title="Subir Imagen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Panel ID="pSubirImagen" runat="server">

    <table cellpadding="3" style="width: 100%" class="MarcoTabla">
        <tr>
            <td class="TablaTitulo" colspan="6">
                Agregar Imagen</td>
        </tr>
        <tr>
            <td style="width: 40px">
                Periodo:</td>
            <td style="width: 43px">
                <asp:DropDownList ID="ddlAnio" runat="server" Width="56px">
                    <asp:ListItem>2008</asp:ListItem>
                    <asp:ListItem>2009</asp:ListItem>
                    <asp:ListItem>2010</asp:ListItem>
                    <asp:ListItem>2011</asp:ListItem>
                    <asp:ListItem>2012</asp:ListItem>
                </asp:DropDownList></td>
            <td style="width: 94px">
                <asp:DropDownList ID="ddlMes" runat="server" SelectedValue='<%# Bind("Mes") %>' Visible='<%# IIF((Eval("IDPeriodo").ToString() ="0"),true,false) %>'
                    Width="96px">
                    <asp:listitem Value="1">ENERO</asp:listitem>
                    <asp:listitem Value="2">FEBRERO</asp:listitem>
                    <asp:listitem Value="3">MARZO</asp:listitem>
                    <asp:listitem Value="4">ABRIL</asp:listitem>
                    <asp:listitem Value="5">MAYO</asp:listitem>
                    <asp:listitem Value="6">JUNIO</asp:listitem>
                    <asp:listitem Value="7">JULIO</asp:listitem>
                    <asp:listitem Value="8">AGOSTO</asp:listitem>
                    <asp:listitem Value="9">SETIEMBRE</asp:listitem>
                    <asp:listitem Value="10">OCTUBRE</asp:listitem>
                    <asp:listitem Value="11">NOVIEMBRE</asp:listitem>
                    <asp:listitem Value="12">DICIEMBRE</asp:listitem>


                </asp:DropDownList></td>
            <td style="width: 42px">
                Imagen:</td>
            <td style="width: 275px">
                <asp:FileUpload ID="fuImagen" runat="server" Width="273px" />
                </td>
            <td>
                <asp:Button ID="btnCargarImagen" runat="server" OnClick="btnCargarImagen_Click" Text="Grabar"
                    Width="84px" /></td>
        </tr>
        <tr>
            <td style="width: 40px; height: 19px;">
            </td>
            <td style="width: 43px; height: 19px;">
            </td>
            <td style="width: 94px; height: 19px;">
            </td>
            <td style="width: 42px; height: 19px;">
            </td>
            <td colspan="2" style="height: 19px">
                <asp:Label ID="lblMensaje" runat="server"></asp:Label></td>
        </tr>
    </table>
    </asp:Panel>
</asp:Content>

