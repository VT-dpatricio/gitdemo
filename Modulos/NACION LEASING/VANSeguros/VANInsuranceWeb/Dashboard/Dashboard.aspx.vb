﻿Public Class Dashboard
    Inherits PaginaBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session(SESSION_BEUSUARIO) Is Nothing Then
            Response.Redirect("~/Login.aspx")
        End If

        If Not IsPostBack Then
            If ValidarEstadoClaveUsuario() Then
                Response.Redirect("~/Seguridad/CambiarClave.aspx", False)
            End If
        End If

    End Sub

End Class