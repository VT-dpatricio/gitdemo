<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Producto.aspx.vb" Inherits="VANInsurance.Producto1" MasterPageFile="~/MasterPage.Master" %>

<%@ Register src="../Controles/Cargando.ascx" tagname="Cargando" tagprefix="uc1" %>
<asp:Content ID="Content2" runat="server" contentplaceholderid="head">
    <style type="text/css">
        .style1
        {
            height: 15px;
            width: 72px;
        }
        .style2
        {
            height: 15px;
            width: 48px;
        }
        .style3
        {
            height: 15px;
            width: 170px;
        }
        .style4
        {
            height: 15px;
        }
        .style5
        {
            height: 15px;
            width: 514px;
        }
        .style6
        {
            text-align: left;
            background: #DCDCDC;
            font-weight: bold;
            padding: 3px;
            background: #FFF url('../Img/ftitulo.png') repeat-x ;
            color : #660000;
            height: 15px;
        }
    </style>
<script src="../Scripts/jquery.simplemodal.1.4.2.min.js" type="text/javascript"></script>   
<script type="text/jscript" language="javascript">
    function DetalleProducto(pID) {
       /* $("#frmDetalle").modal({
            closeHTML: "<a href='#' title='Cerrar' class='modal-close'><img src='../img/close1.gif' alt='Cerrar' width='16' height='16'/></a>",
            containerCss: {
                backgroundColor: "#FFF",
                borderColor: "#750000",
                height: 155,
                padding: 0,
                width: 490
            },
            overlayClose: false,
            appendTo: '#aspnetForm',
            persist: true
        });*/

        
        var src = "dialog/dProductoDetalle.aspx?Id=" + pID;
        $.modal('<iframe src="' + src + '" height="360" width="880" style="border:0" frameborder=0>', {
        closeHTML: "<a href='#' title='Cerrar' class='modal-close'><img src='../Img/cerrar.gif' alt='Cerrar' width='16' height='16'/></a>",
            containerCss: {
                backgroundColor: "#FFF",
                borderColor: "#2C483C",
                height: 360,
                padding: 0,
                width: 880
            },
            overlayClose: true,
            appendTo: '#aspnetForm',
            persist: true
        });

    }
</script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="contenedor" runat="server">
<table style="margin: auto; width: 1000px">
        <tr>
            <td colspan="3" style="height: 39px; width: 1000px;">
                <table class="MarcoTabla" style="width: 100%">
                    <tr>
                        <td class="style6" colspan="5" rowspan="1">
                            Productos</td>
                    </tr>
                    <tr>
                        <td style="width: 27px; height: 15px">
                            &nbsp;Buscar:</td>
                        <td class="style1">
                            <asp:TextBox ID="txtBuscar" runat="server" Width="157px"></asp:TextBox></td>
                        <td class="style2">
                            Entidad:</td>
                        <td class="style3">
                            <asp:DropDownList ID="ddlEntidad" runat="server" AppendDataBoundItems="True" AutoPostBack="True"
                                CssClass="cbo"
                                Width="221px">
                                <asp:ListItem Value="0">---Todas---</asp:ListItem>
                            </asp:DropDownList></td>
                        <td class="style5">
                            <asp:ImageButton ID="btnBuscar" runat="server" CausesValidation="False" ImageUrl="~/img/btnBuscar.png" />
                            <asp:ImageButton ID="btnNuevo" runat="server" CausesValidation="False" ImageUrl="~/img/btnNuevo.png" />&nbsp;
                            <asp:Label ID="lblResultado" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" class="style4">
                            <asp:UpdatePanel ID="upLista" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:GridView ID="gvListaProducto" runat="server" AllowPaging="True" 
                                        AutoGenerateColumns="False" PageSize="15" DataKeyNames="IDProducto">
                                        <Columns>
                                            <asp:TemplateField HeaderText="ID">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("IDProducto") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("IDProducto") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="80px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Nombre">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Nombre") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("Nombre") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="300px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Entidad">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Entidad") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("Entidad") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="200px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Tipo">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("TipoProducto") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("TipoProducto") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="150px" />
                                            </asp:TemplateField>
                                            <asp:CheckBoxField DataField="Activo" HeaderText="Activo">
                                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                                            </asp:CheckBoxField>
                                            <asp:CommandField ButtonType="Image" SelectImageUrl="~/Img/Grid/ver.gif" 
                                                SelectText="Ver Producto" ShowSelectButton="True">
                                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                                            </asp:CommandField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No se encontraron registros
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click">
                                    </asp:AsyncPostBackTrigger>
                                    <asp:AsyncPostBackTrigger ControlID="ddlEntidad" 
                                        EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>            </td>
        </tr>
        <tr>
            <td colspan="3" style="vertical-align: top; width: 488px;">
                &nbsp;
                &nbsp;&nbsp;
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0">
                    <ProgressTemplate>
                        <uc1:Cargando ID="Cargando1" runat="server" />
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
        </table>
</asp:Content>

