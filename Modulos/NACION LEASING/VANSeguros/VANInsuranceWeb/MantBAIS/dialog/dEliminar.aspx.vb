﻿Imports VAN.InsuranceWeb.BE
Partial Public Class dEliminar
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim IdPosicion As Integer = Request.QueryString("Id")
        Dim Pagina As String = Request.QueryString("page")
        IdPosicion -= 1
        If Pagina = "Opcion" Then
            DirectCast(Session("ListOpcionBE"), List(Of OpcionBE)).RemoveAt(IdPosicion)
            For i As Integer = 0 To DirectCast(Session("ListOpcionBE"), List(Of OpcionBE)).Count - 1
                DirectCast(Session("ListOpcionBE"), List(Of OpcionBE)).Item(i).Id = i + 1
            Next

        ElseIf Pagina = "OpcionPrima" Then
            DirectCast(Session("ListOpcionPrimaBE"), List(Of OpcionPrimaBE)).RemoveAt(IdPosicion)
            For i As Integer = 0 To DirectCast(Session("ListOpcionPrimaBE"), List(Of OpcionPrimaBE)).Count - 1
                DirectCast(Session("ListOpcionPrimaBE"), List(Of OpcionPrimaBE)).Item(i).Id = i + 1
            Next

        ElseIf Pagina = "MedioPago" Then
            DirectCast(Session("ListProductoMedioPagoBE"), List(Of ProductoMedioPagoBE)).RemoveAt(IdPosicion)
            For i As Integer = 0 To DirectCast(Session("ListProductoMedioPagoBE"), List(Of ProductoMedioPagoBE)).Count - 1
                DirectCast(Session("ListProductoMedioPagoBE"), List(Of ProductoMedioPagoBE)).Item(i).Id = i + 1
            Next

        ElseIf Pagina = "InfoAsegurado" Then
            DirectCast(Session("ListInfoAseguradoBE"), List(Of InfoAseguradoBE)).RemoveAt(IdPosicion)
            For i As Integer = 0 To DirectCast(Session("ListInfoAseguradoBE"), List(Of InfoAseguradoBE)).Count - 1
                DirectCast(Session("ListInfoAseguradoBE"), List(Of InfoAseguradoBE)).Item(i).Id = i + 1
            Next

        ElseIf Pagina = "InfoProducto" Then
            DirectCast(Session("ListInfoProductoBE"), List(Of InfoProductoBE)).RemoveAt(IdPosicion)
            For i As Integer = 0 To DirectCast(Session("ListInfoProductoBE"), List(Of InfoProductoBE)).Count - 1
                DirectCast(Session("ListInfoProductoBE"), List(Of InfoProductoBE)).Item(i).Id = i + 1
            Next
        End If


        'Dim strScript As String = "<Script language=""JavaScript"">CerrarPagina('" & Pagina & "');</Script>"
        'ClientScript.RegisterStartupScript(GetType(String), "clientScript", strScript)
        ClientScript.RegisterStartupScript(GetType(String), "clientScript", "window.parent.OcultarEliminarPCModal('" & Pagina & "')", True)
    End Sub

End Class