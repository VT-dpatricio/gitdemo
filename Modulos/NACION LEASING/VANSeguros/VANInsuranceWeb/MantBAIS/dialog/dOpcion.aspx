﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="dOpcion.aspx.vb" Inherits="VANInsurance.dOpcion" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>BNP PARIBAS CARDIF - Venta Seguros</title>
    <link href="../../Estilos/Default.css" type="text/css" rel="stylesheet" />
    <link href="../../Estilos/General.css" type="text/css" rel="stylesheet" />
    <link href="../../Estilos/Menu.css" type="text/css" rel="stylesheet" />

    <script src="../../Scripts/General.js" type="text/javascript"></script>

    <script type="text/javascript">
        function CerrarPagina(page) {
            var parentWindow = window.parent;
            parentWindow.OcultarPCModal(page);
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div class="DivHeight096">
        <table cellspacing="4" cellpadding="0" border="0" style="width: 580px" class="BodyMP">
    
            <tr>
                <td colspan="4" class="Titulo">
                    PLANES
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Panel ID="Panel1" runat="server" GroupingText="Datos Basicos" Width="90%">
                        <table width="100%">
                            <tr>
                                <td align="left">
                                    Plan :
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtCodigo" runat="server" MaxLength="15" Width="150px" style="text-transform :uppercase"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtCodigo" runat="server" ErrorMessage="(*)" ControlToValidate="txtCodigo"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left">
                                    Activo :
                                </td>
                                <td colspan="3" align="left">
                                    <asp:CheckBox ID="cbObligatorio" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    Puntos :
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtPuntos" runat="server" MaxLength="4" Width="60px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtPuntos" runat="server" ErrorMessage="(*)" ControlToValidate="txtPuntos"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revtxtPuntos" runat="server" ErrorMessage="(*)" ControlToValidate="txtPuntos" ValidationExpression="[0-9]"></asp:RegularExpressionValidator>
                                </td>
                                <td align="left">
                                    Puntos Valor :
                                </td>
                                <td colspan="3" align="left">
                                    <asp:TextBox ID="txtPuntosValor" runat="server" MaxLength="4" Width="60px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtPuntosValor" runat="server" ErrorMessage="(*)" ControlToValidate="txtPuntosValor"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revtxtPuntosValor" runat="server" ErrorMessage="(*)" ControlToValidate="txtPuntosValor" ValidationExpression="[0-9]"></asp:RegularExpressionValidator>
                                </td>
                                
                            </tr>
                            <tr>
                                <td colspan="4" align="right">
                                    <asp:Button ID="btnGrabar" runat="server" Text="Grabar" class="ButtonGrid" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
