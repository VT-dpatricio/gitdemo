﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="dProductoDetalle.aspx.vb"
    Inherits="VANInsurance.dProductoDetalle" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BNP PARIBAS CARDIF - Venta Seguros</title>
    <link href="../../Estilos/Default.css" type="text/css" rel="stylesheet" />
    <link href="../../Estilos/General.css" type="text/css" rel="stylesheet" />
    <link href="../../Estilos/Menu.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        .style1
        {
            width: 180px;
        }
        .style2
        {
            width: 95px;
        }
        .style3
        {
            width: 105px;
        }
        .style4
        {
            height: 24px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">

        <table cellspacing="4" cellpadding="0" border="0" style="width: 850px" class="BodyMP">
            <tr>
                <td colspan="4" class="Titulo">
                    DETALLE DEL PRODUCTO
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel1" runat="server" GroupingText="Datos Basicos" Width="850px">
                                    <table width="850px">
                                        <tr>
                                            <td align="left" class="style2">
                                                Codigo Producto :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtCodigoProducto" runat="server" MaxLength="4" Width="60px" ReadOnly="True"
                                                    class="CajaTextoNegro"></asp:TextBox>
                                            </td>
                                            <td align="left" class="style3">
                                                Nombre :
                                            </td>
                                            <td colspan="2" align="left">
                                                <asp:TextBox ID="txtNombre" runat="server" MaxLength="50" Width="300px" Style="text-transform: uppercase"
                                                    ReadOnly="True" class="CajaTextoNegro"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="style2">
                                                Asegurador :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtAsegurador" runat="server" Width="200px" Style="text-transform: uppercase"
                                                    ReadOnly="True" class="CajaTextoNegro"></asp:TextBox>
                                            </td>
                                            <td align="left" class="style3">
                                                Entidad :
                                            </td>
                                            <td align="left" class="style1">
                                                <asp:TextBox ID="txtEntidad" runat="server" Width="180px" Style="text-transform: uppercase"
                                                    ReadOnly="True" class="CajaTextoNegro"></asp:TextBox>
                                            </td>
                                            <td align="left">
                                                <%--Regla Cobro :--%>
                                                <%--<asp:DropDownList ID="ddlRegla" runat="server" Width="100px">
                                            </asp:DropDownList>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="style2">
                                                Tipo Producto :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtTipoProducto" runat="server" Width="200px" Style="text-transform: uppercase"
                                                    ReadOnly="True" class="CajaTextoNegro"></asp:TextBox>
                                            </td>
                                            <td align="left" class="style3">
                                                Max Asegurados :
                                            </td>
                                            <td align="left" class="style1">
                                                <asp:TextBox ID="txtMaxAsegurados" runat="server" MaxLength="4" Width="40px" ReadOnly="True"
                                                    class="CajaTextoNegro"></asp:TextBox>
                                            </td>
                                            <td align="left" rowspan="8">
                                                <table>
                                                    <tr>
                                                        <td class="style4">
                                                            <asp:CheckBox ID="cbVigCobro" runat="server" Text="Vig. Cobro" ReadOnly="True" Enabled="False" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="cbNacReq" runat="server" Text="Fecha Nac Requerido" ReadOnly="True"
                                                                Enabled="False" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="cbValParsco" runat="server" Text="Valida Parentesco" ReadOnly="True"
                                                                Enabled="False" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="cbOblCtaHabiente" runat="server" Text="Obligacion Cta Habiente"
                                                                ReadOnly="True" Enabled="False" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="cbGeneracobro" runat="server" Text="Genera cobro" ReadOnly="True"
                                                                Enabled="False" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="cbValDirAseg" runat="server" Text="Valida Dirireccion Asegegurado"
                                                                ReadOnly="True" Enabled="False" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="cbValDirCer" runat="server" Text="Valida Dirreccion Certificado"
                                                                ReadOnly="True" Enabled="False" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="cbCtaHbAsegurado" runat="server" Text="Cta Hb Asegurado" ReadOnly="True"
                                                                Enabled="False" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="cbActivo" runat="server" Text="Activo" ReadOnly="True" Enabled="False" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="style2">
                                                Max Polizas Aseg :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtMaxPolizasAseg" runat="server" MaxLength="4" Width="40px" ReadOnly="True"
                                                    class="CajaTextoNegro"></asp:TextBox>
                                            </td>
                                            <td align="left" class="style3">
                                                Max Polizas Cta Hb :
                                            </td>
                                            <td align="left" class="style1">
                                                <asp:TextBox ID="txtMaxPolizasCtaHb" runat="server" MaxLength="4" Width="40px" ReadOnly="True"
                                                    class="CajaTextoNegro"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="style2">
                                                Max Cta Titular :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtMaxCtaTitular" runat="server" MaxLength="4" Width="40px" ReadOnly="True"
                                                    class="CajaTextoNegro"></asp:TextBox>
                                            </td>
                                            <td align="left" class="style3">
                                                Max Polizas S/DNI :
                                            </td>
                                            <td align="left" class="style1">
                                                <asp:TextBox ID="txtMaxPolizasSDNI" runat="server" MaxLength="4" Width="40px" ReadOnly="True"
                                                    class="CajaTextoNegro"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="style2">
                                                Max Edad Perm :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtMaxEdadPerm" runat="server" MaxLength="4" ReadOnly="True" Width="40px"
                                                    class="CajaTextoNegro"></asp:TextBox>
                                            </td>
                                            <td align="left" class="style3">
                                                Max Polizas Regalo :
                                            </td>
                                            <td align="left" class="style1">
                                                <asp:TextBox ID="txtMaxPolizasRegalo" runat="server" MaxLength="4" Width="40px" ReadOnly="True"
                                                    class="CajaTextoNegro"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="style2">
                                                &nbsp; Max Dias Anu :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtMaxDiasAnu" runat="server" MaxLength="4" Width="40px" ReadOnly="True"
                                                    class="CajaTextoNegro"></asp:TextBox>
                                            </td>
                                            <td align="left" class="style3">
                                                &nbsp;
                                            </td>
                                            <td align="left" class="style1">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="style2">
                                                &nbsp;
                                            </td>
                                            <td align="left">
                                                &nbsp;
                                            </td>
                                            <td align="left" class="style3">
                                                &nbsp;
                                            </td>
                                            <td align="left" class="style1">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="style2">
                                                &nbsp;
                                            </td>
                                            <td align="left">
                                                &nbsp;
                                            </td>
                                            <td align="left" class="style3">
                                                &nbsp;
                                            </td>
                                            <td align="left" class="style1">
                                                &nbsp;
                                            </td>
                                            <td align="left">
                                                &nbsp;
                                            </td>
                                            <td align="left">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="style2">
                                                &nbsp;
                                            </td>
                                            <td align="left">
                                                &nbsp;
                                                <asp:Label ID="lblError" runat="server"></asp:Label>
                                            </td>
                                            <td align="left" class="style3">
                                                &nbsp;
                                            </td>
                                            <td align="left" class="style1">
                                                &nbsp;
                                            </td>
                                            <td align="left" colspan="2">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
