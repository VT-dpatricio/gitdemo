﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="dInfoProducto.aspx.vb"
    Inherits="VANInsurance.dInfoProducto" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>BNP PARIBAS CARDIF - Venta Seguros</title>
    <link href="../../Estilos/Default.css" type="text/css" rel="stylesheet" />
    <link href="../../Estilos/General.css" type="text/css" rel="stylesheet" />
    <link href="../../Estilos/Menu.css" type="text/css" rel="stylesheet" />

    <script src="../../Scripts/General.js" type="text/javascript"></script>

    <script type="text/javascript">
        function CerrarPagina(page) {
            var parentWindow = window.parent;
            parentWindow.OcultarPCModal(page);
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div class="DivHeight096">
        <table cellspacing="4" cellpadding="0" border="0" style="width: 580px" class="BodyMP">
            <tr>
                <td colspan="4" class="Titulo">
                    INFORMACIÓN ADCIONAL DEL CONTRATANTE
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Panel ID="Panel1" runat="server" GroupingText="Datos Basicos" Width="90%">
                        <table width="100%">
                            <tr>
                                <td align="left">
                                    Nombre :
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtNombre" runat="server" MaxLength="30" Width="100px" Style="text-transform: uppercase"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtNombre" runat="server" ErrorMessage="(*)" ControlToValidate="txtNombre"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left">
                                    Descripcion :
                                </td>
                                <td colspan="3" align="left">
                                    <asp:TextBox ID="txtDescripcion" runat="server" MaxLength="50" Width="200px" Style="text-transform: uppercase"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtDescripcion" runat="server" ErrorMessage="(*)"
                                        ControlToValidate="txtDescripcion"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    Tipo Dato :
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlTipoDato" runat="server" CssClass="Body" DataSourceID="sqlTipoDato"
                                        DataTextField="nombre" DataValueField="idTipoDato" Width="100px">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="sqlTipoDato" runat="server" ConnectionString="<%$ ConnectionStrings:BAIS %>"
                                        SelectCommand="select idTipoDato, upper(Nombre) as nombre from TipoDato"></asp:SqlDataSource>
                                    <asp:RequiredFieldValidator ID="rfvddlTipoDato" runat="server" ErrorMessage="(*)"
                                        ControlToValidate="ddlTipoDato"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left">
                                    Obligatorio :
                                </td>
                                <td colspan="3" align="left">
                                    <asp:CheckBox ID="cbObligatorio" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="right">
                                    <asp:Button ID="btnGrabar" runat="server" Text="Grabar" class="ButtonGrid" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
