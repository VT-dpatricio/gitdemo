﻿Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.BL
Partial Public Class MasterPage2
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not HttpContext.Current.Request.IsAuthenticated Then
            'Response.Redirect("~/Login.aspx")
        End If

        If Not IsPostBack Then
            'Page.Header.DataBind()
            'Dim js As New HtmlGenericControl("script")
            'js.Attributes("type") = "text/javascript"
            'js.Attributes("src") = ResolveUrl("~/Scripts/") & "jquery-1.7.1.min.js"
            'Page.Header.Controls.Add(js)
            'Dim js2 As New HtmlGenericControl("script")
            'js2.Attributes("type") = "text/javascript"
            'js2.Attributes("src") = ResolveUrl("~/ddlevelsfiles/") & "ddlevelsmenu.js"
            'Page.Header.Controls.Add(js2)
            'cargarJS("ddlevelsfiles/ddlevelsmenu.js")
            'lblUsuario.Text = Session("NombreUsuario").ToString & " (" & Session("IDUsuario").ToString & ")"
            Try
                lblUsuario.Text = Session("NombreUsuario").ToString & " (" & Session("IDUsuario").ToString & ")"
            Catch ex As Exception
                'Response.Redirect("~/Login.aspx")
            End Try
            CargarMenu()
        End If
    End Sub

    Private Sub cargarJS(ByVal script As String)
        Dim js As New HtmlGenericControl("script")
        js.Attributes("type") = "text/javascript"
        js.Attributes("src") = ResolveUrl("~/") & script
        Page.Header.Controls.Add(js)
    End Sub

    Private Sub CargarMenu()
        Dim oBLMenu As New BLMenu
        Dim Lista As ArrayList = oBLMenu.ListarMenu(Session("IDUsuario").ToString)
        Dim sMenu As String
        Dim sSubMenu As String = ""
        Dim IDSubMenu As String = ""
        sMenu = "<ul>" & vbCrLf
        For Each oBEMenu As BEMenu In Lista
            If (oBEMenu.IDMenuPadre = 0) Then
                IDSubMenu = ""
                If NroSubMenu(oBEMenu.IDMenu, Lista) > 0 Then
                    IDSubMenu = "rel=""ddsubmenu" & oBEMenu.IDMenu & """"
                    sSubMenu &= "<ul id=""ddsubmenu" & oBEMenu.IDMenu & """ class=""ddsubmenustyle"">" & vbCrLf
                    sSubMenu = Me.AgregarSubMenuSis(Lista, sSubMenu, oBEMenu.IDMenu)
                    sSubMenu &= "</ul>" & vbCrLf
                End If
                sMenu &= "<li><a href=""" & Page.ResolveClientUrl(oBEMenu.Url) & """ " & IDSubMenu & " >" & oBEMenu.Descripcion & "</a></li>" & vbCrLf
            End If
        Next oBEMenu
        sMenu &= "</ul>" & vbCrLf
        ltMenu.Text = sMenu
        ltSMenu.Text = sSubMenu
    End Sub

    Private Function AgregarSubMenuSis(ByVal Lista As ArrayList, ByVal sSubMenu As String, ByVal IDMenu As Integer) As String
        For Each oBEMenu As BEMenu In Lista
            If (oBEMenu.IDMenuPadre = IDMenu) Then
                sSubMenu &= "<li><a href=""" & Page.ResolveClientUrl(oBEMenu.Url) & """>" & oBEMenu.Descripcion & "</a>" & vbCrLf
                If NroSubMenu(oBEMenu.IDMenu, Lista) > 0 Then
                    sSubMenu &= "<ul>" & vbCrLf
                    sSubMenu = AgregarSubMenuSis(Lista, sSubMenu, oBEMenu.IDMenu)
                    sSubMenu &= "</ul>" & vbCrLf
                End If
                sSubMenu &= "</li>" & vbCrLf
            End If
        Next oBEMenu
        Return sSubMenu
    End Function
    Public Function NroSubMenu(ByVal pIDMenu As Integer, ByVal pLista As ArrayList) As Integer
        Dim c As Integer = 0
        For Each oBE As BEMenu In pLista
            If oBE.IDMenuPadre = pIDMenu Then
                c += 1
            End If
        Next oBE
        Return c
    End Function

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim MyFirstCtrl As Control = Page.Header.FindControl("FirstCtrlID")
        Page.Header.Controls.Remove(MyFirstCtrl)
        Page.Header.Controls.AddAt(0, MyFirstCtrl)
    End Sub

End Class