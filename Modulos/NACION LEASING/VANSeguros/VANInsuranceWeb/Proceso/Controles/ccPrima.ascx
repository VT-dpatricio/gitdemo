﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ccPrima.ascx.vb" Inherits="VANInsurance.ccPrima" %>
<table style="width: 414px">
    <tr>
        <td style="height: 17px">
            Plan</td>
        <td style="height: 17px">
            Frecuencia</td>
        <td style="height: 17px">
            Moneda Premio</td>
        <td id="ccPremio" style="height: 17px">
            Premio</td>
    </tr>
    <tr>
        <td>
            <asp:DropDownList ID="ddlPlan" runat="server" Width="120px" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td>
            <asp:DropDownList ID="ddlFrecuencia" runat="server" Width="120px" 
                AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td>
            <asp:DropDownList ID="ddlMonedaPrima" runat="server" Width="100px" 
                AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td>
            <asp:TextBox ID="txtPrima" runat="server" Width="50px" Enabled="False"></asp:TextBox>
            <asp:HiddenField ID="hfPrima" runat="server" Value="0" />
        </td>
    </tr>
</table>
<asp:HiddenField ID="hfMontoAsegurado" runat="server" Value="0" />
<asp:HiddenField ID="hfPrimaTotal" runat="server" Value="0" />

