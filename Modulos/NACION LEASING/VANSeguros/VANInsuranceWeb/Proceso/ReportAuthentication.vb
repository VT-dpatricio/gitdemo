﻿Imports Microsoft.VisualBasic
Imports Microsoft.Reporting.WebForms
Public Class ReportAuthentication

    Public Shared Function ConfigureAuthentication(ByRef reportViewer As Microsoft.Reporting.WebForms.ReportViewer) As Object

        Dim useAuntenticationProperty As Object = ConfigurationManager.AppSettings("AuthenticationCredentials")
        If (Not useAuntenticationProperty Is Nothing) Then
            Dim useAuthentication As Boolean = False
            Boolean.TryParse(useAuntenticationProperty.ToString(), useAuthentication)
            If useAuthentication Then
                Dim credential As CustomReportCredentials = New CustomReportCredentials()
                Dim cerd As IReportServerCredentials = DirectCast(credential, IReportServerCredentials)
                reportViewer.ServerReport.ReportServerCredentials = cerd
            End If
        End If
        Return Nothing
    End Function
End Class
