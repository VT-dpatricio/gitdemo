﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pAsegurados.aspx.vb" Inherits="VANInsurance.pAsegurados" EnableEventValidation="false" culture="es-PE" uiCulture="es-PE"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register src="../Controles/Cargando.ascx" tagname="Cargando" tagprefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<script type="text/javascript">
    function Numero(e) {
        key = (document.all) ? e.keyCode : e.which;
        if (key < 48 || key > 57) {
            if (key == 8 || key == 0) {
                return true
            }
            else
                return false;
        }

    }



</script>

</head>
<body>

<script type="text/javascript">


    function ValidaNroDoc(v) {
        if (v == 'NIT') {
            document.getElementById('<%=txtNroDocumento.ClientID%>').maxLength = '10';
       
        }
        else {
            document.getElementById('<%=txtNroDocumento.ClientID%>').maxLength = '7';
      
        }
    }

    function copiarDatos(c) {
        var cboTipoDoc = document.getElementById('<%= cboTipoDoc.ClientID %>');
        var txtNroDocumento = document.getElementById('<%= txtNroDocumento.ClientID %>');
        var cboParentesco = document.getElementById('<%= cboParentesco.ClientID %>');
        var txtApePat = document.getElementById('<%= txtApePat.ClientID %>');
        var txtApeMat = document.getElementById('<%= txtApeMat.ClientID %>');
        var txtPrimerNom = document.getElementById('<%= txtPrimerNom.ClientID %>');
        var txtSegundoNom = document.getElementById('<%= txtSegundoNom.ClientID %>');
        var txtFechaNac = document.getElementById('<%= txtFechaNac.ClientID %>');
        var txtEdad = document.getElementById('<%= txtEdad.ClientID %>');
        var txtDireccion = document.getElementById('<%= txtDireccion.ClientID %>');
        var cboSexo = document.getElementById('<%= cboSexo.ClientID %>');
        var cboEstadoCivil = document.getElementById('<%= cboEstadoCivil.ClientID %>');
        var txtTelefono = document.getElementById('<%= txtTelefono.ClientID %>');
        if (c == true) {
            cboTipoDoc.value = window.parent.document.getElementById('ctl00_ContentPlaceHolder1_cboTipoDoc').value;
            txtNroDocumento.value = window.parent.document.getElementById('ctl00_ContentPlaceHolder1_txtNroDocumento').value;
            cboParentesco.value = '2';
            txtApePat.value = window.parent.document.getElementById('ctl00_ContentPlaceHolder1_txtApePat').value;
            txtApeMat.value = window.parent.document.getElementById('ctl00_ContentPlaceHolder1_txtApeMat').value;
            txtPrimerNom.value = window.parent.document.getElementById('ctl00_ContentPlaceHolder1_txtPrimerNom').value;
            txtSegundoNom.value = window.parent.document.getElementById('ctl00_ContentPlaceHolder1_txtSegundoNom').value;
            txtFechaNac.value = window.parent.document.getElementById('ctl00_ContentPlaceHolder1_txtFechaNac').value;
            txtDireccion.value = window.parent.document.getElementById('ctl00_ContentPlaceHolder1_txtDirecParticular').value;
            cboSexo.selectedIndex = window.parent.document.getElementById('ctl00_ContentPlaceHolder1_cboSexo').value;
            cboEstadoCivil.selectedIndex = window.parent.document.getElementById('ctl00_ContentPlaceHolder1_cboEstadoCivil').value;
            txtTelefono.value = window.parent.document.getElementById('ctl00_ContentPlaceHolder1_txtTelefono').value;
            txtEdad.value = window.parent.document.getElementById('ctl00_ContentPlaceHolder1_txtEdad').value;
        } else {
            cboTipoDoc.selectedIndex = '0';
            txtNroDocumento.value = '';
            cboParentesco.selectedIndex = '0';
            txtApePat.value = '';
            txtApeMat.value = '';
            txtPrimerNom.value = '';
            txtSegundoNom.value = '';
            txtFechaNac.value = '';
            txtDireccion.value = '';
            cboSexo.selectedIndex = '0';
            cboEstadoCivil.selectedIndex = '0';
            txtTelefono.value = '';
            txtEdad.value = '';
        }
    }

    function SFechaNac() {
        var txtFechaNac = document.getElementById('<%=txtFechaNac.ClientID%>');
        txtFechaNac.value = document.getElementById('<%=txtFechaNacAx.ClientID%>').value;
        __doPostBack('<%=txtFechaNac.ClientID%>', '');
    }

    function ftxtApePat() {
        var txt = document.getElementById('<%=txtApePat.ClientID%>');
        txt.focus();
    }

</script>



    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
        <table border="0" cellpadding="0" cellspacing="7" style="margin: auto; width: 645px">
            <tr>
                <td class="MarcoTabla" style="width: 651px">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="TablaTitulo">
                                Datos del Asegurado</td>
                        </tr>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="4" style="width: 100%">
                        <tr>
                            <td class="CampoCelda">
                                Parentesco</td>
                            <td class="CampoCelda">
                                Tipo Documento</td>
                            <td class="CampoCelda" style="width: 119px;">
                                Nº Documento<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                                    ControlToValidate="txtNroDocumento" Display="None" ErrorMessage="Ingrese Nro Documento"
                                    InitialValue=" " SetFocusOnError="True">*</asp:RequiredFieldValidator><asp:Label
                                        ID="Label2" runat="server" EnableViewState="False" ForeColor="Red" Text="*"></asp:Label></td>
                            <td class="CampoCelda">
                                &nbsp;</td>
                            <td class="CampoCelda">
                                F. Nacimiento<asp:RequiredFieldValidator
                                    ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtFechaNac" ErrorMessage="Ingrese la Fecha Nacimiento"
                                    SetFocusOnError="True" Display="None">*</asp:RequiredFieldValidator><asp:CompareValidator ID="CompareValidator3"
                                        runat="server" ControlToValidate="txtFechaNac" ErrorMessage="Ingrese una Fecha Nacimiento válida"
                                        Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" Display="None">*</asp:CompareValidator><asp:RangeValidator
                                            ID="rvFechaN" runat="server" ControlToValidate="txtFechaNac" ErrorMessage="La Fecha de Nacimiento no puede ser mayor a la actual."
                                            MaximumValue="01/01/2010" MinimumValue="01/01/1900" SetFocusOnError="True" Type="Date" Display="None">*</asp:RangeValidator><asp:Label
                                                ID="Label3" runat="server" EnableViewState="False" ForeColor="Red" Text="*"></asp:Label>
                                <cc1:MaskedEditExtender ID="meeFechaNac" runat="server" ClearTextOnInvalid="True"
                                    Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaNac">
                                </cc1:MaskedEditExtender>
                            </td>
                            <td class="CampoCelda" colspan="1">
                                Edad</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="cboParentesco" runat="server" Width="119px">
                                </asp:DropDownList></td>
                            <td>
                                <asp:DropDownList ID="cboTipoDoc" runat="server" Width="121px" onChange="ValidaNroDoc(this.value);">
                                </asp:DropDownList></td>
                            <td colspan="2">
                                <asp:TextBox ID="txtNroDocumento" runat="server" Width="200px" MaxLength="7"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtFechaNac" runat="server" Width="54px" AutoPostBack="True" OnTextChanged="txtFechaNac_TextChanged"></asp:TextBox><asp:TextBox style="BORDER-RIGHT: 0px; PADDING-RIGHT: 0px; BORDER-TOP: 0px; PADDING-LEFT: 0px; FONT-SIZE: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; BORDER-LEFT: 0px; WIDTH: 0px; PADDING-TOP: 0px; BORDER-BOTTOM: 0px; HEIGHT: 0px" id="txtFechaNacAx" runat="server" Width="0px" CssClass="Ocultotxt"></asp:TextBox>
                             
                                </td>
                            <td colspan="1">
                                <asp:UpdatePanel ID="upE" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtEdad" runat="server" Enabled="False" Style="text-align: center"
                                            Width="16px"></asp:TextBox>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtFechaNac" EventName="TextChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="CampoCelda">
                                Apellido Paterno</td>
                            <td class="CampoCelda">
                                Apellido Materno<asp:Label ID="Label5"
                                        runat="server" EnableViewState="False" ForeColor="Red" Text="*"></asp:Label><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"
                                    ControlToValidate="txtApeMat" ErrorMessage="Ingrese Apellido Materno" InitialValue=" "
                                    SetFocusOnError="True" Display="None">*</asp:RequiredFieldValidator></td>
                            <td class="CampoCelda" style="width: 119px">
                                Primer Nombre<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server"
                                    ControlToValidate="txtPrimerNom" ErrorMessage="Ingrese Primer Nombre" InitialValue=" "
                                    SetFocusOnError="True" Display="None">*</asp:RequiredFieldValidator><asp:Label ID="Label4"
                                        runat="server" EnableViewState="False" ForeColor="Red" Text="*"></asp:Label></td>
                            <td class="CampoCelda">
                                Segundo Nombre</td>
                            <td class="CampoCelda" colspan="2">
                                Sexo</td>
                        </tr>
                        <tr>
                            <td style="height: 11px">
                                <asp:TextBox ID="txtApePat" runat="server" Width="110px" SkinID="txtM"></asp:TextBox></td>
                            <td style="height: 11px">
                                <asp:TextBox ID="txtApeMat" runat="server" Width="110px" SkinID="txtM"></asp:TextBox></td>
                            <td style="width: 119px; height: 11px">
                                <asp:TextBox ID="txtPrimerNom" runat="server" Width="110px" SkinID="txtM"></asp:TextBox></td>
                            <td style="height: 11px">
                                <asp:TextBox ID="txtSegundoNom" runat="server" Width="104px" SkinID="txtM"></asp:TextBox></td>
                            <td colspan="2" style="height: 11px">
                                <asp:DropDownList ID="cboSexo" runat="server" Width="117px">
                                    <asp:ListItem Value="M">Masculino</asp:ListItem>
                                    <asp:ListItem Value="F">Femenino</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td class="CampoCelda">
                                Estado Civil</td>
                            <td class="CampoCelda">
                                Teléfono<asp:Label ID="Label6" runat="server" EnableViewState="False" ForeColor="Red"
                                    Text="*"></asp:Label><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                        ControlToValidate="txtTelefono" Display="None" ErrorMessage="Ingrese Teléfono"
                                        InitialValue=" " SetFocusOnError="True">*</asp:RequiredFieldValidator></td>
                            <td class="CampoCelda" colspan="4">
                                Dirección<asp:Label ID="Label7" runat="server" EnableViewState="False" ForeColor="Red"
                                    Text="*"></asp:Label><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                        ControlToValidate="txtDireccion" Display="None" ErrorMessage="Ingrese Dirección"
                                        InitialValue=" " SetFocusOnError="True">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="cboEstadoCivil" runat="server" Width="119px">
                                </asp:DropDownList></td>
                            <td>
                                <asp:TextBox ID="txtTelefono" runat="server" onkeypress="return Numero(event)"  Width="110px" MaxLength="9"></asp:TextBox></td>
                            <td colspan="4">
                                <asp:TextBox ID="txtDireccion" runat="server" Width="362px" SkinID="txtM"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td class="CampoCelda">
                                Localidad</td>
                            <td class="CampoCelda">
                                &nbsp;</td>
                            <td class="CampoCelda" style="width: 119px">
                                &nbsp;</td>
                            <td colspan="3">
                                &nbsp;<asp:CheckBox ID="chkIgual" runat="server" Text="Es igual al contratante" onclick="copiarDatos(this.checked);"  ForeColor="Black" />
                                <asp:HiddenField ID="ID" runat="server" Value="0" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:TextBox ID="TextBox3" runat="server" Width="300px"></asp:TextBox>
                            </td>
                            <td colspan="3" style="text-align: right">
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                                <asp:Button ID="btnGrabar" runat="server" Text="Agregar" Width="72px" />
                                &nbsp;<asp:Button ID="btnCancelar" runat="server" CausesValidation="False" 
                                    OnClick="btnCancelar_Click" Text="Cancelar" Width="70px" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 651px">
                </td>
            </tr>
            <tr>
                <td style="width: 651px">
                    <asp:GridView ID="gvAsegurado" runat="server" AutoGenerateColumns="False" DataKeyNames="ID"
                        OnRowDeleting="gvAsegurado_RowDeleting" OnSelectedIndexChanged="gvAsegurado_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="False">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("ID") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="IDTipoDocumento" HeaderText="IDTipoDocumento" Visible="False" />
                            <asp:BoundField DataField="NroDocumento" HeaderText="N&#186; Documento">
                                <ItemStyle Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Parentesco" HeaderText="Parentesco">
                                <ItemStyle Width="100px" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Apellidos y Nombre">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ApePaterno") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("ApePaterno") + " " +  Eval("ApeMaterno") + " " +  Eval("PrimerNom") + " " +  Eval("SegundoNom") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField ButtonType="Image" SelectImageUrl="~/img/seleccionar.gif" ShowSelectButton="True">
                                <ItemStyle Width="25px" />
                            </asp:CommandField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemStyle Width="25px" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" CommandArgument='<%# Bind("ID") %>'
                                        CommandName="delete" ImageUrl="~/img/Grid/eliminar.gif" OnClientClick="return confirm('Está seguro que desea eliminar el Asegurado?');"
                                        Text="Delete" Visible='<%# Convert.ToBoolean(Convert.ToDecimal(Session("EstForm"))=0) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle Height="23px" />
                        <EmptyDataTemplate>
                            No se ha registrado ningún Asegurado...
                        </EmptyDataTemplate>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td style="width: 651px; text-align: center">
                    <asp:Button ID="btnAceptar" runat="server" OnClientClick="window.parent.cerrarModal(); return false;"
                        Text="Aceptar" />
                    <asp:HiddenField ID="HFIdCertificado" runat="server" Value="0" />
                </td>
            </tr>
            <tr>
                <td style="width: 651px; text-align: left">
                    <asp:Label ID="Label9" runat="server" EnableViewState="False" ForeColor="Red" Text="(*) Campos Obligatorios. "></asp:Label></td>
            </tr>
        </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    
    </div>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0">
            <ProgressTemplate>
                <uc1:Cargando ID="Cargando1" runat="server" />
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
    </form>
</body>
</html>

