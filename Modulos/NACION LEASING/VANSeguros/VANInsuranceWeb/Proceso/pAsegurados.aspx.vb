﻿Imports VAN.InsuranceWeb.BL
Public Class pAsegurados
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            HFIdCertificado.Value = Session("IDCertificado")
            rvFechaN.MaximumValue = Now.Date.ToShortDateString
            CargarTipoDocumento()
            CargarParentesco()
            CargarEstadoCivil()
            Dim objDT As New DataTable()
            If Session("EstForm") = "0" And Session("DTAsegurado") Is Nothing Then 'Trabajando en Memoria
                objDT = CrearTablaAsegurado()
                Session("DTAsegurado") = objDT
            Else
                objDT = Session("DTAsegurado")
            End If
            LlenargvAsegurado(objDT)
            If Session("EstForm") = "0" Then 'Form no grabado aún
                btnGrabar.Enabled = True
            Else
                btnGrabar.Enabled = False
            End If
        End If
    End Sub

    Public Function CrearTablaAsegurado() As DataTable
        Dim objDT As New DataTable("Asegurados")
        objDT.Columns.Add("ID", System.Type.GetType("System.Int32"))
        objDT.Columns("ID").AutoIncrement = True
        objDT.Columns("ID").AutoIncrementSeed = 1
        objDT.Columns.Add("IDTipoDocumento", System.Type.GetType("System.String"))
        objDT.Columns.Add("NroDocumento", System.Type.GetType("System.String"))
        objDT.Columns.Add("IDParentesco", System.Type.GetType("System.String"))
        objDT.Columns.Add("Parentesco", System.Type.GetType("System.String"))
        objDT.Columns.Add("ApePaterno", System.Type.GetType("System.String"))
        objDT.Columns.Add("ApeMaterno", System.Type.GetType("System.String"))
        objDT.Columns.Add("PrimerNom", System.Type.GetType("System.String"))
        objDT.Columns.Add("SegundoNom", System.Type.GetType("System.String"))
        objDT.Columns.Add("Sexo", System.Type.GetType("System.String"))
        objDT.Columns.Add("IDEstadoCivil", System.Type.GetType("System.String"))
        objDT.Columns.Add("Telefono", System.Type.GetType("System.String"))
        objDT.Columns.Add("Direccion", System.Type.GetType("System.String"))
        objDT.Columns.Add("IDDepartamento", System.Type.GetType("System.String"))
        objDT.Columns.Add("IDProvincia", System.Type.GetType("System.String"))
        objDT.Columns.Add("IDDistrito", System.Type.GetType("System.String"))
        objDT.Columns.Add("FechaNac", System.Type.GetType("System.String"))
        Return objDT
    End Function

    Public Sub LlenargvAsegurado(ByVal objDT)
        gvAsegurado.DataSource = objDT
        gvAsegurado.DataBind()
    End Sub

    Public Sub CargarTipoDocumento()
        Dim oBLTipoDoc As New BLListaGenerica
        cboTipoDoc.DataSource = oBLTipoDoc.Seleccionar(Session("IDPRODUCTO"), "TipoDocumento")
        cboTipoDoc.DataTextField = "Descripcion"
        cboTipoDoc.DataValueField = "ID"
        cboTipoDoc.DataBind()
    End Sub

    Public Sub CargarParentesco()
        Dim oBLParentesco As New BLListaGenerica
        cboParentesco.DataSource = oBLParentesco.Seleccionar(Session("IDPRODUCTO"), "Parentesco")
        cboParentesco.DataTextField = "Descripcion"
        cboParentesco.DataValueField = "ID"
        cboParentesco.DataBind()
    End Sub

    Public Sub CargarEstadoCivil()
        cboEstadoCivil.Items.Add(New ListItem("Soltero", "S"))
        cboEstadoCivil.Items.Add(New ListItem("Casado", "C"))
        cboEstadoCivil.Items.Add(New ListItem("Viudo", "V"))
        cboEstadoCivil.Items.Add(New ListItem("Divorciado", "D"))
    End Sub


    Protected Sub btnGrabar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        ' ''Dim oBLAse As New BLAsegurado
        ' ''Valida Edad entre 18 a 60 Años
        ''Dim Edad As Double = CalcularEdad(CDate(txtFechaNac.Text))
        ''If (Edad < 18) Or (Edad > 65) Then
        ''    ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M", "alert('El Asegurado debe tener entre 18 a 65 años.');", True)
        ''    Exit Sub
        ''End If


        '---------
        'Valida NroDoc
        'If Not oBLAse.ValidarNroDocumento(HFIdCertificado.Value, IDPRODUCTO, txtNroDocumento.Text.Trim) Then
        '    ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M", "alert('Ya existe un Asegurado con los datos ingresados');", True)
        '    Exit Sub
        'End If
        '---------

        Dim NroAsegurados As Int32 = CInt(Session("NroAsegurados"))

        Dim objDT As New DataTable()
        objDT = Session("DTAsegurado")
        If objDT Is Nothing Then
            objDT = CrearTablaAsegurado()
        End If
        Dim blnNuevo As Boolean = True
        For Each objDR As DataRow In objDT.Rows
            If objDR("ID") = CInt(ID.Value) Then
                objDR("IDTipoDocumento") = cboTipoDoc.SelectedValue
                objDR("NroDocumento") = txtNroDocumento.Text.Trim
                objDR("IDParentesco") = cboParentesco.SelectedValue
                objDR("Parentesco") = cboParentesco.SelectedItem.Text
                objDR("ApePaterno") = txtApePat.Text.Trim.ToUpper
                objDR("ApeMaterno") = txtApeMat.Text.Trim.ToUpper
                objDR("PrimerNom") = txtPrimerNom.Text.Trim.ToUpper
                objDR("SegundoNom") = txtSegundoNom.Text.Trim.ToUpper
                objDR("FechaNac") = txtFechaNac.Text
                objDR("Direccion") = txtDireccion.Text.Trim.ToUpper
                objDR("Sexo") = cboSexo.SelectedValue
                objDR("IDEstadoCivil") = cboEstadoCivil.SelectedValue
                objDR("Telefono") = txtTelefono.Text.Trim

                blnNuevo = False
                Exit For
            End If
        Next
        If blnNuevo Then
            If objDT.Rows.Count < NroAsegurados Then
                Dim objDR As DataRow = objDT.NewRow()
                objDR("IDTipoDocumento") = cboTipoDoc.SelectedValue
                objDR("NroDocumento") = txtNroDocumento.Text.Trim
                objDR("IDParentesco") = cboParentesco.SelectedValue
                objDR("Parentesco") = cboParentesco.SelectedItem.Text
                objDR("ApePaterno") = txtApePat.Text.Trim.ToUpper
                objDR("ApeMaterno") = txtApeMat.Text.Trim.ToUpper
                objDR("PrimerNom") = txtPrimerNom.Text.Trim.ToUpper
                objDR("SegundoNom") = txtSegundoNom.Text.Trim.ToUpper
                objDR("FechaNac") = txtFechaNac.Text
                objDR("Direccion") = txtDireccion.Text.Trim.ToUpper
                objDR("Sexo") = cboSexo.SelectedValue
                objDR("IDEstadoCivil") = cboEstadoCivil.SelectedValue
                objDR("Telefono") = txtTelefono.Text.Trim

                objDT.Rows.Add(objDR)
            Else
                ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M", "alert('Sólo puede registrar " & NroAsegurados & " Asegurado(s).');", True)
                Exit Sub
            End If
        End If
        LlenargvAsegurado(objDT)
        Session("DTAsegurado") = objDT
        LimpiarControles()
        gvAsegurado.SelectedIndex = -1
        ID.Value = 0
    End Sub
    Public Sub LimpiarControles()
        cboTipoDoc.SelectedIndex = 0
        txtNroDocumento.Text = ""
        cboParentesco.SelectedIndex = 0
        txtApePat.Text = ""
        txtApeMat.Text = ""
        txtPrimerNom.Text = ""
        txtSegundoNom.Text = ""
        txtFechaNac.Text = ""
        txtDireccion.Text = ""
        cboSexo.SelectedIndex = 0
        cboEstadoCivil.SelectedIndex = 0
        txtTelefono.Text = ""
        txtEdad.Text = ""
    End Sub

    Protected Sub gvAsegurado_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        Dim objDT As New DataTable()
        objDT = Session("DTAsegurado")
        objDT.Rows(e.RowIndex).Delete()
        LlenargvAsegurado(objDT)
        gvAsegurado.SelectedIndex = -1
        Session("DTAsegurado") = objDT
    End Sub

    Protected Sub gvAsegurado_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ID.Value = gvAsegurado.SelectedDataKey.Item(0)
        Dim objDT As New DataTable()
        objDT = Session("DTAsegurado")
        Dim objDR As DataRow = objDT.NewRow()
        objDR = objDT.Rows(gvAsegurado.SelectedIndex)
        txtNroDocumento.Text = objDR("NroDocumento")
        cboTipoDoc.SelectedValue = objDR("IDTipoDocumento")
        cboParentesco.SelectedValue = objDR("IDParentesco")
        txtApePat.Text = objDR("ApePaterno")
        txtApeMat.Text = objDR("ApeMaterno")
        txtPrimerNom.Text = objDR("PrimerNom")
        txtSegundoNom.Text = objDR("SegundoNom")
        txtFechaNac.Text = objDR("FechaNac")
        txtDireccion.Text = objDR("Direccion")
        cboSexo.SelectedValue = objDR("Sexo")
        cboEstadoCivil.SelectedValue = objDR("IDEstadoCivil")
        txtTelefono.Text = objDR("Telefono")

    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        LimpiarControles()
        ID.Value = 0
        gvAsegurado.SelectedIndex = -1
    End Sub

    Protected Sub txtFechaNac_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFechaNac.TextChanged
        If Not IsDate(txtFechaNac.Text) Then
            txtEdad.Text = "0"
        Else
            txtEdad.Text = Math.Truncate(CalcularEdad(CDate(txtFechaNac.Text)))
        End If
        ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "focus", "ftxtApePat();", True)
    End Sub

    Public Function CalcularEdad(ByVal FechaNacimiento As DateTime) As Double
        Dim dblEdad As Double = DateTime.Now.Subtract(FechaNacimiento).TotalDays / 365.25
        Return dblEdad
    End Function

End Class