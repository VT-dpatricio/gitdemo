﻿Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.BL
Imports System.Drawing

Public Class SolicitudesContacto
    Inherits PaginaBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session(SESSION_BEUSUARIO) Is Nothing Then
            Response.Redirect("~/Login.aspx")
        End If

        If Not IsPostBack Then
            If ValidarEstadoClaveUsuario() Then
                Response.Redirect("~/Seguridad/CambiarClave.aspx", False)
            End If
        End If

        Dim objBEFiltroSolicitudContacto As New BEFiltroSolicitudContacto
        Dim objBLSolicitudesContacto As New BLSolicitudConctactoCliente

        objBEFiltroSolicitudContacto.IDUsuario = DirectCast(Session(SESSION_BEUSUARIO), BEUsuario).IDUsuario
        gvListaSolicitudesContacto.DataSource = objBLSolicitudesContacto.ListarSolicitudesContacto(objBEFiltroSolicitudContacto)
        gvListaSolicitudesContacto.DataBind()
    End Sub

    Sub gvListaSolicitudesContacto_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)


        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim cellColorEstado As TableCell = e.Row.Cells(GetColumnIndexByName(e.Row, "ColorEstado"))
            cellColorEstado.Visible = False
            Dim colorEstado As Color = ColorTranslator.FromHtml(cellColorEstado.Text)

            Dim cellEstado As TableCell = e.Row.Cells(GetColumnIndexByName(e.Row, "Estado"))
            cellEstado.BackColor = colorEstado

            cellEstado.HorizontalAlign = HorizontalAlign.Center

            Dim cellContactadoDias As TableCell = e.Row.Cells(GetColumnIndexByName(e.Row, "Solicitado hace (Días)"))
            cellContactadoDias.HorizontalAlign = HorizontalAlign.Center


        End If

        If e.Row.RowType = DataControlRowType.Header Then
            Dim cellColorEstado As TableCell = e.Row.Cells(GetColumnIndexByName(e.Row, "ColorEstado"))
            cellColorEstado.Visible = False
        End If

    End Sub


    Private Function GetColumnIndexByName(row As GridViewRow, columnName As String) As Integer
        Dim columnIndex As Integer = 0
        For Each cell As DataControlFieldCell In row.Cells
            If TypeOf cell.ContainingField Is BoundField Then
                If DirectCast(cell.ContainingField, BoundField).DataField.Equals(columnName) Then
                    Exit For
                End If
            End If
            ' keep adding 1 while we don't have the correct name
            columnIndex += 1
        Next
        Return columnIndex
    End Function

End Class