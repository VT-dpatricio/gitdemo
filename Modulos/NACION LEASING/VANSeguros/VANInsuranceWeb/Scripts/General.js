﻿
function SoloNumeros() {
    alert('a');
    var key = window.event.keyCode;
    if (key < 48 || key > 57) {
        window.event.keyCode = 0;
    }
}

function SoloNumeros(evt) {

    var key = (evt.which) ? evt.which : event.keyCode;
    if ((key < 48 || key > 57)) {
        return false;
    } else {
        return true;
    }
}


function SoloNumerosPunto(evt) {

    var key = (evt.which) ? evt.which : event.keyCode;
    if ((key < 45 || key > 57)) {
        return false;
    } else {
        return true;
    }
}



function validarNumeroEntero(obj) {
    txt = obj.value;
    if (parseInt(txt) != parseFloat(txt)) {
        alert('Sólo números enteros');
        obj.value = ""
        obj.focus();
    }
}


function regresar(pagina) {
    alert(pagina);
    window.location = pagina;
}

function RetornarValor() {
    window.returnValue = document.all['txtProductoJanus'].value;
}

function Seleccion(codigo, nombre) {
    document.getElementById('txtProductoJanus').value = codigo + '@' + nombre;
}



function AbrirBusquedaInfoProducto(IdInfoProducto, IdTipo) {
    var cadena;
    //var ConfiguracionPagina = 'center:yes;resizable:no;help:no;status:no;Width:650px;Height:380px';
    var ConfiguracionPagina = 'center:yes;resizable:no;location:no;menubar:no;help:no;status:no;dialogWidth:650px;dialogHeight:450px';
    var Pagina = '../Janus/InfoJanus.aspx?IdTipo=' + IdInfoProducto + '&IdInfoProducto=' + IdTipo

    cadena = window.showModalDialog(Pagina, null, ConfiguracionPagina);
    __doPostBack('BDJanusInfoProducto');
    //location.reload();     
}

function ValidacionEntObj(Tipo, Min, Max) {
    //alert(Tipo + Min + Max);
}


function AbrirProductoJanus() {
    var cadena;
    //var ConfiguracionPagina = 'center:yes;resizable:no;help:no;status:no;Width:650px;Height:380px';
    var ConfiguracionPagina = 'center:yes;resizable:no;location:no;menubar:no;help:no;status:no;dialogWidth:650px;dialogHeight:450px';
    var Pagina = '../Janus/ProductoJanus.aspx'

    //window.open(Pagina, null, null);
    cadena = window.showModalDialog(Pagina, null, ConfiguracionPagina);
    //alert(cadena);
    //if (!isNaN(cadena)){
    //cadena = '';
    //}
    //document.ElementById('ctl00_cphMaster_txtAbreviatura').value = cadena;
    var arreglo = cadena.split('@');

    document.all['ctl00_cphMaster_txtAbreviatura'].value = arreglo[0];
    document.all['ctl00_cphMaster_txtDescripcion'].value = arreglo[1];
}



function AbrirModalBais(page, pos) {
    var cadena;
    //var ConfiguracionPagina = 'center:yes;resizable:no;help:no;status:no;Width:650px;Height:380px';
    var ConfiguracionPagina = 'center:yes;resizable:no;location:no;menubar:no;help:no;status:no;dialogWidth:650px;dialogHeight:450px';
    var Pagina = 'dialog/' + page + '.aspx?id=' + pos

    //window.open(Pagina, null, null);
    cadena = window.showModalDialog(Pagina, null, ConfiguracionPagina);

    __doPostBack(page);

    //alert(cadena);
    //if (!isNaN(cadena)){
    //cadena = '';
    //}
    //document.ElementById('ctl00_cphMaster_txtAbreviatura').value = cadena;



}




function AbrirInfoProductoDetalle(IdInfoProducto, IdTipo) {
    var cadena;
    //var ConfiguracionPagina = 'center:yes;resizable:no;help:no;status:no;Width:650px;Height:380px';
    var ConfiguracionPagina = 'center:yes;resizable:no;location:no;menubar:no;help:no;status:no;dialogWidth:650px;dialogHeight:450px';
    var Pagina = 'DetalleInfoProducto.aspx?IdInfoProducto=' + IdInfoProducto + '&IdTipo=' + IdTipo;

    //cadena = window.open(Pagina, null, null);

    window.location = Pagina;

    //__doPostBack('BDJanusInfoProducto');
    //location.reload();

}

function AbrirInfoProductoModal(id, tipo) {
    var cadena;
    //var ConfiguracionPagina = 'center:yes;resizable:no;help:no;status:no;Width:650px;Height:380px';
    var ConfiguracionPagina = 'center:yes;resizable:no;location:no;menubar:no;help:no;status:no;dialogWidth:650px;dialogHeight:450px';
    var pagina = '';

    if (tipo == 'True') {
        pagina = 'InfoProductoCertficadoPanelModal.aspx?IdInfoProducto=' + id;
        window.showModalDialog(pagina, null);
    }
    else if (tipo == 'False') {
        pagina = 'InfoProductoCertificadoDetalleModal.aspx?IdInfoProducto=' + id;
        window.showModalDialog(pagina, null);
    }
}

function DeshabilitarClickDerecho() {
    return false;
}

function ConfirmarEliminacion(Boton) {
    if (BuscarRadioChecked(Boton) == true) {
        var Posicion = (Boton.src).indexOf("Off");
        if (Posicion == -1) {
            if (confirm("Esta seguro que desea eliminar el registro seleccionado ?") == true) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}

function ValidaRequestURL(paginaOrigen, paginaDestino) {
    if (document.referrer.indexOf(paginaOrigen) == -1) {
        if (document.referrer.indexOf(paginaDestino) == -1) {
            alert('No esta autorizado para acceder a la pagina!');
            window.history.back();
        }
    }
}


function AbrirProyectoReporte(Id, Tipo, Co, Ex) {
    var cadena;
    var ConfiguracionPagina = 'toolbar=no,menubar=no,directories=no';
    var Pagina = '../Reportes/Reports.aspx?Id=' + Id + '&Tipo=' + Tipo;
    //window.open(Pagina,'Reports',ConfiguracionPagina);
    //    alert(Co);
    //    alert(Ex);
    //    if (Co == 0 || Ex == 0) { 
    //    alert('Este presupuesto no tiene movimientos asignados, no se podra mostrar el reporte!');
    //       }
    //    else{
    window.open(Pagina, 'Reports', 'height=600,width=900,left=10,top=10,resizable=no,scrollbars=yes,toolbar=yes,status=yes,center=yes');
    //    }

    //cadena = window.open(Pagina+self.location,'Reports','left=20,top=20,width=850,height=700,toolbar=yes,resizable=0');
    //cadena = window.open(Pagina,'Reports',null);
}


function ProductoEntidadModal(IdProducto, IdEntidad) {
    //var ConfiguracionPagina = 'toolbar=no,menubar=no,directories=no';
    //var Pagina = '../Reportes/Reports.aspx?Id='+ Id +'&Tipo='+ Tipo ;


    var ConfiguracionPagina = 'center:yes;resizable:no;location:no;menubar:no;help:no;status:no;dialogWidth:850px;dialogHeight:450px';
    var Pagina = 'ProductoEntidadValidacion.aspx?IdProducto=' + IdProducto + '&IdEntidad=' + IdEntidad

    if (IdProducto != '') {
        window.showModalDialog(Pagina, null, ConfiguracionPagina);
        return true;
    } else {
        alert('Debe de grabar para poder agregar entidades');
        return false;
    }
}

function ProductoInfoProdModal(IdProducto, IdInfoProducto, Ocultar) {
    //var ConfiguracionPagina = 'toolbar=no,menubar=no,directories=no';
    //var Pagina = '../Reportes/Reports.aspx?Id='+ Id +'&Tipo='+ Tipo ;    
    if (IdProducto != '') {
        window.showModalDialog('DetalleInfoProducto.aspx?IdProducto=' + IdProducto + '&IdInfoProducto=' + IdInfoProducto + '&ocultar=' + Ocultar, null);
        return true;
    } else {
        alert('Debe de grabar para poder agregar info producto');
        return false;
    }
}


function AbrirVentanaProyecto(ItemSubmitted, idProyecto, idTipoProyecto, idBudgetYr) {
    var cadena;
    //var ConfiguracionPagina = 'center:yes;resizable:no;help:no;status:no;Width:650px;Height:380px';
    var ConfiguracionPagina = 'center:yes;resizable:no;help:no;status:no;dialogWidth:650px;dialogHeight:450px';
    var Pagina = 'Proyectos.aspx?ItemSubmitted=' + ItemSubmitted + '&idProyecto=' + idProyecto + "&idTipoProyecto=" + idTipoProyecto + "&idBudgetYr=" + idBudgetYr;
    //cadena = window.open(Pagina, null, ConfiguracionPagina);    
    cadena = window.showModalDialog(Pagina, null, ConfiguracionPagina);
    __doPostBack('Proy');
}



function ConfirmarDatosPadre(Boton, IdControl) {
    if (BuscarRadioCheckedExcluir(Boton, IdControl) == true) {
        var Posicion = (Boton.src).indexOf("Off");
        if (Posicion == -1) {
            if (confirm("Esta seguro que desea realizar esta operacion, con el registro seleccionado ?") == true) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}



function AbrirVentanaActividadProyecto_Control(Boton, IdControl, ItemSubmitted, idProyecto, idTipoActividad, idActividad) {
    if (BuscarRadioCheckedExcluir_Proyecto(Boton, IdControl) == true) {
        var Posicion = (Boton.src).indexOf("Off");
        if (Posicion == -1) {
            AbrirVentanaActividadProyecto(ItemSubmitted, idProyecto, idTipoActividad, idActividad);
        }
    }
}

function AbrirVentanaActividadProyecto(ItemSubmitted, idProyecto, idTipoActividad, idActividad) {
    var cadena;
    //var ConfiguracionPagina = 'center:yes;resizable:no;help:no;status:no;Width:650px;Height:380px';    
    var ConfiguracionPagina = 'center:yes;resizable:no;help:no;status:no;dialogWidth:650px;dialogHeight:380px';
    var Pagina = 'ProyectoActividad.aspx?ItemSubmitted=' + ItemSubmitted + '&idProyecto=' + idProyecto + '&idTipoActividad=' + idTipoActividad + '&idActividad=' + idActividad;

    //cadena = window.open(Pagina, null, ConfiguracionPagina);   
    cadena = window.showModalDialog(Pagina, null, ConfiguracionPagina);
    if (ItemSubmitted == 'Agregar') {
        __doPostBack('Ag');
    } else {
        __doPostBack('Obj');
    }

}

function AbrirVentanaInformacionVoucher(ItemSubmitted, idTransaccion, idTipoMov, idTipoFondo, NoBook, idBudgetYr) {

    var cadena;
    //var ConfiguracionPagina = 'center:yes;resizable:no;help:no;status:no;Width:650px;Height:380px';
    var ConfiguracionPagina = 'center:yes;resizable:no;help:no;status:no;dialogWidth:650px;dialogHeight:380px';
    var Pagina = 'DetailsInformations.aspx?ItemSubmitted=' + ItemSubmitted + '&idTransaccion=' + idTransaccion + '&idTipoMov=' + idTipoMov + '&idTipoFondo=' + idTipoFondo + '&NoBook=' + NoBook + '&idBudgetYr=' + idBudgetYr;

    cadena = window.showModalDialog(Pagina, null, ConfiguracionPagina);
    //cadena = window.open(Pagina, null, ConfiguracionPagina);

    if (ItemSubmitted == 'Agregar') {
        __doPostBack('Ag1');
    } else {
        __doPostBack('Obj1');
    }
}

function AbrirVentanaDetalleVoucher_Control(Boton, IdControl, ItemSubmitted, idTransaccion, idVoucher, Moneda, TipoVoucher) {
    if (BuscarRadioCheckedExcluir_Proyecto(Boton, IdControl) == true) {
        var Posicion = (Boton.src).indexOf("Off");
        if (Posicion == -1) {
            AbrirVentanaDetalleVoucher(ItemSubmitted, idTransaccion, idVoucher, Moneda, TipoVoucher);
        }
    }
}


function AbrirVentanaDetalleVoucher(ItemSubmitted, idTransaccion, idVoucher, Moneda, TipoVoucher) {
    var cadena;
    //var ConfiguracionPagina = 'center:yes;resizable:no;help:no;status:no;Width:750px;Height:300px';
    var ConfiguracionPagina = 'center:yes;resizable:no;help:no;status:no;dialogWidth:800px;dialogHeight:380px';
    var Pagina = 'DetaVoucherS.aspx?ItemSubmitted=' + ItemSubmitted + '&idTransaccion=' + idTransaccion + '&idVoucher=' + idVoucher + '&Moneda=' + Moneda + '&TipoVoucher=' + TipoVoucher;

    cadena = window.showModalDialog(Pagina, null, ConfiguracionPagina);
    //cadena = window.open(Pagina, null, ConfiguracionPagina);      
    if (ItemSubmitted == 'Agregar') {
        __doPostBack('Ag2');
    } else {
        __doPostBack('Obj2');
    }


}


function AbrirVentanaDetalleCashAdvance_Control(Boton, IdControl, ItemSubmitted, idTransaccion, idVoucher, Moneda) {
    if (BuscarRadioCheckedExcluir_Proyecto(Boton, IdControl) == true) {
        var Posicion = (Boton.src).indexOf("Off");
        if (Posicion == -1) {
            AbrirVentanaDetalleCashAdvance(ItemSubmitted, idTransaccion, idVoucher, Moneda);
        }
    }
}

function AbrirVentanaDetalleCashAdvance(ItemSubmitted, idTransaccion, idVoucher, Moneda) {
    var cadena;
    //var ConfiguracionPagina = 'center:yes;resizable:no;help:no;status:no;Width:750px;Height:300px';
    var ConfiguracionPagina = 'center:yes;resizable:no;help:no;status:no;dialogWidth:800px;dialogHeight:380px';
    var Pagina = 'CashAdvance.aspx?ItemSubmitted=' + ItemSubmitted + '&idTransaccion=' + idTransaccion + '&idVoucher=' + idVoucher + '&Moneda=' + Moneda;
    cadena = window.showModalDialog(Pagina, null, ConfiguracionPagina);
    //cadena = window.open(Pagina, null, ConfiguracionPagina);

    if (ItemSubmitted == 'Agregar') {
        __doPostBack('Ag2');
    } else {
        __doPostBack('Obj2');
    }
}


function AbrirVentanaDetalleRendition_Control(Boton, IdControl, ItemSubmitted, idTransaccion, idVoucher, Moneda, IdTipoMov) {
    if (BuscarRadioCheckedExcluir_Proyecto(Boton, IdControl) == true) {
        var Posicion = (Boton.src).indexOf("Off");
        if (Posicion == -1) {
            AbrirVentanaDetalleRendition(ItemSubmitted, idTransaccion, idVoucher, Moneda, IdTipoMov);
        }
    }
}

function AbrirVentanaDetalleRendition(ItemSubmitted, idTransaccion, idVoucher, Moneda, IdTipoMov) {
    var cadena;
    //var ConfiguracionPagina = 'center:yes;resizable:no;help:no;status:no;Width:750px;Height:300px';
    var ConfiguracionPagina = 'center:yes;resizable:no;help:no;status:no;dialogWidth:800px;dialogHeight:380px';
    var Pagina = 'Rendicion_bk.aspx?ItemSubmitted=' + ItemSubmitted + '&idTransaccion=' + idTransaccion + '&idVoucher=' + idVoucher + '&Moneda=' + Moneda + '&Idtipomov=' + IdTipoMov;
    //cadena = window.open(Pagina, null, ConfiguracionPagina);
    cadena = window.showModalDialog(Pagina, null, ConfiguracionPagina);

    if (ItemSubmitted == 'Agregar') {
        __doPostBack('Ag2');
    } else {
        __doPostBack('Obj2');
    }
}

function AbrirVentana() {
    var cadena;
    var ConfiguracionPagina = 'center:yes;resizable:no;help:no;status:no;dialogWidth:750px;dialogHeight:480px';
    var Pagina = '../pagina.htm';
    cadena = window.showModalDialog(Pagina, null, ConfiguracionPagina);
    //cadena = window.showModalDialog(Pagina, null, ConfiguracionPagina);

    if (cadena != null) {
        __doPostBack('Obj');
    }


}

function BuscarRadioChecked(Boton) {
    if ((Boton.src).indexOf("Off") == -1) {
        var f = document.forms.length;
        var i = 0;
        var pos = -1;
        while (pos == -1 && i < f) {
            var e = document.forms[i].elements.length;
            var j = 0;
            var k = 0;
            while (pos == -1 && j < e) {
                if (document.forms[i].elements[j].type == 'radio') {
                    if (document.forms[i].elements[j].checked == true)
                        return true;
                    k++;
                }
                j++;
            }
            i++;
        }
        if (k > 0) {
            alert('Debe seleccionar un registro!');
        }
        else {
            alert('No hay registros!');
        }
        return false;
    }
    else {
        return false;
    }
}


function BuscarRadioChecked2(Boton, ItemSubmitted, idProyecto, idTipoActividad, idActividad) {
    if ((Boton.src).indexOf("Off") == -1) {
        var f = document.forms.length;
        var i = 0;
        var pos = -1;
        while (pos == -1 && i < f) {
            var e = document.forms[i].elements.length;
            var j = 0;
            var k = 0;
            while (pos == -1 && j < e) {
                if (document.forms[i].elements[j].type == 'radio') {
                    if (document.forms[i].elements[j].checked == true)
                        return true;
                    if (idActividad != "") {
                        AbrirVentanaActividadProyecto(ItemSubmitted, idProyecto, idTipoActividad, idActividad);
                    } else {
                        alert('Select a Activity Child!');
                    }
                    return false;
                    k++;
                }
                j++;
            }
            i++;
        }
        if (k > 0) {
            alert('Select a Activity Child!');
        }
        else {
            alert('No Hay Data!');
        }
        return false;
    }
    else {
        return false;
    }
}


function EjecutarValidators() {
    if (typeof (Page_ClientValiDateTime) == 'function') {
        Page_ClientValiDateTime();
    }
}


function Mayusculas(obj) {
    obj.value = obj.value.toUpperCase();
}

function MontoConversion(Caja, TipoCambio) {
    alert(TipoCambio);
    var numero = parseDouble(Caja.value);
    if (isNaN(numero)) {
        document.getElementById('txtUSAmount').value = 0;
    } else {
        document.getElementById('txtUSAmount').value = TipoCambio * numero;
    }
}

function MontoConversionv2() {

    var numero = document.getElementById('txtLCAmount').value;
    var tipocambio = document.getElementById('txtTipoCambio').value;
    if (isNaN(numero)) {
        document.getElementById('txtUSAmount').value = 0;
    } else {
        if (numero > 0) {
            var objNumero = new oNumero(numero / tipocambio);
            document.getElementById('txtUSAmount').value = objNumero.formato(2, false);
        } else {
            document.getElementById('txtUSAmount').value = 0;
        }
    }
}



function BuscarEliminar(Boton) {
    if ((Boton.src).indexOf("Off") == -1) {
        var f = document.forms.length;
        var i = 0;
        var pos = -1;
        while (pos == -1 && i < f) {
            var e = document.forms[i].elements.length;
            var j = 0;
            var k = 0;
            while (pos == -1 && j < e) {
                if (document.forms[i].elements[j].type == 'radio') {
                    if (document.forms[i].elements[j].checked == true)
                        if (confirm("Esta seguro de eliminar")) {
                        return true;
                    }
                    else {
                        return false;
                    }
                    k++;
                }
                j++;
            }
            i++;
        }
        if (k > 0) {
            alert('Debe seleccionar un registro!');
        }
        else {
            alert('No hay registros!');
        }
        return false;
    }
    else {
        return false;
    }
}






function PrincipalProyecto(Boton, ItemSubmitted, idProyecto, idTipoProyecto, idBudgetYr, IdControl) {
    if (BuscarRadioCheckedExcluir(Boton, IdControl) == true) {
        var Posicion = (Boton.src).indexOf("Off");
        if (Posicion == -1) {
            AbrirVentanaProyecto(ItemSubmitted, idProyecto, idTipoProyecto, idBudgetYr);
        }
        else {
            return false;
        }
        //    }
        //    else{
        //        return false;
    }
}



function DetalleProyecto(Boton, ItemSubmitted, idProyecto, idTipoActividad, idActividad, IdControl) {
    if (BuscarRadioCheckedExcluir(Boton, IdControl) == true) {
        var Posicion = (Boton.src).indexOf("Off");
        if (Posicion == -1) {
            AbrirVentanaActividadProyecto(ItemSubmitted, idProyecto, idTipoActividad, idActividad);

        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}



function PrincipalMovimiento(Boton, ItemSubmitted, idTransaccion, idTipoMov, idTipoFondo, NoBook, idBudgetYr, IdControl) {
    if (BuscarRadioCheckedExcluir(Boton, IdControl) == true) {
        var Posicion = (Boton.src).indexOf("Off");
        if (Posicion == -1) {
            AbrirVentanaInformacionVoucher(ItemSubmitted, idTransaccion, idTipoMov, idTipoFondo, NoBook, idBudgetYr);
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}

function PrincipalMovimientoDetalle(Boton, ItemSubmitted, IdTransaccion, IdVoucher, IdControl, Moneda, TipoVoucher) {
    if (BuscarRadioCheckedExcluir(Boton, IdControl) == true) {
        var Posicion = (Boton.src).indexOf("Off");
        if (Posicion == -1) {
            AbrirVentanaDetalleVoucher(ItemSubmitted, IdTransaccion, IdVoucher, Moneda, TipoVoucher);
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}


function ConfirmarDatosPadre(Boton, IdControl) {

    if (BuscarRadioCheckedExcluir(Boton, IdControl) == true) {
        var Posicion = (Boton.src).indexOf("Off");
        if (Posicion == -1) {
            if (confirm("Esta seguro que desea realizar esta operacion, con el registro seleccionado ?") == true) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}



function ConfirmarEliminar(Boton, IdControl) {

    if (BuscarRadioCheckedExcluir(Boton, IdControl) == true) {
        var Posicion = (Boton.src).indexOf("Off");
        if (Posicion == -1) {
            if (confirm("Esta seguro que desea realizar esta operacion, con el registro seleccionado ?") == true) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}


function ConvertirDolares() {
    alert('hola me salio');
}




function BuscarRadioCheckedExcluir(Boton, IdControl) {
    if ((Boton.src).indexOf("Off") == -1) {
        var f = document.forms.length;
        var i = 0;
        var pos = -1;
        var Cadena;
        while (pos == -1 && i < f) {
            var e = document.forms[i].elements.length;
            var j = 0;
            var k = 0;
            while (pos == -1 && j < e) {
                if (document.forms[i].elements[j].type == 'radio') {
                    Cadena = document.forms[i].elements[j].id.split('_');
                    if (Cadena[2] == IdControl) {
                        if (document.forms[i].elements[j].checked == true)
                            return true;
                        k++;
                    }
                }
                j++;
            }
            i++;
        }
        if (k > 0) {
            alert('Debe seleccionar un registro!');
        }
        else {
            alert('No hay registros!');
        }
        return false;
    }
    else {
        return false;
    }
}


function DeshabilitarCombo() {
    if (document.getElementById("ctl00_cphMaster_chkTodos").checked == true) {
        //ctl00_cphMaster_ddlProject
        //document.forms['sub2']['sele2'].disabled=false;
        document.getElementById("ctl00_cphMaster_ddlProject").disabled = false;
    }
    else {
        document.getElementById("ctl00_cphMaster_ddlProject").disabled = false;
    }
}


function BuscarRadioCheckedExcluir_Proyecto(Boton, IdControl) {
    if ((Boton.src).indexOf("Off") == -1) {
        var f = document.forms.length;
        var i = 0;
        var pos = -1;
        var Cadena;
        while (pos == -1 && i < f) {
            var e = document.forms[i].elements.length;
            var j = 0;
            var k = 0;
            while (pos == -1 && j < e) {
                if (document.forms[i].elements[j].type == 'radio') {
                    Cadena = document.forms[i].elements[j].id.split('_');
                    if (Cadena[2] == IdControl) {
                        if (document.forms[i].elements[j].checked == true)
                            return true;
                        k++;
                    }
                }
                j++;
            }
            i++;
        }
        if (k > 0) {
            alert('Debe seleccionar un registro!');
        }
        else {
            alert('Debe seleccionar un registro, para agregar detalles!');
        }
        return false;
    }
    else {
        return false;
    }
}



function oNumero(numero) {
    //Propiedades 
    this.valor = numero || 0
    this.dec = -1;
    //Métodos 
    this.formato = numFormat;
    this.ponValor = ponValor;
    //Definición de los métodos 
    function ponValor(cad) {
        if (cad == '-' || cad == '+') return
        if (cad.length == 0) return
        if (cad.indexOf('.') >= 0)
            this.valor = parseFloat(cad);
        else
            this.valor = parseInt(cad);
    }
    function numFormat(dec, miles) {
        var num = this.valor, signo = 3, expr;
        var cad = "" + this.valor;
        var ceros = "", pos, pdec, i;
        for (i = 0; i < dec; i++)
            ceros += '0';
        pos = cad.indexOf('.')
        if (pos < 0)
            cad = cad + "." + ceros;
        else {
            pdec = cad.length - pos - 1;
            if (pdec <= dec) {
                for (i = 0; i < (dec - pdec); i++)
                    cad += '0';
            }
            else {
                num = num * Math.pow(10, dec);
                num = Math.round(num);
                num = num / Math.pow(10, dec);
                cad = new String(num);
            }
        }
        pos = cad.indexOf('.')
        if (pos < 0) pos = cad.lentgh
        if (cad.substr(0, 1) == '-' || cad.substr(0, 1) == '+')
            signo = 4;
        if (miles && pos > signo)
            do {
            expr = /([+-]?\d)(\d{3}[\.\,]\d*)/
            cad.match(expr)
            cad = cad.replace(expr, RegExp.$1 + ',' + RegExp.$2)
        }
        while (cad.indexOf(',') > signo)
        if (dec < 0) cad = cad.replace(/\./, '')
        return cad;
    }
}