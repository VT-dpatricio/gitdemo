﻿Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.BL
Partial Public Class MasterPage
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not HttpContext.Current.Request.IsAuthenticated Then
            Response.Redirect("~/Login.aspx")
        End If
        Page.Header.DataBind()
        If Not IsPostBack Then
            Try
                lblUsuario.Text = Session("NombreUsuario").ToString & " (" & Session("IDUsuario").ToString & ")"
            Catch ex As Exception
                Response.Redirect("~/Login.aspx")
            End Try

            CargarMenu()
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim MyFirstCtrl As Control = Page.Header.FindControl("FirstCtrlID")
        Page.Header.Controls.Remove(MyFirstCtrl)
        Page.Header.Controls.AddAt(0, MyFirstCtrl)
    End Sub
    

    Private Sub CargarMenu()
        Dim oBLMenu As New BLMenu
        Dim Lista As ArrayList = oBLMenu.ListarMenu(Session("IDUsuario").ToString)
        Dim sMenu As String
        Dim sSubMenu As String = ""
        Dim IDSubMenu As String = ""
        sMenu = "<ul>" & vbCrLf
        For Each oBEMenu As BEMenu In Lista
            If (oBEMenu.IDMenuPadre = 0) Then
                IDSubMenu = ""
                If NroSubMenu(oBEMenu.IDMenu, Lista) > 0 Then
                    IDSubMenu = "rel=""ddsubmenu" & oBEMenu.IDMenu & """"
                    sSubMenu &= "<ul id=""ddsubmenu" & oBEMenu.IDMenu & """ class=""ddsubmenustyle"">" & vbCrLf
                    sSubMenu = Me.AgregarSubMenuSis(Lista, sSubMenu, oBEMenu.IDMenu)
                    sSubMenu &= "</ul>" & vbCrLf
                End If
                sMenu &= "<li><a href=""" & Page.ResolveClientUrl(oBEMenu.Url) & """ " & IDSubMenu & " >" & oBEMenu.Descripcion & "</a></li>" & vbCrLf
            End If
        Next oBEMenu
        sMenu &= "</ul>" & vbCrLf
        ltMenu.Text = sMenu
        ltSMenu.Text = sSubMenu
    End Sub

    Private Function AgregarSubMenuSis(ByVal Lista As ArrayList, ByVal sSubMenu As String, ByVal IDMenu As Integer) As String
        For Each oBEMenu As BEMenu In Lista
            If (oBEMenu.IDMenuPadre = IDMenu) Then
                sSubMenu &= "<li><a href=""" & Page.ResolveClientUrl(oBEMenu.Url) & """>" & oBEMenu.Descripcion & "</a>" & vbCrLf
                If NroSubMenu(oBEMenu.IDMenu, Lista) > 0 Then
                    sSubMenu &= "<ul>" & vbCrLf
                    sSubMenu = AgregarSubMenuSis(Lista, sSubMenu, oBEMenu.IDMenu)
                    sSubMenu &= "</ul>" & vbCrLf
                End If
                sSubMenu &= "</li>" & vbCrLf
            End If
        Next oBEMenu
        Return sSubMenu
    End Function

    Public Function NroSubMenu(ByVal pIDMenu As Integer, ByVal pLista As ArrayList) As Integer
        Dim c As Integer = 0
        For Each oBE As BEMenu In pLista
            If oBE.IDMenuPadre = pIDMenu Then
                c += 1
            End If
        Next oBE
        Return c
    End Function

End Class