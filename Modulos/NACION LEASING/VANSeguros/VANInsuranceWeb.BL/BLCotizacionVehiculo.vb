﻿Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL
Imports System
Imports System.Collections
Public Class BLCotizacionVehiculo

    Public Function Buscar(ByVal pIdProducto As Int32, ByVal pIDTipoDocumento As String, ByVal pNroDocumento As String, ByVal pIDTipoDocumentoAseg As String, ByVal pNroDocumentoAseg As String) As IList
        Dim oDL As New DLNTCotizacionVehiculo
        Dim oBE As New BECotizacionVehiculo
        oBE.IdProducto = pIdProducto
        oBE.IdTipoDocumento = pIDTipoDocumento
        oBE.NroDocumento = pNroDocumento
        oBE.IdTipoDocumentoAseg = pIDTipoDocumentoAseg
        oBE.NroDocumentoAseg = pNroDocumentoAseg
        Return oDL.Seleccionar(oBE)
    End Function

End Class


