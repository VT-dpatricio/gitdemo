﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL

Public Class UsuarioConfigBL

    Dim _UsuarioConfigDA As UsuarioConfigDA

    Public Sub New()
        _UsuarioConfigDA = New UsuarioConfigDA
    End Sub

    Function Agregar(ByVal Objeto As UsuarioConfigBE) As String
        Return _UsuarioConfigDA.Agregar(Objeto, Nothing)
    End Function

    Function Eliminar(ByVal Objeto As UsuarioConfigBE) As Integer
        Return _UsuarioConfigDA.Eliminar(Objeto, Nothing)
    End Function

    Function Listar_IdUsuario(ByVal Objeto As UsuarioConfigBE) As List(Of UsuarioConfigBE)
        Return _UsuarioConfigDA.Listar_IdUsuario(Objeto, Nothing)
    End Function


End Class
