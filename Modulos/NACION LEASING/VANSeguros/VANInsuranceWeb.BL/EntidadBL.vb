﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL

Public Class EntidadBL

    Dim _Entidadda As Entidadda

    Public Sub New()
        _Entidadda = New Entidadda
    End Sub

    Function Agregar(ByVal Objeto As EntidadBE) As String
        Return _Entidadda.Agregar(Objeto, Nothing)
    End Function

    Function Modificar(ByVal Objeto As EntidadBE) As Integer
        Return _Entidadda.Modificar(Objeto, Nothing)
    End Function

    Function Eliminar(ByVal Objeto As EntidadBE) As Integer
        Return _Entidadda.Eliminar(Objeto, Nothing)
    End Function

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of EntidadBE)
        Return _Entidadda.Listar_Todos(Nothing)
    End Function

    Function Listar_Id(ByVal Objeto As EntidadBE, Optional ByVal Transaction As DbTransaction = Nothing) As EntidadBE
        Return _Entidadda.Listar_Id(Objeto, Nothing)
    End Function

End Class
