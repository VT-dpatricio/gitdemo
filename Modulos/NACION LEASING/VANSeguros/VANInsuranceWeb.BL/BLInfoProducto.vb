﻿Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL
Imports System
Imports System.Collections
Public Class BLInfoProducto

    Public Function Seleccionar(ByVal pIDProducto As Int32) As IList
        Dim oDL As New DLNTInfoProducto
        Return oDL.Seleccionar(pIDProducto)
    End Function

    Public Function Seleccionar(ByVal pIDProducto As Int32, ByVal pIDGrupo As String, Optional ByVal filtrar As Boolean = False) As IList
        Dim oDL As New DLNTInfoProducto
        Dim oBE As New BEInfoProducto
        oBE.IdProducto = pIDProducto
        oBE.IdGrupo = pIDGrupo
        oBE.Obligatorio = filtrar
        Return oDL.Seleccionar(oBE)
    End Function

End Class
