﻿Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL
Imports System
Imports System.Collections

Public Class BLOficina


    Public Function Buscar(ByVal pIdProducto As Int32, ByVal pBuscar As String) As IList
        Dim oDL As New DLNTOficina
        Dim oBE As New BEOficina
        oBE.IdProducto = pIdProducto
        oBE.Buscar = pBuscar
        Return oDL.Seleccionar(oBE)
    End Function

End Class


