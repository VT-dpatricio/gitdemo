﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL

Public Class CertificadoBL

    Dim _CertificadoDA As Certificadoda

    Public Sub New()
        _CertificadoDA = New Certificadoda
    End Sub

    Function Agregar(ByVal Objeto As CertificadoBE) As String
        Return _CertificadoDA.Agregar(Objeto, Nothing)
    End Function

    Function Modificar(ByVal Objeto As CertificadoBE) As Integer
        Return _CertificadoDA.Modificar(Objeto, Nothing)
    End Function

    Function Eliminar(ByVal Objeto As CertificadoBE) As Integer
        Return _CertificadoDA.Eliminar(Objeto, Nothing)
    End Function

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of CertificadoBE)
        Return _CertificadoDA.Listar_Todos(Nothing)
    End Function

    Function Listar_Id(ByVal Objeto As CertificadoBE, Optional ByVal Transaction As DbTransaction = Nothing) As CertificadoBE
        Return _CertificadoDA.Listar_Id(Objeto, Nothing)
    End Function

    Function ConsultarPrimaCertificado(ByVal IdProducto As Integer, ByVal Opcion As String, ByVal IdFrecuancia As Integer, ByVal IdMoneda As String, ByVal Edad As Integer) As Decimal
        Return _CertificadoDA.ConsultarPrimaCertificado(IdProducto, Opcion, IdFrecuancia, IdMoneda, Edad)
    End Function


End Class
