﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL

Public Class ProductoEntidadBL

    Dim _ProductoEntidadda As ProductoEntidadda

    Public Sub New()
        _ProductoEntidadda = New ProductoEntidadda
    End Sub

    Function Agregar(ByVal Objeto As ProductoEntidadBE) As String
        Return _ProductoEntidadda.Agregar(Objeto, Nothing)
    End Function

    Function Modificar(ByVal Objeto As ProductoEntidadBE) As Integer
        Return _ProductoEntidadda.Modificar(Objeto, Nothing)
    End Function

    Function Eliminar(ByVal Objeto As ProductoEntidadBE) As Integer
        Return _ProductoEntidadda.Eliminar(Objeto, Nothing)
    End Function

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of ProductoEntidadBE)
        Return _ProductoEntidadda.Listar_Todos(Nothing)
    End Function

    Function Listar_Id(ByVal Objeto As ProductoEntidadBE, Optional ByVal Transaction As DbTransaction = Nothing) As ProductoEntidadBE
        Return _ProductoEntidadda.Listar_Id(Objeto, Nothing)
    End Function

    Function Listar_IdProductoCheck(ByVal Objeto As ProductoEntidadBE, Optional ByVal Transaction As DbTransaction = Nothing) As List(Of EntidadBE)
        Return _ProductoEntidadda.Listar_IdProducto_Check(Objeto, Nothing)
    End Function

    Function Listar_IdProducto(ByVal Objeto As ProductoEntidadBE, Optional ByVal Transaction As DbTransaction = Nothing) As List(Of EntidadBE)
        Return _ProductoEntidadda.Listar_IdProducto(Objeto, Nothing)
    End Function


End Class
