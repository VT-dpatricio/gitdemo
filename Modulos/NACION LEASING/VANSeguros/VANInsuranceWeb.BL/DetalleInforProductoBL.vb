﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL

Public Class DetalleInfoProductoBL

    Dim _DetalleInfoProductoDA As DetalleInfoProductoda

    Public Sub New()
        _DetalleInfoProductoDA = New DetalleInfoProductoda
    End Sub

    Function Agregar(ByVal Objeto As DetalleInfoProductoBE) As String
        Return _DetalleInfoProductoDA.Agregar(Objeto, Nothing)
    End Function

    Function Modificar(ByVal Objeto As DetalleInfoProductoBE) As Integer
        Return _DetalleInfoProductoDA.Modificar(Objeto, Nothing)
    End Function

    Function Eliminar(ByVal Objeto As DetalleInfoProductoBE) As Integer
        Return _DetalleInfoProductoDA.Eliminar(Objeto, Nothing)
    End Function

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of DetalleInfoProductoBE)
        Return _DetalleInfoProductoDA.Listar_Todos(Nothing)
    End Function

    Function Listar_Id(ByVal Objeto As DetalleInfoProductoBE, Optional ByVal Transaction As DbTransaction = Nothing) As DetalleInfoProductoBE
        Return _DetalleInfoProductoDA.Listar_Id(Objeto, Nothing)
    End Function

    Function Listar_IdInfoProducto(ByVal Objeto As DetalleInfoProductoBE, Optional ByVal Transaction As DbTransaction = Nothing) As List(Of DetalleInfoProductoBE)
        Return _DetalleInfoProductoDA.Listar_IdInfoProducto(Objeto, Nothing)
    End Function

End Class
