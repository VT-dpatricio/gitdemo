﻿Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL
Imports System
Imports System.Collections

Public Class BLMarcaModelo

    Public Function Seleccionar(ByVal IdMarca As String) As IList
        Dim oDL As New DLNTMarcaModelo
        Return oDL.Seleccionar(IdMarca)
    End Function

    Public Function SeleccionarTodos(ByVal pMarca As String) As IList
        Dim oDL As New DLNTMarcaModelo
        Return oDL.SeleccionarTodos(pMarca)
    End Function

    Public Function ValidaIMEI(ByVal pValor As String) As String
        Dim oDL As New DLNTMarcaModelo
        Return oDL.ValidaIMEI(pValor)
    End Function

    Public Function ListarMarca(ByVal pIDProducto As Int32) As IList
        Dim oDL As New DLNTMarcaModelo
        Return oDL.Seleccionar(pIDProducto)
    End Function


    Public Function Buscar(ByVal pIDMarca As Int32, ByVal pIDProducto As Int32, ByVal pBuscar As String) As IList
        Dim oDL As New DLNTMarcaModelo
        Dim oBE As New BEMarcaModelo
        oBE.Buscar = pBuscar
        oBE.IDProducto = pIDProducto
        oBE.IDMarca = pIDMarca
        Return oDL.Seleccionar(oBE)
    End Function
End Class
