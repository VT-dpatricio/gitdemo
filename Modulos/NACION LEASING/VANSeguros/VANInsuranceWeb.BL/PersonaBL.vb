﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL

Public Class PersonaBL

    Dim _Personada As Personada

    Public Sub New()
        _Personada = New Personada
    End Sub


    Function Agregar(ByVal Objeto As PersonaBE) As String
        Return _Personada.Agregar(Objeto, Nothing)
    End Function

    Function Modificar(ByVal Objeto As PersonaBE) As Integer
        Return _Personada.Modificar(Objeto, Nothing)
    End Function

    Function Eliminar(ByVal Objeto As PersonaBE) As Integer
        Return _Personada.Eliminar(Objeto, Nothing)
    End Function

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of PersonaBE)
        Return _Personada.Listar_Todos(Nothing)
    End Function

    Function Listar_Id(ByVal Objeto As PersonaBE, Optional ByVal Transaction As DbTransaction = Nothing) As PersonaBE
        Return _Personada.Listar_Id(Objeto, Nothing)
    End Function

End Class
