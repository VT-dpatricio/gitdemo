﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL

Public Class CertificadoDetalleInfoProductoBL

    Dim _CertificadoDetalleInfoProductoDA As CertificadoDetalleInfoProductoda

    Public Sub New()
        _CertificadoDetalleInfoProductoDA = New CertificadoDetalleInfoProductoda
    End Sub

    Function Agregar(ByVal Objeto As CertificadoDetalleInfoProductoBE) As String
        Return _CertificadoDetalleInfoProductoDA.Agregar(Objeto, Nothing)
    End Function

    Function Modificar(ByVal Objeto As CertificadoDetalleInfoProductoBE) As Integer
        Return _CertificadoDetalleInfoProductoDA.Modificar(Objeto, Nothing)
    End Function

    Function Eliminar(ByVal Objeto As CertificadoDetalleInfoProductoBE) As Integer
        Return _CertificadoDetalleInfoProductoDA.Eliminar(Objeto, Nothing)
    End Function

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of CertificadoBE)
        Return _CertificadoDetalleInfoProductoDA.Listar_Todos(Nothing)
    End Function

    Function Listar_Id(ByVal Objeto As CertificadoDetalleInfoProductoBE, Optional ByVal Transaction As DbTransaction = Nothing) As CertificadoDetalleInfoProductoBE
        Return _CertificadoDetalleInfoProductoDA.Listar_Id(Objeto, Nothing)
    End Function
End Class
