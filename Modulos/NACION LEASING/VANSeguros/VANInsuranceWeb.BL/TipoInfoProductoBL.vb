﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL

Public Class TipoInfoProductoBL

    Dim _TipoInfoProductoda As TipoInfoProductoda

    Public Sub New()
        _TipoInfoProductoda = New TipoInfoProductoda
    End Sub

    Function Agregar(ByVal Objeto As TipoInfoProductoBE) As String
        Return _TipoInfoProductoda.Agregar(Objeto, Nothing)
    End Function

    Function Modificar(ByVal Objeto As TipoInfoProductoBE) As Integer
        Return _TipoInfoProductoda.Modificar(Objeto, Nothing)
    End Function

    Function Eliminar(ByVal Objeto As TipoInfoProductoBE) As Integer
        Return _TipoInfoProductoda.Eliminar(Objeto, Nothing)
    End Function

    Function Listar_Todos() As List(Of TipoInfoProductoBE)
        Return _TipoInfoProductoda.Listar_Todos(Nothing)
    End Function

    Function Listar_Id(ByVal Objeto As TipoInfoProductoBE) As TipoInfoProductoBE
        Return _TipoInfoProductoda.Listar_Id(Objeto, Nothing)
    End Function
End Class
