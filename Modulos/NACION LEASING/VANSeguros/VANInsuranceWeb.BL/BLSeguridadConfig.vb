﻿Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL
Imports System
Imports System.Collections
Public Class BLSeguridadConfig

    Public Function Seleccionar(ByVal pIDEntidad As Int32) As IList
        Dim oDL As New DLNTSeguridadConfig
        Return oDL.Seleccionar(pIDEntidad)
    End Function

    Public Function Parametro(ByVal pIDParametro As String, ByVal pListaP As IList) As String
        'Dim oBEConfigSeguridad As New BEConfigSeguridad
        Dim Valor As String = ""
        For Each oBEConfigSeguridad As BESeguridadConfig In pListaP
            If (oBEConfigSeguridad.IDParametro = pIDParametro) Then
                Valor = oBEConfigSeguridad.Valor
                Exit For
            End If
        Next oBEConfigSeguridad
        Return Valor
    End Function

End Class
