Public Class BEProvincia
    Inherits BEBase

    Private _idDepartamento As Int32
    Private _idProvincia As Int32
    Private _nombre As String

    Public Property IdDepartamento() As Int32
        Get
            Return _idDepartamento
        End Get
        Set(ByVal value As Int32)
            _idDepartamento = value
        End Set
    End Property

    Public Property IdProvincia() As Int32
        Get
            Return _idProvincia
        End Get
        Set(ByVal value As Int32)
            _idProvincia = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property
End Class
