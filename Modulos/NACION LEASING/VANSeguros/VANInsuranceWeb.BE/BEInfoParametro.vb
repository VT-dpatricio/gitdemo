﻿Public Class BEInfoParametro
    Inherits BEBase

    Private _nombre As String
    Private _idInfoParametro As Int32
    Private _idParametro As String
    Private _idEntidad As Int32
    Private _idAsegurador As Int32
    Private _idProducto As Int32
    Private _valorString As String
    Private _valorDate As Date
    Private _valorNum As Int32

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Public Property IdInfoParametro() As Int32
        Get
            Return _idInfoParametro
        End Get
        Set(ByVal value As Int32)
            _idInfoParametro = value
        End Set
    End Property

    Public Property IdParametro() As String
        Get
            Return _idParametro
        End Get
        Set(ByVal value As String)
            _idParametro = value
        End Set
    End Property

    Public Property IdEntidad() As Int32
        Get
            Return _idEntidad
        End Get
        Set(ByVal value As Int32)
            _idEntidad = value
        End Set
    End Property

    Public Property IdAsegurador() As Int32
        Get
            Return _idAsegurador
        End Get
        Set(ByVal value As Int32)
            _idAsegurador = value
        End Set
    End Property

    Public Property IdProducto() As Int32
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Int32)
            _idProducto = value
        End Set
    End Property

    Public Property ValorString() As String
        Get
            Return _valorString
        End Get
        Set(ByVal value As String)
            _valorString = value
        End Set
    End Property

    Public Property ValorNum() As Int32
        Get
            Return _valorNum
        End Get
        Set(ByVal value As Int32)
            _valorNum = value
        End Set
    End Property

    Public Property ValorDate() As Date
        Get
            Return _valorDate
        End Get
        Set(ByVal value As Date)
            _valorDate = value
        End Set
    End Property
End Class
