﻿Public Class BEParametro

    Inherits BEBase
    Private _idParametro As Int32
    Private _nombre As String
    Private _descripcion As String
    Private _buscar As String

    Public Property IdParametro() As Int32
        Get
            Return _idParametro
        End Get
        Set(ByVal value As Int32)
            _idParametro = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property

    Public Property Buscar() As String
        Get
            Return _buscar
        End Get
        Set(ByVal value As String)
            _buscar = value
        End Set
    End Property

End Class
