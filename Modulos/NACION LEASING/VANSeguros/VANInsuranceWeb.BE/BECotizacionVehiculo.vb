﻿Public Class BECotizacionVehiculo
    Inherits BEBase
    Private _NroCotizacion As Decimal = 0
    Private _idProducto As Int32 = 0
    Private _idAsegurador As Int32 = 0
    Private _idTipoDocumento As String = String.Empty
    Private _idTipoDocumentoAseg As String = String.Empty
    Private _NroDocumento As String = String.Empty
    Private _NroDocumentoAseg As String = String.Empty
    Private _NroPlaca As String = String.Empty
    Private _idMarca As Int32 = 0
    Private _idModelo As Int32 = 0
    Private _Marca As String = String.Empty
    Private _Modelo As String = String.Empty
    Private _Anio As Int32 = 0
    Private _ValorVehiculo As Decimal = 0
    Private _PrimaMensual As Decimal = 0
    Private _PrimaTotal As Decimal = 0
    Private _MontoAsegurado As Decimal = 0

    Public Property NroCotizacion() As Decimal
        Get
            Return _NroCotizacion
        End Get
        Set(ByVal value As Decimal)
            _NroCotizacion = value
        End Set
    End Property


    Public Property IdMarca() As Int32
        Get
            Return _idMarca
        End Get
        Set(ByVal value As Int32)
            _idMarca = value
        End Set
    End Property


    Public Property IdModelo() As Int32
        Get
            Return _idModelo
        End Get
        Set(ByVal value As Int32)
            _idModelo = value
        End Set
    End Property

    Public Property IdProducto() As Int32
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Int32)
            _idProducto = value
        End Set
    End Property

    Public Property IdAsegurador() As Int32
        Get
            Return _idAsegurador
        End Get
        Set(ByVal value As Int32)
            _idAsegurador = value
        End Set
    End Property

    Public Property IdTipoDocumento() As [String]
        Get
            Return _idTipoDocumento
        End Get
        Set(ByVal value As [String])
            _idTipoDocumento = value
        End Set
    End Property

    Public Property NroDocumento() As [String]
        Get
            Return _NroDocumento
        End Get
        Set(ByVal value As [String])
            _NroDocumento = value
        End Set
    End Property

    Public Property IdTipoDocumentoAseg() As [String]
        Get
            Return _idTipoDocumentoAseg
        End Get
        Set(ByVal value As [String])
            _idTipoDocumentoAseg = value
        End Set
    End Property


    Public Property NroDocumentoAseg() As String
        Get
            Return _NroDocumentoAseg
        End Get
        Set(ByVal value As String)
            _NroDocumentoAseg = value
        End Set
    End Property

    Public Property NroPlaca() As [String]
        Get
            Return _NroPlaca
        End Get
        Set(ByVal value As [String])
            _NroPlaca = value
        End Set
    End Property

    Public Property Marca() As [String]
        Get
            Return _Marca
        End Get
        Set(ByVal value As [String])
            _Marca = value
        End Set
    End Property

    Public Property Modelo() As [String]
        Get
            Return _Modelo
        End Get
        Set(ByVal value As [String])
            _Modelo = value
        End Set
    End Property

    Public Property Anio() As Int32
        Get
            Return _Anio
        End Get
        Set(ByVal value As Int32)
            _Anio = value
        End Set
    End Property

    Public Property ValorVehiculo() As Decimal
        Get
            Return _ValorVehiculo
        End Get
        Set(ByVal value As Decimal)
            _ValorVehiculo = value
        End Set
    End Property

    Public Property PrimaMensual() As Decimal
        Get
            Return _PrimaMensual
        End Get
        Set(ByVal value As Decimal)
            _PrimaMensual = value
        End Set
    End Property

    Public Property PrimaTotal() As Decimal
        Get
            Return _PrimaTotal
        End Get
        Set(ByVal value As Decimal)
            _PrimaTotal = value
        End Set
    End Property

    Public Property MontoAsegurado() As Decimal
        Get
            Return _MontoAsegurado
        End Get
        Set(ByVal value As Decimal)
            _MontoAsegurado = value
        End Set
    End Property


End Class
