﻿Public Class TipoValorBE
    Inherits GenericEntity

    Sub New()

    End Sub

    Private _IdTipoValor As Integer
    Private _Descripcion As String
    

    Property IdTipoValor() As Integer
        Get
            Return _IdTipoValor
        End Get
        Set(ByVal value As Integer)
            _IdTipoValor = value
        End Set
    End Property
    Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property

End Class
