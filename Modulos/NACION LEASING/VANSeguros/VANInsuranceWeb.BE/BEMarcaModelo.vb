﻿Public Class BEMarcaModelo
    Inherits BEBase

    Private _iDCotizacionCelular As Integer = 0
    Private _marca As String = String.Empty
    Private _modelo As String = String.Empty
    Private _opcion As String = String.Empty
    Private _deducible As Decimal = 0
    Private _importe As Decimal = 0
    Private _productCode As String = String.Empty
    Private _productPrice As Decimal = 0

    Private _IDProducto As Int32 = 0
    Private _IDMarca As Int32 = 0
    Private _IDModelo As Int32 = 0
    Private _buscar As String = String.Empty

    Public Property IDCotizacionCelular() As Integer
        Get
            Return Me._iDCotizacionCelular
        End Get
        Set(ByVal value As Integer)
            Me._iDCotizacionCelular = value
        End Set
    End Property

    Public Property Marca() As String
        Get
            Return Me._marca
        End Get
        Set(ByVal value As String)
            Me._marca = value
        End Set
    End Property

    Public Property Modelo() As String
        Get
            Return Me._modelo
        End Get
        Set(ByVal value As String)
            Me._modelo = value
        End Set
    End Property


    Public Property Opcion() As String
        Get
            Return Me._opcion
        End Get
        Set(ByVal value As String)
            Me._opcion = value
        End Set
    End Property

    Public Property Deducible() As String
        Get
            Return Me._deducible
        End Get
        Set(ByVal value As String)
            Me._deducible = value
        End Set
    End Property

    Public Property Importe() As Decimal
        Get
            Return Me._importe
        End Get
        Set(ByVal value As Decimal)
            Me._importe = value
        End Set
    End Property

    Public Property ProductCode() As String
        Get
            Return Me._productCode
        End Get
        Set(ByVal value As String)
            Me._productCode = value
        End Set
    End Property

    Public Property ProductPrice() As Decimal
        Get
            Return Me._productPrice
        End Get
        Set(ByVal value As Decimal)
            Me._productPrice = value
        End Set
    End Property

    Public Property IDProducto() As Int32
        Get
            Return _IDProducto
        End Get
        Set(ByVal value As Int32)
            _IDProducto = value
        End Set
    End Property

    Public Property IDMarca() As Int32
        Get
            Return _IDMarca
        End Get
        Set(ByVal value As Int32)
            _IDMarca = value
        End Set
    End Property

    Public Property IDModelo() As Int32
        Get
            Return _IDModelo
        End Get
        Set(ByVal value As Int32)
            _IDModelo = value
        End Set
    End Property


    Public Property Buscar() As String
        Get
            Return _buscar
        End Get
        Set(ByVal value As String)
            _buscar = value
        End Set
    End Property
End Class
