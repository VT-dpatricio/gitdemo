﻿Public Class CertificadoBE
    Inherits GenericEntity

    Sub New()

    End Sub


    Private _IdCertificado As Integer
    Private _IdProducto As Integer
    Private _IdAseguradora As Integer
    Private _NumeroCertificado As String
    Private _FechaEmision As DateTime
    Private _FechaInicio As DateTime
    Private _FechaFin As DateTime
    Private _PrimaNetaMensual As Decimal
    Private _PrimaBrutaMensual As Decimal
    Private _PrimaNetaAnual As Decimal
    Private _PrimanBrutaAnual As Decimal
    Private _Poliza As String
    Private _Obs As String

    Property IdCertificado() As Integer
        Get
            Return _IdCertificado
        End Get
        Set(ByVal value As Integer)
            _IdCertificado = value
        End Set
    End Property

    Property IdProducto() As Integer
        Get
            Return _IdProducto
        End Get
        Set(ByVal value As Integer)
            _IdProducto = value
        End Set
    End Property
    Property IdAseguradora() As Integer
        Get
            Return _IdAseguradora
        End Get
        Set(ByVal value As Integer)
            _IdAseguradora = value
        End Set
    End Property
    Property NumeroCertificado() As String
        Get
            Return _NumeroCertificado
        End Get
        Set(ByVal value As String)
            _NumeroCertificado = value
        End Set
    End Property
    Property FechaEmision() As DateTime
        Get
            Return _FechaEmision
        End Get
        Set(ByVal value As DateTime)
            _FechaEmision = value
        End Set
    End Property
    Property FechaInicio() As DateTime
        Get
            Return _FechaInicio
        End Get
        Set(ByVal value As DateTime)
            _FechaInicio = value
        End Set
    End Property
    Property FechaFin() As DateTime
        Get
            Return _FechaFin
        End Get
        Set(ByVal value As DateTime)
            _FechaFin = value
        End Set
    End Property
    Property PrimaNetaMensual() As Decimal
        Get
            Return _PrimaNetaMensual
        End Get
        Set(ByVal value As Decimal)
            _PrimaNetaMensual = value
        End Set
    End Property
    Property PrimaBrutaMensual() As Decimal
        Get
            Return _PrimaBrutaMensual
        End Get
        Set(ByVal value As Decimal)
            _PrimaBrutaMensual = value
        End Set
    End Property
    Property PrimaNetaAnual() As Decimal
        Get
            Return _PrimaNetaAnual
        End Get
        Set(ByVal value As Decimal)
            _PrimaNetaAnual = value
        End Set
    End Property
    Property PrimanBrutaAnual() As Decimal
        Get
            Return _PrimanBrutaAnual
        End Get
        Set(ByVal value As Decimal)
            _PrimanBrutaAnual = value
        End Set
    End Property
    Property Poliza() As String
        Get
            Return _Poliza
        End Get
        Set(ByVal value As String)
            _Poliza = value
        End Set
    End Property
    Property Obs() As String
        Get
            Return _Obs
        End Get
        Set(ByVal value As String)
            _Obs = value
        End Set
    End Property

End Class
