﻿Public Class InfoProductoDatoBE

    Private _Lista As List(Of InfoProductoBE)
    Public Property Lista() As List(Of InfoProductoBE)
        Get
            Return _Lista
        End Get
        Set(ByVal value As List(Of InfoProductoBE))
            _Lista = value
        End Set
    End Property

    Sub New()
        _Lista = New List(Of InfoProductoBE)
    End Sub

End Class
