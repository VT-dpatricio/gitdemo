﻿Public Class BEExpedir
#Region "Campos"
    Private m_descripcion As [String]
    Private m_codigo As Int32
#End Region

#Region "Propiedades"
    Public Property Descripcion() As [String]
        Get
            Return m_descripcion
        End Get
        Set(ByVal value As [String])
            m_descripcion = value
        End Set
    End Property

    Public Property Codigo() As Int32
        Get
            Return m_codigo
        End Get
        Set(ByVal value As Int32)
            m_codigo = value
        End Set
    End Property
#End Region
End Class

