﻿Public Class BEInfoControl
    Private _idInfo As Int32
    Private _idControl As String
    Private _tipoControl As String
    Private _idInfoD As String
    Private _GuardarIDValue As Boolean = False
    Public Property IDInfo() As Int32
        Get
            Return _idInfo
        End Get
        Set(ByVal value As Int32)
            _idInfo = value
        End Set
    End Property

    Public Property IDInfoD() As String
        Get
            Return _idInfoD
        End Get
        Set(ByVal value As String)
            _idInfoD = value
        End Set
    End Property

    Public Property IDControl() As String
        Get
            Return _idControl
        End Get
        Set(ByVal value As String)
            _idControl = value
        End Set
    End Property

    Public Property TipoControl() As String
        Get
            Return _tipoControl
        End Get
        Set(ByVal value As String)
            _tipoControl = value
        End Set
    End Property

    Public Property GuardarIDValue() As String
        Get
            Return _GuardarIDValue
        End Get
        Set(ByVal value As String)
            _GuardarIDValue = value
        End Set
    End Property
End Class
