﻿Public Class ProductoBE
    Inherits GenericEntity

    Sub New()
        _ListEntidadBE = New List(Of EntidadBE)
        _ListInfoProductoBE = New List(Of InfoProductoBE)
    End Sub

    Private _i As Integer
    Private _IdProducto As Integer
    Private _Descripcion As String
    Private _Abrev As String
    Private _IdAseguradora As Integer
    Private _Aseguradora As String
    Private _IdSponsor As Integer
    Private _Sponsor As String
    Private _Logo As String
    Private _Banner As String
    Private _Reporte As String
    Private _vEdad As Boolean
    Private _vMonto As Boolean

    Private _NombreAsegurador As String
    Private _NombreEntidad As String

    Private _idAsegurador As Integer
    Private _nombre As String
    Private _idTipoProducto As String
    Private _activo As Boolean
    Private _maxAsegurados As Integer
    Private _nacimientoRequerido As Boolean
    Private _validarParentesco As Boolean
    Private _obligaCuentaHabiente As Boolean
    Private _cumuloMaximo As Integer
    Private _edadMaxPermanencia As Integer
    Private _diasAnulacion As Integer
    Private _generaCobro As Boolean
    Private _idEntidad As Integer
    Private _vigenciaCobro As Boolean
    Private _maxPolizasAsegurado As Integer
    Private _maxPolizasCuentahabiente As Integer
    Private _maxPolizasRegalo As Integer
    Private _idReglaCobro As Integer
    Private _maxCuentasTitular As Integer
    Private _maxPolizasAseguradoSinDNI As Integer
    Private _validaDireccionCer As Boolean
    Private _validaDireccionAse As Boolean
    Private _cuentaHabienteigualAsegurado As Boolean

    Public Property i() As Integer
        Get
            Return _i
        End Get
        Set(ByVal value As Integer)
            _i = value
        End Set
    End Property

    Private _Minimo As Integer
    Public Property Minimo() As Integer
        Get
            Return _Minimo
        End Get
        Set(ByVal value As Integer)
            _Minimo = value
        End Set
    End Property


    Private _Maximo As Integer
    Public Property Maximo() As Integer
        Get
            Return _Maximo
        End Get
        Set(ByVal value As Integer)
            _Maximo = value
        End Set
    End Property





    Public Property vMonto() As Boolean
        Get
            Return _vMonto
        End Get
        Set(ByVal value As Boolean)
            _vMonto = value
        End Set
    End Property


    Public Property vEdad() As Boolean
        Get
            Return _vEdad
        End Get
        Set(ByVal value As Boolean)
            _vEdad = value
        End Set
    End Property



    Private _ListEntidadBE As List(Of EntidadBE)
    Public Property ListEntidadBE() As List(Of EntidadBE)
        Get
            Return _ListEntidadBE
        End Get
        Set(ByVal value As List(Of EntidadBE))
            _ListEntidadBE = value
        End Set
    End Property


    Private _ListInfoProductoBE As List(Of InfoProductoBE)
    Public Property ListInfoProducto() As List(Of InfoProductoBE)
        Get
            Return _ListInfoProductoBE
        End Get
        Set(ByVal value As List(Of InfoProductoBE))
            _ListInfoProductoBE = value
        End Set
    End Property

    Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property
    Property Abrev() As String
        Get
            Return _Abrev
        End Get
        Set(ByVal value As String)
            _Abrev = value
        End Set
    End Property
    Property IdAseguradora() As Integer
        Get
            Return _IdAseguradora
        End Get
        Set(ByVal value As Integer)
            _IdAseguradora = value
        End Set
    End Property

    Property Aseguradora() As String
        Get
            Return _Aseguradora
        End Get
        Set(ByVal value As String)
            _Aseguradora = value
        End Set
    End Property

    Property NombreAsegurador() As String
        Get
            Return _NombreAsegurador
        End Get
        Set(ByVal value As String)
            _NombreAsegurador = value
        End Set
    End Property

    Property IdSponsor() As Integer
        Get
            Return _IdSponsor
        End Get
        Set(ByVal value As Integer)
            _IdSponsor = value
        End Set
    End Property

    Property Sponsor() As String
        Get
            Return _Sponsor
        End Get
        Set(ByVal value As String)
            _Sponsor = value
        End Set
    End Property

    Property Logo() As String
        Get
            Return _Logo
        End Get
        Set(ByVal value As String)
            _Logo = value
        End Set
    End Property
    Property Banner() As String
        Get
            Return _Banner
        End Get
        Set(ByVal value As String)
            _Banner = value
        End Set
    End Property
    Property Reporte() As String
        Get
            Return _Reporte
        End Get
        Set(ByVal value As String)
            _Reporte = value
        End Set
    End Property

    Property NombreEntidad() As String
        Get
            Return _NombreEntidad
        End Get
        Set(ByVal value As String)
            _NombreEntidad = value
        End Set
    End Property


    Property idProducto() As Integer
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Integer)
            _idProducto = value
        End Set
    End Property
    Property idAsegurador() As Integer
        Get
            Return _idAsegurador
        End Get
        Set(ByVal value As Integer)
            _idAsegurador = value
        End Set
    End Property
    Property nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property
    Property idTipoProducto() As String
        Get
            Return _idTipoProducto
        End Get
        Set(ByVal value As String)
            _idTipoProducto = value
        End Set
    End Property
    Property activo() As Boolean
        Get
            Return _activo
        End Get
        Set(ByVal value As Boolean)
            _activo = value
        End Set
    End Property
    Property maxAsegurados() As Integer
        Get
            Return _maxAsegurados
        End Get
        Set(ByVal value As Integer)
            _maxAsegurados = value
        End Set
    End Property
    Property nacimientoRequerido() As Boolean
        Get
            Return _nacimientoRequerido
        End Get
        Set(ByVal value As Boolean)
            _nacimientoRequerido = value
        End Set
    End Property
    Property validarParentesco() As Boolean
        Get
            Return _validarParentesco
        End Get
        Set(ByVal value As Boolean)
            _validarParentesco = value
        End Set
    End Property
    Property obligaCuentaHabiente() As Boolean
        Get
            Return _obligaCuentaHabiente
        End Get
        Set(ByVal value As Boolean)
            _obligaCuentaHabiente = value
        End Set
    End Property
    Property cumuloMaximo() As Integer
        Get
            Return _cumuloMaximo
        End Get
        Set(ByVal value As Integer)
            _cumuloMaximo = value
        End Set
    End Property
    Property edadMaxPermanencia() As Integer
        Get
            Return _edadMaxPermanencia
        End Get
        Set(ByVal value As Integer)
            _edadMaxPermanencia = value
        End Set
    End Property
    Property diasAnulacion() As Integer
        Get
            Return _diasAnulacion
        End Get
        Set(ByVal value As Integer)
            _diasAnulacion = value
        End Set
    End Property
    Property generaCobro() As Boolean
        Get
            Return _generaCobro
        End Get
        Set(ByVal value As Boolean)
            _generaCobro = value
        End Set
    End Property
    Property idEntidad() As Integer
        Get
            Return _idEntidad
        End Get
        Set(ByVal value As Integer)
            _idEntidad = value
        End Set
    End Property
    Property vigenciaCobro() As Boolean
        Get
            Return _vigenciaCobro
        End Get
        Set(ByVal value As Boolean)
            _vigenciaCobro = value
        End Set
    End Property
    Property maxPolizasAsegurado() As Integer
        Get
            Return _maxPolizasAsegurado
        End Get
        Set(ByVal value As Integer)
            _maxPolizasAsegurado = value
        End Set
    End Property
    Property maxPolizasCuentahabiente() As Integer
        Get
            Return _maxPolizasCuentahabiente
        End Get
        Set(ByVal value As Integer)
            _maxPolizasCuentahabiente = value
        End Set
    End Property
    Property maxPolizasRegalo() As Integer
        Get
            Return _maxPolizasRegalo
        End Get
        Set(ByVal value As Integer)
            _maxPolizasRegalo = value
        End Set
    End Property
    Property idReglaCobro() As Integer
        Get
            Return _idReglaCobro
        End Get
        Set(ByVal value As Integer)
            _idReglaCobro = value
        End Set
    End Property
    Property maxCuentasTitular() As Integer
        Get
            Return _maxCuentasTitular
        End Get
        Set(ByVal value As Integer)
            _maxCuentasTitular = value
        End Set
    End Property
    Property maxPolizasAseguradoSinDNI() As Integer
        Get
            Return _maxPolizasAseguradoSinDNI
        End Get
        Set(ByVal value As Integer)
            _maxPolizasAseguradoSinDNI = value
        End Set
    End Property
    Property validaDireccionCer() As Boolean
        Get
            Return _validaDireccionCer
        End Get
        Set(ByVal value As Boolean)
            _validaDireccionCer = value
        End Set
    End Property
    Property validaDireccionAse() As Boolean
        Get
            Return _validaDireccionAse
        End Get
        Set(ByVal value As Boolean)
            _validaDireccionAse = value
        End Set
    End Property
    Property cuentaHabienteigualAsegurado() As Boolean
        Get
            Return _cuentaHabienteigualAsegurado
        End Get
        Set(ByVal value As Boolean)
            _cuentaHabienteigualAsegurado = value
        End Set
    End Property



    
End Class
