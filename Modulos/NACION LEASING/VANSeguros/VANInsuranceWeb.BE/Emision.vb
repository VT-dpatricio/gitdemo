﻿Public Class EmisionBE


    Private _Asegurado As PersonaBE
    Public Property AseguradoBE() As PersonaBE
        Get
            Return _Asegurado
        End Get
        Set(ByVal value As PersonaBE)
            _Asegurado = value
        End Set
    End Property


    Private _Contratante As PersonaBE
    Public Property ContranteBE() As PersonaBE
        Get
            Return _Contratante
        End Get
        Set(ByVal value As PersonaBE)
            _Contratante = value
        End Set
    End Property


    Private _Certificado As CertificadoBE
    Public Property CertificadoBE() As CertificadoBE
        Get
            Return _Certificado
        End Get
        Set(ByVal value As CertificadoBE)
            _Certificado = value
        End Set
    End Property

    Private _FormaPagoBE As FormaPagoBE
    Public Property FormaPagoBE() As FormaPagoBE
        Get
            Return _FormaPagoBE
        End Get
        Set(ByVal value As FormaPagoBE)
            _FormaPagoBE = value
        End Set
    End Property



    Private _ListData As List(Of InfoProductoBE)
    Public Property ListInfoProducto() As List(Of InfoProductoBE)
        Get
            Return _ListData
        End Get
        Set(ByVal value As List(Of InfoProductoBE))
            _ListData = value
        End Set
    End Property


    Private _Lista As List(Of InfoProductoDatoBE)
    Public Property Lista() As List(Of InfoProductoDatoBE)
        Get
            Return _Lista
        End Get
        Set(ByVal value As List(Of InfoProductoDatoBE))
            _Lista = value
        End Set
    End Property

    Sub New()
        _Asegurado = New PersonaBE
        _Certificado = New CertificadoBE
        _Contratante = New PersonaBE
        _FormaPagoBE = New FormaPagoBE
        _Lista = New List(Of InfoProductoDatoBE)
        _ListData = New List(Of InfoProductoBE)
    End Sub


End Class
