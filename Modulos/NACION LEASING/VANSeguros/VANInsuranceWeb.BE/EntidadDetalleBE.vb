﻿Public Class EntidadDetalleBE
    Inherits GenericEntity

    Sub New()

    End Sub

    Private _IdEntidadDetalle As Integer
    Public Property IdEntidadDetalle() As Integer
        Get
            Return _IdEntidadDetalle
        End Get
        Set(ByVal value As Integer)
            _IdEntidadDetalle = value
        End Set
    End Property


    Private _IdEntidad As Integer
    Public Property IdEntidad() As Integer
        Get
            Return _IdEntidad
        End Get
        Set(ByVal value As Integer)
            _IdEntidad = value
        End Set
    End Property


    Private _IdTipoValidacion As Integer
    Public Property IdTipoValidacion() As Integer
        Get
            Return _IdTipoValidacion
        End Get
        Set(ByVal value As Integer)
            _IdTipoValidacion = value
        End Set
    End Property


    Private _TipoValidacion As String
    Public Property TipoValidacion() As String
        Get
            Return _TipoValidacion
        End Get
        Set(ByVal value As String)
            _TipoValidacion = value
        End Set
    End Property


    Private _Descripcion As String
    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property


    Private _Check As Boolean
    Public Property Check() As Boolean
        Get
            Return _Check
        End Get
        Set(ByVal value As Boolean)
            _Check = value
        End Set
    End Property


    Private _ValorMinimo As String
    Public Property ValorMinimo() As String
        Get
            Return _ValorMinimo
        End Get
        Set(ByVal value As String)
            _ValorMinimo = value
        End Set
    End Property


    Private _ValorMaximo As String
    Public Property ValorMaximo() As String
        Get
            Return _ValorMaximo
        End Get
        Set(ByVal value As String)
            _ValorMaximo = value
        End Set
    End Property


    Private _Obs As String
    Public Property Obs() As String
        Get
            Return _Obs
        End Get
        Set(ByVal value As String)
            _Obs = value
        End Set
    End Property








End Class
