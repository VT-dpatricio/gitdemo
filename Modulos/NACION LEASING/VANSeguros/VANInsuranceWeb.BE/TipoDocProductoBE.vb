﻿Public Class TipoDocProductoBE
    Inherits GenericEntity

    Sub New()

    End Sub

    Private _idProducto As Integer
    Private _idTipoDocumento As String




    Private _Id As Integer
    Public Property Id() As Integer
        Get
            Return _Id
        End Get
        Set(ByVal value As Integer)
            _Id = value
        End Set
    End Property

    Private _TipoDocProductoBE As Integer
    Public Property TipoDocProductoBE() As Integer
        Get
            Return _TipoDocProductoBE
        End Get
        Set(ByVal value As Integer)
            _TipoDocProductoBE = value
        End Set
    End Property

    Private _TipoDocumento As String
    Public Property TipoDocumento() As String
        Get
            Return _TipoDocumento
        End Get
        Set(ByVal value As String)
            _TipoDocumento = value
        End Set
    End Property


    Property idProducto() As Integer
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Integer)
            _idProducto = value
        End Set
    End Property
    Property idTipoDocumento() As String
        Get
            Return _idTipoDocumento
        End Get
        Set(ByVal value As String)
            _idTipoDocumento = value
        End Set
    End Property



End Class
