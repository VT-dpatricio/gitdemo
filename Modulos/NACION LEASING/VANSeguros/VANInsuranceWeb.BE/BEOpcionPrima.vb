Public Class BEOpcionPrima
    Inherits BEBase

    Private _idProducto As Int32
    Private _opcion As String
    Private _idFrecuencia As Int32
    Private _prima As Decimal
    Private _montoAsegurado As Decimal
    Private _idMonedaPrima As String
    Private _edadMin As Byte
    Private _edadmax As Byte

    Public Property IdProducto() As Int32
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Int32)
            _idProducto = value
        End Set
    End Property

    Public Property Opcion() As String
        Get
            Return _opcion
        End Get
        Set(ByVal value As String)
            _opcion = value
        End Set
    End Property

    Public Property IdFrecuencia() As Int32
        Get
            Return _idFrecuencia
        End Get
        Set(ByVal value As Int32)
            _idFrecuencia = value
        End Set
    End Property

    Public Property Prima() As Decimal
        Get
            Return _prima
        End Get
        Set(ByVal value As Decimal)
            _prima = value
        End Set
    End Property

    Public Property MontoAsegurado() As Decimal
        Get
            Return _montoAsegurado
        End Get
        Set(ByVal value As Decimal)
            _montoAsegurado = value
        End Set
    End Property

    Public Property IdMonedaPrima() As String
        Get
            Return _idMonedaPrima
        End Get
        Set(ByVal value As String)
            _idMonedaPrima = value
        End Set
    End Property

    Public Property EdadMin() As Byte
        Get
            Return _edadMin
        End Get
        Set(ByVal value As Byte)
            _edadMin = value
        End Set
    End Property

    Public Property Edadmax() As Byte
        Get
            Return _edadmax
        End Get
        Set(ByVal value As Byte)
            _edadmax = value
        End Set
    End Property
End Class
