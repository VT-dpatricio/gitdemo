﻿Public Class BEAsegurado
    Inherits BEAuditoria
#Region "Campos"
    Private m_idcertificado As [String]
    Private m_consecutivo As Int16
    Private m_idtipodocumento As [String]
    Private m_ccaseg As [String]
    Private m_nombre1 As [String]
    Private m_nombre2 As [String]
    Private m_apellido1 As [String]
    Private m_apellido2 As [String]
    Private m_direccion As [String]
    Private m_telefono As [String]
    Private m_idciudad As Int32
    Private m_fechanacimiento As DateTime
    Private m_idparentesco As Int16
    Private m_activo As [Boolean] = True
    Private m_nombres As [String]
    Private m_apellidos As [String]
    Private m_nombrecompleto As [String]
    Private m_nombreestado As [String]
    Private m_condicionIva As [String]
#End Region

#Region "Propiedades"
    Public Property Activo() As [Boolean]
        Get
            Return m_activo
        End Get
        Set(ByVal value As [Boolean])
            m_activo = value
        End Set
    End Property

    Public Property IdCertificado() As [String]
        Get
            Return m_idcertificado
        End Get
        Set(ByVal value As [String])
            m_idcertificado = value
        End Set
    End Property

    Public Property Consecutivo() As Int16
        Get
            Return m_consecutivo
        End Get
        Set(ByVal value As Int16)
            m_consecutivo = value
        End Set
    End Property

    Public Property IdTipodocumento() As [String]
        Get
            Return m_idtipodocumento
        End Get
        Set(ByVal value As [String])
            m_idtipodocumento = value
        End Set
    End Property

    Public Property Ccaseg() As [String]
        Get
            Return m_ccaseg
        End Get
        Set(ByVal value As [String])
            m_ccaseg = value
        End Set
    End Property

    Public Property Nombre1() As [String]
        Get
            Return m_nombre1
        End Get
        Set(ByVal value As [String])
            m_nombre1 = value
        End Set
    End Property

    Public Property Nombre2() As [String]
        Get
            Return m_nombre2
        End Get
        Set(ByVal value As [String])
            m_nombre2 = value
        End Set
    End Property

    Public Property Apellido1() As [String]
        Get
            Return m_apellido1
        End Get
        Set(ByVal value As [String])
            m_apellido1 = value
        End Set
    End Property

    Public Property Apellido2() As [String]
        Get
            Return m_apellido2
        End Get
        Set(ByVal value As [String])
            m_apellido2 = value
        End Set
    End Property

    Public Property Direccion() As [String]
        Get
            Return m_direccion
        End Get
        Set(ByVal value As [String])
            m_direccion = value
        End Set
    End Property

    Public Property Telefono() As [String]
        Get
            Return m_telefono
        End Get
        Set(ByVal value As [String])
            m_telefono = value
        End Set
    End Property

    Public Property IdCiudad() As Int32
        Get
            Return m_idciudad
        End Get
        Set(ByVal value As Int32)
            m_idciudad = value
        End Set
    End Property

    Public Property FechaNacimiento() As DateTime
        Get
            Return m_fechanacimiento
        End Get
        Set(ByVal value As DateTime)
            m_fechanacimiento = value
        End Set
    End Property

    Public Property IdParentesco() As Int16
        Get
            Return m_idparentesco
        End Get
        Set(ByVal value As Int16)
            m_idparentesco = value
        End Set
    End Property
    Public Property Nombres() As [String]
        Get
            Return Nombre1 + " " + Nombre2
        End Get
        Set(ByVal value As [String])
            m_nombres = value
        End Set
    End Property
    Public Property Apellidos() As [String]
        Get
            Return Apellido1 + " " + Apellido2
        End Get
        Set(ByVal value As [String])
            m_apellidos = value
        End Set
    End Property
    Public Property NombreCompleto() As [String]
        Get
            Return Apellidos + " " + Nombres
        End Get
        Set(ByVal value As [String])
            m_nombrecompleto = value
        End Set
    End Property
    Public Property NombreEstado() As [String]
        Get
            Return m_nombreestado
        End Get
        Set(ByVal value As [String])
            m_nombreestado = value
        End Set
    End Property


#End Region

#Region "Campos Consulta"
    Private m_nombretipodocumento As [String]
    Private m_nombreparentesco As [String]
    Private m_departamento As [String]
    Private m_provincia As [String]
    Private m_distrito As [String]
    Private m_observacion As [String]
    Private m_estadoCivil As [String]
    Private m_sexo As [String]
    Private m_email As [String]
    Private m_ocupacion As [String]
#End Region

#Region "Propiedades Consulta"
    Public Property NombreTipoDocumento() As [String]
        Get
            Return m_nombretipodocumento
        End Get
        Set(ByVal value As [String])
            m_nombretipodocumento = value
        End Set
    End Property

    Public Property NombreParentesco() As [String]
        Get
            Return m_nombreparentesco
        End Get
        Set(ByVal value As [String])
            m_nombreparentesco = value
        End Set
    End Property

    Public Property Departamento() As [String]
        Get
            Return m_departamento
        End Get
        Set(ByVal value As [String])
            m_departamento = value
        End Set
    End Property

    Public Property Provincia() As [String]
        Get
            Return m_provincia
        End Get
        Set(ByVal value As [String])
            m_provincia = value
        End Set
    End Property

    Public Property Distrito() As [String]
        Get
            Return m_distrito
        End Get
        Set(ByVal value As [String])
            m_distrito = value
        End Set
    End Property

    Public Property Observacion() As [String]
        Get
            Return Me.m_observacion
        End Get
        Set(ByVal value As [String])
            Me.m_observacion = value
        End Set
    End Property

    ''' <summary>
    ''' Estado civil del asegurado.
    ''' </summary>
    Public Property EstadoCivil() As [String]
        Get
            Return Me.m_estadoCivil
        End Get
        Set(ByVal value As [String])
            Me.m_estadoCivil = value
        End Set
    End Property

    ''' <summary>
    ''' Sexo del asegurado
    ''' </summary>
    Public Property Sexo() As [String]
        Get
            Return Me.m_sexo
        End Get
        Set(ByVal value As [String])
            Me.m_sexo = value
        End Set
    End Property

    ''' <summary>
    ''' Email del asegurado.
    ''' </summary>
    Public Property Email() As [String]
        Get
            Return Me.m_email
        End Get
        Set(ByVal value As [String])
            Me.m_email = value
        End Set
    End Property

    ''' <summary>
    ''' Ocupacion del asegurado
    ''' </summary>
    Public Property Ocupacion() As [String]
        Get
            Return Me.m_ocupacion
        End Get
        Set(ByVal value As [String])
            Me.m_ocupacion = value
        End Set
    End Property
#End Region
End Class
