﻿Public Class ProductoEntidadBE
    Inherits GenericEntity

    Sub New()

    End Sub

    Private _IdProductoEntidad As Integer
    Private _IdProducto As Integer
    Private _IdEntidad As Integer


    Property IdProductoEntidad() As Integer
        Get
            Return _IdProductoEntidad
        End Get
        Set(ByVal value As Integer)
            _IdProductoEntidad = value
        End Set
    End Property
    Property IdProducto() As Integer
        Get
            Return _IdProducto
        End Get
        Set(ByVal value As Integer)
            _IdProducto = value
        End Set
    End Property
    Property IdEntidad() As Integer
        Get
            Return _IdEntidad
        End Get
        Set(ByVal value As Integer)
            _IdEntidad = value
        End Set
    End Property


End Class
