﻿Public Class PersonaBE
    Inherits GenericEntity

    Sub New()

    End Sub

    Private _IdPersona As Integer
    Private _TipoDocumento As String
    Private _NumeroDocumento As String
    Private _Nombre As String
    Private _Nombre2 As String
    Private _ApellidoPaterno As String
    Private _ApellidoMaterno As String
    Private _NombreCompleto As String
    Private _RazonSocial As String
    Private _FechaNacimiento As DateTime
    Private _Direccion As String
    Private _Telefono As String
    Private _Sexo As String

    Private _idAsegurador As Integer
    Private _idTipoProducto As String
    Private _activo As Boolean
    Private _maxAsegurados As Integer
    Private _nacimientoRequerido As Boolean
    Private _validarParentesco As Boolean
    Private _obligaCuentaHabiente As Boolean
    Private _cumuloMaximo As Integer
    Private _edadMaxPermanencia As Integer
    Private _diasAnulacion As Integer
    Private _generaCobro As Boolean
    Private _idEntidad As Integer
    Private _vigenciaCobro As Boolean
    Private _maxPolizasAsegurado As Integer
    Private _maxPolizasCuentahabiente As Integer
    Private _maxPolizasRegalo As Integer
    Private _idReglaCobro As Integer
    Private _maxCuentasTitular As Integer
    Private _maxPolizasAseguradoSinDNI As Integer
    Private _validaDireccionCer As Boolean
    Private _validaDireccionAse As Boolean
    Private _cuentaHabienteigualAsegurado As Boolean



    Property IdPersona() As Integer
        Get
            Return _IdPersona
        End Get
        Set(ByVal value As Integer)
            _IdPersona = value
        End Set
    End Property
    Property TipoDocumento() As String
        Get
            Return _TipoDocumento
        End Get
        Set(ByVal value As String)
            _TipoDocumento = value
        End Set
    End Property
    Property NumeroDocumento() As String
        Get
            Return _NumeroDocumento
        End Get
        Set(ByVal value As String)
            _NumeroDocumento = value
        End Set
    End Property
    Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property
    Property Nombre2() As String
        Get
            Return _Nombre2
        End Get
        Set(ByVal value As String)
            _Nombre2 = value
        End Set
    End Property
    Property ApellidoPaterno() As String
        Get
            Return _ApellidoPaterno
        End Get
        Set(ByVal value As String)
            _ApellidoPaterno = value
        End Set
    End Property
    Property ApellidoMaterno() As String
        Get
            Return _ApellidoMaterno
        End Get
        Set(ByVal value As String)
            _ApellidoMaterno = value
        End Set
    End Property
    Property NombreCompleto() As String
        Get
            Return _NombreCompleto
        End Get
        Set(ByVal value As String)
            _NombreCompleto = value
        End Set
    End Property
    Property RazonSocial() As String
        Get
            Return _RazonSocial
        End Get
        Set(ByVal value As String)
            _RazonSocial = value
        End Set
    End Property
    Property FechaNacimiento() As DateTime
        Get
            Return _FechaNacimiento
        End Get
        Set(ByVal value As DateTime)
            _FechaNacimiento = value
        End Set
    End Property
    Property Direccion() As String
        Get
            Return _Direccion
        End Get
        Set(ByVal value As String)
            _Direccion = value
        End Set
    End Property
    Property Telefono() As String
        Get
            Return _Telefono
        End Get
        Set(ByVal value As String)
            _Telefono = value
        End Set
    End Property
    Property Sexo() As String
        Get
            Return _Sexo
        End Get
        Set(ByVal value As String)
            _Sexo = value
        End Set
    End Property


    Property idAsegurador() As Integer
        Get
            Return _idAsegurador
        End Get
        Set(ByVal value As Integer)
            _idAsegurador = value
        End Set
    End Property
    Property idTipoProducto() As String
        Get
            Return _idTipoProducto
        End Get
        Set(ByVal value As String)
            _idTipoProducto = value
        End Set
    End Property
    Property activo() As Boolean
        Get
            Return _activo
        End Get
        Set(ByVal value As Boolean)
            _activo = value
        End Set
    End Property
    Property maxAsegurados() As Integer
        Get
            Return _maxAsegurados
        End Get
        Set(ByVal value As Integer)
            _maxAsegurados = value
        End Set
    End Property
    Property nacimientoRequerido() As Boolean
        Get
            Return _nacimientoRequerido
        End Get
        Set(ByVal value As Boolean)
            _nacimientoRequerido = value
        End Set
    End Property
    Property validarParentesco() As Boolean
        Get
            Return _validarParentesco
        End Get
        Set(ByVal value As Boolean)
            _validarParentesco = value
        End Set
    End Property
    Property obligaCuentaHabiente() As Boolean
        Get
            Return _obligaCuentaHabiente
        End Get
        Set(ByVal value As Boolean)
            _obligaCuentaHabiente = value
        End Set
    End Property
    Property cumuloMaximo() As Integer
        Get
            Return _cumuloMaximo
        End Get
        Set(ByVal value As Integer)
            _cumuloMaximo = value
        End Set
    End Property
    Property edadMaxPermanencia() As Integer
        Get
            Return _edadMaxPermanencia
        End Get
        Set(ByVal value As Integer)
            _edadMaxPermanencia = value
        End Set
    End Property
    Property diasAnulacion() As Integer
        Get
            Return _diasAnulacion
        End Get
        Set(ByVal value As Integer)
            _diasAnulacion = value
        End Set
    End Property
    Property generaCobro() As Boolean
        Get
            Return _generaCobro
        End Get
        Set(ByVal value As Boolean)
            _generaCobro = value
        End Set
    End Property
    Property idEntidad() As Integer
        Get
            Return _idEntidad
        End Get
        Set(ByVal value As Integer)
            _idEntidad = value
        End Set
    End Property
    Property vigenciaCobro() As Boolean
        Get
            Return _vigenciaCobro
        End Get
        Set(ByVal value As Boolean)
            _vigenciaCobro = value
        End Set
    End Property
    Property maxPolizasAsegurado() As Integer
        Get
            Return _maxPolizasAsegurado
        End Get
        Set(ByVal value As Integer)
            _maxPolizasAsegurado = value
        End Set
    End Property
    Property maxPolizasCuentahabiente() As Integer
        Get
            Return _maxPolizasCuentahabiente
        End Get
        Set(ByVal value As Integer)
            _maxPolizasCuentahabiente = value
        End Set
    End Property
    Property maxPolizasRegalo() As Integer
        Get
            Return _maxPolizasRegalo
        End Get
        Set(ByVal value As Integer)
            _maxPolizasRegalo = value
        End Set
    End Property
    Property idReglaCobro() As Integer
        Get
            Return _idReglaCobro
        End Get
        Set(ByVal value As Integer)
            _idReglaCobro = value
        End Set
    End Property
    Property maxCuentasTitular() As Integer
        Get
            Return _maxCuentasTitular
        End Get
        Set(ByVal value As Integer)
            _maxCuentasTitular = value
        End Set
    End Property
    Property maxPolizasAseguradoSinDNI() As Integer
        Get
            Return _maxPolizasAseguradoSinDNI
        End Get
        Set(ByVal value As Integer)
            _maxPolizasAseguradoSinDNI = value
        End Set
    End Property
    Property validaDireccionCer() As Boolean
        Get
            Return _validaDireccionCer
        End Get
        Set(ByVal value As Boolean)
            _validaDireccionCer = value
        End Set
    End Property
    Property validaDireccionAse() As Boolean
        Get
            Return _validaDireccionAse
        End Get
        Set(ByVal value As Boolean)
            _validaDireccionAse = value
        End Set
    End Property
    Property cuentaHabienteigualAsegurado() As Boolean
        Get
            Return _cuentaHabienteigualAsegurado
        End Get
        Set(ByVal value As Boolean)
            _cuentaHabienteigualAsegurado = value
        End Set
    End Property



End Class



