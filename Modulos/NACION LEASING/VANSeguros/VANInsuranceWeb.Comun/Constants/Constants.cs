﻿using System;
using System.Collections.Generic;
using System.Text;


namespace VAN.InsuranceWeb.Comun
{
    public class Constants
    {
        public const string PATH_ICONS_TOOLTIPS_SALE = @"../Img/Icons/CrossSelling";
        public const string PATH_ICONS =  @"././Img/Icons/CrossSelling";
        public const string SEC_PRODUCTO = @"<table width='170px' border='0' cellspacing='0'> 
                                                    <tr>
                                                    <td class='csTd' colspan='4'>
	                                                        <input type='checkbox' id='checkbox-7-3-" + TAG_CS_INDEX + @"' title='" + TAG_CS_NOMBRE_PRODUCTO + @"' data-name='" + TAG_CS_IDPRODUCTO + @"'/>
                                                            <label  style='width:170px; height:55px; color:#ffffff; background-color:" + TAG_CS_PRODUCT_BUTTON_BACKGOUND_COLOR + @"' for='checkbox-7-3-" + TAG_CS_INDEX + @"'>
                                                                <span runat='server' id='Span2'>" + TAG_CS_NOMBRE_PRODUCTO + @"</span>
                                                            </label>
	                                                </td>
                                                    </tr>
                                                    <tr>
                                                    <td class='csTd' width='43px' height='24px'>" + TAG_CS_MEDIOPAGO + @"</td>
                                                    <td class='csTd' width='51px' height='24px'>" + TAG_CS_VENTA + @"</td>
                                                    <td class='csTd' width='58px' height='24px'>" + TAG_CS_INTERES + @"</td>
                                                    <td class='csTd' width='70px' height='24px'><a id class='tooltip' href='#'>+ iNFO<span class='custom warning'><img src='" + TAG_CS_ICONS_TOOLTIPS + @"/##TIPO_PRODUCTO##.png' alt='Warning' height='48' width='48' /><em>" + TAG_CS_NOMBRE_PRODUCTO + @"</em>" + TAG_CS_DESCRI_PLAN + @"</span></a></td>
                                                    </tr>
                                             </table>";

        public const string WILDCARD_SPACE = "&nbsp;";
        public const string WILDCARD_CHARACTER_RETURN_HTML = "<br>";

        public const string CSS_SALE_ACTIVE = "sale-active";
        public const string CSS_SALE_EXIST = "sale-exist";
        
        //NEW public const string TAG_CS_HTML_IMAGE_MEDIO_PAGO = "<div class='iconoMedioPago' title='Medio de Pago'></div>";
        public const string TAG_CS_HTML_IMAGE_MEDIO_PAGO = "<img src='" + TAG_CS_ICONS + @"/" + TAG_CS_ICO_MEDIO_PAGO + @"' width='auto' height='auto' alt='mp' title='Medio de Pago' />";
        //NEW public const string TAG_CS_HTML_IMAGE_VENTA = "<div class='iconoVenta' onclick='javascript:OpenSale(" + TAG_CS_IDPRODUCTO + @")' title='Vender Producto'></div>";
        public const string TAG_CS_HTML_IMAGE_VENTA = "<img src='" + TAG_CS_ICONS + @"/" + TAG_CS_ICO_SALE + @"' class='iconPanel' onclick='javascript:OpenSale(" + TAG_CS_IDPRODUCTO + @")' alt='mp' title='Vender Producto' />";
        public const string TAG_CS_HTML_IMAGE_INTEREST = "<img src='" + TAG_CS_ICONS + @"/" + ICO_INTEREST + @"' class='iconPanel' alt='mp' title='Solicitudes Contacto' onclick='javascript:OpenSolicitudContacto(" + TAG_CS_IDPRODUCTO + @")' />";
        //public const string TAG_CS_HTML_IMAGE_INTEREST = "<img src=getPathImage()" + @"/" + ICO_INTEREST + @"' class='iconPanel' alt='mp' title='Solicitudes Contacto' onclick='javascript:OpenSolicitudContacto(" + TAG_CS_IDPRODUCTO + @")' />";
        
        public const string ICO_MEDIO_PAGO_ENABLED = "iEPayment.png";
        public const string ICO_MEDIO_PAGO_DISABLED = "iDPayment.png";
        public const string ICO_INTEREST = "iInterest.png";
        public const string ICO_SALE = "iSale.png";

        public const string TAG_CS_ICO_MEDIO_PAGO = "##ICO_MEDIO_PAGO##";
        public const string TAG_CS_ICO_INTEREST = "##ICO_INTEREST##";
        public const string TAG_CS_ICO_SALE = "##ICO_SALE##";
        public const string TAG_CS_INDEX = "##INDEX##";
        public const string TAG_CS_NOMBRE_PRODUCTO = "##NOMBRE_PRODUCTO##";
        public const string TAG_CS_IDPRODUCTO = "##IDPRODUCTO##";
        public const string TAG_CS_COUNT_TIPO_PROD = "##COUNT_TIPO_PROD##";
        public const string TAG_CS_ICONS = "##ICONS##";
        public const string TAG_CS_ICONS_TOOLTIPS = "##TAG_CS_ICONS_TOOLTIPS##";
        public const string TAG_CS_DESCRI_PLAN = "##DESCRI_PLAN##";
        public const string TAG_CS_TIPO_PRODUCTO = "##TIPO_PRODUCTO##";
        public const string TAG_CS_INTERES = "##INTERES##";
        public const string TAG_CS_MEDIOPAGO ="##MEDIOPAGO##";
        public const string TAG_CS_VENTA = "##VENTA##";
        public const string TAG_CS_NOMBRE_PRODUCTO_PARAM = "##NOMBRE_PRODUCTO_PARAM##";
        public const string TAG_CS_PRODUCT_BUTTON_BACKGOUND_COLOR = "#BACKGROUND_COLOR#";

        public const string CATEGORIA_MEDIO_PAGO = "MEDIO_PAGO";
        public const string CATEGORIA_PLAN = "PLAN";
        public const string CATEGORIA_VALIDACION = "VALIDACION";

        public const string STYLE_AUTOAJUSTE = " style='width:100%; background-color:#F7F7F7' ";
        public const string STYLE_DEFAULT_BACKGROUND_COLOR = "#0054A0"; //"#1b7e5a"; //Color de CROSS SELLING para posible venta
        public const string STYPE_BACKGROUND_COLOR_TIENE_CERTIFICADO ="#C679AC";//dpatricio:cambiar color
    }
}
