﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports Microsoft.Practices.EnterpriseLibrary.Data

Public Class InfoProductoDA

    Dim _db As Database = Nothing

    Public Sub New()
        _db = Conexion.Instancia.Database()
    End Sub

    Function Agregar(ByVal Objeto As InfoProductoBE, Optional ByVal Transac As DbTransaction = Nothing) As String
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_InfoProducto_Agregar", Nothing, Objeto.IdTipoInfoProducto, Objeto.Descripcion, Objeto.Abrev, Objeto.Panel, Objeto.Estado, Objeto.UsuarioReg, Objeto.FechaReg, Objeto.UsuarioMod, Objeto.FechaMod)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Modificar(ByVal Objeto As InfoProductoBE, Optional ByVal Transac As DbTransaction = Nothing) As Integer
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_InfoProducto_Modificar", Objeto.IdInfoProducto, Objeto.IdTipoInfoProducto, Objeto.Descripcion, Objeto.Abrev, Objeto.Panel, Objeto.Estado, Objeto.UsuarioMod, Objeto.FechaMod)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Eliminar(ByVal Objeto As InfoProductoBE, Optional ByVal Transac As DbTransaction = Nothing) As Integer
        Dim resultado As String = "0"
        Dim _DetalleInfoProductoDA As New DetalleInfoProductoDA
        Dim _DetalleInfoProductoBE As New DetalleInfoProductoBE
        Try

            _DetalleInfoProductoBE.UsuarioMod = ""
            _DetalleInfoProductoBE.FechaReg = Now
            _DetalleInfoProductoBE.IdInfoProducto = Objeto.IdInfoProducto


            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_InfoProducto_Eliminar", Objeto.IdInfoProducto, Objeto.Estado, Objeto.UsuarioMod, Objeto.FechaMod)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try

                        resultado = _DetalleInfoProductoDA.Eliminar_InfoProducto(_DetalleInfoProductoBE, Transaction)

                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of InfoProductoBE)
        Dim Lista As New List(Of InfoProductoBE)

        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_InfoProducto_Todos")
                While dr.Read()
                    Lista.Add(Populate.InfoProducto_Lista(dr))
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_InfoProducto_Todos")
                While dr.Read()
                    Lista.Add(Populate.InfoProducto_Lista(dr))
                End While
            End Using
        End If
        Return Lista
    End Function

    Function Listar_BDJanus(ByVal IdTipo As Integer, ByVal Descripcion As String, Optional ByVal Transaction As DbTransaction = Nothing) As List(Of InfoProductoBE)
        Dim Lista As New List(Of InfoProductoBE)

        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_BDJanus_InfoProducto_Listar", IdTipo, Descripcion)
                While dr.Read()
                    Lista.Add(Populate.InfoProducto_ListaBDJanus(dr))
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_BDJanus_InfoProducto_Listar", IdTipo, Descripcion)
                While dr.Read()
                    Lista.Add(Populate.InfoProducto_ListaBDJanus(dr))
                End While
            End Using
        End If
        Return Lista
    End Function


    Function Listar_Id(ByVal Objeto As InfoProductoBE, Optional ByVal Transaction As DbTransaction = Nothing) As InfoProductoBE
        Dim _InfoProductoBE As New InfoProductoBE

        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_InfoProducto_Id", Objeto.IdInfoProducto)
                While dr.Read()
                    _InfoProductoBE = Populate.InfoProducto_Lista(dr)
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_InfoProducto_Id", Objeto.IdInfoProducto)
                While dr.Read()
                    _InfoProductoBE = Populate.InfoProducto_Lista(dr)
                End While
            End Using
        End If
        Return _InfoProductoBE
    End Function

    Function Listar_IdTipoInfoProducto(ByVal Objeto As InfoProductoBE, Optional ByVal Transaction As DbTransaction = Nothing) As List(Of InfoProductoBE)
        Dim Lista As New List(Of InfoProductoBE)

        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_InfoProducto_IdTipoInfoProducto", Objeto.IdTipoInfoProducto)
                While dr.Read()
                    Lista.Add(Populate.InfoProducto_Lista(dr))
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_InfoProducto_IdTipoInfoProducto", Objeto.IdTipoInfoProducto)
                While dr.Read()
                    Lista.Add(Populate.InfoProducto_Lista(dr))
                End While
            End Using
        End If
        Return Lista
    End Function

End Class
