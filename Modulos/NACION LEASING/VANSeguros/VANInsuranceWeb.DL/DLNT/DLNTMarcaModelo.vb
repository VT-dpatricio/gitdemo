﻿Imports VAN.InsuranceWeb.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLNTMarcaModelo
    Inherits DLBase
    Implements IDLNTDataEntry


    Public Function ValidaIMEI(ByVal pValor As String) As String
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("BW_PRC_SEL_ValidaIMEI", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@Imei", SqlDbType.VarChar, 50).Value = pValor

        Dim result As String = String.Empty
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                result = rd.GetString(rd.GetOrdinal("Mensaje")).Replace("'", " ")
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return result

    End Function

    Public Function SeleccionarTodos(pCodigo As String) As IList
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("BW_PRC_SEL_DatosMarcaModelo", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@Marca", SqlDbType.VarChar).Value = IIf(String.IsNullOrEmpty(pCodigo), "", pCodigo)
        Dim oBE As BEMarcaModelo
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BEMarcaModelo

                oBE.IDCotizacionCelular = rd.GetInt32(rd.GetOrdinal("IdCotizacionCelular"))
                oBE.Marca = rd.GetString(rd.GetOrdinal("Marca"))
                oBE.Modelo = rd.GetString(rd.GetOrdinal("Modelo"))
                oBE.Opcion = rd.GetString(rd.GetOrdinal("Opcion"))
                oBE.Importe = rd.GetDecimal(rd.GetOrdinal("Importe"))
                oBE.Deducible = rd.GetDecimal(rd.GetOrdinal("Deducible"))
                oBE.ProductCode = rd.GetString(rd.GetOrdinal("ProductCode"))
                oBE.ProductPrice = rd.GetDecimal(rd.GetOrdinal("ProductPrice"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function Seleccionar(pCodigo As String) As IList
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("BW_PRC_SEL_Modelo", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@Marca", SqlDbType.VarChar).Value = pCodigo
        Dim oBE As BEMarcaModelo
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BEMarcaModelo
                oBE.Marca = rd.GetString(rd.GetOrdinal("Marca"))
                oBE.Modelo = rd.GetString(rd.GetOrdinal("Modelo"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function Seleccionar() As IList Implements IDLNTDataEntry.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(pEntidad As BEBase) As IList Implements IDLNTDataEntry.Seleccionar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("BW_BuscarMarcaModeloVeh", cn)
        cm.CommandType = CommandType.StoredProcedure
        Dim oBE As BEMarcaModelo = DirectCast(pEntidad, BEMarcaModelo)
        cm.Parameters.Add("@IDMarca", SqlDbType.Int).Value = oBE.IDMarca
        cm.Parameters.Add("@IDProducto", SqlDbType.Int).Value = oBE.IDProducto
        cm.Parameters.Add("@Buscar", SqlDbType.VarChar, 250).Value = oBE.Buscar
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BEMarcaModelo
                oBE.IDMarca = rd.GetInt32(rd.GetOrdinal("IDMarca"))
                oBE.IDModelo = rd.GetInt32(rd.GetOrdinal("IDModelo"))
                oBE.Marca = rd.GetString(rd.GetOrdinal("Marca"))
                oBE.Modelo = rd.GetString(rd.GetOrdinal("Modelo"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As IList Implements IDLNTDataEntry.Seleccionar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("BW_ListarMarcaVehiculo", cn)
        cm.CommandType = CommandType.StoredProcedure
        Dim oBE As BEMarcaModelo
        cm.Parameters.Add("@IDProducto", SqlDbType.Int).Value = pCodigo
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BEMarcaModelo
                oBE.IDMarca = rd.GetInt32(rd.GetOrdinal("IDMarca"))
                oBE.Marca = rd.GetString(rd.GetOrdinal("Marca"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista

    End Function

    Public Function SeleccionarBE(pCodigo As Integer) As BEBase Implements IDLNTDataEntry.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function
End Class
