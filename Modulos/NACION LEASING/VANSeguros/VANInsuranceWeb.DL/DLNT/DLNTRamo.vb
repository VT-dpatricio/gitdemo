﻿Imports VAN.InsuranceWeb.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLNTRamo
    Inherits DLBase
    Implements IDLNTDataEntry

    Public Function Seleccionar() As System.Collections.IList Implements IDLNTDataEntry.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pEntidad As BEBase) As System.Collections.IList Implements IDLNTDataEntry.Seleccionar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("BW_BuscarRamo", cn)
        cm.CommandType = CommandType.StoredProcedure
        Dim oBE As BERamo = DirectCast(pEntidad, BERamo)
        cm.Parameters.Add("@Buscar", SqlDbType.VarChar, 50).Value = oBE.Buscar
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BERamo
                oBE.IdRamo = rd.GetInt32(rd.GetOrdinal("IdRamo"))
                oBE.Nombre = rd.GetString(rd.GetOrdinal("Nombre"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    '---
    Public Function Seleccionar(ByVal pCodigo As Integer) As System.Collections.IList Implements IDLNTDataEntry.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BEBase Implements IDLNTDataEntry.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function
End Class