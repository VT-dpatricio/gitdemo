﻿Imports VAN.InsuranceWeb.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLNTInfoParametro
    Inherits DLBase
    Implements IDLNTDataEntry

    Public Function Seleccionar(ByVal pIDProducto As Integer, ByVal pCodParametro As String) As List(Of BEInfoParametro)
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("PRC_SEL_InfoParametro", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@idProducto", SqlDbType.Int).Value = pIDProducto
        cm.Parameters.Add("@codigo", SqlDbType.VarChar).Value = pCodParametro

        Dim oBE As BEInfoParametro
        Dim lista As New List(Of BEInfoParametro)
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BEInfoParametro
                oBE.IdEntidad = rd.GetInt32(rd.GetOrdinal("IDEntidad"))
                If Not rd.IsDBNull(rd.GetOrdinal("IdAsegurador")) Then
                    oBE.IdAsegurador = rd.GetInt32(rd.GetOrdinal("IdAsegurador"))
                End If
                oBE.IdInfoParametro = rd.GetInt32(rd.GetOrdinal("IdInfoParametro"))
                oBE.IdParametro = rd.GetString(rd.GetOrdinal("IdParametro"))
                If Not rd.IsDBNull(rd.GetOrdinal("IdProducto")) Then
                    oBE.IdProducto = rd.GetInt32(rd.GetOrdinal("IdProducto"))
                End If
                oBE.ValorString = rd.GetString(rd.GetOrdinal("ValorString"))

                If Not rd.IsDBNull(rd.GetOrdinal("ValorNum")) Then
                    oBE.ValorNum = rd.GetDecimal(rd.GetOrdinal("ValorNum"))
                End If
                If Not rd.IsDBNull(rd.GetOrdinal("ValorDate")) Then
                    oBE.ValorDate = rd.GetDateTime(rd.GetOrdinal("ValorDate"))
                End If
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function Seleccionar() As IList Implements IDLNTDataEntry.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(pEntidad As BEBase) As IList Implements IDLNTDataEntry.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(pCodigo As Integer) As IList Implements IDLNTDataEntry.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(pCodigo As Integer) As BEBase Implements IDLNTDataEntry.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function
End Class
