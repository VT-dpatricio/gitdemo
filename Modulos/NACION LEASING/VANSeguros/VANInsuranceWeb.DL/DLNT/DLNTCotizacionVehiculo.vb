Imports VAN.InsuranceWeb.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLNTCotizacionVehiculo
    Inherits DLBase
    Implements IDLNTDataEntry

    Public Function Seleccionar() As System.Collections.IList Implements IDLNTDataEntry.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pEntidad As BEBase) As System.Collections.IList Implements IDLNTDataEntry.Seleccionar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("BW_BuscarCotizacionVehiculo", cn)
        cm.CommandType = CommandType.StoredProcedure
        Dim oBE As BECotizacionVehiculo = DirectCast(pEntidad, BECotizacionVehiculo)
        cm.Parameters.Add("@IDProducto", SqlDbType.Int).Value = oBE.IdProducto
        cm.Parameters.Add("@IDTipoDocumento", SqlDbType.VarChar, 3).Value = oBE.IdTipoDocumento
        cm.Parameters.Add("@NroDocumento", SqlDbType.VarChar, 20).Value = oBE.NroDocumento
        cm.Parameters.Add("@IDTipoDocumentoAseg", SqlDbType.VarChar, 3).Value = oBE.IdTipoDocumentoAseg
        cm.Parameters.Add("@NroDocumentoAseg", SqlDbType.VarChar, 20).Value = oBE.NroDocumentoAseg
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BECotizacionVehiculo
                oBE.NroCotizacion = rd.GetDecimal(rd.GetOrdinal("nroCotizacion"))
                oBE.IdMarca = rd.GetInt32(rd.GetOrdinal("IdMarca"))
                oBE.IdModelo = rd.GetInt32(rd.GetOrdinal("IdModelo"))
                oBE.Marca = rd.GetString(rd.GetOrdinal("desMarca"))
                oBE.Modelo = rd.GetString(rd.GetOrdinal("desModelo"))
                oBE.Anio = rd.GetInt32(rd.GetOrdinal("anioFab"))
                oBE.NroPlaca = rd.GetString(rd.GetOrdinal("nroPlaca"))
                oBE.ValorVehiculo = rd.GetDecimal(rd.GetOrdinal("valOriVehiculo"))
                oBE.PrimaMensual = rd.GetDecimal(rd.GetOrdinal("primaMensual"))
                oBE.PrimaTotal = rd.GetDecimal(rd.GetOrdinal("primaTotal"))
                oBE.MontoAsegurado = rd.GetDecimal(rd.GetOrdinal("MontoAsegurado"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    '---
    Public Function Seleccionar(ByVal pCodigo As Integer) As System.Collections.IList Implements IDLNTDataEntry.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BEBase Implements IDLNTDataEntry.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function
End Class
