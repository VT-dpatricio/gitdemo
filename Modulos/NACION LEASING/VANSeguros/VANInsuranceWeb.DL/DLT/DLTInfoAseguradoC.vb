﻿Imports VAN.InsuranceWeb.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLTInfoAseguradoC
    Public Function DAInsertaInfoAseguradoC(ByVal pObjBEInfoAseguradoC As BEInfoAseguradoC, ByVal pIdProducto As Int32, ByVal pSqlCon As SqlConnection, ByVal pSqlTranx As SqlTransaction) As Int32
        Dim filasAfectadas As Int32 = -1

        Using sqlCmdInsInfAsegC As New SqlCommand("BW_InsertarInfoAseguradoC", pSqlCon)
            sqlCmdInsInfAsegC.Transaction = pSqlTranx
            sqlCmdInsInfAsegC.CommandType = CommandType.StoredProcedure

            Dim sqlIdCertificado As SqlParameter = sqlCmdInsInfAsegC.Parameters.Add("@idCertificado", SqlDbType.VarChar, 25)
            sqlIdCertificado.Direction = ParameterDirection.Input
            sqlIdCertificado.Value = pObjBEInfoAseguradoC.IdCertificado

            Dim sqlConsecutivo As SqlParameter = sqlCmdInsInfAsegC.Parameters.Add("@consecutivo", SqlDbType.SmallInt)
            sqlConsecutivo.Direction = ParameterDirection.Input
            sqlConsecutivo.Value = pObjBEInfoAseguradoC.Consecutivo

            Dim sqlValorNum As SqlParameter = sqlCmdInsInfAsegC.Parameters.Add("@valorNum", SqlDbType.[Decimal])
            sqlValorNum.Direction = ParameterDirection.Input
            If pObjBEInfoAseguradoC.ValorNum = -1 Then
                sqlValorNum.Value = DBNull.Value
            Else
                sqlValorNum.Value = pObjBEInfoAseguradoC.ValorNum
            End If

            Dim sqlValorDate As SqlParameter = sqlCmdInsInfAsegC.Parameters.Add("@valorDate", SqlDbType.DateTime)
            sqlValorDate.Direction = ParameterDirection.Input
            If pObjBEInfoAseguradoC.ValorDate = DateTime.Parse("1900-01-01") Then
                sqlValorDate.Value = DBNull.Value
            Else
                sqlValorDate.Value = pObjBEInfoAseguradoC.ValorDate
            End If

            Dim sqlValorString As SqlParameter = sqlCmdInsInfAsegC.Parameters.Add("@valorString", SqlDbType.VarChar, 250)
            sqlValorString.Direction = ParameterDirection.Input
            If pObjBEInfoAseguradoC.ValorString = [String].Empty Then
                sqlValorString.Value = DBNull.Value
            Else
                sqlValorString.Value = pObjBEInfoAseguradoC.ValorString
            End If

            Dim sqlIdInfoAsegurado As SqlParameter = sqlCmdInsInfAsegC.Parameters.Add("@idInfoAsegurado", SqlDbType.Int)
            sqlIdInfoAsegurado.Direction = ParameterDirection.Input
            sqlIdInfoAsegurado.Value = pObjBEInfoAseguradoC.IdInfoAsegurado

            filasAfectadas = sqlCmdInsInfAsegC.ExecuteNonQuery()
        End Using
        If filasAfectadas <= 0 Then
            Return -1
        Else
            Return filasAfectadas
        End If
    End Function
End Class
