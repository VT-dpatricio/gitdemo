﻿Imports VAN.InsuranceWeb.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient


Public Class DLTSolicitudConctactoCliente
    Inherits DLBase
    Public Function ListarSolicitudesContacto(ByVal pEntidad As BEFiltroSolicitudContacto) As DataTable
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("BW_PRC_SEL_SolicitudesContactoVendedor", cn)
        Dim dt As New DataTable
        Try
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Add("@IdUsuario", SqlDbType.VarChar).Value = pEntidad.IDUsuario
            cn.Open()
            Dim sqlAddapter As New SqlDataAdapter(cm)

            sqlAddapter.Fill(dt)
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return dt
    End Function

    Public Function Guardar(ByVal pEntidad As BE.BEBase) As Boolean
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("BW_PRC_INS_SolicitudContactoCliente", cn)
        Dim oBe As BESolicitudContactoCliente = DirectCast(pEntidad, BESolicitudContactoCliente)

        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@IDCertificadoOrigen", SqlDbType.VarChar).Value = IIf(String.IsNullOrEmpty(oBe.IDCertificadoOrigen), DBNull.Value, oBe.IDCertificadoOrigen)
        cm.Parameters.Add("@IDTipoDocumento", SqlDbType.VarChar).Value = oBe.IdTipoDocumento
        cm.Parameters.Add("@ccCliente", SqlDbType.VarChar).Value = oBe.ccCliente
        cm.Parameters.Add("@Nombre1", SqlDbType.VarChar).Value = oBe.Nombre1
        cm.Parameters.Add("@Nombre2", SqlDbType.VarChar).Value = IIf(String.IsNullOrEmpty(oBe.Nombre2), DBNull.Value, oBe.Nombre2)
        cm.Parameters.Add("@Apellido1", SqlDbType.VarChar).Value = oBe.Apellido1
        cm.Parameters.Add("@Apellido2", SqlDbType.VarChar).Value = IIf(String.IsNullOrEmpty(oBe.Apellido2), DBNull.Value, oBe.Apellido2)
        cm.Parameters.Add("@Email", SqlDbType.VarChar).Value = IIf(String.IsNullOrEmpty(oBe.Email), DBNull.Value, oBe.Email)
        cm.Parameters.Add("@Telefono", SqlDbType.VarChar).Value = If(String.IsNullOrEmpty(oBe.Telefono), DBNull.Value, oBe.Telefono)
        cm.Parameters.Add("@Celular", SqlDbType.VarChar).Value = If(String.IsNullOrEmpty(oBe.Celular), DBNull.Value, oBe.Celular)
        cm.Parameters.Add("@IDInformador", SqlDbType.VarChar).Value = oBe.IDInformador
        cm.Parameters.Add("@Observacion", SqlDbType.VarChar).Value = oBe.Observaciones
        cm.Parameters.Add("@IdUsuarioCreacion", SqlDbType.VarChar).Value = oBe.IDUsuarioCreacion
        cm.Parameters.Add("@CodigoTipoFrecuenciaContacto", SqlDbType.VarChar).Value = If(String.IsNullOrEmpty(oBe.CodigoTipoFrecuencia), DBNull.Value, oBe.CodigoTipoFrecuencia)
        cm.Parameters.Add("@ValorFrecuenciaContacto", SqlDbType.VarChar).Value = If(String.IsNullOrEmpty(oBe.ValorFrecuenciaContacto), DBNull.Value, oBe.ValorFrecuenciaContacto)

        Dim dtProductos As DataTable = VAN.InsuranceWeb.Comun.Common.Lista_KeyValue()

        For Each sp As BESolicitudContactoProducto In oBe.Productos
            Dim row As DataRow = dtProductos.NewRow
            row("NameKey") = "Producto"
            row("Value") = sp.IdProducto
            row("DataType") = "S"c
            row("Source") = ""
            dtProductos.Rows.Add(row)
        Next

        cm.Parameters.Add("@IdProductosContacto", SqlDbType.Structured).Value = dtProductos 'Pasar como lista de separado por coma
        cm.Parameters.Add("ReturnValue", SqlDbType.Bit).Direction = ParameterDirection.ReturnValue
        Dim resultado As Boolean = False
        Try
            cn.Open()
            cm.ExecuteNonQuery()
            resultado = cm.Parameters("ReturnValue").Value()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return resultado
    End Function

    Public Function ListarSolicitudesContacto(pIdProducto As Integer, pEntidad As BESolicitudContactoCliente) As System.Collections.IList
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("BW_PRC_SEL_SolicitudesContactoCliente", cn)
        Dim oBE As BESolicitudContactoCliente = DirectCast(pEntidad, BESolicitudContactoCliente)
        Dim lista As New ArrayList
        Try
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.Add("@IdProducto", SqlDbType.Int).Value = pIdProducto
            cm.Parameters.Add("@IDTipoDocumento", SqlDbType.VarChar).Value = oBE.IdTipoDocumento
            cm.Parameters.Add("@ccCliente", SqlDbType.VarChar).Value = oBE.ccCliente

            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BESolicitudContactoCliente
                oBE.IdSolicitudContacto = rd.GetInt32(rd.GetOrdinal("IdSolicitudContacto"))
                oBE.FechaCreacion = rd.GetString(rd.GetOrdinal("FechaCreacion"))
                oBE.CantidadDiasContacto = rd.GetInt32(rd.GetOrdinal("CantidadDiasContacto"))
                oBE.ccCliente = rd.GetString(rd.GetOrdinal("ccCliente"))
                oBE.NombreCompleto = rd.GetString(rd.GetOrdinal("NombreCompleto"))
                oBE.Observaciones = rd.GetString(rd.GetOrdinal("Observaciones"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function ListarTipoFrecuenciaContacto() As System.Collections.IList
        Dim oBE As BETipoFrecuenciaContacto
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("BW_PRC_SEL_TipoFrecuenciaContacto", cn)
        Dim lista As New ArrayList
        Try
            cm.CommandType = CommandType.StoredProcedure
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BETipoFrecuenciaContacto
                oBE.Codigo = rd.GetString(rd.GetOrdinal("Codigo"))
                oBE.Nombre = rd.GetString(rd.GetOrdinal("Nombre"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function
End Class
