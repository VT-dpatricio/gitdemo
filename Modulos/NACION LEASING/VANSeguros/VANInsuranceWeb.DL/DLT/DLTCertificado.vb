﻿Imports VAN.InsuranceWeb.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLTCertificado

    ''' <summary>
    ''' Método que inserta un certificado en la base de datos.
    ''' </summary>
    ''' <param name="pObjBECertificado">Certificado a Insertar.</param>
    ''' <param name="pSqlCon">Objeto SqlConnection</param>
    ''' <param name="pSqlTranx">Objeto SqlTransaction</param>
    ''' <returns>
    ''' Devuelve un entero
    ''' </returns>
    Public Function DAInsertarCertificado(ByVal pObjBECertificado As BECertificado, ByVal pSqlCon As SqlConnection, ByVal pSqlTranx As SqlTransaction) As Int32
        Dim filasAfectadas As Int32 = -1

        Using sqlCmdInsCertificado As New SqlCommand("BW_InsertarCertificado", pSqlCon)
            sqlCmdInsCertificado.Transaction = pSqlTranx
            sqlCmdInsCertificado.CommandType = CommandType.StoredProcedure

            Dim sqlIdCertificado As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@idCertificado", SqlDbType.VarChar, 25)
            sqlIdCertificado.Direction = ParameterDirection.Input
            sqlIdCertificado.Value = pObjBECertificado.IdCertificado

            Dim sqlIdProducto As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@idproducto", SqlDbType.Int)
            sqlIdProducto.Direction = ParameterDirection.Input
            sqlIdProducto.Value = pObjBECertificado.IdProducto

            Dim sqlNumCertificado As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@numcertificado", SqlDbType.[Decimal])
            sqlNumCertificado.Direction = ParameterDirection.Input
            sqlNumCertificado.Value = pObjBECertificado.NumCertificado

            Dim sqlOpcion As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@opcion", SqlDbType.VarChar, 10)
            sqlOpcion.Direction = ParameterDirection.Input
            sqlOpcion.Value = pObjBECertificado.Opcion

            Dim sqlIdFrecuencia As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@idfrecuencia", SqlDbType.Int)
            sqlIdFrecuencia.Direction = ParameterDirection.Input
            sqlIdFrecuencia.Value = pObjBECertificado.IdFrecuencia

            Dim sqlIdOficina As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@idoficina", SqlDbType.Int)
            sqlIdOficina.Direction = ParameterDirection.Input
            sqlIdOficina.Value = pObjBECertificado.IdOficina

            Dim sqlIdInformador As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@idinformador", SqlDbType.VarChar, 15)
            sqlIdInformador.Direction = ParameterDirection.Input
            sqlIdInformador.Value = pObjBECertificado.IdInformador

            Dim sqlIdMedioPago As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@idmediopago", SqlDbType.VarChar, 2)
            sqlIdMedioPago.Direction = ParameterDirection.Input
            sqlIdMedioPago.Value = pObjBECertificado.IdMedioPago

            Dim sqlIdEstadoCertificado As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@idestadocertificado", SqlDbType.Int)
            sqlIdEstadoCertificado.Direction = ParameterDirection.Input
            sqlIdEstadoCertificado.Value = pObjBECertificado.IdEstadoCertificado

            Dim sqlMontoAsegurado As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@montoasegurado", SqlDbType.[Decimal])
            sqlMontoAsegurado.Direction = ParameterDirection.Input
            sqlMontoAsegurado.Value = pObjBECertificado.MontoAsegurado

            Dim sqlPrimaBruta As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@primabruta", SqlDbType.[Decimal])
            sqlPrimaBruta.Direction = ParameterDirection.Input
            sqlPrimaBruta.Value = pObjBECertificado.PrimaBruta

            Dim sqlIVA As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@iva", SqlDbType.[Decimal])
            sqlIVA.Direction = ParameterDirection.Input
            sqlIVA.Value = pObjBECertificado.Iva

            Dim sqlPrimaTotal As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@primatotal", SqlDbType.[Decimal])
            sqlPrimaTotal.Direction = ParameterDirection.Input
            sqlPrimaTotal.Value = pObjBECertificado.PrimaTotal

            Dim sqlPrimaCobrar As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@primacobrar", SqlDbType.[Decimal])
            sqlPrimaCobrar.Direction = ParameterDirection.Input
            sqlPrimaCobrar.Value = pObjBECertificado.PrimaCobrar

            Dim sqlNumCuenta As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@numerocuenta", SqlDbType.VarChar, 20)
            sqlNumCuenta.Direction = ParameterDirection.Input
            sqlNumCuenta.Value = pObjBECertificado.NumeroCuenta

            Dim sqlVencimiento As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@vencimiento", SqlDbType.SmallDateTime)
            sqlVencimiento.Direction = ParameterDirection.Input
            If pObjBECertificado.Vencimiento = DateTime.Parse("1900-01-01") Then
                sqlVencimiento.Value = DBNull.Value
            Else
                sqlVencimiento.Value = pObjBECertificado.Vencimiento
            End If

            Dim sqlVigencia As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@vigencia", SqlDbType.SmallDateTime)
            sqlVigencia.Direction = ParameterDirection.Input
            If pObjBECertificado.Vigencia = DateTime.Parse("1900-01-01") Then
                sqlVigencia.Value = DBNull.Value
            Else
                sqlVigencia.Value = pObjBECertificado.Vigencia
            End If

            Dim sqlPuntos As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@puntos", SqlDbType.Int)
            sqlPuntos.Direction = ParameterDirection.Input
            If pObjBECertificado.Puntos = -1 Then
                sqlPuntos.Value = DBNull.Value
            Else
                sqlPuntos.Value = pObjBECertificado.Puntos
            End If

            Dim sqlMotivoAnulacion As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@idmotivoanulacion", SqlDbType.Int)
            sqlMotivoAnulacion.Direction = ParameterDirection.Input
            If pObjBECertificado.IdMotivoAnulacion = -1 Then
                sqlMotivoAnulacion.Value = DBNull.Value
            Else
                sqlMotivoAnulacion.Value = pObjBECertificado.IdMotivoAnulacion
            End If

            Dim sqlConsistente As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@consistente", SqlDbType.Bit)
            sqlConsistente.Direction = ParameterDirection.Input
            sqlConsistente.Value = pObjBECertificado.Consistente

            Dim sqlPuntosAnual As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@puntosanual", SqlDbType.Int)
            sqlPuntosAnual.Direction = ParameterDirection.Input
            sqlPuntosAnual.Value = pObjBECertificado.PuntosAnual

            Dim sqlIdCiudad As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@idciudad", SqlDbType.Int)
            sqlIdCiudad.Direction = ParameterDirection.Input
            sqlIdCiudad.Value = pObjBECertificado.IdCiudad

            Dim sqlDireccion As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@direccion", SqlDbType.VarChar, 250)
            sqlDireccion.Direction = ParameterDirection.Input
            sqlDireccion.Value = pObjBECertificado.Direccion

            Dim sqlTelefono As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@telefono", SqlDbType.VarChar, 15)
            sqlTelefono.Direction = ParameterDirection.Input
            sqlTelefono.Value = pObjBECertificado.Telefono

            Dim sqlNombre1 As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@nombre1", SqlDbType.VarChar, 60)
            sqlNombre1.Direction = ParameterDirection.Input
            sqlNombre1.Value = pObjBECertificado.Nombre1

            Dim sqlNombre2 As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@nombre2", SqlDbType.VarChar, 60)
            sqlNombre2.Direction = ParameterDirection.Input
            If pObjBECertificado.Nombre2 = [String].Empty Then
                sqlNombre2.Value = DBNull.Value
            Else
                sqlNombre2.Value = pObjBECertificado.Nombre2
            End If

            Dim sqlApellido1 As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@apellido1", SqlDbType.VarChar, 60)
            sqlApellido1.Direction = ParameterDirection.Input
            sqlApellido1.Value = pObjBECertificado.Apellido1

            Dim sqlApellido2 As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@apellido2", SqlDbType.VarChar, 60)
            sqlApellido2.Direction = ParameterDirection.Input
            If pObjBECertificado.Apellido2 = [String].Empty Then
                sqlApellido2.Value = DBNull.Value
            Else
                sqlApellido2.Value = pObjBECertificado.Apellido2
            End If

            Dim sqlCcCliente As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@cccliente", SqlDbType.VarChar, 20)
            sqlCcCliente.Direction = ParameterDirection.Input
            sqlCcCliente.Value = pObjBECertificado.Cccliente

            Dim sqlIdTipoDocumento As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@idtipodocumento", SqlDbType.VarChar, 3)
            sqlIdTipoDocumento.Direction = ParameterDirection.Input
            sqlIdTipoDocumento.Value = pObjBECertificado.IdTipoDocumento

            Dim sqlDigitacion As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@digitacion", SqlDbType.SmallDateTime)
            sqlDigitacion.Direction = ParameterDirection.Input
            sqlDigitacion.Value = pObjBECertificado.Digitacion

            Dim sqlIncentivo As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@incentivo", SqlDbType.[Decimal])
            sqlIncentivo.Direction = ParameterDirection.Input
            If pObjBECertificado.Incentivo = -1 Then
                sqlIncentivo.Value = DBNull.Value
            Else
                sqlIncentivo.Value = pObjBECertificado.Incentivo
            End If

            Dim sqlSaldoIncentivo As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@saldoincentivo", SqlDbType.[Decimal])
            sqlSaldoIncentivo.Direction = ParameterDirection.Input
            If pObjBECertificado.SaldoIncentivo = -1 Then
                sqlSaldoIncentivo.Value = DBNull.Value
            Else
                sqlSaldoIncentivo.Value = pObjBECertificado.SaldoIncentivo
            End If

            Dim sqlUsuarioCreacion As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@usuariocreacion", SqlDbType.VarChar, 50)
            sqlUsuarioCreacion.Direction = ParameterDirection.Input
            sqlUsuarioCreacion.Value = pObjBECertificado.UsuarioCreacion

            Dim sqlVenta As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@venta", SqlDbType.SmallDateTime)
            sqlVenta.Direction = ParameterDirection.Input
            sqlVenta.Value = pObjBECertificado.Venta

            Dim sqlFinVigencia As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@finVigencia", SqlDbType.SmallDateTime)
            sqlFinVigencia.Direction = ParameterDirection.Input
            If pObjBECertificado.FinVigencia = DateTime.Parse("1900-01-01") Then
                sqlFinVigencia.Value = DBNull.Value
            Else
                sqlFinVigencia.Value = pObjBECertificado.FinVigencia
            End If

            Dim sqlIdMonedaPrima As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@idMonedaPrima", SqlDbType.VarChar, 5)
            sqlIdMonedaPrima.Direction = ParameterDirection.Input
            sqlIdMonedaPrima.Value = pObjBECertificado.IdMonedaPrima

            Dim sqlIdMonedaCobro As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@idMonedaCobro", SqlDbType.VarChar, 5)
            sqlIdMonedaCobro.Direction = ParameterDirection.Input
            sqlIdMonedaCobro.Value = pObjBECertificado.IdMonedaCobro

            Dim sqlAfiliadoHasta As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@afiliadoHasta", SqlDbType.SmallDateTime)
            sqlAfiliadoHasta.Direction = ParameterDirection.Input
            If pObjBECertificado.AfiliadoHasta = DateTime.Parse("1900-01-01") Then
                sqlAfiliadoHasta.Value = DBNull.Value
            Else
                sqlAfiliadoHasta.Value = pObjBECertificado.AfiliadoHasta
            End If

            Dim sqlIdCiclo As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@idCiclo", SqlDbType.Int)
            sqlIdCiclo.Direction = ParameterDirection.Input
            If pObjBECertificado.IdCiclo = -1 Then
                sqlIdCiclo.Value = DBNull.Value
            Else
                sqlIdCiclo.Value = pObjBECertificado.IdCiclo
            End If

            Dim sqlDiaGenera As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@diaGenera", SqlDbType.SmallInt)
            sqlDiaGenera.Direction = ParameterDirection.Input
            If pObjBECertificado.DiaGenera = -1 Then
                sqlDiaGenera.Value = DBNull.Value
            Else
                sqlDiaGenera.Value = pObjBECertificado.DiaGenera
            End If

            Dim sqlMesGenera As SqlParameter = sqlCmdInsCertificado.Parameters.Add("@mesGenera", SqlDbType.SmallInt)
            sqlMesGenera.Direction = ParameterDirection.Input
            If pObjBECertificado.MesGenera = -1 Then
                sqlMesGenera.Value = DBNull.Value
            Else
                sqlMesGenera.Value = pObjBECertificado.MesGenera
            End If

            Dim sqlIdCertificadoCrossSelling = sqlCmdInsCertificado.Parameters.Add("@idCertificadoCrossSelling", SqlDbType.VarChar, 25)
            sqlIdCertificadoCrossSelling.Direction = ParameterDirection.Input
            sqlIdCertificadoCrossSelling.Value = IIf(String.IsNullOrEmpty(pObjBECertificado.IdCertificadoCrossSelling), DBNull.Value, pObjBECertificado.IdCertificadoCrossSelling)

            filasAfectadas = sqlCmdInsCertificado.ExecuteNonQuery()

        End Using
        If filasAfectadas <= 0 Then
            Return -1
        Else
            Return filasAfectadas
        End If
    End Function

End Class
