﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports Microsoft.Practices.EnterpriseLibrary.Data

Public Class ProductoInfoProductoDA

    Dim _db As Database = Nothing

    Public Sub New()
        _db = Conexion.Instancia.Database()
    End Sub

    Function Agregar(ByVal Objeto As ProductoInfoProductoBE, Optional ByVal Transac As DbTransaction = Nothing) As String
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_ProductoInfoProducto_Agregar", Nothing, Objeto.IdProducto, Objeto.IdInfoProducto, Objeto.Estado, Objeto.UsuarioReg, Objeto.FechaReg, Objeto.UsuarioMod, Objeto.FechaMod)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
            Throw ex
        End Try
        Return resultado

    End Function

    Function Modificar(ByVal Objeto As ProductoInfoProductoBE, Optional ByVal Transac As DbTransaction = Nothing) As Integer
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_ProductoInfoProducto_Modificar", Objeto.IdProductoInfoProducto, Objeto.IdInfoProducto, Objeto.Estado, Objeto.UsuarioMod, Objeto.FechaMod)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Eliminar(ByVal Objeto As ProductoInfoProductoBE, Optional ByVal Transac As DbTransaction = Nothing) As Integer
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_ProductoInfoProducto_Eliminar", Objeto.IdProductoInfoProducto, Objeto.Estado, Objeto.UsuarioMod, Objeto.FechaMod)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function


    Function Eliminar_IdProducto(ByVal Objeto As ProductoInfoProductoBE, Optional ByVal Transac As DbTransaction = Nothing) As Integer
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_ProductoInfoProducto_Eliminar_IdProducto", Objeto.IdProducto)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function


    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of ProductoInfoProductoBE)
        Dim Lista As New List(Of ProductoInfoProductoBE)

        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_ProductoInfoProducto_IdOficina")
                While dr.Read()
                    Lista.Add(Populate.ProductoInfoProducto_Lista(dr))
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_ProductoInfoProducto_IdOficina")
                While dr.Read()
                    Lista.Add(Populate.ProductoInfoProducto_Lista(dr))
                End While
            End Using
        End If
        Return Lista
    End Function

    Function Listar_Id(ByVal Objeto As ProductoInfoProductoBE, Optional ByVal Transaction As DbTransaction = Nothing) As ProductoInfoProductoBE
        Dim _ProductoInfoProductoBE As New ProductoInfoProductoBE

        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_ProductoInfoProducto_IdOficina", Objeto.IdProductoInfoProducto)
                While dr.Read()
                    _ProductoInfoProductoBE = Populate.ProductoInfoProducto_Lista(dr)
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_ProductoInfoProducto_IdOficina", Objeto.IdProductoInfoProducto)
                While dr.Read()
                    _ProductoInfoProductoBE = Populate.ProductoInfoProducto_Lista(dr)
                End While
            End Using
        End If
        Return _ProductoInfoProductoBE
    End Function


    Function Listar_IdProducto_Check(ByVal Objeto As ProductoInfoProductoBE, Optional ByVal Transaction As DbTransaction = Nothing) As List(Of InfoProductoBE)
        Dim _List As New List(Of InfoProductoBE)
        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_ProductoInfoProducto_IdProducto_Check", Objeto.IdProducto)
                While dr.Read()
                    _List.Add(Populate.ProductoInfoProducto_Check_Lista(dr))
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_ProductoInfoProducto_IdProducto_Check", Objeto.IdProducto)
                While dr.Read()
                    _List.Add(Populate.ProductoInfoProducto_Check_Lista(dr))
                End While
            End Using
        End If
        Return _List
    End Function


    Function Listar_IdProducto_X_Check(ByVal Objeto As ProductoInfoProductoBE, Optional ByVal Transaction As DbTransaction = Nothing) As List(Of InfoProductoBE)
        Dim _List As New List(Of InfoProductoBE)
        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_ProductoInfoProducto_IdProducto_X_Check", Objeto.IdProducto)
                While dr.Read()
                    _List.Add(Populate.ProductoInfoProducto_Check_Lista(dr))
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_ProductoInfoProducto_IdProducto_X_Check", Objeto.IdProducto)
                While dr.Read()
                    _List.Add(Populate.ProductoInfoProducto_Check_Lista(dr))
                End While
            End Using
        End If
        Return _List
    End Function

    Function Listar_IdProducto(ByVal Objeto As ProductoInfoProductoBE, Optional ByVal Transaction As DbTransaction = Nothing) As List(Of InfoProductoBE)
        Dim _List As New List(Of InfoProductoBE)
        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_ProductoInfoProducto_IdProducto", Objeto.IdProducto)
                While dr.Read()
                    _List.Add(Populate.ProductoInfoProducto_Check_Lista(dr))
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_ProductoInfoProducto_IdProducto", Objeto.IdProducto)
                While dr.Read()
                    _List.Add(Populate.ProductoInfoProducto_Check_Lista(dr))
                End While
            End Using
        End If
        Return _List
    End Function

   

End Class
