﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports Microsoft.Practices.EnterpriseLibrary.Data


Public Class ProductoEntidadValidacionDA

    Dim _db As Database = Nothing

    Public Sub New()
        _db = Conexion.Instancia.Database()
    End Sub


    Function AgregarT(ByVal List As List(Of ProductoEntidadValidacionBE)) As String
        Dim resultado As String = "0"
        Try
            Using Connection As DbConnection = _db.CreateConnection()
                Connection.Open()
                Dim Transaction As DbTransaction = Connection.BeginTransaction
                If List.Count > 0 Then
                    EliminarProductoEntidad(List.Item(0), Transaction)
                End If
                For i As Integer = 0 To List.Count - 1
                    Agregar(List.Item(i), Transaction)
                Next

                Try
                    Transaction.Commit()
                Catch ex As Exception
                    Transaction.Rollback()
                    Throw ex
                End Try
            End Using
        Catch ex As Exception
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Agregar(ByVal Objeto As ProductoEntidadValidacionBE, Optional ByVal Transac As DbTransaction = Nothing) As String
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_ProductoEntidadValidacion_Agregar", Nothing, Objeto.IdProducto, Objeto.IdEntidad, Objeto.IdEntidadDetalle, Objeto.IdTipoValidacion, Objeto.ValorMinimo, Objeto.ValorMaximo, Objeto.Obs, Objeto.Estado, Objeto.UsuarioReg, Objeto.FechaReg, Objeto.UsuarioMod, Objeto.FechaMod)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Modificar(ByVal Objeto As ProductoEntidadValidacionBE, Optional ByVal Transac As DbTransaction = Nothing) As Integer
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_ProductoEntidadValidacion_Modificar", Objeto.IdProductoEntidadValidacion, Objeto.IdProducto, Objeto.IdEntidad, Objeto.IdEntidadDetalle, Objeto.IdTipoValidacion, Objeto.Obs, Objeto.Estado, Objeto.UsuarioMod, Objeto.FechaMod)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Eliminar(ByVal Objeto As ProductoEntidadValidacionBE, Optional ByVal Transac As DbTransaction = Nothing) As Integer
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_ProductoEntidadValidacion_Eliminar", Objeto.IdProductoEntidadValidacion, Objeto.Estado, Objeto.UsuarioMod, Objeto.FechaMod)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function EliminarProductoEntidad(ByVal Objeto As ProductoEntidadValidacionBE, Optional ByVal Transac As DbTransaction = Nothing) As Integer
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_ProductoEntidadValidacion_Eliminar_ProductoEntidad", Objeto.IdProducto, Objeto.IdEntidad)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of ProductoEntidadValidacionBE)
        Dim Lista As New List(Of ProductoEntidadValidacionBE)

        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_ProductoEntidadValidacion_Todos")
                While dr.Read()
                    Lista.Add(Populate.ProductoEntidadValidacion_Lista(dr))
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_ProductoEntidadValidacion_Todos")
                While dr.Read()
                    Lista.Add(Populate.ProductoEntidadValidacion_Lista(dr))
                End While
            End Using
        End If
        Return Lista
    End Function

    Function Listar_Id(ByVal Objeto As ProductoEntidadValidacionBE, Optional ByVal Transaction As DbTransaction = Nothing) As ProductoEntidadValidacionBE
        Dim _ProductoEntidadValidacionBE As New ProductoEntidadValidacionBE

        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_ProductoEntidadValidacion_Id", Objeto.IdProductoEntidadValidacion)
                While dr.Read()
                    _ProductoEntidadValidacionBE = Populate.ProductoEntidadValidacion_Lista(dr)
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_ProductoEntidadValidacion_Id", Objeto.IdProductoEntidadValidacion)
                While dr.Read()
                    _ProductoEntidadValidacionBE = Populate.ProductoEntidadValidacion_Lista(dr)
                End While
            End Using
        End If
        Return _ProductoEntidadValidacionBE
    End Function


    Function Listar_IdProductoIdEntidad(ByVal Objeto As ProductoEntidadValidacionBE, Optional ByVal Transaction As DbTransaction = Nothing) As List(Of EntidadDetalleBE)
        Dim _List As New List(Of EntidadDetalleBE)

        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_ProductoEntidadValidacion_Listar_IdProductoIdEntidad", Objeto.IdProducto, Objeto.IdEntidad)
                While dr.Read()
                    _List.Add(Populate.ProductoEntidadValidacion_ProductoEntidad_Lista(dr))
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_ProductoEntidadValidacion_Listar_IdProductoIdEntidad", Objeto.IdProducto, Objeto.IdEntidad)
                While dr.Read()
                    _List.Add(Populate.ProductoEntidadValidacion_ProductoEntidad_Lista(dr))
                End While
            End Using
        End If
        Return _List
    End Function


End Class
