﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports Microsoft.Practices.EnterpriseLibrary.Data

Public Class CertificadoInfoProductoDA

    Dim _db As Database = Nothing

    Public Sub New()
        _db = Conexion.Instancia.Database()
    End Sub

    Function Agregar(ByVal Objeto As CertificadoInfoProductoBE, Optional ByVal Transac As DbTransaction = Nothing) As String
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_CertificadoInfoProducto_Agregar", Objeto.IdProducto, Objeto.IdInfoProducto, Objeto.IdCertificado, Objeto.Estado, Objeto.UsuarioReg, Objeto.FechaReg, Objeto.UsuarioMod, Objeto.FechaMod)
            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Modificar(ByVal Objeto As CertificadoInfoProductoBE, Optional ByVal Transac As DbTransaction = Nothing) As Integer
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_CertificadoInfoProducto_Modificar", Objeto.IdCertificadoInfoProducto, Objeto.IdProducto, Objeto.IdInfoProducto, Objeto.IdCertificado, Objeto.Estado, Objeto.UsuarioMod, Objeto.FechaMod)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Eliminar(ByVal Objeto As CertificadoInfoProductoBE, Optional ByVal Transac As DbTransaction = Nothing) As Integer
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_CertificadoInfoProducto_Eliminar", Objeto.IdCertificadoInfoProducto, Objeto.Estado, Objeto.UsuarioMod, Objeto.FechaMod)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of CertificadoInfoProductoBE)
        Dim Lista As New List(Of CertificadoInfoProductoBE)

        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_CertificadoInfoProducto_Todos")
                While dr.Read()
                    Lista.Add(Populate.CertificadoInfoProducto_Lista(dr))
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_CertificadoInfoProducto_Todos")
                While dr.Read()
                    Lista.Add(Populate.CertificadoInfoProducto_Lista(dr))
                End While
            End Using
        End If
        Return Lista
    End Function

    Function Listar_Id(ByVal Objeto As CertificadoInfoProductoBE, Optional ByVal Transaction As DbTransaction = Nothing) As CertificadoInfoProductoBE
        Dim _CertificadoInfoProductoBE As New CertificadoInfoProductoBE

        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_CertificadoInfoProducto_Id", Objeto.IdCertificadoInfoProducto)
                While dr.Read()
                    _CertificadoInfoProductoBE = Populate.CertificadoInfoProducto_Lista(dr)
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_CertificadoInfoProducto_Id", Objeto.IdCertificadoInfoProducto)
                While dr.Read()
                    _CertificadoInfoProductoBE = Populate.CertificadoInfoProducto_Lista(dr)
                End While
            End Using
        End If
        Return _CertificadoInfoProductoBE
    End Function

End Class
