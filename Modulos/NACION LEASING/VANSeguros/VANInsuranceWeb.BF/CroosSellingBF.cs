﻿using System;
using System.Collections.Generic;
using System.Text;
using VAN.InsuranceWeb.BE;
using VAN.InsuranceWeb.DL;
using VAN.InsuranceWeb.Comun;
using System.Data;

namespace VAN.InsuranceWeb.BF
{
    public class CroosSellingBF
    {

        private object[][] GetStructure(IList<BECrossSelling> lsConfig, ref int maxIndex)
        {
            string tActual = "";
            int countTipoDistinct = 1;
            int index = 0;

            //Obtengo los distintos tipos de productos que tengo para no redimensionar luego el array
            int[] aAux = new int[1];
            foreach (BECrossSelling olsBECr in lsConfig)
            {
                //Solo accedo la primera vez
                if (tActual == "")
                {
                    tActual = olsBECr.TipoProducto;
                    maxIndex = 1;
                }

                if (tActual != olsBECr.TipoProducto)
                {
                    //Guardo la longitud del arreglo mas largo
                    if (maxIndex < index)
                    {
                        maxIndex = index;
                    }
                    countTipoDistinct = countTipoDistinct + 1;
                    Array.Resize(ref aAux, countTipoDistinct);
                    aAux[countTipoDistinct - 2] = index;
                    index = 0;
                    tActual = olsBECr.TipoProducto;
                }

                index = index + 1;
            }
            //Vuelvo a chekear si la ultima iteracion tiene la longitud del arreglo mas largo
            if (maxIndex < index)
            {
                maxIndex = index;
            }
            aAux[countTipoDistinct - 1] = index;


            //Inicializo el tamaño de cada uno de los sub-arreglos
            object[][] structure = new object[aAux.Length][];
            for (int n = 0; n < aAux.Length; n++)
            {
                structure[n] = new object[aAux[n]];
            }

            tActual = "";
            foreach (BECrossSelling olsBECr in lsConfig)
            {
                //Solo accedo la primera vez
                if (tActual == "")
                {
                    countTipoDistinct = 0;
                    index = 0;
                    tActual = olsBECr.TipoProducto;
                }

                if (tActual != olsBECr.TipoProducto)
                {
                    index = 0;
                    countTipoDistinct = countTipoDistinct + 1;
                    tActual = olsBECr.TipoProducto;
                }
                structure[countTipoDistinct][index] = olsBECr;
                index = index + 1;
            }
            return structure;
        }


        private string AddHeaderTable(string nameColumn)
        {
            return "<th width='170px'><div class='border-radius-object'>" + nameColumn + "</div></th>";
        }

        private string AddRowTable(BECrossSelling oBEcr, int index)
        {
            string celda="";
            string informacionExtra = Constants.WILDCARD_CHARACTER_RETURN_HTML + "<strong>Información Extra</strong>" + Constants.WILDCARD_CHARACTER_RETURN_HTML;
            bool hasExtraInformation = false;
            string descriPlan = Constants.WILDCARD_CHARACTER_RETURN_HTML + "<strong>Planes</strong>" + Constants.WILDCARD_CHARACTER_RETURN_HTML;
            string medioPago = Constants.WILDCARD_CHARACTER_RETURN_HTML + "<strong>Medios de Pago</strong>" + Constants.WILDCARD_CHARACTER_RETURN_HTML;
            if (oBEcr != null)
            {
                foreach (BECrossSellingInfoAdicional item in oBEcr.Informacion)
                {
                    if (item.Categoria == Constants.CATEGORIA_PLAN)
                    {
                        descriPlan = descriPlan + item.Descripcion + Constants.WILDCARD_CHARACTER_RETURN_HTML;
                    }
                    if (item.Categoria == Constants.CATEGORIA_MEDIO_PAGO)
                    {
                        medioPago = medioPago + item.Descripcion + Constants.WILDCARD_CHARACTER_RETURN_HTML;
                    }
                    if (item.Categoria == Constants.CATEGORIA_VALIDACION)
                    {
                        informacionExtra = informacionExtra + item.Descripcion + Constants.WILDCARD_CHARACTER_RETURN_HTML;
                        hasExtraInformation = true;
                    }
                }
                
                //Reemplazo lo que corresponde a la tabla de cheks.
                string InfoProducto = (hasExtraInformation ? informacionExtra : string.Empty) + descriPlan + medioPago;
                if (InfoProducto.Length >= 4 && InfoProducto.Substring(0, 4) == Constants.WILDCARD_CHARACTER_RETURN_HTML)
                {
                    InfoProducto = InfoProducto.Substring(4);
                }
                //celda = "<td class='csTd'>" + Constants.SEC_PRODUCTO.Replace(Constants.TAG_CS_INDEX, index.ToString()).Replace(Constants.TAG_CS_NOMBRE_PRODUCTO, oBEcr.Nombre).Replace(Constants.TAG_CS_TIPO_PRODUCTO, oBEcr.TipoProducto.Trim()).Replace(Constants.TAG_CS_ICONS, Constants.PATH_ICONS.Trim()).Replace(Constants.TAG_CS_DESCRI_PLAN, (hasExtraInformation ? informacionExtra : string.Empty) + descriPlan + medioPago).Replace(Constants.TAG_CS_NOMBRE_PRODUCTO, oBEcr.NombreTipoProducto.Trim()).Replace(Constants.TAG_CS_NOMBRE_PRODUCTO_PARAM, oBEcr.Nombre.Trim().Replace(" ", "%20")) + "</td>";
                celda = "<td class='csTd'>" + Constants.SEC_PRODUCTO.Replace(Constants.TAG_CS_INDEX, index.ToString()).Replace(Constants.TAG_CS_NOMBRE_PRODUCTO, oBEcr.Nombre).Replace(Constants.TAG_CS_TIPO_PRODUCTO, oBEcr.TipoProducto.Trim()).Replace(Constants.TAG_CS_ICONS, Constants.PATH_ICONS.Trim()).Replace(Constants.TAG_CS_DESCRI_PLAN, InfoProducto).Replace(Constants.TAG_CS_NOMBRE_PRODUCTO, oBEcr.NombreTipoProducto.Trim()).Replace(Constants.TAG_CS_NOMBRE_PRODUCTO_PARAM, oBEcr.Nombre.Trim().Replace(" ", "%20")) + "</td>";


                if (oBEcr.VentaHabilitada)
                {
                    celda = celda.Replace(Constants.TAG_CS_VENTA, Constants.TAG_CS_HTML_IMAGE_VENTA);
                    celda = celda.Replace(Constants.TAG_CS_ICO_SALE, Constants.ICO_SALE);//Icono de venta de producto
                    celda = celda.Replace(Constants.TAG_CS_MEDIOPAGO, Constants.TAG_CS_HTML_IMAGE_MEDIO_PAGO);
                    //Seteo el icono para el medio de pago si es que esta o no habilitado
                    if (oBEcr.TieneMedioPagoHabilitado)
                    {
                        celda = celda.Replace(Constants.TAG_CS_ICO_MEDIO_PAGO, Constants.ICO_MEDIO_PAGO_ENABLED);
                    }
                    else
                    {
                        celda = celda.Replace(Constants.TAG_CS_ICO_MEDIO_PAGO, Constants.ICO_MEDIO_PAGO_DISABLED);
                    }
                }
                else
                {
                    celda = celda.Replace(Constants.TAG_CS_VENTA, Constants.WILDCARD_SPACE);
                    celda = celda.Replace(Constants.TAG_CS_MEDIOPAGO, Constants.WILDCARD_SPACE);
                }


                //Agrego el icono de interes en caso de que haya solicitado contacto
                if (oBEcr.TieneSolicitudContacto)
                {
                    celda = celda.Replace(Constants.TAG_CS_INTERES, Constants.TAG_CS_HTML_IMAGE_INTEREST); 
                }
                else
                {
                    celda = celda.Replace(Constants.TAG_CS_INTERES, Constants.WILDCARD_SPACE);
                }

                //Setea color para identificar si el cliente ya tiene un certificado asociado con ese producto (ya sea uno activo o una preventa)
                if (oBEcr.TieneOtroCertificado)
                {
                    celda = celda.Replace(Constants.TAG_CS_PRODUCT_BUTTON_BACKGOUND_COLOR, Constants.STYPE_BACKGROUND_COLOR_TIENE_CERTIFICADO);
                }
                else
                {
                    celda = celda.Replace(Constants.TAG_CS_PRODUCT_BUTTON_BACKGOUND_COLOR, Constants.STYLE_DEFAULT_BACKGROUND_COLOR);
                }

                celda = celda.Replace(Constants.TAG_CS_IDPRODUCTO, oBEcr.IDProducto.ToString());
            }
            else
            {
                //Es una celda vacia
                celda = "<td class='csTd'>&nbsp;</td>";
            }
            return celda;
        }

        public string Seleccionar(BECrossSelling oBECr, bool venta=true)
        {
            //Busco la configuracion de la base
            DLNTCrossSelling oDLNT = new DLNTCrossSelling();
            IList<BECrossSelling> lsConfig = oDLNT.BuscarConfigCrossSelling(oBECr);

            string tableCrossSelling=""; //Salida final de la tabla
            string rowTableCrossSelling = ""; //Mantiene las filas que se van generando para la tabla
            string titleHeaderTable = ""; // Solo contiene los nombres de tipos de productos de la cabecera
            //Tabla general contenedora
            string headerTable = @"<table id='tProductos' width='100%' border='0' cellspacing='0' height='200px'>
                                    <tr>
                                       <th width='5'></th>
                                    <tr>";
            //Espacio de separacion de la linea de titulos con el promer producto
            string lineTitleData =@"<tr>
                                        <td class='csTd' colspan='" + Constants.TAG_CS_COUNT_TIPO_PROD + @"'>&nbsp;</td>
                                    </tr>";
            //Fila final de la tabla
            string footerTable = @"<tr style='height:100px;'>
                                            <td class='csTd' colspan='" + Constants.TAG_CS_COUNT_TIPO_PROD + @"'>&nbsp;</td>
                                        </tr>
                                    </table>";
            //footerTable = "";
            int countTipoDistinct = 0;
            string tActual = "";
            int cantRows = 0;
            object[][] structureTable = GetStructure(lsConfig, ref cantRows);
            int cant=1;
            string row = "";
            string colAjuste = "";
            for (int t = 0; t <= structureTable.Length; t++)
            {
                if (t == structureTable.Length)
                {
                    titleHeaderTable = titleHeaderTable + "<th width='100%'></th>";
                }
                else
                {
                    tActual = ((BECrossSelling)structureTable[t][0]).NombreTipoProducto;
                    titleHeaderTable = titleHeaderTable + AddHeaderTable(tActual);
                }
            }
             

            //Genero cada una de las filas que va a tener la tabla de Cross Selling
            for (int r = 0; r <= cantRows; r++)
            {
                row = "";
                for (int i = 0; i < structureTable.Length; i++)
                {
                    if (structureTable[i].Length > r)
                    {
                        //Genero la celda con la configuracion del producto
                        BECrossSelling oBEcr = (BECrossSelling)structureTable[i][r];
                        row = row + AddRowTable(oBEcr, cant);
                    }
                    else
                    {
                        //En caso de que no tiene producto en esa celda tengo que generarla igual pero vacia
                        row = row + AddRowTable(null, cant);
                    }
                    cant = cant + 1;
                }

                colAjuste = "<td"+ Constants.STYLE_AUTOAJUSTE + ">&nbsp;</td>";
                row = row + colAjuste;
                rowTableCrossSelling = rowTableCrossSelling + "<tr>" + row + "</tr>";
            }
            
            tableCrossSelling = headerTable.Replace(Constants.TAG_CS_COUNT_TIPO_PROD, (structureTable.Length + 1).ToString()) + titleHeaderTable + lineTitleData.Replace(Constants.TAG_CS_COUNT_TIPO_PROD, (structureTable.Length + 1).ToString()) + rowTableCrossSelling + footerTable.Replace(Constants.TAG_CS_COUNT_TIPO_PROD, (structureTable.Length + 1).ToString());
            tableCrossSelling = tableCrossSelling.Replace(Constants.TAG_CS_ICONS, venta ? Constants.PATH_ICONS_TOOLTIPS_SALE : Constants.PATH_ICONS).Replace(Constants.TAG_CS_COUNT_TIPO_PROD, countTipoDistinct.ToString());
            tableCrossSelling = tableCrossSelling.Replace(Constants.TAG_CS_ICONS_TOOLTIPS, venta ? Constants.PATH_ICONS_TOOLTIPS_SALE : Constants.PATH_ICONS);
            return tableCrossSelling;
        }

    }
}
