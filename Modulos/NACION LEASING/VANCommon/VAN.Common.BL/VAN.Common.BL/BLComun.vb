﻿Imports System
Imports System.Configuration


Public Class BLComun
    ' Methods
    Public Function IDEntidad() As Integer

        Return Convert.ToInt32(ConfigurationManager.AppSettings.Item("IDEntidad").ToString())
    End Function

    Public Function IDSistema() As Integer
        Return Convert.ToInt32(ConfigurationManager.AppSettings.Item("IDSistema").ToString())
    End Function

    'Public Function MethodLDAP() As Integer
    '    Dim val As Integer = 0
    '    Try
    '        val = Convert.ToInt32(ConfigurationManager.AppSettings.Item("LDAP").ToString())
    '    Catch ex As Exception
    '        val = 0
    '    End Try
    '    Return val

    'End Function

End Class

