﻿Imports VAN.Common.BE
Imports VAN.Common.DL

Public Class BLSecurity


    Public Function GetUser(ByVal pUsuario As String, ByVal IDSistema As Int32) As BEUsuario
        Dim oBLUsuario As New BLUsuario
        Dim oBEUsuario As New BEUsuario

        Try
            oBEUsuario = oBLUsuario.Seleccionar(pUsuario, IDSistema)
        Catch ex As Exception

        End Try
        Return oBEUsuario
    End Function

    Public Function ValidateAccesFrm(ByVal pIDUsuario As String, ByVal pUrl As String, Optional IDProducto As Int32 = 0) As Boolean
        Dim result As Boolean = False

        Dim oBE As New BEMenu
        Dim oDL As New DLNTMenu
        oBE.IDUsuario = pIDUsuario

        Dim strarry As String() = pUrl.Split("/")
        Dim lengh As Int16 = strarry.Length
        Dim sPage As String = strarry(lengh - 1)

        Dim list As IList = oDL.Seleccionar(oBE)

        For Each item As BEMenu In list
            If item.Url = sPage Then
                result = True
                Exit For
            End If
        Next
        Return result
        Return True
    End Function

    Public Function ConfigUserLogin(ByVal IDUsuario As String, ByVal IDSistema As Int32, ByVal IDEntidad As Int32, ByVal hostAddress As String) As IList
        'Parámetros de Seguridad de la Aplicación por Entidad-----------------------------------
        Dim oBLCSeguridad As New BLSeguridadConfig
        Dim pListParametro As IList = oBLCSeguridad.Seleccionar(IDEntidad)

        Dim oBLSegEvento As New BLSeguridadEvento

        'El cambio obligatorio de las contraseñas de acceso en el primer inicio de sesión.
        Dim pPrimerLogin As Boolean = False
        If CBool(oBLCSeguridad.Parametro("CONTRASE01", pListParametro)) Then
            'Consultar si es la primera vez que el usuario inicia sesión
            pPrimerLogin = oBLSegEvento.PrimerLogin(IDUSuario)
        End If

        If Not pPrimerLogin Then
            'Registro del Inicio de sesión--------------
            Dim oBESegEvento As New BESeguridadEvento
            oBESegEvento.IDSistema = IDSistema
            oBESegEvento.IDUsuario = IDUSuario
            oBESegEvento.IDTipoEvento = 1 'Login
            oBESegEvento.Host = hostAddress
            oBESegEvento.UsuarioCreacion = IDUSuario
            oBLSegEvento.Insertar(oBESegEvento)
        End If

        Return pListParametro
    End Function

    Private Sub RegistrarEvento(ByVal IDSistema As Int32, ByVal pUsuario As String, ByVal detalle As String, ByVal hostAddress As String, ByVal IDEvento As Int32)

        Dim oBESegEvento As New BESeguridadEvento
        Dim oBLSegEvento As New BLSeguridadEvento
        oBESegEvento.IDSistema = IDSistema
        oBESegEvento.IDUsuario = pUsuario
        oBESegEvento.IDTipoEvento = IDEvento
        oBESegEvento.Host = hostAddress
        oBESegEvento.Detalle = detalle
        oBESegEvento.UsuarioCreacion = pUsuario
        oBLSegEvento.Insertar(oBESegEvento)

    End Sub


    Public Sub RegistrarIntentoLogin(ByVal IDSistema As Int32, ByVal pUsuario As String, ByVal detalle As String, ByVal hostAddress As String)

        'Evento (4) Intento de Login
        RegistrarEvento(IDSistema, pUsuario, detalle, hostAddress, 4)
    End Sub

    Public Sub RegistrarLogout(ByVal IDSistema As Int32, ByVal pUsuario As String, ByVal hostAddress As String)

        'Evento (2) Logout
        RegistrarEvento(IDSistema, pUsuario, "Logout app", hostAddress, 2)
    End Sub

    Public Sub RegistrarCambioClave(ByVal IDSistema As Int32, ByVal pUsuario As String, ByVal hostAddress As String)

        'Evento (3) Cambio Clave
        RegistrarEvento(IDSistema, pUsuario, "Cambio Clave", hostAddress, 3)
    End Sub

    Public Sub RegistrarPrimerLogin(ByVal IDSistema As Int32, ByVal pUsuario As String, ByVal hostAddress As String)

        'Evento (1) Primer Login
        RegistrarEvento(IDSistema, pUsuario, "Primer Login", hostAddress, 1)
    End Sub

    Public Function ValidaIntentoLogin(ByVal NroIntento As Int32, Optional ByVal IDSistema As Int32 = 0) As Boolean
        Dim intentoDefault As Int32 = 3
        Dim result As Int32

        Select Case IDSistema
            Case Else
                result = intentoDefault
        End Select

        If NroIntento > result Then
            Return False
        End If
        Return True
    End Function
End Class
