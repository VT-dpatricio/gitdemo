﻿Imports VAN.Common.BE
Imports Microsoft.VisualBasic.CompilerServices
Imports System
Imports System.Collections
Imports System.DirectoryServices
Imports System.Runtime.CompilerServices


Public Class BLSeguridad
    ' Methods

    Private Sub loguear(ByVal evento As String, ByVal usr As String, ByVal err As String)
        Dim oBLComun As New BLComun
        Dim sistema As New BLLogSistema
        Dim pEntidad As New BELogSistema
        Try
            pEntidad.Detalle = usr
            pEntidad.Evento = evento
            pEntidad.MensajeError = err
            pEntidad.Compilado = "ITAU-PYG"
            pEntidad.IDSistema = oBLComun.IDSistema

            sistema.Insertar(pEntidad)
        Catch ex As Exception

        End Try

    End Sub

    Public Function LoginLDAP(ByVal username As String, ByVal pwd As String) As String
        Dim Respuesta As String = "LDAP No Configurado"

        'DirectoryEntry. Proporciona una conexión al Active Directory conteniendo los hijos del Active Directory.
        'DirectorySearcher. Realiza búsquedas dentro del Active Directory.
        Try
            Dim oBLCSeguridad As New BLSeguridadConfig
            Dim oBLComun As New BLComun
            Dim pListParametro As IList = oBLCSeguridad.Seleccionar(oBLComun.IDEntidad())
            Dim strPath As String = oBLCSeguridad.Parametro("DOMINIODIR", pListParametro)

            If (strPath.Trim.Length <> 0) Then
                Dim strDomain As String = oBLCSeguridad.Parametro("DOMINIONOM", pListParametro)
                Dim domainAndUsername As String = (strDomain & Convert.ToString("\")) & username

                'Si deseas listar todos los grupos de AD debes usar el siguiente FILTRO search.Filter = "(OU=Groups)" 
                'Creamos una entra al Active Directory que devolverá el árbol en la variable entry.
                Dim entry As New DirectoryEntry(strPath, domainAndUsername, pwd)
                'Realizamos una busqueda sobre la entrada anteriormente seleccionada.    
                Dim search As New DirectorySearcher(entry)
                'Filtramos el usuario del que queremos obtener los datos.
                search.Filter = "name=" + username
                'Y realizamos una busqueda de todos sus datos.
                Dim results As SearchResultCollection = search.FindAll()
                'El resultado de la busqueda se guarda en un colección SearchResultCollection. 
                'Recorriendo esta colección encontraremos sus propiedades.
                For Each resultados As SearchResult In results
                    'Creamos una variable para almacenar las propiedades, 
                    'que será una colección de propiedades, ResultPropertyCollection.
                    Dim colProperties As ResultPropertyCollection = resultados.Properties
                    'Y recorremos dicha colección, en el campo Key 
                    'obtendremos el nombre de la propiedad y en el campo value el valor de dicha propiedad.   
                    Dim ls As [String] = [String].Empty
                    'For Each key As String In colProperties.PropertyNames
                    '    'For Each value As Object In colProperties(key)
                    '    '    ls = ls + "" + key.ToString() + ": " + value + ""
                    '    'Next
                    'Next
                Next
                Respuesta = "" 'Login OK
            End If
        Catch ex As Exception
            Respuesta = ex.Message.ToString()
        End Try
        Return Respuesta

    End Function


    Public Function LoginLDAP2(ByVal username As String, ByVal pwd As String) As String
        Dim Respuesta As String = "LDAP No Configurado"
        loguear("LoginLDAP2", username, "Step 1")
        'DirectoryEntry. Proporciona una conexión al Active Directory conteniendo los hijos del Active Directory.
        'DirectorySearcher. Realiza búsquedas dentro del Active Directory.
        Try
            Dim oBLCSeguridad As New BLSeguridadConfig
            Dim oBLComun As New BLComun
            loguear("LoginLDAP2", username, "Step 2")
            Dim pListParametro As IList = oBLCSeguridad.Seleccionar(oBLComun.IDEntidad())
            loguear("LoginLDAP2", username, "Step 3")
            Dim strPath As String = oBLCSeguridad.Parametro("DOMINIODIR", pListParametro)
            If (strPath.Trim.Length <> 0) Then
                loguear("LoginLDAP2", username, "Step 4")
                Dim strDomain As String = oBLCSeguridad.Parametro("DOMINIONOM", pListParametro)
                Dim domainAndUsername As String = (strDomain & Convert.ToString("\")) & username
                loguear("LoginLDAP2", username, "domainAndUsername: " + domainAndUsername)
                'Si deseas listar todos los grupos de AD debes usar el siguiente FILTRO search.Filter = "(OU=Groups)" 
                'Creamos una entra al Active Directory que devolverá el árbol en la variable entry.
                loguear("LoginLDAP2", username, "Step 5")
                loguear("LoginLDAP2", username, "strPath: " + strPath)
                Dim entry As New DirectoryEntry(strPath, domainAndUsername, pwd)
                'Realizamos una busqueda sobre la entrada anteriormente seleccionada.    
                Dim search As New DirectorySearcher(entry)
                'Filtramos el usuario del que queremos obtener los datos.
                loguear("LoginLDAP2", username, "Step 6")
                loguear("LoginLDAP2", username, "name: " + "name=" + username)
                search.Filter = "name=" + username
                'Y realizamos una busqueda de todos sus datos.
                loguear("LoginLDAP2", username, "Step 7")
                Dim results As SearchResultCollection = search.FindAll()
                'El resultado de la busqueda se guarda en un colección SearchResultCollection. 
                'Recorriendo esta colección encontraremos sus propiedades.
                loguear("LoginLDAP2", username, "Step 8")
                For Each resultados As SearchResult In results
                    'Creamos una variable para almacenar las propiedades, 
                    'que será una colección de propiedades, ResultPropertyCollection.
                    loguear("LoginLDAP2", username, "Step 9")
                    Dim colProperties As ResultPropertyCollection = resultados.Properties
                    'Y recorremos dicha colección, en el campo Key 
                    'obtendremos el nombre de la propiedad y en el campo value el valor de dicha propiedad.   
                    Dim ls As [String] = [String].Empty
                    For Each key As String In colProperties.PropertyNames
                        loguear("LoginLDAP2", username, "Step 10 -  KEY: " + key)
                        For Each value As Object In colProperties(key)
                            loguear("LoginLDAP2", username, "Step 11 -  value: " + value)
                            ls = ls + "" + key.ToString() + ": " + value + ""
                            loguear("LoginLDAP2", username, "Step 12 -  key:value: " + ls)
                        Next
                    Next
                Next
                Respuesta = "" 'Login OK
            End If
        Catch ex As Exception
            Respuesta = ex.Message.ToString()
        End Try
        Return Respuesta

    End Function

    'Public Function LoginLDAP(ByVal username As String, ByVal pwd As String) As String

    '    Dim oBLComun2 As New BLComun
    '    If oBLComun2.MethodLDAP = 0 Then
    '        loguear("LoginLDAP", username, "Method LDAP")
    '        Try
    '            'LogSistema("Intento de logueo por LDAP", Usuario, "Fecha: " + DateTime.Now.ToString)
    '            loguear("LoginLDAP", username, "Step 1")
    '            Dim enumerator As IEnumerator
    '            Dim config As New BLSeguridadConfig
    '            Dim pListaP As IList = config.Seleccionar(New BLComun().IDEntidad)
    '            loguear("LoginLDAP", username, "Step 2")
    '            Dim path As String = config.Parametro("DOMINIODIR", pListaP)
    '            loguear("LoginLDAP", username, "Step 3")
    '            Dim str2 As String = (config.Parametro("DOMINIONOM", pListaP) & Convert.ToString("\") & username)
    '            loguear("LoginLDAP", username, "Step 4")
    '            loguear("LoginLDAP", username, "DirectoryEntry -> path: " + path + " - UserName: " + str2)
    '            Dim searchRoot As New DirectoryEntry(path, str2, pwd)
    '            loguear("LoginLDAP", username, "Step 5")
    '            loguear("LoginLDAP", username, "searchRoot: " + searchRoot.ToString)
    '            Dim searcher As New DirectorySearcher(searchRoot)
    '            loguear("LoginLDAP", username, "Step 6")
    '            loguear("LoginLDAP", username, "Filter: " + "name=" & username)
    '            searcher.Filter = ("name=" & username)
    '            loguear("LoginLDAP", username, "Step 7")
    '            Dim results As SearchResultCollection = searcher.FindAll
    '            loguear("LoginLDAP", username, "Step 8")
    '            loguear("LoginLDAP", username, "results: " + results.ToString)

    '            Try
    '                Dim idx1, idx2, idx3 As Integer

    '                idx1 = 0
    '                idx2 = 0
    '                idx3 = 0
    '                loguear("LoginLDAP", username, "Step 9")
    '                enumerator = results.GetEnumerator
    '                Do While enumerator.MoveNext
    '                    idx1 = idx1 + 1
    '                    loguear("LoginLDAP", username, "Step 10 While enumerator " + idx1.ToString)
    '                    Dim enumerator2 As IEnumerator
    '                    loguear("LoginLDAP", username, "Step 11")
    '                    loguear("LoginLDAP", username, enumerator.Current.ToString)
    '                    Dim current As SearchResult = DirectCast(enumerator.Current, SearchResult)
    '                    Dim properties As ResultPropertyCollection = current.Properties
    '                    Dim str5 As String = String.Empty
    '                    Try
    '                        enumerator2 = properties.PropertyNames.GetEnumerator
    '                        Do While enumerator2.MoveNext
    '                            idx2 = idx2 + 1
    '                            loguear("LoginLDAP", username, "Step 12 While enumerator2 " + idx2.ToString)
    '                            Dim enumerator3 As IEnumerator
    '                            Dim str6 As String = Conversions.ToString(enumerator2.Current)
    '                            Try
    '                                enumerator3 = properties.Item(str6).GetEnumerator
    '                                Do While enumerator3.MoveNext
    '                                    idx3 = idx3 + 1
    '                                    loguear("LoginLDAP", username, "Step 13 While enumerator2 " + idx3.ToString)
    '                                    loguear("LoginLDAP", username, enumerator3.Current.ToString)
    '                                    Dim objectValue As Object = RuntimeHelpers.GetObjectValue(enumerator3.Current)
    '                                    loguear("LoginLDAP", username, "Step 14 While enumerator2 " + idx3.ToString)
    '                                    loguear("LoginLDAP", username, "str5: " + str5.ToString + " - str6: " + str6.ToString)
    '                                    str5 = Conversions.ToString(Operators.AddObject(Operators.AddObject(((str5 & "") & str6.ToString & ": "), objectValue), ""))
    '                                    loguear("LoginLDAP", username, "Step 15 While enumerator2 " + idx3.ToString)
    '                                    loguear("LoginLDAP", username, "result str5: " + str5.ToString)
    '                                Loop
    '                                Continue Do
    '                            Finally
    '                                If TypeOf enumerator3 Is IDisposable Then
    '                                    TryCast(enumerator3, IDisposable).Dispose()
    '                                End If
    '                            End Try
    '                        Loop
    '                        Continue Do
    '                    Finally
    '                        If TypeOf enumerator2 Is IDisposable Then
    '                            TryCast(enumerator2, IDisposable).Dispose()
    '                        End If
    '                    End Try
    '                Loop
    '            Finally
    '                If TypeOf enumerator Is IDisposable Then
    '                    TryCast(enumerator, IDisposable).Dispose()
    '                End If
    '            End Try
    '        Catch exception1 As Exception
    '            ProjectData.SetProjectError(exception1)
    '            Dim exception As Exception = exception1
    '            Dim str As String = exception.Message.ToString
    '            ProjectData.ClearProjectError()
    '            Return str
    '        End Try
    '        Return ""
    '    Else
    '        loguear("LoginLDAP", username, "Method LDAP 2")
    '        Return LoginLDAP2(username, pwd)
    '    End If
    'End Function


    Public Function ValidarAccesoFrm(ByVal pIDUsuario As String, ByVal pUrl As String) As Boolean
        Dim valida As Boolean = False
        Try
            Dim oBLMenu As New BLMenu
            Dim Lista As ArrayList = oBLMenu.ListarMenu(pIDUsuario)
            For Each oBEMenu As BEMenu In Lista
                If oBEMenu.Url = pUrl Then
                    valida = True
                    Exit For
                End If
            Next
        Catch ex As Exception
            valida = False
        End Try
        Return valida
    End Function

    'Public Function ValidarAccesoFrm(ByVal pIDUsuario As String, ByVal pUrl As String) As Boolean
    '    Dim flag As Boolean = False
    '    Try
    '        Dim enumerator As IEnumerator
    '        Dim list As ArrayList = New BLMenu().ListarMenu(pIDUsuario)
    '        Try
    '            enumerator = list.GetEnumerator
    '            Do While enumerator.MoveNext
    '                Dim current As BEMenu = DirectCast(enumerator.Current, BEMenu)
    '                If (current.Url = pUrl) Then
    '                    Return True
    '                End If
    '            Loop
    '            Return flag
    '        Finally
    '            If TypeOf enumerator Is IDisposable Then
    '                TryCast(enumerator, IDisposable).Dispose()
    '            End If
    '        End Try
    '    Catch exception1 As Exception
    '        ProjectData.SetProjectError(exception1)
    '        Dim exception As Exception = exception1
    '        flag = False
    '        ProjectData.ClearProjectError()
    '    End Try
    '    Return flag
    'End Function

End Class

