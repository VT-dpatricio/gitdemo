﻿Imports System


Public Class BEUsuario
    Inherits BEBase

    ' Methods
    Public Property Apellido() As String
        Get
            Return Me._apellido
        End Get
        Set(ByVal value As String)
            Me._apellido = value
        End Set
    End Property

    Public Property Buscar() As String
        Get
            Return Me._buscar
        End Get
        Set(ByVal value As String)
            Me._buscar = value
        End Set
    End Property

    Public Property Clave() As String
        Get
            Return Me._clave
        End Get
        Set(ByVal value As String)
            Me._clave = value
        End Set
    End Property

    Public Property DescripcionCargo() As String
        Get
            Return Me._descripcionCargo
        End Get
        Set(ByVal value As String)
            Me._descripcionCargo = value
        End Set
    End Property

    Public Property DescripcionRol() As String
        Get
            Return Me._descripcionRol
        End Get
        Set(ByVal value As String)
            Me._descripcionRol = value
        End Set
    End Property

    Public Property DNI() As String
        Get
            Return Me._dNI
        End Get
        Set(ByVal value As String)
            Me._dNI = value
        End Set
    End Property

    Public Property Eliminado() As Boolean
        Get
            Return Me._eliminado
        End Get
        Set(ByVal value As Boolean)
            Me._eliminado = value
        End Set
    End Property

    Public Property Email() As String
        Get
            Return Me._email
        End Get
        Set(ByVal value As String)
            Me._email = value
        End Set
    End Property

    Public Property Estado() As Boolean
        Get
            Return Me._estado
        End Get
        Set(ByVal value As Boolean)
            Me._estado = value
        End Set
    End Property

    Public Property IDCargo() As Integer
        Get
            Return Me._iDCargo
        End Get
        Set(ByVal value As Integer)
            Me._iDCargo = value
        End Set
    End Property

    Public Property IDRol() As Integer
        Get
            Return Me._iDRol
        End Get
        Set(ByVal value As Integer)
            Me._iDRol = value
        End Set
    End Property

    Public Property IDUsuario() As String
        Get
            Return Me._iDUsuario
        End Get
        Set(ByVal value As String)
            Me._iDUsuario = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._nombre
        End Get
        Set(ByVal value As String)
            Me._nombre = value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return Me._usuario
        End Get
        Set(ByVal value As String)
            Me._usuario = value
        End Set
    End Property

    Public Property IDEntidad() As Integer
        Get
            Return Me._iDEntidad
        End Get
        Set(ByVal value As Integer)
            Me._iDEntidad = value
        End Set
    End Property

    Public Property IDInformador() As String
        Get
            Return Me._iDInformador
        End Get
        Set(ByVal value As String)
            Me._iDInformador = value
        End Set
    End Property

    Public Property Apellido1() As String
        Get
            Return Me._apellido1
        End Get
        Set(ByVal value As String)
            Me._apellido1 = value
        End Set
    End Property

    Public Property Apellido2() As String
        Get
            Return Me._apellido2
        End Get
        Set(ByVal value As String)
            Me._apellido2 = value
        End Set
    End Property

    Public Property Nombre1() As String
        Get
            Return Me._nombre1
        End Get
        Set(ByVal value As String)
            Me._nombre1 = value
        End Set
    End Property

    Public Property Nombre2() As String
        Get
            Return Me._nombre2
        End Get
        Set(ByVal value As String)
            Me._nombre2 = value
        End Set
    End Property

    Public Property Oficina() As String
        Get
            Return Me._oficina
        End Get
        Set(ByVal value As String)
            Me._oficina = value
        End Set
    End Property

    Public Property EstadoUsuario As Boolean
        Get
            Return Me._estadoUsuario
        End Get
        Set(ByVal value As Boolean)
            Me._estadoUsuario = value
        End Set
    End Property

    Public Property IDOficina() As Integer
        Get
            Return Me._iDOficina
        End Get
        Set(ByVal value As Integer)
            Me._iDOficina = value
        End Set
    End Property

    Public Property CodOficina() As Integer
        Get
            Return Me._codOficina
        End Get
        Set(ByVal value As Integer)
            Me._codOficina = value
        End Set
    End Property

    Public Property IDCanal() As Integer
        Get
            Return Me._iDCanal
        End Get
        Set(ByVal value As Integer)
            Me._iDCanal = value
        End Set
    End Property

    Public Property Acceso() As Boolean
        Get
            Return _acceso
        End Get
        Set(ByVal value As Boolean)
            _acceso = value
        End Set
    End Property

    Public Property LogoEntidad() As String
        Get
            Return Me._logoEntidad
        End Get
        Set(ByVal value As String)
            Me._logoEntidad = value
        End Set
    End Property

    Public Property IDUsuarioDominio() As String
        Get
            Return Me._usuarioDominio
        End Get
        Set(ByVal value As String)
            Me._usuarioDominio = value
        End Set
    End Property

    ' Fields
    Private _apellido As String = String.Empty
    Private _buscar As String
    Private _clave As String = String.Empty
    Private _descripcionCargo As String = String.Empty
    Private _descripcionRol As String = String.Empty
    Private _dNI As String = String.Empty
    Private _eliminado As Boolean = True
    Private _email As String = String.Empty

    'Private _iDCargo As Integer = 0
    Private _iDRol As Integer = 0
    Private _iDUsuario As String = String.Empty
    Private _nombre As String = String.Empty
    Private _usuario As String = String.Empty

    Private _iDInformador As String = String.Empty
    Private _apellido1 As String = String.Empty
    Private _apellido2 As String = String.Empty
    Private _nombre1 As String = String.Empty
    Private _nombre2 As String = String.Empty

    Private _estadoUsuario As Boolean = False
    Private _oficina As String = String.Empty
    Private _logoEntidad As String = String.Empty
    Private _iDEntidad As Integer = 0 '--
    Private _iDOficina As Integer = 0
    Private _codOficina As Integer = 0
    Private _iDCargo As Integer = 0 '--
    Private _iDCanal As Integer = 0
    Private _estado As Boolean = True '--
    Private _acceso As Boolean = False
    Private _usuarioDominio As String = String.Empty
End Class

