﻿Imports System

Public Class BELogSistema
    Inherits BEBase
    ' Properties
    Public Property Compilado As String
        Get
            Return Me._Compilado
        End Get
        Set(ByVal value As String)
            Me._Compilado = value
        End Set
    End Property

    Public Property Detalle As String
        Get
            Return Me._Detalle
        End Get
        Set(ByVal value As String)
            Me._Detalle = value
        End Set
    End Property

    Public Property Evento As String
        Get
            Return Me._Evento
        End Get
        Set(ByVal value As String)
            Me._Evento = value
        End Set
    End Property

    Public Property Host As String
        Get
            Return Me._Host
        End Get
        Set(ByVal value As String)
            Me._Host = value
        End Set
    End Property

    Public Property IDLogSistema As Integer
        Get
            Return Me._IDLogSistema
        End Get
        Set(ByVal value As Integer)
            Me._IDLogSistema = value
        End Set
    End Property

    Public Property IDSistema As Integer
        Get
            Return Me._IDSistema
        End Get
        Set(ByVal value As Integer)
            Me._IDSistema = value
        End Set
    End Property

    Public Property MensajeError As String
        Get
            Return Me._MensajeError
        End Get
        Set(ByVal value As String)
            Me._MensajeError = value
        End Set
    End Property

    Public Property Opcion As String
        Get
            Return Me._Opcion
        End Get
        Set(ByVal value As String)
            Me._Opcion = value
        End Set
    End Property

    Public Property Usuario As String
        Get
            Return Me._Usuario
        End Get
        Set(ByVal value As String)
            Me._Usuario = value
        End Set
    End Property


    ' Fields
    Private _Compilado As String = ""
    Private _Detalle As String = ""
    Private _Evento As String = ""
    Private _Host As String = ""
    Private _IDLogSistema As Integer = 0
    Private _IDSistema As Integer = 0
    Private _MensajeError As String = ""
    Private _Opcion As String = ""
    Private _Usuario As String = ""
End Class

