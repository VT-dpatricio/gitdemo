﻿Imports System
Imports System.Configuration

Public Class DLBase
    ' Methods
    Public Function CadenaConexion() As String
        Return ConfigurationManager.ConnectionStrings.Item("BAIS").ConnectionString
    End Function

    Public Function IDEntidad() As Integer
        Return Convert.ToInt32(ConfigurationManager.AppSettings.Item("IDEntidad").ToString)
    End Function

    Public Function IDSistema() As Integer
        Return Convert.ToInt32(ConfigurationManager.AppSettings.Item("IDSistema").ToString)
    End Function

End Class


