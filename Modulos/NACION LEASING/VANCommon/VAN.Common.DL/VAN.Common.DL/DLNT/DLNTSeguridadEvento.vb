﻿Imports VAN.Common.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLNTSeguridadEvento
    Inherits DLBase
    Implements IDLNTDataEntry

    Public Function Seleccionar() As System.Collections.IList Implements IDLNTDataEntry.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pEntidad As BE.BEBase) As System.Collections.IList Implements IDLNTDataEntry.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As System.Collections.IList Implements IDLNTDataEntry.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BE.BEBase Implements IDLNTDataEntry.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarUltimo(ByVal pUsuario As String, ByVal pIDTipoEvento As Integer) As BESeguridadEvento
        Dim cn As New SqlConnection(CadenaConexion)
        Dim cm As New SqlCommand("SE_SeleccionarEventoUltimo", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@IDUsuario", SqlDbType.VarChar, 50).Value = pUsuario
        cm.Parameters.Add("@IDTipoEvento", SqlDbType.Int).Value = pIDTipoEvento
        Dim oBE As New BESeguridadEvento
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            If rd.Read Then
                oBE.IDEvento = rd.GetInt32(rd.GetOrdinal("IDEvento"))
                oBE.IDSistema = rd.GetInt32(rd.GetOrdinal("IDSistema"))
                oBE.FechaRegistro = rd.GetDateTime(rd.GetOrdinal("FechaRegistro"))
                oBE.Detalle = rd.GetString(rd.GetOrdinal("Detalle"))
            End If
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return oBE
    End Function


End Class
