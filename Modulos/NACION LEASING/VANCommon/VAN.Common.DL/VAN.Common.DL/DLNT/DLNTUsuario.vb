﻿Imports VAN.Common.BE

Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLNTUsuario
    Inherits DLBase
    Implements IDLNTDataEntry

    ' Methods

    Public Function Seleccionar() As IList Implements IDLNTDataEntry.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pEntidad As BEBase) As IList Implements IDLNTDataEntry.Seleccionar
         Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As IList Implements IDLNTDataEntry.Seleccionar
         Throw New Exception("The method or operation is not implemented.")

    End Function

    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BEBase Implements IDLNTDataEntry.SeleccionarBE
         Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarE(ByVal pCodigo As String, ByVal pIDSistema As Int32) As BEUsuario
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("BW_SeleccionarUsuarioxID", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@IDUsuario", SqlDbType.VarChar, 50).Value = pCodigo
        cm.Parameters.Add("@IDSistema", SqlDbType.Int).Value = pIDSistema
        Dim oBE As New BEUsuario
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            If rd.Read Then
                oBE.IDUsuario = rd.GetString(rd.GetOrdinal("IDUsuario"))
                oBE.IDInformador = rd.GetString(rd.GetOrdinal("IDInformador"))

                oBE.Nombre = rd.GetString(rd.GetOrdinal("Nombre"))
                oBE.Nombre1 = rd.GetString(rd.GetOrdinal("Nombre1"))
                oBE.Nombre2 = rd.GetString(rd.GetOrdinal("Nombre2"))
                oBE.Apellido1 = rd.GetString(rd.GetOrdinal("Apellido1"))
                oBE.Apellido2 = rd.GetString(rd.GetOrdinal("Apellido2"))
                oBE.LogoEntidad = rd.GetString(rd.GetOrdinal("LogoEntidad"))
                oBE.IDEntidad = rd.GetInt32(rd.GetOrdinal("IDEntidad"))
                oBE.IDOficina = rd.GetInt32(rd.GetOrdinal("IDOficina"))
                oBE.CodOficina = rd.GetInt32(rd.GetOrdinal("CodOficina"))
                oBE.IDCargo = rd.GetInt32(rd.GetOrdinal("IDCargo"))
                oBE.IDCanal = rd.GetInt32(rd.GetOrdinal("IDCanal"))

                oBE.Oficina = rd.GetString(rd.GetOrdinal("Oficina"))

                oBE.DNI = rd.GetString(rd.GetOrdinal("CC"))
                oBE.Estado = rd.GetBoolean(rd.GetOrdinal("Activo"))
                oBE.Acceso = rd.GetBoolean(rd.GetOrdinal("Acceso"))

            End If
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return oBE
    End Function

    Public Function SeleccionarE(ByVal pCodigo As String, ByVal pIDSistema As Integer, ByVal pLoginDominio As Boolean) As BEUsuario
        Dim connection As New SqlConnection(Me.CadenaConexion)
        Dim command As New SqlCommand("BW_SeleccionarUsuarioxID", connection)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.Add("@IDUsuario", SqlDbType.VarChar, 50).Value = pCodigo
        command.Parameters.Add("@IDSistema", SqlDbType.Int).Value = pIDSistema
        command.Parameters.Add("@LoginDominio", SqlDbType.Bit).Value = pLoginDominio
        Dim usuario As New BEUsuario
        Try
            connection.Open()
            Dim reader As SqlDataReader = command.ExecuteReader
            If reader.Read Then
                usuario.IDUsuario = reader.GetString(reader.GetOrdinal("IDUsuario"))
                usuario.IDUsuarioDominio = reader.GetString(reader.GetOrdinal("IDUsuarioDominio"))
                usuario.IDInformador = reader.GetString(reader.GetOrdinal("IDInformador"))
                usuario.Nombre = reader.GetString(reader.GetOrdinal("Nombre"))
                usuario.Nombre1 = reader.GetString(reader.GetOrdinal("Nombre1"))
                usuario.Nombre2 = reader.GetString(reader.GetOrdinal("Nombre2"))
                usuario.Apellido1 = reader.GetString(reader.GetOrdinal("Apellido1"))
                usuario.Apellido2 = reader.GetString(reader.GetOrdinal("Apellido2"))
                usuario.IDEntidad = reader.GetInt32(reader.GetOrdinal("IDEntidad"))
                usuario.IDOficina = reader.GetInt32(reader.GetOrdinal("IDOficina"))
                usuario.CodOficina = reader.GetInt32(reader.GetOrdinal("CodOficina"))
                usuario.IDCargo = reader.GetInt32(reader.GetOrdinal("IDCargo"))
                usuario.IDCanal = reader.GetInt32(reader.GetOrdinal("IDCanal"))
                usuario.Oficina = reader.GetString(reader.GetOrdinal("Oficina"))
                usuario.DNI = reader.GetString(reader.GetOrdinal("CC"))
                usuario.Estado = reader.GetBoolean(reader.GetOrdinal("Activo"))
                usuario.Acceso = reader.GetBoolean(reader.GetOrdinal("Acceso"))
                usuario.EstadoUsuario = reader.GetBoolean(reader.GetOrdinal("EstadoUsuario"))
            End If
            reader.Close()
        Catch exception1 As Exception

            Dim exception As Exception = exception1
            Throw exception
        Finally
            If (connection.State = ConnectionState.Open) Then
                connection.Close()
            End If
        End Try
        Return usuario
    End Function

End Class


