﻿Imports VAN.Common.BE
Imports Microsoft.VisualBasic.CompilerServices
Imports System
Imports System.Data
Imports System.Data.SqlClient


Public Class DLTLogSistema
    Inherits DLBase
    Implements IDLTDataEntry
    ' Methods
    Public Function Actualizar(ByVal pEntidad As BEBase) As Boolean Implements IDLTDataEntry.Actualizar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Eliminar(ByVal pCodigo As Integer) As Boolean Implements IDLTDataEntry.Eliminar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Insertar(ByVal pEntidad As BEBase) As Boolean Implements IDLTDataEntry.Insertar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function InsertarLog(ByVal pEntidad As BEBase) As Integer
        Dim connection As New SqlConnection(MyBase.CadenaConexion)
        Dim pcm As New SqlCommand("BW_InsertarLogSistema", connection)
        pcm.CommandType = CommandType.StoredProcedure
        pcm = Me.LlenarEstructura(pEntidad, pcm, "I")
        Dim num2 As Integer = 0
        Try
            connection.Open()
            pcm.ExecuteNonQuery()
            num2 = Conversions.ToInteger(pcm.Parameters.Item("@IDLogSistema").Value)
        Catch exception1 As Exception
            ProjectData.SetProjectError(exception1)
            Dim exception As Exception = exception1
            Throw exception
        Finally
            If (connection.State = ConnectionState.Open) Then
                connection.Close()
            End If
        End Try
        Return num2
    End Function

    Public Function LlenarEstructura(ByVal pEntidad As BEBase, ByVal pcm As SqlCommand, ByVal TipoTransaccion As String) As SqlCommand Implements IDLTDataEntry.LlenarEstructura
        Dim sistema As BELogSistema = DirectCast(pEntidad, BELogSistema)
        If (TipoTransaccion = "I") Then
            pcm.Parameters.Add("@IDLogSistema", SqlDbType.Int).Direction = ParameterDirection.Output
            pcm.Parameters.Add("@IDSistema", SqlDbType.Int).Value = sistema.IDSistema
            pcm.Parameters.Add("@Compilado", SqlDbType.VarChar, 10).Value = sistema.Compilado
            pcm.Parameters.Add("@Host", SqlDbType.VarChar, 50).Value = sistema.Host
            pcm.Parameters.Add("@Usuario", SqlDbType.VarChar, 50).Value = sistema.Usuario
            pcm.Parameters.Add("@Opcion", SqlDbType.VarChar, 250).Value = sistema.Opcion
            pcm.Parameters.Add("@Evento", SqlDbType.VarChar, 250).Value = sistema.Evento
            pcm.Parameters.Add("@Error", SqlDbType.VarChar, &H1F40).Value = sistema.MensajeError
            pcm.Parameters.Add("@Detalle", SqlDbType.VarChar, &H1F40).Value = sistema.Detalle
        End If
        Return pcm
    End Function

End Class

