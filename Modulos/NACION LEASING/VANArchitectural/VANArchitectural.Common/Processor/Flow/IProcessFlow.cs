﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using AONArchitectural.Common.Processor.Validation;

namespace AONArchitectural.Common.Processor.Flow
{
    public interface IProcessFlow
    {
        IValidator PreProcess();
        IValidator Process();
        IValidator PostProcess();
    }
}
