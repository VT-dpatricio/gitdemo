﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AONArchitectural.Common.Processor.Formatters
{
    public class StringObfuscateAccNumberFormatter : Formatter
    {

        public override string Format(string value)
        {
            return AONArchitectural.Common.Obfuscator.Obfuscator.AccountNumber(value);
        }
    }
}
