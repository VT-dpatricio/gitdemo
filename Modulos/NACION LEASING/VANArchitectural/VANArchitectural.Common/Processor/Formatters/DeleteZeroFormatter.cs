﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace AONArchitectural.Common.Processor.Formatters
{
    public class DeleteZeroFormatter : Formatter
    {
        public DeleteZeroFormatter() : base() { }
        public DeleteZeroFormatter(int errorCode) : base(errorCode) { }

        public override string Format(string value)
        {
            if (IsNumeric(value))
            {
                char CaracteresAEliminar = '0';
                value = value.TrimStart(CaracteresAEliminar);
            }
            return value;
        }


    }
}
