﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace AONArchitectural.Common.DataDynamic
{
    public class ContainerDataDynamic
    {
        public List<BEDataDynamic> dataVariable = new List<BEDataDynamic>();

        public void addStringValue(string key, string value, string source)
        {
            BEDataDynamic eDym = new BEDataDynamic();

            eDym._nameKey = key;
            eDym._value = value;
            eDym._dataType = "S";
            dataVariable.Add(eDym);
        }

        public void addIntegerValue(string key, string value, string source)
        {
            BEDataDynamic eDym = new BEDataDynamic();

            eDym._nameKey = key;
            eDym._value = value;
            eDym._dataType = "I";
            eDym._source = source;
            dataVariable.Add(eDym);
        }

        public void addDecimalValue(string key, string value, string source)
        {
            BEDataDynamic eDym = new BEDataDynamic();

            eDym._nameKey = key;
            eDym._value = value;
            eDym._dataType = "D";
            eDym._source = source;
            dataVariable.Add(eDym);
        }

        public void addBooleanValue(string key, string value, string source)
        {
            BEDataDynamic eDym = new BEDataDynamic();

            eDym._nameKey = key;
            eDym._value = value;
            eDym._dataType = "B";
            eDym._source = source;
            dataVariable.Add(eDym);
        }

        public void addDateValue(string key, string value, string source)
        {
            DateTime AuxDate = new DateTime(1900, 01, 01);
            BEDataDynamic eDym = new BEDataDynamic();

            eDym._nameKey = key;
            if (DateTime.TryParse(key, out AuxDate) == true)
            {
                eDym._value = value;
            }
            eDym._dataType = "F";
            eDym._source = source;
            dataVariable.Add(eDym);
        }
    }
}
