﻿Imports VAN.Siniestros.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLTSiniestroCobertura
    Inherits DLBase

    Public Function Insertar(ByVal pEntidad As BESiniestroCobertura, ByVal cn As SqlConnection, ByVal sqlTranx As SqlTransaction) As Boolean
        Dim cm As New SqlCommand("SS_InsertarSiniestroCobertura", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm = Me.LlenarEstructura(pEntidad, cm, "I")
        Dim resultado As Boolean = False
        Try
            cm.Transaction = sqlTranx
            Dim nDA As Integer = cm.ExecuteNonQuery()
            'If nDA > 0 Then
            resultado = True
            'End If
        Catch ex As Exception
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function LlenarEstructura(ByVal oBE As BESiniestroCobertura, ByVal pcm As SqlCommand, ByVal TipoTransaccion As String) As SqlCommand
        If (TipoTransaccion = "I") Then
            pcm.Parameters.Add("@IDSiniestro", SqlDbType.Int).Value = oBE.IDSiniestro
            pcm.Parameters.Add("@IDCobertura", SqlDbType.Int).Value = oBE.IDCobertura
        End If
        Return pcm
    End Function

End Class
