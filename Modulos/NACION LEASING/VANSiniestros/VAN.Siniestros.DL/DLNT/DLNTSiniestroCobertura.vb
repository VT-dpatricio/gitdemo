﻿Imports VAN.Siniestros.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLNTSiniestroCobertura
    Inherits DLBase
    Implements IDLNT


    Public Function Seleccionar() As System.Collections.IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pEntidad As BE.BEBase) As System.Collections.IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As System.Collections.IList Implements IDLNT.Seleccionar
        Dim cn As New SqlConnection(CadenaConexion)
        Dim cm As New SqlCommand("SS_PRC_SEL_SiniestroCobertura", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@IDSiniestro", SqlDbType.Int).Value = pCodigo
        Dim oBE As BESiniestroCobertura
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BESiniestroCobertura
                oBE.IDCobertura = rd.GetInt32(rd.GetOrdinal("IDCobertura"))
                oBE.Cobertura = rd.GetString(rd.GetOrdinal("Cobertura"))
                oBE.Estado = True
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BE.BEBase Implements IDLNT.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function
End Class
