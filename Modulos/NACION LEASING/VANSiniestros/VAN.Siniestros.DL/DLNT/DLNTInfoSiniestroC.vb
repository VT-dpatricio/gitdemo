﻿Imports VAN.Siniestros.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLNTInfoSiniestroC
    Inherits DLBase
    Implements IDLNT

    Public Function SeleccionarAll(pCodigo As Integer) As IList
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SS_PRC_SEL_InfoSiniestroC", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@IDSiniestro", SqlDbType.VarChar, 25).Value = pCodigo
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                Dim oBE = New BEInfoSiniestroC
                oBE.IdInfoSiniestro = rd.GetInt32(rd.GetOrdinal("idInfoSiniestro"))
                oBE.Nombre = rd.GetString(rd.GetOrdinal("Nombre"))
                oBE.ValorString = rd.GetString(rd.GetOrdinal("Valor"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function Seleccionar() As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(pEntidad As BE.BEBase) As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(pCodigo As Integer) As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(pCodigo As Integer) As BE.BEBase Implements IDLNT.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function
End Class
