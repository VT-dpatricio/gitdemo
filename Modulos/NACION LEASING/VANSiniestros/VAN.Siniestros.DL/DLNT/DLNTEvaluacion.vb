﻿Imports VAN.Siniestros.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLNTEvaluacion
    Inherits DLBase
    Implements IDLNT

    Public Function Seleccionar() As System.Collections.IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pEntidad As BE.BEBase) As System.Collections.IList Implements IDLNT.Seleccionar
        Dim cn As New SqlConnection(CadenaConexion)
        Dim cm As New SqlCommand("SS_ListarEvaluacionA", cn)
        cm.CommandType = CommandType.StoredProcedure
        Dim oBE As BEEvaluacion = DirectCast(pEntidad, BEEvaluacion)
        cm.Parameters.Add("@IDProducto", SqlDbType.Int).Value = oBE.IDProducto
        cm.Parameters.Add("@IDSiniestro", SqlDbType.Int).Value = oBE.IDSiniestro
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BEEvaluacion
                oBE.IDPregunta = rd.GetInt32(rd.GetOrdinal("IDPregunta"))
                'oBE.IdProducto = rd.GetInt32(rd.GetOrdinal("IDProducto"))
                oBE.Nombre = rd.GetString(rd.GetOrdinal("Nombre"))
                oBE.Descripcion = rd.GetString(rd.GetOrdinal("Descripcion"))
                oBE.Respuesta = rd.GetBoolean(rd.GetOrdinal("Respuesta"))
                'oBE.Estado = rd.GetBoolean(rd.GetOrdinal("Estado"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista

    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As System.Collections.IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BE.BEBase Implements IDLNT.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function
End Class
