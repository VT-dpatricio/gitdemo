﻿Imports VAN.Siniestros.BE

Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLNTUsuario
    Inherits DLBase
    Implements IDLNT
    ' Methods

    Public Function Seleccionar() As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pEntidad As BEBase) As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")

    End Function

    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BEBase Implements IDLNT.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarE(ByVal pCodigo As String, ByVal pIDSistema As Int32) As BEUsuario
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        'Dim cm As New SqlCommand("AONAdmin_SeleccionarUsuarioxID", cn)
        Dim cm As New SqlCommand("BW_SeleccionarUsuarioxID", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@IDUsuario", SqlDbType.VarChar, 50).Value = pCodigo
        cm.Parameters.Add("@IDSistema", SqlDbType.Int).Value = pIDSistema
        Dim oBE As New BEUsuario
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            If rd.Read Then
                oBE.IDUsuario = rd.GetString(rd.GetOrdinal("IDUsuario"))
                oBE.IDInformador = rd.GetString(rd.GetOrdinal("IDInformador"))

                oBE.Nombre = rd.GetString(rd.GetOrdinal("Nombre"))
                oBE.Nombre1 = rd.GetString(rd.GetOrdinal("Nombre1"))
                oBE.Nombre2 = rd.GetString(rd.GetOrdinal("Nombre2"))
                oBE.Apellido1 = rd.GetString(rd.GetOrdinal("Apellido1"))
                oBE.Apellido2 = rd.GetString(rd.GetOrdinal("Apellido2"))
                oBE.LogoEntidad = rd.GetString(rd.GetOrdinal("LogoEntidad"))
                oBE.IDEntidad = rd.GetInt32(rd.GetOrdinal("IDEntidad"))
                oBE.IDOficina = rd.GetInt32(rd.GetOrdinal("IDOficina"))
                oBE.CodOficina = rd.GetInt32(rd.GetOrdinal("CodOficina"))
                oBE.IDCargo = rd.GetInt32(rd.GetOrdinal("IDCargo"))
                oBE.IDCanal = rd.GetInt32(rd.GetOrdinal("IDCanal"))

                oBE.DNI = rd.GetString(rd.GetOrdinal("CC"))
                oBE.Estado = rd.GetBoolean(rd.GetOrdinal("Activo"))
                oBE.Acceso = rd.GetBoolean(rd.GetOrdinal("Acceso"))

            End If
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return oBE
    End Function

End Class


