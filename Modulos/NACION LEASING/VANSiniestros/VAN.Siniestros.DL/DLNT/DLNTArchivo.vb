﻿Imports VAN.Siniestros.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLNTArchivo
    Inherits DLBase
    Implements IDLNT


    Public Function Seleccionar() As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(pEntidad As BE.BEBase) As IList Implements IDLNT.Seleccionar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SS_PRC_SEL_Archivo", cn)
        cm.CommandType = CommandType.StoredProcedure
        Dim oBE As BEArchivo = DirectCast(pEntidad, BEArchivo)
        cm.Parameters.Add("@IDSiniestro", SqlDbType.Int).Value = oBE.IdSiniestro
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BEArchivo
                oBE.IdArchivo = rd.GetInt32(rd.GetOrdinal("IdArchivo"))
                oBE.IdSiniestro = rd.GetInt32(rd.GetOrdinal("IdSiniestro"))
                oBE.FechaCreacion = rd.GetDateTime(rd.GetOrdinal("FechaCreacion"))
                oBE.NombreArchivo = rd.GetString(rd.GetOrdinal("NombreArchivo"))
                oBE.UsuarioCreacion = rd.GetString(rd.GetOrdinal("UsuarioCreacion"))
                oBE.Estado = rd.GetBoolean(rd.GetOrdinal("Estado"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function


    Public Function Eliminar(ByVal IdDocuento As Integer, ByVal IdUsuario As String) As Boolean
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SS_PRC_DEL_Archivo", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@IDArchivo", SqlDbType.Int).Value = IdDocuento
        cm.Parameters.Add("@IDUsuario", SqlDbType.VarChar, 50).Value = IdUsuario
        cm.Parameters.Add("@RetVal", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
        Dim resultado As Boolean = False
        Try
            cn.Open()
            cm.ExecuteNonQuery()
            resultado = cm.Parameters("@RetVal").Value()
            'resultado = True
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return resultado
    End Function

    Public Function Insertar(ByVal pEntidad As BEArchivo) As Boolean
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SS_PRC_INS_Archivo", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@IDSiniestro", SqlDbType.Int).Value = pEntidad.IdSiniestro
        cm.Parameters.Add("@Ruta", SqlDbType.VarChar, 8000).Value = pEntidad.RutaArchivo
        cm.Parameters.Add("@NombreArchivo", SqlDbType.VarChar, 500).Value = pEntidad.NombreArchivo
        cm.Parameters.Add("@FechaIngreso", SqlDbType.DateTime).Value = pEntidad.FechaIngreso
        cm.Parameters.Add("@IDUsuario", SqlDbType.VarChar, 50).Value = pEntidad.UsuarioCreacion
        cm.Parameters.Add("@RetVal", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
        Dim resultado As Boolean = False
        Try
            cn.Open()
            cm.ExecuteNonQuery()
            resultado = cm.Parameters("@RetVal").Value()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return resultado
    End Function

    Public Function Insertar(ByVal pEntidad As BEArchivo, ByVal cn As SqlConnection, ByVal sqlTranx As SqlTransaction) As Boolean
        'Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SS_PRC_INS_Archivo", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@IDSiniestro", SqlDbType.Int).Value = pEntidad.IdSiniestro
        cm.Parameters.Add("@Ruta", SqlDbType.VarChar, 8000).Value = pEntidad.RutaArchivo
        cm.Parameters.Add("@NombreArchivo", SqlDbType.VarChar, 500).Value = pEntidad.NombreArchivo
        cm.Parameters.Add("@FechaIngreso", SqlDbType.DateTime).Value = pEntidad.FechaIngreso
        cm.Parameters.Add("@IDUsuario", SqlDbType.VarChar, 50).Value = pEntidad.UsuarioCreacion
        cm.Parameters.Add("@RetVal", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
        Dim resultado As Boolean = False
        Try
            cm.Transaction = sqlTranx
            cm.ExecuteNonQuery()
            Dim nDA As Integer = cm.Parameters("@RetVal").Value()
            If nDA > 0 Then
                resultado = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function Seleccionar(pCodigo As Integer) As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(pCodigo As Integer) As BE.BEBase Implements IDLNT.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function
End Class
