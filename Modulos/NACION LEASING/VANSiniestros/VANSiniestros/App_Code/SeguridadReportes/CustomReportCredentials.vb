﻿Imports System.Net.NetworkCredential
Imports System.Net
Imports System.Security.Principal
Imports Microsoft.Reporting.WebForms


<Serializable()> _
Public NotInheritable Class CustomReportCredentials : Implements IReportServerCredentials

    Public Function GetFormsCredentials(ByRef authCookie As System.Net.Cookie, ByRef userName As String, ByRef password As String, ByRef authority As String) As Boolean Implements IReportServerCredentials.GetFormsCredentials


        authCookie = Nothing
        userName = Nothing
        password = Nothing
        authority = Nothing

        Return False


    End Function

    Public ReadOnly Property ImpersonationUser() As WindowsIdentity Implements IReportServerCredentials.ImpersonationUser
        Get
            Return Nothing
        End Get
    End Property

    Public ReadOnly Property NetworkCredentials() As System.Net.ICredentials Implements IReportServerCredentials.NetworkCredentials
        Get

            'User name
            Dim userName As String = ConfigurationManager.AppSettings("AuthenticationUser")
            
            If (String.IsNullOrEmpty(userName)) Then
                Throw New Exception("No se definio el usuario para la auntentificacion de los reportes. Por favor verifique la propiedad AuthenticationUser en el web.config")
            End If

            'Password
            Dim password As String = ConfigurationManager.AppSettings("AuthenticationPassword")
            '_ConfigurationManager.AppSettings("MyReportViewerPassword")

            If (String.IsNullOrEmpty(password)) Then
                Throw New Exception("No se definio la contraseña para la auntentificacion de los reportes. Por favor verifique la propiedad AuthenticationPassword en el web.config")
            End If

            'Domain
            Dim domain As String = ConfigurationManager.AppSettings("AuthenticationDomain")


            If (String.IsNullOrEmpty(domain)) Then
                Throw New Exception("No se definio el dominio para la auntentificacion de los reportes. Por favor verifique la propiedad AuthenticationDomain en el web.config")
            End If


            Return New System.Net.NetworkCredential(userName, password, domain)
        End Get
    End Property


End Class
