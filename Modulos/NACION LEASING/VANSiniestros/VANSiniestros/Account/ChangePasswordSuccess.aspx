﻿<%@ Page Title="Cambiar contraseña" Language="vb" MasterPageFile="~/Site.Master" AutoEventWireup="false"
    CodeBehind="ChangePasswordSuccess.aspx.vb" Inherits="VANSiniestros.ChangePasswordSuccess" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Cambiar contraseña
    </h2>
    <p>
        La contraseña se ha cambiado correctamente.
        <asp:DynamicControl ID="DynamicControl1" runat="server" />
    </p>
</asp:Content>
