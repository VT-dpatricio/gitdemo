﻿Imports VAN.Siniestros.BL
Public Class CobroReclamo
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            hfIDCertificado.Value = Request.QueryString("ID")
            cargarCobro()
        End If
    End Sub
    Private Sub cargarCobro()
        Dim oBL As New BLCobro
        gvCobro.DataSource = oBL.Seleccionar(hfIDCertificado.Value, -10)
        gvCobro.DataBind()
    End Sub

    Protected Sub gvCobro_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCobro.PageIndexChanging
        gvCobro.PageIndex = e.NewPageIndex
        cargarCobro()
    End Sub
End Class