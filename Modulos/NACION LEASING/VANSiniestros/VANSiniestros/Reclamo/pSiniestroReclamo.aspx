﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pSiniestroReclamo.aspx.vb" Inherits="VANSiniestros.pSiniestroReclamo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:GridView 
        ID="gvSiniestro" runat="server" 
                                        AutoGenerateColumns="False" DataKeyNames="IDSiniestro,IDCertificado" 
                                        Width="891px">
                                        <Columns>
                                            <asp:TemplateField HeaderText="N° Ticket">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" 
                                                        Text='<%# Format(Eval("IDSiniestro"),"000000") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("IDSiniestro") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="70px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="F. Siniestro">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label3" runat="server" 
                                                        Text='<%# Format(Eval("FechaSiniestro"),"dd/MM/yyyy") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("FechaSiniestro") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="TipoSiniestro" HeaderText="TipoSiniestro" />
                                            <asp:TemplateField HeaderText="Producto">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label9" runat="server" Text='<%# Bind("Producto") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Tipo Documento">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label10" runat="server" Text='<%# Bind("TipoDocumento") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="N° Documento">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label11" runat="server" Text='<%# Bind("CcCliente") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="100px" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Apellidos y Nombres">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label12" runat="server" 
                                                        Text='<%# Eval("Apellido1") &  " " & Eval("Nombre1") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="180px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Estado">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label13" runat="server" Text='<%# Bind("EstadoSiniestro") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="70px" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No se encontron registros
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                
&nbsp;<asp:HiddenField ID="hfIDCertificado" runat="server" Value="0" />
    </form>
</body>
</html>
