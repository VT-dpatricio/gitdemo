﻿Imports VAN.Siniestros.BL
Public Class ImagenReclamo
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            hfIDCertificado.Value = Request.QueryString("ID")
            Dim oBL As New BLImagen
            lbImagen.DataSource = oBL.Seleccionar(hfIDCertificado.Value)
            lbImagen.DataValueField = "RutaImagen"
            lbImagen.DataTextField = "NombreImagen"
            lbImagen.DataBind()
            If (lbImagen.Items.Count > 0) Then
                lbImagen.SelectedIndex = 0
                VisualizarImgCer(lbImagen.SelectedValue)
                imgCer.Visible = True
            Else
                imgCer.Visible = False
            End If
        End If
    End Sub

    Protected Sub lbImagen_SelectedIndexChanged1(ByVal sender As Object, ByVal e As EventArgs) Handles lbImagen.SelectedIndexChanged
        VisualizarImgCer(lbImagen.SelectedValue)
    End Sub
    Private Sub VisualizarImgCer(ByVal pImg As String)
        Dim UrlImg As String = ConfigurationManager.AppSettings("UrlImgCertificado").ToString
        imgCer.ImageUrl = UrlImg & pImg
    End Sub
End Class