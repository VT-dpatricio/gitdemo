﻿Imports Microsoft.Reporting.WebForms
Imports System.IO
Imports VAN.Siniestros.BL
Imports VAN.Siniestros.BE

Public Class Imprimir
    Inherits PaginaBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim rv As New ReportViewer
            Dim warnings As Warning() = Nothing
            Dim streamids As String() = Nothing
            Dim mimeType As String = Nothing
            Dim encoding As String = Nothing
            Dim extension As String = Nothing
            Dim bytes As Byte()
            Dim deviceInf As String = Nothing
            Dim rpt As String = ""
            Dim id As String = Request.QueryString("ID")
            Dim TipoReporte As String = Request.QueryString("Tipo")
            Dim oBLRep As New BLReporte
            Dim oBERep As New BEReporte
            Dim paramList As Generic.List(Of ReportParameter) = New Generic.List(Of ReportParameter)()

            If TipoReporte = "N" Then
                Dim idReclamo As String = id.Substring(id.LastIndexOf("-") + 1)
                oBERep = oBLRep.Seleccionar(idReclamo, TipoReporte)
                paramList.Add(New ReportParameter("IDReclamo", idReclamo))
                rpt = "rptNotaEstadoReclamo"
            End If
            'Dim idCertificado As String = "5327-111111"
            Dim ServidorRS As String = ConfigurationManager.AppSettings("ReportServerUri").ToString()
            Dim ServidorDirApp As String = ConfigurationManager.AppSettings("ServidorRSDirApp").ToString()


            ReportAunthentication.ConfigureAuthentication(rv)

            rv.ProcessingMode = ProcessingMode.Remote
            rv.ServerReport.ReportServerUrl = New Uri(ServidorRS)
            rv.ServerReport.ReportPath = ServidorDirApp + rpt

            rv.ServerReport.SetParameters(paramList)
            bytes = rv.ServerReport.Render("PDF", deviceInf, mimeType, encoding, extension, streamids, warnings)
            Response.ClearContent()
            Response.ContentType = "application/pdf"
            Response.AddHeader("content-length", bytes.Length.ToString())
            'Response.AddHeader("Content-Disposition", "inline; filename=Certificado.pdf")
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()
        End If

    End Sub

End Class