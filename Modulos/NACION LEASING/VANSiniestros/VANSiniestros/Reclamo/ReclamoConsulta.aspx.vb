﻿Imports VAN.Siniestros.BE
Imports VAN.Siniestros.BL

Imports WSInvoker = VANWebServiceInvoker
Public Class ReclamoConsulta
    Inherits PaginaBase

#Region "Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'ddlBuscarPor.SelectedValue = "ApeNom"
            'txtPar1.Text = "25498747"
            'BuscarReclamo()
            If ValidarEstadoClaveUsuario() Then
                Response.Redirect(Constantes.CHANGE_PSW_PAGE, False)
            End If

            pCambioMedioPago.Visible = False
            pDevolucion.Visible = False
            cargarMotivo()
            cargarEstadoReclamo()
            cargarEntidad()

            'txtFechaAtencion.Text = Date.Now().ToString("dd/MM/yyyy")
            'ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "TabEnabled", "TabActive(false);", True)
        End If
    End Sub

    Private Sub cargarEntidad()
        Dim oBLEntidad As New BLEntidad
        Dim IDUsuario As String = User.Identity.Name
        ddlEntidad.DataSource = oBLEntidad.Seleccionar(IDUsuario)
        ddlEntidad.DataTextField = "Descripcion"
        ddlEntidad.DataValueField = "IDEntidad"
        ddlEntidad.DataBind()
    End Sub

    Private Sub cargarMotivo()
        Dim oBL As New BLMontivoReclamo
        ddlMotivo.Items.Add(New ListItem("-Seleccionar-", "0"))
        ddlMotivo.DataSource = oBL.Listar()
        ddlMotivo.DataValueField = "IDMotivoReclamo"
        ddlMotivo.DataTextField = "Nombre"
        ddlMotivo.DataBind()
    End Sub

    Private Sub cargarEstadoReclamo()
        Dim oBL As New BLEstadoReclamo
        ddlEstado.Items.Add(New ListItem("-Seleccionar-", "0"))
        ddlEstado.DataSource = oBL.Listar()
        ddlEstado.DataValueField = "IDEstadoReclamo"
        ddlEstado.DataTextField = "Nombre"
        ddlEstado.DataBind()
    End Sub

#End Region

#Region "TAB Buscar Reclamo"
    Protected Sub ddlBuscarPor_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlBuscarPor.SelectedIndexChanged
        'If ddlBuscarPor.SelectedValue = "NroDoc" Then
        '    lblPar1.Text = "N° Documento:"
        '    lblPar2.Visible = False
        '    txtPar2.Visible = False
        'ElseIf ddlBuscarPor.SelectedValue = "ApeNom" Then
        '    lblPar1.Text = "Apellidos:"
        '    lblPar2.Visible = True
        '    txtPar2.Visible = True
        'End If
        Select Case ddlBuscarPor.SelectedValue
            Case "ApeNom"
                lblPar1.Text = "Apellidos:"
                lblPar2.Visible = True
                txtPar2.Visible = True
            Case Else
                lblPar2.Visible = False
                txtPar2.Visible = False
                lblPar1.Text = ddlBuscarPor.SelectedItem.Text & ":"
        End Select
    End Sub

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click
        BuscarReclamo()
    End Sub

    Private Sub BuscarReclamo()
        Dim oBL As New BLReclamo
        Dim IDUsuario As String = User.Identity.Name
        gvListaReclamo.DataSource = oBL.Buscar(CInt(ddlEntidad.SelectedValue), IDUsuario, ddlBuscarPor.SelectedValue, txtPar1.Text.Trim, txtPar2.Text.Trim, 0)
        gvListaReclamo.DataBind()
        gvListaReclamo.SelectedIndex = -1
    End Sub
#End Region

#Region "TAB Datos del Reclamo"
    Protected Sub btnActRegReclamo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnActRegReclamo.Click
        Try
            cargarDatosReclamo()
        Catch ex As Exception
            LimpiarControles()
            EnabledControles(False)
            btnModificar.Enabled = False
            ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "MsgError", "alert('No se pudo cargar el reclamo con éxito.');", True)
        End Try
    End Sub

    Private Sub cargarDatosReclamo()
        LimpiarControles()
        'EnabledControles(False)
        Dim oBL As New BLCertificado
        Dim oBE As BECertificado = oBL.Seleccionar(gvListaReclamo.SelectedDataKey.Item(1).ToString)

        'cargarTipoSiniestro(oBE.IdProducto)
        'cargarMoneda(oBE.IdProducto)
        Session(Constantes.SS_APP_BECER) = oBE
        LimpiarControles()
        hfIDCertificado.Value = oBE.IdCertificado
        lblNroCertificado.Text = oBE.IdCertificado
        hfIDProducto.Value = oBE.IdProducto
        hfIDTipoDocumento.Value = oBE.IdTipoDocumento
        lblProducto.Text = oBE.Producto
        lblCanal.Text = oBE.Oficina
        lblTipoDocumento.Text = oBE.TipoDocumento
        lblNroDocumento.Text = oBE.CcCliente
        lblAsegurado.Text = oBE.Nombre1 & " " & oBE.Nombre2 & " " & oBE.Apellido1 & " " & oBE.Apellido2
        lblNroCerBanco.Text = oBE.NumCertificadoBanco
        lblNroCobro.Text = oBE.NroCobro
        lblNroSiniestro.Text = oBE.NroSiniestro
        lblEstadoCer.Text = oBE.Estado
        If (oBE.DigitacionAnulacion.Trim() <> "" And oBE.DigitacionAnulacion.Trim() <> "01/01/1900") Then
            lblFechaAnulacion.Text = oBE.DigitacionAnulacion
        End If

        txtIDMoneda.Text = oBE.IdMonedaPrima
        hfSimboloMoneda.Value = oBE.SimboloMonedaPrima
        If oBE.NroImagen = 0 Then
            lblNroImagen.Text = "NO"
            lblNroImagen.ForeColor = Drawing.Color.Red
        Else
            lblNroImagen.ForeColor = Drawing.Color.Green
            lblNroImagen.Text = "SI"
        End If

        If oBE.NroSiniestro = 0 Then
            lblNroSiniestro.Text = "NO"
        Else
            lblNroSiniestro.Text = "SI"
        End If

        lblAseguradora.Text = oBE.Aseguradora
        lblMonedaCer.Text = oBE.IdMonedaPrima

        'Datos del Siniestro

        Dim oBLRec As New BLReclamo
        Dim oBERec As BEReclamo = oBLRec.Seleccionar(gvListaReclamo.SelectedDataKey.Item(0).ToString)
        Session(Constantes.SS_APP_BEREC) = oBERec
        chkCtaExterna.Checked = oBERec.CuentaExterna
        txtNroTicket.Text = oBERec.FechaCreacion.ToString("yyyyMMdd") & "-" & oBERec.IDReclamo.ToString("000000")
        hfIDReclamo.Value = oBERec.IDReclamo
        txtNroReclamo.Text = oBERec.NroReclamo
        txtFechaReclamo.Text = oBERec.FechaReclamo
        txtAseDireccion.Text = oBERec.AseDireccion
        txtAseEmail.Text = oBERec.AseEmail
        txtAseTelefono.Text = oBERec.AseTelefono

        If oBERec.FechaAtencion.ToString("dd/MM/yyyy") <> "01/01/1900" Then
            txtFechaAtencion.Text = oBERec.FechaAtencion
        Else
            txtFechaAtencion.Text = ""
        End If

        If oBERec.FechaFinAtencion.ToString("dd/MM/yyyy") <> "01/01/1900" Then
            txtFechaFinAtencion.Text = oBERec.FechaFinAtencion
        Else
            txtFechaFinAtencion.Text = ""
        End If

        ddlEstado.SelectedValue = oBERec.IDEstadoReclamo
        If (oBERec.IDEstadoReclamo = 1) Then
            pDevolucion.Visible = True
        Else
            pDevolucion.Visible = False
        End If
        ddlMotivo.SelectedValue = oBERec.IDMotivoReclamo
        SelTipoMotivoReclamo(ddlMotivo.SelectedValue)
        If oBERec.CobroInicioPeriodo.ToString("dd/MM/yyyy") <> "01/01/1900" Then
            txtFechaVigInicio.Text = oBERec.CobroInicioPeriodo
        End If

        If oBERec.CobroFinPeriodo.ToString("dd/MM/yyyy") <> "01/01/1900" Then
            txtFechaVigFin.Text = oBERec.CobroFinPeriodo
        End If


        txtIDMoneda.Text = oBERec.IDMoneda
        If (oBERec.FechaExtorno <> "01/01/1900") Then
            'txtFechaExtorno.Text = oBERec.FechaExtorno
        End If

        Dim lstMedioPago As New ArrayList
        Dim oBLMReclamo As New BLMontivoReclamo

        CargarDatosCuentaCheck()

        'txtMedioDevolucion.Text = oBERec.MedioDevolucion

        txtObservacion.Text = oBERec.Observacion
        txtObservacionAseg.Text = oBERec.ObservacionAsegurado

        EnabledControles(False)
    End Sub

    Protected Sub btnGrabar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGrabar.Click
        Try
            Dim oBERec As New BEReclamo
            Dim oBECer As BECertificado
            oBERec = DirectCast(Session(Constantes.SS_APP_BEREC), BEReclamo)
            oBECer = DirectCast(Session(Constantes.SS_APP_BECER), BECertificado)

            If Not String.IsNullOrWhiteSpace(hfIDTipoMotivoReclamo.Value) AndAlso hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_CMP AndAlso (String.IsNullOrEmpty(txtNumero.Text) Or ddlMedioPago.SelectedValue = "0") Then
                If ddlMotivo.SelectedValue <> oBERec.IDMotivoReclamo Then
                    ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "MsgError", "alert('Debe seleccionar el nuevo Medio de Pago');", True)
                    Exit Sub
                End If
            Else
                If Not String.IsNullOrWhiteSpace(hfIDTipoMotivoReclamo.Value) AndAlso hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_DEV AndAlso (String.IsNullOrEmpty(txtNumeroDev.Text) Or ddlMedioPagoDev.SelectedValue = "0") Then
                    ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "MsgError", "alert('Debe selecciona el Medio de Pago y Nro de Cuenta para la devolución');", True)
                    Exit Sub
                End If
            End If

            'Verifico si el medio de pago seleccionado en caso del Endoso se encuentra habilitado para el producto ya que el cambio afecta al Certificado
            If Not ValidaMedioPago(hfIDTipoMotivoReclamo.Value) Then
                ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "MsgError", "alert('No es posible actualizar el Reclamo debido a que el Medio de Pago seleccionado no se encuentra habilitado para este producto');", True)
                Exit Sub
            End If


            Dim oBL As New BLReclamo
            Dim oBE As New BEReclamo
            oBE.IDReclamo = CInt(hfIDReclamo.Value)
            oBE.IDCertificado = hfIDCertificado.Value
            oBE.FechaReclamo = CDate(txtFechaReclamo.Text)
            oBE.NroReclamo = txtNroReclamo.Text
            If txtFechaAtencion.Text.Trim() = "" Then
                oBE.FechaAtencion = CDate("01/01/1900")
            Else
                oBE.FechaAtencion = CDate(txtFechaAtencion.Text)
            End If

            If txtFechaFinAtencion.Text.Trim() = "" Then
                oBE.FechaFinAtencion = CDate("01/01/1900")
            Else
                oBE.FechaFinAtencion = CDate(txtFechaFinAtencion.Text)
            End If

            oBE.GenerarDevolucion = False
            oBE.CuentaExterna = chkCtaExterna.Checked
            If hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_DEV Or hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_CMP Then
                oBE.RAIZ = hfRaiz.Value
                If hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_CMP Then
                    If ddlMotivo.SelectedValue.ToString() <> oBERec.IDMotivoReclamo.ToString() Or (Not String.IsNullOrEmpty(txtNumero.Text) And txtNumero.Text <> txtNumeroActual.Text) Then
                        oBE.MedioDevolucion = txtNumero.Text
                        oBE.CBU = txtCBU.Text
                        oBE.IDMedioPago = ddlMedioPago.SelectedValue
                    Else
                        oBE.MedioDevolucion = txtNumeroActual.Text
                        oBE.CBU = txtCBUActual.Text
                        oBE.IDMedioPago = ddlMedioPagoActual.SelectedValue
                    End If

                    oBECer.CBU = oBE.CBU
                    oBECer.IdMedioPago = oBE.IDMedioPago
                    oBECer.NumeroCuenta = oBE.MedioDevolucion
                    txtNumeroActual.Text = oBECer.NumeroCuenta
                    ddlMedioPagoActual.SelectedValue = oBECer.IdMedioPago
                    txtCBUActual.Text = oBECer.CBU
                    txtNumero.Text = String.Empty
                    txtCBU.Text = String.Empty
                    ddlMedioPago.SelectedValue = "0"
                Else
                    If hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_DEV Then
                        oBE.MedioDevolucion = txtNumeroDev.Text
                        oBE.CBU = txtCBUDev.Text
                        oBE.IDMedioPago = ddlMedioPagoDev.SelectedValue
                    End If
                End If
            End If
            oBE.Imagen = IIf(lblNroImagen.Text = "SI", True, False)
            oBE.Siniestro = IIf(lblNroSiniestro.Text = "SI", True, False)
            oBE.NroCobro = CInt(lblNroCobro.Text)

            oBE.IDEstadoReclamo = ddlEstado.SelectedValue
            oBE.IDMotivoReclamo = ddlMotivo.SelectedValue


            oBE.AseDireccion = txtAseDireccion.Text
            oBE.AseEmail = txtAseEmail.Text
            oBE.AseTelefono = txtAseTelefono.Text

            oBE.Observacion = txtObservacion.Text
            oBE.ObservacionAsegurado = txtObservacionAseg.Text
            oBE.UsuarioModificacion = User.Identity.Name
            oBE.MedioDevolucionOriginal = DirectCast(Session(Constantes.SS_APP_BEREC), BEReclamo).MedioDevolucion
            If oBL.Actualizar(oBE) <> 0 Then
                'txtNroTicket.Text = pIDReclamo.ToString("000000")
                EnabledControles(False)

                ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "MsgError", "alert('Reclamo actualizado con éxito');", True)
            Else
                ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "MsgError", "alert('Ocurrió un error al grabar. Consulte con el Administrador del Sistema');", True)
            End If
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "MsgError", "alert('Ocurrió un error al grabar. Consulte con el Administrador del Sistema');", True)
        End Try
        'ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "TabActRS", "alert('" & txtAseDireccion.Text & ");", True)
    End Sub

    Private Function ValidaMedioPago(ByVal codTipoMotivo As String)
        'Esta validacion se realiza ya que si es un Endoso el cambio afecta al Certificado y en consultas
        ' solo se muestran los medios de pago habilitados que tiene el producto.
        Dim result As Boolean = False

        If hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_CMP Then
            Dim oBL As New BLListaGenerica
            Dim lst As New ArrayList
            lst = oBL.Seleccionar(CInt(hfIDProducto.Value), "MedioPago")

            For Each oBEMedios As BEListaGenerica In lst
                If oBEMedios.ID = ddlMedioPago.SelectedValue Then
                    result = True
                    Exit For
                End If
            Next
        Else
            result = True
        End If
        Return result
    End Function

    Private Sub LimpiarControles()
        hfIDProducto.Value = 0
        hfIDCertificado.Value = 0
        hfRaiz.Value = String.Empty
        hfIDTipoMotivoReclamo.Value = String.Empty
        lblNroCertificado.Text = String.Empty
        lblProducto.Text = String.Empty
        lblCanal.Text = String.Empty
        lblTipoDocumento.Text = String.Empty
        lblNroDocumento.Text = String.Empty
        lblAsegurado.Text = String.Empty
        lblCanal.Text = String.Empty
        lblAseguradora.Text = String.Empty
        lblNroImagen.Text = String.Empty
        lblNroCerBanco.Text = String.Empty
        lblNroCobro.Text = String.Empty
        txtFechaReclamo.Text = String.Empty
        txtNroTicket.Text = "-"
        ddlEstado.SelectedValue = "0"
        ddlMotivo.SelectedValue = "0"
        'txtFechaExtorno.Text = String.Empty
        'txtOperacionExtorno.Text = String.Empty
        txtObservacion.Text = String.Empty
        txtNroReclamo.Text = String.Empty
        txtFechaVigInicio.Text = String.Empty
        txtFechaVigFin.Text = String.Empty
        'txtNroCuotaDev.Text = String.Empty
        'txtMontoTotal.Text = String.Empty
        txtIDMoneda.Text = String.Empty
        pDevolucion.Visible = False
        lblNroSiniestro.Text = String.Empty
        lblEstadoCer.Text = String.Empty
        lblFechaAnulacion.Text = String.Empty
        txtObservacionAseg.Text = String.Empty
        pResumen.Visible = False

        pCambioMedioPago.Visible = False
        pDevolucion.Visible = False
        hfIDProducto.Value = 0
        hfIDTipoMotivoReclamo.Value = String.Empty
        txtCBU.Text = String.Empty
        txtAseTelefono.Text = String.Empty
        txtAseDireccion.Text = String.Empty
        txtAseEmail.Text = String.Empty
        txtFechaAtencion.Text = String.Empty

        ResetPanelDevolucion()
        ResetPanelMedioPago()

    End Sub

    Private Sub EnabledControles(ByVal pEstado As Boolean)
        txtFechaReclamo.Enabled = pEstado
        txtNroReclamo.Enabled = pEstado
        txtAseDireccion.Enabled = pEstado
        txtAseEmail.Enabled = pEstado
        txtAseTelefono.Enabled = pEstado
        'txtMedioDevolucion.Enabled = pEstado
        txtFechaFinAtencion.Enabled = pEstado
        txtCBU.Enabled = pEstado
        ddlEstado.Enabled = pEstado
        ddlMotivo.Enabled = pEstado
        ' txtFechaExtorno.Enabled = pEstado
        txtFechaAtencion.Enabled = pEstado
        'txtOperacionExtorno.Enabled = pEstado
        txtObservacion.ReadOnly = Not pEstado
        txtObservacionAseg.ReadOnly = Not pEstado

        'chkGenerarDevolucion.Enabled = pEstado
        chkCtaExterna.Enabled = pEstado
        btnGrabar.Enabled = pEstado
        btnModificar.Enabled = Not pEstado
        btnCancelar.Enabled = pEstado
        iBtnVerCobroSel.Visible = pEstado


        If hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_DEV And Not chkCtaExterna.Checked Then
            iBtnVerCobroSelDev.Visible = btnGrabar.Enabled
            txtCBUDev.Visible = True
        Else
            If hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_DEV Then
                iBtnVerCobroSelDev.Visible = False
                txtCBUDev.Visible = False
            End If
        End If

        If hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_DEV And chkCtaExterna.Checked Then
            iBtnVerCobroSelDev.Visible = False
            txtCBUDev.Visible = False
        Else
            If hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_DEV And Not chkCtaExterna.Checked Then
                iBtnVerCobroSelDev.Visible = btnGrabar.Enabled
                txtCBUDev.Visible = True
            End If
        End If
        btnImpresionNota.Enabled = Not pEstado
        ddlMedioPagoActual.Enabled = False
        txtCBUActual.Enabled = False
        txtCBUActual.ReadOnly = True
        txtNumeroActual.Enabled = False
        txtNumeroActual.ReadOnly = True
        txtNumero.Enabled = False
        txtCBU.Enabled = False
        txtCBU.ReadOnly = True
        ddlMedioPago.Enabled = False

        txtCBUDev.Enabled = False
        txtCBUDev.ReadOnly = True
        ChangeColorControlMP(txtNumeroDev, True)
        ChangeColorControlMP(ddlMedioPagoDev, True)

        ddlMedioPagoDev.Enabled = False
        txtNumeroDev.Enabled = False
        txtNumeroDev.ReadOnly = True

        If chkCtaExterna.Checked Then
            ddlMedioPagoDev.Enabled = pEstado
            txtNumeroDev.Enabled = pEstado
            txtNumeroDev.ReadOnly = Not pEstado
        End If

        gvCuentaTarjeta.DataSource = New List(Of WSInvoker.Model.MedioPago)
        gvCuentaTarjeta.DataBind()

    End Sub


    Protected Sub iBtnVerCobro_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles iBtnVerCobroSel.Click, iBtnVerCobroSelDev.Click
        'LlenarCobrosGrid()
        'ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "verCobroSel", "verCobroSel();", True)

        ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "MedioPago", "BuscarMedioPago();", True)
        LlenarMedioPagoGrid()
    End Sub

    Private Sub LlenarCobrosGrid()
        Dim oBL As New BLCobro
        gvCobro.DataSource = oBL.Seleccionar(hfIDCertificado.Value, 0)
        gvCobro.DataBind()
    End Sub

    Protected Sub iBtnVerImgCertificado_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles iBtnVerImgCertificado.Click
        ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "verImg", "verImagenCer('" & hfIDCertificado.Value & "');", True)
    End Sub

    Protected Sub iBtnVerCobro_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles iBtnVerCobro.Click
        ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "verCobro", "verCobro('" & hfIDCertificado.Value & "');", True)
    End Sub

    Protected Sub iBtnVerSiniestro_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles iBtnVerSiniestro.Click
        ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "verSiniestro", "verSiniestro('" & hfIDCertificado.Value & "');", True)
    End Sub

    Protected Sub btnSelCobroAceptar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSelCobroAceptar.Click
        'Dim i As Integer
        'Dim c As Int32 = 0
        'Dim MontoTotal As Double = 0

        'Dim IndiceIniPer As Int32 = 0
        'Dim IndiceFinPer As Int32 = 0
        'For i = 0 To gvCobro.Rows.Count - 1
        '    If DirectCast(gvCobro.Rows.Item(i).Cells.Item(0).Controls.Item(1), CheckBox).Checked Then
        '        MontoTotal = CDbl(DirectCast(gvCobro.Rows.Item(i).Cells.Item(1).Controls.Item(1), Label).Text) + MontoTotal
        '        c = 1 + c
        '        If c = 1 Then
        '            IndiceFinPer = i
        '        End If
        '        IndiceIniPer = i
        '    End If
        'Next i

        'If c <> 0 Then
        '    txtFechaVigInicio.Text = DirectCast(gvCobro.Rows.Item(IndiceIniPer).Cells.Item(2).Controls.Item(1), Label).Text
        '    txtFechaVigFin.Text = DirectCast(gvCobro.Rows.Item(IndiceFinPer).Cells.Item(2).Controls.Item(1), Label).Text
        'End If
        'txtNroCuotaDev.Text = c
        'txtMontoTotal.Text = MontoTotal.ToString("0.00")
        'txtIDMoneda.Text = lblMonedaCer.Text
    End Sub

    Protected Sub gvCobro_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCobro.PageIndexChanging
        gvCobro.PageIndex = e.NewPageIndex
        LlenarCobrosGrid()
    End Sub

    Protected Sub gvListaReclamo_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gvListaReclamo.SelectedIndexChanged
        ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "TabActRS", "ActTabRegistroSiniestro();", True)
    End Sub

    Protected Sub gvListaReclamo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListaReclamo.PageIndexChanging
        gvListaReclamo.PageIndex = e.NewPageIndex
        BuscarReclamo()
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancelar.Click
        cargarDatosReclamo()
        'EnabledControles(False)
    End Sub

    Protected Sub btnModificar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnModificar.Click
        Try
            Dim oBLRec As New BLReclamo
            Dim oBERec As BEReclamo = oBLRec.Seleccionar(CInt(hfIDReclamo.Value))
            Session(Constantes.SS_APP_BEREC) = oBERec
            EnabledControles(True)
        Catch ex As Exception
            LimpiarControles()
            EnabledControles(False)
            btnModificar.Enabled = False
            ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "MsgError", "alert('Error al intentar modificar el reclamo.", True)
        End Try
    End Sub

#End Region


    Protected Sub btnImpresionNota_Click(sender As Object, e As EventArgs) Handles btnImpresionNota.Click

        Dim ID As String = txtNroTicket.Text
        ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M2", "Imprimir('" & ID & "','N');", True)
    End Sub


    Private Sub LlenarMedioPagoGrid()
        Try
            Dim IDPRODUCTO As Int32 = CInt(hfIDProducto.Value)
            Dim SrvInvoker As New WSInvoker.WebServiceInvoker()
            Dim ClientITAU As New WSInvoker.Model.Cliente()
            Dim oBLEquivalencia As New BLListaEquivalencia
            Dim NroDocumento As String 'Para pruebas usar 123456
            Dim IDTipoDocumento As String '= hfIDTipoDocumento.Value
            Dim oBECer As New BECertificado

            oBECer = DirectCast(Session(Constantes.SS_APP_BECER), BECertificado)
            IDTipoDocumento = IIf(IsMock(), TipoDocumentoMockDefault(), oBLEquivalencia.getValorEquivalencia(CInt(IDPRODUCTO), "TipoDocumento", oBECer.IdTipoDocumento).ValorEquivalencia)
            NroDocumento = IIf(IsMock(), NroDocumentoMockDefault(), lblNroDocumento.Text.Trim)
            'ClientITAU = SrvInvoker.GetFullClient(IDTipoDocumento, NroDocumento, IIf(hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_DEV, 0, IDPRODUCTO))
            ClientITAU = SrvInvoker.GetFullClient(IDTipoDocumento, NroDocumento, 0)
            If ClientITAU.EstadoRespuesta = WSInvoker.Constant.Constant.RESPONSE_OK Then
                If ClientITAU.Codigo <> String.Empty Then
                    gvCuentaTarjeta.DataSource = ClientITAU.MediosDePago
                Else
                    gvCuentaTarjeta.DataSource = New List(Of WSInvoker.Model.MedioPago)
                End If
                gvCuentaTarjeta.DataBind()
                gvCuentaTarjeta.SelectedIndex = -1
            Else
                gvCuentaTarjeta.DataSource = New List(Of WSInvoker.Model.MedioPago)
                gvCuentaTarjeta.DataBind()
                gvCuentaTarjeta.SelectedIndex = -1
                ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('No se encontró ningún cliente para el tipo y número de documento ingresado.');", True)
            End If
        Catch ex As Exception
            gvCuentaTarjeta.DataSource = New List(Of WSInvoker.Model.MedioPago)
            gvCuentaTarjeta.DataBind()
            gvCuentaTarjeta.SelectedIndex = -1
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('Error de inconsistencia de datos.');", True)
        End Try
    End Sub

    Protected Sub BCbtnAceptar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BCbtnAceptar.Click
        If gvCuentaTarjeta.SelectedIndex = -1 Then
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('No ha seleccionado el medio de pago');", True)
            Exit Sub
        End If
        Try
            If hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_CMP Then
                hfRaiz.Value = DirectCast(gvCuentaTarjeta.SelectedRow.Cells(5).Controls(1), Label).Text
                ddlMedioPago.SelectedValue = DirectCast(gvCuentaTarjeta.SelectedRow.Cells(1).Controls(1), Label).Text
                txtNumero.Text = DirectCast(gvCuentaTarjeta.SelectedRow.Cells(4).Controls(1), Label).Text
                txtCBU.Text = DirectCast(gvCuentaTarjeta.SelectedRow.Cells(6).Controls(1), Label).Text
            Else
                If hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_DEV Then
                    hfRaiz.Value = DirectCast(gvCuentaTarjeta.SelectedRow.Cells(5).Controls(1), Label).Text
                    ddlMedioPagoDev.SelectedValue = DirectCast(gvCuentaTarjeta.SelectedRow.Cells(1).Controls(1), Label).Text
                    txtNumeroDev.Text = DirectCast(gvCuentaTarjeta.SelectedRow.Cells(4).Controls(1), Label).Text
                    txtCBUDev.Text = DirectCast(gvCuentaTarjeta.SelectedRow.Cells(6).Controls(1), Label).Text
                End If
            End If
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('No se pudo seleccionar el medio de pago.');", True)
        End Try
        ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "AC", "$.modal.close();", True)
    End Sub


    Protected Sub ddlMotivo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlMotivo.SelectedIndexChanged

        Try
            If ddlMotivo.SelectedValue <> "0" Then
                SelTipoMotivoReclamo(CInt(ddlMotivo.SelectedValue))
            Else
                pCambioMedioPago.Visible = False
                pDevolucion.Visible = False
            End If
        Catch ex As Exception
            Dim s As String = ex.Message
        End Try

    End Sub

    Private Sub SelTipoMotivoReclamo(ByVal IDMotivoReclamo As Integer)
        Dim lstMedioPago As New ArrayList
        Dim oBLMReclamo As New BLMontivoReclamo

        Try
            hfIDTipoMotivoReclamo.Value = oBLMReclamo.GetTipoMotivoReclamo(ddlMotivo.SelectedValue)
            If Not String.IsNullOrWhiteSpace(hfIDTipoMotivoReclamo.Value) And hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_CMP Then
                pCambioMedioPago.Visible = True
                ResetPanelDevolucion()
                lstMedioPago = ListaGenerica(CInt(hfIDProducto.Value), "MedioPago")

                ddlMedioPago.Items.Clear()
                ddlMedioPago.Items.Add(New ListItem("-Seleccionar-", "0"))
                ddlMedioPago.DataSource = lstMedioPago
                ddlMedioPago.DataValueField = "IDMedioPago"
                ddlMedioPago.DataTextField = "Nombre"
                ddlMedioPago.DataBind()
                ddlMedioPago.SelectedValue = "0"

                ddlMedioPagoActual.Items.Clear()
                ddlMedioPagoActual.DataSource = lstMedioPago
                ddlMedioPagoActual.DataValueField = "IDMedioPago"
                ddlMedioPagoActual.DataTextField = "Nombre"
                ddlMedioPagoActual.DataBind()
                CargarDatosCuenta(txtNumeroActual, txtCBUActual, ddlMedioPagoActual)
                ChangeColorControlMP(txtNumero, False)
                ChangeColorControlMP(txtCBU, False)
                ChangeColorControlMP(ddlMedioPago, False)
            Else
                If Not String.IsNullOrWhiteSpace(hfIDTipoMotivoReclamo.Value) And hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_DEV Then
                    lstMedioPago = ListaGenerica(CInt(hfIDProducto.Value), "MedioPago")
                    ddlMedioPagoDev.Items.Clear()
                    ddlMedioPagoDev.Items.Add(New ListItem("-Seleccionar-", "0"))
                    ddlMedioPagoDev.DataSource = lstMedioPago
                    ddlMedioPagoDev.DataValueField = "IDMedioPago"
                    ddlMedioPagoDev.DataTextField = "Nombre"
                    ddlMedioPagoDev.DataBind()
                    pDevolucion.Visible = True
                    ResetPanelMedioPago()

                    Dim oBERec As New BEReclamo
                    oBERec = DirectCast(Session(Constantes.SS_APP_BEREC), BEReclamo)
                    chkCtaExterna.Checked = oBERec.CuentaExterna
                    ControlDevoluciones(True)
                    CargarDatosCuenta(txtNumeroDev, txtCBUDev, ddlMedioPagoDev)

                Else
                    pDevolucion.Visible = False
                    pCambioMedioPago.Visible = False
                End If
            End If
        Catch ex As Exception
            Dim s As String = ex.Message
        End Try
    End Sub

    Private Sub ResetPanelMedioPago()
        pCambioMedioPago.Visible = False
        txtCBU.Text = String.Empty
        txtNumero.Text = String.Empty
    End Sub

    Private Sub ResetPanelDevolucion()
        pDevolucion.Visible = False
        chkCtaExterna.Checked = False
        txtNumeroDev.Text = String.Empty
    End Sub

    Private Function ListaGenerica(ByVal pIDProducto As Integer, ByVal pTabla As String) As ArrayList
        'Dim oBL As New BLListaGenerica
        'Dim r As New ArrayList
        'Return oBL.Seleccionar(pIDProducto, pTabla)
        Dim oBL As New BLMedioPago

        Return oBL.SeleccionarPermitidos()
    End Function

    Protected Sub chkCtaExterna_CheckedChanged(sender As Object, e As EventArgs) Handles chkCtaExterna.CheckedChanged
        txtNumeroDev.Text = String.Empty
        txtCBUDev.Text = String.Empty
        CargarDatosCuentaCheck()
    End Sub

    Protected Sub CargarDatosCuentaCheck()
        If chkCtaExterna.Checked Then
            'Selecciona la cuenta manualmente
            ControlDevoluciones(True)
            CuentaDevolucion()

            Dim oBERec As New BEReclamo
            oBERec = DirectCast(Session(Constantes.SS_APP_BEREC), BEReclamo)

            If oBERec.IDMotivoReclamo <> ddlMotivo.SelectedValue Or chkCtaExterna.Checked <> oBERec.CuentaExterna Then
                txtNumeroDev.Text = String.Empty
            End If

            If String.IsNullOrEmpty(oBERec.IDMedioPago) Then
                ddlMedioPagoDev.SelectedValue = Constantes.ID_CUENTA_CORRIENTE
            Else
                If chkCtaExterna.Checked = oBERec.CuentaExterna Then
                    ddlMedioPagoDev.SelectedValue = oBERec.IDMedioPago
                    txtNumeroDev.Text = oBERec.MedioDevolucion
                Else
                    txtNumeroDev.Text = String.Empty
                    ddlMedioPagoDev.SelectedValue = "0"
                End If
            End If
        Else
                'Selecciona la cuenta desde los servicios de ITAU
                ControlDevoluciones(False)
                CargarDatosCuenta(txtNumeroDev, txtCBUDev, ddlMedioPagoDev)
        End If
    End Sub

    Private Sub CargarDatosCuenta(ByVal txtNro As TextBox, ByVal txtCBU As TextBox, ByVal cboMP As DropDownList)
        Dim oBECer As New BECertificado
        Dim oBERec As New BEReclamo
        Dim IDmedioPago As String = "0"

        oBERec = DirectCast(Session(Constantes.SS_APP_BEREC), BEReclamo)
        oBECer = DirectCast(Session(Constantes.SS_APP_BECER), BECertificado)
        If Not chkCtaExterna.Checked And Not hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_DEV And Not oBERec.CuentaExterna Then
            txtNro.Text = oBECer.NumeroCuenta
            IDmedioPago = oBECer.IdMedioPago.ToString.ToUpper()
            txtCBU.Text = oBECer.CBU
        Else
            If hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_DEV Then
                If chkCtaExterna.Checked And chkCtaExterna.Checked = oBERec.CuentaExterna Then
                    txtNro.Text = oBERec.MedioDevolucion
                    IDmedioPago = oBERec.IDMedioPago
                Else
                    If Not chkCtaExterna.Checked And chkCtaExterna.Checked = oBERec.CuentaExterna Then
                        txtNro.Text = IIf(Not String.IsNullOrEmpty(oBERec.MedioDevolucion), oBERec.MedioDevolucion, oBECer.NumeroCuenta)
                        IDmedioPago = IIf(Not String.IsNullOrEmpty(oBERec.IDMedioPago) And oBERec.IDMedioPago <> "0", oBERec.IDMedioPago, oBECer.IdMedioPago.ToString.ToUpper())
                        txtCBU.Text = IIf(Not String.IsNullOrEmpty(oBERec.CBU), oBERec.CBU, oBECer.CBU)
                    Else
                        txtNro.Text = String.Empty
                        IDmedioPago = "0"
                    End If
                End If
            Else
                txtNro.Text = oBECer.NumeroCuenta
                IDmedioPago = oBECer.IdMedioPago.ToString.ToUpper()
                txtCBU.Text = oBECer.CBU
            End If
        End If

        If (Not cboMP.Items.FindByValue(IDmedioPago.ToString.ToUpper()) Is Nothing) Then
            cboMP.SelectedValue = IDmedioPago
        Else
            cboMP.SelectedValue = "0"
        End If

    End Sub


    Private Sub ControlDevoluciones(ByVal estado As Boolean)


        If chkCtaExterna.Checked Then
            txtCBUDev.Visible = False
            lb_cbuDev.Visible = False
            iBtnVerCobroSelDev.Visible = False
            txtNumeroDev.Enabled = True
            txtNumeroDev.ReadOnly = False
            txtCBUDev.Enabled = True
            txtCBUDev.ReadOnly = False
            ChangeColorControlMP(txtNumeroDev, IIf(btnModificar.Enabled, False, True))
            ChangeColorControlMP(ddlMedioPagoDev, IIf(btnModificar.Enabled, False, True))
        Else
            txtCBUDev.Visible = True
            lb_cbuDev.Visible = True
            iBtnVerCobroSelDev.Visible = True
            txtNumeroDev.Enabled = False
            txtNumeroDev.ReadOnly = True
            txtCBUDev.Enabled = False
            txtCBUDev.ReadOnly = True
            ChangeColorControlMP(txtNumeroDev, True)
            ChangeColorControlMP(ddlMedioPagoDev, True)
        End If

        ddlMedioPagoDev.Enabled = Not iBtnVerCobroSelDev.Visible


    End Sub

    Protected Sub ddlMedioPagoDev_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlMedioPagoDev.SelectedIndexChanged
        If chkCtaExterna.Checked Then
            CuentaDevolucion()
        End If
    End Sub

    Private Sub CuentaDevolucion()
        Try
            txtNumeroDev.Enabled = True
            txtNumeroDev.ReadOnly = False

            txtCBUDev.Enabled = False
            txtCBUDev.ReadOnly = True

        Catch ex As Exception
            Dim s As String = ex.Message
        End Try
    End Sub
End Class