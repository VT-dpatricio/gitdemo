﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Login.aspx.vb" Inherits="VANSiniestros.Login1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
 <meta id="FirstCtrlID" http-equiv="X-UA-Compatible" content="IE=8" />
    <title>Federación Patronal Seguros SA - Atención de Siniestros</title>
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
   
    <asp:ScriptManager ID="sm" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="upLogin" runat="server">
        <ContentTemplate>
            <asp:MultiView ID="mvLogin" runat="server" ActiveViewIndex="0">
                <asp:View ID="vFrmLogin" runat="server">
                    <table  cellspacing="0" cellpadding="0" border="0" class="frmLogin">
	                    <tbody>
	                    <tr>
                            <td class="LEncaSup"></td></tr><tr>
                            <td class="LEncaCen">
			                    <br />
			                    <br />
			                    <table style="margin: auto; width: 416px" cellspacing="7" cellpadding="0" border="0">
                                        <tbody>
						                    <tr>
							                    <td style="text-align: right; height: 34px;" colspan=2>
                                                    <img alt="" src="img/Login/LAcceso.png" title="Sistema de Siniestros" />
                                                </td>
							                    <td style="height: 34px"></td>
						                    </tr>
                                            <tr>
							                    <td style="width: 83px; " class="LEtiquetaC">Usuario:</td>
							                    <td style="width: 272px;" class="txtLoginBorde">							
							                        <asp:TextBox ID="txtUsuario" runat="server" SkinID="txtLogin" Width="256px"></asp:TextBox>
							                    </td>
							                    <td>
							                        <asp:RequiredFieldValidator ID="rfvUsuario" runat="server" 
                                                        ControlToValidate="txtUsuario" ErrorMessage="Campo Requerido" 
                                                        ForeColor="#C00000" ToolTip="Campo Requerido">*</asp:RequiredFieldValidator>
							                    </td>
						                    </tr>
                                            <tr>
							                    <td class="LEtiquetaC" style="width: 83px">Contraseña:</td>
							                    <td style="width: 272px" class="txtLoginBorde">
								                    <asp:TextBox ID="txtClave" runat="server" SkinID="txtLogin" Width="256px" 
                                                        TextMode="Password"></asp:TextBox>
							                    </td>
							                    <td>
                                                    <asp:RequiredFieldValidator ID="rfvClave" runat="server" 
                                                        ControlToValidate="txtClave" ErrorMessage="Campo Requerido" ForeColor="#C00000" 
                                                        ToolTip="Campo Requerido">*</asp:RequiredFieldValidator>
							                    </td>
						                    </tr>
                                            <tr>
							                    <td style="text-align: right;" colspan="2">
							                        &nbsp;<asp:Label ID="lblMsg" runat="server" ForeColor="#C00000"></asp:Label>
                                                    &nbsp;<asp:ImageButton ID="btnEntrar" runat="server" 
                                                        ImageUrl="~/img/Login/btnIngresar.png" />
                                                    </td>
                                                <td>
							                    </td>
						                    </tr>
					                    </tbody>
				                    </table>
                                    <br />
		                        <asp:UpdateProgress ID="uproLogin" runat="server" DisplayAfter="0">
                                    <ProgressTemplate>
                                       <div style="width: 171px; height: 39px; margin:auto; background-color: #ffffff;">
                                <table style="width: 169px">
                                    <tr>
                                        <td style="width: 38px">
                                            <img alt="" src="img/wait.gif" />
                                        </td>
                                        <td colspan="2" style="width: 157px">
                                            Validando Usuario...</td>
                                    </tr>
                                </table>
                            </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                &nbsp;&nbsp;
		
                                <asp:HiddenField ID="HFIntentos" runat="server" Value="0" />
		
                            <br />
		                    <br />
		                    </td>
		                    </tr>
                            <tr>
			                    <td class="LEncaInf"></td></tr>
		                    </tbody>
                    </table>
                </asp:View>
                <asp:View ID="vMsg" runat="server">
                    <div style="width: 254px; height: 138px; margin:auto; border-right: #cc9900 1px solid; border-top: #cc9900 1px solid; border-left: #cc9900 1px solid; border-bottom: #cc9900 1px solid; background-color: #ffffff; color: #ff3333; text-align: center;">
                            <br />
                            <br />
                            <br />
                            Usted a superado el número de Intentos para el inicio de sesión<br />
                            Por favor contacte con el Administrador</div>
                </asp:View>
                                   <asp:View ID="View3" runat="server">

 <table style="MARGIN: auto; WIDTH: 617px" cellSpacing="0" cellPadding="0" border="0">
 
         <tr>
             <td class="LEncaSup">
             </td>
         </tr>
             <tr>
                 <td class="LEncaCen">
                     <BR />
                     <BR />
                     <table border="0" cellpadding="0" cellspacing="7" 
                         style="MARGIN: auto; WIDTH: 416px; height: 167px;">
                         <tr>
                             <td style="text-align: center;" class="style2">
                                 <asp:Image ID="imgMsg" runat="server" ImageUrl="~/Img/Icons/denegado.png" />
                                 <br />
                                 <br />
                                 <asp:Label ID="lblMensaje" runat="server" Font-Size="12px" ForeColor="#C00000">Acceso denegado</asp:Label>
                             </td>
                         </tr>
                     </table>
                     <BR />
                     
                     <BR />
                     <BR />
                 </td>
             </tr>
          <tr>
                     <td class="LEncaInf">
                     </td>
                 </tr>
             
</table>
                    </asp:View>
            </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>
   
    </form>
</body>
</html>
