﻿Imports VAN.Siniestros.BL
Imports Microsoft.Reporting.WebForms



Public Class rptReclamos
    Inherits PaginaBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            If ValidarEstadoClaveUsuario() Then
                Response.Redirect(Constantes.CHANGE_PSW_PAGE, False)
            End If

            txtFDesde.Text = Now.AddDays(-30).ToString("dd/MM/yyyy")
            txtFHasta.Text = Now.ToString("dd/MM/yyyy")
            Dim oBLEntidad As New BLEntidad
            Dim IDUsuario As String = User.Identity.Name
            ddlEntidad.DataSource = oBLEntidad.Seleccionar(IDUsuario)
            ddlEntidad.DataTextField = "Descripcion"
            ddlEntidad.DataValueField = "IDEntidad"
            ddlEntidad.DataBind()

            Dim oBL As New BLEstadoReclamo
            'ddlEstado.Items.Add(New ListItem("-Seleccionar-", "0"))
            ddlEstado.DataSource = oBL.Listar()
            ddlEstado.DataValueField = "IDEstadoReclamo"
            ddlEstado.DataTextField = "Nombre"
            ddlEstado.DataBind()

            'CargarEntidadxUsuario(ddlEntidad)
            'CargarProductoxUsuario(lbProducto, CInt(ddlEntidad.SelectedValue))
            'RptV.CurrentPage = 1
        End If

        Dim js As New HtmlGenericControl("script")
        js.Attributes("type") = "text/javascript"
        js.Attributes("src") = "http://AONATC/Scripts/ui.core.js"
        Page.Header.Controls.Add(js)
        Dim js2 As New HtmlGenericControl("script")
        js2.Attributes("type") = "text/javascript"
        js2.Attributes("src") = "http://AONATC/Scripts/ui.dropdownchecklist.js"
        Page.Header.Controls.Add(js2)
    End Sub


    Public Sub CargarProductoxUsuario()
        Dim IDUsuario As String = User.Identity.Name
        Dim oBLProducto As New BLProducto
        lbProducto.Items.Clear()
        lbProducto.Items.Add(New ListItem("(Todos)", "0"))
        lbProducto.DataSource = oBLProducto.Seleccionar(CInt(ddlEntidad.SelectedValue), IDUsuario)
        lbProducto.DataTextField = "Descripcion"
        lbProducto.DataValueField = "IDProducto"
        lbProducto.DataBind()
        lbProducto.Visible = True
    End Sub

    Protected Sub ddlEntidad_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlEntidad.SelectedIndexChanged
        CargarProductoxUsuario()
        '------------------------      
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "", "Load();", True)
    End Sub

    Protected Sub BtnVerReporte_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnVerReporte.Click
        Dim rpt As String = "rptReclamoConsolidado"
        Dim IDProducto As String = "0"

        Dim ServidorRS As String = ConfigurationManager.AppSettings("ReportServerUri").ToString()
        Dim ServidorDirApp As String = ConfigurationManager.AppSettings("ServidorRSDirApp").ToString()

        ReportAunthentication.ConfigureAuthentication(RptV)


        RptV.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote
        RptV.ServerReport.ReportServerUrl = New Uri(ServidorRS)
        RptV.ServerReport.ReportPath = ServidorDirApp + rpt


        Dim paramList As Generic.List(Of ReportParameter) = New Generic.List(Of ReportParameter)()
        paramList.Add(New ReportParameter("FechaDesde", txtFDesde.Text))
        paramList.Add(New ReportParameter("FechaHasta", txtFHasta.Text))
        paramList.Add(New ReportParameter("IDProducto", CType(getDatos(lbProducto).ToArray(GetType(String)), String())))
        paramList.Add(New ReportParameter("IDEstadoReclamo", CInt(ddlEstado.SelectedValue)))
        RptV.ServerReport.SetParameters(paramList)
        RptV.ShowParameterPrompts = False

    End Sub

    Public Function getDatos(ByVal lb As ListBox) As ArrayList
        Dim intCount As Integer
        Dim sTer As String = ""
        Dim lista As New ArrayList()
        For intCount = 0 To lb.Items.Count - 1 Step intCount + 1
            If (lb.Items(intCount).Selected = True) Then
                If (lb.Items(intCount).Value <> "0") Then
                    lista.Add(lb.Items(intCount).Value)
                End If
            End If
        Next
        Return lista
    End Function
End Class