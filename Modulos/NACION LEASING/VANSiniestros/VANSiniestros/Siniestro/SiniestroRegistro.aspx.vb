﻿Imports VAN.Siniestros.BE
Imports VAN.Siniestros.BL
Public Class RegistroSiniestro
    Inherits PaginaBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            If ValidarEstadoClaveUsuario() Then
                Response.Redirect(Constantes.CHANGE_PSW_PAGE, False)
            End If

            cargarEntidad()
            txtMontoDenunciado.Text = "0"
            'cargarMoneda(5113)
            'ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "TabEnabled", "TabActive(false);", True)
        End If
    End Sub


    Private Sub cargarEntidad()
        Dim oBLEntidad As New BLEntidad
        Dim IDUsuario As String = User.Identity.Name
        ddlEntidad.DataSource = oBLEntidad.Seleccionar(IDUsuario)
        ddlEntidad.DataTextField = "Descripcion"
        ddlEntidad.DataValueField = "IDEntidad"
        ddlEntidad.DataBind()
    End Sub

    Private Sub cargarTipoSiniestro(ByVal pIDProducto As Int32, ByVal pOpcion As String)
        Dim oBL As New BLTipoSiniestro
        ddlTipoSiniestro.Items.Clear()
        ddlTipoSiniestro.Items.Add(New ListItem("---Seleccionar---", "0"))
        ddlTipoSiniestro.DataSource = oBL.Listar(pIDProducto, pOpcion)
        ddlTipoSiniestro.DataValueField = "IDTipoSiniestro"
        ddlTipoSiniestro.DataTextField = "Nombre"
        ddlTipoSiniestro.DataBind()
    End Sub

    Private Sub cargarMoneda(ByVal pIDProducto As Int32)
        Dim oBL As New BLMoneda
        ddlMonedaDe.DataSource = oBL.Listar(pIDProducto)
        ddlMonedaDe.DataValueField = "IDMoneda"
        ddlMonedaDe.DataTextField = "IDMoneda"
        ddlMonedaDe.DataBind()

    End Sub


    Protected Sub ddlBuscarPor_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlBuscarPor.SelectedIndexChanged
        Select Case ddlBuscarPor.SelectedValue
            Case "ApeNom"
                lblPar1.Text = "Apellidos:"
                lblPar2.Visible = True
                txtPar2.Visible = True
            Case Else
                lblPar2.Visible = False
                txtPar2.Visible = False
                lblPar1.Text = ddlBuscarPor.SelectedItem.Text & ":"
        End Select
    End Sub

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click
        BuscarCertificado()
    End Sub

    Private Sub BuscarCertificado()
        Dim oBL As New BLCertificado
        Dim IDUsuario As String = User.Identity.Name
        gvListaCertificado.DataSource = oBL.Buscar(CInt(ddlEntidad.SelectedValue), IDUsuario, ddlBuscarPor.SelectedValue, txtPar1.Text.Trim, txtPar2.Text.Trim)
        gvListaCertificado.DataBind()
        gvListaCertificado.SelectedIndex = -1
    End Sub

    Protected Sub gvListaCertificado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListaCertificado.PageIndexChanging
        gvListaCertificado.PageIndex = e.NewPageIndex
        BuscarCertificado()
    End Sub

    Protected Sub gvListaCertificado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gvListaCertificado.SelectedIndexChanged
        ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "TabActRS", "ActTabRegistroSiniestro();", True)
        ' ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "TabActRS", "$get('" & btnActRegSiniestro.ClientID & "').click();", True)
    End Sub

    Protected Sub ddlTipoSiniestro_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTipoSiniestro.SelectedIndexChanged
        CargarCobertura()
        CargarGrillaArchivos(0)
    End Sub

    Private Sub CargarCobertura()
        If ddlTipoSiniestro.SelectedValue = 0 Then
            gvCobertura.DataSource = Nothing
            gvCobertura.Visible = False
            lblMsgSelTipoSiniestro.Visible = True
            CargarGridDocumento("0")
        Else
            Dim oBL As New BLCobertura
            gvCobertura.DataSource = oBL.Listar(CInt(ddlTipoSiniestro.SelectedValue), CInt(hfIDProducto.Value), lblOpcion.Text)
            gvCobertura.Visible = True
            lblMsgSelTipoSiniestro.Visible = False
        End If
        gvCobertura.DataBind()
    End Sub

    Protected Sub btnGrabar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGrabar.Click
        Dim oBL As New BLSiniestro
        Dim oBE As New BESiniestro
        oBE.IDCertificado = hfIDCertificado.Value
        oBE.IDTipoSiniestro = CInt(ddlTipoSiniestro.SelectedValue)
        oBE.FechaSiniestro = CDate(txtFechaSiniestro.Text)

        If txtMontoDenunciado.Text = "" Then
            txtMontoDenunciado.Text = "0"
        Else
            oBE.MontoSiniestro = CDec(txtMontoDenunciado.Text)
        End If
        'oBE.MontoSiniestro = IIf(txtMontoDenunciado.Text = "", 0, CDec(txtMontoDenunciado.Text))
        oBE.IDMonedaMSiniestro = ddlMonedaDe.SelectedValue
        oBE.AseEmail = txtAseEmail.Text.Trim
        oBE.AseTelefono = txtAseTelefono.Text.Trim
        oBE.AseDireccion = txtAseDireccion.Text.Trim
        oBE.AseObservacion = txtAseObservacion.Text.Trim
        oBE.UsuarioCreacion = User.Identity.Name
        oBE.NroSiniestroAseguradora = ""
        'Coberturas Seleccionadas
        Dim ListaCobertura As New ArrayList
        For i As Integer = 0 To gvCobertura.Rows.Count - 1
            Dim Sel = DirectCast(gvCobertura.Rows.Item(i).Cells.Item(0).Controls.Item(0), CheckBox).Checked
            If Sel Then
                'oBESinCober.IDCobertura = CInt(gvListaCertificado.SelectedDataKey.Item(0))
                ListaCobertura.Add(CInt(gvCobertura.DataKeys(i).Item(0)))
            End If
        Next
        If ListaCobertura.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "MsgError", "alert('No ha seleccionado ninguna cobertura');", True)
            Exit Sub
        End If

        oBE.ListaCobertura = ListaCobertura
        oBE.ListaArchivos = IIf(Not Session("lsDocumentos") Is Nothing, DirectCast(Session("lsDocumentos"), ArrayList), New ArrayList)
        oBE.ListaCoberturaDocumento = IIf(Not Session("lsCoberturaDocumentos") Is Nothing, DirectCast(Session("lsCoberturaDocumentos"), ArrayList), New ArrayList)

        Dim pIDSiniestro = oBL.Insertar(oBE)
        If (pIDSiniestro <> 0) Then
            'txtNroTicket.Text = pIDSiniestro.ToString("000000")
            txtNroTicket.Text = Date.Now().ToString("yyyyMMdd") & "-" & pIDSiniestro.ToString("000000")
            EnabledControles(False)
            ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "MsgError", "alert('Siniestro registrado con éxito con el N° de Ticket: " & txtNroTicket.Text & "');", True)
        Else
            ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "MsgError", "alert('Ocurrió un error al grabar. Consulte con el Administrador del Sistema');", True)
        End If

        Session("lsDocumentos") = Nothing
        Session("lsCoberturaDocumentos") = Nothing
        For i As Integer = 0 To gvDocumento.Rows.Count - 1
            DirectCast(gvDocumento.Rows.Item(i).Cells(3).Controls.Item(1), CheckBox).Enabled = False
            DirectCast(Me.gvDocumento.Rows.Item(i).Cells.Item(4).Controls.Item(1), ImageButton).Enabled = False
        Next

        For i As Integer = 0 To gvRegDoc.Rows.Count - 1
            DirectCast(Me.gvRegDoc.Rows.Item(i).Cells.Item(4).Controls.Item(1), ImageButton).Enabled = False
            Me.gvRegDoc.Columns.Item(3).Visible = False
        Next

        'ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "TabActRS", "alert('" & txtAseDireccion.Text & ");", True)
    End Sub

    Protected Sub IBCancear_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Me.gvDocumento.EditIndex = -1
        Me.gvDocumento.SelectedIndex = -1
        CargarGridDocumento(Constantes.TAG_LIST_TEMP)
    End Sub

    Private Function SynchronizeData(ByVal arrNewDataList As ArrayList, ByVal arrOldDataList As ArrayList) As ArrayList
        If arrNewDataList.Count = 0 OrElse arrOldDataList.Count = 0 Then
            Return arrNewDataList
        End If

        For Each oBESiDocNew As BESiniestroDocumento In arrNewDataList
            For Each oBESiDocOld As BESiniestroDocumento In arrOldDataList
                If (oBESiDocNew.IDDocumento = oBESiDocOld.IDDocumento) Then
                    oBESiDocNew.FechaEntrega = oBESiDocOld.FechaEntrega
                    oBESiDocNew.Entregado = oBESiDocOld.Entregado
                End If
            Next
        Next
        Return arrNewDataList
    End Function

    Private Sub RenameUplodadFile(ByVal lsArchivos As ArrayList, ByVal IDSiniestro As Int32)
        'Dim temp As String = ""
        'Dim index = 1
        'For Each oBEArch As BEArchivo In lsArchivos
        '    Dim newPath As String = ""
        '    temp = oBEArch.RutaArchivo.Substring(oBEArch.RutaArchivo.LastIndexOf("/") + 1)
        '    temp = temp.Substring(temp.IndexOf("_"))
        '    newPath = oBEArch.RutaArchivo.Substring(0, oBEArch.RutaArchivo.LastIndexOf("/") + 1) + IDSiniestro.ToString() + temp
        '    IO.File.Copy(oBEArch.RutaArchivo, newPath)
        '    DeleteUplodadFile(lsArchivos, index)
        '    index = index + 1
        'Next
    End Sub

    Private Sub DeleteUplodadFile(ByVal lsArchivos As ArrayList, ByVal IDArchivo As Int32)
        Try
            Dim obj As BEArchivo = FindArchivoById(lsArchivos, IDArchivo)
            IO.File.Delete(obj.RutaArchivo + obj.NombreArchivo)
        Catch ex As Exception
        End Try
    End Sub

    Private Function FindArchivoById(ByVal lsArchivos As ArrayList, ByVal IDArchivo As Int32) As BEArchivo
        Dim obj As BEArchivo = Nothing
        For Each oBEArch As BEArchivo In lsArchivos
            If oBEArch.IdArchivo = IDArchivo Then
                obj = oBEArch
                Return obj
            End If
        Next
        Return obj
    End Function


    Private Sub LimpiarControles()
        hfIDCertificado.Value = 0
        lblNroCertificado.Text = String.Empty
        lblProducto.Text = String.Empty
        lblCanal.Text = String.Empty
        lblTipoDocumento.Text = String.Empty
        lblNroDocumento.Text = String.Empty
        lblAsegurado.Text = String.Empty
        txtAseDireccion.Text = String.Empty
        txtAseTelefono.Text = String.Empty
        txtAseEmail.Text = String.Empty
        lblCanal.Text = String.Empty
        lblAseguradora.Text = String.Empty

        txtFechaSiniestro.Text = String.Empty
        txtMontoDenunciado.Text = String.Empty
        txtAseObservacion.Text = String.Empty
        txtNroTicket.Text = "-"

        lblOpcion.Text = String.Empty
        lblMonedaPrima.Text = String.Empty
        lblEstadoCer.Text = String.Empty

    End Sub

    Private Sub EnabledControles(ByVal pEstado As Boolean)
        txtAseDireccion.Enabled = pEstado
        txtAseTelefono.Enabled = pEstado
        txtAseEmail.Enabled = pEstado
        ddlTipoSiniestro.Enabled = pEstado
        gvCobertura.Enabled = pEstado
        txtFechaSiniestro.Enabled = pEstado
        txtMontoDenunciado.Enabled = pEstado
        txtAseObservacion.Enabled = pEstado
        ddlMonedaDe.Enabled = pEstado
        btnGrabar.Visible = pEstado
        btnNuevo.Visible = Not pEstado
    End Sub

    Protected Sub btnActRegSiniestro_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnActRegSiniestro.Click
        LimpiarControles()
        EnabledControles(True)

        Dim oBL As New BLCertificado
        Dim oBE As BECertificado = oBL.Seleccionar(gvListaCertificado.SelectedDataKey.Item(0).ToString)

        cargarTipoSiniestro(oBE.IdProducto, oBE.Opcion)
        cargarMoneda(oBE.IdProducto)
        ddlTipoSiniestro.SelectedValue = 0
        CargarCobertura()
        CargarGrillaArchivos(0)
        hfIDCertificado.Value = oBE.IdCertificado
        hfIDProducto.Value = oBE.IdProducto
        lblNroCertificado.Text = oBE.IdCertificado
        lblProducto.Text = oBE.Producto
        lblCanal.Text = oBE.Oficina
        lblTipoDocumento.Text = oBE.TipoDocumento
        lblNroDocumento.Text = oBE.CcCliente
        lblAsegurado.Text = oBE.Nombre1 & " " & oBE.Nombre2 & " " & oBE.Apellido1 & " " & oBE.Apellido2
        txtAseDireccion.Text = oBE.Direccion '& oBE.Apellido1
        txtAseTelefono.Text = oBE.Telefono
        txtAseEmail.Text = oBE.Email
        lblCanal.Text = oBE.Entidad
        lblAseguradora.Text = oBE.Aseguradora

        lblOpcion.Text = oBE.Opcion
        lblMonedaPrima.Text = oBE.IdMonedaPrima
        lblEstadoCer.Text = oBE.Estado
        Session("lsDocumentos") = Nothing
        Session("lsCoberturaDocumentos") = Nothing
        EnabledControles(True)
    End Sub

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNuevo.Click
        LimpiarControles()
        EnabledControles(True)
        ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "TabActBS", "ActTabBusquedaCliente();", True)
    End Sub

    Protected Sub chkSelCobertura_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        CargarGridDocumento(CoberturaID())
        'DirectCast(sender, CheckBox).Checked = False
        'ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "TabActRS", "alert('Mensaje');", True)
    End Sub

    Protected Function FormatDate(ByVal input As DateTime) As Object
        If (input = New DateTime(1900, 1, 1) Or input.Date.ToShortDateString = "01/01/0001") Then
            Return ""
        Else
            Return String.Format("{0:dd/MM/yyyy}", input)
        End If
    End Function

    Private Sub CargarGrillaArchivos(ByVal pIDSiniestro As String)
        Dim activeCell As Boolean = False
        Dim oBLArchivo As New BLArchivo
        Dim lista As ArrayList
        Try
            Me.gvRegDoc.Columns.Item(3).Visible = True
            lista = IIf(Not Session("lsDocumentos") Is Nothing, DirectCast(Session("lsDocumentos"), ArrayList), New ArrayList)
            gvRegDoc.DataSource = lista ' oBLArchivo.Seleccionar(pIDSiniestro)
            gvRegDoc.DataBind()
            gvRegDoc.Visible = True
        Catch ex As Exception
            Dim s As String = ex.Message
        End Try
        If gvRegDoc.Rows.Count > 0 Then
            activeCell = True
        End If

        For i = 0 To gvDocumento.Rows.Count - 1
            If gvRegDoc.Rows.Count = 0 Then
                DirectCast(gvDocumento.Rows.Item(i).Cells(3).Controls.Item(1), CheckBox).Checked = False
            End If
            DirectCast(gvDocumento.Rows.Item(i).Cells(3).Controls.Item(1), CheckBox).Enabled = activeCell
            DirectCast(Me.gvDocumento.Rows.Item(i).Cells.Item(4).Controls.Item(1), ImageButton).Enabled = activeCell
        Next

    End Sub

    Private Sub CargarGridDocumento(ByVal pID As String)
        Try

            If pID <> "0" Then
                Dim oBL As New BLSiniestroDocumento
                Dim lista As ArrayList
                Dim listaTemp As ArrayList
                listaTemp = IIf(Not Session("lsCoberturaDocumentos") Is Nothing, DirectCast(Session("lsCoberturaDocumentos"), ArrayList), New ArrayList)
                If (pID <> Constantes.TAG_LIST_TEMP) Then
                    lista = SynchronizeData(oBL.ListarPorCobertura(pID), listaTemp)
                    Session("lsCoberturaDocumentos") = lista
                Else
                    lista = listaTemp
                End If

                gvDocumento.DataSource = lista
                gvDocumento.Visible = True
                lblMsgSelCobertura.Visible = False
            Else
                gvDocumento.Visible = False
                gvDocumento.DataSource = Nothing
                lblMsgSelCobertura.Visible = True
            End If
            gvDocumento.DataBind()
        Catch ex As Exception
            RegisterLog(ex)
        End Try
    End Sub

    Private Function CoberturaID() As String
        Dim r As String = "0"
        Try
            For i As Integer = 0 To gvCobertura.Rows.Count - 1
                If DirectCast(gvCobertura.Rows.Item(i).Cells.Item(0).Controls.Item(0), CheckBox).Checked Then
                    r &= "," & gvCobertura.DataKeys(i).Item(0).ToString
                End If
            Next
        Catch ex As Exception
            RegisterLog(ex)
        End Try
        Return r
    End Function

    Protected Sub btn_RfArchivo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_RfArchivo.Click
        CargarGrillaArchivos(-1)
    End Sub

    Protected Sub btnEliminar_Command(ByVal sender As Object, ByVal e As CommandEventArgs)
        'Guarda el registro eliminado de la grilla de Archivos
        Dim oArc As New BLArchivo
        Dim pIDArchivo As Integer = Convert.ToInt32(e.CommandArgument)

        Dim lista As ArrayList = IIf(Not Session("lsDocumentos") Is Nothing, DirectCast(Session("lsDocumentos"), ArrayList), New ArrayList)
        If lista.Count > 0 Then
            DeleteUplodadFile(lista, pIDArchivo)
            lista.Remove(FindArchivoById(lista, pIDArchivo))
        End If

        Me.gvRegDoc.SelectedIndex = -1
        Me.gvRegDoc.EditIndex = -1
        CargarGrillaArchivos(0)
        ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "actArf", "ActElimArchivo();", True)
    End Sub


    Protected Sub IBGrabar_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Try
            Dim sinDoc As New BLSiniestroDocumento
            Dim pEndtidad As New BESiniestroDocumento
            Dim lista As ArrayList
            lista = IIf(Not Session("lsCoberturaDocumentos") Is Nothing, DirectCast(Session("lsCoberturaDocumentos"), ArrayList), New ArrayList)

            For i As Integer = 0 To gvDocumento.Rows.Count - 1
                For Each oBESiDoc As BESiniestroDocumento In lista
                    If (oBESiDoc.IDDocumento = CInt(gvDocumento.DataKeys(i).Item(0)) And DirectCast(gvDocumento.Rows.Item(i).Cells(3).Controls.Item(1), CheckBox).Checked) Then
                        Try
                            oBESiDoc.FechaEntrega = DirectCast(gvDocumento.Rows.Item(i).Cells(2).Controls.Item(1), TextBox).Text
                        Catch ex As Exception
                            oBESiDoc.FechaEntrega = DirectCast(gvDocumento.Rows.Item(i).Cells(2).Controls.Item(1), Label).Text
                        End Try
                        oBESiDoc.UsuarioCreacion = Session("IDUsuario")
                        oBESiDoc.Entregado = True
                    End If
                Next
            Next
            Session("lsCoberturaDocumentos") = lista
            Me.gvDocumento.EditIndex = -1
            Me.gvDocumento.SelectedIndex = -1
            CargarGridDocumento(Constantes.TAG_LIST_TEMP)

        Catch ex As Exception
            RegisterLog(ex)
        End Try
    End Sub

    Protected Sub gvDocumento_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvDocumento.RowEditing
        Me.gvDocumento.EditIndex = e.NewEditIndex
        CargarGridDocumento(Constantes.TAG_LIST_TEMP)
        DirectCast(gvDocumento.Rows.Item(e.NewEditIndex).Cells(3).Controls.Item(1), CheckBox).Checked = True
        If (Convert.ToDateTime(DirectCast(gvDocumento.Rows.Item(e.NewEditIndex).Cells(2).Controls.Item(1), TextBox).Text).ToShortDateString = "01/01/1900") Then
            DirectCast(gvDocumento.Rows.Item(e.NewEditIndex).Cells(2).Controls.Item(1), TextBox).Text = Date.Now.ToShortDateString()
        End If

    End Sub

    Protected Sub IBCancelar_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Me.gvDocumento.EditIndex = -1
        Me.gvDocumento.SelectedIndex = -1
        CargarGridDocumento(CoberturaID())
    End Sub

    Protected Sub btn_AllDoc_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_AllDoc.Click
        btn_refresDoc_Click(sender, e)
    End Sub


    Protected Sub btn_refresDoc_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_refresDoc.Click
        Try
            Dim id As Integer = CInt(idDocSin.Value)
            id = id - 1
            Dim fecha As DateTime
            fecha = Date.Now

            Dim lista As ArrayList
            lista = IIf(Not Session("lsCoberturaDocumentos") Is Nothing, DirectCast(Session("lsCoberturaDocumentos"), ArrayList), New ArrayList)

            For Each oBESiDoc As BESiniestroDocumento In lista
                If (oBESiDoc.IDDocumento = CInt(gvDocumento.DataKeys(id).Item(0))) Then
                    oBESiDoc.Entregado = DirectCast(gvDocumento.Rows.Item(id).Cells(3).Controls.Item(1), CheckBox).Checked
                    If oBESiDoc.Entregado Then
                        If oBESiDoc.FechaEntrega = New DateTime(1900, 1, 1) Then
                            oBESiDoc.FechaEntrega = fecha
                        End If
                    Else
                        oBESiDoc.FechaEntrega = New DateTime(1900, 1, 1)
                    End If
                    Exit For
                End If
            Next
            Session("lsCoberturaDocumentos") = lista
            Me.gvDocumento.EditIndex = -1
            Me.gvDocumento.SelectedIndex = -1
            CargarGridDocumento(Constantes.TAG_LIST_TEMP)

        Catch ex As Exception
            RegisterLog(ex)
        End Try
    End Sub

    Protected Sub gvDocumento_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvDocumento.RowDataBound

        Dim i As Integer = 0
        Dim activeCell = False

        If (e.Row.RowType = DataControlRowType.DataRow) Then
            If gvRegDoc.Rows.Count > 0 Then
                activeCell = True
            End If

            If (e.Row.RowState = 4 Or e.Row.RowState = 5) Then
                DirectCast(e.Row.Cells(3).Controls.Item(1), CheckBox).Enabled = False
                Exit Sub
            End If

            DirectCast(e.Row.Cells(3).Controls.Item(1), CheckBox).Attributes.Add("onclick", "ActCheck(" + (e.Row.RowIndex + 1).ToString() + ")")
            DirectCast(e.Row.Cells(3).Controls.Item(1), CheckBox).Enabled = activeCell
            DirectCast(e.Row.Cells(4).Controls.Item(1), ImageButton).Enabled = activeCell

        End If
    End Sub

    Private Sub RegisterLog(ByVal ex As Exception)

    End Sub
End Class