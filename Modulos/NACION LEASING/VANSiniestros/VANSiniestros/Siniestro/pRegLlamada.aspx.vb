﻿Imports System.IO
Imports VAN.Siniestros.BE
Imports VAN.Siniestros.BL
Public Class RegLlamada
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            hfIDSiniestro.Value = Int32.Parse(Request.QueryString("ID"))
        End If
    End Sub

    Protected Sub btnGrabar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGrabar.Click
        If Not IsDate(txtFecha.Text & " " & txtHora.Text & " " & ddlT.SelectedValue) Then
            ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "Msg", "alert('La Fecha y hora ingresada no es válida')", True)
            Exit Sub
        End If

        If (txtFecha.Text.Trim() = "" Or txtHora.Text.Trim() = "") Then
            ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "Msg", "alert('Ingrese la Fecha y hora')", True)
            Exit Sub
        End If

        Dim oBE As New BESiniestroLlamada
        Dim oBL As New BLSiniestroLlamada
        oBE.IDSiniestro = CInt(hfIDSiniestro.Value)
        oBE.FechaHora = CDate(txtFecha.Text & " " & txtHora.Text & " " & ddlT.SelectedValue)
        oBE.Observacion = txtObservacion.Text
        oBE.Adjunto = ""
        oBE.UsuarioCreacion = User.Identity.Name
        oBL.Insertar(oBE)

        ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "actL", "parent.ActgvLlamada();", True)
    End Sub
End Class