﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class SiniestroSeguimiento

    '''<summary>
    '''TabSiniestro control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TabSiniestro As Global.AjaxControlToolkit.TabContainer

    '''<summary>
    '''TabPanel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TabPanel1 As Global.AjaxControlToolkit.TabPanel

    '''<summary>
    '''upFiltroBuscar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents upFiltroBuscar As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''ddlEntidad control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlEntidad As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''pFiltroSeg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pFiltroSeg As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''ddlBuscarPor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlBuscarPor As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lblPar1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPar1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtPar1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPar1 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lblPar2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPar2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtPar2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPar2 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnBuscar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnBuscar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pFiltroEval control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pFiltroEval As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''ddlEstadoSiniestroF control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlEstadoSiniestroF As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''upListaCertificado control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents upListaCertificado As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''gvListaSiniestro control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvListaSiniestro As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''TabPanel2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TabPanel2 As Global.AjaxControlToolkit.TabPanel

    '''<summary>
    '''upRSiniestro control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents upRSiniestro As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''hfIDSiniestro control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfIDSiniestro As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfIDProducto control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfIDProducto As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''lblNroCertificado control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNroCertificado As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblProducto control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblProducto As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblOpcion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOpcion As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCanal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCanal As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAseguradora control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAseguradora As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTipoDocumento control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTipoDocumento As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblNroDocumento control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNroDocumento As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAsegurado control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAsegurado As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblMonedaPrima control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMonedaPrima As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblEstadoCer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEstadoCer As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''RequiredFieldValidator2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RequiredFieldValidator2 As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''CompareValidator1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CompareValidator1 As Global.System.Web.UI.WebControls.CompareValidator

    '''<summary>
    '''me control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents [me] As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''RequiredFieldValidator3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RequiredFieldValidator3 As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''RequiredFieldValidator4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RequiredFieldValidator4 As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''txtNroTicket control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtNroTicket As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtTipoSiniestro control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTipoSiniestro As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtFechaSiniestro control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFechaSiniestro As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtMontoDenunciado control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMontoDenunciado As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddlMonedaDe control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlMonedaDe As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtMontoPago control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMontoPago As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddlMonedaPago control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlMonedaPago As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''upEstado control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents upEstado As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''txtEstado control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtEstado As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtMontoPagoAse control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMontoPagoAse As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddlMonedaPagoAse control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlMonedaPagoAse As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtNroSiniestro control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtNroSiniestro As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''upCobertura control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents upCobertura As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''gvCobertura control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvCobertura As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''upDocSolicitado control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents upDocSolicitado As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''btnActgvDocumento control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnActgvDocumento As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''UpdatePanel2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel2 As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''gvDocumento0 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvDocumento0 As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''btn_refresDoc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btn_refresDoc As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btn_AllDoc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btn_AllDoc As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''updArchivo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents updArchivo As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''gvRegDoc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvRegDoc As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''btn_RfArchivo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btn_RfArchivo As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''RegularExpressionValidator1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RegularExpressionValidator1 As Global.System.Web.UI.WebControls.RegularExpressionValidator

    '''<summary>
    '''txtAseEmail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAseEmail As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtAseTelefono control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAseTelefono As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtAseDireccion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAseDireccion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtAseObservacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAseObservacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''upLlamada control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents upLlamada As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''gvLLamada control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvLLamada As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''btnActgvLLamada control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnActgvLLamada As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnGrabar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnGrabar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnActSiniestro control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnActSiniestro As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnEvaluar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnEvaluar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnRechazar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRechazar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''oDS control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents oDS As Global.System.Web.UI.WebControls.ObjectDataSource

    '''<summary>
    '''idDocSin control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents idDocSin As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hidEmployeeId control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hidEmployeeId As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''ValidationSummary1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ValidationSummary1 As Global.System.Web.UI.WebControls.ValidationSummary

    '''<summary>
    '''upEvaluacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents upEvaluacion As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''gvEvaluacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvEvaluacion As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''btnValidar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnValidar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pResulEval control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pResulEval As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''txtEstadoEval control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtEstadoEval As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtObservacionEval control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtObservacionEval As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnGrabarEval control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnGrabarEval As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''TabPanel3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TabPanel3 As Global.AjaxControlToolkit.TabPanel

    '''<summary>
    '''UpdatePanel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''gvInfoSiniestroC control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvInfoSiniestroC As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''btnActInfoAdicional control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnActInfoAdicional As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''uproSin control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents uproSin As Global.System.Web.UI.UpdateProgress

    '''<summary>
    '''hfOperacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfOperacion As Global.System.Web.UI.WebControls.HiddenField
End Class
