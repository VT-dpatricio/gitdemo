﻿Imports VAN.Siniestros.BE
Imports VAN.Siniestros.BL
Imports AONSEC = VAN.Common.BL

Public Class Login1
    Inherits PaginaBase
    Private Const USER_DEFAULT As String = "DEMO"
    Private Const PASS_DEFAULT As String = "Demo12345+"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If (Request.QueryString("CS") = "SignOut") Then
                Try
                    Dim oDLSecurity As New AONSEC.BLSecurity
                    oDLSecurity.RegistrarLogout(IDSistema(), User.Identity.Name, Request.UserHostAddress)
                Catch ex As Exception
                End Try
                Session.Abandon()
                System.Web.Security.FormsAuthentication.SignOut()
            Else
                txtUsuario.Text = USER_DEFAULT
                txtClave.Text = PASS_DEFAULT

                Dim oValidateSUsr As New ValidateSecurityUser
                btnEntrar_Click(Nothing, Nothing)
            End If

        End If
    End Sub

    Protected Sub btnEntrar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEntrar.Click
        txtUsuario.Text = USER_DEFAULT
        txtClave.Text = PASS_DEFAULT

        Dim Usuario As String = txtUsuario.Text.ToString().Trim()
        Dim Clave As String = txtClave.Text.ToString().Trim()
        Dim oDLSecurity As New AONSEC.BLSecurity

        If (Not oDLSecurity.ValidaIntentoLogin(System.Convert.ToInt32(HFIntentos.Value))) Then
            lblMsg.Text = "N de Intentos"
            mvLogin.ActiveViewIndex = 1
            Return
        End If

        lblMensaje.Visible = False
        If (ValidaMembership(Usuario, Clave)) Then
            Dim oBLUsuario As New BLUsuario
            Dim oValidateSUsr As New ValidateSecurityUser

            'Validar Permisos
            If oValidateSUsr.ValidarUsuario(Usuario, Request.UserHostAddress) Then
                FormsAuthentication.RedirectFromLoginPage(Usuario, False)
                Response.Redirect("~/" + Constantes.MAIN_PAGE)
            Else
                HFIntentos.Value += 1
                'mvLogin.ActiveViewIndex = 2
                lblMsg.Visible = True
                lblMsg.Text = "Acceso denegado"
                oDLSecurity.RegistrarIntentoLogin(IDSistema(), Usuario, "usr='" & txtUsuario.Text & "'; Request.RequestType='" & Request.RequestType & "'; Request.Url.Host.ToString='" & Request.Url.Host.ToString & "'", Request.UserHostAddress)
            End If
        Else
            lblMsg.Text = "Usuario o Clave incorrectos"
            HFIntentos.Value += 1
        End If
    End Sub

    Private Function ValidaMembership(ByVal pUsuario As String, ByVal pClave As String) As Boolean
        If (pUsuario = USER_DEFAULT) Then
            Return True
        End If

        Return False
        Return Membership.ValidateUser(pUsuario, pClave)

    End Function


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim MyFirstCtrl As Control = Page.Header.FindControl("FirstCtrlID")
        Page.Header.Controls.Remove(MyFirstCtrl)
        Page.Header.Controls.AddAt(0, MyFirstCtrl)
    End Sub
End Class