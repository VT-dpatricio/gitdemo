﻿Imports VAN.Siniestros.BE
Imports VAN.Siniestros.DL
Imports System
Imports System.Collections
Public Class BLReclamo
    Public Function Buscar(ByVal pIDEntidad As Int32, ByVal pIDUsuario As String, ByVal pIDFiltro As String, ByVal pParametro1 As String, ByVal pParametro2 As String, ByVal pIDEstadoReclamo As Int32) As IList
        Dim oDL As New DLNTReclamo
        Dim oBE As New BEReclamo
        oBE.IDEntidad = pIDEntidad
        oBE.Usuario = pIDUsuario
        oBE.IDFiltro = pIDFiltro
        oBE.Parametro1 = pParametro1
        oBE.Parametro2 = pParametro2
        oBE.IDEstadoReclamo = pIDEstadoReclamo
        Return oDL.Seleccionar(oBE)
    End Function

    Public Function Seleccionar(ByVal pIDReclamo As Int32) As BEReclamo
        Dim oDL As New DLNTReclamo
        Return oDL.SeleccionarBE(pIDReclamo)
    End Function

    Public Function Insertar(ByVal pEntidad As BEReclamo) As Int32
        Dim oDL As New DLTReclamo
        Return oDL.InsertarR(pEntidad)
    End Function

    Public Function Actualizar(ByVal pEntidad As BEReclamo) As Int32
        Dim oDL As New DLTReclamo
        Return oDL.Actualizar(pEntidad)
    End Function
End Class
