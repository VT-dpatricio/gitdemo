﻿Imports VAN.Siniestros.BE
Imports VAN.Siniestros.DL
Imports System
Imports System.Collections
Public Class BLMontivoReclamo
    Public Function Listar() As IList
        Dim oDL As New DLNTMotivoReclamo
        Return oDL.Seleccionar()
    End Function

    Public Function GetTipoMotivoReclamo(ByVal IDMotivoReclamo As Int32) As String
        Dim lst As IList
        Dim IDtipoMotivo As String = String.Empty
        Dim oDL As New DLNTMotivoReclamo

        lst = oDL.Seleccionar()

        For Each ob As BEMontivoReclamo In lst
            If ob.IDMotivoReclamo = IDMotivoReclamo Then
                IDtipoMotivo = ob.IDTipoMotivoReclamo
                Exit For
            End If
        Next
        Return IDtipoMotivo
    End Function
End Class
