﻿Imports VAN.Siniestros.BE
Imports VAN.Siniestros.DL
Imports System
Imports System.Collections

Public Class BLCertificado
    Public Function Buscar(ByVal pIDEntidad As Int32, ByVal pIDUsuario As String, ByVal pIDFiltro As String, ByVal pParametro1 As String, ByVal pParametro2 As String) As IList
        Dim oDL As New DLNTCertificado
        Dim oBE As New BECertificado
        oBE.IDFiltro = pIDFiltro
        oBE.Parametro1 = pParametro1
        oBE.Parametro2 = pParametro2
        oBE.IDEntidad = pIDEntidad
        oBE.Usuario = pIDUsuario
        Return oDL.Seleccionar(oBE)
    End Function

    Public Function Seleccionar(ByVal pIDCertificado As String) As BECertificado
        Dim oDL As New DLNTCertificado
        Return oDL.SeleccionarCer(pIDCertificado)
    End Function
End Class
