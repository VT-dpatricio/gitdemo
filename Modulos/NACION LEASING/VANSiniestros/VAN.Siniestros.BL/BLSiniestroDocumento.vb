﻿Imports VAN.Siniestros.DL
Imports VAN.Siniestros.BE
Imports System
Imports System.Collections
Public Class BLSiniestroDocumento

    Public Function ListarPorCobertura(ByVal pIdCoberturas As String) As IList
        Dim oDL As New DLNTSiniestroDocumento
        Return oDL.ListarPorCobertura(pIdCoberturas)
    End Function

    Public Function Listar(ByVal pIDSiniestro As Int32) As IList
        Dim oDL As New DLNTSiniestroDocumento
        Return oDL.Seleccionar(pIDSiniestro)
    End Function


    Public Function Actualizar(ByVal pEntidad As BESiniestroDocumento) As Boolean
        Dim oDL As New DLTSiniestroDocumento
        Return oDL.Actualizar(pEntidad)
    End Function

End Class
