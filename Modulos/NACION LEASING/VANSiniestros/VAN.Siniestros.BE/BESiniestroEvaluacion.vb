﻿Public Class BESiniestroEvaluacion
    Inherits BEBase
    Private _iDPregunta As Integer
    Private _iDSiniestro As Integer
    Private _nombre As String
    Private _descripcion As String
    Private _respuesta As Boolean
    Private _estado As Boolean

    Public Property IDPregunta() As Integer
        Get
            Return _iDPregunta
        End Get
        Set(ByVal value As Integer)
            _iDPregunta = value
        End Set
    End Property

    Public Property IDSiniestro() As Integer
        Get
            Return _iDSiniestro
        End Get
        Set(ByVal value As Integer)
            _iDSiniestro = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property

    Public Property Estado() As Boolean
        Get
            Return _estado
        End Get
        Set(ByVal value As Boolean)
            _estado = value
        End Set
    End Property

    Public Property Respuesta() As Boolean
        Get
            Return _respuesta
        End Get
        Set(ByVal value As Boolean)
            _respuesta = value
        End Set
    End Property
End Class
