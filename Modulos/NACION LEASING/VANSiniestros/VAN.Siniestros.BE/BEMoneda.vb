﻿Public Class BEMoneda
    Inherits BEBase

    Private _iDMoneda As String
    Private _nombre As String
    Private _simbolo As String


    Public Property IDMoneda() As String
        Get
            Return _iDMoneda
        End Get
        Set(ByVal value As String)
            _iDMoneda = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Public Property Simbolo() As String
        Get
            Return _simbolo
        End Get
        Set(ByVal value As String)
            _simbolo = value
        End Set
    End Property

End Class
