﻿Public Class BEReporte
    Inherits BEBase

    Private _idCertificado As String
    Private _idProducto As Int32
    Private _nroFisico As String
    Private _nroTicket As Int32
    Private _asegurado As String
    Private _dni As Int32
    Private _motivoReclamo As String
    Private _ramo As String
    Private _estadoReclamo As String
    Private _aseguradora As String
    Private _nombreProducto As String

    Public Property IdCertificado() As String
        Get
            Return _idCertificado
        End Get
        Set(ByVal value As String)
            _idCertificado = value
        End Set
    End Property

    Public Property IdProducto() As Int32
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Int32)
            _idProducto = value
        End Set
    End Property

    Public Property NroFisico() As String
        Get
            Return _nroFisico
        End Get
        Set(ByVal value As String)
            _nroFisico = value
        End Set
    End Property

    Public Property NroTicket() As Int32
        Get
            Return _nroTicket
        End Get
        Set(ByVal value As Int32)
            _nroTicket = value
        End Set
    End Property

    Public Property Asegurado() As String
        Get
            Return _asegurado
        End Get
        Set(ByVal value As String)
            _asegurado = value
        End Set
    End Property

    Public Property Dni() As String
        Get
            Return _dni
        End Get
        Set(ByVal value As String)
            _dni = value
        End Set
    End Property

    Public Property MotivoReclamo() As String
        Get
            Return _motivoReclamo
        End Get
        Set(ByVal value As String)
            _motivoReclamo = value
        End Set
    End Property

    Public Property Ramo() As String
        Get
            Return _ramo
        End Get
        Set(ByVal value As String)
            _ramo = value
        End Set
    End Property

    Public Property EstadoReclamo() As String
        Get
            Return _estadoReclamo
        End Get
        Set(ByVal value As String)
            _estadoReclamo = value
        End Set
    End Property


    Public Property Aseguradora() As String
        Get
            Return _aseguradora
        End Get
        Set(ByVal value As String)
            _aseguradora = value
        End Set
    End Property

    Public Property NombreProducto() As String
        Get
            Return _nombreProducto
        End Get
        Set(ByVal value As String)
            _nombreProducto = value
        End Set
    End Property
End Class
