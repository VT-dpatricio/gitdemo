﻿Public Class BECertificado
    Inherits BEBase

    Private _parametro1 As String
    Private _parametro2 As String
    Private _entidad As String
    Private _aseguradora As String
    Private _idCertificado As String
    Private _idProducto As Int32
    Private _producto As String
    Private _numCertificado As Decimal
    Private _numCertificadoBanco As String
    Private _opcion As String
    Private _nroFisico As String
    Private _idFrecuencia As Int32
    Private _frecuencia As String
    Private _idOficina As Int32
    Private _codOficina As Int32
    Private _oficina As String
    Private _idInformador As String
    Private _informador As String
    Private _idMedioPago As String
    Private _medioPago As String
    Private _idEstadoCertificado As Int32
    Private _montoAsegurado As Decimal
    Private _primaBruta As Decimal
    Private _iVA As Decimal
    Private _primaTotal As Decimal
    Private _primaTotalS As String
    Private _primaCobrar As Decimal
    Private _numeroCuenta As String
    Private _vencimiento As String
    Private _vigencia As String
    Private _puntos As Int32
    Private _idMotivoAnulacion As Int32
    Private _consistente As Boolean
    Private _puntosAnual As Int32
    Private _idCiudad As Int32
    Private _direccion As String
    Private _telefono As String
    Private _nombre1 As String
    Private _nombre2 As String
    Private _apellido1 As String
    Private _apellido2 As String
    Private _ccCliente As String = ""
    Private _idTipoDocumento As String
    Private _digitacion As DateTime
    Private _incentivo As Decimal
    Private _saldoIncentivo As Decimal
    Private _usuarioCreacion As String
    Private _usuarioModificacion As String
    Private _fechaModificacion As DateTime
    Private _solicitudAnulacion As String
    Private _digitacionAnulacion As String
    Private _efectuarAnulacion As String
    Private _venta As DateTime
    Private _finVigencia As String
    Private _simboloMoneda As String
    Private _idMonedaPrima As String
    Private _idMonedaCobro As String
    Private _idMonedaCuenta As String
    Private _afiliadoHasta As DateTime
    Private _idCiclo As Int32
    Private _diaGenera As Int16
    Private _mesGenera As Int16
    Private _sexo As String
    Private _estadoCivil As String
    Private _fechaNacimiento As String
    Private _direccionComercial As String
    Private _idCiudadDirC As Int32
    Private _iDEntidad As Integer = 0
    Private _usuario As String = String.Empty
    Private _estado As String = String.Empty
    Private _tipo As String = String.Empty
    Private _tipoDocumento As String = String.Empty
    Private _numero As String = String.Empty
    Private _idFiltro As String = String.Empty
    Private _observacion As String = String.Empty
    Private _nacionalidad As String = String.Empty
    Private _profesion As String = String.Empty
    Private _email As String = String.Empty
    Private _direccionC As String = String.Empty
    Private _numTarjAsegurada As String = String.Empty
    Private _monedaCuenta As String = String.Empty
    Private _fechaActivacion As String = String.Empty
    Private _cbu As String = String.Empty

    Private _iDdepartamentoC As Int32
    Private _iDprovinciaC As Int32
    Private _iDciudadC As Int32
    Private _iDPaisC As Int32

    Private _iDDepartamento As Int32
    Private _iDProvincia As Int32
    Private _iDPais As Int32
    Private _filtroAsegurado As Boolean

    Private _nroImagen As Int32
    Private _nroCobro As Int32
    Private _nroSiniestro As Int32

    Private _simboloMonedaPrima As String = String.Empty
    Private _nroSiniestroAseg As String = String.Empty


    Public Property SimboloMonedaPrima() As String
        Get
            Return _simboloMonedaPrima
        End Get
        Set(ByVal value As String)
            _simboloMonedaPrima = value
        End Set
    End Property


    Public Property FiltroAsegurado() As String
        Get
            Return _filtroAsegurado
        End Get
        Set(ByVal value As String)
            _filtroAsegurado = value
        End Set
    End Property


    Public Property Parametro1() As String
        Get
            Return _parametro1
        End Get
        Set(ByVal value As String)
            _parametro1 = value
        End Set
    End Property

    Public Property Parametro2() As String
        Get
            Return _parametro2
        End Get
        Set(ByVal value As String)
            _parametro2 = value
        End Set
    End Property

    Public Property Entidad() As String
        Get
            Return _entidad
        End Get
        Set(ByVal value As String)
            _entidad = value
        End Set
    End Property

    Public Property Aseguradora() As String
        Get
            Return _aseguradora
        End Get
        Set(ByVal value As String)
            _aseguradora = value
        End Set
    End Property

    Public Property IdCertificado() As String
        Get
            Return _idCertificado
        End Get
        Set(ByVal value As String)
            _idCertificado = value
        End Set
    End Property

    Public Property IdProducto() As Int32
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Int32)
            _idProducto = value
        End Set
    End Property

    Public Property Producto() As String
        Get
            Return _producto
        End Get
        Set(ByVal value As String)
            _producto = value
        End Set
    End Property

    Public Property NumCertificado() As Decimal
        Get
            Return _numCertificado
        End Get
        Set(ByVal value As Decimal)
            _numCertificado = value
        End Set
    End Property

    Public Property NumCertificadoBanco() As String
        Get
            Return _numCertificadoBanco
        End Get
        Set(ByVal value As String)
            _numCertificadoBanco = value
        End Set
    End Property

    Public Property Opcion() As String
        Get
            Return _opcion
        End Get
        Set(ByVal value As String)
            _opcion = value
        End Set
    End Property

    Public Property NroFisico() As String
        Get
            Return _nroFisico
        End Get
        Set(ByVal value As String)
            _nroFisico = value
        End Set
    End Property

    Public Property IdFrecuencia() As Int32
        Get
            Return _idFrecuencia
        End Get
        Set(ByVal value As Int32)
            _idFrecuencia = value
        End Set
    End Property

    Public Property Frecuencia() As String
        Get
            Return _frecuencia
        End Get
        Set(ByVal value As String)
            _frecuencia = value
        End Set
    End Property

    Public Property IdOficina() As Int32
        Get
            Return _idOficina
        End Get
        Set(ByVal value As Int32)
            _idOficina = value
        End Set
    End Property

    Public Property CodOficina() As Int32
        Get
            Return _codOficina
        End Get
        Set(ByVal value As Int32)
            _codOficina = value
        End Set
    End Property

    Public Property Oficina() As String
        Get
            Return _oficina
        End Get
        Set(ByVal value As String)
            _oficina = value
        End Set
    End Property

    Public Property IdInformador() As String
        Get
            Return _idInformador
        End Get
        Set(ByVal value As String)
            _idInformador = value
        End Set
    End Property

    Public Property Informador() As String
        Get
            Return _informador
        End Get
        Set(ByVal value As String)
            _informador = value
        End Set
    End Property

    Public Property IdMedioPago() As String
        Get
            Return _idMedioPago
        End Get
        Set(ByVal value As String)
            _idMedioPago = value
        End Set
    End Property

    Public Property MedioPago() As String
        Get
            Return _medioPago
        End Get
        Set(ByVal value As String)
            _medioPago = value
        End Set
    End Property

    Public Property IdEstadoCertificado() As Int32
        Get
            Return _idEstadoCertificado
        End Get
        Set(ByVal value As Int32)
            _idEstadoCertificado = value
        End Set
    End Property

    Public Property MontoAsegurado() As Decimal
        Get
            Return _montoAsegurado
        End Get
        Set(ByVal value As Decimal)
            _montoAsegurado = value
        End Set
    End Property

    Public Property PrimaBruta() As Decimal
        Get
            Return _primaBruta
        End Get
        Set(ByVal value As Decimal)
            _primaBruta = value
        End Set
    End Property

    Public Property IVA() As Decimal
        Get
            Return _iVA
        End Get
        Set(ByVal value As Decimal)
            _iVA = value
        End Set
    End Property

    Public Property PrimaTotal() As Decimal
        Get
            Return _primaTotal
        End Get
        Set(ByVal value As Decimal)
            _primaTotal = value
        End Set
    End Property

    Public Property PrimaCobrar() As Decimal
        Get
            Return _primaCobrar
        End Get
        Set(ByVal value As Decimal)
            _primaCobrar = value
        End Set
    End Property

    Public Property NumeroCuenta() As String
        Get
            Return _numeroCuenta
        End Get
        Set(ByVal value As String)
            _numeroCuenta = value
        End Set
    End Property

    Public Property PrimaTotalS() As String
        Get
            Return _primaTotalS
        End Get
        Set(ByVal value As String)
            _primaTotalS = value
        End Set
    End Property

    Public Property Vencimiento() As String
        Get
            Return _vencimiento
        End Get
        Set(ByVal value As String)
            _vencimiento = value
        End Set
    End Property

    Public Property Vigencia() As String
        Get
            Return _vigencia
        End Get
        Set(ByVal value As String)
            _vigencia = value
        End Set
    End Property

    Public Property Puntos() As Int32
        Get
            Return _puntos
        End Get
        Set(ByVal value As Int32)
            _puntos = value
        End Set
    End Property

    Public Property IdMotivoAnulacion() As Int32
        Get
            Return _idMotivoAnulacion
        End Get
        Set(ByVal value As Int32)
            _idMotivoAnulacion = value
        End Set
    End Property

    Public Property Consistente() As Boolean
        Get
            Return _consistente
        End Get
        Set(ByVal value As Boolean)
            _consistente = value
        End Set
    End Property

    Public Property PuntosAnual() As Int32
        Get
            Return _puntosAnual
        End Get
        Set(ByVal value As Int32)
            _puntosAnual = value
        End Set
    End Property

    Public Property IdCiudad() As Int32
        Get
            Return _idCiudad
        End Get
        Set(ByVal value As Int32)
            _idCiudad = value
        End Set
    End Property

    Public Property Direccion() As String
        Get
            Return _direccion
        End Get
        Set(ByVal value As String)
            _direccion = value
        End Set
    End Property

    Public Property Telefono() As String
        Get
            Return _telefono
        End Get
        Set(ByVal value As String)
            _telefono = value
        End Set
    End Property

    Public Property Nombre1() As String
        Get
            Return _nombre1
        End Get
        Set(ByVal value As String)
            _nombre1 = value
        End Set
    End Property

    Public Property Nombre2() As String
        Get
            Return _nombre2
        End Get
        Set(ByVal value As String)
            _nombre2 = value
        End Set
    End Property

    Public Property Apellido1() As String
        Get
            Return _apellido1
        End Get
        Set(ByVal value As String)
            _apellido1 = value
        End Set
    End Property

    Public Property Apellido2() As String
        Get
            Return _apellido2
        End Get
        Set(ByVal value As String)
            _apellido2 = value
        End Set
    End Property

    Public Property CcCliente() As String
        Get
            Return _ccCliente
        End Get
        Set(ByVal value As String)
            _ccCliente = value
        End Set
    End Property

    Public Property IdTipoDocumento() As String
        Get
            Return _idTipoDocumento
        End Get
        Set(ByVal value As String)
            _idTipoDocumento = value
        End Set
    End Property

    Public Property Digitacion() As DateTime
        Get
            Return _digitacion
        End Get
        Set(ByVal value As DateTime)
            _digitacion = value
        End Set
    End Property

    Public Property Incentivo() As Decimal
        Get
            Return _incentivo
        End Get
        Set(ByVal value As Decimal)
            _incentivo = value
        End Set
    End Property

    Public Property SaldoIncentivo() As Decimal
        Get
            Return _saldoIncentivo
        End Get
        Set(ByVal value As Decimal)
            _saldoIncentivo = value
        End Set
    End Property

    Public Property UsuarioCreacion() As String
        Get
            Return _usuarioCreacion
        End Get
        Set(ByVal value As String)
            _usuarioCreacion = value
        End Set
    End Property

    Public Property UsuarioModificacion() As String
        Get
            Return _usuarioModificacion
        End Get
        Set(ByVal value As String)
            _usuarioModificacion = value
        End Set
    End Property

    Public Property FechaModificacion() As DateTime
        Get
            Return _fechaModificacion
        End Get
        Set(ByVal value As DateTime)
            _fechaModificacion = value
        End Set
    End Property

    Public Property SolicitudAnulacion() As String
        Get
            Return _solicitudAnulacion
        End Get
        Set(ByVal value As String)
            _solicitudAnulacion = value
        End Set
    End Property

    Public Property DigitacionAnulacion() As String
        Get
            Return _digitacionAnulacion
        End Get
        Set(ByVal value As String)
            _digitacionAnulacion = value
        End Set
    End Property

    Public Property EfectuarAnulacion() As String
        Get
            Return _efectuarAnulacion
        End Get
        Set(ByVal value As String)
            _efectuarAnulacion = value
        End Set
    End Property

    Public Property Venta() As DateTime
        Get
            Return _venta
        End Get
        Set(ByVal value As DateTime)
            _venta = value
        End Set
    End Property

    Public Property FinVigencia() As String
        Get
            Return _finVigencia
        End Get
        Set(ByVal value As String)
            _finVigencia = value
        End Set
    End Property

    Public Property SimboloMoneda() As String
        Get
            Return _simboloMoneda
        End Get
        Set(ByVal value As String)
            _simboloMoneda = value
        End Set
    End Property

    Public Property IdMonedaPrima() As String
        Get
            Return _idMonedaPrima
        End Get
        Set(ByVal value As String)
            _idMonedaPrima = value
        End Set
    End Property

    Public Property IdMonedaCobro() As String
        Get
            Return _idMonedaCobro
        End Get
        Set(ByVal value As String)
            _idMonedaCobro = value
        End Set
    End Property

    Public Property IdMonedaCuenta() As String
        Get
            Return _idMonedaCuenta
        End Get
        Set(ByVal value As String)
            _idMonedaCuenta = value
        End Set
    End Property

    Public Property AfiliadoHasta() As DateTime
        Get
            Return _afiliadoHasta
        End Get
        Set(ByVal value As DateTime)
            _afiliadoHasta = value
        End Set
    End Property

    Public Property IdCiclo() As Int32
        Get
            Return _idCiclo
        End Get
        Set(ByVal value As Int32)
            _idCiclo = value
        End Set
    End Property

    Public Property DiaGenera() As Int16
        Get
            Return _diaGenera
        End Get
        Set(ByVal value As Int16)
            _diaGenera = value
        End Set
    End Property

    Public Property MesGenera() As Int16
        Get
            Return _mesGenera
        End Get
        Set(ByVal value As Int16)
            _mesGenera = value
        End Set
    End Property

    '------
    Public Property Sexo() As String
        Get
            Return _sexo
        End Get
        Set(ByVal value As String)
            _sexo = value
        End Set
    End Property

    Public Property EstadoCivil() As String
        Get
            Return _estadoCivil
        End Get
        Set(ByVal value As String)
            _estadoCivil = value
        End Set
    End Property

    Public Property FechaNacimiento() As String
        Get
            Return _fechaNacimiento
        End Get
        Set(ByVal value As String)
            _fechaNacimiento = value
        End Set
    End Property

    Public Property DireccionComercial() As String
        Get
            Return _direccionComercial
        End Get
        Set(ByVal value As String)
            _direccionComercial = value
        End Set
    End Property

    Public Property IdCiudadDirC() As Int32
        Get
            Return _idCiudadDirC
        End Get
        Set(ByVal value As Int32)
            _idCiudadDirC = value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return Me._usuario
        End Get
        Set(ByVal value As String)
            Me._usuario = value
        End Set
    End Property

    Public Property IDEntidad() As Integer
        Get
            Return _iDEntidad
        End Get
        Set(ByVal value As Integer)
            _iDEntidad = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return _estado
        End Get
        Set(ByVal value As String)
            _estado = value
        End Set
    End Property

    Public Property Tipo() As String
        Get
            Return _tipo
        End Get
        Set(ByVal value As String)
            _tipo = value
        End Set
    End Property

    Public Property TipoDocumento() As String
        Get
            Return _tipoDocumento
        End Get
        Set(ByVal value As String)
            _tipoDocumento = value
        End Set
    End Property

    Public Property Numero() As String
        Get
            Return _numero
        End Get
        Set(ByVal value As String)
            _numero = value
        End Set
    End Property

    Public Property IDFiltro() As String
        Get
            Return _idFiltro
        End Get
        Set(ByVal value As String)
            _idFiltro = value
        End Set
    End Property

    Public Property Observacion() As String
        Get
            Return _observacion
        End Get
        Set(ByVal value As String)
            _observacion = value
        End Set
    End Property

    Public Property Nacionalidad() As String
        Get
            Return _nacionalidad
        End Get
        Set(ByVal value As String)
            _nacionalidad = value
        End Set
    End Property

    Public Property Profesion() As String
        Get
            Return _profesion
        End Get
        Set(ByVal value As String)
            _profesion = value
        End Set
    End Property

    Public Property Email() As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property

    Public Property DireccionC() As String
        Get
            Return _direccionC
        End Get
        Set(ByVal value As String)
            _direccionC = value
        End Set
    End Property

    Public Property NumTarjAsegurada() As String
        Get
            Return _numTarjAsegurada
        End Get
        Set(ByVal value As String)
            _numTarjAsegurada = value
        End Set
    End Property

    Public Property MonedaCuenta() As String
        Get
            Return _monedaCuenta
        End Get
        Set(ByVal value As String)
            _monedaCuenta = value
        End Set
    End Property

    Public Property FechaActivacion() As String
        Get
            Return _fechaActivacion
        End Get
        Set(ByVal value As String)
            _fechaActivacion = value
        End Set
    End Property

    Public Property CBU() As String
        Get
            Return _cbu
        End Get
        Set(ByVal value As String)
            _cbu = value
        End Set
    End Property

    Public Property IDDepartamentoC() As Int32
        Get
            Return _iDdepartamentoC
        End Get
        Set(ByVal value As Int32)
            _iDdepartamentoC = value
        End Set
    End Property

    Public Property IDProvinciaC() As Int32
        Get
            Return _iDprovinciaC
        End Get
        Set(ByVal value As Int32)
            _iDprovinciaC = value
        End Set
    End Property

    Public Property IDCiudadC() As Int32
        Get
            Return _iDciudadC
        End Get
        Set(ByVal value As Int32)
            _iDciudadC = value
        End Set
    End Property

    Public Property IDPaisC() As Int32
        Get
            Return _iDPaisC
        End Get
        Set(ByVal value As Int32)
            _iDPaisC = value
        End Set
    End Property

    Public Property IDDepartamento() As Int32
        Get
            Return _iDDepartamento
        End Get
        Set(ByVal value As Int32)
            _iDDepartamento = value
        End Set
    End Property

    Public Property IDProvincia() As Int32
        Get
            Return _iDProvincia
        End Get
        Set(ByVal value As Int32)
            _iDProvincia = value
        End Set
    End Property

    Public Property IDPais() As Int32
        Get
            Return _iDPais
        End Get
        Set(ByVal value As Int32)
            _iDPais = value
        End Set
    End Property

    Public Property NroCobro() As Int32
        Get
            Return _nroCobro
        End Get
        Set(ByVal value As Int32)
            _nroCobro = value
        End Set
    End Property

    Public Property NroImagen() As Int32
        Get
            Return _nroImagen
        End Get
        Set(ByVal value As Int32)
            _nroImagen = value
        End Set
    End Property

    Public Property NroSiniestro() As Int32
        Get
            Return _nroSiniestro
        End Get
        Set(ByVal value As Int32)
            _nroSiniestro = value
        End Set
    End Property

    Public Property NroSiniestroAseg() As String
        Get
            Return _nroSiniestroAseg
        End Get
        Set(ByVal value As String)
            _nroSiniestroAseg = value
        End Set
    End Property





End Class
