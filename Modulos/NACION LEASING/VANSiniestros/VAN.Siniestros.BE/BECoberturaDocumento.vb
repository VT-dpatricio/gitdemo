﻿Public Class BECoberturaDocumento
    Inherits BEBase
    Private _iDCobertura As Integer
    Private _iDCoberturaPar As String
    Private _iDDocumento As Integer
    Private _documento As String
    Public Property IDCobertura() As Integer
        Get
            Return _iDCobertura
        End Get
        Set(ByVal value As Integer)
            _iDCobertura = value
        End Set
    End Property

    Public Property IDCoberturaPar() As String
        Get
            Return _iDCoberturaPar
        End Get
        Set(ByVal value As String)
            _iDCoberturaPar = value
        End Set
    End Property

    Public Property IDDocumento() As Integer
        Get
            Return _iDDocumento
        End Get
        Set(ByVal value As Integer)
            _iDDocumento = value
        End Set
    End Property

    Public Property Documento() As String
        Get
            Return _documento
        End Get
        Set(ByVal value As String)
            _documento = value
        End Set
    End Property
End Class
