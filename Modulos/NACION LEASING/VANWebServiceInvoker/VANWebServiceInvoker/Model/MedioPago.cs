﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VANWebServiceInvoker.Model
{
    public sealed class MedioPago
    {

        public int ID { get; set; }
        public string Raiz { get; set; }
        public string Tipo { get; set; } //CUENTA/TARJETA
        public string IDMedioPago { get; set; } //Visa, cuenta, master
        public string IDMedioPagoBAIS { get; set; } //C, A
        public string Descripcion{ get; set; } //Cuenta Corriente, Caja de Ahorro
        public string IDMoneda { get; set; }
        public string Numero { get; set; }
        public string NumeroCipher { get; set; }
        public string CBU { get; set; }
        public string CBUCipher { get; set; }
        public string Paquete { get; set; }
        public string Sucursal { get; set; }
        public string Vinculo { get; set; }
        public string Estado { get; set; }
        public string CuentaOrigen { get; set; }
        public string TipoCuentaOrigen { get; set; }
    }
}
