﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VANWebServiceInvoker.Model
{
    public sealed class Cliente
    {
        public Cliente()
        {
            Documentos = new Documento[1];
            Domicilios = new Domicilio[1];
            Telefonos = new Telefono[1];
            DatosContacto = new DatosContacto[0];
            MediosDePago = new List<MedioPago>();
            Nombre = new string[2];
            Apellido = new string[2];
        }

        public string Codigo { get; set; }
        public string EstadoRespuesta { get; set; }
        public Documento[] Documentos { get; set; }
        public string IndicadorCrediticio { get; set; }
        public string Tipo { get; set; }
        public string[] Nombre { get; set; }
        public string[] Apellido { get; set; }
        public string FechaNacimiento { get; set; }
        public string IndicadorFuncionario { get; set; }
        public string Naturalizacion { get; set; }
        public string Sexo { get; set; }
        public string EstadoCivil { get; set; }
        public Domicilio[] Domicilios { get; set; }
        public Telefono[] Telefonos { get; set; }
        public DatosContacto[] DatosContacto { get; set; }
        public Profesion Profesion { get; set; }
        public List<MedioPago> MediosDePago { get; set; }
        public string ProvinciaNacimiento { get; set; }


    }
}
