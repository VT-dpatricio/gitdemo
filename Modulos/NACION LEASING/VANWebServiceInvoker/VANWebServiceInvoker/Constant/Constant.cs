﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VANWebServiceInvoker.Constant
{
    public class Constant
    {
        public const string RESPONSE_OK = "00";
        public const string RESPONSE_ERROR = "01";
        public const string MEDIO_PAGO_CUENTA = "CUENTA";
        public const string MEDIO_PAGO_TARJETA = "TARJETA";
        public const string ESTADO_AT = "AT";

        public const string S_FORMAT_YYYYMMDD = "yyyyMMdd";
        public const string S_FORMAT_DDMMYYYY = "ddMMyyyy";
        public const string S_FORMAT_MMDDYYYY = "MMddyyyy";

        public const string TIPO_MEDIO_PAGO_CA = "A";
        public const string TIPO_MEDIO_PAGO_CC = "C";

        public const string T_MEDIO_PAGO = "MedioPago";
        public const string T_ESTADO_CIVIL = "EstadoCivil";

        public const int CIUDAD_DEFAULT_ID = 0;
    }
}
