﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Reflection;
using System.Configuration;
using AONArchitectural.Common.Security;
using AONArchitectural.Common.Obfuscator;

namespace VANWebServiceInvoker.Mapper
{
    public class MockFactory
    {
        private const string XML_CLIENT = "getCliente";
        private const string XML_CUENTAS = "getCuentas";
        private const string XML_TARJETAS = "getTarjetas";
        private const string XML_NRO_CUENTA = "getCbu";
        private const string FOLDER_XML = "WSxml\\";

        public static string GetFile(string ccCliente, string xmlType)
        {
            string pathFile = null;
            string xmlPath = ConfigurationManager.AppSettings["MockDirectory"];
            if (!String.IsNullOrEmpty(xmlPath))
            {
                if (!Directory.Exists(xmlPath)) xmlPath = AppDomain.CurrentDomain.BaseDirectory;
            }

            if (String.IsNullOrEmpty(xmlPath))
            {
                xmlPath = AppDomain.CurrentDomain.BaseDirectory;
            }
            pathFile = xmlPath + FOLDER_XML + xmlType + ccCliente + ".xml";

            if (!File.Exists(pathFile))
            {
                pathFile = xmlPath + FOLDER_XML + xmlType + "NotFound" + ".xml";
                
            }
            return pathFile;
        }

        public static void LoadXML(XmlDocument xmlRespDatosCliente, string ccCliente,string xmlType)
        {
            if (Common.Common.ConvertToBoolean(ConfigurationManager.AppSettings["MockFromDirectory"]))
            {
                xmlRespDatosCliente.Load(GetFile(ccCliente, xmlType));
            }
            else
            {
                System.Reflection.Assembly _assembly = Assembly.GetExecutingAssembly();
                string xmlNameSpace = "VANWebServiceInvoker.Wsxml." + xmlType + ccCliente + ".xml";
                System.IO.Stream _xmlStream = _assembly.GetManifestResourceStream(xmlNameSpace);
                System.IO.StreamReader _textStreamReader = new System.IO.StreamReader(_xmlStream);
                string xml = _textStreamReader.ReadToEnd();
                xmlRespDatosCliente.LoadXml(xml);
            }
        }

        public static Model.Cliente LoadClient(XmlDocument xmlRespDatosCliente, string ccCliente)
        {
            Model.Cliente objCliente = new Model.Cliente();
            try
            {
                LoadXML(xmlRespDatosCliente, ccCliente, XML_CLIENT);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objCliente;
        }

        public static ITAU_SERVICES.consultaPersonaFisicaResponse LoadCliente(string ccCliente)
        {
            XmlDocument xmlRespCliente = new XmlDocument();
            LoadClient(xmlRespCliente, ccCliente);
            XmlSerializer serializer = new XmlSerializer(typeof(VANWebServiceInvoker.ITAU_SERVICES.consultaPersonaFisicaResponse));
            ITAU_SERVICES.consultaPersonaFisicaResponse wrapperCliente = null;
            using (XmlReader reader = new XmlNodeReader(xmlRespCliente))
            {
                //reader.MoveToContent();
                wrapperCliente = (VANWebServiceInvoker.ITAU_SERVICES.consultaPersonaFisicaResponse)serializer.Deserialize(reader);
            }            
            return wrapperCliente;
        }

        public static ITAU_SERVICES.consultaResponse LoadCuentas(string codCliente)
        {
            XmlDocument xmlRespCuentas = new XmlDocument();
            LoadXML(xmlRespCuentas, codCliente, XML_CUENTAS);
            XmlSerializer serializer = new XmlSerializer(typeof(VANWebServiceInvoker.ITAU_SERVICES.consultaResponse));
            VANWebServiceInvoker.ITAU_SERVICES.consultaResponse wrapperCliente = null;
            using (XmlReader reader = new XmlNodeReader(xmlRespCuentas))
            {
                //reader.MoveToContent();
                wrapperCliente = (VANWebServiceInvoker.ITAU_SERVICES.consultaResponse)serializer.Deserialize(reader);
            }

            if (wrapperCliente != null && wrapperCliente.response != null && wrapperCliente.response.raiz != null)
            {
                foreach (VANWebServiceInvoker.ITAU_SERVICES.Raiz raiz in wrapperCliente.response.raiz)
                {
                    foreach (VANWebServiceInvoker.ITAU_SERVICES.Cuenta cuenta in raiz.cuenta)
                    {
                        cuenta.numeroCipher = Encrypter.Encrypt(cuenta.numero);
                        cuenta.numero = Obfuscator.AccountNumber(cuenta.numero);
                    }
                }
            }

            return wrapperCliente;
        }

        public static ITAU_SERVICES.consultaCBUResponse LoadCbu(string numero)
        {
            XmlDocument xmlRespCbu = new XmlDocument();
            LoadXML(xmlRespCbu, numero, XML_NRO_CUENTA);
            XmlSerializer serializer = new XmlSerializer(typeof(VANWebServiceInvoker.ITAU_SERVICES.consultaCBUResponse));
            VANWebServiceInvoker.ITAU_SERVICES.consultaCBUResponse wrapperCliente = null;
            using (XmlReader reader = new XmlNodeReader(xmlRespCbu))
            {
                //reader.MoveToContent();
                wrapperCliente = (VANWebServiceInvoker.ITAU_SERVICES.consultaCBUResponse)serializer.Deserialize(reader);
            }

            if (wrapperCliente != null)
            {
                if (wrapperCliente.response != null)
                {
                    wrapperCliente.response.numeroCipher = Encrypter.Encrypt(wrapperCliente.response.cbu.Trim());
                    wrapperCliente.response.cbu = Obfuscator.AccountNumber(wrapperCliente.response.cbu.Trim());
                }
            }

            return wrapperCliente;
        }

        public static ITAU_SERVICES.listadoResponse LoadTarjetas(string codCliente)
        {
            XmlDocument xmlRespTarjetas = new XmlDocument();
            LoadXML(xmlRespTarjetas, codCliente, XML_TARJETAS);
            XmlSerializer serializer = new XmlSerializer(typeof(VANWebServiceInvoker.ITAU_SERVICES.listadoResponse));
            VANWebServiceInvoker.ITAU_SERVICES.listadoResponse wrapperCliente = null;
            using (XmlReader reader = new XmlNodeReader(xmlRespTarjetas))
            {
                //reader.MoveToContent();
                wrapperCliente = (VANWebServiceInvoker.ITAU_SERVICES.listadoResponse)serializer.Deserialize(reader);
            }

            try
            {
                if (wrapperCliente != null && wrapperCliente.retornoListado != null && wrapperCliente.retornoListado.tarjetas != null)
                {
                    foreach (VANWebServiceInvoker.ITAU_SERVICES.Tarjeta objTarjeta in wrapperCliente.retornoListado.tarjetas)
                    {
                        objTarjeta.numeroCipher = Encrypter.Encrypt(objTarjeta.numero);
                        objTarjeta.numero = Obfuscator.AccountNumber(objTarjeta.numero);

                    }
                }
            }
            catch (Exception ex)
            {
            }

            return wrapperCliente;
        }

    }
}
