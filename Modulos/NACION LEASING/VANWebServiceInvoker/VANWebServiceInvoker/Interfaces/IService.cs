﻿using System;
using System.Collections.Generic;
using System.Text;
using VANWebServiceInvoker.Model;

namespace VANWebServiceInvoker.Interfaces
{
    public interface IService
    {

        Cliente ConsultaPersonaFisica(string tipoDoc, string nroDoc);
    }
}
