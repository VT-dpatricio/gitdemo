﻿Imports Microsoft.VisualBasic
Imports System.Drawing
Imports VAN.Siniestros.BE
Imports VAN.Siniestros.BL

Public Class PaginaBase
    Inherits System.Web.UI.Page

    'Public Function ValidarEstadoClaveUsuario() As Boolean
    '    Dim valida As Boolean = False
    '    Try
    '        'If ) Or CBool(Session("ClaveCaducada")) Then
    '        valida = CBool(Session("PrimerLogin")) Or CBool(Session("ClaveCaducada"))
    '        'End If
    '    Catch ex As Exception
    '        valida = False
    '    End Try
    '    Return valida

    'End Function


    Public Function IDSistema() As Int32

        Return CInt(Constantes.SISTEMA_ID) 'ConfigurationManager.AppSettings("IDSistema").ToString()
    End Function

    Public Function NroDocumentoMockDefault() As String
        Return "123456"
    End Function

    Public Function TipoDocumentoMockDefault() As String
        Return "L"
    End Function

    Public Function IsMock() As Boolean
        Dim mock As Boolean = False
        Try
            mock = CBool(ConfigurationManager.AppSettings("IsMock").ToString())
        Catch ex As Exception
            mock = False
        End Try
        Return mock
    End Function

    Public Function ValidarEstadoClaveUsuario() As Boolean
        Dim valida As Boolean = False
        Try
            'If ) Or CBool(Session("ClaveCaducada")) Then
            valida = CBool(Session("PrimerLogin")) Or CBool(Session("ClaveCaducada"))
            'End If
        Catch ex As Exception
            valida = False
        End Try
        Return valida

    End Function

    Public Sub ChangeColorControlMP(ByVal control As Object, ByVal bEstado As Boolean)
        Dim colorHexa As String = "#ffffff"

        If Not bEstado Then
            colorHexa = "#FDF8C2"
        End If

        If TypeOf (control) Is TextBox Then
            DirectCast(control, TextBox).BackColor = ColorTranslator.FromHtml(colorHexa)
        ElseIf TypeOf (control) Is DropDownList Then
            DirectCast(control, DropDownList).BackColor = ColorTranslator.FromHtml(colorHexa)
        End If
    End Sub

End Class
