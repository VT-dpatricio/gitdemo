﻿Imports VAN.Common.BL
Imports VAN.Common.BE
Imports AONSEC = VAN.Common.BL

Public Class ValidateSecurityUser
    Inherits PaginaBase

    Public Function ValidarUsuario(ByVal pUsuario As String, ByVal hostAddress As String) As Boolean
        Dim validate As Boolean = False
        Dim oBLSegEvento As New AONSEC.BLSeguridadEvento
        Dim oBLSecurity As New AONSEC.BLSecurity
        Dim oBLCSeguridad As New AONSEC.BLSeguridadConfig
        Try
            Dim oBEUsuario As BEUsuario
            oBEUsuario = oBLSecurity.GetUser(pUsuario, IDSistema())

            If oBEUsuario.IDUsuario <> "" And oBEUsuario.Acceso = True Then
                Session(Constantes.SS_APP_BEUSER) = oBEUsuario
                Session(Constantes.SS_ID_USUARIO) = oBEUsuario.IDUsuario
                Session(Constantes.SS_NAME_USER) = oBEUsuario.Nombre1 & " " & oBEUsuario.Apellido1

                'Parámetros de Seguridad de la Aplicación por Entidad-----------------------------------
                Dim pListParametro As IList = oBLSecurity.ConfigUserLogin(oBEUsuario.IDUsuario, IDSistema(), Constantes.ENTIDAD_ID, hostAddress) 'oBLCSeguridad.Seleccionar(18)
                Session(Constantes.SS_APP_LST_PARAMETERS) = pListParametro

                'Tiempo (minutos) para la desconexión automática de la sesión de usuario
                Session.Timeout = CInt(oBLCSeguridad.Parametro(Constantes.SS_TIME_OUT, pListParametro))


                Session(Constantes.SS_APP_FIRST_LOGIN) = False
                Session(Constantes.SS_APP_EXPIRED_PSW) = False

                'El cambio obligatorio de las contraseñas de acceso en el primer inicio de sesión.
                If CBool(oBLCSeguridad.Parametro(Constantes.SS_CHANGE_PSW, pListParametro)) Then
                    'Consultar si es la primera vez que el usuario inicia sesión
                    Session(Constantes.SS_APP_FIRST_LOGIN) = oBLSegEvento.PrimerLogin(oBEUsuario.IDUsuario)
                End If

                'El intervalo de caducidad automática de las mismas a los 30 (treinta) días.
                Dim pNroDiasCaduca As Integer = CInt(oBLCSeguridad.Parametro(Constantes.SS_EXPIRED_PSW, pListParametro))
                If pNroDiasCaduca <> 0 And Not CBool(Session(Constantes.SS_APP_FIRST_LOGIN)) Then
                    'Consultar si la contraseña ha caducado
                    Session(Constantes.SS_APP_EXPIRED_PSW) = oBLSegEvento.ClaveCaducada(oBEUsuario.IDUsuario, pNroDiasCaduca)
                End If
                validate = True
            Else
                validate = False
            End If
        Catch ex As Exception
            validate = False
        End Try
        Return validate
    End Function
End Class
