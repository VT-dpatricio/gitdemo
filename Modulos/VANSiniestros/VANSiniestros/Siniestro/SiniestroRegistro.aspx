﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="SiniestroRegistro.aspx.vb" Inherits="VANSiniestros.RegistroSiniestro" EnableEventValidation="false" culture="es-PE" uiCulture="es-PE"%>
<%@ Register src="../Controles/Cargando.ascx" tagname="Cargando" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
        <script src="../Scripts/jquery-1.7.1.min.js" type="text/javascript"></script>  
<script src="../Scripts/jquery.simplemodal.1.4.3.min.js" type="text/javascript"></script> 
    <script type="text/javascript">

        function AdjuntarDoc() {
            var pRD = getRandom()
            var pID = "-1"
            var src = "pAdjuntarDoc.aspx?ID=" + pID + "&RD=" + pRD;
                    $.modal('<iframe src="' + src + '" height="95" width="635" style="border:0" frameborder=0>', {
                        closeHTML: "<a href='#' title='Cerrar' class='modal-close'><img src='../Images/cerrar.gif' alt='Cerrar' width='16' height='16'/></a>",
                        containerCss: {
                            backgroundColor: "#FFF",
                            borderColor: "#750000",
                            height: 100,
                            padding: 0,
                            width: 640
                        },
                        overlayClose: false,
                        appendTo: '#aspnetForm',
                        //appendTo: 'form',
                        persist: true
                    });
                    return false;
        }

        function getRandom() {
            return ((Math.random() * 6e6) | 0)
        }

        function ActElimArchivo() {
             $get('<%= btn_AllDoc.ClientID%>').click();
            alert('El archivo se eliminó con éxito');
        }


        function ActgvDocumento() {
            $get('<%= btn_RfArchivo.ClientID%>').click();
            $.modal.close();
            alert('Documento guardado con éxito');
        }

        function ActCheck(id) {
            SetFechaEntrega(id);
            document.getElementById('<%=idDocSin.ClientID%>').value = id;
            $get('<%= btn_refresDoc.ClientID%>').click();
        }

         function SetFechaEntrega(id) {
            var TargetBaseControl = document.getElementById('<%= gvDocumento.ClientID%>');
            var TargetChildControl = "chkPermiso";
            var Inputs = TargetBaseControl.getElementsByTagName("input");

            var Grid_Table = document.getElementById('<%= gvDocumento.ClientID%>');
             //alert(id)
             //alert(Grid_Table.rows[id].cells[1].innerText)
             //alert(Inputs[id].checked)
             if (Inputs[id].checked) {
                 Grid_Table.rows[id].cells[1].textContent = fechaActual();
                 Grid_Table.rows[id].cells[1].innerText = fechaActual();
                 Grid_Table.rows[id].cells[1].value = fechaActual();
             }
             else {
                 Grid_Table.rows[id].cells[1].textContent = '';
                 Grid_Table.rows[id].cells[1].innerText = '';
                 Grid_Table.rows[id].cells[1].value = '';
             }
             //alert(Grid_Table.rows[id].cells[1].innerText)
         }

        function fechaActual() {
            var today = new Date();
            var dd = today.getDate().toString();
            var mm = (today.getMonth() + 1).toString();
            var pad = "00"
            var fecha = (pad + dd.toString()).slice(-2) + "/" + (pad + mm.toString()).slice(-2) + "/" + today.getFullYear();

            return fecha;
        }
    
        function ActTabRegistroSiniestro() {
            TabActive(true);
            $get('<%= btnActRegSiniestro.ClientID%>').click();
            $find('<%=TabRegSiniestro.ClientID%>').set_activeTabIndex(1);

        }

        function ActTabBusquedaCliente() {
            TabActive(false);
            //$get('<%= btnActRegSiniestro.ClientID%>').click();
            $find('<%=TabRegSiniestro.ClientID%>').set_activeTabIndex(0);

        }

        function ActiveTabChanged(sender, e) 
        {
    //        var tab=sender.get_activeTab().get_tabIndex();
    //        var Bt;
    //        if (tab == '1') 
    //        {
    //            TabActive(false);
    //            Bt = $get('<%= btnActRegSiniestro.ClientID%>');
    //            Bt.click;
    //        }
        }

        
        function vDecimal(evt) {
            // NOTE: Backspace = 8, Enter = 13, '0' = 48, '9' = 57, '.' = 46       
            if (window.event) // IE
            {
                key = evt.keyCode;
            }
            else if (e.which) // Netscape/Firefox/Opera
            {
                key = evt.which;
            }

            return (key <= 13 || (key >= 48 && key <= 57) || key == 46);
        }
        //onkeypress="javascript:return vDecimal(event);"
        
    </script>

</asp:Content>
  
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<asp:TabContainer 
        ID="TabRegSiniestro" runat="server" ActiveTabIndex="0" Width="100%">
        <asp:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel1">
            <HeaderTemplate>Búsqueda del Cliente</HeaderTemplate>
            <ContentTemplate><table cellpadding="3" cellspacing="0" style="width: 998px"><tr><td><asp:UpdatePanel ID="upFiltroBuscar" runat="server" UpdateMode="Conditional"><ContentTemplate><table class="TablaMarco" style="width: 990px" cellpadding="0" cellspacing="5"><tr><td class="TablaLabelR" style="width: 85px">Entidad:&#160;</td><td style="width: 161px"><asp:DropDownList ID="ddlEntidad" runat="server" AppendDataBoundItems="True" 
                        Width="162px"><asp:ListItem Value="0">Todas</asp:ListItem></asp:DropDownList></td><td class="TablaLabelR" style="width: 120px">&nbsp;</td><td style="width: 161px">&#160;</td><td class="TablaLabelR" style="width: 73px">&nbsp;</td><td style="width: 157px">&#160;</td><td>&#160;</td></tr><tr><td class="TablaLabelR" style="width: 85px">Buscar por:</td><td style="width: 161px"><asp:DropDownList ID="ddlBuscarPor" runat="server" AutoPostBack="True" 
                            ValidationGroup="Buscar" Width="162px"><asp:ListItem Selected="True" Value="NroDoc">N° Documento</asp:ListItem><asp:ListItem Value="ApeNom">Apellidos y Nombres</asp:ListItem><asp:ListItem Value="NroCertificado">N° Certificado BAIS</asp:ListItem><asp:ListItem Value="NroFisico">N° Certificado Aseg.</asp:ListItem><asp:ListItem Value="NroCuenta">N° Cuenta</asp:ListItem><asp:ListItem Value="NroRaiz">N° Raíz</asp:ListItem></asp:DropDownList></td><td class="TablaLabelR" style="width: 88px"><asp:Label ID="lblPar1" runat="server" Text="N° Documento:"></asp:Label></td><td style="width: 161px"><asp:TextBox ID="txtPar1" runat="server" Width="160px"></asp:TextBox></td><td class="TablaLabelR" style="width: 73px"><asp:Label ID="lblPar2" runat="server" Visible="False">Nombres:</asp:Label></td><td style="width: 157px"><asp:TextBox ID="txtPar2" runat="server" Visible="False" Width="160px"></asp:TextBox></td><td><asp:Button ID="btnBuscar" runat="server" Text="Buscar" 
                            ValidationGroup="Buscar" Width="60px" /></td></tr></table></ContentTemplate></asp:UpdatePanel></td></tr><tr><td><asp:UpdatePanel ID="upListaCertificado" runat="server" 
                                UpdateMode="Conditional"><ContentTemplate>
                                    <asp:GridView ID="gvListaCertificado" runat="server" AllowPaging="True" 
                                            AutoGenerateColumns="False" DataKeyNames="IDCertificado,IDProducto" Width="989px">
                                        <Columns>
                                            <asp:TemplateField HeaderText="IDCertificado">
                                                <ItemTemplate><asp:Label ID="Label8" runat="server" Text='<%# Bind("IdCertificado") %>'></asp:Label>
                                                </ItemTemplate><ItemStyle Width="120px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="IDProducto" HeaderText="IDProducto" Visible="False" />
                                            <asp:TemplateField HeaderText="Producto"><ItemTemplate>
                                                <asp:Label ID="Label9" runat="server" Text='<%# Bind("Producto") %>'></asp:Label></ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Tipo Documento">
                                                <ItemTemplate><asp:Label ID="Label10" runat="server" Text='<%# Bind("TipoDocumento") %>'></asp:Label></ItemTemplate><ItemStyle Width="100px" /></asp:TemplateField><asp:TemplateField HeaderText="N° Documento"><ItemTemplate><asp:Label ID="Label11" runat="server" Text='<%# Bind("CcCliente") %>'></asp:Label></ItemTemplate><ItemStyle Width="120px" /></asp:TemplateField><asp:TemplateField HeaderText="Apellidos"><ItemTemplate><asp:Label ID="Label12" runat="server" 
                                            Text='<%# Eval("Apellido1") %>'></asp:Label></ItemTemplate><ItemStyle Width="180px" /></asp:TemplateField><asp:TemplateField HeaderText="Nombres"><ItemTemplate><asp:Label ID="Label5" runat="server" Text='<%# Bind("Nombre1") %>'></asp:Label></ItemTemplate><ItemStyle Width="200px" /></asp:TemplateField><asp:TemplateField HeaderText="Estado"><ItemTemplate><asp:Label ID="Label13" runat="server" Text='<%# Bind("Estado") %>'></asp:Label></ItemTemplate><ItemStyle HorizontalAlign="Center" Width="40px" /></asp:TemplateField>
                                            <asp:CommandField ButtonType="Image" SelectImageUrl="~/img/seleccionar.gif" 
                                    ShowSelectButton="True"><ItemStyle HorizontalAlign="Center" Width="30px" /></asp:CommandField></Columns>
                                        <EmptyDataTemplate>No se encontron registros</EmptyDataTemplate>

                                    </asp:GridView>
                       </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                                </Triggers>
                   </asp:UpdatePanel></td></tr></table></ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
            <HeaderTemplate>Registro de Siniestro</HeaderTemplate>
            <ContentTemplate><asp:UpdatePanel ID="upRSiniestro" runat="server" UpdateMode="Conditional"><ContentTemplate><table cellpadding="0" cellspacing="5" style="width: 100%"><tr><td><table class="TablaMarco" style="width: 100%"><tr><td class="TablaTitulo">Datos del Certificado
                <asp:HiddenField ID="hfIDCertificado" runat="server" />
                <asp:HiddenField ID="hfIDProducto" runat="server" Value="0" />
                <asp:HiddenField ID="idDocSin" runat="server" />
                                                                                                                                                                                                                                              </td></tr><tr><td><table style="width: 977px"><tr><td style="width: 119px">N° Certificado</td><td style="width: 142px">Producto</td><td style="width: 211px">&#160;</td><td style="width: 108px">Plan</td><td style="width: 157px">Canal</td><td>Aseguradora</td></tr><tr><td class="TablaCeldaBox" style="width: 119px"><asp:Label ID="lblNroCertificado" runat="server"></asp:Label></td><td class="TablaCeldaBox" colspan="2"><asp:Label ID="lblProducto" runat="server"></asp:Label>&#160;</td><td class="TablaCeldaBox" style="width: 108px"><asp:Label ID="lblOpcion" runat="server"></asp:Label></td><td class="TablaCeldaBox" style="width: 157px"><asp:Label ID="lblCanal" runat="server"></asp:Label></td><td class="TablaCeldaBox"><asp:Label ID="lblAseguradora" runat="server"></asp:Label></td></tr><tr><td style="width: 119px">Tipo Documento</td><td style="width: 142px">N° Documento</td><td style="width: 211px">Asegurado</td><td style="width: 108px">&#160;</td><td style="width: 157px">Moneda Prima</td><td>Estado</td></tr><tr><td class="TablaCeldaBox" style="width: 119px"><asp:Label ID="lblTipoDocumento" runat="server"></asp:Label></td><td class="TablaCeldaBox" style="width: 142px"><asp:Label ID="lblNroDocumento" runat="server"></asp:Label></td><td class="TablaCeldaBox" colspan="2"><asp:Label ID="lblAsegurado" runat="server"></asp:Label></td><td class="TablaCeldaBox"><asp:Label ID="lblMonedaPrima" runat="server"></asp:Label></td><td class="TablaCeldaBox"><asp:Label ID="lblEstadoCer" runat="server"></asp:Label></td></tr></table></td></tr></table></td></tr><tr><td><table class="TablaMarco" style="width: 100%"><tr><td class="TablaTitulo">Datos del Siniestro</td></tr><tr><td><table cellpadding="0" cellspacing="5" style="width: 974px"><tr><td style="width: 97px; font-weight: bold;">N° Ticket</td><td style="width: 297px">Tipo Siniestro<asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
                            runat="server" ControlToValidate="ddlTipoSiniestro" 
                            ErrorMessage="Seleccione el Tipo de Siniestro" ForeColor="Red" InitialValue="0" 
                            ValidationGroup="grabar">*</asp:RequiredFieldValidator></td><td style="width: 89px">Fecha Siniestro<asp:RequiredFieldValidator 
                        ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="txtFechaSiniestro" 
                        ErrorMessage="Ingrese la Fecha de Siniestro" ForeColor="Red" 
                        ValidationGroup="grabar" Display="Dynamic">*</asp:RequiredFieldValidator><asp:CompareValidator ID="CompareValidator1" runat="server" 
                            ControlToValidate="txtFechaSiniestro" Display="Dynamic" 
                            ErrorMessage="Ingrese una Fecha de Siniestro válida" ForeColor="Red" 
                            Operator="DataTypeCheck" Type="Date" ValidationGroup="grabar">*</asp:CompareValidator><asp:MaskedEditExtender ID="me" runat="server" ClearTextOnInvalid="True" 
                        Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaSiniestro"></asp:MaskedEditExtender></td><td style="width: 110px">Monto Denunciado</td><td style="width: 104px">Moneda</td><td style="width: 292px">&nbsp;</td></tr><tr><td style="width: 97px"><asp:TextBox ID="txtNroTicket" runat="server" Font-Bold="True" ReadOnly="True" 
                        Width="95px">-</asp:TextBox></td><td style="width: 297px"><asp:DropDownList ID="ddlTipoSiniestro" runat="server" 
                            AppendDataBoundItems="True" AutoPostBack="True" Width="298px"></asp:DropDownList></td><td style="width: 89px"><asp:TextBox ID="txtFechaSiniestro" runat="server" Width="84px"></asp:TextBox></td><td style="width: 110px"><asp:TextBox ID="txtMontoDenunciado" runat="server" onkeypress="javascript:return vDecimal(event);"></asp:TextBox></td><td style="width: 104px"><asp:DropDownList ID="ddlMonedaDe" runat="server" Width="93px"></asp:DropDownList></td><td style="width: 292px">&nbsp;</td></tr><tr><td colspan="2">Cobertura</td><td style="width: 89px">&#160;</td><td colspan="2">Documentos Solicitados</td><td style="width: 292px">&#160;</td></tr><tr><td colspan="3" style="vertical-align: top">
                                <asp:UpdatePanel ID="upCobertura" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:GridView ID="gvCobertura" runat="server" AutoGenerateColumns="False" 
                                            DataKeyNames="IDCobertura" Width="501px" BorderColor="White" 
                                            BorderStyle="Solid" BorderWidth="4px">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Seleccionar">
                                                <ItemTemplate><asp:CheckBox ID="chkSelCobertura" runat="server" AutoPostBack="True" 
                                                            CausesValidation="True" oncheckedchanged="chkSelCobertura_CheckedChanged" 
                                                            ToolTip='<%# Eval("IDCobertura") %>' />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                            </asp:TemplateField><asp:BoundField DataField="IDCobertura" HeaderText="IDCobertura" Visible="False" />
                                            <asp:BoundField DataField="Nombre" HeaderText="Nombre">
                                                <ItemStyle Width="350px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Descripcion" HeaderText="Descripción" Visible="False" />
                                        </Columns>
                                        <EmptyDataTemplate>No se encontraron coberturas</EmptyDataTemplate>
                                    </asp:GridView>
                                <asp:Label ID="lblMsgSelTipoSiniestro" runat="server" ForeColor="#666666" Text="Seleccione un Tipo de Siniestro"></asp:Label><br />
                                </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlTipoSiniestro" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                </td><td colspan="3" style="vertical-align: top">
                                            <asp:UpdatePanel ID="upDocSolicitado" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gvDocumento" runat="server" AutoGenerateColumns="False" 
                                                        Width="450px" BorderColor="White" BorderStyle="Solid" BorderWidth="4px" 
                                                        OnRowEditing="gvDocumento_RowEditing" DataKeyNames="IDDocumento,Documento,FechaEntrega" >
                                                        <Columns>
                                                            <asp:BoundField DataField="IDDocumento" HeaderText="IDDocumento" Visible="false" />
                                                            <asp:BoundField DataField="Documento" HeaderText="Documento" ReadOnly="true" />
                                                            <asp:TemplateField HeaderText="F. Entrega">
                                                                             <ItemTemplate>
                                                                                 <asp:Label ID="Label2" runat="server" Text='<%#FormatDate(Eval("FechaEntrega"))%>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <EditItemTemplate>
                                                                                    <asp:TextBox ID="txtFDesde" runat="server" Width="56px" Text='<%#Bind("FechaEntrega")%>'></asp:TextBox>
                                                                                    <asp:CalendarExtender ID="CExt1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtFDesde">
                                                                                    </asp:CalendarExtender>
                                                                                    <asp:MaskedEditExtender ID="MEE1" runat="server" ClearTextOnInvalid="True" Mask="99/99/9999"
                                                                                        MaskType="Date" TargetControlID="txtFDesde">
                                                                                    </asp:MaskedEditExtender>
                                                                            </EditItemTemplate>
                                                                             <HeaderStyle HorizontalAlign="Center" />
                                                                             <ItemStyle HorizontalAlign="Center" Width="110px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Entregado">
                                                                        <EditItemTemplate>
                                                                            <asp:CheckBox ID="CheckBox2" runat="server" Checked='<%# Bind("Entregado")%>' />
                                                                        </EditItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="20px" />
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkPermiso" runat="server" Checked='<%# Bind("Entregado")%>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <EditItemTemplate>
                                                                            <asp:ImageButton ID="IBGrabar0" runat="server" CommandArgument='<%# Eval("IDSiniestroDoc") %>' ImageUrl="~/images/grid/Save.gif" onclick="IBGrabar_Click" Text="Update" ToolTip="Grabar" ValidationGroup="ValidarGrid" />
                                                                            <asp:ImageButton ID="IBCancelar0" runat="server" CausesValidation="False" CommandName="Cancel" ImageUrl="~/images/grid/b_cancelar.gif" onclick="IBCancelar_Click" Text="Cancel" ToolTip="Cancelar" />
                                                                        </EditItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="45px" />
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="IBEditar0" runat="server" CausesValidation="False" CommandArgument='<%# Eval("IDSiniestroDoc") %>'  CommandName="Edit" ImageUrl="~/images/grid/b_editar.gif" Text="Edit" ToolTip="Modificar Registro" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate>No se encontraron registros</EmptyDataTemplate>
                                                    </asp:GridView>
                                                    
                                                    <asp:Label ID="lblMsgSelCobertura" runat="server" ForeColor="#666666" Text="Seleccione una Cobertura"></asp:Label>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="gvCobertura" />
                                                    <asp:AsyncPostBackTrigger ControlID="ddlTipoSiniestro" EventName="SelectedIndexChanged" />
                                                    <asp:AsyncPostBackTrigger ControlID="btn_AllDoc" EventName="click" />
                                                    <asp:AsyncPostBackTrigger ControlID="btn_refresDoc" EventName="click" />
                                                </Triggers>
                                          </asp:UpdatePanel>
                                            <asp:Button ID="btn_refresDoc" runat="server" CssClass="btnOculto" />
                                            <asp:Button ID="btn_AllDoc" runat="server" CssClass="btnOculto" />
                                                                                                             </td></tr><tr><td colspan="6" style="vertical-align: top">
                                              <asp:UpdatePanel ID="updArchivo" runat="server" UpdateMode="Conditional">
                                                  <ContentTemplate>
                                            <asp:GridView ID="gvRegDoc" runat="server" AllowPaging="True" 
                                                AutoGenerateColumns="False" BorderColor="White" BorderStyle="Solid" 
                                                BorderWidth="4px" PageSize="5" Width="100%" Caption="Documentos Adjuntos"
                                                DataKeyNames="FechaCreacion,NombreArchivo">
                                                <Columns>
                                                    <asp:BoundField DataField="IDArchivo" HeaderText="IDArchivo" Visible="False" />
                                                    <asp:BoundField DataField="FechaCreacion" HeaderText="Fecha Ingreso" />
                                                    <asp:BoundField DataField="NombreArchivo" HeaderText="Nombre Archivo" />
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:ImageButton ID="ibtnReglla" runat="server" 
                                                                ImageUrl="~/Images/Icon/nuevo.gif" CommandArgument='<%# Eval("IDArchivo")%>' 
                                                                ToolTip="Adjuntar Documento"  OnClientClick='AdjuntarDoc()'  />
                                                        </HeaderTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                     <ItemTemplate>
                                                                <asp:ImageButton ID="btnEliminarDoc" runat="server" CausesValidation="False" CommandArgument='<%# Bind("IDArchivo") %>'
                                                                    ImageUrl="~/Images/Grid/anular.gif" OnClientClick="return confirm('Está seguro que desea eliminar el documento adjunto?');"
                                                                    OnCommand="btnEliminar_Command"
                                                                    Text="Delete" CommandName="delete" 
                                                                    ToolTip="Eliminar" />
                                                               </ItemTemplate>
                                                          <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                     </asp:TemplateField>
                                                </Columns>
                                                    <EmptyDataTemplate>
                                                        <asp:Button ID="btnRegistrarArchivo" runat="server" 
                                                            Text="Nuevo Documento" Width="140px" 
                                                            onclientclick="AdjuntarDoc();" />
                                                    </EmptyDataTemplate>
                                                 </asp:GridView>
                                                  </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="btn_RfArchivo" EventName="click" />
                                                    </Triggers>
                                              </asp:UpdatePanel>
                                              <asp:Button ID="btn_RfArchivo" runat="server" CssClass="btnOculto" />
                                              <asp:Label ID="lblMsgSelArchivo" runat="server" ForeColor="#666666" Text="Seleccione los archivos"></asp:Label>
                                              <asp:FileUpload ID="fuDocumento" runat="server" Width="273px" Visible="false" />
                        </td></tr></table></td></tr></table></td></tr><tr><td><table class="TablaMarco" style="width: 100%"><tr><td class="TablaTitulo">Actualizar Información del Cliente</td></tr><tr><td><table style="width: 974px"><tr><td style="width: 337px">E-mail <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                        ControlToValidate="txtAseEmail" ErrorMessage="Ingrese un E-mail válido" 
                        ForeColor="Red" 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                        ValidationGroup="grabar">*</asp:RegularExpressionValidator></td><td style="width: 143px">Teléfono</td><td>Observaciones</td></tr><tr><td style="width: 337px"><asp:TextBox ID="txtAseEmail" runat="server" Width="335px"></asp:TextBox></td><td style="width: 143px"><asp:TextBox ID="txtAseTelefono" runat="server" Width="143px"></asp:TextBox></td><td rowspan="3"><asp:TextBox ID="txtAseObservacion" runat="server" Height="69px" 
                                                                        TextMode="MultiLine" 
                        Width="453px"></asp:TextBox></td></tr><tr><td style="width: 337px">Dirección</td><td style="width: 143px">&#160;</td></tr><tr><td colspan="2"><asp:TextBox ID="txtAseDireccion" runat="server" Width="483px"></asp:TextBox></td></tr></table></td></tr></table></td></tr><tr><td><asp:Button ID="btnGrabar" runat="server" Text="Grabar" Width="80px" 
                    ValidationGroup="grabar" /><asp:Button ID="btnActRegSiniestro" runat="server" CssClass="btnOculto" />&#160;<asp:Button ID="btnNuevo" runat="server" CausesValidation="False" Text="Nuevo" 
                    Width="80px" Visible="False" /><asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" 
                    ShowMessageBox="True" ShowSummary="False" ValidationGroup="grabar" /></td></tr></table></ContentTemplate></asp:UpdatePanel><br /></ContentTemplate>
        </asp:TabPanel>
    </asp:TabContainer>

    &nbsp;<asp:UpdateProgress ID="upGeneral" runat="server" DisplayAfter="0">
        <ProgressTemplate>
            <uc1:Cargando ID="Cargando1" runat="server" />
        </ProgressTemplate>
    </asp:UpdateProgress>
        <script type="text/javascript">
//            window.onload = function () {
//               TabActive(false);
//            }

            function TabActive(value) {
                $find('<%=TabRegSiniestro.ClientID%>').get_tabs()[1].set_enabled(value);
                
                
                /*var CTab2 = $get('__tab_MainContent_TabRegSiniestro_TabPanel2');
                CTab2.disabled = value;*/
                //__tab_MainContent_TabRegSiniestro_TabPanel2
            }
    </script>
</asp:Content>
