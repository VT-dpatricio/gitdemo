﻿Public Class Reportes
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not HttpContext.Current.Request.IsAuthenticated Then
            Session.Abandon()
            Response.Redirect("~/Login.aspx")
        End If
        Page.Header.DataBind()
        Me.Page.ClientScript.RegisterStartupScript(
         Me.GetType(),
         "StartupScript",
         "Sys.Application.add_load(function () { $find('ctl00_MainContent_RptV').add_propertyChanged(viewerPropertyChanged); });",
     True)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim MyFirstCtrl As Control = Page.Header.FindControl("FirstCtrlID")
        Page.Header.Controls.Remove(MyFirstCtrl)
        Page.Header.Controls.AddAt(0, MyFirstCtrl)
    End Sub
End Class