﻿Imports VAN.Siniestros.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLNTReclamo
    Inherits DLBase
    Implements IDLNT

    Public Function Seleccionar() As System.Collections.IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pEntidad As BE.BEBase) As System.Collections.IList Implements IDLNT.Seleccionar
        Dim cn As New SqlConnection(CadenaConexion)
        Dim cm As New SqlCommand("SS_BuscarReclamo", cn)
        cm.CommandType = CommandType.StoredProcedure
        Dim oBE As BEReclamo = DirectCast(pEntidad, BEReclamo)
        cm.Parameters.Add("@IDUsuario", SqlDbType.VarChar, 50).Value = oBE.Usuario
        cm.Parameters.Add("@IDEntidad", SqlDbType.Int).Value = oBE.IDEntidad
        cm.Parameters.Add("@IDFiltro", SqlDbType.VarChar, 15).Value = oBE.IDFiltro
        cm.Parameters.Add("@Parametro1", SqlDbType.VarChar, 200).Value = oBE.Parametro1
        cm.Parameters.Add("@Parametro2", SqlDbType.VarChar, 200).Value = oBE.Parametro2
        cm.Parameters.Add("@IDEstadoReclamo", SqlDbType.Int).Value = oBE.IDEstadoReclamo
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BEReclamo
                oBE.IDReclamo = rd.GetInt32(rd.GetOrdinal("IDReclamo"))
                oBE.FechaAtencion = rd.GetDateTime(rd.GetOrdinal("FechaAtencion"))
                oBE.IDCertificado = rd.GetString(rd.GetOrdinal("IdCertificado"))
                'oBE.IdProducto = rd.GetInt32(rd.GetOrdinal("IDProducto"))
                oBE.Producto = rd.GetString(rd.GetOrdinal("Producto"))
                oBE.EstadoReclamo = rd.GetString(rd.GetOrdinal("EstadoReclamo"))
                oBE.TipoDocumento = rd.GetString(rd.GetOrdinal("TipoDocumento"))
                oBE.CcCliente = rd.GetString(rd.GetOrdinal("Nrodocumento"))
                oBE.Nombre1 = rd.GetString(rd.GetOrdinal("Nombre"))
                oBE.Apellido1 = rd.GetString(rd.GetOrdinal("Apellido"))
                oBE.FechaCreacion = rd.GetDateTime(rd.GetOrdinal("FechaCreacion"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As System.Collections.IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BE.BEBase Implements IDLNT.SeleccionarBE
        Dim cn As New SqlConnection(CadenaConexion)
        Dim cm As New SqlCommand("SS_SeleccionarReclamo", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@IDReclamo", SqlDbType.Int).Value = pCodigo
        Dim oBE As New BEReclamo
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            If rd.Read Then
                oBE.IDReclamo = rd.GetInt32(rd.GetOrdinal("IDReclamo"))
                oBE.FechaAtencion = rd.GetDateTime(rd.GetOrdinal("FechaAtencion"))
                oBE.FechaFinAtencion = rd.GetDateTime(rd.GetOrdinal("FechaFinAtencion"))
                oBE.FechaReclamo = rd.GetDateTime(rd.GetOrdinal("FechaReclamo"))
                oBE.EstadoReclamo = rd.GetString(rd.GetOrdinal("EstadoReclamo"))
                oBE.IDEstadoReclamo = rd.GetInt32(rd.GetOrdinal("IDEstadoReclamo"))
                oBE.IDMotivoReclamo = rd.GetInt32(rd.GetOrdinal("IDMotivoReclamo"))
                oBE.NroReclamo = rd.GetString(rd.GetOrdinal("NroReclamo"))
                oBE.NroCobro = rd.GetInt32(rd.GetOrdinal("NroCobro"))
                oBE.Imagen = rd.GetBoolean(rd.GetOrdinal("Imagen"))
                oBE.Siniestro = rd.GetBoolean(rd.GetOrdinal("Siniestro"))
                oBE.CobroInicioPeriodo = rd.GetDateTime(rd.GetOrdinal("CobroInicioPeriodo"))
                oBE.CobroFinPeriodo = rd.GetDateTime(rd.GetOrdinal("CobroFinPeriodo"))
                oBE.NroCobroDevolver = rd.GetInt32(rd.GetOrdinal("NroCobroDevolver"))
                oBE.MontoTotal = rd.GetDecimal(rd.GetOrdinal("MontoTotal"))
                oBE.IDMoneda = rd.GetString(rd.GetOrdinal("IDMoneda"))
                oBE.FechaExtorno = rd.GetDateTime(rd.GetOrdinal("FechaExtorno"))
                oBE.OperacionExtorno = rd.GetString(rd.GetOrdinal("OperacionExtorno"))
                oBE.Observacion = rd.GetString(rd.GetOrdinal("Observacion"))
                oBE.Moneda = rd.GetString(rd.GetOrdinal("Moneda"))
                oBE.SimboloMoneda = rd.GetString(rd.GetOrdinal("SimboloMoneda"))
                oBE.FechaCreacion = rd.GetDateTime(rd.GetOrdinal("FechaCreacion"))
                If (rd.IsDBNull(rd.GetOrdinal("MedioDevolucion"))) Then
                    oBE.MedioDevolucion = String.Empty
                Else
                    oBE.MedioDevolucion = rd.GetString(rd.GetOrdinal("MedioDevolucion"))
                End If
                oBE.AseDireccion = rd.GetString(rd.GetOrdinal("AseDireccion"))
                oBE.AseTelefono = rd.GetString(rd.GetOrdinal("AseTelefono"))
                oBE.AseEmail = rd.GetString(rd.GetOrdinal("AseEmail"))
                oBE.ObservacionAsegurado = rd.GetString(rd.GetOrdinal("ObservacionAsegurado"))
                oBE.CuentaExterna = rd.GetBoolean(rd.GetOrdinal("CuentaExterna"))
                oBE.IDMedioPago = rd.GetString(rd.GetOrdinal("IdMedioPago"))
                oBE.CBU = rd.GetString(rd.GetOrdinal("Cbu"))
            End If
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return oBE
    End Function


End Class

