﻿Imports VAN.Siniestros.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLNTMoneda
    Inherits DLBase
    Implements IDLNT

    Public Function Seleccionar() As System.Collections.IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pEntidad As BE.BEBase) As System.Collections.IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As System.Collections.IList Implements IDLNT.Seleccionar
        Dim cn As New SqlConnection(CadenaConexion)
        Dim cm As New SqlCommand("SS_ListarMoneda", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@IDProducto", SqlDbType.Int).Value = pCodigo
        Dim oBE As BEMoneda
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BEMoneda
                oBE.IDMoneda = rd.GetString(rd.GetOrdinal("IDMoneda"))
                oBE.Nombre = rd.GetString(rd.GetOrdinal("Nombre"))
                oBE.Simbolo = rd.GetString(rd.GetOrdinal("Simbolo"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BE.BEBase Implements IDLNT.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function
End Class
