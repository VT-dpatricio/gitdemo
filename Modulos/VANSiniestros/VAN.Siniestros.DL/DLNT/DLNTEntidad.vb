Imports VAN.Siniestros.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLNTEntidad
    Inherits DLBase

    Public Function Seleccionar() As System.Collections.IList
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pEntidad As BEBase) As System.Collections.IList
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SS_ListarEntidad", cn)
        cm.CommandType = CommandType.StoredProcedure
        Dim oBE As BEEntidad = DirectCast(pEntidad, BEEntidad)
        cm.Parameters.Add("@IDUsuario", SqlDbType.NVarChar, 50).Value = oBE.Usuario
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BEEntidad
                oBE.IDEntidad = rd.GetInt32(rd.GetOrdinal("idEntidad"))
                oBE.Descripcion = rd.GetString(rd.GetOrdinal("nombre"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

End Class
