Imports VAN.Siniestros.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLNTProducto
    Inherits DLBase


    Public Function Seleccionar(ByVal pEntidad As BEBase) As System.Collections.IList
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand()
        cm.CommandType = CommandType.StoredProcedure
        cm.Connection = cn
        Dim oBE As BEProducto = DirectCast(pEntidad, BEProducto)
        If oBE.Opcion = "SeleccionarActivos" Then
            cm.CommandText = "RS_Producto"
        Else
            cm.CommandText = "RS_ProductoTodos"
        End If
        cm.Parameters.Add("@IDEntidad", SqlDbType.Int).Value = oBE.IDEntidad
        cm.Parameters.Add("@IDUsuario", SqlDbType.NVarChar, 50).Value = oBE.IDUsuario
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BEProducto
                oBE.IDProducto = rd.GetInt32(rd.GetOrdinal("idProducto"))
                oBE.Descripcion = rd.GetString(rd.GetOrdinal("nombre"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

  
End Class
