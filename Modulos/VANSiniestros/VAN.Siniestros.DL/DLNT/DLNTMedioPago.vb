﻿Imports VAN.Siniestros.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLNTMedioPago
    Inherits DLBase
    Implements IDLNT


    Public Function Seleccionar() As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(pEntidad As BEBase) As IList Implements IDLNT.Seleccionar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SR_PRC_SEL_MedioPago", cn)
        Dim oBE As BEMedioPago = DirectCast(pEntidad, BEMedioPago)

        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@IDMedioPago", SqlDbType.VarChar, 25).Value = IIf(oBE.IDMedioPago = String.Empty, System.DBNull.Value, oBE.IDMedioPago)

        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BEMedioPago
                oBE.IDMedioPago = rd.GetString(rd.GetOrdinal("IdMedioPago"))
                oBE.Nombre = rd.GetString(rd.GetOrdinal("Nombre"))
                If rd.IsDBNull(rd.GetOrdinal("IdTipoMedioPago")) Then
                    oBE.IDTipoMedioPago = String.Empty
                Else
                    oBE.IDTipoMedioPago = rd.GetString(rd.GetOrdinal("IdTipoMedioPago"))
                End If
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function Seleccionar(pCodigo As Integer) As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(pCodigo As Integer) As BEBase Implements IDLNT.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(pCodigo As String) As BEBase
        Dim oBE = New BEMedioPago
        Dim lista As New ArrayList

        oBE.IDMedioPago = pCodigo
        lista = Seleccionar(oBE)
        If lista.Count > 0 Then
            Return lista(0)
        End If
        Return Nothing
    End Function
End Class
