﻿Imports VAN.Siniestros.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLNTCertificado
    Inherits DLBase
    Implements IDLNT

    Public Function Seleccionar() As System.Collections.IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pEntidad As BE.BEBase) As System.Collections.IList Implements IDLNT.Seleccionar
        Dim cn As New SqlConnection(CadenaConexion)
        Dim cm As New SqlCommand("SS_BuscarCertificado", cn)
        cm.CommandType = CommandType.StoredProcedure
        Dim oBE As BECertificado = DirectCast(pEntidad, BECertificado)
        cm.CommandTimeout = 300
        cm.Parameters.Add("@IDUsuario", SqlDbType.VarChar, 50).Value = oBE.Usuario
        cm.Parameters.Add("@IDEntidad", SqlDbType.Int).Value = oBE.IDEntidad
        cm.Parameters.Add("@IDFiltro", SqlDbType.VarChar, 15).Value = oBE.IDFiltro
        cm.Parameters.Add("@Parametro1", SqlDbType.VarChar, 200).Value = oBE.Parametro1
        cm.Parameters.Add("@Parametro2", SqlDbType.VarChar, 200).Value = oBE.Parametro2
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BECertificado
                oBE.IdCertificado = rd.GetString(rd.GetOrdinal("IdCertificado"))
                oBE.IdProducto = rd.GetInt32(rd.GetOrdinal("IDProducto"))
                oBE.Producto = rd.GetString(rd.GetOrdinal("Producto"))
                oBE.NumCertificado = rd.GetDecimal(rd.GetOrdinal("NumCertificado"))
                oBE.Estado = rd.GetString(rd.GetOrdinal("Estado"))
                oBE.TipoDocumento = rd.GetString(rd.GetOrdinal("TipoDocumento"))
                oBE.CcCliente = rd.GetString(rd.GetOrdinal("Nrodocumento"))
                oBE.Nombre1 = rd.GetString(rd.GetOrdinal("Nombre"))
                oBE.Apellido1 = rd.GetString(rd.GetOrdinal("Apellido"))
                oBE.Vigencia = rd.GetString(rd.GetOrdinal("Vigencia"))
                oBE.FinVigencia = rd.GetString(rd.GetOrdinal("FinVigencia"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As System.Collections.IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BE.BEBase Implements IDLNT.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarCer(ByVal pIDCertificado As String) As BECertificado
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SS_SeleccionarCertificadoxID", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@IDCertificado", SqlDbType.VarChar, 25).Value = pIDCertificado
        Dim oBE As New BECertificado
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            If rd.Read Then
                oBE.IdCertificado = rd.GetString(rd.GetOrdinal("IdCertificado"))
                oBE.IdProducto = rd.GetInt32(rd.GetOrdinal("IdProducto"))
                oBE.Producto = rd.GetString(rd.GetOrdinal("Producto"))
                oBE.NumCertificado = rd.GetDecimal(rd.GetOrdinal("NumCertificado"))
                oBE.IdOficina = rd.GetInt32(rd.GetOrdinal("IdOficina"))
                oBE.CodOficina = rd.GetInt32(rd.GetOrdinal("CodOficina"))
                oBE.Oficina = rd.GetString(rd.GetOrdinal("Oficina"))
                oBE.TipoDocumento = rd.GetString(rd.GetOrdinal("TipoDocumento"))
                oBE.IdTipoDocumento = rd.GetString(rd.GetOrdinal("IdTipoDocumento"))
                oBE.IdEstadoCertificado = rd.GetInt32(rd.GetOrdinal("IdEstadoCertificado"))
                oBE.Nombre1 = rd.GetString(rd.GetOrdinal("Nombre1"))
                oBE.Nombre2 = rd.GetString(rd.GetOrdinal("Nombre2"))
                oBE.Apellido1 = rd.GetString(rd.GetOrdinal("Apellido1"))
                oBE.Apellido2 = rd.GetString(rd.GetOrdinal("Apellido2"))
                oBE.CcCliente = rd.GetString(rd.GetOrdinal("CcCliente"))
                oBE.Telefono = rd.GetString(rd.GetOrdinal("Telefono"))
                oBE.Direccion = rd.GetString(rd.GetOrdinal("Direccion"))
                oBE.Email = rd.GetString(rd.GetOrdinal("Email"))
                oBE.Entidad = rd.GetString(rd.GetOrdinal("Entidad"))
                oBE.Aseguradora = rd.GetString(rd.GetOrdinal("Aseguradora"))

                oBE.NroImagen = rd.GetInt32(rd.GetOrdinal("NroImagen"))
                oBE.NroCobro = rd.GetInt32(rd.GetOrdinal("NroCobro"))
                oBE.NroSiniestro = rd.GetInt32(rd.GetOrdinal("NroSiniestro"))
                oBE.Estado = rd.GetString(rd.GetOrdinal("Estado"))
                oBE.NumCertificadoBanco = rd.GetString(rd.GetOrdinal("NumCertificadoBanco"))
                oBE.DigitacionAnulacion = rd.GetString(rd.GetOrdinal("DigitacionAnulacion"))
                oBE.IdMedioPago = rd.GetString(rd.GetOrdinal("IdMedioPago"))
                oBE.CBU = rd.GetString(rd.GetOrdinal("Cbu"))
                oBE.NumeroCuenta = rd.GetString(rd.GetOrdinal("numeroCuenta"))
                oBE.IdMonedaPrima = rd.GetString(rd.GetOrdinal("IdMonedaPrima"))
                oBE.SimboloMonedaPrima = rd.GetString(rd.GetOrdinal("SimboloMonedaPrima"))
                oBE.Opcion = rd.GetString(rd.GetOrdinal("Opcion"))
            End If
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return oBE
    End Function
End Class
