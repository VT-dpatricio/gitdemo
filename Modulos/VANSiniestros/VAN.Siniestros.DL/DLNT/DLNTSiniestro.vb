﻿Imports VAN.Siniestros.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLNTSiniestro
    Inherits DLBase
    Implements IDLNT

    Public Function Seleccionar() As System.Collections.IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pEntidad As BE.BEBase) As System.Collections.IList Implements IDLNT.Seleccionar
        Dim cn As New SqlConnection(CadenaConexion)
        Dim cm As New SqlCommand("SS_BuscarSiniestro", cn)
        cm.CommandType = CommandType.StoredProcedure
        Dim oBE As BESiniestro = DirectCast(pEntidad, BESiniestro)
        cm.Parameters.Add("@IDUsuario", SqlDbType.VarChar, 50).Value = oBE.Usuario
        cm.Parameters.Add("@IDEntidad", SqlDbType.Int).Value = oBE.IDEntidad
        cm.Parameters.Add("@IDFiltro", SqlDbType.VarChar, 15).Value = oBE.IDFiltro
        cm.Parameters.Add("@Parametro1", SqlDbType.VarChar, 200).Value = oBE.Parametro1
        cm.Parameters.Add("@Parametro2", SqlDbType.VarChar, 200).Value = oBE.Parametro2
        cm.Parameters.Add("@IDEstadoSiniestro", SqlDbType.Int).Value = oBE.IDEstadoSiniestro
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BESiniestro
                oBE.IDSiniestro = rd.GetInt32(rd.GetOrdinal("IDSiniestro"))
                oBE.FechaSiniestro = rd.GetDateTime(rd.GetOrdinal("FechaSiniestro"))
                oBE.IdCertificado = rd.GetString(rd.GetOrdinal("IdCertificado"))
                'oBE.IdProducto = rd.GetInt32(rd.GetOrdinal("IDProducto"))
                oBE.Producto = rd.GetString(rd.GetOrdinal("Producto"))
                oBE.EstadoSiniestro = rd.GetString(rd.GetOrdinal("EstadoSiniestro"))
                oBE.TipoDocumento = rd.GetString(rd.GetOrdinal("TipoDocumento"))
                oBE.CcCliente = rd.GetString(rd.GetOrdinal("Nrodocumento"))
                oBE.Nombre1 = rd.GetString(rd.GetOrdinal("Nombre"))
                oBE.Apellido1 = rd.GetString(rd.GetOrdinal("Apellido"))
                oBE.FechaCreacion = rd.GetDateTime(rd.GetOrdinal("FechaCreacion"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function


    Public Function Seleccionar(ByVal pIDCertificado As String) As System.Collections.IList
        Dim cn As New SqlConnection(CadenaConexion)
        Dim cm As New SqlCommand("SS_ListarSiniestro", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@IDCertificado", SqlDbType.VarChar, 25).Value = pIDCertificado
        Dim oBE As BESiniestro
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BESiniestro
                oBE.IDSiniestro = rd.GetInt32(rd.GetOrdinal("IDSiniestro"))
                oBE.FechaSiniestro = rd.GetDateTime(rd.GetOrdinal("FechaSiniestro"))
                oBE.IDCertificado = rd.GetString(rd.GetOrdinal("IdCertificado"))
                'oBE.IdProducto = rd.GetInt32(rd.GetOrdinal("IDProducto"))
                oBE.Producto = rd.GetString(rd.GetOrdinal("Producto"))
                oBE.EstadoSiniestro = rd.GetString(rd.GetOrdinal("EstadoSiniestro"))
                oBE.TipoDocumento = rd.GetString(rd.GetOrdinal("TipoDocumento"))
                oBE.CcCliente = rd.GetString(rd.GetOrdinal("Nrodocumento"))
                oBE.Nombre1 = rd.GetString(rd.GetOrdinal("Nombre"))
                oBE.Apellido1 = rd.GetString(rd.GetOrdinal("Apellido"))
                oBE.TipoSiniestro = rd.GetString(rd.GetOrdinal("TipoSiniestro"))

                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As System.Collections.IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BE.BEBase Implements IDLNT.SeleccionarBE
        Dim cn As New SqlConnection(CadenaConexion)
        Dim cm As New SqlCommand("SS_PRC_SEL_Siniestro", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@IDSiniestro", SqlDbType.Int).Value = pCodigo
        Dim oBE As New BESiniestro
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            If rd.Read Then
                oBE.IDSiniestro = rd.GetInt32(rd.GetOrdinal("IDSiniestro"))
                oBE.FechaSiniestro = rd.GetDateTime(rd.GetOrdinal("FechaSiniestro"))
                oBE.EstadoSiniestro = rd.GetString(rd.GetOrdinal("EstadoSiniestro"))
                oBE.IDEstadoSiniestro = rd.GetInt32(rd.GetOrdinal("IDEstadoSiniestro"))
                oBE.IDTipoSiniestro = rd.GetInt32(rd.GetOrdinal("IDTipoSiniestro"))
                oBE.TipoSiniestro = rd.GetString(rd.GetOrdinal("TipoSiniestro"))
                oBE.FechaSiniestro = rd.GetDateTime(rd.GetOrdinal("FechaSiniestro"))
                oBE.MontoSiniestro = rd.GetDecimal(rd.GetOrdinal("MontoSiniestro"))
                oBE.IDMonedaMSiniestro = rd.GetString(rd.GetOrdinal("IDMonedaMSiniestro"))
                oBE.MontoPago = rd.GetDecimal(rd.GetOrdinal("MontoPago"))
                oBE.IDMonedaMPago = rd.GetString(rd.GetOrdinal("IDMonedaMPago"))
                oBE.MontoPagoAse = rd.GetDecimal(rd.GetOrdinal("MontoPagoAse"))
                oBE.IDMonedaMPagoAse = rd.GetString(rd.GetOrdinal("IDMonedaMPagoAse"))
                'oBE.FechaDocCompleta = rd.GetDateTime(rd.GetOrdinal("FechaDocCompleta"))
                oBE.AseDireccion = rd.GetString(rd.GetOrdinal("AseDireccion"))
                oBE.AseTelefono = rd.GetString(rd.GetOrdinal("AseTelefono"))
                oBE.AseEmail = rd.GetString(rd.GetOrdinal("AseEmail"))
                oBE.AseObservacion = rd.GetString(rd.GetOrdinal("AseObservacion"))
                oBE.FechaCreacion = rd.GetDateTime(rd.GetOrdinal("FechaCreacion"))
                oBE.NroSiniestroAseguradora = rd.GetString(rd.GetOrdinal("NroSiniestroAseg"))
            End If
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return oBE
    End Function

    
End Class

