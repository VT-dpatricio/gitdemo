﻿Imports VAN.Siniestros.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLNTMotivoReclamo
    Inherits DLBase
    Implements IDLNT

    Public Function Seleccionar() As System.Collections.IList Implements IDLNT.Seleccionar
        Dim cn As New SqlConnection(CadenaConexion)
        Dim cm As New SqlCommand("SS_ListarMotivoReclamo", cn)
        cm.CommandType = CommandType.StoredProcedure
        Dim oBE As BEMontivoReclamo
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BEMontivoReclamo
                oBE.IDMotivoReclamo = rd.GetInt32(rd.GetOrdinal("IDMotivoReclamo"))
                oBE.Nombre = rd.GetString(rd.GetOrdinal("Nombre"))
                oBE.CodigoMotivoReclamo = rd.GetInt32(rd.GetOrdinal("CodMotivoReclamo"))

                If rd.IsDBNull(rd.GetOrdinal("IDTipoMotivoReclamo")) Then
                    oBE.IDTipoMotivoReclamo = String.Empty
                Else
                    oBE.IDTipoMotivoReclamo = rd.GetString(rd.GetOrdinal("IDTipoMotivoReclamo"))
                End If
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista

    End Function

    Public Function Seleccionar(ByVal pEntidad As BE.BEBase) As System.Collections.IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As System.Collections.IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BE.BEBase Implements IDLNT.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function
End Class
