﻿Public Class BESiniestroLlamada
    Inherits BEBase
    Private _iDLlamada As Integer
    Private _iDSiniestro As Integer
    Private _observacion As String
    Private _adjunto As String
    Private _fechaHora As DateTime
    Private _usuarioCreacion As String

    Public Property IDLlamada() As Integer
        Get
            Return _iDLlamada
        End Get
        Set(ByVal value As Integer)
            _iDLlamada = value
        End Set
    End Property

    Public Property IDSiniestro() As Integer
        Get
            Return _iDSiniestro
        End Get
        Set(ByVal value As Integer)
            _iDSiniestro = value
        End Set
    End Property

    Public Property Observacion() As String
        Get
            Return _observacion
        End Get
        Set(ByVal value As String)
            _observacion = value
        End Set
    End Property

    Public Property Adjunto() As String
        Get
            Return _adjunto
        End Get
        Set(ByVal value As String)
            _adjunto = value
        End Set
    End Property

    Public Property FechaHora() As DateTime
        Get
            Return _fechaHora
        End Get
        Set(ByVal value As DateTime)
            _fechaHora = value
        End Set
    End Property


    Public Property UsuarioCreacion() As String
        Get
            Return _usuarioCreacion
        End Get
        Set(ByVal value As String)
            _usuarioCreacion = value
        End Set
    End Property

End Class
