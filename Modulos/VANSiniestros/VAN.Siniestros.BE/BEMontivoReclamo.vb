﻿Public Class BEMontivoReclamo
    Inherits BEBase
    Private _iDMotivoReclamo As Integer
    Private _nombre As String
    Private _idTipoMotivoReclamo As String
    Private _codMotivoReclamo As Integer

    Public Property IDMotivoReclamo() As Integer
        Get
            Return _iDMotivoReclamo
        End Get
        Set(ByVal value As Integer)
            _iDMotivoReclamo = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Public Property IDTipoMotivoReclamo() As String
        Get
            Return _idTipoMotivoReclamo
        End Get
        Set(ByVal value As String)
            _idTipoMotivoReclamo = value
        End Set
    End Property

    Public Property CodigoMotivoReclamo() As Integer
        Get
            Return _codMotivoReclamo
        End Get
        Set(ByVal value As Integer)
            _codMotivoReclamo = value
        End Set
    End Property
End Class
