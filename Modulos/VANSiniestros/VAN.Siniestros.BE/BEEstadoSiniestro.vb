﻿Public Class BEEstadoSiniestro
    Inherits BEBase
    Private _iDEstadoSiniestro As Integer
    Private _nombre As String
    Public Property IDEstadoSiniestro() As Integer
        Get
            Return _iDEstadoSiniestro
        End Get
        Set(ByVal value As Integer)
            _iDEstadoSiniestro = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

End Class
