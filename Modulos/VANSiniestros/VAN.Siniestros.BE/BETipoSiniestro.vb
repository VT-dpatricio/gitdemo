﻿Public Class BETipoSiniestro
    Inherits BEBase

    Private _iDTipoSiniestro As Integer
    Private _iDProducto As Integer
    Private _opcion As String
    Private _nombre As String
    Private _descripcion As String
    Private _estado As Boolean

    Public Property IDTipoSiniestro() As Integer
        Get
            Return _iDTipoSiniestro
        End Get
        Set(ByVal value As Integer)
            _iDTipoSiniestro = value
        End Set
    End Property

    Public Property IDProducto() As Integer
        Get
            Return _iDProducto
        End Get
        Set(ByVal value As Integer)
            _iDProducto = value
        End Set
    End Property

    Public Property Opcion() As String
        Get
            Return _opcion
        End Get
        Set(ByVal value As String)
            _opcion = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return _estado
        End Get
        Set(ByVal value As String)
            _estado = value
        End Set
    End Property

End Class
