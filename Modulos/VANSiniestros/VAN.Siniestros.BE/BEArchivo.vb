﻿Public Class BEArchivo
    Inherits BEBase

    Private _idArchivo As Int32
    Private _idSiniestro As Int32
    Private _nombreArchivo As String = String.Empty
    Private _rutaArchivo As String = String.Empty
    Private _fechaIngreso As DateTime
    Private _fechaCreacion As DateTime
    Private _usuarioCreacion = String.Empty
    Private _estado As Boolean


    Public Property IdArchivo() As Int32
        Get
            Return _idArchivo
        End Get
        Set(ByVal value As Int32)
            _idArchivo = value
        End Set
    End Property

    Public Property IdSiniestro() As Int32
        Get
            Return _idSiniestro
        End Get
        Set(ByVal value As Int32)
            _idSiniestro = value
        End Set
    End Property

    Public Property NombreArchivo() As String
        Get
            Return _nombreArchivo
        End Get
        Set(ByVal value As String)
            _nombreArchivo = value
        End Set
    End Property

    Public Property RutaArchivo() As String
        Get
            Return _rutaArchivo
        End Get
        Set(ByVal value As String)
            _rutaArchivo = value
        End Set
    End Property

    Public Property UsuarioCreacion() As String
        Get
            Return _usuarioCreacion
        End Get
        Set(ByVal value As String)
            _usuarioCreacion = value
        End Set
    End Property

    Public Property FechaCreacion() As DateTime
        Get
            Return _fechaCreacion
        End Get
        Set(ByVal value As DateTime)
            _fechaCreacion = value
        End Set
    End Property

    Public Property FechaIngreso() As DateTime
        Get
            Return _fechaIngreso
        End Get
        Set(ByVal value As DateTime)
            _fechaIngreso = value
        End Set
    End Property

    Public Property Estado() As Boolean
        Get
            Return _estado
        End Get
        Set(ByVal value As Boolean)
            _estado = value
        End Set
    End Property

End Class
