﻿Public Class BEInfoSiniestroC

    Private _idSiniestro As Int32
    Private _idInfoSiniestro As Int32
    Private _nombre As String
    Private _valorString As String
    Private _valorDate As DateTime
    Private _valorInt As Decimal

    Public Property IdSiniestro() As Int32
        Get
            Return _idSiniestro
        End Get
        Set(ByVal value As Int32)
            _idSiniestro = value
        End Set
    End Property

    Public Property IdInfoSiniestro() As Int32
        Get
            Return _idInfoSiniestro
        End Get
        Set(ByVal value As Int32)
            _idInfoSiniestro = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property


    Public Property ValorDate() As DateTime
        Get
            Return _valorDate
        End Get
        Set(ByVal value As DateTime)
            _valorDate = value
        End Set
    End Property

    Public Property ValorString() As String
        Get
            Return _valorString
        End Get
        Set(ByVal value As String)
            _valorString = value
        End Set
    End Property

    Public Property ValorInt() As String
        Get
            Return _valorInt
        End Get
        Set(ByVal value As String)
            _valorInt = value
        End Set
    End Property

End Class
