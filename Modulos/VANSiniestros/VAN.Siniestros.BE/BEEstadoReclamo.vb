﻿Public Class BEEstadoReclamo
    Inherits BEBase
    Private _iDEstadoReclamo As Integer
    Private _nombre As String
    Public Property IDEstadoReclamo() As Integer
        Get
            Return _iDEstadoReclamo
        End Get
        Set(ByVal value As Integer)
            _iDEstadoReclamo = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

End Class
