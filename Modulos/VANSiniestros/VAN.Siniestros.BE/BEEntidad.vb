Public Class BEEntidad
    Inherits BEBase

    Private _iDEntidad As Integer = 0
    Private _descripcion As String = String.Empty
    Private _usuario As String = String.Empty

    Public Property Usuario() As String
        Get
            Return Me._usuario
        End Get
        Set(ByVal value As String)
            Me._usuario = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._descripcion
        End Get
        Set(ByVal value As String)
            Me._descripcion = value
        End Set
    End Property

    Public Property IDEntidad() As Integer
        Get
            Return _iDEntidad
        End Get
        Set(ByVal value As Integer)
            _iDEntidad = value
        End Set
    End Property

End Class
