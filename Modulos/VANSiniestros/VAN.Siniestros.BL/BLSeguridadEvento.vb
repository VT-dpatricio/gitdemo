﻿Imports VAN.Siniestros.BE
Imports VAN.Siniestros.DL
Imports System
Imports System.Collections
Public Class BLSeguridadEvento

    Public Function Insertar(ByVal pEntidad As BEBase) As Boolean
        Dim oDL As New DLTSeguridadEvento
        Return oDL.Insertar(pEntidad)
    End Function

    Public Function PrimerLogin(ByVal pIDUsuario As String) As Boolean
        Dim oDL As New DLNTSeguridadEvento
        Dim oBE As BESeguridadEvento = oDL.SeleccionarUltimo(pIDUsuario, 1)
        If (oBE.IDEvento = 0) Then
            'Si no existe ningún inicio de sesión
            Return True
        Else
            Return False
        End If
    End Function

    Public Function ClaveCaducada(ByVal pIDUsuario As String, ByVal pNroDiasCaduca As Integer) As Boolean
        Dim oDL As New DLNTSeguridadEvento
        Dim oBE As BESeguridadEvento = oDL.SeleccionarUltimo(pIDUsuario, 3)
        If (DateDiff(("d"), oBE.FechaRegistro, Date.Now) > pNroDiasCaduca) Then
            'Si el número de días desde el último cambio de contraseña es mayor a los días en que la contraseña caduca
            Return True
        Else
            Return False
        End If
    End Function

    Public Function ClaveModificadaHoy(ByVal pIDUsuario As String) As Boolean
        Dim oDL As New DLNTSeguridadEvento
        Dim oBE As BESeguridadEvento = oDL.SeleccionarUltimo(pIDUsuario, 3)
        If (oBE.IDEvento <> 0) Then
            If oBE.FechaRegistro.ToString("dd/MM/yyyy") = Date.Now.ToString("dd/MM/yyyy") Then
                Return True
            End If
        Else
            Return False
        End If
    End Function


End Class
