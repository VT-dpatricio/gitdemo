Imports VAN.Siniestros.BE
Imports VAN.Siniestros.DL
Imports System
Imports System.Collections
Public Class BLCobro
    Public Function Seleccionar(ByVal pIDCertificado As String, ByVal pIDEstadoCobro As Int32) As IList
        Dim oDL As New DLNTCobro
        Dim oBE As New BECobro
        oBE.IdCertificado = pIDCertificado
        oBE.IdEstadoCobro = pIDEstadoCobro
        Return oDL.Seleccionar(oBE)
    End Function
End Class
