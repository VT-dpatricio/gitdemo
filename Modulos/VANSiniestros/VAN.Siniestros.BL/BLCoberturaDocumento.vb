﻿Imports VAN.Siniestros.BE
Imports VAN.Siniestros.DL
Imports System
Imports System.Collections
Public Class BLCoberturaDocumento
    Public Function Listar(ByVal pIDCobertura As String) As IList
        Dim oDL As New DLNTCoberturaDocumento
        Dim oBE As New BECoberturaDocumento
        oBE.IDCoberturaPar = pIDCobertura
        Return oDL.Seleccionar(oBE)
    End Function
End Class
