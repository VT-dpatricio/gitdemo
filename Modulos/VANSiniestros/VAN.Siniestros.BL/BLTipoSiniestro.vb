﻿Imports VAN.Siniestros.BE
Imports VAN.Siniestros.DL
Imports System
Imports System.Collections
Public Class BLTipoSiniestro
    Public Function Listar(ByVal pIDProducto As Int32, ByVal pOpcion As String) As IList
        Dim oDL As New DLNTTipoSiniestro
        Dim oBE As New BETipoSiniestro
        oBE.IDProducto = pIDProducto
        oBE.Opcion = pOpcion
        Return oDL.Seleccionar(oBE)
    End Function
End Class
