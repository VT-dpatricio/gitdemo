﻿Imports VAN.Siniestros.BE
Imports VAN.Siniestros.DL
Imports System
Imports System.Collections

Public Class BLArchivo

    Public Function Seleccionar(ByVal pIDSiniestro As String) As IList
        Dim oDL As New DLNTArchivo
        Dim oBE As New BEArchivo
        oBE.IdSiniestro = pIDSiniestro
        Return oDL.Seleccionar(oBE)
    End Function

    Public Function Eliminar(ByVal pIDDocumento As Integer, ByVal IdUsuario As String) As Boolean
        Dim oDL As New DLNTArchivo
        Return oDL.Eliminar(pIDDocumento, IdUsuario)
    End Function

    Public Function Insertar(ByVal pEntidad As BEArchivo) As Boolean
        Dim oDL As New DLNTArchivo
        Return oDL.Insertar(pEntidad)
    End Function
End Class
