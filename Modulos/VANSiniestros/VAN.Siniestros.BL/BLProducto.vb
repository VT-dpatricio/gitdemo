﻿Imports VAN.Siniestros.BE
Imports VAN.Siniestros.DL
Imports System
Imports System.Collections

Public Class BLProducto

    Public Function Seleccionar(ByVal pIDEntidad As Integer, ByVal pIDUsuario As String) As IList
        Dim oDL As New DLNTProducto
        Dim oBE As New BEProducto
        oBE.IDEntidad = pIDEntidad
        oBE.IDUsuario = pIDUsuario
        oBE.Opcion = "SeleccionarActivos"
        Return oDL.Seleccionar(oBE)
    End Function

    Public Function SeleccionarTodos(ByVal pIDEntidad As Integer, ByVal pIDUsuario As String) As IList
        Dim oDL As New DLNTProducto
        Dim oBE As New BEProducto
        oBE.IDEntidad = pIDEntidad
        oBE.IDUsuario = pIDUsuario
        oBE.Opcion = "SeleccionarTodos"
        Return oDL.Seleccionar(oBE)
    End Function

End Class


