﻿Public Class BESeguridadEvento
    Inherits BEBase
    Private _iDEvento As Integer = 0
    Private _iDSistema As Integer = 0
    Private _iDUsuario As String = String.Empty
    Private _iDTipoEvento As Integer = 0
    Private _fechaRegistro As Date = Date.Now
    Private _detalle As String = String.Empty
    Private _host As String = String.Empty

    Public Property IDEvento() As Integer
        Get
            Return Me._iDEvento
        End Get
        Set(ByVal value As Integer)
            Me._iDEvento = value
        End Set
    End Property

    Public Property IDSistema() As Integer
        Get
            Return Me._iDSistema
        End Get
        Set(ByVal value As Integer)
            Me._iDSistema = value
        End Set
    End Property

    Public Property IDUsuario() As String
        Get
            Return Me._iDUsuario
        End Get
        Set(ByVal value As String)
            Me._iDUsuario = value
        End Set
    End Property

    Public Property IDTipoEvento() As Integer
        Get
            Return Me._iDTipoEvento
        End Get
        Set(ByVal value As Integer)
            Me._iDTipoEvento = value
        End Set
    End Property

    Public Property FechaRegistro() As Date
        Get
            Return Me._fechaRegistro
        End Get
        Set(ByVal value As Date)
            Me._fechaRegistro = value
        End Set
    End Property

    Public Property Detalle() As String
        Get
            Return Me._detalle
        End Get
        Set(ByVal value As String)
            Me._detalle = value
        End Set
    End Property

    Public Property Host() As String
        Get
            Return Me._host
        End Get
        Set(ByVal value As String)
            Me._host = value
        End Set
    End Property

    'Campos de Auditoria
    Private _usuarioCreacion As String = String.Empty
    Private _usuarioModificacion As String = String.Empty
    Private _fechaCreacion As DateTime
    Private _fechaModificacion As DateTime


    Public Property UsuarioCreacion() As String
        Get
            Return Me._usuarioCreacion
        End Get
        Set(ByVal value As String)
            Me._usuarioCreacion = value
        End Set
    End Property

    Public Property UsuarioModificacion() As String
        Get
            Return Me._usuarioModificacion
        End Get
        Set(ByVal value As String)
            Me._usuarioModificacion = value
        End Set
    End Property

    Public Property FechaCreacion() As DateTime
        Get
            Return Me._fechaCreacion
        End Get
        Set(ByVal value As DateTime)
            Me._fechaCreacion = value
        End Set
    End Property

    Public Property FechaModificacion() As DateTime
        Get
            Return Me._fechaModificacion
        End Get
        Set(ByVal value As DateTime)
            Me._fechaModificacion = value
        End Set
    End Property

End Class
