﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AONArchitectural.Common.Processor.Formatters;

namespace AONArchitectural.Common.Processor.Validation
{
    public class ValidationDescriptor
    {
        public string PropertyName { get; set; }

        public bool Mandatory { get; set; }

        public int Length { get; set; }

        public bool ValidateNegativeValue { get; set; }

        public bool AllowNegativeValue { get; set; }

        public List<Formatter> ListFormatter {get; set;}

        public int ErrorCodeReferenced { get; set; }

        public ValidationDescriptor()
        {
            this.Mandatory = false;
            this.Length = 0;
            this.ValidateNegativeValue = false;
            this.ListFormatter = null;
            this.ErrorCodeReferenced = 0;
        }

    }
}
