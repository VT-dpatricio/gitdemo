﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace AONArchitectural.Common.Processor.Validation
{
    public interface IValidator
    {
       
        string cadenaError { get; set; }
        string cadenaInforme { get; set; }
        bool IsOk { get; set; }
        void AddChildResult(IValidator r);
    }
}
