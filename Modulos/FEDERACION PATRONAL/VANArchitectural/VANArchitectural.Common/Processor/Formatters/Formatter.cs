﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace AONArchitectural.Common.Processor.Formatters
{
    public class Formatter : IFormatter
    {
        public int ErrorCodeReferenced { get; set; }

        public Formatter(): base()
        {
        }

        public Formatter(int errorCode)
        {
            this.ErrorCodeReferenced = errorCode;
        }

        public bool IsNumeric(string value)
        {
            float n;
            bool isNumeric = float.TryParse(value, out n);
            return isNumeric;
        }

        public virtual string Format(string value)
        {
            throw new NotImplementedException();
        }
    }
}
