﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace AONArchitectural.Common.Processor.Formatters
{
    public class StringNumericFormatter : Formatter
    {
        public StringNumericFormatter() : base() { }
        public StringNumericFormatter(int errorCode) : base(errorCode) { }
        
        public override string Format(string value)
        {
            string valorAux = value;
            if (!String.IsNullOrEmpty(value))
            {
                valorAux = long.Parse(value.Trim()).ToString();
            }
            
            //if (IsNumeric(value))
            //{
            //    valorAux = long.Parse(value.Trim()).ToString();
            //}

            return valorAux;
        }



    }
}
