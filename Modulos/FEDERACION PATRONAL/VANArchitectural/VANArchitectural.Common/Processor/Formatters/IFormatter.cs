﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace AONArchitectural.Common.Processor.Formatters
{
    public interface IFormatter
    {
        string Format(string value);
    }
}
