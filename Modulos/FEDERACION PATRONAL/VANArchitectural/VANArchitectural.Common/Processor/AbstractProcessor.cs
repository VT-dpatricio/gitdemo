﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using AONArchitectural.Common.Processor.Flow;
using AONArchitectural.Common.Processor.Formatters;
using AONArchitectural.Common.Processor.Validation;
using AONArchitectural.Common.DataDynamic;

namespace AONArchitectural.Common.Processor
{
    public class AbstractProcessor : IProcessFlow
    {

        protected Dictionary<string, List<IFormatter>> Formatters { get; set; }

        protected Dictionary<string, ValidationDescriptor> ValidatorsDescriptors { get; set; }

        protected Dictionary<string, List<ValidationDescriptor>> GroupValidatorsDescriptors { get; set; }

        public TransformDataDynamic Transformer { get; set; }

        public virtual Validation.IValidator PreProcess()
        {
            throw new NotImplementedException();
        }

        public virtual Validation.IValidator Process()
        {
            throw new NotImplementedException();
        }

        public virtual Validation.IValidator PostProcess()
        {
            throw new NotImplementedException();
        }
    }
}
