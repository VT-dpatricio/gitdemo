﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AONArchitectural.Common.Constants;
using log4net;
using log4net.Config;
using System.IO;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace AONArchitectural.Common.Logger
{
    public class Logger
    {
        private ILog Log4j { get; set; }

        public void AddProperty(string propertyName, string value)
        {
            log4net.LogicalThreadContext.Properties[propertyName] = value;
        }

        public Logger(Type type)
        {
            Log4j = log4net.LogManager.GetLogger(type);
        }
       
        public Logger(string loggerName)
        {
            Log4j = log4net.LogManager.GetLogger(loggerName);
        }

        public void E(string msg, Exception e)
        {
            E(msg + "." + e.Message);
        }

        public void E(Exception e)
        {
            E(e.ToString());
        }

        public void E(string msg)
        {
            Log4j.Error(msg);
        }


        public void W(string msg)
        {
            Log4j.Warn(msg);
        }

        public void I(string msg)
        {
            Log4j.Info(msg);
        }
    }
}
