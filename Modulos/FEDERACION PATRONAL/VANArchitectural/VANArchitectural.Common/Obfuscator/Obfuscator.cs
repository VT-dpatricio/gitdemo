﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AONArchitectural.Common.Processor.Formatters;
using System.Numerics;
namespace AONArchitectural.Common.Obfuscator
{
    public static class Obfuscator
    {
        public static string AccountNumber(string account, char obfuscatorChar ='*')
        {
            
            string newValue = string.Empty;
            try{
                if (!string.IsNullOrEmpty(account))
                {
                    account = account.Replace(obfuscatorChar, '0');
                    DeleteZeroFormatter formatter = new DeleteZeroFormatter();
                    account = formatter.Format(account);
                    BigInteger nAccount;

                    if (!BigInteger.TryParse(account, out nAccount))
                    {
                        return account;
                    }

                    if (account.Length <= 10)
                    {
                        StringBuilder bd = new StringBuilder("");
                        bd.Append(obfuscatorChar, account.Length - 4);
                        bd.Append(account.Substring(account.Length - 4));
                        newValue = bd.ToString();
                    }
                    else
                    {
                        if (account.Length > 10)
                        {
                            string inicio = account.Substring(0, 4);
                            string fin = account.Substring(account.Length - 4, 4);
                            StringBuilder builder = new StringBuilder("");
                            builder.Append(inicio);
                            builder.Append(obfuscatorChar, account.Length - 8);
                            builder.Append(fin);
                            newValue = builder.ToString();
                        }

                        if (newValue != null && account != null && newValue.Length < account.Length)
                        {
                            if (account.Length > 4)
                            {
                                StringBuilder bd = new StringBuilder("");
                                bd.Append(obfuscatorChar, account.Length - 4);
                                bd.Append(account.Substring(account.Length - 4));
                                newValue = bd.ToString();
                            }
                            else
                            {
                                newValue = account;
                            }
                        }
                    }
                    
                }
            }catch(Exception ex)
            {
                newValue = account;
            }

            /*Asegurarse que siempre el valor que retorne quede diferente del recibido
	        En caso de ser iguales modifica un caracter más*/
            try
            {
                if (!String.IsNullOrEmpty(account) && !String.IsNullOrEmpty(newValue) && String.Equals(account, newValue))
                {
                    int chatsToReplace = Convert.ToInt32(Math.Floor(Convert.ToDouble(newValue.Length / 2)));
                    int countChanges = 0;
                    for (int i = 1; i <= newValue.Length; i++)
                    {
                        char strChar = newValue[i - 1];
                        if (strChar != obfuscatorChar)
                        {
                            StringBuilder sb = new StringBuilder(newValue);
                            sb[i -1] = obfuscatorChar;
                            newValue = sb.ToString();
                            countChanges++;
                            if (chatsToReplace == countChanges)
                            {
                                break;
                            }
                        }
                    }
                    if (countChanges == 0)
                    {
                        newValue = new String(obfuscatorChar, 16);
                    }
                }
            }
            catch (Exception ex)
            {
                newValue = new String(obfuscatorChar, 16);
            }
           
            return newValue;
        }
    }
}
