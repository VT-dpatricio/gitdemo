﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace AONArchitectural.Common.DataDynamic
{
    public class BLDataDynamic
    {

        public DataTable GetEstructuraListaKeyValue()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("NameKey").DataType = System.Type.GetType("System.String");
            dt.Columns.Add("value").DataType = System.Type.GetType("System.String");
            dt.Columns.Add("DataType").DataType = System.Type.GetType("System.String");
            dt.Columns.Add("Source").DataType = System.Type.GetType("System.String");
            return dt;
        }
    }
}
