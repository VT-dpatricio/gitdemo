﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;


namespace AONArchitectural.Common.DataDynamic
{
    public class TransformDataDynamic
    {
        public DataTable transformDataTable(DataTable tableEstructure, ContainerDataDynamic container)
        {
            if (container != null)
            {
                foreach (BEDataDynamic eEddym in container.dataVariable)
                {
                    DataRow dr = tableEstructure.NewRow();

                    dr["nameKey"] = eEddym.NameKey;
                    dr["value"] = eEddym.Value;
                    dr["dataType"] = eEddym.DataType;
                    dr["source"] = eEddym.Source;
                    tableEstructure.Rows.Add(dr);
                }
            }
           
            return tableEstructure;
        }

        public DataSet transformDataSet(DataTable tableEstructure, ContainerDataDynamic container)
        {
            DataSet dsDataValue = new DataSet();
            foreach (BEDataDynamic eEddym in container.dataVariable)
            {
                DataRow dr = tableEstructure.NewRow();

                dr["key"] = eEddym.NameKey;
                dr["value"] = eEddym.Value;
                dr["type"] = eEddym.DataType;
                dr["source"] = eEddym.Source;
                tableEstructure.Rows.Add(dr);
            }
            dsDataValue.Tables.Add(tableEstructure);
            return dsDataValue;
        }
    }
}
