﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;

namespace AONArchitectural.Common.DataDynamic
{
    public class BEDataDynamic
    {
        public string _nameKey;
        public string _value;
        public string _dataType;
        public string _source;

        public string NameKey
        {
            get { return _nameKey; }
            set { _nameKey = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public string DataType
        {
            get { return _dataType; }
            set { _dataType = value; }
        }

        public string Source
        {
            get { return _source; }
            set { _source = value; }
        }
    }


}
