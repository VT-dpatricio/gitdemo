Imports VAN.Consulta.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLNTCobroLog
    Inherits DLBase
    Implements IDLNT

    Public Function Seleccionar() As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pEntidad As BE.BEBase) As IList Implements IDLNT.Seleccionar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SC_SeleccionarCobroLog", cn)
        cm.CommandType = CommandType.StoredProcedure
        Dim oBE As BECobroLog = DirectCast(pEntidad, BECobroLog)
        cm.Parameters.Add("@IdCobro", SqlDbType.Int).Value = oBE.IdCobro
        cm.Parameters.Add("@IDCertificado", SqlDbType.VarChar, 25).Value = oBE.IdCertificado
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BECobroLog
                oBE.IdCertificado = rd.GetString(rd.GetOrdinal("IdCertificado"))
                oBE.IdCobro = rd.GetInt32(rd.GetOrdinal("IdCobro"))
                oBE.MedioPago = rd.GetString(rd.GetOrdinal("MedioPago"))
                oBE.NumeroCuenta = rd.GetString(rd.GetOrdinal("numeroCuenta"))
                oBE.Valor = rd.GetDecimal(rd.GetOrdinal("valor"))
                oBE.FechaCobro = rd.GetDateTime(rd.GetOrdinal("fechaCobro"))
                oBE.FechaUltimoCobro = rd.GetDateTime(rd.GetOrdinal("fechaUltimoCobro"))
                oBE.EstadoCobro = rd.GetString(rd.GetOrdinal("EstadoCobro"))
                'oBE.InicioPeriodo = rd.GetDateTime(rd.GetOrdinal("inicioPeriodo"))
                'oBE.FinPeriodo = rd.GetDateTime(rd.GetOrdinal("finPeriodo"))
                'oBE.FechaProceso = rd.GetString(rd.GetOrdinal("fechaproceso"))
                oBE.ValorMoneda = rd.GetString(rd.GetOrdinal("ValorMoneda"))
                oBE.Referencia = rd.GetInt32(rd.GetOrdinal("Referencia"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BEBase Implements IDLNT.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function

End Class
