﻿Imports VAN.Consulta.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLNTProductoHogar
    Inherits DLBase
    Implements IDLNT


    Public Function Seleccionar() As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(pEntidad As BEBase) As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(pCodigo As Integer) As BEBase Implements IDLNT.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(pCodigo As Integer) As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(pCodigo As String) As IList
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SC_SeleccionarProductoHogar", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@idCertificado", SqlDbType.VarChar, 25).Value = pCodigo.Trim()
        Dim oBE As BEProductoHogar
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            oBE = New BEProductoHogar
            Do While rd.Read
                oBE.IdCertificado = rd.GetString(rd.GetOrdinal("idCertificado"))

                Select Case rd.GetString(rd.GetOrdinal("nombre"))
                    Case "ObservacionH"
                        oBE.DetalleBienes = rd.GetString(rd.GetOrdinal("valorString")) 'DetalleBienes
                        oBE.DetalleBienesOblig = rd.GetBoolean(rd.GetOrdinal("Obligatorio")) 'DetalleBienesOblig
                    Case "SuperficieComercio"
                        oBE.Superficie = rd.GetDecimal(rd.GetOrdinal("valorNum")) 'Superficie
                        oBE.SuperficieOblig = rd.GetBoolean(rd.GetOrdinal("Obligatorio")) 'SuperficieOblig
                    Case "C_IdRamo"
                        oBE.IdRamo = rd.GetDecimal(rd.GetOrdinal("valorNum")) 'IdRamo
                        oBE.RamoOblig = rd.GetBoolean(rd.GetOrdinal("Obligatorio")) 'RamoOblig
                End Select
            Loop
            lista.Add(oBE)
            oBE = Nothing
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function
End Class
