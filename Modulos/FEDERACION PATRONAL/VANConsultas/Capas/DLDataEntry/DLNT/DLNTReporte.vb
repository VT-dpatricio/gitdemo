﻿Imports VAN.Consulta.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLNTReporte
    Inherits DLBase

    Public Function SeleccionarNota(ByVal pID As String) As BEReporte
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SS_NotaReclamo", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@IDReclamo", SqlDbType.Int).Value = CInt(pID)
        Dim oBE As New BEReporte
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            If rd.Read Then
                oBE.IdCertificado = rd.GetString(rd.GetOrdinal("IdCertificado"))
                oBE.IdProducto = rd.GetInt32(rd.GetOrdinal("IdProducto"))
                oBE.Asegurado = rd.GetString(rd.GetOrdinal("Asegurado"))
                oBE.Aseguradora = rd.GetString(rd.GetOrdinal("Aseguradora"))
                oBE.Dni = rd.GetString(rd.GetOrdinal("dniAseg"))
                oBE.EstadoReclamo = rd.GetString(rd.GetOrdinal("EstadoReclamo"))
                oBE.MotivoReclamo = rd.GetString(rd.GetOrdinal("MotivoReclamo"))
                oBE.NombreProducto = rd.GetString(rd.GetOrdinal("NombreProducto"))
                oBE.NroFisico = rd.GetString(rd.GetOrdinal("NroFisico"))
                oBE.NroTicket = rd.GetInt32(rd.GetOrdinal("NroTicket"))
                oBE.Ramo = rd.GetString(rd.GetOrdinal("Ramo"))
            End If
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return oBE
    End Function
End Class
