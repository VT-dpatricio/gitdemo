Imports VAN.Consulta.BE
Imports VAN.Consulta.DL
Imports System
Imports System.Collections
Public Class BLMenuOperacion
    Public Function Seleccionar(ByVal pIDMenu As Integer, ByVal pIDUsuario As String) As BEOperacion
        Dim oDL As New DLNTMenuOperacion
        Dim oBEMOperacion As New BEMenuOperacion
        Dim oBEOperacion As New BEOperacion
        Dim oLMOperacion As IList = oDL.Seleccionar(pIDMenu, pIDUsuario)
        '1: Modificar
        '2: Anular
        '3: Activar
        '4: Consultar
        For Each oBEMOperacion In oLMOperacion
            If (oBEMOperacion.IDOperacion = 1) Then
                oBEOperacion.Modificar = True
            End If
            If (oBEMOperacion.IDOperacion = 2) Then
                oBEOperacion.Anular = True
            End If
            If (oBEMOperacion.IDOperacion = 3) Then
                oBEOperacion.Activar = True
            End If
            If (oBEMOperacion.IDOperacion = 4) Then
                oBEOperacion.Consultar = True
            End If
            If (oBEMOperacion.IDOperacion = 5) Then
                oBEOperacion.SubirImagen = True
            End If
            If (oBEMOperacion.IDOperacion = 6) Then
                oBEOperacion.Imprimir = True
            End If
        Next oBEMOperacion

        Return oBEOperacion
    End Function
End Class
