Imports VAN.Consulta.BE
Imports VAN.Consulta.DL
Imports System
Imports System.Collections
Public Class BLOpcionPrima
    Public Function Seleccionar(ByVal pIdProducto As Int32, ByVal pOpcion As String, ByVal pIdFrecuencia As Int32) As IList
        Dim oDL As New DLNTOpcionPrima
        Dim oBE As New BEOpcionPrima
        oBE.IdProducto = pIdProducto
        oBE.Opcion = pOpcion
        oBE.IdFrecuencia = pIdFrecuencia
        Return oDL.Seleccionar(oBE)
    End Function
End Class
