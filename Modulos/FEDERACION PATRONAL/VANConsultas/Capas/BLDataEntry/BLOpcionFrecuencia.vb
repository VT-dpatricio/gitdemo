Imports VAN.Consulta.BE
Imports VAN.Consulta.DL
Imports System
Imports System.Collections
Public Class BLOpcionFrecuencia
    Public Function Seleccionar(ByVal pIdProducto As Int32, ByVal pOpcion As String) As IList
        Dim oDL As New DLNTOpcionFrecuencia
        Dim oBE As New BEOpcionFrecuencia
        oBE.IdProducto = pIdProducto
        oBE.Opcion = pOpcion
        Return oDL.Seleccionar(oBE)
    End Function
End Class
