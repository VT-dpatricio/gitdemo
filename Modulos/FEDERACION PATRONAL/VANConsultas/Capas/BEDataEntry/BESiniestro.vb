﻿Public Class BESiniestro
    Inherits BEBase
    Private _NroTicket As String
    Public Property NroTicket() As String
        Get
            Return _NroTicket
        End Get
        Set(ByVal value As String)
            _NroTicket = value
        End Set
    End Property

    Private _iDSiniestro As Integer
    Public Property IDSiniestro() As Integer
        Get
            Return _iDSiniestro
        End Get
        Set(ByVal value As Integer)
            _iDSiniestro = value
        End Set
    End Property

    Private _iDCertificado As String
    Public Property IDCertificado() As String
        Get
            Return _iDCertificado
        End Get
        Set(ByVal value As String)
            _iDCertificado = value
        End Set
    End Property

    Private _iDTipoSiniestro As Integer
    Public Property IDTipoSiniestro() As Integer
        Get
            Return _iDTipoSiniestro
        End Get
        Set(ByVal value As Integer)
            _iDTipoSiniestro = value
        End Set
    End Property

    Private _tipoSiniestro As String
    Public Property TipoSiniestro() As String
        Get
            Return _tipoSiniestro
        End Get
        Set(ByVal value As String)
            _tipoSiniestro = value
        End Set
    End Property

    Private _iDEstadoSiniestro As Integer
    Public Property IDEstadoSiniestro() As Integer
        Get
            Return _iDEstadoSiniestro
        End Get
        Set(ByVal value As Integer)
            _iDEstadoSiniestro = value
        End Set
    End Property

    Private _fechaRegSiniestro As Date
    Public Property FechaRegSiniestro() As Date
        Get
            Return _fechaRegSiniestro
        End Get
        Set(ByVal value As Date)
            _fechaRegSiniestro = value
        End Set
    End Property

    Private _fechaSiniestro As Date
    Public Property FechaSiniestro() As String
        Get
            Return _fechaSiniestro
        End Get
        Set(ByVal value As String)
            _fechaSiniestro = value
        End Set
    End Property

    Private _montoSiniestro As Double
    Public Property MontoSiniestro() As Double
        Get
            Return _montoSiniestro
        End Get
        Set(ByVal value As Double)
            _montoSiniestro = value
        End Set
    End Property

    Private _iDMonedaMSiniestro As String
    Public Property IDMonedaMSiniestro() As String
        Get
            Return _iDMonedaMSiniestro
        End Get
        Set(ByVal value As String)
            _iDMonedaMSiniestro = value
        End Set
    End Property

    Private _montoPago As Double
    Public Property MontoPago() As Double
        Get
            Return _montoPago
        End Get
        Set(ByVal value As Double)
            _montoPago = value
        End Set
    End Property

    Private _iDMonedaMPago As String
    Public Property IDMonedaMPago() As String
        Get
            Return _iDMonedaMPago
        End Get
        Set(ByVal value As String)
            _iDMonedaMPago = value
        End Set
    End Property

    Private _montoPagoAse As Double
    Public Property MontoPagoAse() As Double
        Get
            Return _montoPagoAse
        End Get
        Set(ByVal value As Double)
            _montoPagoAse = value
        End Set
    End Property

    Private _iDMonedaMPagoAse As String
    Public Property IDMonedaMPagoAse() As String
        Get
            Return _iDMonedaMPagoAse
        End Get
        Set(ByVal value As String)
            _iDMonedaMPagoAse = value
        End Set
    End Property

    Private _fechaDocCompleta As Date
    Public Property FechaDocCompleta() As Date
        Get
            Return _fechaDocCompleta
        End Get
        Set(ByVal value As Date)
            _fechaDocCompleta = value
        End Set
    End Property

    Private _aseDireccion As String
    Public Property AseDireccion() As String
        Get
            Return _aseDireccion
        End Get
        Set(ByVal value As String)
            _aseDireccion = value
        End Set
    End Property

    Private _aseTelefono As String
    Public Property AseTelefono() As String
        Get
            Return _aseTelefono
        End Get
        Set(ByVal value As String)
            _aseTelefono = value
        End Set
    End Property

    Private _aseEmail As String
    Public Property AseEmail() As String
        Get
            Return _aseEmail
        End Get
        Set(ByVal value As String)
            _aseEmail = value
        End Set
    End Property

    Private _aseObservacion As String
    Public Property AseObservacion() As String
        Get
            Return _aseObservacion
        End Get
        Set(ByVal value As String)
            _aseObservacion = value
        End Set
    End Property

    Private _listaCobertura As ArrayList
    Public Property ListaCobertura() As ArrayList
        Get
            Return _listaCobertura
        End Get
        Set(ByVal value As ArrayList)
            _listaCobertura = value
        End Set
    End Property

    Private _estadoSiniestro As String
    Public Property EstadoSiniestro() As String
        Get
            Return _estadoSiniestro
        End Get
        Set(ByVal value As String)
            _estadoSiniestro = value
        End Set
    End Property

    Private _parametro1 As String
    Public Property Parametro1() As String
        Get
            Return _parametro1
        End Get
        Set(ByVal value As String)
            _parametro1 = value
        End Set
    End Property

    Private _parametro2 As String
    Public Property Parametro2() As String
        Get
            Return _parametro2
        End Get
        Set(ByVal value As String)
            _parametro2 = value
        End Set
    End Property

    Dim _idFiltro As String
    Public Property IDFiltro() As String
        Get
            Return _idFiltro
        End Get
        Set(ByVal value As String)
            _idFiltro = value
        End Set
    End Property

    Dim _producto As String
    Public Property Producto() As String
        Get
            Return _producto
        End Get
        Set(ByVal value As String)
            _producto = value
        End Set
    End Property

    Dim _tipoDocumento As String
    Public Property TipoDocumento() As String
        Get
            Return _tipoDocumento
        End Get
        Set(ByVal value As String)
            _tipoDocumento = value
        End Set
    End Property

    Dim _ccCliente As String
    Public Property CcCliente() As String
        Get
            Return _ccCliente
        End Get
        Set(ByVal value As String)
            _ccCliente = value
        End Set
    End Property

    Private _nombre1 As String
    Public Property Nombre1() As String
        Get
            Return _nombre1
        End Get
        Set(ByVal value As String)
            _nombre1 = value
        End Set
    End Property

    Private _apellido1 As String
    Public Property Apellido1() As String
        Get
            Return _apellido1
        End Get
        Set(ByVal value As String)
            _apellido1 = value
        End Set
    End Property

    Private _observacionEvaluacion As String
    Public Property ObservacionEvaluacion() As String
        Get
            Return _observacionEvaluacion
        End Get
        Set(ByVal value As String)
            _observacionEvaluacion = value
        End Set
    End Property


    Private _usuarioCreacion As String
    Public Property UsuarioCreacion() As String
        Get
            Return _usuarioCreacion
        End Get
        Set(ByVal value As String)
            _usuarioCreacion = value
        End Set
    End Property

    Private _fechaCreacion As Date
    Public Property FechaCreacion() As Date
        Get
            Return _fechaCreacion
        End Get
        Set(ByVal value As Date)
            _fechaCreacion = value
        End Set
    End Property

    Private _usuarioModificacion As String
    Public Property UsuarioModificacion() As String
        Get
            Return _usuarioModificacion
        End Get
        Set(ByVal value As String)
            _usuarioModificacion = value
        End Set
    End Property

    Private _fechaModificacion As Date
    Public Property FechaModificacion() As Date
        Get
            Return _fechaModificacion
        End Get
        Set(ByVal value As Date)
            _fechaModificacion = value
        End Set
    End Property

    Private _identidad As Int32
    Public Property IDEntidad() As String
        Get
            Return _identidad
        End Get
        Set(ByVal value As String)
            _identidad = value
        End Set
    End Property

    Private _usuario As String
    Public Property Usuario() As String
        Get
            Return Me._usuario
        End Get
        Set(ByVal value As String)
            Me._usuario = value
        End Set
    End Property


End Class
