﻿Imports System


Public Class BEUsuario
    Inherits BEBase
    ' Methods
    Public Property Apellido() As String
        Get
            Return _apellido
        End Get
        Set(ByVal value As String)
            _apellido = value
        End Set
    End Property

    Public Property Buscar() As String
        Get
            Return _buscar
        End Get
        Set(ByVal value As String)
            _buscar = value
        End Set
    End Property

    Public Property Clave() As String
        Get
            Return _clave
        End Get
        Set(ByVal value As String)
            _clave = value
        End Set
    End Property

    Public Property DescripcionCargo() As String
        Get
            Return _descripcionCargo
        End Get
        Set(ByVal value As String)
            _descripcionCargo = value
        End Set
    End Property

    Public Property DescripcionRol() As String
        Get
            Return _descripcionRol
        End Get
        Set(ByVal value As String)
            _descripcionRol = value
        End Set
    End Property

    Public Property DNI() As String
        Get
            Return _dNI
        End Get
        Set(ByVal value As String)
            _dNI = value
        End Set
    End Property

    Public Property Eliminado() As Boolean
        Get
            Return _eliminado
        End Get
        Set(ByVal value As Boolean)
            _eliminado = value
        End Set
    End Property

    Public Property Email() As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property

    Public Property Estado() As Boolean
        Get
            Return _estado
        End Get
        Set(ByVal value As Boolean)
            _estado = value
        End Set
    End Property

    Public Property IDCargo() As Integer
        Get
            Return _iDCargo
        End Get
        Set(ByVal value As Integer)
            _iDCargo = value
        End Set
    End Property

    Public Property IDRol() As Integer
        Get
            Return _iDRol
        End Get
        Set(ByVal value As Integer)
            _iDRol = value
        End Set
    End Property

    Public Property IDUsuario() As String
        Get
            Return _iDUsuario
        End Get
        Set(ByVal value As String)
            _iDUsuario = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return _usuario
        End Get
        Set(ByVal value As String)
            _usuario = value
        End Set
    End Property

    Public Property IDEntidad() As Integer
        Get
            Return _iDEntidad
        End Get
        Set(ByVal value As Integer)
            _iDEntidad = value
        End Set
    End Property

    Public Property IDInformador() As String
        Get
            Return _iDInformador
        End Get
        Set(ByVal value As String)
            _iDInformador = value
        End Set
    End Property

    Public Property Apellido1() As String
        Get
            Return _apellido1
        End Get
        Set(ByVal value As String)
            _apellido1 = value
        End Set
    End Property

    Public Property Apellido2() As String
        Get
            Return _apellido2
        End Get
        Set(ByVal value As String)
            _apellido2 = value
        End Set
    End Property

    Public Property Nombre1() As String
        Get
            Return _nombre1
        End Get
        Set(ByVal value As String)
            _nombre1 = value
        End Set
    End Property

    Public Property Nombre2() As String
        Get
            Return _nombre2
        End Get
        Set(ByVal value As String)
            _nombre2 = value
        End Set
    End Property

    Public Property IDOficina() As Integer
        Get
            Return _iDOficina
        End Get
        Set(ByVal value As Integer)
            _iDOficina = value
        End Set
    End Property

    Public Property IDCanal() As Integer
        Get
            Return _iDCanal
        End Get
        Set(ByVal value As Integer)
            _iDCanal = value
        End Set
    End Property

    Public Property Acceso() As Boolean
        Get
            Return _acceso
        End Get
        Set(ByVal value As Boolean)
            _acceso = value
        End Set
    End Property

    ' Fields
    Private _apellido As String = String.Empty
    Private _buscar As String
    Private _clave As String = String.Empty
    Private _descripcionCargo As String = String.Empty
    Private _descripcionRol As String = String.Empty
    Private _dNI As String = String.Empty
    Private _eliminado As Boolean = True
    Private _email As String = String.Empty

    'Private _iDCargo As Integer = 0
    Private _iDRol As Integer = 0
    Private _iDUsuario As String = String.Empty
    Private _nombre As String = String.Empty
    Private _usuario As String = String.Empty

    Private _iDInformador As String = String.Empty
    Private _apellido1 As String = String.Empty
    Private _apellido2 As String = String.Empty
    Private _nombre1 As String = String.Empty
    Private _nombre2 As String = String.Empty

    Private _iDEntidad As Integer = 0 '--
    Private _iDOficina As Integer = 0
    Private _iDCargo As Integer = 0 '--
    Private _iDCanal As Integer = 0
    Private _estado As Boolean = True '--
    Private _acceso As Boolean = False
End Class

