Public Class BEOpcion
    Inherits BEBase

    Private _idProducto As Int32
    Private _opcion As String
    Private _activo As Boolean
    Private _puntos As Int32
    Private _puntosvalor As Decimal

    Public Property IdProducto() As Int32
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Int32)
            _idProducto = value
        End Set
    End Property

    Public Property Opcion() As String
        Get
            Return _opcion
        End Get
        Set(ByVal value As String)
            _opcion = value
        End Set
    End Property

    Public Property Activo() As Boolean
        Get
            Return _activo
        End Get
        Set(ByVal value As Boolean)
            _activo = value
        End Set
    End Property

    Public Property Puntos() As Int32
        Get
            Return _puntos
        End Get
        Set(ByVal value As Int32)
            _puntos = value
        End Set
    End Property

    Public Property Puntosvalor() As Decimal
        Get
            Return _puntosvalor
        End Get
        Set(ByVal value As Decimal)
            _puntosvalor = value
        End Set
    End Property
End Class
