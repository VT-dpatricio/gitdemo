Public Class BEAsegurado
    Inherits BEBase
    Private _idCertificado As String
    Private _consecutivo As Int16
    Private _idTipoDocumento As String
    Private _ccAseg As String
    Private _nombre1 As String
    Private _nombre2 As String
    Private _apellido1 As String
    Private _apellido2 As String
    Private _direccion As String
    Private _telefono As String
    Private _idCiudad As Int32
    Private _iDDepartamento As Int32
    Private _iDProvincia As Int32
    Private _iDPais As Int32
    Private _iDPaisCer As Int32
    Private _fechaNacimiento As DateTime
    Private _idParentesco As Int16
    Private _activo As Boolean
    Private _sexo As String
    Private _estadoCivil As String
    Private _tipoDocumento As String
    Private _estado As String
    Private _plan As String
    Private _prima As Decimal
    Private _usuarioModificacion As String
    Private _observacion As String = String.Empty
    Private _ciudad As String = String.Empty
    Private _provincia As String = String.Empty
    Private _DirNumero As String = String.Empty
    Private _DirPiso As String = String.Empty
    Private _DirDpto As String = String.Empty
    Private _DirCodPostal As String = String.Empty
    Private _CodAreaTelefono As String = String.Empty

    Public Property IdCertificado() As String
        Get
            Return _idCertificado
        End Get
        Set(ByVal value As String)
            _idCertificado = value
        End Set
    End Property

    Public Property Consecutivo() As Int16
        Get
            Return _consecutivo
        End Get
        Set(ByVal value As Int16)
            _consecutivo = value
        End Set
    End Property

    Public Property IdTipoDocumento() As String
        Get
            Return _idTipoDocumento
        End Get
        Set(ByVal value As String)
            _idTipoDocumento = value
        End Set
    End Property

    Public Property CcAseg() As String
        Get
            Return _ccAseg
        End Get
        Set(ByVal value As String)
            _ccAseg = value
        End Set
    End Property

    Public Property Nombre1() As String
        Get
            Return _nombre1
        End Get
        Set(ByVal value As String)
            _nombre1 = value
        End Set
    End Property

    Public Property Nombre2() As String
        Get
            Return _nombre2
        End Get
        Set(ByVal value As String)
            _nombre2 = value
        End Set
    End Property

    Public Property Apellido1() As String
        Get
            Return _apellido1
        End Get
        Set(ByVal value As String)
            _apellido1 = value
        End Set
    End Property

    Public Property Apellido2() As String
        Get
            Return _apellido2
        End Get
        Set(ByVal value As String)
            _apellido2 = value
        End Set
    End Property

    Public Property Direccion() As String
        Get
            Return _direccion
        End Get
        Set(ByVal value As String)
            _direccion = value
        End Set
    End Property

    Public Property Telefono() As String
        Get
            Return _telefono
        End Get
        Set(ByVal value As String)
            _telefono = value
        End Set
    End Property

    Public Property IdCiudad() As Int32
        Get
            Return _idCiudad
        End Get
        Set(ByVal value As Int32)
            _idCiudad = value
        End Set
    End Property

    Public Property FechaNacimiento() As DateTime
        Get
            Return _fechaNacimiento
        End Get
        Set(ByVal value As DateTime)
            _fechaNacimiento = value
        End Set
    End Property

    Public Property IdParentesco() As Int16
        Get
            Return _idParentesco
        End Get
        Set(ByVal value As Int16)
            _idParentesco = value
        End Set
    End Property

    Public Property Activo() As Boolean
        Get
            Return _activo
        End Get
        Set(ByVal value As Boolean)
            _activo = value
        End Set
    End Property

    Public Property Sexo() As String
        Get
            Return _sexo
        End Get
        Set(ByVal value As String)
            _sexo = value
        End Set
    End Property

    Public Property EstadoCivil() As String
        Get
            Return _estadoCivil
        End Get
        Set(ByVal value As String)
            _estadoCivil = value
        End Set
    End Property

    Public Property TipoDocumento() As String
        Get
            Return _tipoDocumento
        End Get
        Set(ByVal value As String)
            _tipoDocumento = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return _estado
        End Get
        Set(ByVal value As String)
            _estado = value
        End Set
    End Property

    Public Property Plan() As String
        Get
            Return _plan
        End Get
        Set(ByVal value As String)
            _plan = value
        End Set
    End Property

    Public Property Prima() As Decimal
        Get
            Return _prima
        End Get
        Set(ByVal value As Decimal)
            _prima = value
        End Set
    End Property

    Public Property UsuarioModificacion() As String
        Get
            Return _usuarioModificacion
        End Get
        Set(ByVal value As String)
            _usuarioModificacion = value
        End Set
    End Property

    Public Property Observacion() As String
        Get
            Return _observacion
        End Get
        Set(ByVal value As String)
            _observacion = value
        End Set
    End Property

    Public Property IDPaisCer() As Int32
        Get
            Return _iDPaisCer
        End Get
        Set(ByVal value As Int32)
            _iDPaisCer = value
        End Set
    End Property

    Public Property IDDepartamento() As Int32
        Get
            Return _iDDepartamento
        End Get
        Set(ByVal value As Int32)
            _iDDepartamento = value
        End Set
    End Property

    Public Property IDProvincia() As Int32
        Get
            Return _iDProvincia
        End Get
        Set(ByVal value As Int32)
            _iDProvincia = value
        End Set
    End Property

    Public Property IDPais() As Int32
        Get
            Return _iDPais
        End Get
        Set(ByVal value As Int32)
            _iDPais = value
        End Set
    End Property

    Public Property Ciudad() As String
        Get
            Return _ciudad
        End Get
        Set(ByVal value As String)
            _ciudad = value
        End Set
    End Property

    Public Property Provincia() As String
        Get
            Return _provincia
        End Get
        Set(ByVal value As String)
            _provincia = value
        End Set
    End Property

    Public Property DirNumero() As String
        Get
            Return _DirNumero
        End Get
        Set(ByVal value As String)
            _DirNumero = value
        End Set
    End Property

    Public Property DirPiso() As String
        Get
            Return _DirPiso
        End Get
        Set(ByVal value As String)
            _DirPiso = value
        End Set
    End Property

    Public Property DirDpto() As String
        Get
            Return _DirDpto
        End Get
        Set(ByVal value As String)
            _DirDpto = value
        End Set
    End Property

    Public Property DirCodPostal() As String
        Get
            Return _DirCodPostal
        End Get
        Set(ByVal value As String)
            _DirCodPostal = value
        End Set
    End Property


    Public Property CodAreaTelefono() As String
        Get
            Return _CodAreaTelefono
        End Get
        Set(ByVal value As String)
            _CodAreaTelefono = value
        End Set
    End Property
End Class
