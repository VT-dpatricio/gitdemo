﻿Public Class BEProductoHogar
    Inherits BEBase

    'Aplicado al producto 5424
    Private _idCertificado As String
    Private _idRamo As Int32
    Private _detalleBienes As String
    Private _superficie As Int32

    'Informacion Obligatoria
    Private _ramoOblig As Boolean
    Private _detalleBienesOblig As Boolean
    Private _superficieOblig As Boolean
    Private _usuarioModificacion As String


    Public Property IdRamo() As Int32
        Get
            Return _idRamo
        End Get
        Set(ByVal value As Int32)
            _idRamo = value
        End Set
    End Property

    Public Property IdCertificado() As String
        Get
            Return _idCertificado
        End Get
        Set(ByVal value As String)
            _idCertificado = value
        End Set
    End Property

    Public Property DetalleBienes() As String
        Get
            Return _detalleBienes
        End Get
        Set(ByVal value As String)
            _detalleBienes = value
        End Set
    End Property

    Public Property Superficie() As Int32
        Get
            Return _superficie
        End Get
        Set(ByVal value As Int32)
            _superficie = value
        End Set
    End Property

    Public Property RamoOblig() As Boolean
        Get
            Return _ramoOblig
        End Get
        Set(ByVal value As Boolean)
            _ramoOblig = value
        End Set
    End Property

    Public Property DetalleBienesOblig() As Boolean
        Get
            Return _detalleBienesOblig
        End Get
        Set(ByVal value As Boolean)
            _detalleBienesOblig = value
        End Set
    End Property

    Public Property SuperficieOblig() As Boolean
        Get
            Return _superficieOblig
        End Get
        Set(ByVal value As Boolean)
            _superficieOblig = value
        End Set
    End Property

    Public Property UsuarioModificacion() As String
        Get
            Return _usuarioModificacion
        End Get
        Set(ByVal value As String)
            _usuarioModificacion = value
        End Set
    End Property
End Class
