Public Class BECiudad
    Inherits BEBase

    Private _idCiudad As Int32
    Private _idProvincia As Int32
    Private _nombre As String

    Public Property IdCiudad() As Int32
        Get
            Return _idCiudad
        End Get
        Set(ByVal value As Int32)
            _idCiudad = value
        End Set
    End Property

    Public Property IdProvincia() As Int32
        Get
            Return _idProvincia
        End Get
        Set(ByVal value As Int32)
            _idProvincia = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property
End Class
