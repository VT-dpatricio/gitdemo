﻿Public Class BEProductoImpresionDetalleEdad

    Private _idProductoDetalleEdad As Integer = 0
    Private _edadInicial As Integer = 0
    Private _edadFinal As Integer = 0
    Private _visualiza As Boolean = False
    Private _opcion As String = String.Empty
    Private _idProducto As Integer = 0
    Private _idImpresion As Integer = 0
    Private _directorio As String = String.Empty
    Private _nombreArchivo As String = String.Empty


    Public Property IDProductoDetalleEdad() As Integer
        Get
            Return _idProductoDetalleEdad
        End Get
        Set(ByVal value As Integer)
            _idProductoDetalleEdad = value
        End Set
    End Property

    Public Property EdadInicial() As Integer
        Get
            Return _edadInicial
        End Get
        Set(ByVal value As Integer)
            _edadInicial = value
        End Set
    End Property

    Public Property EdadFinal() As Integer
        Get
            Return _edadFinal
        End Get
        Set(ByVal value As Integer)
            _edadFinal = value
        End Set
    End Property

    Public Property Visualiza() As Boolean
        Get
            Return _visualiza
        End Get
        Set(ByVal value As Boolean)
            _visualiza = value
        End Set
    End Property

    Public Property Opcion() As String
        Get
            Return _opcion
        End Get
        Set(ByVal value As String)
            _opcion = value
        End Set
    End Property

    Public Property IDProducto() As Integer
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Integer)
            _idProducto = value
        End Set
    End Property

    Public Property IDImpresion() As Integer
        Get
            Return _idImpresion
        End Get
        Set(ByVal value As Integer)
            _idImpresion = value
        End Set
    End Property

    Public Property Directorio() As String
        Get
            Return _directorio
        End Get
        Set(ByVal value As String)
            _directorio = value
        End Set
    End Property

    Public Property NombreArchivo() As String
        Get
            Return _nombreArchivo
        End Get
        Set(ByVal value As String)
            _nombreArchivo = value
        End Set
    End Property

End Class
