Public Class BEOperacion
    Inherits BEBase
    Private _Modificar As Boolean = False
    Private _Anular As Boolean = False
    Private _Activar As Boolean = False
    Private _Consultar As Boolean = False
    Private _subirImagen As Boolean = False
    Private _imprimir As Boolean = False

    Public Property Modificar() As Boolean
        Get
            Return _Modificar
        End Get
        Set(ByVal value As Boolean)
            _Modificar = value
        End Set
    End Property

    Public Property Anular() As Boolean
        Get
            Return _Anular
        End Get
        Set(ByVal value As Boolean)
            _Anular = value
        End Set
    End Property

    Public Property Activar() As Boolean
        Get
            Return _Activar
        End Get
        Set(ByVal value As Boolean)
            _Activar = value
        End Set
    End Property

    Public Property Consultar() As Boolean
        Get
            Return _Consultar
        End Get
        Set(ByVal value As Boolean)
            _Consultar = value
        End Set
    End Property

    Public Property SubirImagen() As Boolean
        Get
            Return _subirImagen
        End Get
        Set(ByVal value As Boolean)
            _subirImagen = value
        End Set
    End Property

    Public Property Imprimir() As Boolean
        Get
            Return _imprimir
        End Get
        Set(ByVal value As Boolean)
            _imprimir = value
        End Set
    End Property
End Class
