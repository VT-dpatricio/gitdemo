﻿Imports Microsoft.VisualBasic

Public Class Constantes

    'Utilizado para verificar si se esta ejecutando en desarrollo o en produccion
    Public Const ENVIROMENT_DEV As String = "DEV"
    Public Const ENVIROMENT_PRO As String = "PRO"
    Public Const SISTEMA_ID As String = "3"
    Public Const MAIN_PAGE As String = "Consulta.aspx"
    Public Const ENTIDAD_ID As Int32 = 18

    Public Const ID_TIPO_PRODUCTO_HOGAR As String = "H"
    Public Const ID_TIPO_PRODUCTO_INTEGRAL_COMERCIO As String = "IC"

    Public Const COD_ASSU As String = "ASSU"

    Public Const REGULAR_EXPRESION_MAIL_ASSURANT As String = "(\s)*([a-zA-Z0-9ñÑ_])+(\.([a-zA-Z0-9ñÑ_])+)*@([a-zA-Z0-9_])+\.([a-zA-Z]{3})(\.([a-zA-Z]){2})?(\s)*"
    Public Const REGULAR_EXPRESION_MAIL_DEFAULT As String = "\w+([-+.'']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
    Public Const REGULAR_EXPRESION_ALL As String = ".*" 'Permite cualquier caracter


    'Nombre de sessiones
    Public Const SS_TIME_OUT As String = "SESIONUS01"
    Public Const SS_CHANGE_PSW As String = "CONTRASE01"
    Public Const SS_LENGTH_PSW As String = "CONTRASE02"
    Public Const SS_SECURITY_PSW As String = "CONTRASE03"
    Public Const SS_EXPIRED_PSW As String = "CONTRASE05"
    Public Const SS_CHANGE_PSW_ONCE_A_DAY As String = "CONTRASE06"

    Public Const SS_APP_BEUSER As String = "BEUsuario"
    Public Const SS_APP_FIRST_LOGIN As String = "PrimerLogin"
    Public Const SS_APP_EXPIRED_PSW As String = "ClaveCaducada"
    Public Const SS_APP_LST_PARAMETERS As String = "SeguridadPar"
    Public Const SS_NAME_USER As String = "NombreUsuario"
    Public Const SS_ID_USUARIO As String = "IDUsuario"

End Class
