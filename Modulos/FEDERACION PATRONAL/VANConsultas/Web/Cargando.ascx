<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Cargando.ascx.vb" Inherits="Cargando" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Panel ID="PCar" runat="server" Width="117px">
<table class="divLoading" style="width: 121px">
    <tr>
        <td>
            <img src="img/wait.gif" /></td>
        <td colspan="2" style="width: 183px">
            Procesando...</td>
    </tr>
</table>
</asp:Panel>
<cc1:AlwaysVisibleControlExtender ID="AVCE" runat="server" TargetControlID="PCar" VerticalSide="Middle" HorizontalSide="Center" HorizontalOffset=50>
</cc1:AlwaysVisibleControlExtender>