Imports VAN.Consulta.BE
Imports VAN.Consulta.BL
Imports System.IO
Partial Class Consulta
    Inherits PaginaBase

#Region "Inicio"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            If ValidarEstadoClaveUsuario() Then
                Response.Redirect("~/Seguridad/CambiarClave.aspx", False)
            End If
            Session("OProductoHogar") = Nothing


            Dim oBLEntidad As New BLEntidad
            Dim IDUsuario As String = Membership.GetUser().UserName
            ddlEntidad.Items.Add(New ListItem("--Todas--", "0"))
            ddlEntidad.DataSource = oBLEntidad.Seleccionar(IDUsuario)
            ddlEntidad.DataTextField = "Descripcion"
            ddlEntidad.DataValueField = "IDEntidad"
            ddlEntidad.DataBind()
            'ddlEntidad.SelectedIndex = 0
            'ddlEntidad_SelectedIndexChanged(sender, e)

            'LLenarProductos()
            'FechaServidor
            'cvtxtIPVencimientoFS.ValueToCompare = Now.Date.ToString("dd/MM/yyyy")
            Dim script As String = "<script> function ActiveTabChanged(sender, e){"
            script = script & "var tab=sender.get_activeTab().get_tabIndex(); var Bt;" & _
                              "if (tab=='1'){Bt = $get('" & btnActCertificado.ClientID & "');  Bt.click(); }" & _
                              "if (tab=='2'){Bt = $get('" & btnActAsegurado.ClientID & "');  Bt.click();}" & _
                              "if (tab=='3'){Bt = $get('" & btnActBeneficiario.ClientID & "');  Bt.click();}" & _
                              "if (tab=='4'){Bt = $get('" & btnActImagen.ClientID & "');  Bt.click();}" & _
                              "if (tab=='5'){Bt = $get('" & btnActCobro.ClientID & "');  Bt.click();}" & _
                              "if (tab=='6'){Bt = $get('" & btnActSinRec.ClientID & "');  Bt.click();}" & _
                              "if (tab=='7'){Bt = $get('" & btnActAditional.ClientID & "');  Bt.click();}" & _
                              "if (tab=='8'){Bt = $get('" & btnActHogar.ClientID & "');  Bt.click();}" & _
                              "}</script>"
            MyBase.ClientScript.RegisterClientScriptBlock(Me.Page.GetType, "Tab", script)
            ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "TabEnable1", "TabActive(true);", True)



            'Cargar DDL's Certificado
            LLenarEstadoCertificado()
            'LLenarMotivoAnulacion()
            LLenarTipoDocumento(ddlITTipoDoc)
            'LLenarMonedaCuenta()
            LlenarEstadoCivil()

            'Asegurado
            LLenarTipoDocumento(ASEddlTipoDoc)
            LLenarParentesco(ASEddlParentesco)

            'Beneficiario
            LLenarTipoDocumento(BENddlTipoDoc)
            LLenarParentesco(BENddlParentesco)

            'Establece Permisos-----------------
            Dim oBLMenuO As New BLMenuOperacion
            Dim oBEOperacion As New BEOperacion
            oBEOperacion = oBLMenuO.Seleccionar(4, IDUsuario)
            Dim pModificar As Boolean = oBEOperacion.Modificar
            Dim pAnular As Boolean = oBEOperacion.Anular
            Dim pActivar As Boolean = oBEOperacion.Activar
            Dim pImprimir As Boolean = oBEOperacion.Imprimir
            '---Tab Cer
            CERbtnModificar.Visible = pModificar
            CERbtnAnular.Visible = pAnular
            CERbtnActivar.Visible = pActivar
            CERbtnGrabar.Visible = pModificar Or pAnular Or pActivar
            CERbtnCancelar.Visible = pModificar Or pAnular Or pActivar
            CERbtnImprimir.Visible = pImprimir
            EstadoBtnCer(True)


            ASEbtnAnular.Visible = False
            ASEbtnAnular.Visible = False
            ASEbtnGrabar.Visible = False
            'ASEbtnModificar.Visible = pModificar
            'ASEbtnAnular.Visible = pAnular
            'ASEbtnGrabar.Visible = pModificar Or pAnular Or pActivar
            'ASEbtnCancelar.Visible = pModificar Or pAnular Or pActivar

            BENbtnModificar.Visible = pModificar
            BENbtnAnular.Visible = pAnular
            BENbtnGrabar.Visible = pModificar Or pAnular Or pActivar
            BENbtnCancelar.Visible = pModificar Or pAnular Or pActivar

            'Producto Hogar
            'Ramos
            'LlenarRamos()

            PHogarbtnModificar.Visible = pModificar
            PHogarbtnGrabar.Visible = pModificar Or pAnular Or pActivar
            PHogarbtnCancelar.Visible = pModificar Or pAnular Or pActivar
            EstadoBtnPHogar(True)
            EstadoControlesProdHogar(False)
            TabConsulta.ActiveTabIndex = 0
            tb_superficieComercio.Attributes.Add("onkeypress", "return Numero(event);")
            tb_superficieComercio.MaxLength = 8

        End If
    End Sub

    Private Sub LLenarParentesco(ByVal pDDL As DropDownList)
        Dim oBL As New BLParentesco
        pDDL.DataSource = oBL.Seleccionar()
        pDDL.DataTextField = "Nombre"
        pDDL.DataValueField = "IdParentesco"
        pDDL.DataBind()
    End Sub

    Private Sub LLenarMedioPago(ByVal pIDProducto As Int32, ByVal pIDMedioPago As String)
        Dim oBL As New BLMedioPago
        ddlIPMedioPago.DataSource = oBL.Seleccionar(pIDProducto, pIDMedioPago)
        ddlIPMedioPago.DataTextField = "Nombre"
        ddlIPMedioPago.DataValueField = "IdMedioPago"
        ddlIPMedioPago.DataBind()
    End Sub

    Private Sub LLenarEstadoCertificado()
        Dim oBL As New BLEstadoCertificado
        ddlDCEstadoCer.DataSource = oBL.Seleccionar()
        ddlDCEstadoCer.DataTextField = "Nombre"
        ddlDCEstadoCer.DataValueField = "IdEstadoCertificado"
        ddlDCEstadoCer.DataBind()
    End Sub

    Private Sub LLenarMotivoAnulacion(ByVal pIDProducto As Integer)
        Dim oBL As New BLMotivoAnulacion
        ddlDCMotAnulacion.Items.Clear()
        ddlDCMotAnulacion.Items.Add(New ListItem("-----Seleccionar-----", "0"))
        ddlDCMotAnulacion.DataSource = oBL.Seleccionar(pIDProducto)
        ddlDCMotAnulacion.DataTextField = "Nombre"
        ddlDCMotAnulacion.DataValueField = "IdMotivoAnulacion"
        ddlDCMotAnulacion.DataBind()
    End Sub

    Private Sub LLenarTipoDocumento(ByVal pDDL As DropDownList)
        Dim oBL As New BLTipoDocumento
        pDDL.DataSource = oBL.Seleccionar()
        pDDL.DataTextField = "Nombre"
        pDDL.DataValueField = "IdTipoDocumento"
        pDDL.DataBind()
    End Sub

    'Private Sub LLenarMonedaCuenta()
    '    ddlIPMonCuenta.Items.Add(New ListItem("-----NA-----", "X"))
    '    ddlIPMonCuenta.Items.Add(New ListItem("NUEVOS SOLES", "PEN"))
    '    ddlIPMonCuenta.Items.Add(New ListItem("D�LARES", "USD"))
    'End Sub

#End Region

#Region "Tab B�squeda"
    Protected Sub ddlBuscarx_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBuscarx.SelectedIndexChanged
        Select Case ddlBuscarx.SelectedValue
            Case "NCertificado", "NDocumento", "NCuenta", "NSuministro", "NPlaca", "RAIZ"
                txtNombres.Visible = False
                lblNombres.Visible = False
                lblBuscarx.Text = ddlBuscarx.SelectedItem.Text & ":"
            Case "ApeNom"
                txtNombres.Visible = True
                lblNombres.Visible = True
                lblBuscarx.Text = "Apellidos:"
        End Select
        txtParametro.Text = ""
    End Sub

    Protected Sub ddlEntidad_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        LLenarProductos()

    End Sub

    Private Sub LLenarProductos()
        Dim oBLProducto As New BLProducto
        ddlProducto.Items.Clear()
        ddlProducto.Items.Add(New ListItem("--Todos--", "0"))
        ddlProducto.DataSource = oBLProducto.Seleccionar(CInt(ddlEntidad.SelectedValue), Membership.GetUser().UserName)
        ddlProducto.DataTextField = "Descripcion"
        ddlProducto.DataValueField = "IDProducto"
        ddlProducto.DataBind()
    End Sub

   
    Private Sub LlenarGridBusqueda()

        Dim oBLCertificado As New BLCertificado
        Dim pIdFiltro As String = ddlBuscarx.SelectedValue
        Dim pIdUsuario As String = Membership.GetUser().UserName
        Dim pIdEntidad As Int32 = CInt(ddlEntidad.SelectedValue)
        Dim pIdProducto As Int32 = CInt(ddlProducto.SelectedValue)
        Dim pNumero As String = txtParametro.Text.Trim
        Dim pApellido As String = txtParametro.Text.Trim
        Dim pNombre As String = txtNombres.Text.Trim
        Dim pFiltroAse As Boolean = chkAse.Checked
        gvBusqueda.DataSource = oBLCertificado.Buscar(pIdFiltro, pIdUsuario, pIdEntidad, pIdProducto, pNumero, pApellido, pNombre, pFiltroAse)
        gvBusqueda.DataBind()
        gvBusqueda.SelectedIndex = -1
        ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "TabEnable1", "TabActive(true);", True)
    End Sub

    'Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    LlenarGridBusqueda()
    'End Sub

    Protected Sub btnBuscar_Click1(sender As Object, e As EventArgs) Handles btnBuscar.Click
        LlenarGridBusqueda()
    End Sub

    Protected Sub gvBusqueda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBusqueda.SelectedIndexChanged
        ViewState("eTabDataAditional") = False
        ViewState("eTabSinRec") = False
        ViewState("eTabCertificado") = False
        ViewState("eTabAsegurado") = False
        ViewState("eTabBeneficiario") = False
        ViewState("eTabCobro") = False
        ViewState("eTabImagenes") = False
        ViewState("eTabHogar") = False
        CancelarEditTabCer()
        Dim IDCertificado As String = gvBusqueda.SelectedDataKey.Item(0).ToString()
        Session("IDCertificado") = IDCertificado
        Session("NumCertificado") = gvBusqueda.SelectedDataKey.Item(1).ToString()
        Session("IDProducto") = gvBusqueda.SelectedDataKey.Item(2).ToString()
        Session("IDReclamo") = Nothing
        btnImpresionNota.Enabled = False

        gvDatosAdicionales.DataSource = Nothing
        gvDatosAdicionales.DataBind() 'datos de siniestros

        gvDataAditional.DataSource = Nothing
        gvDataAditional.DataBind() 'info asegurados + info producto

        'LLenarDatosAdicionales(IDCertificado)
        LLenarDataAditional(IDCertificado)

        ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "TabEnable1", "TabActive(false);", True)

        Dim oBECer As New BECertificado
        Dim oBLCer As New BLCertificado

        oBECer = oBLCer.Seleccionar(IDCertificado)
        hfIDTipoProducto.Value = oBECer.IdTipoProducto
        If hfIDTipoProducto.Value <> Constantes.ID_TIPO_PRODUCTO_INTEGRAL_COMERCIO Then
            ddIA_CondicionIva.Visible = False
            lb_condIva.Visible = False
            'ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "TabEnable1", "Ocultar(8);", True)
            ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "EstadoIC", "EstadoTabIC(false);", True)
        Else
            ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "EstadoIC", "EstadoTabIC(true);", True)
            ddIA_CondicionIva.Visible = True
            lb_condIva.Visible = True
        End If

    End Sub

    Protected Sub gvBusqueda_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvBusqueda.PageIndex = e.NewPageIndex
        LlenarGridBusqueda()
    End Sub
#End Region

#Region "Tab Certificado"

    Protected Sub btnActCertificado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActCertificado.Click
        If Not (CBool(ViewState("eTabCertificado"))) Then
            LlenarCertificado()
            ViewState("eTabCertificado") = True
            EstadoBtnCer(True)
        End If
    End Sub

    Private Sub LlenarCertificado()
        Dim oBECer As New BECertificado
        Dim oBLCer As New BLCertificado
        If Not gvBusqueda.SelectedDataKey Is Nothing Then

            Dim IDCertificado As String = gvBusqueda.SelectedDataKey.Item(0).ToString()
            oBECer = oBLCer.Seleccionar(IDCertificado)
            '//Datos del Certificado----------
            txtDCNroCertificado.Text = IDCertificado
            txtDCNroCertificadoB.Text = oBECer.Raiz
            txtDCNroCertificadoAseg.Text = IIf(oBECer.NroFisico <> "-1", oBECer.NroFisico, "")
            txtDCIDAgencia.Value = oBECer.IdOficina
            txtDCAgenciaNombre.Text = oBECer.IdOficina & " - " & oBECer.Oficina.Trim
            txtDCIDVendedor.Value = oBECer.IdInformador
            txtDCVendedorNombre.Text = oBECer.IdInformador & " - " & oBECer.Informador.Trim
            ddlDCEstadoCer.SelectedValue = oBECer.IdEstadoCertificado
            txtDCFechaVenta.Text = oBECer.Venta.ToString("dd/MM/yyyy")
            txtDCFechaDigitacion.Text = oBECer.Digitacion.ToString("dd/MM/yyyy HH:mm:ss")
            txtDCInicioVigencia.Text = IIf(oBECer.Vigencia = "01/01/1900", "", oBECer.Vigencia)
            txtDCFinVigencia.Text = IIf(oBECer.FinVigencia = "01/01/1900", "", oBECer.FinVigencia)
            txtDCEfectuarAnu.Text = IIf(oBECer.EfectuarAnulacion = "01/01/1900", "", oBECer.EfectuarAnulacion)
            txtDCSolAnulacion.Text = IIf(oBECer.SolicitudAnulacion = "01/01/1900", "", oBECer.SolicitudAnulacion)
            txtDCUsuarioMod.Text = oBECer.UsuarioModificacion
            LLenarMotivoAnulacion(oBECer.IdProducto)
            ddlDCMotAnulacion.SelectedValue = oBECer.IdMotivoAnulacion
            txtDCObs.Text = oBECer.Observacion
            hfIDProducto.Value = oBECer.IdProducto
            hfIDTipoProducto.Value = oBECer.IdTipoProducto
            hfCodAsegurador.Value = oBECer.Sigla

            '//Informaci�n del Titular--------------
            ddlITTipoDoc.SelectedValue = oBECer.IdTipoDocumento
            txtITNroDoc.Text = oBECer.CcCliente
            txtITNombre1.Text = oBECer.Nombre1
            txtITNombre2.Text = oBECer.Nombre2
            txtITApellido1.Text = oBECer.Apellido1
            txtITApellido2.Text = oBECer.Apellido2
            txtITDirecPar.Text = oBECer.Direccion
            txtITDirNumero.Text = oBECer.DirNumero
            txtITDirPiso.Text = oBECer.DirPiso
            txtITDirDpto.Text = oBECer.DirDpto
            txtITDirCPostal.Text = oBECer.DirCodPostal
            txtITDirUbigeo.Text = oBECer.Ciudad & " / " & oBECer.Provincia
            hfITIDCiudad.Value = oBECer.IdCiudad
            txtITEmail.Text = IIf(oBECer.Email <> "-1", oBECer.Email, "")
            txtITCAreaTelefono.Text = oBECer.CodAreaTelefono
            txtITTelefono.Text = oBECer.Telefono
            txtITCAreaOtroTelf.Text = oBECer.CodAreaOtroTelefono
            txtITOtroTelefono.Text = oBECer.OtroTelefono
            txtITCelular.Text = oBECer.Celular
            chk_envioMailPoliza.Checked = IIf(oBECer.EnvioMailPoliza = "0", False, True)

            '//Domicilio del Riesgo Asegurado---------------------
            txtITDRADireccion.Text = IIf(oBECer.DRADireccion <> "-1", oBECer.DRADireccion, "")
            txtITDRADirNumero.Text = IIf(oBECer.DRADirNumero <> "-1", oBECer.DRADirNumero, "")
            txtITDRADirPiso.Text = IIf(oBECer.DRADirPiso <> "-1", oBECer.DRADirPiso, "")
            txtITDRADirDpto.Text = IIf(oBECer.DRADirDpto <> "-1", oBECer.DRADirDpto, "")
            txtITDRADirCPostal.Text = IIf(oBECer.DRADirCodPostal <> "-1", oBECer.DRADirCodPostal, "")
            If (oBECer.DRAIDCiudad <> -1) Then
                txtITDRADirUbigeo.Text = oBECer.DRALocalidad & " / " & oBECer.DRAProvincia
                hfIDAIDCiudad.Value = oBECer.DRAIDCiudad
            Else
                txtITDRADirUbigeo.Text = ""
                hfIDAIDCiudad.Value = 0
            End If


            '//Informaci�n Adicional del Titular---------------
            Try
                ddlITEstadoCivil.SelectedValue = IIf(oBECer.EstadoCivil <> "-1", oBECer.EstadoCivil, "0")
            Catch ex As Exception
                ddlITEstadoCivil.SelectedValue = "0"
            End Try
            ddlITSexo.SelectedValue = IIf(oBECer.Sexo <> "-1", oBECer.Sexo, "0")
            If (oBECer.FechaNacimiento <> "-1") Then
                txtITFechaNac.Text = oBECer.FechaNacimiento
                txtITEdad.Text = CalcularEdad(CDate(oBECer.FechaNacimiento))
            Else
                txtITFechaNac.Text = ""
                txtITEdad.Text = ""
            End If

            txtITCUIT.Text = IIf(oBECer.CUIT <> "-1", oBECer.CUIT, "")
            'Math.Truncate(CalcularEdad(CDate(txtFechaNac.Text)))
            txtITNacimiento.Text = IIf(oBECer.Nacimiento <> "-1", oBECer.Nacimiento, "")
            txtITProfesion.Text = IIf(oBECer.Profesion <> "-1", oBECer.Profesion, "")
            txtITNacionalidad.Text = IIf(oBECer.Nacionalidad <> "-1", oBECer.Nacionalidad, "")

            If oBECer.IdTipoProducto = Constantes.ID_TIPO_PRODUCTO_INTEGRAL_COMERCIO Then
                ddIA_CondicionIva.SelectedValue = oBECer.CondicionIVA
            End If


            '//Informaci�n de Pago--------------------
            txtIPProducto.Text = oBECer.Producto
            txtIPPlan.Text = oBECer.Opcion
            txtIPFrecPago.Text = oBECer.Frecuencia
            LLenarMedioPago(oBECer.IdProducto, oBECer.IdMedioPago)
            ddlIPMedioPago.SelectedValue = oBECer.IdMedioPago

            txtIPCatTarj.Text = oBECer.NumeroCuenta
            ViewState("vNumeroCuenta") = oBECer.NumeroCuenta
            Dim cultureToUse As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("es-AR")
            'txtIPMontoAseg.Text = oBECer.SimboloMoneda & " " & Format(oBECer.MontoAsegurado, "#0.00").Replace(".", ",")
            txtIPMontoAseg.Text = oBECer.SimboloMoneda & " " & oBECer.MontoAsegurado.ToString("#,##0.00", cultureToUse)
            txtIPPrimaBruta.Text = oBECer.SimboloMoneda & " " & oBECer.PrimaBruta.ToString("#,##0.00", cultureToUse)
            txtIPModenaPrima.Text = oBECer.IdMonedaPrima
            lblEstadoVen.Text = oBECer.EstadoVenta
            txtITTCBU.Text = oBECer.CBU
            '---Estado de los campos InfoProductoC para la modificaci�n / "-1":No existe el campo
            ViewState("eFechaNacimiento") = IIf(oBECer.FechaNacimiento <> "-1", True, False)
            ViewState("eSexo") = IIf(oBECer.Sexo <> "-1", True, False)
            ViewState("eEstadoCivil") = IIf(oBECer.EstadoCivil <> "-1", True, False)
            ViewState("eEmail") = IIf(oBECer.Email <> "-1", True, False)
            ViewState("eProfesion") = IIf(oBECer.Profesion <> "-1", True, False)
            ViewState("eNacionalidad") = IIf(oBECer.Nacionalidad <> "-1", True, False)

            'ViewState("eNacionalidad") = IIf(oBECer.Nacionalidad <> "-1", True, False)
            'ViewState("eNumTarjAsegurada") = IIf(oBECer.NumTarjAsegurada <> "-1", True, False)
            'ViewState("eFechaActivacion") = IIf(oBECer.FechaActivacion <> "-1", True, False)
            'ViewState("eMonedaCuenta") = IIf(oBECer.MonedaCuenta <> "-1", True, False)
        End If

    End Sub

    Private Sub LlenarRamos()
        Dim oBLRamo As New BLRamo
        ddlRamo.Items.Clear()
        ddlRamo.DataSource = oBLRamo.Seleccionar()
        ddlRamo.DataTextField = "Nombre"
        ddlRamo.DataValueField = "IdRamo"
        ddlRamo.DataBind()
    End Sub

    Public Function CalcularEdad(ByVal FechaNacimiento As DateTime) As Int16
        Dim dblEdad As Double = DateTime.Now.Subtract(FechaNacimiento).TotalDays / 365.25
        Return Math.Truncate(dblEdad)
    End Function

    Private Sub LlenarEstadoCivil()
        Dim oBLEstadoCivil As New BLEstadoCivil
        ddlITEstadoCivil.Items.Clear()
        ddlITEstadoCivil.Items.Add(New ListItem("------------", "0"))
        ddlITEstadoCivil.DataSource = oBLEstadoCivil.Seleccionar()
        ddlITEstadoCivil.DataTextField = "Nombre"
        ddlITEstadoCivil.DataValueField = "IdEstadoCivil"
        ddlITEstadoCivil.DataBind()
    End Sub

    Private Sub LlenarDepartamento(ByVal pDDL As DropDownList, ByVal pIDPais As Int32)
        Dim oBLDepartamento As New BLDepartamento
        pDDL.Items.Clear()
        pDDL.DataSource = oBLDepartamento.Seleccionar(pIDPais)
        pDDL.DataTextField = "Nombre"
        pDDL.DataValueField = "IdDepartamento"
        pDDL.DataBind()
    End Sub

    Private Sub LlenarProvincia(ByVal pDDL As DropDownList, ByVal pIDDepartamento As Int32)
        Dim oBL As New BLProvincia
        pDDL.Items.Clear()
        pDDL.DataSource = oBL.Seleccionar(pIDDepartamento)
        pDDL.DataTextField = "Nombre"
        pDDL.DataValueField = "IdProvincia"
        pDDL.DataBind()
    End Sub

    Private Sub LlenarDistrito(ByVal pDDL As DropDownList, ByVal pIDProvincia As Int32)
        Dim oBL As New BLCiudad
        pDDL.Items.Clear()
        pDDL.DataSource = oBL.Seleccionar(pIDProvincia)
        pDDL.DataTextField = "Nombre"
        pDDL.DataValueField = "IdCiudad"
        pDDL.DataBind()
    End Sub

    Private Sub EstadoControlesCer(ByVal pEstado As Boolean)
        'EstadoTextBox(txtDCIDVendedor, pEstado)
        'EstadoTextBox(txtDCIDAgencia, pEstado)
        '
        '//Informaci�n del Titular--------------
        'EstadoDDL(ddlITTipoDoc, pEstado)
        'EstadoTextBox(txtITNroDoc, pEstado)
        'EstadoTextBox(txtITApellido1, pEstado)
        'EstadoTextBox(txtITApellido2, pEstado)
        'EstadoTextBox(txtITNombre1, pEstado)
        'EstadoTextBox(txtITNombre2, pEstado)
        EstadoTextBox(txtITDirecPar, pEstado)

        EstadoTextBox(txtITDirNumero, pEstado)
        EstadoTextBox(txtITDirPiso, pEstado)
        EstadoTextBox(txtITDirDpto, pEstado)
        EstadoTextBox(txtITDirCPostal, pEstado)
        EstadoTextBox(txtITEmail, pEstado)
        EstadoTextBox(txtITCAreaTelefono, pEstado)
        EstadoTextBox(txtITTelefono, pEstado)
        EstadoTextBox(txtITCAreaOtroTelf, pEstado)
        EstadoTextBox(txtITOtroTelefono, pEstado)
        EstadoTextBox(txtITCelular, pEstado)
        EstadoTextBox(txtITEmail, pEstado And CBool(ViewState("eEmail")))
        EstadoCheckBox(chk_envioMailPoliza, pEstado)
        'EstadoTextBox(, pEstado)

        '//Domicilio del Riesgo Asegurado---------------------
        EstadoTextBox(txtITDRADireccion, pEstado And hfIDProducto.Value = "5264")
        EstadoTextBox(txtITDRADirNumero, pEstado And hfIDProducto.Value = "5264")
        EstadoTextBox(txtITDRADirPiso, pEstado And hfIDProducto.Value = "5264")
        EstadoTextBox(txtITDRADirDpto, pEstado And hfIDProducto.Value = "5264")
        EstadoTextBox(txtITDRADirCPostal, pEstado And hfIDProducto.Value = "5264")

        '//Informaci�n Adicional del Asegurado--------------
        EstadoTextBox(txtITFechaNac, pEstado And CBool(ViewState("eFechaNacimiento")))
        cvtxtITFechaNac.Enabled = pEstado And CBool(ViewState("eFechaNacimiento"))
        EstadoTextBox(txtITCUIT, pEstado)
        EstadoTextBox(txtITNacimiento, pEstado)
        EstadoDDL(ddlITSexo, pEstado And CBool(ViewState("eSexo")))
        EstadoDDL(ddlITEstadoCivil, pEstado And CBool(ViewState("eEstadoCivil")))
        EstadoDDL(ddIA_CondicionIva, pEstado)

        EstadoTextBox(txtITProfesion, pEstado And CBool(ViewState("eProfesion")))
        EstadoTextBox(txtITNacionalidad, pEstado And CBool(ViewState("eNacionalidad")))



        EstadoDDL(ddlIPMedioPago, pEstado)
        EstadoTextBox(txtIPCatTarj, False)

        'EstadoTextBox(txtIPNroTarj, pEstado And CBool(ViewState("eNumTarjAsegurada")))
        'EstadoTextBox(txtIPFActivacion, pEstado And CBool(ViewState("eFechaActivacion")))
        'EstadoDDL(ddlIPMonCuenta, pEstado And CBool(ViewState("eMonedaCuenta")))
    End Sub

    Protected Sub CERbtnModificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CERbtnModificar.Click
        EstadoControlesCer(True)
        hfOperacion.Value = "Modificar"
        EstadoBtnCer(False)
        txtIPCatTarj.Text = ViewState("vNumeroCuenta").ToString
    End Sub

    Protected Sub CERbtnChangeMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CERbtnChangeMail.Click
        Dim estado As Boolean = False
        Dim style As String = ""

        estado = IIf(Not String.IsNullOrEmpty(hfEditarEmail.Value) AndAlso hfEditarEmail.Value = "S", True, False)
        style = IIf(Not String.IsNullOrEmpty(hfEditarEmail.Value) AndAlso hfEditarEmail.Value = "S", "txtTexto", "txtTextoD")

        ViewState("eEmail") = estado
        txtITEmail.Enabled = estado
        txtITEmail.ReadOnly = Not estado
        txtITEmail.CssClass = style
    End Sub

    Protected Sub CERbtnAnular_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        hfOperacion.Value = "Anular"
        EstadoDDL(ddlDCMotAnulacion, True)
        rfvddlDCMotAnulacion.Enabled = True
        EstadoTextBox(txtDCObs, True)
        EstadoBtnCer(False)
        'cvFechaVenc.Enabled = False
    End Sub

    Protected Sub CERbtnActivar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        EstadoTextBox(txtDCObs, True)
        ddlDCMotAnulacion.SelectedIndex = 0
        txtDCObs.Focus()
        hfOperacion.Value = "Activar"
        EstadoBtnCer(False)
    End Sub

    Protected Function ValidateFormatEmail(ByVal email As String) As Boolean
        Dim tipoExpresion As String = ""

        If hfCodAsegurador.Value = Constantes.COD_ASSU Then
            tipoExpresion = Constantes.REGULAR_EXPRESION_MAIL_ASSURANT
        Else
            tipoExpresion = Constantes.REGULAR_EXPRESION_ALL
        End If

        If (Regex.IsMatch(email, tipoExpresion)) Then
            If (Regex.Replace(email, tipoExpresion, String.Empty).Length = 0) Then
                Return True
            Else
                Return False
            End If
        End If
    End Function

    Protected Sub CERbtnGrabar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CERbtnGrabar.Click
        Dim oBL As New BLCertificado
        Dim IDUsuario As String = Membership.GetUser().UserName
        Dim IDCertificado As String = gvBusqueda.SelectedDataKey.Item(0).ToString()
        Select Case hfOperacion.Value
            Case "Modificar"
                Dim oBECer As New BECertificado

                If chk_envioMailPoliza.Checked Then
                    If hfCodAsegurador.Value = Constantes.COD_ASSU And String.IsNullOrEmpty(txtITEmail.Text) Then
                        ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M", "alert('Debe ingresar la direccion de correo.');", True)
                        Exit Sub
                    End If
                    If Not ValidateFormatEmail(txtITEmail.Text) Then
                        ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M", "alert('La direcci�n de correo ingresada no es correcta.');", True)
                        Exit Sub
                    End If
                    oBECer.Email = txtITEmail.Text.Trim.ToUpper
                Else
                    oBECer.Email = txtITEmail.Text.Trim.ToUpper
                End If


                oBECer.IdCertificado = IDCertificado
                oBECer.IdProducto = hfIDProducto.Value
                oBECer.IdOficina = txtDCIDAgencia.Value
                oBECer.IdInformador = txtDCIDVendedor.Value
                '//Informaci�n del Titular---------
                oBECer.IdTipoDocumento = ddlITTipoDoc.SelectedValue
                oBECer.CcCliente = txtITNroDoc.Text.Trim
                oBECer.Apellido1 = txtITApellido1.Text.ToUpper.Trim
                oBECer.Apellido2 = txtITApellido2.Text.ToUpper.Trim
                oBECer.Nombre1 = txtITNombre1.Text.ToUpper.Trim
                oBECer.Nombre2 = txtITNombre2.Text.ToUpper.Trim
                oBECer.Direccion = txtITDirecPar.Text.ToUpper.Trim
                oBECer.DirNumero = txtITDirNumero.Text.Trim
                oBECer.DirPiso = txtITDirPiso.Text.Trim
                oBECer.DirDpto = txtITDirDpto.Text.Trim
                oBECer.DirCodPostal = txtITDirCPostal.Text.Trim
                oBECer.IdCiudad = CInt(hfITIDCiudad.Value)

                oBECer.EnvioMailPoliza = IIf(chk_envioMailPoliza.Checked, "1", "0")
                oBECer.CodAreaTelefono = txtITCAreaTelefono.Text.Trim
                oBECer.Telefono = txtITTelefono.Text.Trim
                oBECer.CodAreaOtroTelefono = txtITCAreaOtroTelf.Text.Trim
                oBECer.OtroTelefono = txtITOtroTelefono.Text.Trim
                oBECer.Celular = txtITCelular.Text.Trim
                oBECer.DRAIDCiudad = CInt(hfIDAIDCiudad.Value)

                '//Domicilio del Riesgo Asegurado----------------
                oBECer.DRADireccion = txtITDRADireccion.Text.Trim
                oBECer.DRADirNumero = txtITDRADirNumero.Text.Trim
                oBECer.DRADirPiso = txtITDRADirPiso.Text.Trim
                oBECer.DRADirDpto = txtITDRADirDpto.Text.Trim
                oBECer.DRADirCodPostal = txtITDRADirCPostal.Text.Trim
                oBECer.DRAIDCiudad = CInt(hfIDAIDCiudad.Value)

                '//Informaci�n Adicional del Titular---
                If CBool(ViewState("eEstadoCivil")) Then
                    oBECer.EstadoCivil = ddlITEstadoCivil.SelectedValue
                Else
                    oBECer.EstadoCivil = String.Empty
                End If
                If CBool(ViewState("eSexo")) Then
                    oBECer.Sexo = ddlITSexo.SelectedValue
                Else
                    oBECer.Sexo = String.Empty
                End If
                If CBool(ViewState("eFechaNacimiento")) Then
                    oBECer.FechaNacimiento = FormatoFecha(txtITFechaNac.Text)
                Else
                    oBECer.FechaNacimiento = String.Empty
                End If
                oBECer.CUIT = txtITCUIT.Text.Trim
                oBECer.Nacimiento = txtITNacimiento.Text.Trim
                If CBool(ViewState("eProfesion")) Then
                    oBECer.Profesion = txtITProfesion.Text.Trim.ToUpper
                Else
                    oBECer.Profesion = String.Empty
                End If

                If CBool(ViewState("eNacionalidad")) Then
                    oBECer.Nacionalidad = txtITNacionalidad.Text.Trim.ToUpper
                Else
                    oBECer.Nacionalidad = String.Empty
                End If

                If hfIDTipoProducto.Value = Constantes.ID_TIPO_PRODUCTO_INTEGRAL_COMERCIO Then
                    oBECer.CondicionIVA = ddIA_CondicionIva.SelectedValue
                End If


                'If Len(txtIPVencimiento.Text.Trim.ToString) = 0 Then
                '    oBECer.Vencimiento = String.Empty
                'Else
                '    oBECer.Vencimiento = FormatoFecha(txtIPVencimiento.Text.Trim)
                'End If
                'oBECer.Vencimiento = IIf(Len(txtIPVencimiento.Text.Trim()) = 0, String.Empty, FormatoFecha(txtIPVencimiento.Text.Trim))
                oBECer.IdMedioPago = ddlIPMedioPago.SelectedValue
                oBECer.IdEstadoCertificado = ddlDCEstadoCer.SelectedValue
                oBECer.NumeroCuenta = txtIPCatTarj.Text.Trim
                oBECer.UsuarioModificacion = IDUsuario
                oBECer.Observacion = txtDCObs.Text.ToUpper.Trim

                'InfoProductoC

                'If CBool(ViewState("eNumTarjAsegurada")) Then
                '    oBECer.NumTarjAsegurada = txtIPNroTarj.Text.Trim
                'Else
                '    oBECer.NumTarjAsegurada = String.Empty
                'End If

                'If CBool(ViewState("eFechaActivacion")) Then
                '    oBECer.FechaActivacion = FormatoFecha(txtIPFActivacion.Text)
                'Else
                '    oBECer.FechaActivacion = String.Empty
                'End If

                'If CBool(ViewState("eMonedaCuenta")) Then
                '    oBECer.MonedaCuenta = ddlIPMonCuenta.SelectedValue
                'Else
                '    oBECer.MonedaCuenta = String.Empty
                'End If


                If (oBL.Actualizar(oBECer)) Then
                    ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M", "alert('Certificado modificado con �xito');", True)
                    EstadoControlesCer(False)
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M", "alert('Ocurri� un error al modificar el certificado.');", True)
                    Exit Sub
                End If

            Case "Anular"

                If (oBL.Anular(IDCertificado, IDUsuario, txtDCObs.Text.Trim.ToUpper, CInt(ddlDCMotAnulacion.SelectedValue))) Then
                    ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M", "alert('Certificado anulado con �xito');", True)
                    EstadoTextBox(txtDCObs, False)
                    EstadoDDL(ddlDCMotAnulacion, False)
                    rfvddlDCMotAnulacion.Enabled = False
                    'cvFechaVenc.Enabled = True
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M", "alert('Ocurri� un error al intentar anular el certificado.');", True)
                    Exit Sub
                End If

            Case "Activar"

                If (oBL.Activar(IDCertificado, IDUsuario, txtDCObs.Text.Trim.ToUpper)) Then
                    ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M", "alert('Certificado activado con �xito');", True)
                    EstadoTextBox(txtDCObs, False)
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M", "alert('Ocurri� un error al activar el certificado.');", True)
                    Exit Sub
                End If
        End Select
        LlenarCertificado()
        hfOperacion.Value = ""
        EstadoBtnCer(True)
    End Sub

    Protected Sub CERbtnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        CancelarEditTabCer()
    End Sub

    Private Sub CancelarEditTabCer()
        Select Case hfOperacion.Value
            Case "Modificar"
                EstadoControlesCer(False)
            Case "Anular"
                EstadoTextBox(txtDCObs, False)
                EstadoDDL(ddlDCMotAnulacion, False)
                rfvddlDCMotAnulacion.Enabled = False
                'cvFechaVenc.Enabled = True
            Case "Activar"
                EstadoTextBox(txtDCObs, False)
        End Select
        hfOperacion.Value = ""
        'Cargar los datos del certificado siempre y cuando el usuario se encuentre en el tab cer
        If CBool(ViewState("eTabCertificado")) Then
            LlenarCertificado()
        End If

        EstadoBtnCer(True)
    End Sub

    Private Sub EstadoBtnCer(ByVal pEstado As Boolean)
        '1:Activo
        '2:Inactivo
        '3:Cancelado
        '4:Eliminado
        '5:Cotizado
        Dim EstadoCer As Int32 = CInt(ddlDCEstadoCer.SelectedValue)
        Dim Anulado As Boolean = False
        Dim Activo As Boolean = False
        If (EstadoCer = 3) Then
            Anulado = True
        End If
        If (EstadoCer = 1) Then
            Activo = True
        End If
        CERbtnModificar.Enabled = pEstado And Not Anulado
        CERbtnAnular.Enabled = pEstado And Not Anulado
        CERbtnActivar.Enabled = pEstado And Not Activo
        CERbtnGrabar.Enabled = Not pEstado
        CERbtnCancelar.Enabled = Not pEstado
    End Sub

    Private Sub EstadoBtnPHogar(ByVal pEstado As Boolean)
        '1:Activo
        '2:Inactivo
        '3:Cancelado
        '4:Eliminado
        '5:Cotizado
        Dim EstadoCer As Int32 = CInt(ddlDCEstadoCer.SelectedValue)
        Dim Anulado As Boolean = False
        Dim Activo As Boolean = False
        If (EstadoCer = 3) Then
            Anulado = True
        End If
        If (EstadoCer = 1) Then
            Activo = True
        End If
        PHogarbtnModificar.Enabled = pEstado And Not Anulado
        PHogarbtnGrabar.Enabled = Not pEstado
        PHogarbtnCancelar.Enabled = Not pEstado
    End Sub

#End Region

#Region "Tab Adicionales"
    Protected Sub btnActAditional_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActAditional.Click
        If Not gvBusqueda.SelectedDataKey Is Nothing Then
            If Not (CBool(ViewState("eTabDataAditional"))) Then
                gvDatosAdicionales.DataSource = Nothing
                gvDatosAdicionales.DataBind()
                LLenarDataAditional(gvBusqueda.SelectedDataKey.Item(0).ToString)
                gvDataAditional.SelectedIndex = -1
                'ViewState("eTabSinRec") = True  --> SE SACA
                ViewState("eTabDataAditional") = True
            End If
        End If
    End Sub

    Protected Sub gvDataAditionalInfo_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvDataAditional.PageIndexChanging
        gvDataAditional.PageIndex = e.NewPageIndex
        LLenarDataAditional(gvBusqueda.SelectedDataKey.Item(0).ToString)
    End Sub

    Private Sub LLenarDataAditional(ByVal pIDCertificado As String)
        If Not gvBusqueda.SelectedRow Is Nothing Then

            Dim oBL As New BLInfoAdicional
            gvDataAditional.DataSource = oBL.SeleccionarDatosAdicionales(pIDCertificado)
            gvDataAditional.DataBind()
        End If
    End Sub

#End Region

#Region "Tab Asegurado"
    Protected Sub btnActAsegurado_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActAsegurado.Click
        If Not gvBusqueda.SelectedDataKey Is Nothing Then
            If Not (CBool(ViewState("eTabAsegurado"))) Then
                LlenarGridAsegurado()
                gvAsegurado.SelectedIndex = -1
                ViewState("eTabAsegurado") = True
                LimpiarControlesAse()
                EstadoControlesAse(False)
            End If
        End If
    End Sub

    Private Sub LlenarGridAsegurado()
        Dim oBL As New BLAsegurado
        gvAsegurado.DataSource = oBL.Seleccionar(gvBusqueda.SelectedDataKey.Item(0).ToString)
        gvAsegurado.DataBind()
    End Sub

    Protected Sub gvAsegurado_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvAsegurado.SelectedIndexChanged
        LlenarAsegurado()
        EstadoBtnAse(True)
        EstadoControlesAse(False)
        EstadoTextBox(ASEtxtObservacion, False)
    End Sub

    Private Sub LlenarAsegurado()
        Dim oBL As New BLAsegurado
        Dim oBE As New BEAsegurado
        Dim IDCer As String = gvAsegurado.SelectedDataKey.Item(0).ToString
        Dim IDCon As Int16 = CInt(gvAsegurado.SelectedDataKey.Item(1))
        oBE = oBL.Seleccionar(IDCer, IDCon)
        Try
            ASEddlParentesco.SelectedValue = oBE.IdParentesco
        Catch ex As Exception
            ASEddlParentesco.SelectedValue = "2"
        End Try

        ASEtxtNombre1.Text = oBE.Nombre1
        ASEtxtNombre2.Text = oBE.Nombre2
        ASEtxtApellido1.Text = oBE.Apellido1
        ASEtxtApellido2.Text = oBE.Apellido2
        ASEtxtFechaNac.Text = oBE.FechaNacimiento
        ASEtxtTelefono.Text = oBE.Telefono
        Try
            ASEddlTipoDoc.SelectedValue = oBE.IdTipoDocumento
        Catch ex As Exception
            ASEddlTipoDoc.SelectedIndex = 0
        End Try
        ASEtxtNroDoc.Text = oBE.CcAseg
        ASEtxtNroDoc.MaxLength = IIf(oBE.IdTipoDocumento = "L", 8, 20)
        ASEtxtDireccion.Text = oBE.Direccion
        'If (oBE.IdCiudad <> 0) Then
        '    LlenarDepartamento(ASEddlDep, oBE.IDPais)
        '    ASEddlDep.SelectedValue = oBE.IDDepartamento
        '    LlenarProvincia(ASEddlPro, oBE.IDDepartamento)
        '    ASEddlPro.SelectedValue = oBE.IDProvincia
        '    LlenarDistrito(ASEddlDis, oBE.IDProvincia)
        '    ASEddlDis.SelectedValue = oBE.IdCiudad
        'Else
        '    ASEddlDep.Items.Clear()
        '    ASEddlDep.Items.Add(New ListItem("---------------------------", "0"))
        '    ASEddlPro.Items.Clear()
        '    ASEddlPro.Items.Add(New ListItem("---------------------------", "0"))
        '    ASEddlDis.Items.Clear()
        '    ASEddlDis.Items.Add(New ListItem("-----------------------------", "0"))
        'End If
        ASEtxtDirNumero.Text = IIf(oBE.DirNumero <> "-1", oBE.DirNumero, "")
        ASEtxtDirPiso.Text = IIf(oBE.DirPiso <> "-1", oBE.DirPiso, "")
        ASEtxtDirDpto.Text = IIf(oBE.DirDpto <> "-1", oBE.DirDpto, "")
        ASEtxtDirCPostal.Text = IIf(oBE.DirCodPostal <> "-1", oBE.DirCodPostal, "")
        ASEtxtDirUbigeo.Text = oBE.Ciudad & " / " & oBE.Provincia
        ASEtxtCAreaTelefono.Text = IIf(oBE.CodAreaTelefono <> "-1", oBE.CodAreaTelefono, "")

        ASEtxtPlan.Text = oBE.Plan
        ASEtxtPrima.Text = IIf(oBE.Prima = -1, String.Empty, Format(oBE.Prima, "#.00").Replace(".", ","))

        ASEtxtObservacion.Text = ""
        ViewState("eASEActivo") = oBE.Activo
    End Sub

    Protected Sub ASEbtnAnular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ASEbtnAnular.Click
        ASEhfOperacion.Value = "Anular"
        EstadoTextBox(ASEtxtObservacion, True)
        EstadoBtnAse(False)
    End Sub

    Protected Sub ASEbtnModificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ASEbtnModificar.Click
        ASEhfOperacion.Value = "Modificar"
        EstadoControlesAse(True)
        EstadoBtnAse(False)
    End Sub

    Protected Sub ASEbtnGrabar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ASEbtnGrabar.Click
        Dim oBL As New BLAsegurado
        Dim IDUsuario As String = Membership.GetUser().UserName
        'Dim IDCertificado As String = gvBusqueda.SelectedDataKey.Item(0).ToString()
        Dim IDCertificado As String = gvAsegurado.SelectedDataKey.Item(0).ToString
        Dim IDConsecutivo As Int16 = CInt(gvAsegurado.SelectedDataKey.Item(1))
        Select Case ASEhfOperacion.Value
            Case "Modificar"
                Dim oBEAse As New BEAsegurado
                oBEAse.IdCertificado = IDCertificado
                oBEAse.Consecutivo = IDConsecutivo
                oBEAse.IdParentesco = ASEddlParentesco.SelectedValue
                'oBEAse.IdCiudad = ASEddlDis.SelectedValue
                oBEAse.Direccion = ASEtxtDireccion.Text
                oBEAse.Telefono = ASEtxtTelefono.Text.Trim
                oBEAse.Nombre1 = ASEtxtNombre1.Text.Trim.ToUpper
                oBEAse.Nombre2 = ASEtxtNombre2.Text.Trim.ToUpper
                oBEAse.Apellido1 = ASEtxtApellido1.Text.Trim.ToUpper
                oBEAse.Apellido2 = ASEtxtApellido2.Text.Trim.ToUpper
                oBEAse.CcAseg = ASEtxtNroDoc.Text.Trim
                oBEAse.IdTipoDocumento = ASEddlTipoDoc.SelectedValue
                oBEAse.UsuarioModificacion = IDUsuario
                'oBECer.Observacion = txtDCObs.Text
                oBEAse.FechaNacimiento = FormatoFecha(ASEtxtFechaNac.Text)

                If (oBL.Actualizar(oBEAse)) Then
                    ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M", "alert('Asegurado modificado con �xito');", True)
                    EstadoControlesAse(False)
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M", "alert('Ocurri� un error al modificar el Asegurado.');", True)
                    Exit Sub
                End If

            Case "Anular"
                If (oBL.Anular(IDCertificado, IDConsecutivo, IDUsuario, ASEtxtObservacion.Text.Trim)) Then
                    ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M", "alert('Asegurado anulado con �xito');", True)
                    EstadoTextBox(ASEtxtObservacion, False)
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M", "alert('Ocurri� un error al intentar anular el Asegurado.');", True)
                    Exit Sub
                End If

        End Select
        LlenarGridAsegurado()
        LlenarAsegurado()
        ASEhfOperacion.Value = ""
        EstadoBtnAse(True)
    End Sub

    Protected Sub ASEbtnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ASEbtnCancelar.Click
        CancelarEditTabASE()
    End Sub

    Private Sub CancelarEditTabASE()
        Select Case ASEhfOperacion.Value
            Case "Modificar"
                EstadoControlesAse(False)
            Case "Anular"
                EstadoTextBox(ASEtxtObservacion, False)
        End Select
        ASEhfOperacion.Value = ""
        LlenarAsegurado()
        EstadoBtnAse(True)
    End Sub

    Private Sub EstadoControlesAse(ByVal pEstado As Boolean)
        EstadoDDL(ASEddlParentesco, pEstado)
        EstadoTextBox(ASEtxtNombre1, pEstado)
        EstadoTextBox(ASEtxtNombre2, pEstado)
        EstadoTextBox(ASEtxtApellido1, pEstado)
        EstadoTextBox(ASEtxtApellido2, pEstado)
        EstadoTextBox(ASEtxtFechaNac, pEstado)
        EstadoTextBox(ASEtxtTelefono, pEstado)
        EstadoDDL(ASEddlTipoDoc, pEstado)
        EstadoTextBox(ASEtxtNroDoc, pEstado)
        EstadoTextBox(ASEtxtNroDoc, pEstado)
        EstadoTextBox(ASEtxtDireccion, pEstado)

        'EstadoDDL(ASEddlDep, pEstado)
        'EstadoDDL(ASEddlPro, pEstado)
        'EstadoDDL(ASEddlDis, pEstado)
    End Sub

    Private Sub EstadoBtnAse(ByVal pEstado As Boolean)
        Dim Anulado As Boolean = CBool(ViewState("eASEActivo"))
        ASEbtnModificar.Enabled = pEstado And Anulado
        ASEbtnAnular.Enabled = pEstado And Anulado
        ASEbtnGrabar.Enabled = Not pEstado
        ASEbtnCancelar.Enabled = Not pEstado
    End Sub

    Private Sub LimpiarControlesAse()
        ASEddlParentesco.SelectedIndex = 0
        ASEtxtNombre1.Text = ""
        ASEtxtNombre2.Text = ""
        ASEtxtApellido1.Text = ""
        ASEtxtApellido2.Text = ""
        ASEtxtFechaNac.Text = ""
        ASEtxtTelefono.Text = ""
        ASEddlTipoDoc.SelectedIndex = 0
        ASEtxtNroDoc.Text = ""
        ASEtxtDireccion.Text = ""
        'ASEddlDep.Items.Clear()
        'ASEddlDep.Items.Add(New ListItem("---------------------------", "0"))
        'ASEddlPro.Items.Clear()
        'ASEddlPro.Items.Add(New ListItem("---------------------------", "0"))
        'ASEddlDis.Items.Clear()
        'ASEddlDis.Items.Add(New ListItem("-----------------------------", "0"))
        ASEtxtDirNumero.Text = ""
        ASEtxtDirPiso.Text = ""
        ASEtxtDirDpto.Text = ""
        ASEtxtDirCPostal.Text = ""
        ASEtxtDirUbigeo.Text = ""
        ASEtxtCAreaTelefono.Text = ""

        ASEtxtPlan.Text = ""
        ASEtxtPrima.Text = ""
        ASEtxtObservacion.Text = ""
        ASEbtnModificar.Enabled = False
        ASEbtnAnular.Enabled = False
        ASEbtnGrabar.Enabled = False
        ASEbtnCancelar.Enabled = False

    End Sub

    'Protected Sub ASEddlDep_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    LlenarProvincia(ASEddlPro, ASEddlDep.SelectedValue)
    '    ASEddlPro_SelectedIndexChanged(sender, e)
    'End Sub

    'Protected Sub ASEddlPro_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    LlenarDistrito(ASEddlDis, ASEddlPro.SelectedValue)
    'End Sub

#End Region

#Region "Tab Beneficiario"
    Protected Sub btnActBeneficiario_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActBeneficiario.Click
        If Not gvBusqueda.SelectedDataKey Is Nothing Then
            If Not (CBool(ViewState("eTabBeneficiario"))) Then
                LlenarGridBeneficiario()
                gvBeneficiario.SelectedIndex = -1
                ViewState("eTabBeneficiario") = True
                LimpiarControlesBen()
                EstadoControlesBen(False)
            End If
        End If
    End Sub

    Private Sub LlenarGridBeneficiario()
        Dim oBL As New BLBeneficiario
        gvBeneficiario.DataSource = oBL.Seleccionar(gvBusqueda.SelectedDataKey.Item(0).ToString)
        gvBeneficiario.DataBind()
    End Sub

    Protected Sub gvBeneficiario_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        LlenarBeneficiario()
        EstadoBtnBen(True)
        EstadoControlesBen(False)
        EstadoTextBox(BENtxtObservacion, False)
    End Sub

    Private Sub LlenarBeneficiario()
        Dim oBL As New BLBeneficiario
        Dim oBE As New BEBeneficiario
        Dim IDBeneficiario As Integer = CInt(gvBeneficiario.SelectedDataKey.Item(0))
        oBE = oBL.Seleccionar(IDBeneficiario)
        BENddlParentesco.SelectedValue = oBE.IdParentesco
        BENtxtNombre1.Text = oBE.Nombre1
        BENtxtNombre2.Text = oBE.Nombre2
        BENtxtApellido1.Text = oBE.Apellido1
        BENtxtApellido2.Text = oBE.Apellido2
        BENtxtFechaNac.Text = oBE.FechaNacimiento
        Try
            BENddlSexo.SelectedValue = oBE.Sexo
        Catch ex As Exception
            BENddlSexo.SelectedValue = "0"
        End Try

        BENtxtTelefono.Text = oBE.Telefono
        Try
            BENddlTipoDoc.SelectedValue = oBE.IdTipoDocumento
        Catch ex As Exception
            BENddlTipoDoc.SelectedIndex = 0
        End Try

        BENtxtNroDoc.Text = oBE.CcBenef
        BENtxtDireccion.Text = oBE.Domicilio
        BENtxtOcupacion.Text = oBE.Ocupacion
        BENtxtPorcentaje.Text = oBE.Porcentaje
        BENtxtObservacion.Text = ""
        ViewState("eBENActivo") = oBE.Activo
    End Sub

    Protected Sub BENbtnModificar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        BENhfOperacion.Value = "Modificar"
        EstadoControlesBen(True)
        EstadoBtnBen(False)
    End Sub

    Protected Sub BENbtnAnular_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        BENhfOperacion.Value = "Anular"
        EstadoTextBox(BENtxtObservacion, True)
        EstadoBtnBen(False)
    End Sub

    Protected Sub BENbtnGrabar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BENbtnGrabar.Click
        If Not gvBusqueda.SelectedDataKey Is Nothing Then
            Dim oBL As New BLBeneficiario
            Dim IDUsuario As String = Membership.GetUser().UserName
            Dim IDBeneficiario As Int32 = gvBeneficiario.SelectedDataKey.Item(0).ToString()
            Dim IDCertificado As String = gvBeneficiario.SelectedDataKey.Item(1).ToString
            Dim IDConsecutivo As Int16 = CInt(gvBeneficiario.SelectedDataKey.Item(2))
            Select Case BENhfOperacion.Value
                Case "Modificar"
                    Dim oBEBen As New BEBeneficiario
                    oBEBen.IdBeneficiario = IDBeneficiario
                    oBEBen.IdCertificado = IDCertificado
                    oBEBen.Consecutivo = IDConsecutivo
                    oBEBen.IdParentesco = BENddlParentesco.SelectedValue
                    oBEBen.Domicilio = BENtxtDireccion.Text.Trim.ToUpper
                    oBEBen.Telefono = BENtxtTelefono.Text.Trim
                    oBEBen.Nombre1 = BENtxtNombre1.Text.Trim.ToUpper
                    oBEBen.Nombre2 = BENtxtNombre2.Text.Trim.ToUpper
                    oBEBen.Apellido1 = BENtxtApellido1.Text.Trim.ToUpper
                    oBEBen.Apellido2 = BENtxtApellido2.Text.Trim.ToUpper
                    oBEBen.CcBenef = BENtxtNroDoc.Text.Trim
                    oBEBen.IdTipoDocumento = BENddlTipoDoc.SelectedValue
                    oBEBen.UsuarioModificacion = IDUsuario
                    oBEBen.Observacion = ""
                    oBEBen.Ocupacion = BENtxtOcupacion.Text.Trim.ToUpper
                    oBEBen.Porcentaje = Decimal.Parse(BENtxtPorcentaje.Text)
                    oBEBen.Sexo = BENddlSexo.SelectedValue
                    oBEBen.FechaNacimiento = FormatoFecha(BENtxtFechaNac.Text)

                    If (oBL.Actualizar(oBEBen)) Then
                        ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M", "alert('Beneficiario modificado con �xito');", True)
                        EstadoControlesBen(False)
                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M", "alert('Ocurri� un error al modificar el Beneficiario.');", True)
                        Exit Sub
                    End If

                Case "Anular"
                    If (oBL.Anular(IDBeneficiario, IDCertificado, IDConsecutivo, IDUsuario, BENtxtObservacion.Text.Trim)) Then
                        ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M", "alert('Beneficiario anulado con �xito');", True)
                        EstadoTextBox(BENtxtObservacion, False)
                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M", "alert('Ocurri� un error al intentar anular el Beneficiario.');", True)
                        Exit Sub
                    End If

            End Select
            LlenarGridBeneficiario()
            LlenarBeneficiario()
            BENhfOperacion.Value = ""
            EstadoBtnBen(True)
        End If
    End Sub

    Protected Sub BENbtnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BENbtnCancelar.Click
        CancelarEditTabBEN()
    End Sub

    Private Sub CancelarEditTabBEN()
        Select Case BENhfOperacion.Value
            Case "Modificar"
                EstadoControlesBen(False)
            Case "Anular"
                EstadoTextBox(BENtxtObservacion, False)
        End Select
        BENhfOperacion.Value = ""
        LlenarBeneficiario()
        EstadoBtnBen(True)
    End Sub

    Private Sub EstadoControlesBen(ByVal pEstado As Boolean)
        EstadoDDL(BENddlParentesco, pEstado)
        EstadoTextBox(BENtxtNombre1, pEstado)
        EstadoTextBox(BENtxtNombre2, pEstado)
        EstadoTextBox(BENtxtApellido1, pEstado)
        EstadoTextBox(BENtxtApellido2, pEstado)
        EstadoTextBox(BENtxtFechaNac, pEstado)
        EstadoTextBox(BENtxtTelefono, pEstado)
        EstadoDDL(BENddlTipoDoc, pEstado)
        EstadoTextBox(BENtxtNroDoc, pEstado)
        EstadoTextBox(BENtxtNroDoc, pEstado)
        EstadoTextBox(BENtxtDireccion, pEstado)
        EstadoTextBox(BENtxtOcupacion, pEstado)
        EstadoTextBox(BENtxtPorcentaje, pEstado)
        EstadoDDL(BENddlSexo, pEstado)
    End Sub

    Private Sub EstadoBtnBen(ByVal pEstado As Boolean)
        Dim Anulado As Boolean = CBool(ViewState("eBENActivo"))
        BENbtnModificar.Enabled = pEstado And Anulado
        BENbtnAnular.Enabled = pEstado And Anulado
        BENbtnGrabar.Enabled = Not pEstado
        BENbtnCancelar.Enabled = Not pEstado
    End Sub

    Private Sub LimpiarControlesBen()
        BENddlParentesco.SelectedIndex = 0
        BENtxtNombre1.Text = ""
        BENtxtNombre2.Text = ""
        BENtxtApellido1.Text = ""
        BENtxtApellido2.Text = ""
        BENtxtFechaNac.Text = ""
        BENtxtTelefono.Text = ""
        BENddlTipoDoc.SelectedIndex = 0
        BENtxtNroDoc.Text = ""
        BENtxtDireccion.Text = ""
        BENtxtObservacion.Text = ""
        BENbtnModificar.Enabled = False
        BENbtnAnular.Enabled = False
        BENbtnGrabar.Enabled = False
        BENbtnCancelar.Enabled = False
    End Sub

#End Region

#Region "Tab Im�genes"
    Protected Sub btnActImagen_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActImagen.Click
        If Not gvBusqueda.SelectedDataKey Is Nothing Then
            'If Not (CBool(ViewState("eTabImagenes"))) Then
            Dim oBL As New BLImagen
            lbImagen.DataSource = oBL.Seleccionar(gvBusqueda.SelectedDataKey.Item(0).ToString)
            lbImagen.DataValueField = "RutaImagen"
            lbImagen.DataTextField = "NombreImagen"
            lbImagen.DataBind()
            If (lbImagen.Items.Count > 0) Then
                'imgCer.ImageUrl = lbImagen.Items(0).Value.ToString
                lbImagen.SelectedIndex = 0
                VisualizarImgCer(lbImagen.SelectedValue)
                imgCer.Visible = True
            Else
                imgCer.Visible = False
            End If
            ViewState("eTabImagenes") = True
        End If
    End Sub

    Protected Sub lbImagen_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        VisualizarImgCer(lbImagen.SelectedValue)
    End Sub
    Private Sub VisualizarImgCer(ByVal ImagenNombre As String)
        Dim UrlImg1 As String = ConfigurationManager.AppSettings("UrlImagen1").ToString
        'Dim UrlImg2 As String = ConfigurationManager.AppSettings("UrlImagen2").ToString
        imgCer.ImageUrl = UrlImg1 & ImagenNombre

        'If oBL.ValidarArchivoRemoto(UrlImg1 & ImagenNombre) Then
        '    imgCer.ImageUrl = UrlImg1 & ImagenNombre
        'Else
        '    imgCer.ImageUrl = UrlImg2 & ImagenNombre
        'End If

    End Sub
#End Region

#Region "Tab Cobro"
    Protected Sub btnActCobro_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActCobro.Click
        If Not (CBool(ViewState("eTabCobro"))) Then
            LlenarCobrosGrid()
            gvCobro.SelectedIndex = -1
            ViewState("eTabCobro") = True
            gvCobroLog.Visible = False
            gvExtorno.Visible = False
        End If
    End Sub

    Protected Sub btnImprimirECuenta_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M2", "Imprimir('" & gvBusqueda.SelectedDataKey.Item(0).ToString & "');", True)
    End Sub
    Private Sub LlenarCobrosGrid()
        If Not gvBusqueda.SelectedDataKey Is Nothing Then
            Dim oBL As New BLCobro
            gvCobro.DataSource = oBL.Seleccionar(gvBusqueda.SelectedDataKey.Item(0).ToString)
            gvCobro.DataBind()
        End If
    End Sub
    Protected Sub gvCobro_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvCobro.PageIndex = e.NewPageIndex
        LlenarCobrosGrid()
        gvCobro.SelectedIndex = -1
        gvCobroLog.Visible = False
        gvExtorno.Visible = False
    End Sub

    Protected Sub gvCobro_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub


    Protected Sub gvCobro_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        If e.CommandName = "Select" Then
            Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, ImageButton).Parent.Parent, GridViewRow)
            'gvCobro.SelectedIndex = row.RowIndex
            If e.CommandArgument = "Historial" Then
                gvCobroLog.Visible = True
                gvExtorno.Visible = False
                LlenarCroboLogGrid(row.RowIndex)
            ElseIf e.CommandArgument = "Extorno" Then
                gvExtorno.Visible = True
                gvCobroLog.Visible = False
                LlenarExtornoGrid(row.RowIndex)
            End If
        End If
        'MsgBox(row.RowIndex)
    End Sub

    Private Sub LlenarCroboLogGrid(ByVal pRowIndex As Int32)
        Dim oBL As New BLCobroLog
        'gvCobroLog.DataSource = oBL.Seleccionar(CInt(gvCobro.SelectedDataKey.Item(0)), gvCobro.SelectedDataKey.Item(1))
        gvCobroLog.DataSource = oBL.Seleccionar(CInt(gvCobro.DataKeys(pRowIndex)(0)), gvCobro.DataKeys(pRowIndex)(1))
        gvCobroLog.DataBind()
    End Sub

    Private Sub LlenarExtornoGrid(ByVal pRowIndex As Int32)
        Dim oBL As New BLExtorno
        gvExtorno.DataSource = oBL.Seleccionar(CInt(gvCobro.DataKeys(pRowIndex)(0)), gvCobro.DataKeys(pRowIndex)(1))
        gvExtorno.DataBind()
    End Sub

    Protected Sub gvCobroLog_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvCobroLog.PageIndex = e.NewPageIndex
        LlenarCroboLogGrid(gvCobro.SelectedIndex)
    End Sub
#End Region

#Region "Tab Siniestro"
    Protected Sub btnActSinRec_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActSinRec.Click
        If Not gvBusqueda.SelectedDataKey Is Nothing Then
            If Not (CBool(ViewState("eTabSinRec"))) Then
                Dim oBLRec As New BLReclamo
                gvReclamo.DataSource = oBLRec.Listar(gvBusqueda.SelectedDataKey.Item(0).ToString)
                gvReclamo.DataBind()

                Dim oBLSin As New BLSiniestro
                gvSiniestro.DataSource = oBLSin.Listar(gvBusqueda.SelectedDataKey.Item(0).ToString)
                gvSiniestro.DataBind()
                ViewState("eTabSinRec") = True

            End If
        End If
    End Sub

    Protected Sub gvReclamo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles gvReclamo.SelectedIndexChanged
        Session("IDReclamo") = gvReclamo.SelectedDataKey.Item(0).ToString()
        btnImpresionNota.Enabled = True
    End Sub

    Protected Sub btnImpresionNota_Click(sender As Object, e As EventArgs) Handles btnImpresionNota.Click

        Dim IDReclamo As String = Session("IDReclamo")

        If Not IDReclamo Is Nothing And IDReclamo <> String.Empty Then
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M2", "Imprimir('" & IDReclamo & "','N');", True)
        End If
    End Sub

#End Region

#Region "Tab Hogar"
    Protected Sub btnActHogar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActHogar.Click
        If Not gvBusqueda.SelectedDataKey Is Nothing Then
            If Not (CBool(ViewState("eTabHogar"))) Then

                ViewState("eTabHogar") = True
                EstadoBtnPHogar(True)
                LlenarHogar()
            End If
        End If
    End Sub

    Protected Sub LlenarHogar()
        If Not gvBusqueda.SelectedDataKey Is Nothing Then
            Dim oBL As New BLProductoHogar
            Dim oBE As New BEProductoHogar
            Dim IDCertificado As String = gvBusqueda.SelectedDataKey.Item(0).ToString

            tb_detalleBienes.Text = ""
            tb_superficieComercio.Text = ""

            oBE = oBL.Seleccionar(IDCertificado).Item(0)
            If oBE.IdRamo <> 0 Then
                LlenarRamos()
                Session("OProductoHogar") = oBE
                tb_detalleBienes.Text = oBE.DetalleBienes
                tb_superficieComercio.Text = oBE.Superficie
                ddlRamo.SelectedValue = oBE.IdRamo
                ViewState("eTabHogar") = True
            Else
                ddlRamo.Items.Clear()
                EstadoBtnProdHogarInactivo(False)
            End If
        End If
    End Sub

    Protected Sub PHogarbtnCancelar_Click(sender As Object, e As EventArgs) Handles PHogarbtnCancelar.Click
        CancelarEditTabPHogar()
    End Sub

    Private Sub CancelarEditTabPHogar()
        Select Case PHhfOperacion.Value
            Case "Modificar"
                EstadoControlesProdHogar(False)
        End Select
        PHhfOperacion.Value = ""
        If Not gvBusqueda.SelectedDataKey Is Nothing Then
            LlenarHogar()
        End If
        EstadoBtnPHogar(True)
    End Sub

    Protected Sub PHogarbtnModificar_Click(sender As Object, e As EventArgs) Handles PHogarbtnModificar.Click
        PHhfOperacion.Value = "Modificar"
        EstadoControlesProdHogar(True)
        EstadoBtnPHogar(False)
    End Sub

    Private Sub EstadoControlesProdHogar(ByVal pEstado As Boolean)
        'EstadoDDL(ddlRamo, pEstado)
        If Not Session("OProductoHogar") Is Nothing Then
            Dim oBE As New BEProductoHogar
            oBE = DirectCast(Session("OProductoHogar"), BEProductoHogar)
            If PHhfOperacion.Value = "Modificar" Then
                EstadoDDL(ddlRamo, False)
                EstadoTextBox(tb_detalleBienes, pEstado)
                EstadoTextBox(tb_superficieComercio, pEstado)
            Else
                EstadoDDL(ddlRamo, oBE.RamoOblig)
                EstadoTextBox(tb_detalleBienes, oBE.DetalleBienesOblig)
                EstadoTextBox(tb_superficieComercio, oBE.SuperficieOblig)
            End If
        Else
            EstadoDDL(ddlRamo, False)
            EstadoTextBox(tb_detalleBienes, False)
            EstadoTextBox(tb_superficieComercio, False)
            EstadoBtnProdHogar(False)
        End If

    End Sub

    Private Sub EstadoBtnProdHogarInactivo(ByVal pEstado As Boolean)
        Dim Anulado As Boolean = CBool(ViewState("eTabHogar"))
        PHogarbtnModificar.Enabled = pEstado And Anulado
        PHogarbtnCancelar.Enabled = pEstado And Anulado
        PHogarbtnGrabar.Enabled = pEstado And Anulado

    End Sub

    Private Sub EstadoBtnProdHogar(ByVal pEstado As Boolean)
        Dim Anulado As Boolean = CBool(ViewState("eTabHogar"))
        PHogarbtnModificar.Enabled = pEstado And Anulado
        PHogarbtnCancelar.Enabled = pEstado And Anulado
        PHogarbtnGrabar.Enabled = Not pEstado

    End Sub

    Protected Sub PHogarbtnGrabar_Click(sender As Object, e As EventArgs) Handles PHogarbtnGrabar.Click

        Dim oBL As New BLProductoHogar
        Dim IDUsuario As String = Membership.GetUser().UserName
        Dim IDCertificado As String = gvBusqueda.SelectedDataKey.Item(0).ToString()
        Dim oBE As New BEProductoHogar
        oBE = DirectCast(Session("OProductoHogar"), BEProductoHogar)
        Select Case PHhfOperacion.Value
            Case "Modificar"
                Dim oBEProdHog As New BEProductoHogar
                oBEProdHog.IdCertificado = IDCertificado
                oBEProdHog.IdRamo = ddlRamo.SelectedValue
                oBEProdHog.DetalleBienes = tb_detalleBienes.Text
                oBEProdHog.Superficie = tb_superficieComercio.Text
                oBEProdHog.UsuarioModificacion = Session("IDUsuario")

                If (oBL.Actualizar(oBEProdHog)) Then
                    ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M", "alert('La informaci�n de Riesgo del Asegurado ha sido modificado con �xito');", True)
                    EstadoControlesProdHogar(False)
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M", "alert('Ocurri� un error al modificar la informaci�n de Riesgo del Asegurado.');", True)
                    Exit Sub
                End If
        End Select
        LlenarHogar()
        hfOperacion.Value = ""
        EstadoBtnPHogar(True)
    End Sub
#End Region

#Region "Funciones"
    Public Shared Function FormatoNumeroCuenta(ByVal psNumerocuenta As String) As String
        If psNumerocuenta.Length >= 13 Then
            Dim iLongitud As Int32 = psNumerocuenta.Length
            Dim sPrimeros As [String] = psNumerocuenta.Substring(0, 4)
            Dim sUltimos As [String] = psNumerocuenta.Substring(iLongitud - 4, 4)
            Dim sbNumeroCuentaFormato As New StringBuilder("")
            sbNumeroCuentaFormato.Append(sPrimeros)
            sbNumeroCuentaFormato.Append("*"c, iLongitud - 8)
            sbNumeroCuentaFormato.Append(sUltimos)
            Return sbNumeroCuentaFormato.ToString()
        Else
            Return psNumerocuenta
        End If
    End Function

    Private Sub EstadoDDL(ByVal pDDL As DropDownList, ByVal pEstado As Boolean)
        If (pEstado) Then
            pDDL.Enabled = True
            pDDL.CssClass = ""
        Else
            pDDL.Enabled = False
            pDDL.CssClass = "ddlDisable"
        End If

    End Sub

    Private Sub EstadoTextBox(ByVal pTextBox As TextBox, ByVal pEstado As Boolean)
        If (pEstado) Then
            pTextBox.ReadOnly = False
            pTextBox.CssClass = "txtTexto"
            pTextBox.Attributes.Add("onfocus", "this.className='txtTextoFocus';")
            pTextBox.Attributes.Add("onblur", "this.className='txtTexto';")
        Else
            pTextBox.ReadOnly = True
            pTextBox.CssClass = "txtTextoD"
            pTextBox.Attributes.Add("onfocus", "")
            pTextBox.Attributes.Add("onblur", "")
        End If

    End Sub

    Private Sub EstadoCheckBox(ByVal pChekBox As CheckBox, ByVal pEstado As Boolean)
        If (pEstado) Then
            pChekBox.Enabled = True
            'pChekBox.CssClass = "txtTexto"
            'pChekBox.Attributes.Add("onfocus", "this.className='txtTextoFocus';")
            'pChekBox.Attributes.Add("onblur", "this.className='txtTexto';")
        Else
            pChekBox.Enabled = False
            'pChekBox.CssClass = "txtTextoD"
            'pChekBox.Attributes.Add("onfocus", "")
            'pChekBox.Attributes.Add("onblur", "")
        End If

    End Sub

    Private Function FormatoFecha(ByVal psFecha As String) As String
        Dim cCharS As Char = "/"
        Dim sElementos As String() = psFecha.Split(cCharS)
        Dim sDia As String = sElementos(0)
        Dim sMes As String = sElementos(1)
        Dim sAnio As String = sElementos(2)
        Return sAnio & "-" & sMes & "-" & sDia
    End Function

#End Region



    Protected Sub btnImprimir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CERbtnImprimir.Click
        ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M2", "ImprimirCer('" & txtDCNroCertificado.Text & "','C');", True)
    End Sub

    Protected Sub gvSiniestro_SelectedIndexChanged(sender As Object, e As EventArgs) Handles gvSiniestro.SelectedIndexChanged
        LLenarDatosAdicionales(gvSiniestro.SelectedDataKey.Item(0).ToString)
    End Sub


    Protected Sub gvDatosAdicionales_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvDatosAdicionales.PageIndexChanging
        gvDatosAdicionales.PageIndex = e.NewPageIndex
        LLenarDatosAdicionales(gvSiniestro.SelectedDataKey.Item(0).ToString)
    End Sub

    Private Sub LLenarDatosAdicionales(ByVal pIDSiniestro As String)
        Dim oBL As New BLInfoSiniestro
        gvDatosAdicionales.DataSource = oBL.Seleccionar(pIDSiniestro.Substring(pIDSiniestro.LastIndexOf("-") + 1))
        gvDatosAdicionales.DataBind()
    End Sub

End Class
