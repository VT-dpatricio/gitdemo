<%@ Page Language="VB" MasterPageFile="~/MasterAcceso.master" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Logi"  EnableEventValidation="false" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<br />
        <br />
        <br />
    <br />
    <br />
    <br />
    <br />
        <br />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
<asp:HiddenField id="HFIntentos" runat="server" Value="0"></asp:HiddenField>
                <asp:MultiView id="MultiView1" runat="server" ActiveViewIndex="2"><asp:View id="View1" runat="server"><TABLE style="MARGIN: auto; WIDTH: 617px" cellSpacing=0 cellPadding=0 border=0><TBODY><TR>
                                <TD class="LEncaSup"></TD></TR><TR>
                                <TD class="LEncaCen"><BR />
                                    <BR /><TABLE style="MARGIN: auto; WIDTH: 416px" cellSpacing=7 cellPadding=0 border=0>
                                        <TBODY><TR>
                                            <TD style="WIDTH: 83px; HEIGHT: 34px"></TD>
                                            <TD style="VERTICAL-ALIGN: middle; WIDTH: 272px; HEIGHT: 34px; TEXT-ALIGN: left"><IMG src="img/Login/LAcceso.png" /></TD>
                                            <TD style="HEIGHT: 34px"></TD></TR>
                                            <TR><TD class="LEtiquetaC" style="WIDTH: 83px">Usuario:</TD><TD style="WIDTH: 272px" class="txtLoginBorde"><asp:TextBox id="txtUsuario" runat="server" Width="256px" SkinID="txtLogin"></asp:TextBox></TD>
                                                <TD><asp:RequiredFieldValidator 
                        ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUsuario" 
                        ErrorMessage="*" ToolTip="Campo Requerido"></asp:RequiredFieldValidator></TD></TR>
                                            <TR><TD class="LEtiquetaC" style="WIDTH: 83px">Contrase�a:</TD><TD style="WIDTH: 272px" class="txtLoginBorde"><asp:TextBox id="txtContra" runat="server" Width="256px" SkinID="txtLogin" TextMode="Password"></asp:TextBox></TD><TD>
                                                    <asp:RequiredFieldValidator 
                        ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtContra" 
                        ErrorMessage="*" ToolTip="Campo Requerido"></asp:RequiredFieldValidator></TD></TR>
                                            <TR><TD colspan="2" style="TEXT-ALIGN: right"><asp:Label 
                        ID="lblMsg" runat="server" ForeColor="#C00000"></asp:Label>&nbsp; 
                                                <asp:ImageButton ID="btnEntrar" runat="server" 
                        ImageUrl="~/img/Login/btnIngresar.png" onclick="btnEntrar_Click"></asp:ImageButton>&nbsp; </TD>
                                                <TD></TD></TR></TBODY></TABLE>
                                    <BR /><asp:UpdateProgress id="UpdateProgress1" runat="server" DisplayAfter="0">
            <ProgressTemplate>
        <div style="width: 171px; height: 39px; margin:auto; background-color: #ffffff;">
            <table style="width: 169px">
                <tr>
                    <td style="width: 38px">
                        <img src="img/wait.gif"></td>
                    <td colspan="2" style="width: 157px">
                        Validando Usuario...</td>
                </tr>
            </table>
        </div>
            </ProgressTemplate>
        </asp:UpdateProgress><BR /><BR /></TD></TR>
                                <TR><TD class="LEncaInf"></TD></TR></TBODY></TABLE></asp:View> <asp:View id="View2" runat="server">
                        <div style="width: 254px; height: 138px; margin:auto; border-right: #cc9900 1px solid; border-top: #cc9900 1px solid; border-left: #cc9900 1px solid; border-bottom: #cc9900 1px solid; background-color: #ffffff; color: #ff3333; text-align: center;">
                            <br />
                            <br />
                            <br />
                            Usted a superado el n�mero de Intentos para el inicio de sesi�n<br />
                            Por favor contacte con el Administrador</div>
                    </asp:View>
        <asp:View ID="View3" runat="server">

 <table style="MARGIN: auto; WIDTH: 617px" cellSpacing="0" cellPadding="0" border="0">
 
         <tr>
             <td class="LEncaSup">
             </td>
         </tr>
             <tr>
                 <td class="LEncaCen">
                     <BR />
                     <BR />
                     <table border="0" cellpadding="0" cellspacing="7" 
                         style="MARGIN: auto; WIDTH: 416px; height: 167px;">
                         <tr>
                             <td style="text-align: center;" class="style2">
                                 <asp:Image ID="imgMsg" runat="server" ImageUrl="~/Img/Icons/denegado.png" />
                                 <br />
                                 <br />
                                 <asp:Label ID="lblMensaje" runat="server" Font-Size="12px" ForeColor="#C00000">Acceso denegado</asp:Label>
                             </td>
                         </tr>
                     </table>
                     <BR />
                     
                     <BR />
                     <BR />
                 </td>
             </tr>
          <tr>
                     <td class="LEncaInf">
                     </td>
                 </tr>
             
</table>
                    </asp:View>            
                    
                    
                    
                     </asp:MultiView> 
</ContentTemplate>
        </asp:UpdatePanel>
        <br />




</asp:Content>

