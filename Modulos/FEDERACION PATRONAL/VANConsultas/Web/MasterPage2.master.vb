Imports System.Web.Security
Partial Class MasterPage2
    Inherits System.Web.UI.MasterPage

    'Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    '    ' LLena las variables User y Pass con controles contenidos en el LoginView1
    '    Dim User As String = DirectCast(LoginView1.Controls.Item(0).Controls.Item(3), TextBox).Text
    '    Dim Pass As String = DirectCast(LoginView1.Controls.Item(0).Controls.Item(7), TextBox).Text

    '    ' Autentica al usuario en el sitio
    '    If Membership.ValidateUser(User, Pass) Then            
    '        FormsAuthentication.RedirectFromLoginPage(User, False)
    '        ' Almanacena el password del usuario en una varible de Session 
    '        ' con el fin de pasar este dato como parametro a los reportes 
    '        Session("Password") = Pass
    '    End If

    'End Sub
    'Protected Sub LoginStatus1_LoggingOut(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.LoginCancelEventArgs) Handles LoginStatus1.LoggingOut
    ' Se borran las variables de Session
    'Session.Clear()
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not HttpContext.Current.Request.IsAuthenticated Then
            Response.Redirect("~/Login.aspx")
        End If

        '    Dim o As New AccessSiteMapProvider()
        '    o = CType(SiteMap.Providers("AccessSiteMapProvider"), AccessSiteMapProvider)
        '    o.OnSiteMapChanged()
        'Page.ClientScript.RegisterClientScriptInclude("js1", Request.ApplicationPath & Page.ResolveClientUrl("~/script/jquery-1.3.2.min.js"))

        Page.Header.DataBind()

        'Page.ClientScript.RegisterClientScriptInclude("js1", Page.ResolveClientUrl("~/script/jquery-1.3.2.min.js"))
        'Page.ClientScript.RegisterClientScriptInclude("js2", Page.ResolveClientUrl("~/script/superfish.js"))
        'Page.ClientScript.RegisterClientScriptInclude("FormScript", System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath + "/script/jquery-1.3.2.min.js");


    End Sub

    'Public Sub RegisterPostBackTrigger(ByVal triggerOn As Control)
    '    Me.ScriptManager1.RegisterPostBackControl(triggerOn)
    'End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim MyFirstCtrl As Control = Page.Header.FindControl("FirstCtrlID")
        Page.Header.Controls.Remove(MyFirstCtrl)
        Page.Header.Controls.AddAt(0, MyFirstCtrl)
    End Sub

End Class

