﻿Imports VAN.Siniestros.BE
Imports VAN.Siniestros.DL
Imports System
Imports System.Collections
Public Class BLCobertura
    Public Function Listar(ByVal pIDTipoSiniestro As Int32, ByVal pIDProducto As Int32, ByVal pOpcion As String) As IList
        Dim oDL As New DLNTCobertura
        Dim oBE As New BECobertura
        oBE.IDTipoSiniestro = pIDTipoSiniestro
        oBE.IDProducto = pIDProducto
        oBE.Opcion = pOpcion
        Return oDL.Seleccionar(oBE)
    End Function
End Class
