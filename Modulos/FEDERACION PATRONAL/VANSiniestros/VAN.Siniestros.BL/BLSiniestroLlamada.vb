﻿Imports VAN.Siniestros.BE
Imports VAN.Siniestros.DL
Imports System
Imports System.Collections
Public Class BLSiniestroLlamada
    Public Function Listar(ByVal pIDSiniestro As Int32) As IList
        Dim oDL As New DLNTSiniestroLlamada
        Dim oBE As New BESiniestroLlamada
        Return oDL.Seleccionar(pIDSiniestro)
    End Function

    Public Function Insertar(ByVal pEntidad As BESiniestroLlamada) As Boolean
        Dim oDL As New DLTSiniestroLlamada
        Return oDL.Insertar(pEntidad)
    End Function
End Class
