Imports VAN.Siniestros.BE
Imports VAN.Siniestros.DL
Imports System
Imports System.Collections
Public Class BLListaGenerica

    Public Function Seleccionar(ByVal pIDProducto As Integer, ByVal pTabla As String) As IList
        Dim oDL As New DLNTListaGenerica
        Dim oBE As New BEListaGenerica
        oBE.IdProducto = pIDProducto
        oBE.Tabla = pTabla
        Return oDL.Seleccionar(oBE)
    End Function

End Class
