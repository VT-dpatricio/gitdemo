Imports VAN.Siniestros.BE
Imports VAN.Siniestros.DL
Imports System
Imports System.Collections
Public Class BLListaEquivalencia

    Public Function Seleccionar(ByVal pIDProducto As Integer, ByVal pTabla As String) As IList
        Dim oDL As New DLNTListaEquivalencia
        Dim oBE As New BEListaEquivalencia
        oBE.IdProducto = pIDProducto
        oBE.Tabla = pTabla
        Return oDL.Seleccionar(oBE)
    End Function

    Public Function getValorAON(ByVal pLista As IList, ByVal pValoEquivalente As String) As BEListaEquivalencia
        Dim oBE As New BEListaEquivalencia
        For Each oBEEQ As BEListaEquivalencia In pLista
            If oBEEQ.ValorEquivalencia = pValoEquivalente Then
                oBE = oBEEQ
                Exit For
            End If
        Next
        Return oBE
    End Function

    Public Function getValorAON(ByVal pIDProducto As Integer, ByVal pTabla As String, ByVal pValoEquivalente As String) As BEListaEquivalencia
        Dim oBE As New BEListaEquivalencia
        Dim pLista As IList = Seleccionar(pIDProducto, pTabla)
        Return getValorAON(pLista, pValoEquivalente)
    End Function


    Public Function getValorEquivalencia(ByVal pLista As IList, ByVal pValoAON As String) As BEListaEquivalencia
        Dim oBE As New BEListaEquivalencia
        For Each oBEEQ As BEListaEquivalencia In pLista
            If oBEEQ.ValorAON = pValoAON Then
                oBE = oBEEQ
                Exit For
            End If
        Next
        Return oBE
    End Function

    Public Function getValorEquivalencia(ByVal pIDProducto As Integer, ByVal pTabla As String, ByVal pValoAON As String) As BEListaEquivalencia
        Dim oBE As New BEListaEquivalencia
        Dim pLista As IList = Seleccionar(pIDProducto, pTabla)
        Return getValorEquivalencia(pLista, pValoAON)
    End Function

End Class
