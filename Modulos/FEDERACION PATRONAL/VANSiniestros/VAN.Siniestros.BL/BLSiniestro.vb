﻿Imports VAN.Siniestros.BE
Imports VAN.Siniestros.DL
Imports System
Imports System.Collections
Public Class BLSiniestro
    Public Function Buscar(ByVal pIDEntidad As Int32, ByVal pIDUsuario As String, ByVal pIDFiltro As String, ByVal pParametro1 As String, ByVal pParametro2 As String, ByVal pIDEstadoSiniestro As String) As IList
        Dim oDL As New DLNTSiniestro
        Dim oBE As New BESiniestro
        oBE.IDEntidad = pIDEntidad
        oBE.Usuario = pIDUsuario
        oBE.IDFiltro = pIDFiltro
        oBE.Parametro1 = pParametro1
        oBE.Parametro2 = pParametro2
        oBE.IDEstadoSiniestro = pIDEstadoSiniestro
        Return oDL.Seleccionar(oBE)
    End Function

    Public Function Seleccionar(ByVal pIDSiniestro As Int32) As BESiniestro
        Dim oDL As New DLNTSiniestro
        Return oDL.SeleccionarBE(pIDSiniestro)
    End Function


    Public Function Seleccionar(ByVal pIDCertificado As String) As IList
        Dim oDL As New DLNTSiniestro
        Return oDL.Seleccionar(pIDCertificado)
    End Function

    Public Function Insertar(ByVal pEntidad As BESiniestro) As Int32
        Dim oDL As New DLTSiniestro
        Return oDL.InsertarS(pEntidad)
    End Function

    Public Function Actualizar(ByVal pEntidad As BESiniestro) As Int32
        Dim oDL As New DLTSiniestro
        Return oDL.Actualizar(pEntidad)
    End Function

    Public Function Evaluacion(ByVal pEntidad As BESiniestro) As Int32
        Dim oDL As New DLTSiniestro
        Return oDL.Evaluacion(pEntidad)
    End Function

End Class
