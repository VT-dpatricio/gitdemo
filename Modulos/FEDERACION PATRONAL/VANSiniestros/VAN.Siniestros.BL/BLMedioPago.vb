﻿Imports VAN.Siniestros.BE
Imports VAN.Siniestros.DL
Imports System
Imports System.Collections

Public Class BLMedioPago

    Private Const COD_ID_TIPO_CUENTA As String = "C"
    Private Const COD_ID_TIPO_TARJETA As String = "T"

    Public Function Seleccionar() As IList
        Dim oDL As New DLNTMedioPago

        Return oDL.Seleccionar()
    End Function

    Public Function Seleccionar(ByVal IDMedioPago As String) As BEMedioPago
        Dim oDL As New DLNTMedioPago
        Dim oBE As New BEMedioPago
        oBE.IDMedioPago = IDMedioPago

        Return oDL.Seleccionar(oBE)
    End Function

    Public Function IsCuenta(ByVal IDMedioPago As String) As Boolean
        Dim oBLMP As New BLMedioPago
        Dim oBEmp As BEMedioPago

        oBEmp = oBLMP.Seleccionar(IDMedioPago)
        If Not oBEmp Is Nothing AndAlso oBEmp.IDTipoMedioPago = COD_ID_TIPO_CUENTA Then
            Return True
        End If
        Return False
    End Function

    Public Function IsTarjeta(ByVal IDMedioPago As String) As Boolean
        Dim oDL As New DLNTMedioPago
        Dim oBEmp As BEMedioPago

        oBEmp = oDL.Seleccionar(IDMedioPago)
        If Not oBEmp Is Nothing AndAlso oBEmp.IDTipoMedioPago = COD_ID_TIPO_TARJETA Then
            Return True
        End If
        Return False
    End Function

    Public Function SeleccionarPermitidos() As IList
        Dim oDL As New DLNTMedioPago
        Dim oBE As New BEMedioPago
        Dim lista As New ArrayList
        Dim listaAux As New ArrayList

        oBE.IDMedioPago = String.Empty
        lista = oDL.Seleccionar(oBE)
        For Each ob As BEMedioPago In lista
            If Not String.IsNullOrEmpty(ob.IDTipoMedioPago) Then
                listaAux.Add(ob)
            End If
        Next
        Return listaAux
    End Function
End Class
