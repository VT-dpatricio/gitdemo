﻿Imports VAN.Siniestros.BE
Imports VAN.Siniestros.DL
Imports System
Imports System.Collections
Public Class BLEvaluacion
    Public Function Listar(ByVal pIDSiniestro As Int32, ByVal pIDProducto As Int32) As IList
        Dim oDL As New DLNTEvaluacion
        Dim oBE As New BEEvaluacion
        oBE.IDProducto = pIDProducto
        oBE.IDSiniestro = pIDSiniestro
        Return oDL.Seleccionar(oBE)
    End Function
End Class
