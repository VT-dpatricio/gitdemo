﻿Public Class BESiniestroCobertura
    Inherits BEBase
    Private _iDCobertura As Integer
    Private _iDSiniestro As Integer
    Private _cobertura As String
    Private _estado As Boolean

    Public Property IDCobertura() As Integer
        Get
            Return _iDCobertura
        End Get
        Set(ByVal value As Integer)
            _iDCobertura = value
        End Set
    End Property

    Public Property IDSiniestro() As Integer
        Get
            Return _iDSiniestro
        End Get
        Set(ByVal value As Integer)
            _iDSiniestro = value
        End Set
    End Property

    Public Property Cobertura() As String
        Get
            Return _cobertura
        End Get
        Set(ByVal value As String)
            _cobertura = value
        End Set
    End Property

    Public Property Estado() As Boolean
        Get
            Return _estado
        End Get
        Set(ByVal value As Boolean)
            _estado = value
        End Set
    End Property
End Class
