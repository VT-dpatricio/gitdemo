﻿Public Class BESiniestroDocumento
    Inherits BEBase
    Private _iDDocumento As Integer
    Private _iDSiniestro As Integer
    Private _iDSiniestroDoc As Integer
    Private _documento As String
    Private _adjunto As String
    Private _fechaEntrega As Date
    Private _entregado As Boolean
    Private _usuarioModificacion As String
    Private _usuarioCreacion As String
    Private _fechaModificacion As DateTime
    Private _fechaCreacion As DateTime

    Public Property IDSiniestro() As Integer
        Get
            Return _iDSiniestro
        End Get
        Set(ByVal value As Integer)
            _iDSiniestro = value
        End Set
    End Property

    Public Property IDSiniestroDoc() As Integer
        Get
            Return _iDSiniestroDoc
        End Get
        Set(ByVal value As Integer)
            _iDSiniestroDoc = value
        End Set
    End Property

    Public Property IDDocumento() As Integer
        Get
            Return _iDDocumento
        End Get
        Set(ByVal value As Integer)
            _iDDocumento = value
        End Set
    End Property

    Public Property Documento() As String
        Get
            Return _documento
        End Get
        Set(ByVal value As String)
            _documento = value
        End Set
    End Property

    Public Property Adjunto() As String
        Get
            Return _adjunto
        End Get
        Set(ByVal value As String)
            _adjunto = value
        End Set
    End Property

    Public Property FechaEntrega() As Date
        Get
            Return _fechaEntrega
        End Get
        Set(ByVal value As Date)
            _fechaEntrega = value
        End Set
    End Property

    Public Property Entregado() As Boolean
        Get
            Return _entregado
        End Get
        Set(ByVal value As Boolean)
            _entregado = value
        End Set
    End Property

    Public Property UsuarioCreacion() As String
        Get
            Return _usuarioCreacion
        End Get
        Set(ByVal value As String)
            _usuarioCreacion = value
        End Set
    End Property

    Public Property UsuarioModificacion() As String
        Get
            Return _usuarioModificacion
        End Get
        Set(ByVal value As String)
            _usuarioModificacion = value
        End Set
    End Property

    Public Property FechaCreacion() As String
        Get
            Return _fechaCreacion
        End Get
        Set(ByVal value As String)
            _fechaCreacion = value
        End Set
    End Property

    Public Property FechaModificacion() As String
        Get
            Return _fechaModificacion
        End Get
        Set(ByVal value As String)
            _fechaModificacion = value
        End Set
    End Property

End Class
