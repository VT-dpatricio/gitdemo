﻿Public Class BESeguridadConfig
    Inherits BEBase
    Private _iDParametro As String = String.Empty
    Private _iDEntidad As Integer = 0
    Private _valor As String = String.Empty

    Public Property IDParametro() As String
        Get
            Return Me._iDParametro
        End Get
        Set(ByVal value As String)
            Me._iDParametro = value
        End Set
    End Property

    Public Property IDEntidad() As Integer
        Get
            Return _iDEntidad
        End Get
        Set(ByVal value As Integer)
            _iDEntidad = value
        End Set
    End Property

    Public Property Valor() As String
        Get
            Return Me._valor
        End Get
        Set(ByVal value As String)
            Me._valor = value
        End Set
    End Property
End Class
