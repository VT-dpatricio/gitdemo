Public Class BEProducto
    Inherits BEBase

    Private _iDProducto As Integer = 0
    Private _iDEntidad As Integer = 0
    Private _descripcion As String = String.Empty
    Private _iDUsuario As String = String.Empty
    Private _opcion As String = String.Empty

    Public Property IDProducto() As String
        Get
            Return Me._iDProducto
        End Get
        Set(ByVal value As String)
            Me._iDProducto = value
        End Set
    End Property

    Public Property IDUsuario() As String
        Get
            Return Me._iDUsuario
        End Get
        Set(ByVal value As String)
            Me._iDUsuario = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._descripcion
        End Get
        Set(ByVal value As String)
            Me._descripcion = value
        End Set
    End Property

    Public Property IDEntidad() As Integer
        Get
            Return _iDEntidad
        End Get
        Set(ByVal value As Integer)
            _iDEntidad = value
        End Set
    End Property

    Public Property Opcion() As String
        Get
            Return Me._opcion
        End Get
        Set(ByVal value As String)
            Me._opcion = value
        End Set
    End Property

End Class
