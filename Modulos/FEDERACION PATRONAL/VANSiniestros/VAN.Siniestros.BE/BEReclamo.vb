﻿Public Class BEReclamo
    Inherits BEBase

    Private _iDReclamo As Integer

    Public Property IDReclamo() As Integer
        Get
            Return _iDReclamo
        End Get
        Set(ByVal value As Integer)
            _iDReclamo = value
        End Set
    End Property

    Private _iDCertificado As String
    Public Property IDCertificado() As String
        Get
            Return _iDCertificado
        End Get
        Set(ByVal value As String)
            _iDCertificado = value
        End Set
    End Property

    Private _fechaAtencion As Date
    Public Property FechaAtencion() As Date
        Get
            Return _fechaAtencion
        End Get
        Set(ByVal value As Date)
            _fechaAtencion = value
        End Set
    End Property

    Private _fechaFinAtencion As Date
    Public Property FechaFinAtencion() As Date
        Get
            Return _fechaFinAtencion
        End Get
        Set(ByVal value As Date)
            _fechaFinAtencion = value
        End Set
    End Property


    Private _fechaReclamo As Date
    Public Property FechaReclamo() As Date
        Get
            Return _fechaReclamo
        End Get
        Set(ByVal value As Date)
            _fechaReclamo = value
        End Set
    End Property


    Private _nroReclamo As String
    Public Property NroReclamo() As String
        Get
            Return _nroReclamo
        End Get
        Set(ByVal value As String)
            _nroReclamo = value
        End Set
    End Property

    Private _imagen As Boolean
    Public Property Imagen() As Boolean
        Get
            Return _imagen
        End Get
        Set(ByVal value As Boolean)
            _imagen = value
        End Set
    End Property

    Private _nroCobro As Int32
    Public Property NroCobro() As Int32
        Get
            Return _nroCobro
        End Get
        Set(ByVal value As Int32)
            _nroCobro = value
        End Set
    End Property

    Private _siniestro As Boolean
    Public Property Siniestro() As Boolean
        Get
            Return _siniestro
        End Get
        Set(ByVal value As Boolean)
            _siniestro = value
        End Set
    End Property

    Private _cobroInicioPeriodo As Date
    Public Property CobroInicioPeriodo() As Date
        Get
            Return _cobroInicioPeriodo
        End Get
        Set(ByVal value As Date)
            _cobroInicioPeriodo = value
        End Set
    End Property

    Private _cobroFinPeriodo As Date
    Public Property CobroFinPeriodo() As Date
        Get
            Return _cobroFinPeriodo
        End Get
        Set(ByVal value As Date)
            _cobroFinPeriodo = value
        End Set
    End Property

    Private _nroCobroDevolver As Int32
    Public Property NroCobroDevolver() As Int32
        Get
            Return _nroCobroDevolver
        End Get
        Set(ByVal value As Int32)
            _nroCobroDevolver = value
        End Set
    End Property


    Private _montoTotal As Double
    Public Property MontoTotal() As Double
        Get
            Return _montoTotal
        End Get
        Set(ByVal value As Double)
            _montoTotal = value
        End Set
    End Property

    Private _iDMoneda As String
    Public Property IDMoneda() As String
        Get
            Return _iDMoneda
        End Get
        Set(ByVal value As String)
            _iDMoneda = value
        End Set
    End Property


    Private _fechaExtorno As Date
    Public Property FechaExtorno() As Date
        Get
            Return _fechaExtorno
        End Get
        Set(ByVal value As Date)
            _fechaExtorno = value
        End Set
    End Property


    Private _operacionExtorno As String
    Public Property OperacionExtorno() As String
        Get
            Return _operacionExtorno
        End Get
        Set(ByVal value As String)
            _operacionExtorno = value
        End Set
    End Property


    Private _iDMotivoReclamo As Int32
    Public Property IDMotivoReclamo() As Int32
        Get
            Return _iDMotivoReclamo
        End Get
        Set(ByVal value As Int32)
            _iDMotivoReclamo = value
        End Set
    End Property

    Private _iDEstadoReclamo As Int32
    Public Property IDEstadoReclamo() As Int32
        Get
            Return _iDEstadoReclamo
        End Get
        Set(ByVal value As Int32)
            _iDEstadoReclamo = value
        End Set
    End Property

    Private _estadoReclamo As String
    Public Property EstadoReclamo() As String
        Get
            Return _estadoReclamo
        End Get
        Set(ByVal value As String)
            _estadoReclamo = value
        End Set
    End Property

    Private _observacion As String
    Public Property Observacion() As String
        Get
            Return _observacion
        End Get
        Set(ByVal value As String)
            _observacion = value
        End Set
    End Property

    Private _usuarioCreacion As String
    Public Property UsuarioCreacion() As String
        Get
            Return _usuarioCreacion
        End Get
        Set(ByVal value As String)
            _usuarioCreacion = value
        End Set
    End Property

    Private _usuarioModificacion As String
    Public Property UsuarioModificacion() As String
        Get
            Return _usuarioModificacion
        End Get
        Set(ByVal value As String)
            _usuarioModificacion = value
        End Set
    End Property


    Private _parametro1 As String
    Public Property Parametro1() As String
        Get
            Return _parametro1
        End Get
        Set(ByVal value As String)
            _parametro1 = value
        End Set
    End Property

    Private _parametro2 As String
    Public Property Parametro2() As String
        Get
            Return _parametro2
        End Get
        Set(ByVal value As String)
            _parametro2 = value
        End Set
    End Property

    Dim _idFiltro As String
    Public Property IDFiltro() As String
        Get
            Return _idFiltro
        End Get
        Set(ByVal value As String)
            _idFiltro = value
        End Set
    End Property

    Dim _producto As String
    Public Property Producto() As String
        Get
            Return _producto
        End Get
        Set(ByVal value As String)
            _producto = value
        End Set
    End Property

    Dim _tipoDocumento As String
    Public Property TipoDocumento() As String
        Get
            Return _tipoDocumento
        End Get
        Set(ByVal value As String)
            _tipoDocumento = value
        End Set
    End Property

    Dim _ccCliente As String
    Public Property CcCliente() As String
        Get
            Return _ccCliente
        End Get
        Set(ByVal value As String)
            _ccCliente = value
        End Set
    End Property

    Private _nombre1 As String
    Public Property Nombre1() As String
        Get
            Return _nombre1
        End Get
        Set(ByVal value As String)
            _nombre1 = value
        End Set
    End Property

    Private _apellido1 As String
    Public Property Apellido1() As String
        Get
            Return _apellido1
        End Get
        Set(ByVal value As String)
            _apellido1 = value
        End Set
    End Property

    Private _moneda As String
    Public Property Moneda() As String
        Get
            Return _moneda
        End Get
        Set(ByVal value As String)
            _moneda = value
        End Set
    End Property

    Private _simboloMoneda As String
    Public Property SimboloMoneda() As String
        Get
            Return _simboloMoneda
        End Get
        Set(ByVal value As String)
            _simboloMoneda = value
        End Set
    End Property

    Private _generarDevolucion As Boolean
    Public Property GenerarDevolucion() As Boolean
        Get
            Return _generarDevolucion
        End Get
        Set(ByVal value As Boolean)
            _generarDevolucion = value
        End Set
    End Property


    Private _fechaCreacion As Date
    Public Property FechaCreacion() As Date
        Get
            Return _fechaCreacion
        End Get
        Set(ByVal value As Date)
            _fechaCreacion = value
        End Set
    End Property

    Private _identidad As Int32
    Public Property IDEntidad() As String
        Get
            Return _identidad
        End Get
        Set(ByVal value As String)
            _identidad = value
        End Set
    End Property

    Private _usuario As String
    Public Property Usuario() As String
        Get
            Return Me._usuario
        End Get
        Set(ByVal value As String)
            Me._usuario = value
        End Set
    End Property


    Private _aseDireccion As String
    Public Property AseDireccion() As String
        Get
            Return _aseDireccion
        End Get
        Set(ByVal value As String)
            _aseDireccion = value
        End Set
    End Property

    Private _aseTelefono As String
    Public Property AseTelefono() As String
        Get
            Return _aseTelefono
        End Get
        Set(ByVal value As String)
            _aseTelefono = value
        End Set
    End Property

    Private _aseEmail As String
    Public Property AseEmail() As String
        Get
            Return _aseEmail
        End Get
        Set(ByVal value As String)
            _aseEmail = value
        End Set
    End Property

    Private _medioDevolucion As String
    Public Property MedioDevolucion() As String
        Get
            Return _medioDevolucion
        End Get
        Set(ByVal value As String)
            _medioDevolucion = value
        End Set
    End Property


    Private _medioDevolucionOriginal As String
    Public Property MedioDevolucionOriginal() As String
        Get
            Return _medioDevolucionOriginal
        End Get
        Set(ByVal value As String)
            _medioDevolucionOriginal = value
        End Set
    End Property

    Private _observacionAse As String
    Public Property ObservacionAsegurado() As String
        Get
            Return _observacionAse
        End Get
        Set(ByVal value As String)
            _observacionAse = value
        End Set
    End Property

    Private _cuentaExterna As Boolean = False
    Public Property CuentaExterna() As Boolean
        Get
            Return _cuentaExterna
        End Get
        Set(ByVal value As Boolean)
            _cuentaExterna = value
        End Set
    End Property

    Private _medioDevolucionCipher As BitArray
    Public Property MedioDevolucionCipher() As BitArray
        Get
            Return _medioDevolucionCipher
        End Get
        Set(ByVal value As BitArray)
            _medioDevolucionCipher = value
        End Set
    End Property

    Private _cbu As String = String.Empty
    Public Property CBU() As String
        Get
            Return _cbu
        End Get
        Set(ByVal value As String)
            _cbu = value
        End Set
    End Property

    Private _raiz As String = String.Empty
    Public Property RAIZ() As String
        Get
            Return _raiz
        End Get
        Set(ByVal value As String)
            _raiz = value
        End Set
    End Property

    Private _idMedioPago As String = String.Empty
    Public Property IDMedioPago() As String
        Get
            Return _idMedioPago
        End Get
        Set(ByVal value As String)
            _idMedioPago = value
        End Set
    End Property
End Class
