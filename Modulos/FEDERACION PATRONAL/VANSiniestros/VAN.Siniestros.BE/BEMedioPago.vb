﻿Public Class BEMedioPago
    Inherits BEBase

    Private _idMedioPago As String = String.Empty
    Public Property IDMedioPago() As String
        Get
            Return _idMedioPago
        End Get
        Set(ByVal value As String)
            _idMedioPago = value
        End Set
    End Property

    Private _nombre As String = String.Empty
    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Private _idTipoMedioPago As String = String.Empty
    Public Property IDTipoMedioPago() As String
        Get
            Return _idTipoMedioPago
        End Get
        Set(ByVal value As String)
            _idTipoMedioPago = value
        End Set
    End Property

End Class
