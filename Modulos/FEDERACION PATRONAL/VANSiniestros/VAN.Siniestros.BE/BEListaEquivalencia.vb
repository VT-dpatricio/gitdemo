﻿Public Class BEListaEquivalencia
    Inherits BEBase
    Private _valorAON As String = String.Empty
    Private _valorEquivalencia As String = String.Empty
    Private _descripcion As String = String.Empty

    Private _idProducto As Int32 = 0
    Private _tabla As String = String.Empty

    Public Property ValorAON() As String
        Get
            Return _valorAON
        End Get
        Set(ByVal value As String)
            _valorAON = value
        End Set
    End Property

    Public Property ValorEquivalencia() As String
        Get
            Return _valorEquivalencia
        End Get
        Set(ByVal value As String)
            _valorEquivalencia = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property

    Public Property IdProducto() As Int32
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Int32)
            _idProducto = value
        End Set
    End Property

    Public Property Tabla() As String
        Get
            Return _tabla
        End Get
        Set(ByVal value As String)
            _tabla = value
        End Set
    End Property
End Class
