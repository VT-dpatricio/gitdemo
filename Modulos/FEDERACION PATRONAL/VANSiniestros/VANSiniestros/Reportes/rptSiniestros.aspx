﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Reportes.Master" CodeBehind="rptSiniestros.aspx.vb" Inherits="VANSiniestros.rptSiniestros" culture="es-PE" uiCulture="es-PE"%>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845DCD8080CC91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="../Controles/Cargando.ascx" tagname="Cargando" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script language="JavaScript" type="text/JavaScript">
    var rpt = '<%=RptV.ClientID%>';
    var altoEnca = 220;
    $(document).ready(function () { Load(); });
    function Load() {
        $("#<%=lbProducto.ClientID%>").dropdownchecklist({ firstItemChecksAll: true, maxDropHeight: 200, width: 200, emptyText: "Seleccione..." });
    }        
</script> 
<style type="text/css">
body
{
background:#FFF;   
}
</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id="FiltroRpt" style="height:100px">
    <div id="FiltroRptTituloL">
        <div id="FiltroRptTitulo" >Siniestro - Opciones de Filtros de Información -</div>
    </div>           
            <table style="width: 849px" cellspacing="4">
                <tr>
                    <td style="width: 42px" class="TilCeldaD">
                        Desde:</td>
                    <td style="width: 84px">
                        <asp:TextBox ID="txtFDesde" runat="server" Width="56px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RFV_FD" runat="server" ControlToValidate="txtFDesde"
                            ErrorMessage="Ingrese la Fecha de Inicio" ForeColor="Red" 
                            SetFocusOnError="True">*</asp:RequiredFieldValidator>
                        <asp:CompareValidator
                                ID="CV_FD" runat="server" ControlToValidate="txtFDesde" ErrorMessage="Ingrese una Fecha de Inicio válida"
                                Operator="DataTypeCheck" Type="Date" ForeColor="Red">*</asp:CompareValidator><asp:calendarextender
                                    id="CExt1" runat="server" format="dd/MM/yyyy" targetcontrolid="txtFDesde"> </asp:calendarextender><asp:maskededitextender
                                        id="MEE1" runat="server" cleartextoninvalid="True" mask="99/99/9999" masktype="Date"
                                        targetcontrolid="txtFDesde"></asp:maskededitextender></td>
                    <td style="width: 57px" class="TilCeldaD">
            Entidad:</td>
                    <td style="width: 233px">
                        &nbsp;<asp:DropDownList ID="ddlEntidad" runat="server" Width="218px" AutoPostBack="True" AppendDataBoundItems="True">
                            <asp:ListItem Value="-1">-- Seleccionar--</asp:ListItem>
                            <asp:ListItem Value="0">Todas</asp:ListItem>
                        </asp:DropDownList><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                            ControlToValidate="ddlEntidad" ErrorMessage="Seleccione la Entidad" 
                            InitialValue="0" Enabled="False" ForeColor="Red">*</asp:RequiredFieldValidator></td>
                    <td style="width: 86px" class="TilCeldaD">
                        Estado:</td>
                    <td style="width: 131px">
                        <asp:DropDownList ID="ddlEstado" runat="server" Width="150px" 
                            AppendDataBoundItems="True">
                            <asp:ListItem Value="0">Todos</asp:ListItem>
                        </asp:DropDownList></td>
                    <td>
                        <asp:ValidationSummary ID="ValSum" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                            ShowMessageBox="True" ShowSummary="False" />
                        <asp:Button ID="BtnVerReporte" runat="server" Text="Ver Reporte" Width="84px" /></td>
                </tr>
                <tr>
                    <td class="TilCeldaD" style="width: 42px">
                        Hasta:</td>
                    <td style="width: 84px">
                        <asp:TextBox ID="txtFHasta" runat="server" Width="56px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RFV_FH" runat="server" ControlToValidate="txtFHasta"
                            ErrorMessage="Ingrese la Fecha de Fin" ForeColor="Red">*</asp:RequiredFieldValidator>
                        <asp:CompareValidator
                                ID="CV_FH" runat="server" ControlToValidate="txtFHasta" ErrorMessage="Ingrese una Fecha de Fin válida"
                                Operator="DataTypeCheck" Type="Date" ForeColor="Red">*</asp:CompareValidator><asp:calendarextender
                                    ID="CExt2" runat="server" Format="dd/MM/yyyy" 
                            TargetControlID="txtFHasta">
                                </asp:calendarextender>
                        <asp:maskededitextender ID="MEE2" runat="server" ClearTextOnInvalid="True" Mask="99/99/9999"
                            MaskType="Date" TargetControlID="txtFHasta">
                        </asp:maskededitextender>
                    </td>
                    <td class="TilCeldaD" style="width: 57px">
            Producto:</td>
                    <td style="width: 233px">
                        <asp:UpdatePanel id="UPProducto" runat="server" UpdateMode="Conditional">
                            <contenttemplate>
<asp:ListBox id="lbProducto" runat="server" Width="176px" AppendDataBoundItems="true" CssClass="ocultar" SelectionMode="Multiple" __designer:wfdid="w12"></asp:ListBox> 
                                <asp:RequiredFieldValidator id="RF_P" runat="server" InitialValue=" " 
                                    ErrorMessage="Seleccione Producto" ControlToValidate="lbProducto" 
                                    __designer:wfdid="w13" ForeColor="Red">*</asp:RequiredFieldValidator> 
</contenttemplate>
                            <Triggers>
<asp:AsyncPostBackTrigger ControlID="ddlEntidad" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
</Triggers>
                        </asp:UpdatePanel>
                    </td>
                    <td class="TilCeldaD" style="width: 86px">
                        &nbsp;</td>
                    <td style="width: 131px">
                        &nbsp;</td>
                    <td>
                    </td>
                </tr>
            </table>            
            </div>  
    <rsweb:ReportViewer ID="RptV" runat="server" Width="100%"></rsweb:ReportViewer>

    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0">
    <ProgressTemplate>
            <uc1:Cargando ID="Cargando1" runat="server" />

        </ProgressTemplate>
    </asp:UpdateProgress>


</asp:Content>
