﻿Imports VAN.Siniestros.BE
Imports VAN.Siniestros.BL
Imports WSInvoker = VANWebServiceInvoker
Imports System.Drawing

Public Class RegistroReclamo
    Inherits PaginaBase

#Region "Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'ddlBuscarPor.SelectedValue = "ApeNom"
            'txtPar1.Text = "25498747"
            'BuscarCertificado()

            If ValidarEstadoClaveUsuario() Then
                Response.Redirect(Constantes.CHANGE_PSW_PAGE, False)
            End If

            pCambioMedioPago.Visible = False
            pDevolucion.Visible = False
            cargarMotivo()
            cargarEstadoReclamo()
            cargarEntidad()

            'ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "TabEnabled", "TabActive(false);", True)
        End If
    End Sub

    Private Sub cargarEntidad()
        Dim oBLEntidad As New BLEntidad
        Dim IDUsuario As String = User.Identity.Name
        ddlEntidad.DataSource = oBLEntidad.Seleccionar(IDUsuario)
        ddlEntidad.DataTextField = "Descripcion"
        ddlEntidad.DataValueField = "IDEntidad"
        ddlEntidad.DataBind()
    End Sub


    Private Sub cargarMotivo()
        Dim oBL As New BLMontivoReclamo
        ddlMotivo.Items.Add(New ListItem("-Seleccionar-", "0"))
        ddlMotivo.DataSource = oBL.Listar()
        ddlMotivo.DataValueField = "IDMotivoReclamo"
        ddlMotivo.DataTextField = "Nombre"
        ddlMotivo.DataBind()
    End Sub

    Private Sub cargarEstadoReclamo()
        Dim oBL As New BLEstadoReclamo
        ddlEstado.Items.Add(New ListItem("-Seleccionar-", "0"))
        ddlEstado.DataSource = oBL.Listar()
        ddlEstado.DataValueField = "IDEstadoReclamo"
        ddlEstado.DataTextField = "Nombre"
        ddlEstado.DataBind()
    End Sub
#End Region

#Region "TAB Búsqueda del Cliente"
    Protected Sub ddlBuscarPor_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlBuscarPor.SelectedIndexChanged
        Select Case ddlBuscarPor.SelectedValue
            Case "ApeNom"
                lblPar1.Text = "Apellidos:"
                lblPar2.Visible = True
                txtPar2.Visible = True
            Case Else
                lblPar2.Visible = False
                txtPar2.Visible = False
                lblPar1.Text = ddlBuscarPor.SelectedItem.Text & ":"
        End Select
    End Sub

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click
        BuscarCertificado()
    End Sub

    Private Sub BuscarCertificado()
        Dim oBL As New BLCertificado
        Dim IDUsuario As String = User.Identity.Name
        gvListaCertificado.DataSource = oBL.Buscar(CInt(ddlEntidad.SelectedValue), IDUsuario, ddlBuscarPor.SelectedValue, txtPar1.Text.Trim, txtPar2.Text.Trim)
        gvListaCertificado.DataBind()
        gvListaCertificado.SelectedIndex = -1
    End Sub

    Protected Sub gvListaCertificado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListaCertificado.PageIndexChanging
        gvListaCertificado.PageIndex = e.NewPageIndex
        BuscarCertificado()
    End Sub

    Protected Sub gvListaCertificado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gvListaCertificado.SelectedIndexChanged
        ResetPanelDevolucion()
        ResetPanelMedioPago()
        ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "TabActRS", "ActTabRegistroSiniestro();", True)
        ' ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "TabActRS", "$get('" & btnActRegSiniestro.ClientID & "').click();", True)
    End Sub

#End Region

#Region "TAB Registro Reclamo"
    Protected Sub btnActRegReclamo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnActRegReclamo.Click
        LimpiarControles()
        'EnabledControles(True)

        Dim oBL As New BLCertificado
        Dim oBE As BECertificado = oBL.Seleccionar(gvListaCertificado.SelectedDataKey.Item(0).ToString)

        Session(Constantes.SS_APP_BECER) = oBE
        txtFechaAtencion.Text = Date.Now().ToString("dd/MM/yyyy")
        txtFechaReclamo.Text = Date.Now().ToString("dd/MM/yyyy")
        hfIDCertificado.Value = oBE.IdCertificado
        hfIDProducto.Value = oBE.IdProducto
        hfIDTipoDocumento.Value = oBE.IdTipoDocumento
        lblNroCertificado.Text = oBE.IdCertificado
        lblProducto.Text = oBE.Producto
        lblCanal.Text = oBE.Oficina
        lblTipoDocumento.Text = oBE.TipoDocumento
        lblNroDocumento.Text = oBE.CcCliente
        lblAsegurado.Text = oBE.Nombre1 & " " & oBE.Nombre2 & " " & oBE.Apellido1 & " " & oBE.Apellido2
        lblNroCerBanco.Text = oBE.NumCertificadoBanco
        lblNroCobro.Text = oBE.NroCobro
        txtAseDireccion.Text = oBE.Direccion
        txtAseTelefono.Text = oBE.Telefono
        txtAseEmail.Text = oBE.Email

        txtCBUActual.Text = oBE.CBU

        lblNroSiniestro.Text = oBE.NroSiniestro
        lblEstadoCer.Text = oBE.Estado
        If (oBE.DigitacionAnulacion.Trim() <> "" And oBE.DigitacionAnulacion.Trim() <> "01/01/1900") Then
            lblFechaAnulacion.Text = oBE.DigitacionAnulacion
        End If
        lblMonedaCer.Text = oBE.IdMonedaPrima
        'txtIDMoneda.Text = oBE.IdMonedaPrima
        hfSimboloMoneda.Value = oBE.SimboloMonedaPrima
        If oBE.NroImagen = 0 Then
            lblNroImagen.Text = "NO"
            lblNroImagen.ForeColor = Drawing.Color.Red
        Else
            lblNroImagen.ForeColor = Drawing.Color.Green
            lblNroImagen.Text = "SI"
        End If

        If oBE.NroSiniestro = 0 Then
            lblNroSiniestro.Text = "NO"
        Else
            lblNroSiniestro.Text = "SI"
        End If

        lblAseguradora.Text = oBE.Aseguradora
        EnabledControles(True)
    End Sub

    Protected Sub btnGrabar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGrabar.Click

        If Not String.IsNullOrWhiteSpace(hfIDTipoMotivoReclamo.Value) AndAlso hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_CMP AndAlso (String.IsNullOrEmpty(txtNumero.Text) Or ddlMedioPago.SelectedValue = "0") Then
            ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "MsgError", "alert('Debe seleccionar el nuevo Medio de Pago');", True)
            Exit Sub

        Else
            If Not String.IsNullOrWhiteSpace(hfIDTipoMotivoReclamo.Value) AndAlso hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_DEV AndAlso (String.IsNullOrEmpty(txtNumeroDev.Text) Or ddlMedioPagoDev.SelectedValue = "0") Then
                ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "MsgError", "alert('Debe selecciona el Medio de Pago y Nro de Cuenta para la devolución');", True)
                Exit Sub
            End If
        End If

        'SOLO en caso de Endoso se verifica si el medio de pago seleccionado se encuentra habilitado para el producto ya que el cambio afecta al Certificado
        If Not ValidaMedioPago(hfIDTipoMotivoReclamo.Value) Then
            ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "MsgError", "alert('No es posible actualizar el Reclamo debido a que el Medio de Pago seleccionado no se encuentra habilitado para este producto');", True)
            Exit Sub
        End If

        Try
            Dim oBL As New BLReclamo
            Dim oBE As New BEReclamo

            oBE.IDCertificado = hfIDCertificado.Value
            oBE.FechaReclamo = CDate(txtFechaReclamo.Text)
            oBE.NroReclamo = txtNroReclamo.Text
            If txtFechaAtencion.Text.Trim() = "" Then
                oBE.FechaAtencion = CDate("01/01/1900")
            Else
                oBE.FechaAtencion = CDate(txtFechaAtencion.Text)
            End If
            If txtFechaFinAtencion.Text.Trim() = "" Then
                oBE.FechaFinAtencion = CDate("01/01/1900")
            Else
                oBE.FechaFinAtencion = CDate(txtFechaFinAtencion.Text)
            End If

            oBE.GenerarDevolucion = chkCtaExterna.Checked
            oBE.Imagen = IIf(lblNroImagen.Text = "SI", True, False)
            oBE.Siniestro = IIf(lblNroSiniestro.Text = "SI", True, False)
            oBE.NroCobro = CInt(lblNroCobro.Text)

            oBE.AseDireccion = txtAseDireccion.Text
            oBE.AseEmail = txtAseEmail.Text
            oBE.AseTelefono = txtAseTelefono.Text

            oBE.IDEstadoReclamo = ddlEstado.SelectedValue
            oBE.IDMotivoReclamo = ddlMotivo.SelectedValue
            oBE.MedioDevolucion = String.Empty
            oBE.GenerarDevolucion = False
            oBE.CuentaExterna = chkCtaExterna.Checked
            If hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_DEV Or hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_CMP Then
                oBE.RAIZ = hfRaiz.Value
                If hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_CMP Then
                    oBE.MedioDevolucion = txtNumero.Text
                    oBE.CBU = txtCBU.Text
                    oBE.IDMedioPago = ddlMedioPago.SelectedValue
                Else
                    If hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_DEV Then
                        oBE.MedioDevolucion = txtNumeroDev.Text
                        oBE.CBU = txtCBUDev.Text
                        oBE.IDMedioPago = ddlMedioPagoDev.SelectedValue
                    End If
                End If
            End If
            oBE.Observacion = txtObservacion.Text
            oBE.UsuarioCreacion = User.Identity.Name
            oBE.ObservacionAsegurado = txtObservacionAseg.Text
            oBE.CuentaExterna = chkCtaExterna.Checked

            Dim pIDReclamo As Int32 = oBL.Insertar(oBE)
            If pIDReclamo <> 0 Then
                txtNroTicket.Text = Date.Now().ToString("yyyyMMdd") & "-" & pIDReclamo.ToString("000000")
                EnabledControles(False)

                btnImpresionNota.Enabled = True
                ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "MsgError", "alert('Reclamo registrado con éxito con el N° de Ticket: " & txtNroTicket.Text & "');", True)
            Else
                ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "MsgError", "alert('Ocurrió un error al grabar. Consulte con el Administrador del Sistema');", True)
            End If
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "MsgError", "alert('Ocurrió un error al grabar. Consulte con el Administrador del Sistema');", True)
        End Try
        'ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "TabActRS", "alert('" & txtAseDireccion.Text & ");", True)
    End Sub

    Private Sub LimpiarControles()
        Session(Constantes.SS_APP_BECER) = Nothing
        hfIDProducto.Value = 0
        hfIDCertificado.Value = 0
        hfIDTipoMotivoReclamo.Value = String.Empty
        hfIDTipoDocumento.Value = String.Empty
        lblNroCertificado.Text = String.Empty
        lblProducto.Text = String.Empty
        lblCanal.Text = String.Empty
        lblTipoDocumento.Text = String.Empty
        lblNroDocumento.Text = String.Empty
        lblAsegurado.Text = String.Empty
        lblCanal.Text = String.Empty
        lblAseguradora.Text = String.Empty
        lblNroImagen.Text = String.Empty
        lblNroCerBanco.Text = String.Empty
        lblNroCobro.Text = String.Empty
        txtFechaReclamo.Text = String.Empty
        txtAseDireccion.Text = String.Empty
        txtAseEmail.Text = String.Empty
        txtAseTelefono.Text = String.Empty
        txtCBU.Text = String.Empty
        txtFechaFinAtencion.Text = String.Empty
        txtNroTicket.Text = "-"
        ddlEstado.SelectedValue = "0"
        ddlMotivo.SelectedValue = "0"
        'txtFechaExtorno.Text = String.Empty
        'txtOperacionExtorno.Text = String.Empty
        txtObservacion.Text = String.Empty
        txtNroReclamo.Text = String.Empty
        txtFechaVigInicio.Text = String.Empty
        txtFechaVigFin.Text = String.Empty
        'txtMedioPago.Text = String.Empty
        txtNumero.Text = String.Empty
        txtIDMoneda.Text = String.Empty
        pCambioMedioPago.Visible = False
        pDevolucion.Visible = False
        lblNroSiniestro.Text = String.Empty
        lblEstadoCer.Text = String.Empty
        lblFechaAnulacion.Text = String.Empty
        txtObservacionAseg.Text = String.Empty
        pResumen.Visible = False
        btnImpresionNota.Enabled = False
    End Sub

    Private Sub EnabledControles(ByVal pEstado As Boolean)
        txtFechaReclamo.Enabled = pEstado
        txtNroReclamo.Enabled = pEstado
        txtAseDireccion.Enabled = pEstado
        txtAseEmail.Enabled = pEstado
        txtAseTelefono.Enabled = pEstado
        txtCBU.Enabled = pEstado
        txtFechaFinAtencion.Enabled = pEstado

        ddlEstado.Enabled = pEstado
        ddlMotivo.Enabled = pEstado
        'txtFechaExtorno.Enabled = pEstado
        txtFechaAtencion.Enabled = pEstado
        'txtOperacionExtorno.Enabled = pEstado
        txtObservacion.Enabled = pEstado
        txtObservacionAseg.Enabled = pEstado
        chkCtaExterna.Enabled = pEstado
        btnGrabar.Visible = pEstado
        btnNuevo.Visible = Not pEstado
        btnImpresionNota.Visible = Not pEstado
        iBtnVerCobroSel.Enabled = pEstado
        iBtnVerCobroSelDev.Enabled = pEstado
        'btnNuevo.Visible = pEstado

    End Sub

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNuevo.Click
        LimpiarControles()
        EnabledControles(True)
        ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "TabActBS", "ActTabBusquedaCliente();", True)
    End Sub

    Protected Sub iBtnVerCobro_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles iBtnVerCobroSel.Click, iBtnVerCobroSelDev.Click
        'LlenarCobrosGrid()
        'ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "verCobroSel", "verCobroSel();", True)
        ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "MedioPago", "BuscarMedioPago();", True)
        LlenarMedioPagoGrid()
    End Sub

    Private Sub LlenarMedioPagoGrid()
        Try
            Dim IDPRODUCTO As Int32 = CInt(hfIDProducto.Value)
            Dim SrvInvoker As New WSInvoker.WebServiceInvoker()
            Dim ClientITAU As New WSInvoker.Model.Cliente()
            Dim oBLEquivalencia As New BLListaEquivalencia

            Dim NroDocumento As String
            Dim IDTipoDocumento As String '= hfIDTipoDocumento.Value
            Dim oBECer As New BECertificado

            oBECer = DirectCast(Session(Constantes.SS_APP_BECER), BECertificado)
            IDTipoDocumento = IIf(IsMock(), TipoDocumentoMockDefault(), oBLEquivalencia.getValorEquivalencia(CInt(IDPRODUCTO), "TipoDocumento", oBECer.IdTipoDocumento).ValorEquivalencia)
            NroDocumento = IIf(IsMock(), NroDocumentoMockDefault(), lblNroDocumento.Text.Trim)
            'ClientITAU = SrvInvoker.GetFullClient(IDTipoDocumento, NroDocumento, IIf(hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_DEV, 0, IDPRODUCTO))
            ClientITAU = SrvInvoker.GetFullClient(IDTipoDocumento, NroDocumento, 0)
            If ClientITAU.EstadoRespuesta = WSInvoker.Constant.Constant.RESPONSE_OK Then
                If ClientITAU.Codigo <> String.Empty Then
                    gvCuentaTarjeta.DataSource = ClientITAU.MediosDePago
                Else
                    gvCuentaTarjeta.DataSource = New List(Of WSInvoker.Model.MedioPago)
                End If
                gvCuentaTarjeta.DataBind()
                gvCuentaTarjeta.SelectedIndex = -1
            Else
                gvCuentaTarjeta.DataSource = New List(Of WSInvoker.Model.MedioPago)
                gvCuentaTarjeta.DataBind()
                gvCuentaTarjeta.SelectedIndex = -1
                ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('No se encontró ningún cliente para el tipo y número de documento ingresado.');", True)
            End If
        Catch ex As Exception
            gvCuentaTarjeta.DataSource = New List(Of WSInvoker.Model.MedioPago)
            gvCuentaTarjeta.DataBind()
            gvCuentaTarjeta.SelectedIndex = -1
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('Error de inconsistencia de datos.');", True)
        End Try
    End Sub

    Protected Sub BCbtnAceptar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BCbtnAceptar.Click
        If gvCuentaTarjeta.SelectedIndex = -1 Then
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('No ha seleccionado el medio de pago');", True)
            Exit Sub
        End If
        Try
            If hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_CMP Then
                hfRaiz.Value = DirectCast(gvCuentaTarjeta.SelectedRow.Cells(5).Controls(1), Label).Text
                ddlMedioPago.SelectedValue = DirectCast(gvCuentaTarjeta.SelectedRow.Cells(1).Controls(1), Label).Text
                txtNumero.Text = DirectCast(gvCuentaTarjeta.SelectedRow.Cells(4).Controls(1), Label).Text
                txtCBU.Text = DirectCast(gvCuentaTarjeta.SelectedRow.Cells(6).Controls(1), Label).Text
                ChangeColorControlMP(txtNumero, True)
                ChangeColorControlMP(txtCBU, True)
                ChangeColorControlMP(ddlMedioPago, True)
            Else
                If hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_DEV Then
                    hfRaiz.Value = DirectCast(gvCuentaTarjeta.SelectedRow.Cells(5).Controls(1), Label).Text
                    ddlMedioPagoDev.SelectedValue = DirectCast(gvCuentaTarjeta.SelectedRow.Cells(1).Controls(1), Label).Text
                    txtNumeroDev.Text = DirectCast(gvCuentaTarjeta.SelectedRow.Cells(4).Controls(1), Label).Text
                    txtCBUDev.Text = DirectCast(gvCuentaTarjeta.SelectedRow.Cells(6).Controls(1), Label).Text
                End If
            End If
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('No se pudo seleccionar el medio de pago.');", True)
        End Try
        ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "AC", "$.modal.close();", True)
    End Sub

    Private Sub LlenarCobrosGrid()
        Dim oBL As New BLCobro
        gvCobro.DataSource = oBL.Seleccionar(hfIDCertificado.Value, 0)
        gvCobro.DataBind()
    End Sub

    Protected Sub iBtnVerImgCertificado_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles iBtnVerImgCertificado.Click
        ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "verImg", "verImagenCer('" & hfIDCertificado.Value & "');", True)
    End Sub

    Protected Sub iBtnVerCobro_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles iBtnVerCobro.Click
        ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "verCobro", "verCobro('" & hfIDCertificado.Value & "');", True)
    End Sub

    Protected Sub iBtnVerSiniestro_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles iBtnVerSiniestro.Click
        ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "verSiniestro", "verSiniestro('" & hfIDCertificado.Value & "');", True)
    End Sub

    Protected Sub btnSelCobroAceptar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSelCobroAceptar.Click
        'Dim i As Integer
        'Dim c As Int32 = 0
        'Dim MontoTotal As Double = 0

        'Dim IndiceIniPer As Int32 = 0
        'Dim IndiceFinPer As Int32 = 0
        'For i = 0 To gvCobro.Rows.Count - 1
        '    If DirectCast(gvCobro.Rows.Item(i).Cells.Item(0).Controls.Item(1), CheckBox).Checked Then
        '        MontoTotal = CDbl(DirectCast(gvCobro.Rows.Item(i).Cells.Item(1).Controls.Item(1), Label).Text) + MontoTotal
        '        c = 1 + c
        '        If c = 1 Then
        '            IndiceFinPer = i
        '        End If
        '        IndiceIniPer = i
        '    End If
        'Next i

        'If c <> 0 Then
        '    txtFechaVigInicio.Text = DirectCast(gvCobro.Rows.Item(IndiceIniPer).Cells.Item(2).Controls.Item(1), Label).Text
        '    txtFechaVigFin.Text = DirectCast(gvCobro.Rows.Item(IndiceFinPer).Cells.Item(2).Controls.Item(1), Label).Text
        'End If
        'txtMedioPago.Text = c
        'txtNumero.Text = MontoTotal.ToString("0.00")
    End Sub

    Protected Sub gvCobro_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCobro.PageIndexChanging
        gvCobro.PageIndex = e.NewPageIndex
        LlenarCobrosGrid()
    End Sub

#End Region

    Protected Sub btnImpresionNota_Click(sender As Object, e As EventArgs) Handles btnImpresionNota.Click
        Dim ID As String = txtNroTicket.Text
        ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M2", "Imprimir('" & ID & "','N');", True)
    End Sub

    Protected Sub ddlMotivo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlMotivo.SelectedIndexChanged

        Try
            If ddlMotivo.SelectedValue <> "0" Then
                SelTipoMotivoReclamo(CInt(ddlMotivo.SelectedValue))
            Else
                pCambioMedioPago.Visible = False
                pDevolucion.Visible = False
            End If
        Catch ex As Exception
            Dim s As String = ex.Message
        End Try

    End Sub

    Private Sub SelTipoMotivoReclamo(ByVal IDMotivoReclamo As Integer)
        Dim lstMedioPago As New ArrayList
        Dim oBLMReclamo As New BLMontivoReclamo

        Try
            hfIDTipoMotivoReclamo.Value = oBLMReclamo.GetTipoMotivoReclamo(ddlMotivo.SelectedValue)
            If Not String.IsNullOrWhiteSpace(hfIDTipoMotivoReclamo.Value) And hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_CMP Then
                pCambioMedioPago.Visible = True
                ResetPanelDevolucion()
                ControlDevoluciones(False)
                lstMedioPago = ListaGenerica(CInt(hfIDProducto.Value), "MedioPago")
                ddlMedioPago.Items.Clear()
                ddlMedioPago.Items.Add(New ListItem("-Seleccionar-", "0"))
                ddlMedioPago.DataSource = lstMedioPago
                ddlMedioPago.DataValueField = "IDMedioPago"
                ddlMedioPago.DataTextField = "Nombre"
                ddlMedioPago.DataBind()
                ddlMedioPago.SelectedValue = "0"

                ddlMedioPagoActual.Items.Clear()
                ddlMedioPagoActual.DataSource = lstMedioPago
                ddlMedioPagoActual.DataValueField = "IDMedioPago"
                ddlMedioPagoActual.DataTextField = "Nombre"
                ddlMedioPagoActual.DataBind()
                CargarDatosCuenta(txtNumeroActual, txtCBUActual, ddlMedioPagoActual)
                ChangeColorControlMP(txtNumero, False)
                ChangeColorControlMP(txtCBU, False)
                ChangeColorControlMP(ddlMedioPago, False)
            Else
                If Not String.IsNullOrWhiteSpace(hfIDTipoMotivoReclamo.Value) And hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_DEV Then
                    lstMedioPago = ListaGenerica(CInt(hfIDProducto.Value), "MedioPago")
                    ddlMedioPagoDev.Items.Clear()
                    ddlMedioPagoDev.Items.Add(New ListItem("-Seleccionar-", "0"))
                    ddlMedioPagoDev.DataSource = lstMedioPago
                    ddlMedioPagoDev.DataValueField = "IDMedioPago"
                    ddlMedioPagoDev.DataTextField = "Nombre"
                    ddlMedioPagoDev.DataBind()
                    pDevolucion.Visible = True
                    ResetPanelMedioPago()
                    ControlDevoluciones(True)
                    CargarDatosCuenta(txtNumeroDev, txtCBUDev, ddlMedioPagoDev)
                Else
                    pDevolucion.Visible = False
                    pCambioMedioPago.Visible = False
                    ResetPanelDevolucion()
                    ResetPanelMedioPago()
                End If
            End If
        Catch ex As Exception
            Dim s As String = ex.Message
        End Try
    End Sub

    Private Function ValidaMedioPago(ByVal codTipoMotivo As String)
        'Esta validacion se realiza ya que si es un Endoso el cambio afecta al Certificado y en consultas
        ' solo se muestran los medios de pago habilitados que tiene el producto.
        Dim result As Boolean = False

        If hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_CMP Then
            Dim oBL As New BLListaGenerica
            Dim lst As New ArrayList
            lst = oBL.Seleccionar(CInt(hfIDProducto.Value), "MedioPago")

            For Each oBEMedios As BEListaGenerica In lst
                If oBEMedios.ID = ddlMedioPago.SelectedValue Then
                    result = True
                    Exit For
                End If
            Next
        Else
            result = True
        End If
        Return result
    End Function

    Private Sub ResetPanelMedioPago()
        pCambioMedioPago.Visible = False
        txtCBU.Text = String.Empty
        txtNumero.Text = String.Empty
    End Sub

    Private Sub ResetPanelDevolucion()
        pDevolucion.Visible = False
        chkCtaExterna.Checked = False
        txtNumeroDev.Text = String.Empty
    End Sub

    Private Function ListaGenerica(ByVal pIDProducto As Integer, ByVal pTabla As String) As ArrayList
        'Dim oBL As New BLListaGenerica
        'Dim r As New ArrayList
        'Return oBL.Seleccionar(pIDProducto, pTabla)
        Dim oBL As New BLMedioPago

        Return oBL.SeleccionarPermitidos()
    End Function

    Protected Sub chkCtaExterna_CheckedChanged(sender As Object, e As EventArgs) Handles chkCtaExterna.CheckedChanged
        If chkCtaExterna.Checked Then
            'Selecciona la cuenta manualmente
            ControlDevoluciones(True)
            CuentaDevolucion()
            If (Not ddlMedioPagoDev.Items.FindByValue(Constantes.ID_CUENTA_CORRIENTE) Is Nothing) Then
                ddlMedioPagoDev.SelectedValue = Constantes.ID_CUENTA_CORRIENTE
            End If
        Else
            'Selecciona la cuenta desde los servicios de ITAU
            ControlDevoluciones(False)
            CargarDatosCuenta(txtNumeroDev, txtCBUDev, ddlMedioPagoDev)
        End If
    End Sub

    Private Sub CargarDatosCuenta(ByVal txtNro As TextBox, ByVal txtCBU As TextBox, ByVal cboMP As DropDownList)
        Dim oBECer As New BECertificado

        If Not chkCtaExterna.Checked And Not hfIDTipoMotivoReclamo.Value = Constantes.ID_TIPO_MOTIVO_DEV Then
            oBECer = DirectCast(Session(Constantes.SS_APP_BECER), BECertificado)
            txtNro.Text = oBECer.NumeroCuenta
            txtNro.Enabled = False
            txtCBU.Text = oBECer.CBU
            txtCBU.Enabled = False

            If (Not cboMP.Items.FindByValue(oBECer.IdMedioPago.ToString.ToUpper()) Is Nothing) Then
                cboMP.SelectedValue = oBECer.IdMedioPago
            Else
                cboMP.SelectedValue = "0"
            End If
        Else
            txtNro.Text = String.Empty
            txtCBU.Text = String.Empty
            If (Not cboMP.Items.FindByValue("0") Is Nothing) Then
                cboMP.SelectedValue = "0"
            End If
        End If
    End Sub

    Private Sub ControlDevoluciones(ByVal estado As Boolean)

        txtNumeroDev.Enabled = estado
        txtNumeroDev.ReadOnly = Not estado

        If chkCtaExterna.Checked Then
            txtCBUDev.Visible = False
            lb_cbuDev.Visible = False
            iBtnVerCobroSelDev.Visible = False
            ChangeColorControlMP(txtNumeroDev, False)
            ChangeColorControlMP(ddlMedioPagoDev, False)
        Else
            txtCBUDev.Visible = True
            lb_cbuDev.Visible = True
            iBtnVerCobroSelDev.Visible = True
            ChangeColorControlMP(txtNumeroDev, True)
            ChangeColorControlMP(ddlMedioPagoDev, True)
        End If
        txtCBUDev.Enabled = estado
        txtCBUDev.ReadOnly = Not estado
        ddlMedioPagoDev.Enabled = Not iBtnVerCobroSelDev.Visible
        'iBtnVerCobroSelDev.ImageUrl = IIf(estado, "~/Images/Icon/ver_disabled.png", "~/Images/Icon/ver.png")
    End Sub

    Protected Sub ddlMedioPagoDev_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlMedioPagoDev.SelectedIndexChanged
        If chkCtaExterna.Checked Then
            CuentaDevolucion()
        End If
    End Sub

    Private Sub CuentaDevolucion()
        Try
            txtNumeroDev.Enabled = True
            txtNumeroDev.ReadOnly = False
            txtNumeroDev.Text = String.Empty

            txtCBUDev.Enabled = False
            txtCBUDev.ReadOnly = True
            txtCBUDev.Text = String.Empty
        Catch ex As Exception
            Dim s As String = ex.Message
        End Try
    End Sub
End Class