﻿Imports VAN.Siniestros.BL
Public Class pSiniestroReclamo
    Inherits PaginaBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            If ValidarEstadoClaveUsuario() Then
                Response.Redirect(Constantes.CHANGE_PSW_PAGE, False)
            End If

            hfIDCertificado.Value = Request.QueryString("ID")
            cargarSiniestro()
        End If
    End Sub
    Private Sub cargarSiniestro()
        Dim oBL As New BLSiniestro
        gvSiniestro.DataSource = oBL.Seleccionar(hfIDCertificado.Value)
        gvSiniestro.DataBind()
    End Sub
End Class