﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ReclamoConsulta.aspx.vb" Inherits="VANSiniestros.ReclamoConsulta" EnableEventValidation="false" culture="es-PE" uiCulture="es-PE"%>
<%@ Register src="../Controles/Cargando.ascx" tagname="Cargando" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/jquery-1.7.1.min.js" type="text/javascript"></script>  
<script src="../Scripts/jquery.simplemodal.1.4.2.min.js" type="text/javascript"></script>  

    <script type="text/javascript">

        function verImagenCer(pID) {
            var src = "pImagenReclamo.aspx?ID=" + pID;
            $.modal('<iframe src="' + src + '" height="520" width="895" style="border:0" frameborder=0>', {
                closeHTML: "<a href='#' title='Cerrar' class='modal-close'><img src='../Images/cerrar.gif' alt='Cerrar' width='16' height='16'/></a>",
                containerCss: {
                    backgroundColor: "#FFF",
                    borderColor: "#750000",
                    height: 525,
                    padding: 0,
                    width: 895
                },
                overlayClose: true,
                appendTo: '#aspnetForm',
                persist: false
            });
        }


        function verCobro(pID) {
            var src = "pCobroReclamo.aspx?ID=" + pID;
            $.modal('<iframe src="' + src + '" height="440" width="895" style="border:0" frameborder=0>', {
                closeHTML: "<a href='#' title='Cerrar' class='modal-close'><img src='../Images/cerrar.gif' alt='Cerrar' width='16' height='16'/></a>",
                containerCss: {
                    backgroundColor: "#FFF",
                    borderColor: "#750000",
                    height: 450,
                    padding: 0,
                    width: 895
                },
                overlayClose: true,
                appendTo: '#aspnetForm',
                persist: false
            });
        }


        function verSiniestro(pID) {
            var src = "pSiniestroReclamo.aspx?ID=" + pID;
            $.modal('<iframe src="' + src + '" height="440" width="895" style="border:0" frameborder=0>', {
                closeHTML: "<a href='#' title='Cerrar' class='modal-close'><img src='../Images/cerrar.gif' alt='Cerrar' width='16' height='16'/></a>",
                containerCss: {
                    backgroundColor: "#FFF",
                    borderColor: "#750000",
                    height: 450,
                    padding: 0,
                    width: 895
                },
                overlayClose: true,
                appendTo: '#aspnetForm',
                persist: false
            });
        }



        function verCobroSel() {
            $("#frmCobro").modal({
                closeHTML: "<a href='#' title='Cerrar' class='modal-close'><img src='../Images/cerrar.gif' alt='Cerrar' width='16' height='16'/></a>",
                containerCss: {
                    backgroundColor: "#FFF",
                    borderColor: "#750000",
                    height: 350,
                    padding: 0,
                    width: 920
                },
                overlayClose: false,
                appendTo: '#aspnetForm',
                persist: true
            });
        }



    function ActTabRegistroSiniestro() {
        TabActive(true);
        $get('<%= btnActRegReclamo.ClientID%>').click();
        $find('<%=TabRegReclamo.ClientID%>').set_activeTabIndex(1);

    }

    function ActTabBusquedaCliente() {
        $find('<%=TabRegReclamo.ClientID%>').set_activeTabIndex(0);
    }

    function ActiveTabChanged(sender, e) 
    {
//        var tab=sender.get_activeTab().get_tabIndex();
//        var Bt;
//        if (tab == '1') 
//        {
//            TabActive(false);
        //            Bt = $get('<%= btnActRegReclamo.ClientID%>');
//            Bt.click;
//        }
    }

        
    function vDecimal(evt) {
        // NOTE: Backspace = 8, Enter = 13, '0' = 48, '9' = 57, '.' = 46       
        if (window.event) // IE
        {
            key = evt.keyCode;
        }
        else if (e.which) // Netscape/Firefox/Opera
        {
            key = evt.which;
        }

        return (key <= 13 || (key >= 48 && key <= 57) || key == 46);
    }
    //onkeypress="javascript:return vDecimal(event);"


    function SelectCobro(CheckBox) {
        TotalChkBx = parseInt('<%= gvCobro.Rows.Count %>');
        var TargetBaseControl = document.getElementById('<%= gvCobro.ClientID %>');
        var TargetChildControl = "chkSelCobro";
        var Inputs = TargetBaseControl.getElementsByTagName("input");
        for (var iCount = 0; iCount < Inputs.length; ++iCount) {
            if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0)
                Inputs[iCount].checked = CheckBox.checked;
        }
    }

        function Imprimir(ID, Tipo) {
            var src;
            src = 'Imprimir.aspx?ID=' + ID + '&Tipo=' + Tipo;
            $.modal('<iframe frameborder="0" src=' + src + ' height="582" width="1000" style="border:0">', {
                closeHTML: "<div class='modal-DivclosePrint'> <a href='#' title='Cerrar' class='modal-closePrint'><img src='../Images/cerrarCer.gif' alt='Cerrar' width='55' height='18'/></a></div>",
                containerCss: {
                    backgroundColor: "#FFF",
                    borderColor: "#750000",
                    height: 600,
                    padding: 0,
                    width: 1000
                },
                overlayClose: true,
                appendTo: '#aspnetForm',
                persist: true
            });

        }

        function BuscarModal(pDiv, pWidth, pHeight) {
            $(pDiv).modal({
                closeHTML: "<div class='modal-DivclosePrint'> <a href='#' title='Cerrar' class='modal-close'><img src='../Images/cerrarCer.gif' alt='Cerrar' width='55' height='18'/></a></div>",
                containerCss: {
                    backgroundColor: "#FFF",
                    borderColor: "#750000",
                    height: pHeight,
                    padding: 0,
                    width: pWidth
                },
                overlayClose: false,
                appendTo: '#aspnetForm',
                persist: true
            });
        }


        function BuscarMedioPago() {
            $get('<%= btnLoad.ClientID%>').click();
            BuscarModal("#frmBuscarMedioPago", 452, 280);
        }


    </script>

    </asp:Content>
  
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<asp:TabContainer 
        ID="TabRegReclamo" runat="server" ActiveTabIndex="0" Width="100%">
        <asp:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel1">
            <HeaderTemplate>Búsqueda de Reclamo</HeaderTemplate>
            






<ContentTemplate><table cellpadding="3" cellspacing="0" style="width: 998px"><tr><td><asp:UpdatePanel ID="upFiltroBuscar" runat="server" UpdateMode="Conditional"><ContentTemplate><table class="TablaMarco" style="width: 990px" cellpadding="0" cellspacing="5"><tr><td class="TablaLabelR" style="width: 85px">Entidad:</td><td style="width: 161px"><asp:DropDownList ID="ddlEntidad" runat="server" AppendDataBoundItems="True" 
            AutoPostBack="True" Width="162px"><asp:ListItem Value="0">Todas</asp:ListItem></asp:DropDownList></td><td class="TablaLabelR" style="width: 120px">&#160;</td><td style="width: 161px">&nbsp;</td><td class="TablaLabelR" style="width: 73px">&#160;</td><td style="width: 160px">&nbsp;</td><td>&nbsp;</td></tr><tr><td class="TablaLabelR" style="width: 85px">Buscar por:</td><td style="width: 161px"><asp:DropDownList ID="ddlBuscarPor" runat="server" AutoPostBack="True" 
                ValidationGroup="Buscar" Width="162px"><asp:ListItem Selected="True" Value="NroDoc">N° Documento</asp:ListItem><asp:ListItem Value="ApeNom">Apellidos y Nombres</asp:ListItem><asp:ListItem Value="NroTicket">N° Ticket</asp:ListItem><asp:ListItem Value="NroReclamo">N° Incidente</asp:ListItem><asp:ListItem Value="NroCertificado">N° Certificado BAIS</asp:ListItem><asp:ListItem Value="NroFisico">N° Certificado Aseg.</asp:ListItem><asp:ListItem Value="NroRaiz">N° Raíz</asp:ListItem></asp:DropDownList></td><td class="TablaLabelR" style="width: 88px"><asp:Label ID="lblPar1" runat="server" Text="N° Documento:"></asp:Label></td><td style="width: 161px"><asp:TextBox ID="txtPar1" runat="server" Width="160px"></asp:TextBox></td><td class="TablaLabelR" style="width: 73px"><asp:Label ID="lblPar2" runat="server" Visible="False">Nombres:</asp:Label></td><td style="width: 157px"><asp:TextBox ID="txtPar2" runat="server" Visible="False" Width="160px"></asp:TextBox></td><td><asp:Button ID="btnBuscar" runat="server" Text="Buscar" ValidationGroup="Buscar" Width="60px" /></td></tr></table></ContentTemplate></asp:UpdatePanel></td></tr><tr><td><asp:UpdatePanel ID="upListaCertificado" runat="server" 
                                UpdateMode="Conditional"><ContentTemplate><asp:GridView ID="gvListaReclamo" runat="server" AllowPaging="True" 
                            AutoGenerateColumns="False" DataKeyNames="IDReclamo,IDCertificado" 
                            Width="989px"><Columns><asp:TemplateField HeaderText="N° Ticket"><ItemTemplate><asp:Label ID="lblNroTicket" runat="server" 
                            Text='<%#  Format(Eval("FechaCreacion"),"yyyyMMdd") + "-" + Format(Eval("IDReclamo"),"000000") %>'></asp:Label></ItemTemplate><ItemStyle Width="120px" /></asp:TemplateField><asp:TemplateField HeaderText="F. Atención"><ItemTemplate><asp:Label ID="Label15" runat="server" 
                                Text='<%# Format(Eval("FechaAtencion"),"dd/MM/yyyy") %>' 
                                Visible='<%# IIF(Format(Eval("FechaAtencion"),"dd/MM/yyyy")="01/01/1900",True,False) %>'></asp:Label></ItemTemplate><ItemStyle HorizontalAlign="Center" Width="60px" /></asp:TemplateField><asp:TemplateField HeaderText="IDCertificado"><ItemTemplate><asp:Label ID="Label20" 
                            runat="server" Text='<%# Eval("Apellido1") &  " " & Eval("Nombre1") %>'></asp:Label></ItemTemplate><ItemStyle Width="180px" /><ItemTemplate><asp:Label ID="Label14" runat="server" 
                                Text='<%# Format(Eval("IDReclamo"),"000000") %>'></asp:Label></ItemTemplate><ItemStyle HorizontalAlign="Center" Width="70px" /><ItemTemplate><asp:Label ID="Label16" runat="server" Text='<%# Bind("IdCertificado") %>'></asp:Label></ItemTemplate><ItemStyle HorizontalAlign="Center" Width="120px" /></asp:TemplateField><asp:BoundField DataField="IDProducto" HeaderText="IDProducto" 
                                    Visible="False" /><asp:TemplateField HeaderText="Producto"><ItemTemplate><asp:Label ID="Label17" runat="server" Text='<%# Bind("Producto") %>'></asp:Label></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="Tipo Documento"><ItemTemplate><asp:Label ID="Label18" runat="server" Text='<%# Bind("TipoDocumento") %>'></asp:Label></ItemTemplate><ItemStyle Width="90px" HorizontalAlign="Center" /></asp:TemplateField><asp:TemplateField HeaderText="N° Documento"><ItemTemplate><asp:Label ID="Label19" runat="server" Text='<%# Bind("CcCliente") %>'></asp:Label></ItemTemplate><ItemStyle Width="90px" HorizontalAlign="Center" /></asp:TemplateField><asp:TemplateField HeaderText="Apellidos y Nombres"><ItemTemplate><asp:Label ID="Label20" 
                            runat="server" Text='<%# Eval("Apellido1") &  " " & Eval("Nombre1") %>'></asp:Label></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="Estado"><ItemTemplate><asp:Label ID="Label21" runat="server" 
                                            Text='<%# Bind("EstadoReclamo") %>'></asp:Label></ItemTemplate><ItemStyle Width="70px" HorizontalAlign="Center" /></asp:TemplateField><asp:CommandField ButtonType="Image" SelectImageUrl="~/img/seleccionar.gif" 
                                    ShowSelectButton="True"><ItemStyle HorizontalAlign="Center" Width="30px" /></asp:CommandField></Columns><EmptyDataTemplate>No se encontron registros</EmptyDataTemplate></asp:GridView></ContentTemplate><Triggers><asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" /></Triggers></asp:UpdatePanel></td></tr></table></ContentTemplate>
        






</asp:TabPanel>
        <asp:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
            <HeaderTemplate>Datos del Reclamo</HeaderTemplate>
            






<ContentTemplate><asp:UpdatePanel ID="upRSiniestro" runat="server" UpdateMode="Conditional"><ContentTemplate><table cellpadding="0" cellspacing="5" style="width: 100%"><tr><td><table class="TablaMarco" style="width: 100%"><tr><td class="TablaTitulo">Datos del Certificado <asp:HiddenField ID="hfIDCertificado" runat="server" /><asp:HiddenField ID="hfIDTipoDocumento" runat="server" /><asp:HiddenField ID="hfIDProducto" runat="server" /><asp:HiddenField ID="hfIDTipoMotivoReclamo" runat="server" /></td></tr><tr><td><table style="width: 977px"><tr><td style="width: 119px">N° Certificado</td><td style="width: 142px">Producto</td><td style="width: 211px">&#160;</td><td style="width: 245px">Canal</td><td style="width: 90px">Aseguradora</td><td>&#160;</td></tr><tr><td style="width: 119px" class="TablaCeldaBox"><asp:Label ID="lblNroCertificado" runat="server"></asp:Label></td><td class="TablaCeldaBox" colspan="2"><asp:Label ID="lblProducto" runat="server"></asp:Label>&#160;</td><td style="width: 245px" class="TablaCeldaBox"><asp:Label ID="lblCanal" runat="server"></asp:Label></td><td class="TablaCeldaBox" colspan="2"><asp:Label ID="lblAseguradora" runat="server"></asp:Label></td></tr><tr><td style="width: 119px">Tipo Documento</td><td style="width: 142px">N° Documento</td><td style="width: 211px">Asegurado</td><td style="width: 245px">&#160;</td><td colspan="2">N° Certificado Aseguradora</td></tr><tr><td style="width: 119px" class="TablaCeldaBox"><asp:Label ID="lblTipoDocumento" runat="server"></asp:Label></td><td style="width: 142px" class="TablaCeldaBox"><asp:Label ID="lblNroDocumento" runat="server"></asp:Label></td><td colspan="2" class="TablaCeldaBox"><asp:Label ID="lblAsegurado" runat="server"></asp:Label>&#160;</td><td class="TablaCeldaBox" colspan="2"><asp:Label ID="lblNroCerBanco" runat="server"></asp:Label></td></tr><tr><td style="width: 119px">&#160;Imagen <asp:ImageButton ID="iBtnVerImgCertificado" runat="server" 
                                                    ImageUrl="~/Images/Icon/ver.gif" /></td><td style="width: 142px">N° Cuotas <asp:ImageButton ID="iBtnVerCobro" runat="server" 
            ImageUrl="~/Images/Icon/ver.gif" /></td><td colspan="0">Siniestros <asp:ImageButton ID="iBtnVerSiniestro" runat="server" 
            ImageUrl="~/Images/Icon/ver.gif" Width="16px" /></td><td>Fecha Cancelación</td><td style="width: 90px">Moneda Prima</td><td>Estado</td></tr><tr><td class="TablaCeldaBox" style="width: 119px"><asp:Label ID="lblNroImagen" runat="server"></asp:Label></td><td class="TablaCeldaBox" style="width: 142px"><asp:Label ID="lblNroCobro" runat="server"></asp:Label></td><td colspan="0" class="TablaCeldaBox"><asp:Label ID="lblNroSiniestro" runat="server"></asp:Label></td><td class="TablaCeldaBox"><asp:Label ID="lblFechaAnulacion" runat="server"></asp:Label>&#160;</td><td class="TablaCeldaBox" style="width: 103px"><asp:Label ID="lblMonedaCer" runat="server"></asp:Label></td><td class="TablaCeldaBox"><asp:Label ID="lblEstadoCer" runat="server"></asp:Label></td></tr><tr><td style="width: 119px">N° Teléfono</td><td style="width: 142px">Dirección </td><td colspan="0">&#160;</td><td class="T">&#160;</td><td style="width: 103px">E-mail<asp:RegularExpressionValidator ID="RegularExpressionValidator1" 
                runat="server" ControlToValidate="txtAseEmail" 
                ErrorMessage="Ingrese un E-mail válido" ForeColor="Red" 
                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                ValidationGroup="grabar">*</asp:RegularExpressionValidator></td><td>&#160;</td></tr><tr><td style="width: 119px"><asp:TextBox ID="txtAseTelefono" runat="server" Width="125px"></asp:TextBox></td><td colspan="3"><asp:TextBox ID="txtAseDireccion" runat="server" Width="500px"></asp:TextBox></td><td colspan="2"><asp:TextBox ID="txtAseEmail" runat="server" Width="300px"></asp:TextBox></td></tr></table></td></tr></table></td></tr><tr><td><table class="TablaMarco" style="width: 100%"><tr><td class="TablaTitulo">Datos del Reclamo</td></tr><tr><td><table cellpadding="0" cellspacing="5" style="width: 974px"><tr><td style="width: 97px; font-weight: bold;">N° Ticket<asp:HiddenField 
            ID="hfIDReclamo" runat="server" Value="0" /></td><td style="width: 84px">N° Incidente</td><td style="width: 90px">Fecha Reclamo<asp:RequiredFieldValidator 
                        ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="txtFechaReclamo" 
                        ErrorMessage="Ingrese la Fecha de Reclamo" ForeColor="Red" 
                        ValidationGroup="grabar" Display="Dynamic">*</asp:RequiredFieldValidator><asp:CompareValidator ID="CompareValidator1" 
            ControlToValidate="txtFechaReclamo" runat="server" ErrorMessage="Ingrese una Fecha de Reclamo válida"
                                Operator="DataTypeCheck" Type="Date" ForeColor="Red" 
            ValidationGroup="grabar" Display="Dynamic">*</asp:CompareValidator><asp:MaskedEditExtender ID="me" runat="server" ClearTextOnInvalid="True" 
                        Mask="99/99/9999" MaskType="Date" 
        TargetControlID="txtFechaReclamo"></asp:MaskedEditExtender><asp:RangeValidator ID ="rvFechaReclamo" runat ="server" 
            ControlToValidate="txtFechaReclamo" 
            ErrorMessage="Ingrese una fecha de reclamo válida" Type="Date"  ValidationGroup="grabar"
            MinimumValue="01/01/1900" MaximumValue="01/01/5000" Display="None" 
            ForeColor="Red">*</asp:RangeValidator></td><td style="width: 91px">Fecha Atención <asp:RangeValidator ID="rvFechaAtencion" runat="server" 
                ControlToValidate="txtFechaAtencion" Display="None" 
                ErrorMessage="Ingrese una Fecha de Atención válida" ForeColor="Red" 
                MaximumValue="01/01/5000" MinimumValue="01/01/1900" Type="Date">*</asp:RangeValidator></td><td style="width: 93px">Fecha Cierre<asp:CompareValidator 
                ID="CompareValidator5" runat="server" ControlToValidate="txtFechaFinAtencion" 
                Display="Dynamic" ErrorMessage="Ingrese una Fecha de Cierre válida" 
                ForeColor="Red" Operator="DataTypeCheck" Type="Date" ValidationGroup="grabar">*</asp:CompareValidator><asp:RangeValidator ID="rvFechaFinAtencion" runat="server" 
                ControlToValidate="txtFechaFinAtencion" Display="None" 
                ErrorMessage="Ingrese una Fecha de Cierre válida" ForeColor="Red" 
                MaximumValue="01/01/5000" MinimumValue="01/01/1900" Type="Date">*</asp:RangeValidator></td><td style="width: 93px">Estado<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                ControlToValidate="ddlEstado" ErrorMessage="Seleccione el Estado" 
                ForeColor="Red" InitialValue="0" ValidationGroup="grabar">*</asp:RequiredFieldValidator></td><td>Motivo Reclamo<asp:RequiredFieldValidator ID="RequiredFieldValidator4" 
            runat="server" ControlToValidate="ddlMotivo" 
            ErrorMessage="Seleccione el Motivo" ForeColor="Red" InitialValue="0" 
            ValidationGroup="grabar">*</asp:RequiredFieldValidator></td></tr><tr><td style="width: 97px"><asp:TextBox ID="txtNroTicket" runat="server" Font-Bold="True" ReadOnly="True" Width="95px">-</asp:TextBox></td><td style="width: 84px"><asp:TextBox ID="txtNroReclamo" runat="server" Width="90px"></asp:TextBox></td><td style="width: 89px"><asp:TextBox ID="txtFechaReclamo" runat="server" Width="84px"></asp:TextBox></td><td style="width: 91px"><asp:TextBox ID="txtFechaAtencion" runat="server" Width="84px"></asp:TextBox><asp:MaskedEditExtender ID="txtFechaAtencion_MaskedEditExtender" runat="server" 
                                    ClearTextOnInvalid="True" Mask="99/99/9999" MaskType="Date" 
                                    TargetControlID="txtFechaAtencion"></asp:MaskedEditExtender></td><td style="width: 93px"><asp:TextBox ID="txtFechaFinAtencion" runat="server" Width="84px"></asp:TextBox><asp:MaskedEditExtender ID="txtFechaFinAtencion_MaskedEditExtender" 
                                        runat="server" ClearTextOnInvalid="True" Mask="99/99/9999" MaskType="Date" 
                                        TargetControlID="txtFechaFinAtencion"></asp:MaskedEditExtender></td><td style="width: 93px"><asp:DropDownList ID="ddlEstado" runat="server" AppendDataBoundItems="true" 
                    Width="100px"></asp:DropDownList></td><td><asp:DropDownList ID="ddlMotivo" runat="server" AppendDataBoundItems="True" 
                                    Width="300px" AutoPostBack="True"></asp:DropDownList></td></tr><tr><td colspan="2">&#160;</td><td style="width: 89px">&#160;</td><td style="width: 91px">&#160;</td><td style="width: 93px">&#160;</td><td style="width: 93px">&#160;</td><td>&#160;</td></tr><tr><td colspan="7">
                                        <asp:UpdatePanel ID="upDevolucion" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Panel ID="pCambioMedioPago" runat="server">
                                                    <table class="TablaMarco" style="width: 100%">
                                                        <tr>
                                                            <td class="TablaTitulo">Cambio Forma de Pago</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="5" style="width: 565px">
                                                        <tr>
                                                            <td></td>
                                                            <td>Medio de Pago</td>
                                                            <td>Número</td>
                                                            <td>CBU</td>
                                                            <td>&nbsp;</td>
                                                        <tr>
                                                            <td style="background-color: #B5B5B5; width: 303px; text-align:right;">Actual Medio de Pago</td>
                                                            <td style="width: 93px">
                                                                <asp:DropDownList ID="ddlMedioPagoActual" runat="server" AppendDataBoundItems="true" Enabled="False" Width="151px"></asp:DropDownList></td>
                                                            <td style="width: 92px">
                                                                <asp:TextBox ID="txtNumeroActual" runat="server" Enabled="False" Width="90px" ReadOnly="True"></asp:TextBox></td>
                                                            <td style="width: 88px">
                                                                <asp:TextBox ID="txtCBUActual" runat="server" Width="120px" Enabled="False" ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 68px">&nbsp;</td>
                                                        </tr><tr>
                                                            <td style="background-color: #E0E0E0; text-align:right;">Nuevo Medio de Pago</td>
                                                            <td class="auto-style3"><asp:DropDownList ID="ddlMedioPago" runat="server" AppendDataBoundItems="true" Enabled="False" Width="151px"></asp:DropDownList></td>
                                                            <td class="auto-style4"><asp:TextBox ID="txtNumero" runat="server" Enabled="False" Width="90px"></asp:TextBox></td>
                                                            <td class="auto-style5"><asp:TextBox ID="txtCBU" runat="server" Width="120px" Enabled="False" ReadOnly="True"></asp:TextBox></td>
                                                            <td><asp:ImageButton ID="iBtnVerCobroSel" runat="server" ImageUrl="~/Images/Icon/ver.gif" /></td>
                                                        </tr>
                                                            </table>
                                                                </td></tr>
                                                            </table></asp:Panel><asp:Panel ID="pDevolucion" runat="server" Height="100px"><table class="TablaMarco" style="width: 100%"><tr><td class="TablaTitulo">Devolución</td></tr><tr><td><table border="0" width="565px"><tr><td style="width:280px; text-align:center" rowspan="2"><asp:CheckBox ID="chkCtaExterna" runat="server" Text="Cuenta Externa" AutoPostBack="True" /></td><td>Medio de Pago</td><td>Número</td><td style="width:120px;"><asp:Label ID="lb_cbuDev" runat="server" Text="CBU"></asp:Label></td><td style="width:16px;">&nbsp;</td></tr><tr><td><asp:DropDownList ID="ddlMedioPagoDev" runat="server" AppendDataBoundItems="true" Enabled="False" Width="151px" AutoPostBack="True"></asp:DropDownList></td><td><asp:TextBox ID="txtNumeroDev" runat="server" Enabled="False" Width="90px"></asp:TextBox></td><td><asp:TextBox ID="txtCBUDev" runat="server" Enabled="False" ReadOnly="True" Width="120px"></asp:TextBox></td><td><asp:ImageButton ID="iBtnVerCobroSelDev" runat="server" ImageUrl="~/Images/Icon/ver.png" /></td></tr></table></td></tr></table></asp:Panel>
            <div id="frmCobro" style="width:900px; height:350px; display:none;	padding-top:5px; margin:auto;"><asp:Button ID="btnSelCobroAceptar" runat="server" Text="Aceptar" 
                        Width="70px" /><asp:GridView ID="gvCobro" runat="server" AutoGenerateColumns="False" 
                        Width="891px"><Columns><asp:TemplateField><HeaderTemplate><input id="chkSelTodo" name="chkRpt" 
    onclick="SelectCobro(this);" type="checkbox" /></HeaderTemplate><ItemTemplate><asp:CheckBox ID="chkSelCobro" runat="server" /></ItemTemplate><ItemStyle HorizontalAlign="Center" Width="30px" /></asp:TemplateField><asp:TemplateField HeaderText="ValorCobrado" Visible="False"><EditItemTemplate><asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("valor") %>'></asp:TextBox></EditItemTemplate><ItemTemplate><asp:Label ID="Label1" runat="server" Text='<%# Bind("valor") %>'></asp:Label></ItemTemplate></asp:TemplateField><asp:TemplateField HeaderText="finPeriodoSel" Visible="False"><EditItemTemplate><asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("inicioPeriodo") %>'></asp:TextBox></EditItemTemplate><ItemTemplate><asp:Label ID="Label3" runat="server" 
                                        Text='<%# Bind("inicioPeriodo", "{0:d}") %>'></asp:Label></ItemTemplate></asp:TemplateField><asp:BoundField DataField="IdCobro" HeaderText="Nº Cuota"><ItemStyle HorizontalAlign="Center" Width="70px" /></asp:BoundField><asp:BoundField DataField="MedioPago" HeaderText="Medio Pago"><ItemStyle HorizontalAlign="Center" Width="150px" /></asp:BoundField><asp:BoundField DataField="numeroCuenta" HeaderText="Nº Cuenta/Tarjeta"><ItemStyle HorizontalAlign="Center" Width="200px" /></asp:BoundField><asp:BoundField DataField="ValorMoneda" HeaderText="Valor Cobrado"><ItemStyle HorizontalAlign="Center" Width="100px" /></asp:BoundField><asp:BoundField DataField="fechaUltimoCobro" DataFormatString="{0:d}" 
                                HeaderText="Fecha Cobro" HtmlEncode="False"><ItemStyle HorizontalAlign="Center" Width="80px" /></asp:BoundField><asp:TemplateField HeaderText="Inicio Periodo"><EditItemTemplate><asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("inicioPeriodo") %>'></asp:TextBox></EditItemTemplate><ItemTemplate><asp:Label ID="Label2" runat="server" 
                                        Text='<%# Bind("inicioPeriodo", "{0:d}") %>'></asp:Label></ItemTemplate><ItemStyle HorizontalAlign="Center" Width="80px" /></asp:TemplateField><asp:BoundField DataField="finPeriodo" DataFormatString="{0:d}" 
                                HeaderText="Fin Periodo" HtmlEncode="False"><ItemStyle HorizontalAlign="Center" Width="80px" /></asp:BoundField><asp:BoundField DataField="EstadoCobro" HeaderText="Estado"><ItemStyle HorizontalAlign="Center" /></asp:BoundField></Columns></asp:GridView></div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddlEstado" EventName="SelectedIndexChanged" />
                                                <asp:AsyncPostBackTrigger ControlID="ddlMotivo" EventName="SelectedIndexChanged" />
                                                <asp:AsyncPostBackTrigger ControlID="BCbtnAceptar" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="chkCtaExterna" EventName="CheckedChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>

                                                                                                                                                                                                                                                                                                      </td></tr><tr><td style="width: 150px">Observaciones AON</td><td style="width: 84px">&#160;</td><td style="width: 89px">&#160;</td><td style="width: 91px">&#160;</td><td style="width: 93px">&#160;</td><td style="width: 93px">&nbsp;</td><td>&#160;</td></tr><tr><td colspan="5"><asp:TextBox ID="txtObservacion" runat="server" Height="100px" 
                                    TextMode="MultiLine" Width="490px" MaxLength="2000"></asp:TextBox></td><td colspan="2"><asp:Panel ID="pResumen" runat="server" Visible="False"><table style="width: 374px"><tr><td style="width: 195px">Resolución</td><td rowspan="5"><table style="width: 100%"><tr><td><asp:Label ID="lblrResolucion" runat="server"></asp:Label></td></tr><tr><td><asp:Label ID="lblrCuotasDev" runat="server"></asp:Label></td></tr><tr><td><asp:Label ID="lblrMesesDev" runat="server"></asp:Label></td></tr><tr><td><asp:Label ID="lblrImporteDev" runat="server"></asp:Label></td></tr><tr><td><asp:Label ID="lblrFechaCancelCer" runat="server"></asp:Label></td></tr></table></td></tr><tr><td style="width: 195px">Cuotas a Devolver</td></tr><tr><td style="width: 195px">Meses a Devolver</td></tr><tr><td style="width: 195px">Importe a Devolver</td></tr><tr><td style="width: 195px">Fecha de Cancelación&#160; de Seguro</td></tr></table></asp:Panel></td></tr><tr><td style="width: 150px">Observaciones Aseg</td><td style="width: 84px">&#160;</td><td style="width: 89px">&#160;</td><td style="width: 91px">&#160;</td><td style="width: 93px">&#160;</td><td style="width: 93px">&nbsp;</td><td>&#160;</td></tr><tr><td colspan="5"><asp:TextBox ID="txtObservacionAseg" runat="server" Height="100px" MaxLength="2000" TextMode="MultiLine" Width="490px"></asp:TextBox></td><td colspan="2"><asp:TextBox ID="txtFechaVigFin" runat="server" Enabled="False" Width="84px" Visible="False"></asp:TextBox><asp:MaskedEditExtender ID="txtFechaVigFin_MaskedEditExtender" runat="server" ClearTextOnInvalid="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaVigFin"></asp:MaskedEditExtender><asp:TextBox ID="txtFechaVigInicio" runat="server" Enabled="False" Width="84px" Visible="False"></asp:TextBox><asp:MaskedEditExtender ID="txtFechaVigInicio_MaskedEditExtender" runat="server" ClearTextOnInvalid="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaVigInicio"></asp:MaskedEditExtender><asp:TextBox ID="txtIDMoneda" runat="server" Enabled="False" Width="90px" Visible="False"></asp:TextBox></td></tr></table></td></tr></table></td></tr><tr><td><asp:HiddenField ID="hfSimboloMoneda" runat="server" /></td></tr><tr><td><asp:Button ID="btnModificar" runat="server" Text="Modificar" 
            Width="80px" />&#160;<asp:Button ID="btnGrabar" runat="server" Text="Grabar" Width="80px" 
                    ValidationGroup="grabar" Enabled="False" /><asp:Button ID="btnActRegReclamo" runat="server" 
                    CssClass="btnOculto" />&nbsp;<asp:Button ID="btnCancelar" runat="server" 
            Enabled="False" Text="Cancelar" Width="80px" />&#160;<asp:Button ID="btnImpresionNota" runat="server" CausesValidation="False" Text="Imprimir Nota Reclamo" Width="150px" Enabled="False" /><asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" 
                    ShowMessageBox="True" ShowSummary="False" ValidationGroup="grabar" /></td></tr></table></ContentTemplate></asp:UpdatePanel><br /></ContentTemplate>
        






</asp:TabPanel>
    </asp:TabContainer>

    &nbsp;<asp:UpdateProgress ID="upGeneral" runat="server" DisplayAfter="0">
        <ProgressTemplate>
            <uc1:Cargando ID="Cargando1" runat="server" />
        </ProgressTemplate>
    </asp:UpdateProgress>
        <div id="frmBuscarMedioPago"
        style="width: 452px; display: none; margin: auto">
        <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:HiddenField ID="hfRaiz" runat="server" />
                <table class="TablaMarco" style="width: 440px">
                    <tr>
                        <td class="TablaTitulo">Buscar Medio de Pago</td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width: 420px">
                                <tr>
                                    <td>
                                        <table style="width: 431px">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvCuentaTarjeta" runat="server" AutoGenerateColumns="False" Caption="Medio Pago" EnableModelValidation="True" Width="431px">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("ID") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="IDMedioPago" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("IDMedioPagoBAIS")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("IDMedioPagoBAIS")%>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Tipo">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Descripcion")%>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemStyle Width="130px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Moneda">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("IDMoneda") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("IDMoneda") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" Width="80px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Número">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("Numero") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("Numero") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Raíz">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("raiz") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("raiz") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" Width="140px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="CBU">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label7" runat="server" Text='<%# Bind("CBU") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("CBU") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="ibSeleccionar" runat="server" CausesValidation="False" CommandName="Select" ImageUrl="~/Img/seleccionar.gif" ToolTip="Seleccionar" />
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            No se encontraron Cuentas/Tarjetas
                                                        </EmptyDataTemplate>
                                                        <RowStyle Height="23px" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                            </table>
                            <table style="width: 100%;">
                                <tr>
                                    <td style="text-align: center">
                                        <asp:Button ID="BCbtnAceptar" runat="server" CausesValidation="False" Text="Aceptar" />
                                    </td>
                                    <td style="text-align:center">
                                        <asp:Button ID="btnCancelarBCliente" runat="server" Text="Cancelar" CausesValidation="False" OnClientClick="$.modal.close(); return false;" />
                                        <asp:Button ID="btnLoad" CssClass="btnOculto" runat="server" CausesValidation="False" Text="load" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress8" runat="server" DisplayAfter="0">
            <ProgressTemplate>
                <uc1:Cargando ID="Cargando8" runat="server" />
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>




        <script type="text/javascript">
//            window.onload = function () {
//               TabActive(false);
//            }

            function TabActive(value) {
                $find('<%=TabRegReclamo.ClientID%>').get_tabs()[1].set_enabled(value);



                /*var CTab2 = $get('__tab_MainContent_TabRegSiniestro_TabPanel2');
                CTab2.disabled = value;*/
                //__tab_MainContent_TabRegSiniestro_TabPanel2
            }
    </script>
</asp:Content>
