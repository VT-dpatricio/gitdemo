﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pCobroReclamo.aspx.vb" Inherits="VANSiniestros.CobroReclamo" culture="es-PE" uiCulture="es-PE"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">

                <asp:GridView ID="gvCobro" runat="server" AutoGenerateColumns="False" 
                    Width="891px" PageSize="14" AllowPaging="True">
                    <Columns>
                        <asp:BoundField DataField="IdCobro" HeaderText="Nº Cuota">
                        <ItemStyle HorizontalAlign="Center" Width="70px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MedioPago" HeaderText="Medio Pago">
                        <ItemStyle HorizontalAlign="Center" Width="150px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="numeroCuenta" HeaderText="Nº Cuenta/Tarjeta">
                        <ItemStyle HorizontalAlign="Center" Width="200px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ValorMoneda" HeaderText="Valor Cobrado">
                        <ItemStyle HorizontalAlign="Center" Width="100px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FechaProceso" 
                            HeaderText="Fecha Cobro" HtmlEncode="False">
                        <ItemStyle HorizontalAlign="Center" Width="80px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="inicioPeriodo" DataFormatString="{0:d}" 
                            HeaderText="Inicio Periodo" HtmlEncode="False">
                        <ItemStyle HorizontalAlign="Center" Width="80px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="finPeriodo" DataFormatString="{0:d}" 
                            HeaderText="Fin Periodo" HtmlEncode="False">
                        <ItemStyle HorizontalAlign="Center" Width="80px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="EstadoCobro" HeaderText="Estado">
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
            
    <asp:HiddenField ID="hfIDCertificado" runat="server" Value="0" />

    </form>
</body>
</html>
