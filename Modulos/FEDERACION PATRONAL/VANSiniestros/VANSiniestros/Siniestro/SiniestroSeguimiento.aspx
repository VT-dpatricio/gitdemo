﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="SiniestroSeguimiento.aspx.vb" Inherits="VANSiniestros.SiniestroSeguimiento" culture="es-PE" uiCulture="es-PE" EnableEventValidation="false"%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="../Controles/Cargando.ascx" tagname="Cargando" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/jquery-1.7.1.min.js" type="text/javascript"></script>  
<script src="../Scripts/jquery.simplemodal.1.4.3.min.js" type="text/javascript"></script>  
    <script type="text/javascript">

        function AdjuntarDoc() {
            var pID = $get('<%= hfIDSiniestro.ClientID%>').value;
            var rowsCount = $get('<%=gvDocumento0.Rows.Count%>');

            var src = "pAdjuntarDoc.aspx?ID=" + pID + "&cR=" + rowsCount;
            $.modal('<iframe src="' + src + '" height="95" width="635" style="border:0" frameborder=0>', {
                closeHTML: "<a href='#' title='Cerrar' class='modal-close'><img src='../Images/cerrar.gif' alt='Cerrar' width='16' height='16'/></a>",
                containerCss: {
                    backgroundColor: "#FFF",
                    borderColor: "#750000",
                    height: 100,
                    padding: 0,
                    width: 640
                },
                overlayClose: false,
                appendTo: '#aspnetForm',
                //appendTo: 'form',
                persist: true
            });
            return false;

            /*
            var src = "http://365.ericmmartin.com/";
            $.modal('<iframe src="' + src + '" height="450" width="830" style="border:0">', {
                closeHTML: "",
                containerCss: {
                    backgroundColor: "#fff",
                    borderColor: "#fff",
                    height: 450,
                    padding: 0,
                    width: 830
                },
                overlayClose: true
            });
            */




       }

        function RegArchivo() {
            var pID = $get('<%= hfIDSiniestro.ClientID%>').value;
                   var src = "pRegLlamada.aspx?ID=" + pID;
                   $.modal('<iframe src="' + src + '" height="145" width="500" style="border:0" frameborder=0>', {
                       closeHTML: "<a href='#' title='Cerrar' class='modal-close'><img src='../Images/cerrar.gif' alt='Cerrar' width='16' height='16'/></a>",
                       containerCss: {
                           backgroundColor: "#FFF",
                           borderColor: "#750000",
                           height: 150,
                           padding: 0,
                           width: 510
                       },
                       overlayClose: false//,
                       //appendTo: '#aspnetForm',
                       //persist: false
                   });
               }




       function RegLlamada() {
           var pID = $get('<%= hfIDSiniestro.ClientID%>').value;
            var src = "pRegLlamada.aspx?ID=" + pID;
            $.modal('<iframe src="' + src + '" height="145" width="500" style="border:0" frameborder=0>', {
                closeHTML: "<a href='#' title='Cerrar' class='modal-close'><img src='../Images/cerrar.gif' alt='Cerrar' width='16' height='16'/></a>",
                containerCss: {
                    backgroundColor: "#FFF",
                    borderColor: "#750000",
                    height: 150,
                    padding: 0,
                    width: 510
                },
                overlayClose: false//,
            });
        }

        function ActgvLlamada() {
            $get('<%= btnActgvLlamada.ClientID%>').click();
            $.modal.close();
            alert('Llamada registrada con éxito');
        }


        function verDocumento(url) {
            window.open(url, 'popup', 'width=687,height=500');
        }

        function ActTabBusquedaSiniestro() {
            TabActive(false);         
            $find('<%=TabSiniestro.ClientID%>').set_activeTabIndex(0);
         }

        function ActTabSiniestro() {
             TabActive(true);
             $get('<%= btnActSiniestro.ClientID%>').click();
             $find('<%=TabSiniestro.ClientID%>').set_activeTabIndex(1);
         }

        //function ActTabInfo() {
        //    TabActive(false);
        //    $find('<%=TabSiniestro.ClientID%>').set_activeTabIndex(2);
        //}

         function ActgvDocumento() {
             $get('<%= btn_RfArchivo.ClientID%>').click();
             $.modal.close();
             alert('Documento guardado con éxito');
         }

        function ActElimArchivo() {
            $get('<%= btnActgvDocumento.ClientID%>').click();
            $get('<%= btn_AllDoc.ClientID%>').click();
            alert('El archivo se eliminó con éxito');
        }


        function ActCheck(id) {
            SetFechaEntrega(id);
            document.getElementById('<%=idDocSin.ClientID%>').value = id;
            $get('<%= btn_refresDoc.ClientID%>').click();
        }

        function SetFechaEntrega(id) {
            var TargetBaseControl = document.getElementById('<%= gvDocumento0.ClientID%>');
            var TargetChildControl = "chkPermiso";
            var Inputs = TargetBaseControl.getElementsByTagName("input");

            var Grid_Table = document.getElementById('<%= gvDocumento0.ClientID%>');

            if (!Inputs[id].checked) {
                Grid_Table.rows[id].cells[1].textContent = fechaActual();
                Grid_Table.rows[id].cells[1].innerText = fechaActual();
                Grid_Table.rows[id].cells[1].value = fechaActual();
            }

        }


         function ActpostEvaluacion(pEstado) {
             if ($get('<%= btnEvaluar.ClientID%>')) {
                 $get('<%= btnEvaluar.ClientID%>').style.display = 'none';
              }
             
             //$get('<%= btnGrabar.ClientID%>').style.display = 'none';
             $get('<%= btnRechazar.ClientID%>').style.display = 'none';
             $get('<%= txtEstado.ClientID%>').value = pEstado;
             $.modal.close();
             alert('Evaluación registrada con éxito');
         }

         function vDecimal(evt) {
             // NOTE: Backspace = 8, Enter = 13, '0' = 48, '9' = 57, '.' = 46       
             if (window.event) // IE
             {
                 key = evt.keyCode;
             }
             else if (e.which) // Netscape/Firefox/Opera
             {
                 key = evt.which;
             }

             return (key <= 13 || (key >= 48 && key <= 57) || key == 46);
         }
        //onkeypress="javascript:return vDecimal(event);"
         
         //window.onload = function () {
         //    TabActive(false);
         //}

         //function TabActive(value) {
         //    $find('<%=TabSiniestro.ClientID%>').get_tabs()[1].set_enabled(value);
         //}
         


         function frmEvaluar() {
             $("#frmEvaluar").modal({
                 closeHTML: "<a href='#' title='Cerrar' class='modal-close'><img src='../Images/cerrar.gif' alt='Cerrar' width='16' height='16'/></a>",
                 containerCss: {
                     backgroundColor: "#FFF",
                     borderColor: "#750000",
                     height: 500,
                     padding: 0,
                     width: 590
                 },
                 overlayClose: false,
                 appendTo: '#aspnetForm',
                 persist: true                 
             });             
         }

         function SelectDoc(CheckBox) {
             TotalChkBx = parseInt('<%= gvDocumento0.Rows.Count%>');
            var TargetBaseControl = document.getElementById('<%= gvDocumento0.ClientID%>');
            var TargetChildControl = "chkPermiso";
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            for (var iCount = 0; iCount < Inputs.length; ++iCount) {
                if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0)
                    Inputs[iCount].checked = CheckBox.checked;
            }

         }

        function fechaActual() {
            var today = new Date();
            var dd = today.getDate().toString();
            var mm = (today.getMonth() + 1).toString();
            var pad = "00"
            var fecha = (pad + dd.toString()).slice(-2) + "/" + (pad + mm.toString()).slice(-2) + "/" + today.getFullYear();

            return fecha;
        }


        function SelectFecha(id) {
            var TargetBaseControl = document.getElementById('<%= gvDocumento0.ClientID%>');
            var TargetChildControl = "chkPermiso";
            var Inputs = TargetBaseControl.getElementsByTagName("input");

            var str = '';
            var Grid_Table = document.getElementById('<%= gvDocumento0.ClientID%>');
            //alert(Inputs[id].checked);
            //alert(id + "-" + Grid_Table.rows[id].cells[1].innerText);

            if (!Inputs[id].checked & Grid_Table.rows[id].cells[1].innerText == "01/01/1900") {
                Grid_Table.rows[id].cells[1].textContent = fechaActual();
                Grid_Table.rows[id].cells[1].innerText = fechaActual();
                Grid_Table.rows[id].cells[1].value = fechaActual();
            }

            /*for (var row = 1; row < Grid_Table.rows.length; row++) {
                for (var col = 0; col < Grid_Table.rows[row].cells.length; col++) {
                    if (Grid_Table.rows[row].cells[0].textContent == id) {
                        Grid_Table.rows[row].cells[1].textContent = "31/12/1979";
                        Grid_Table.rows[row].cells[1].innerText = "31/12/1979";
                        Grid_Table.rows[row].cells[1].value = "31/12/1979";
                    }
                }
            }
            return false;*/
        }


        function EditCell(employeeID, celText, ctrlType, rowNo, colNo) {
            var cellPos;
            var grdCtl = document.getElementById('gvDocumento0');
            cellPos = grdCtl.rows[rowNo].cells[colNo]

            document.getElementById('<%=hidEmployeeId.ClientID%>').value = employeeID;


        switch (ctrlType) {
            case "TextBox":
                divEmployee = document.getElementById("divText");
                document.getElementById('txtEmployeeName').value = celText;
                HideDDLDiv();
                HideRdBDiv();
                break;
            case "DropDownList":
                divEmployee = document.getElementById("divDropDownList");
                document.getElementById("ddlLocation").value = celText;
                HideTextDiv();
                HideRdBDiv();
                break;
            case "RadioButton":
                divEmployee = document.getElementById("dirRdB");
                var radioButtons = document.getElementsByName("rdDesignation");
                for (var x = 1; x < radioButtons.length; x++) {
                    if (radioButtons[x].value == celText) {
                        radioButtons[x].checked = true;
                    }
                }
                HideTextDiv();
                HideDDLDiv();
                break;

        }

        divEmployee.style.left = (cellPos.offsetLeft + 9) + "px";
        divEmployee.style.top = (cellPos.offsetTop + 9) + "px";
        divEmployee.style.display = "";
    }



    function HideTextDiv() {
        var divEmployee = document.getElementById("divText");
        divEmployee.style.display = "none";
        return false;
    }

    function UplodaFile() {
        var pID = $get('<%= hfIDSiniestro.ClientID%>').value;
                   var src = "pRegLlamada.aspx?ID=" + pID;
                   $.modal('<iframe src="' + src + '" height="145" width="500" style="border:0" frameborder=0>', {
                       closeHTML: "<a href='#' title='Cerrar' class='modal-close'><img src='../Images/cerrar.gif' alt='Cerrar' width='16' height='16'/></a>",
                       containerCss: {
                           backgroundColor: "#FFF",
                           borderColor: "#750000",
                           height: 150,
                           padding: 0,
                           width: 510
                       },
                       overlayClose: false//,
                   });
               }
        
    </script>

    <style type="text/css">
        .auto-style1 {
            text-align: right;
            vertical-align: middle;
            font-size: 11px;
            width: 134px;
        }
        .auto-style2 {
            text-align: right;
            vertical-align: middle;
            font-size: 11px;
            width: 135px;
        }
        .auto-style8 {
            width: 122px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <asp:TabContainer ID="TabSiniestro" runat="server" ActiveTabIndex="0" 
        Width="100%">
        <asp:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
            <HeaderTemplate>
                Búsqueda de Siniestro
            
</HeaderTemplate>
            
<ContentTemplate>
                <table cellpadding="3" cellspacing="0" style="width: 998px">
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="upFiltroBuscar" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="5" class="TablaMarco" style="width: 990px">
                                        <tr>
                                            <td class="TablaLabelR" style="width: 91px">
                                                Entidad:</td>
                                            <td style="width: 161px">
                                                <asp:DropDownList ID="ddlEntidad" runat="server" AppendDataBoundItems="True" 
                                                    AutoPostBack="True" Width="162px">
                                                    <asp:ListItem Value="0">Todas</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td class="auto-style1">
                                                &nbsp;</td>
                                            <td style="width: 161px">
                                                &nbsp;</td>
                                            <td class="TablaLabelR">
                                                &nbsp;</td>
                                            <td style="width: 157px">
                                                &nbsp;</td>
                                            <td style="width: 225px">
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="7">
                                                <asp:Panel ID="pFiltroSeg" runat="server">
                                                    <table cellpadding="0" cellspacing="5" style="width: 100%">
                                                        <tr>
                                                            <td class="TablaLabelR" style="width: 85px">
                                                                Buscar por:</td>
                                                            <td style="width: 161px">
                                                                <asp:DropDownList ID="ddlBuscarPor" runat="server" AutoPostBack="True" 
                                                                    ValidationGroup="Buscar" Width="162px">
                                                                    <asp:ListItem Selected="True" Value="NroDoc">N° Documento</asp:ListItem>
                                                                    <asp:ListItem Value="ApeNom">Apellidos y Nombres</asp:ListItem>
                                                                    <asp:ListItem Value="NroTicket">N° Ticket</asp:ListItem>                                                                    
                                                                    <asp:ListItem Value="NroCertificado">N° Certificado BAIS</asp:ListItem>
                                                                    <asp:ListItem Value="NroFisico">N° Certificado Aseg.</asp:ListItem>
                                                                    <asp:ListItem Value="NroCuenta">N° Cuenta</asp:ListItem>
                                                                    <asp:ListItem Value="NroRaiz">N° Raíz</asp:ListItem>
                                                                    <asp:ListItem Value="NroAseg">N° Siniestro Aseg.</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="auto-style2">
                                                                <asp:Label ID="lblPar1" runat="server" Text="N° Documento:"></asp:Label>
                                                            </td>
                                                            <td style="width: 161px">
                                                                <asp:TextBox ID="txtPar1" runat="server" Width="160px"></asp:TextBox>
                                                            </td>
                                                            <td class="TablaLabelR" style="width: 73px">
                                                                <asp:Label ID="lblPar2" runat="server" Visible="False">Nombres:</asp:Label>
                                                            </td>
                                                            <td style="width: 157px">
                                                                <asp:TextBox ID="txtPar2" runat="server" Visible="False" Width="160px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnBuscar" runat="server" Text="Buscar" 
                                                                    ValidationGroup="Buscar" Width="60px" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                
                                                <asp:Panel ID="pFiltroEval" runat="server">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td class="TablaLabelR" style="width: 88px">
                                                                Estado:</td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlEstadoSiniestroF" runat="server" 
                                                                    AppendDataBoundItems="True" AutoPostBack="True" Width="162px">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                               
                                            </td>
                                        </tr>
                                    </table>
&nbsp;
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="upListaCertificado" runat="server" 
                                UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:GridView ID="gvListaSiniestro" runat="server" AllowPaging="True" 
                                        AutoGenerateColumns="False" DataKeyNames="IDSiniestro,IDCertificado" 
                                        Width="989px">
                                        <Columns>
                                            <asp:TemplateField HeaderText="N° Ticket">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text='<%#  Format(Eval("FechaCreacion"),"yyyyMMdd") + "-" + Format(Eval("IDSiniestro"),"000000")  %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("IDSiniestro") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="120px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="F. Siniestro">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label3" runat="server" Text='<%# Format(Eval("FechaSiniestro"),"dd/MM/yyyy") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("FechaSiniestro") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="IDCertificado">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label8" runat="server" Text='<%# Bind("IdCertificado") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="120px" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="IDProducto" HeaderText="IDProducto" 
                                                Visible="False" />
                                            <asp:TemplateField HeaderText="Producto">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label9" runat="server" Text='<%# Bind("Producto") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Tipo Documento">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label10" runat="server" Text='<%# Bind("TipoDocumento") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="100px" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="N° Documento">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label11" runat="server" Text='<%# Bind("CcCliente") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="100px" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Apellidos y Nombres">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label12" runat="server" 
                                                        Text='<%# Eval("Apellido1") &  " " & Eval("Nombre1") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="180px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Estado">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label13" runat="server" Text='<%# Bind("EstadoSiniestro") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="70px" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:CommandField ButtonType="Image" SelectImageUrl="~/img/seleccionar.gif" 
                                                ShowSelectButton="True">
                                            <ItemStyle HorizontalAlign="Center" Width="30px" />
                                            </asp:CommandField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No se encontron registros
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="ddlEstadoSiniestroF" 
                                        EventName="SelectedIndexChanged" />
                                    <asp:AsyncPostBackTrigger ControlID="ddlEntidad" 
                                        EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            
</ContentTemplate>
        
</asp:TabPanel>
        <asp:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
            <HeaderTemplate>Datos del Siniestro</HeaderTemplate>
            <ContentTemplate>
                <asp:UpdatePanel ID="upRSiniestro" runat="server" UpdateMode="Conditional"><ContentTemplate>
                        <table cellpadding="0" cellspacing="5" style="width: 100%">
                            <tr>
                                <td colspan="2">
                                    <table class="TablaMarco" style="width: 100%">
                                        <tr>
                                            <td class="TablaTitulo">
                                                Datos del Certificado<asp:HiddenField ID="hfIDSiniestro" runat="server" />
                                                <asp:HiddenField ID="hfIDProducto" runat="server" Value="0" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table style="width: 977px">
                                        <tr>
                                            <td style="width: 119px">
                                                N° Certificado</td>
                                            <td style="width: 142px">
                                                Producto</td>
                                            <td style="width: 211px">
                                                &nbsp;</td>
                                            <td style="width: 108px">
                                                Plan</td>
                                            <td style="width: 157px">
                                                Canal</td>
                                            <td>
                                                Aseguradora</td>
                                        </tr>
                                        <tr>
                                            <td class="TablaCeldaBox" style="width: 119px">
                                                <asp:Label ID="lblNroCertificado" runat="server"></asp:Label>
                                            </td>
                                            <td class="TablaCeldaBox" colspan="2">
                                                <asp:Label ID="lblProducto" runat="server"></asp:Label>
                                                &nbsp;</td>
                                            <td class="TablaCeldaBox" style="width: 108px">
                                                <asp:Label ID="lblOpcion" runat="server"></asp:Label>
                                            </td>
                                            <td class="TablaCeldaBox" style="width: 157px">
                                                <asp:Label ID="lblCanal" runat="server"></asp:Label>
                                            </td>
                                            <td class="TablaCeldaBox">
                                                <asp:Label ID="lblAseguradora" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 119px">
                                                Tipo Documento</td>
                                            <td style="width: 142px">
                                                N° Documento</td>
                                            <td style="width: 211px">
                                                Asegurado</td>
                                            <td style="width: 108px">
                                                &nbsp;</td>
                                            <td style="width: 157px">
                                                Moneda Prima</td>
                                            <td>
                                                Estado</td>
                                        </tr>
                                        <tr>
                                            <td class="TablaCeldaBox" style="width: 119px">
                                                <asp:Label ID="lblTipoDocumento" runat="server"></asp:Label>
                                            </td>
                                            <td class="TablaCeldaBox" style="width: 142px">
                                                <asp:Label ID="lblNroDocumento" runat="server"></asp:Label>
                                            </td>
                                            <td class="TablaCeldaBox" colspan="2">
                                                <asp:Label ID="lblAsegurado" runat="server"></asp:Label>
                                            </td>
                                            <td class="TablaCeldaBox">
                                                <asp:Label ID="lblMonedaPrima" runat="server"></asp:Label>
                                            </td>
                                            <td class="TablaCeldaBox">
                                                <asp:Label ID="lblEstadoCer" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table class="TablaMarco" style="width: 100%">
                                        <tr>
                                            <td class="TablaTitulo">
                                                Datos del Siniestro</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellpadding="0" cellspacing="5" style="width: 974px">
                                                    <tr>
                                                        <td style="width: 103px; font-weight: bold;">
                                                            N° Ticket</td>
                                                        <td style="width: 275px" colspan="2">
                                                            Tipo Siniestro</td>
                                                        <td style="width: 111px">
                                                            Fecha Siniestro<asp:RequiredFieldValidator ID="RequiredFieldValidator2" 
                                                                runat="server" ControlToValidate="txtFechaSiniestro" 
                                                                ErrorMessage="Ingrese la Fecha de Siniestro" ForeColor="Red" 
                                                                ValidationGroup="grabar" Display="Dynamic">*</asp:RequiredFieldValidator>
                                                            <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                                                ControlToValidate="txtFechaSiniestro" Display="Dynamic" 
                                                                ErrorMessage="Ingrese una Fecha de Siniestro válida" ForeColor="Red" 
                                                                Operator="DataTypeCheck" Type="Date" ValidationGroup="grabar">*</asp:CompareValidator>
                                                            <asp:MaskedEditExtender ID="me" runat="server" ClearTextOnInvalid="True" 
                                                                Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaSiniestro">
                                                            </asp:MaskedEditExtender>
                                                        </td>
                                                        <td style="width: 87px">
                                                            M. Denunciado<asp:RequiredFieldValidator ID="RequiredFieldValidator3" 
                                                                runat="server" ControlToValidate="txtMontoDenunciado" 
                                                                ErrorMessage="Ingrese el Monto Denunciado" ForeColor="Red" 
                                                                ValidationGroup="grabar">*</asp:RequiredFieldValidator>
                                                        </td>
                                                        <td style="width: 76px">
                                                            Moneda</td>
                                                        <td style="width: 73px">
                                                            Monto Pago<asp:RequiredFieldValidator ID="RequiredFieldValidator4" 
                                                                runat="server" ControlToValidate="txtMontoPago" 
                                                                ErrorMessage=" Ingrese el Monto Pago" ForeColor="Red" 
                                                                ValidationGroup="grabar" Enabled="False">*</asp:RequiredFieldValidator>
                                                        </td>
                                                        <td style="width: 75px">
                                                            Moneda Pago</td>
                                                        <td style="width: 243px">
                                                            Estado Siniestro</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 103px">
                                                            <asp:TextBox ID="txtNroTicket" runat="server" Font-Bold="True" ReadOnly="True" 
                                                                Width="90px">-</asp:TextBox>
                                                        </td>
                                                        <td style="width: 275px" colspan="2">
                                                            <asp:TextBox ID="txtTipoSiniestro" runat="server" ReadOnly="True" Width="260px"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 111px">
                                                            <asp:TextBox ID="txtFechaSiniestro" runat="server" Width="84px"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 87px">
                                                            <asp:TextBox ID="txtMontoDenunciado" runat="server" 
                                                                onkeypress="javascript:return vDecimal(event);" Width="85px">0</asp:TextBox>
                                                        </td>
                                                        <td style="width: 76px">
                                                            <asp:DropDownList ID="ddlMonedaDe" runat="server" Width="82px">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 73px">
                                                            <asp:TextBox ID="txtMontoPago" runat="server" 
                                                                onkeypress="javascript:return vDecimal(event);" Width="80px"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 75px">
                                                            <asp:DropDownList ID="ddlMonedaPago" runat="server" Width="82px" 
                                                                AppendDataBoundItems="True">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 243px">
                                                            <asp:UpdatePanel ID="upEstado" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:TextBox ID="txtEstado" runat="server" ReadOnly="True" Width="110px"></asp:TextBox>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="btnActgvDocumento" EventName="Click" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 103px">
                                                            Monto Pago Aseg.</td>
                                                        <td class="auto-style8">
                                                            Moneda Pago Aseg.</td>
                                                        <td style="width: 275px">N° Siniestro Aseg.</td>
                                                        <td style="width: 111px">
                                                            &nbsp;</td>
                                                        <td style="width: 87px">
                                                            &nbsp;</td>
                                                        <td style="width: 76px">
                                                            &nbsp;</td>
                                                        <td style="width: 73px">
                                                            &nbsp;</td>
                                                        <td style="width: 75px">
                                                            &nbsp;</td>
                                                        <td style="width: 243px">
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 103px">
                                                            <asp:TextBox ID="txtMontoPagoAse" runat="server" 
                                                                onkeypress="javascript:return vDecimal(event);" Width="80px"></asp:TextBox>
                                                        </td>
                                                        <td class="auto-style8">
                                                            <asp:DropDownList ID="ddlMonedaPagoAse" runat="server" 
                                                                AppendDataBoundItems="True" Width="82px">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 275px">
                                                            <asp:TextBox ID="txtNroSiniestro" runat="server" onkeypress="javascript:return vDecimal(event);" ReadOnly="True" Width="100%"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 111px">
                                                            &nbsp;</td>
                                                        <td style="width: 87px">
                                                            &nbsp;</td>
                                                        <td style="width: 76px">
                                                            &nbsp;</td>
                                                        <td style="width: 73px">
                                                            &nbsp;</td>
                                                        <td style="width: 75px">
                                                            &nbsp;</td>
                                                        <td style="width: 243px">
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" style="vertical-align: top">
                                                            <asp:UpdatePanel ID="upCobertura" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:GridView ID="gvCobertura" runat="server" AutoGenerateColumns="False" 
                                                                        DataKeyNames="IDCobertura" Width="473px" BorderColor="White" 
                                                                        BorderStyle="Solid" BorderWidth="4px" Height="100%">
                                                                        <Columns>
                                                                            <asp:BoundField DataField="IDCobertura" HeaderText="IDCobertura" 
                                                                                Visible="False" />
                                                                            <asp:BoundField DataField="Cobertura" HeaderText="Cobertura(s)">
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                        <EmptyDataTemplate>
                                                                            No se encontraron coberturas
                                                                        </EmptyDataTemplate>
                                                                    </asp:GridView>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                        <td colspan="5" style="vertical-align: top">
                                                            <asp:UpdatePanel ID="upDocSolicitado" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>

                                                                    <asp:Button ID="btnActgvDocumento" runat="server" CssClass="btnOculto" />
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="btnActgvDocumento" EventName="Click" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>

                                                           <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional" ViewStateMode="Enabled">
                                                                <ContentTemplate>
                                                            <asp:GridView ID="gvDocumento0" runat="server" AutoGenerateColumns="False" BorderColor="White" BorderStyle="Solid" BorderWidth="4px" 
                                                                DataKeyNames="IDSiniestroDoc" 
                                                                DataSourceID="oDS" 
                                                                OnRowEditing="gvDocumento_RowEditing" Width="490px">
                                                                <Columns>
                                                                    <asp:BoundField DataField="IDSiniestroDoc" HeaderText="IDSiniestroDoc" Visible="false" />
                                                                    <asp:TemplateField HeaderText="Adjunto" Visible="False">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label14" runat="server" Text='<%# Bind("Adjunto") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="TextBox3" runat="server" ReadOnly="True" Text='<%# Bind("Adjunto") %>'></asp:TextBox>
                                                                        </EditItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="Documento" HeaderText="Documentos Solicitados" ReadOnly="True" />

                                                                   <asp:TemplateField HeaderText="F. Entrega">
                                                                             <ItemTemplate>
                                                                                 <asp:Label ID="Label2" runat="server" Text='<%#FormatDate(Eval("FechaEntrega"))%>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <EditItemTemplate>
                                                                                    <asp:TextBox ID="txtFDesde" runat="server" Width="56px" Text='<%#Bind("FechaEntrega")%>'></asp:TextBox>
                                                                                    <asp:CalendarExtender ID="CExt1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtFDesde">
                                                                                    </asp:CalendarExtender>
                                                                                    <asp:MaskedEditExtender ID="MEE1" runat="server" ClearTextOnInvalid="True" Mask="99/99/9999"
                                                                                        MaskType="Date" TargetControlID="txtFDesde">
                                                                                    </asp:MaskedEditExtender>
                                                                            </EditItemTemplate>
                                                                             <HeaderStyle HorizontalAlign="Center" />
                                                                             <ItemStyle HorizontalAlign="Center" Width="110px" />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Entregado">
                                                                        <EditItemTemplate>
                                                                            <asp:CheckBox ID="CheckBox2" runat="server" AutoPostBack="true" Checked='<%# Bind("Entregado")%>' />
                                                                        </EditItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="20px" />
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkPermiso" CommandName="chk" runat="server" Checked='<%# Bind("Entregado")%>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <EditItemTemplate>
                                                                            <asp:ImageButton ID="IBGrabar" runat="server" CommandArgument='<%# Eval("IDSiniestroDoc") %>' ImageUrl="~/images/grid/Save.gif" onclick="IBGrabar_Click" Text="Update" ToolTip="Grabar" ValidationGroup="ValidarGrid" />
                                                                            <asp:ImageButton ID="IBCancelar" runat="server" CausesValidation="False" CommandName="Cancel" ImageUrl="~/images/grid/b_cancelar.gif" OnClick="IBCancelar_Click" Text="Cancel" ToolTip="Cancelar" />
                                                                        </EditItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="45px" />
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="IBEditar0" runat="server" CausesValidation="False" CommandArgument='<%# Eval("IDSiniestroDoc") %>'  CommandName="Edit" ImageUrl="~/images/grid/b_editar.gif" Text="Edit" ToolTip="Modificar Registro" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <EmptyDataTemplate>
                                                                    No se encontraron registros
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                            </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="btn_refresDoc" EventName="click" />
                                                                    <asp:AsyncPostBackTrigger ControlID="btn_AllDoc" EventName="click" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                            <asp:Button ID="btn_refresDoc" runat="server" CssClass="btnOculto" />
                                                            <asp:Button ID="btn_AllDoc" runat="server" CssClass="btnOculto" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                         <td colspan="9" style="vertical-align: top">
                                         <asp:UpdatePanel ID="updArchivo" runat="server" UpdateMode="Conditional">
                                              <ContentTemplate>
                                            <asp:GridView ID="gvRegDoc" runat="server" AllowPaging="True" 
                                                AutoGenerateColumns="False" BorderColor="White" BorderStyle="Solid" 
                                                BorderWidth="4px" PageSize="5" Width="100%" Caption="Documentos Adjuntos" 
                                                OnRowDeleting="gvRegDoc_RowDeleting"
                                                DataKeyNames="FechaCreacion,NombreArchivo">
                                                <Columns>
                                                    <asp:BoundField DataField="IDArchivo" HeaderText="IDArchivo" Visible="False" />
                                                    <asp:BoundField DataField="FechaCreacion" HeaderText="Fecha Ingreso" />
                                                    <asp:BoundField DataField="NombreArchivo" HeaderText="Nombre Archivo" />
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:ImageButton ID="ibtnReglla" runat="server" 
                                                                ImageUrl="~/Images/Icon/nuevo.gif" CommandArgument='<%# Eval("IDArchivo")%>' 
                                                                ToolTip="Adjuntar Documento"  OnClientClick='AdjuntarDoc()'  />
                                                        </HeaderTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                     <ItemTemplate>
                                                                <asp:ImageButton ID="btnEliminarDoc" runat="server" CausesValidation="False" CommandArgument='<%# Bind("IDArchivo") %>'
                                                                    ImageUrl="~/Images/Grid/anular.gif" OnClientClick="return confirm('Está seguro que desea eliminar el documento adjunto?');"
                                                                    OnCommand="btnEliminar_Command"
                                                                    Text="Delete" CommandName="delete" 
                                                                    ToolTip="Eliminar" />
                                                               </ItemTemplate>
                                                          <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                     </asp:TemplateField>
                                                </Columns>
                                                    <EmptyDataTemplate>
                                                        <asp:Button ID="btnRegistrarArchivo" runat="server" 
                                                            onclick="btnRegistraArchivo_Click1" Text="Nuevo Documento" 
                                                            Width="140px" onclientclick="AdjuntarDoc();" />
                                                    </EmptyDataTemplate>
                                                 </asp:GridView>
                                             </ContentTemplate>
                                                 <Triggers>
                                                           <asp:AsyncPostBackTrigger ControlID="btn_RfArchivo" EventName="click" />
                                                  </Triggers>
                                                </asp:UpdatePanel>
                                                <asp:Button ID="btn_RfArchivo" runat="server" CssClass="btnOculto" />
                                                      </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 398px">
                                    <table class="TablaMarco" style="width: 100%">
                                        <tr>
                                            <td class="TablaTitulo">
                                                Actualizar Información del Cliente</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table style="width: 463px">
                                                    <tr>
                                                        <td style="width: 320px">
                                                            E-mail<asp:RegularExpressionValidator ID="RegularExpressionValidator1" 
                                                                runat="server" ControlToValidate="txtAseEmail" 
                                                                ErrorMessage="Ingrese un E-mail válido" ForeColor="Red" 
                                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                                                ValidationGroup="grabar">*</asp:RegularExpressionValidator>
                                                        </td>
                                                        <td>
                                                            Teléfono</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 320px">
                                                            <asp:TextBox ID="txtAseEmail" runat="server" Width="320px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAseTelefono" runat="server" Width="127px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 320px">
                                                            Dirección</td>
                                                        <td>
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="txtAseDireccion" runat="server" Width="455px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 320px">
                                                            Observaciones</td>
                                                        <td>
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="txtAseObservacion" runat="server" Height="40px" 
                                                                TextMode="MultiLine" Width="453px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="TablaMarco" style="width: 534px; vertical-align: top;">
                                    <asp:UpdatePanel ID="upLlamada" runat="server">
                                        <ContentTemplate>
                                            <asp:GridView ID="gvLLamada" runat="server" AllowPaging="True" 
                                                AutoGenerateColumns="False" BorderColor="White" BorderStyle="Solid" 
                                                BorderWidth="4px" PageSize="5" Width="490px" Caption="Llamadas">
                                                <Columns>
                                                    <asp:BoundField DataField="IDLLamada" HeaderText="IDLLamada" Visible="False" />
                                                    <asp:TemplateField HeaderText="Fecha Hora">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FechaHora") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" 
                                                                Text='<%# Format(Eval("FechaHora"),"dd/MM/yyyy HH:mm tt") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Width="150px" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Observacion" HeaderText="Observaciones" />
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:ImageButton ID="ibtnReglla" runat="server" 
                                                                ImageUrl="~/Images/Icon/nuevo.gif" onclick="ibtnReglla_Click" 
                                                                ToolTip="Registrar Llamada" onclientclick="RegLlamada();" />
                                                        </HeaderTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    <asp:Button ID="btnRegistrarLlamada" runat="server" 
                                                        onclick="btnRegistrarLlamada_Click1" Text="Registrar Llamada" 
                                                        Width="140px" onclientclick="RegLlamada();" />
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                            <asp:Button ID="btnActgvLLamada" runat="server" CssClass="btnOculto" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Button ID="btnGrabar" runat="server" Text="Grabar" 
                                        ValidationGroup="grabar" Width="80px" />
                                    <asp:Button ID="btnActSiniestro" runat="server" CssClass="btnOculto" />
                                    <asp:Button ID="btnEvaluar" runat="server" Text="Aprobar" Width="70px" />
                                    <asp:Button ID="btnRechazar" runat="server" Text="Rechazar" Width="90px" />
                                    <asp:ObjectDataSource ID="oDS" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="Listar" TypeName="VAN.Siniestros.BL.BLSiniestroDocumento">
                                        <SelectParameters>
                                            <asp:Parameter DefaultValue="" Name="pIDSiniestro" Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                    <asp:HiddenField ID="idDocSin" runat="server" />
                                    <asp:HiddenField ID="hidEmployeeId" runat="server" />
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" 
                                        ShowMessageBox="True" ShowSummary="False" ValidationGroup="grabar" />
                                </td>
                            </tr>
                        </table>

                        
                    
</ContentTemplate>
</asp:UpdatePanel>
                &nbsp;<div id="frmEvaluar" 
                    style="width:590px;height:200px; display:none;	padding-top:5px; margin:auto;">
                    <asp:UpdatePanel ID="upEvaluacion" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table style="width: 100%">
                                <tr>
                                    <td>
                                        
                                        <asp:GridView ID="gvEvaluacion" runat="server" AutoGenerateColumns="False" 
                                            DataKeyNames="IDPregunta" SkinID="GridList" Width="578px">
                                            <Columns>
                                                <asp:BoundField HeaderText="IDPregunta" Visible="False" />
                                                <asp:BoundField DataField="Nombre" HeaderText="Pregunta" />
                                                <asp:TemplateField HeaderText="Respuesta">
                                                    <ItemTemplate>
                                                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" 
                                                            RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="1">Si      </asp:ListItem>
                                                            <asp:ListItem Value="0">No</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="80px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Respuesta" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("Respuesta") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Respuesta") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Resultado" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label1" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="100px" Wrap="True" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>

                                        </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnValidar" runat="server" Text="Validar Respuestas" 
                                            Width="150px" />
                                        <asp:Panel ID="pResulEval" runat="server">
                                            <table class="TablaMarco" style="width: 100%">
                                                <tr>
                                                    <td class="TablaTitulo">
                                                        Resultado de la Evaluación</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellpadding="0" cellspacing="5" style="width: 100%">
                                                            <tr>
                                                                <td style="width: 162px">
                                                                    Estado</td>
                                                                <td>
                                                                    Observaciones</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 162px; vertical-align: top;">
                                                                    <asp:TextBox ID="txtEstadoEval" runat="server" ReadOnly="True" Width="160px"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtObservacionEval" runat="server" Height="50px" 
                                                                        TextMode="MultiLine" Width="369px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 162px">
                                                                    <asp:Button ID="btnGrabarEval" runat="server" Text="Grabar" Width="70px" />
                                                                </td>
                                                                <td>
                                                                    &nbsp;</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnEvaluar" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnRechazar" EventName="Click" />
                            <asp:PostBackTrigger ControlID="btnGrabarEval" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <br />
                </div>
            
</ContentTemplate>
        
</asp:TabPanel>
        <asp:TabPanel ID="TabPanel3" runat="server" HeaderText="Información Adicional">
            <HeaderTemplate>Información Adicional</HeaderTemplate>
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="gvInfoSiniestroC" runat="server" AllowPaging="True" DataKeyNames="Nombre,ValorString" Height="100%" Width="100%" AutoGenerateColumns="False">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="nombre" HeaderText="Nombre" >
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="ValorString" HeaderText="Valor" >
                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                    <EmptyDataTemplate>
                                                                        No se encontron registros
                                                                    </EmptyDataTemplate>
                       </asp:GridView>
                    </ContentTemplate>
                    <triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnActInfoAdicional" EventName="Click"></asp:AsyncPostBackTrigger>
                    </triggers>
                </asp:UpdatePanel><asp:Button ID="btnActInfoAdicional" runat="server" SkinID="BotonOculto" />
            </ContentTemplate>

        </asp:TabPanel>
    </asp:TabContainer>

    <asp:UpdateProgress ID="uproSin" runat="server" DisplayAfter="0">
        <ProgressTemplate>
            <uc1:Cargando ID="Cargando1" runat="server" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:HiddenField ID="hfOperacion" runat="server" />


        <script type="text/javascript">
            /*window.onload = function () {
                TabActive(false);
            }*/

            function TabActive(value) {
                $find('<%=TabSiniestro.ClientID%>').get_tabs()[1].set_enabled(value);



                /*var CTab2 = $get('__tab_MainContent_TabRegSiniestro_TabPanel2');
                CTab2.disabled = value;*/
                //__tab_MainContent_TabRegSiniestro_TabPanel2
            }
    </script>
    &nbsp;

</asp:Content>
