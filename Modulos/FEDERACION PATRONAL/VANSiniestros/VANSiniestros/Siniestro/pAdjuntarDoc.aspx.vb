﻿Imports System.IO
Imports VAN.Siniestros.BE
Imports VAN.Siniestros.BL
Public Class AdjuntarDoc
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            hfIDSiniestroDoc.Value = Int32.Parse(Request.QueryString("ID"))
            Dim cantidadRegistros As String = Request.QueryString("cR")
            hrowCount.Value = Int32.Parse(IIf((String.IsNullOrEmpty(cantidadRegistros) Or cantidadRegistros = "null"), 0, Request.QueryString("cR")))
            hNRandom.Value = Request.QueryString("RD")
            txtFechaEntrega.Text = DateTime.Now.ToShortDateString
        End If
    End Sub

    Protected Sub btnCargar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCargar.Click
        If (txtFechaEntrega.Text.Trim() = "") Then
            ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "Msg", "alert('Ingrese la Fecha de Ingreso')", True)
            Exit Sub
        End If
        Dim lista As ArrayList


        If hfIDSiniestroDoc.Value = "0" Then
            lblMensaje.Text = "...."
            Exit Sub
        Else
            If hfIDSiniestroDoc.Value = "-1" Then
                'Por este caso ingresa cuando es un alta de Siniestro
                lista = IIf(Not Session("lsDocumentos") Is Nothing, DirectCast(Session("lsDocumentos"), ArrayList), New ArrayList)
            End If
        End If

        Dim sExt As String = String.Empty
        Dim sName As String = String.Empty
        Dim sIDSiniestroDoc As Int32 = Int32.Parse(hfIDSiniestroDoc.Value)
        Dim RutaImgServer As String = System.Configuration.ConfigurationManager.AppSettings("DirDocumento").ToString
        'Dim sIDUsuario As String = Membership.GetUser().UserName
        Try
            If fuDocumento.HasFile Then
                If Not IO.Directory.Exists(RutaImgServer) Then
                    IO.Directory.CreateDirectory(RutaImgServer)
                End If

                sName = fuDocumento.FileName
                sExt = Path.GetExtension(sName)

                If hfIDSiniestroDoc.Value = "-1" Then
                    sName = "#" + hNRandom.Value & "_" & sName
                Else
                    sName = sIDSiniestroDoc.ToString & "_" & sName
                End If

                If ValidaExtension(sExt) Then
                    If Not ExisteDocumento(RutaImgServer, sName) Then

                        Dim oBE As New BEArchivo
                        Dim oBL As New BLArchivo
                        oBE.IdSiniestro = sIDSiniestroDoc
                        oBE.UsuarioCreacion = Session("IDUsuario")
                        oBE.FechaIngreso = DateTime.Now
                        oBE.FechaCreacion = CDate(txtFechaEntrega.Text + " " + DateTime.Now.ToShortTimeString)
                        oBE.NombreArchivo = sName
                        oBE.RutaArchivo = RutaImgServer
                        If hfIDSiniestroDoc.Value = "-1" Then
                            oBE.IdArchivo = lista.Count + 1
                            lista.Add(oBE)
                            Session("lsDocumentos") = lista
                        End If
                        fuDocumento.SaveAs(RutaImgServer & sName)
                        If hfIDSiniestroDoc.Value <> "-1" Then
                            oBL.Insertar(oBE)
                        End If
                        ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "act", "parent.ActgvDocumento();", True)
                    Else
                        lblMensaje.Text = "Existe un documento en el servidor con el mismo nombre de Archivo."
                    End If
                Else
                    lblMensaje.Text = "El archivo seleccionado no es válido."
                End If
            Else
                lblMensaje.Text = "Seleccione el documento que desea subir."
            End If
        Catch ex As Exception
            lblMensaje.Text = "Ocurrió un error al cargar el documento"
        End Try

    End Sub

    Private Function ValidaExtension(ByVal sExtension As String) As Boolean
        'Case ".jpg", ".jpeg", ".pdf", ".doc", ".docx", "png"
        Select Case sExtension.ToLower
            Case ".exe", ".dll", ".sql", ".reg", ".msi", ".bat", ".js", ".com", ".pif", ".src", ".job"
                Return False
            Case Else
                Return True
        End Select
    End Function

    Private Function ExisteDocumento(ByVal rutaServer As String, ByVal doc As String) As Boolean
        Dim dirServ As DirectoryInfo = New DirectoryInfo(rutaServer)
        Dim files As FileInfo()
        Dim found As Boolean
        files = dirServ.GetFiles()
        found = False
        Try
            For Each file As FileInfo In files 'Buscamos el archivo
                If (file.Name = doc) Then
                    If CInt(hrowCount.Value) = 0 Then
                        IO.File.Copy(rutaServer + "\" + doc, rutaServer + "\" + "#bk_" + Date.Now.ToShortDateString.Replace("/", ".") + Date.Now.ToLongTimeString.Substring(0, 8).Replace(":", "") + "_" + doc)
                        found = False
                    Else
                        found = True
                    End If

                    Exit For
                End If
            Next
            Return found
        Catch ex As Exception
            Return False
        End Try

        Return False
    End Function
End Class