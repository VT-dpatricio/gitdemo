﻿Imports VAN.Siniestros.BE
Imports VAN.Siniestros.BL


Public Class SiniestroSeguimiento
    Inherits PaginaBase




#Region "Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'ddlBuscarPor.SelectedValue = "ApeNom"
            'txtPar1.Text = "ALBERCA"
            If ValidarEstadoClaveUsuario() Then
                Response.Redirect(Constantes.CHANGE_PSW_PAGE, False)
            End If

            ListarEstadoSiniestroF()
            cargarEntidad()
            ''BuscarSiniestro()

            Dim op As String = Request.QueryString("op")
            If op = "Eval" Then
                hfOperacion.Value = "Eval"
                pFiltroEval.Visible = True
                pFiltroSeg.Visible = False
                ddlEntidad.AutoPostBack = True
            Else
                hfOperacion.Value = "Seg"
                pFiltroEval.Visible = False
                pFiltroSeg.Visible = True
                ddlEntidad.AutoPostBack = False
            End If

            'ScriptManager.RegisterStartupScript(Me, Me.GetType, "TabEnabled", "TabActive(false);", True)

        End If
    End Sub



    Private Sub cargarEntidad()
        Dim oBLEntidad As New BLEntidad
        Dim IDUsuario As String = User.Identity.Name
        ddlEntidad.DataSource = oBLEntidad.Seleccionar(IDUsuario)
        ddlEntidad.DataTextField = "Descripcion"
        ddlEntidad.DataValueField = "IDEntidad"
        ddlEntidad.DataBind()
    End Sub

    Private Sub ListarEstadoSiniestroF()
        Dim oBL As New BLEstadoSiniestro
        ddlEstadoSiniestroF.Items.Add(New ListItem("-Seleccionar-", "0"))
        ddlEstadoSiniestroF.DataSource = oBL.Listar
        ddlEstadoSiniestroF.DataValueField = "IDEstadoSiniestro"
        ddlEstadoSiniestroF.DataTextField = "Nombre"
        ddlEstadoSiniestroF.DataBind()
    End Sub

#End Region

#Region "Búsqueda de Siniestro"

    Protected Sub ddlEstadoSiniestroF_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlEstadoSiniestroF.SelectedIndexChanged
        BuscarSiniestro()
    End Sub

    Protected Sub ddlEntidad_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlEntidad.SelectedIndexChanged
        BuscarSiniestro()
    End Sub

    Protected Sub ddlBuscarPor_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlBuscarPor.SelectedIndexChanged
        Select Case ddlBuscarPor.SelectedValue
            Case "ApeNom"
                lblPar1.Text = "Apellidos:"
                lblPar2.Visible = True
                txtPar2.Visible = True
            Case Else
                lblPar2.Visible = False
                txtPar2.Visible = False
                lblPar1.Text = ddlBuscarPor.SelectedItem.Text & ":"
        End Select
    End Sub

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click
        BuscarSiniestro()
    End Sub

    Private Sub BuscarSiniestro()
        Dim oBL As New BLSiniestro
        Dim IDUsuario As String = User.Identity.Name
        gvListaSiniestro.DataSource = oBL.Buscar(CInt(ddlEntidad.SelectedValue), IDUsuario, ddlBuscarPor.SelectedValue, txtPar1.Text.Trim, txtPar2.Text.Trim, ddlEstadoSiniestroF.SelectedValue)
        gvListaSiniestro.DataBind()
        gvListaSiniestro.SelectedIndex = -1
    End Sub

    Protected Sub gvListaSiniestro_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListaSiniestro.PageIndexChanging
        gvListaSiniestro.PageIndex = e.NewPageIndex
        BuscarSiniestro()
    End Sub

    Protected Sub gvListaSiniestro_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gvListaSiniestro.SelectedIndexChanged
        ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "TabActDS", "ActTabSiniestro();", True)
    End Sub

    Protected Sub btnActInfoAdicional_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActInfoAdicional.Click
        LlenarGridInfoAdiciona()
    End Sub

    Private Sub LlenarGridInfoAdiciona()
        Dim oBL As New BLInfoSiniestroC

        gvInfoSiniestroC.DataSource = oBL.SeleccionarAll(gvListaSiniestro.SelectedDataKey.Item(0).ToString)
        gvInfoSiniestroC.DataBind()
    End Sub

#End Region

#Region "Datos del Siniestro"

#Region "Principal"
    Protected Sub btnActSiniestro_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnActSiniestro.Click
        CargarSiniestro()
    End Sub


    Private Sub CargarInfoAdicional(ByVal pIDSiniestro As Int32)
        Try
            Dim oBL As New BLInfoSiniestroC
            Dim pIDSiniestro1 As Int32 = CInt(gvListaSiniestro.SelectedDataKey.Item(0))
            gvInfoSiniestroC.DataSource = oBL.SeleccionarAll(pIDSiniestro1)
            gvInfoSiniestroC.DataBind()
        Catch ex As Exception
            RegisterLog(ex)
        End Try

    End Sub

    Private Sub CargarSiniestro()
        Try
            Dim oBL As New BLCertificado
            Dim pIDSiniestro As Int32 = CInt(gvListaSiniestro.SelectedDataKey.Item(0))
            hfIDSiniestro.Value = pIDSiniestro
            Dim oBE As BECertificado = oBL.Seleccionar(gvListaSiniestro.SelectedDataKey.Item(1).ToString)
            cargarMoneda(oBE.IdProducto)
            'hfIDSiniestro.Value = oBE.IdCertificado
            hfIDProducto.Value = oBE.IdProducto
            lblNroCertificado.Text = oBE.IdCertificado
            lblProducto.Text = oBE.Producto
            lblCanal.Text = oBE.Oficina
            lblTipoDocumento.Text = oBE.TipoDocumento
            lblNroDocumento.Text = oBE.CcCliente
            lblAsegurado.Text = oBE.Nombre1 & " " & oBE.Nombre2 & " " & oBE.Apellido1 & " " & oBE.Apellido2
            lblCanal.Text = oBE.Entidad
            lblAseguradora.Text = oBE.Aseguradora
            'Datos del Siniestro
            Dim oBLSin As New BLSiniestro
            Dim oBESin As BESiniestro = oBLSin.Seleccionar(pIDSiniestro)
            'txtNroTicket.Text = oBESin.IDSiniestro.ToString("000000")
            txtNroTicket.Text = oBESin.FechaCreacion.ToString("yyyyMMdd") & "-" & oBESin.IDSiniestro.ToString("000000")
            'hfIDSiniestro.Value = oBESin.IDSiniestro

            txtTipoSiniestro.Text = oBESin.TipoSiniestro
            txtFechaSiniestro.Text = oBESin.FechaSiniestro
            txtMontoDenunciado.Text = oBESin.MontoSiniestro
            If oBESin.IDMonedaMSiniestro <> "0" Then
                ddlMonedaDe.SelectedValue = oBESin.IDMonedaMSiniestro
            End If

            txtNroSiniestro.Text = oBESin.NroSiniestroAseguradora
            txtMontoPago.Text = oBESin.MontoPago.ToString()
            ddlMonedaPago.SelectedValue = oBESin.IDMonedaMPago

            txtMontoPagoAse.Text = oBESin.MontoPagoAse.ToString()
            ddlMonedaPagoAse.SelectedValue = oBESin.IDMonedaMPagoAse


            txtEstado.Text = oBESin.EstadoSiniestro
            txtAseDireccion.Text = oBESin.AseDireccion
            txtAseTelefono.Text = oBESin.AseTelefono
            txtAseEmail.Text = oBESin.AseEmail
            txtAseObservacion.Text = oBESin.AseObservacion

            lblOpcion.Text = oBE.Opcion
            lblMonedaPrima.Text = oBE.IdMonedaPrima
            lblEstadoCer.Text = oBE.Estado

            If (oBESin.IDEstadoSiniestro = 2) And hfOperacion.Value = "Eval" Then
                btnEvaluar.Visible = True
            Else
                btnEvaluar.Visible = False
            End If

            If ((oBESin.IDEstadoSiniestro <> 3) And (oBESin.IDEstadoSiniestro <> 4)) And hfOperacion.Value = "Eval" Then
                btnRechazar.Visible = True
            Else
                btnRechazar.Visible = False
            End If

            If (oBESin.IDEstadoSiniestro = 1 Or oBESin.IDEstadoSiniestro = 2) Then
                EstadoControles(True)
                'btnGrabar.Visible = True
            Else
                EstadoControles(False)
                'btnGrabar.Visible = False
            End If

            'Si el estado es rechazado
            If (oBESin.IDEstadoSiniestro = 4) Then
                txtMontoPagoAse.Enabled = False
                ddlMonedaPagoAse.Enabled = False
                btnGrabar.Visible = False
            Else
                btnGrabar.Visible = True
            End If

            'Coberturas
            Dim oBLSinCober As New BLSiniestroCobertura
            gvCobertura.DataSource = Nothing
            gvCobertura.DataBind()
            gvCobertura.DataSource = oBLSinCober.Listar(pIDSiniestro)
            gvCobertura.DataBind()

            'oDS.Select()
            'oDS.DataBind()
            gvDocumento0.DataBind()

            'cargarDocumento(pIDSiniestro)
            cargarLlamada(pIDSiniestro)
            CargarInfoAdicional(pIDSiniestro)
            CargarGrillaArchivos(pIDSiniestro)

            For i = 0 To gvDocumento0.Rows.Count - 1
                DirectCast(Me.gvDocumento0.Rows.Item(i).Cells.Item(5).Controls.Item(1), ImageButton).Enabled = DirectCast(gvDocumento0.Rows.Item(i).Cells(4).Controls.Item(1), CheckBox).Checked
                'If (DirectCast(Me.gvDocumento0.Rows.Item(i).Cells.Item(3).Controls.Item(1), Label).Text.Trim = "" OrElse DirectCast(Me.gvDocumento0.Rows.Item(i).Cells.Item(3).Controls.Item(1), Label).Text.Trim = String.Empty) Then
                '    DirectCast(gvDocumento0.Rows.Item(i).Cells(4).Controls.Item(1), CheckBox).Enabled = False
                'End If
            Next

        Catch ex As Exception
            RegisterLog(ex)
        End Try

    End Sub

    Protected Function FormatDate(ByVal input As DateTime) As Object
        If (input = New DateTime(1900, 1, 1)) Then
            Return ""
        Else
            Return String.Format("{0:dd/MM/yyyy}", input)
        End If
    End Function

    Protected Sub IBGrabar_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Try
            Dim sinDoc As New BLSiniestroDocumento
            Dim pEndtidad As New BESiniestroDocumento

            If (DirectCast(Me.gvDocumento0.Rows.Item(Me.gvDocumento0.EditIndex).Cells.Item(4).Controls.Item(1), CheckBox).Checked) Then
                pEndtidad.IDSiniestro = CInt(gvListaSiniestro.SelectedDataKey.Item(0))
                'pEndtidad.Adjunto = DirectCast(Me.gvDocumento0.Rows.Item(Me.gvDocumento0.EditIndex).Cells.Item(1).Controls.Item(3), TextBox).Text.Trim
                If Not String.IsNullOrWhiteSpace(DirectCast(Me.gvDocumento0.Rows.Item(Me.gvDocumento0.EditIndex).Cells.Item(3).Controls.Item(1), TextBox).Text.Trim) Then
                    pEndtidad.FechaEntrega = Convert.ToDateTime(DirectCast(Me.gvDocumento0.Rows.Item(Me.gvDocumento0.EditIndex).Cells.Item(3).Controls.Item(1), TextBox).Text.Trim)
                Else
                    pEndtidad.FechaEntrega = Convert.ToDateTime(Date.Now.ToShortDateString)
                End If
                pEndtidad.IDSiniestroDoc = Convert.ToInt32(DirectCast(Me.gvDocumento0.Rows.Item(Me.gvDocumento0.EditIndex).Cells.Item(5).Controls.Item(1), ImageButton).CommandArgument)
                pEndtidad.Entregado = True
                pEndtidad.UsuarioModificacion = Session("IDUsuario")
                pEndtidad.Adjunto = ""

                sinDoc.Actualizar(pEndtidad)
                Me.gvDocumento0.EditIndex = -1
                Me.gvDocumento0.DataBind()
                'cargarDocumento(gvListaSiniestro.SelectedDataKey.Item(0))
                oDS.Select()
                oDS.DataBind()
                gvDocumento0.DataBind()
            End If
        Catch ex As Exception
            RegisterLog(ex)
        End Try
    End Sub

    Private Sub SaveData(ByRef oESinDoc As BESiniestroDocumento)
        Dim sinDoc As New BLSiniestroDocumento
        Dim pEndtidad As New BESiniestroDocumento

        sinDoc.Actualizar(oESinDoc)
    End Sub


    Protected Sub IBCancelar_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs)


    End Sub



    Protected Sub btnEliminar_Command(ByVal sender As Object, ByVal e As CommandEventArgs)
        'Guarda el registro eliminado de la grilla de Archivos
        Dim oArc As New BLArchivo
        Dim pIDArchivo As Integer = Convert.ToInt32(e.CommandArgument)
        oArc.Eliminar(pIDArchivo, Session("IDUsuario"))
        Me.gvRegDoc.SelectedIndex = -1
        Me.gvRegDoc.EditIndex = -1
        CargarGrillaArchivos(CInt(hfIDSiniestro.Value))
        ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "actArf", "ActElimArchivo();", True)
    End Sub


    Protected Sub gvRegDoc_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvRegDoc.RowDeleting

    End Sub

    Private Sub CargarGrillaArchivos(ByVal pIDSiniestro As String)
        Dim activeCell As Boolean = False
        Dim oBLArchivo As New BLArchivo
        gvRegDoc.DataSource = oBLArchivo.Seleccionar(pIDSiniestro)
        gvRegDoc.DataBind()


        If gvRegDoc.Rows.Count > 0 Then
            activeCell = True
        Else
            gvDocumento0.DataBind()
        End If

        For i = 0 To gvDocumento0.Rows.Count - 1
            'If gvRegDoc.Rows.Count = 0 Then
            '    DirectCast(gvDocumento0.Rows.Item(i).Cells(4).Controls.Item(1), CheckBox).Checked = False
            'End If
            DirectCast(gvDocumento0.Rows.Item(i).Cells(4).Controls.Item(1), CheckBox).Enabled = activeCell
            DirectCast(Me.gvDocumento0.Rows.Item(i).Cells.Item(5).Controls.Item(1), ImageButton).Enabled = activeCell
        Next

    End Sub


    Private Sub EstadoControles(ByVal pEstado As Boolean)
        txtFechaSiniestro.Enabled = pEstado
        txtMontoDenunciado.Enabled = pEstado
        ddlMonedaDe.Enabled = pEstado
        txtMontoPago.Enabled = pEstado
        ddlMonedaPago.Enabled = pEstado
        txtMontoPagoAse.Enabled = Not pEstado
        ddlMonedaPagoAse.Enabled = Not pEstado
        txtAseDireccion.Enabled = pEstado
        txtAseEmail.Enabled = pEstado
        txtAseObservacion.Enabled = pEstado
        txtAseTelefono.Enabled = pEstado
        gvDocumento0.Columns(5).Visible = pEstado
        gvLLamada.Columns(3).Visible = pEstado

    End Sub

    Private Sub cargarDocumento(ByVal pIDSiniestro As Int32)
        Dim oBLSinDoc As New BLSiniestroDocumento

        gvDocumento0.DataSource = oBLSinDoc.Listar(pIDSiniestro)
        gvDocumento0.DataBind()
        gvDocumento0.SelectedIndex = -1
    End Sub

    Private Sub cargarLlamada(ByVal pIDSiniestro As Int32)
        Dim oBLSin As New BLSiniestroLlamada
        gvLLamada.DataSource = oBLSin.Listar(pIDSiniestro)
        gvLLamada.DataBind()
    End Sub

    Private Sub cargarMoneda(ByVal pIDProducto As Int32)
        Dim oBL As New BLMoneda
        Dim ListaMoneda As IList = oBL.Listar(pIDProducto)
        ddlMonedaDe.DataSource = ListaMoneda
        ddlMonedaDe.DataValueField = "IDMoneda"
        ddlMonedaDe.DataTextField = "IDMoneda"
        ddlMonedaDe.DataBind()

        ddlMonedaPago.Items.Clear()
        ddlMonedaPago.Items.Add(New ListItem("-Seleccionar-", "0"))
        ddlMonedaPago.DataSource = ListaMoneda
        ddlMonedaPago.DataValueField = "IDMoneda"
        ddlMonedaPago.DataTextField = "IDMoneda"
        ddlMonedaPago.DataBind()

        ddlMonedaPagoAse.Items.Clear()
        ddlMonedaPagoAse.Items.Add(New ListItem("-Seleccionar-", "0"))
        ddlMonedaPagoAse.DataSource = ListaMoneda
        ddlMonedaPagoAse.DataValueField = "IDMoneda"
        ddlMonedaPagoAse.DataTextField = "IDMoneda"
        ddlMonedaPagoAse.DataBind()

    End Sub

    Protected Sub btnGrabar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGrabar.Click
        Dim oBL As New BLSiniestro
        Dim oBE As New BESiniestro
        oBE.IDSiniestro = hfIDSiniestro.Value
        oBE.FechaSiniestro = CDate(txtFechaSiniestro.Text)
        oBE.MontoSiniestro = CDec(txtMontoDenunciado.Text)
        oBE.IDMonedaMSiniestro = ddlMonedaDe.SelectedValue

        If txtMontoPago.Text = "" Then
            oBE.MontoPago = -1
        Else
            oBE.MontoPago = CDec(txtMontoPago.Text)
        End If
        oBE.IDMonedaMPago = ddlMonedaPago.SelectedValue

        If txtMontoPagoAse.Text = "" Then
            oBE.MontoPagoAse = -1
        Else
            oBE.MontoPagoAse = CDec(txtMontoPagoAse.Text)
        End If
        oBE.IDMonedaMPagoAse = ddlMonedaPagoAse.SelectedValue

        oBE.AseEmail = txtAseEmail.Text.Trim
        oBE.AseTelefono = txtAseTelefono.Text.Trim
        oBE.AseDireccion = txtAseDireccion.Text.Trim
        oBE.AseObservacion = txtAseObservacion.Text.Trim
        oBE.UsuarioModificacion = User.Identity.Name
        If oBL.Actualizar(oBE) Then
            ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "MsgError", "alert('Siniestro actualizado con éxito.');", True)
        Else
            ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "MsgError", "alert('Ocurrió un error al grabar. Consulte con el Administrador del Sistema');", True)
        End If
    End Sub
#End Region

#Region "Documentos"

    Protected Sub btnActgvDocumento_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnActgvDocumento.Click

    End Sub

    Protected Sub btn_RfArchivo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_RfArchivo.Click
        CargarGrillaArchivos(CInt(hfIDSiniestro.Value))
    End Sub

#End Region

    Protected Sub btnRegistraArchivo_Click1(ByVal sender As Object, ByVal e As EventArgs)
        RegLlamada()
    End Sub

#Region "LLamadas"
    Private Sub RegLlamada()
        ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "RegLla", "RegLlamada(" & hfIDSiniestro.Value & ");", True)
    End Sub

    Protected Sub btnActgvLLamada_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnActgvLLamada.Click
        cargarLlamada(CInt(hfIDSiniestro.Value))
    End Sub

    Protected Sub btnRegistrarLlamada_Click1(ByVal sender As Object, ByVal e As EventArgs)
        RegLlamada()
    End Sub

    Protected Sub ibtnReglla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        RegLlamada()
    End Sub
#End Region

#Region "Evaluación"
    Protected Sub btnEvaluar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEvaluar.Click
        pResulEval.Visible = False
        btnValidar.Visible = True
        txtEstadoEval.Text = ""
        txtObservacionEval.Text = ""
        gvEvaluacion.Columns(4).Visible = False


        Dim oBLEval As New BLEvaluacion
        gvEvaluacion.DataSource = oBLEval.Listar(CInt(hfIDSiniestro.Value), CInt(hfIDProducto.Value))
        gvEvaluacion.DataBind()
        gvEvaluacion.Visible = True
        ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "pEvaluar", "frmEvaluar();", True)
    End Sub

    Protected Sub btnValidar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnValidar.Click
        'Validar Selección
        Dim Sel As Int32 = 0
        For i As Integer = 0 To gvEvaluacion.Rows.Count - 1
            Sel = DirectCast(gvEvaluacion.Rows.Item(i).Cells.Item(2).Controls.Item(1), RadioButtonList).SelectedIndex
            If Sel = -1 Then
                ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "MsgError", "alert('Por favor responda toda las preguntas');", True)
                Exit Sub
            End If
        Next

        'Validar Respuestas
        Dim RptCorrecta As String = ""
        Dim RptEstadoEval As Boolean = True
        gvEvaluacion.Columns(4).Visible = True
        For i As Integer = 0 To gvEvaluacion.Rows.Count - 1
            DirectCast(gvEvaluacion.Rows.Item(i).Cells.Item(2).Controls.Item(1), RadioButtonList).Enabled = False
            Sel = DirectCast(gvEvaluacion.Rows.Item(i).Cells.Item(2).Controls.Item(1), RadioButtonList).SelectedValue
            RptCorrecta = DirectCast(gvEvaluacion.Rows.Item(i).Cells.Item(3).Controls.Item(1), Label).Text
            RptCorrecta = IIf(RptCorrecta = "True", "1", "0")
            If Sel = RptCorrecta Then
                DirectCast(gvEvaluacion.Rows.Item(i).Cells.Item(4).Controls.Item(1), Label).Text = "Correcto"
                DirectCast(gvEvaluacion.Rows.Item(i).Cells.Item(4).Controls.Item(1), Label).ForeColor = Drawing.Color.Green
            Else
                DirectCast(gvEvaluacion.Rows.Item(i).Cells.Item(4).Controls.Item(1), Label).Text = "Incorrecto"
                DirectCast(gvEvaluacion.Rows.Item(i).Cells.Item(4).Controls.Item(1), Label).ForeColor = Drawing.Color.Red
                RptEstadoEval = False
            End If
        Next

        If RptEstadoEval Then
            txtEstadoEval.Text = "Aprobado"
        Else
            txtEstadoEval.Text = "Rechazado"
        End If
        btnValidar.Visible = False
        pResulEval.Visible = True

    End Sub

    Protected Sub btnGrabarEval_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGrabarEval.Click
        Dim oBLEval As New BLSiniestroEvaluacion
        Dim oBEEval As BESiniestroEvaluacion
        Dim Sel As String = "0"
        For i As Integer = 0 To gvEvaluacion.Rows.Count - 1
            DirectCast(gvEvaluacion.Rows.Item(i).Cells.Item(2).Controls.Item(1), RadioButtonList).Enabled = False
            Sel = DirectCast(gvEvaluacion.Rows.Item(i).Cells.Item(2).Controls.Item(1), RadioButtonList).SelectedValue
            oBEEval = New BESiniestroEvaluacion
            oBEEval.IDSiniestro = CInt(hfIDSiniestro.Value)
            oBEEval.IDPregunta = CInt(gvEvaluacion.DataKeys(i).Item(0))
            oBEEval.Respuesta = IIf(Sel = "1", True, False)
            oBLEval.Insertar(oBEEval)
        Next

        Dim oBLSin As New BLSiniestro
        Dim oBESin As New BESiniestro
        oBESin.IDSiniestro = CInt(hfIDSiniestro.Value)
        oBESin.IDEstadoSiniestro = IIf(txtEstadoEval.Text = "Aprobado", 3, 4)
        oBESin.ObservacionEvaluacion = txtObservacionEval.Text
        oBESin.UsuarioModificacion = User.Identity.Name
        If (oBLSin.Evaluacion(oBESin)) Then
            CargarSiniestro()
            'ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "ActPE", "ActpostEvaluacion();", True)
            ' ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "ActPE", "ActpostEvaluacion('" & txtEstadoEval.Text & "');", True)
        Else
            ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "MsgError", "alert('Error al grabar');", True)
        End If


    End Sub

    Protected Sub btnRechazar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRechazar.Click
        pResulEval.Visible = True
        btnValidar.Visible = False
        txtEstadoEval.Text = "Rechazado"
        txtObservacionEval.Text = ""
        gvEvaluacion.Visible = False
        gvEvaluacion.DataSource = Nothing
        gvEvaluacion.DataBind()
        ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "pEvaluar", "frmEvaluar();", True)
    End Sub
#End Region

#End Region

    Public Sub EstadoControles()
        Me.gvDocumento0.EditIndex = -1
        Me.gvDocumento0.SelectedIndex = -1
    End Sub

    Protected Sub gvDocumento_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvDocumento0.RowEditing
        'Dim pIDSiniestro As Int32 = CInt(gvListaSiniestro.SelectedDataKey.Item(0))
        'cargarDocumento(pIDSiniestro)

        Me.gvDocumento0.SelectedIndex = -1
    End Sub

    Protected Sub oDS_Selecting(sender As Object, e As ObjectDataSourceSelectingEventArgs) Handles oDS.Selecting
        Try
            Dim pIDSiniestro As Int32 = 0

            If Not gvListaSiniestro.SelectedDataKey Is Nothing Then
                pIDSiniestro = CInt(gvListaSiniestro.SelectedDataKey.Item(0))
            End If
            e.InputParameters("pIDSiniestro") = pIDSiniestro
            oDS.DataBind()
        Catch ex As Exception
            RegisterLog(ex)
        End Try
    End Sub

    Protected Sub gvDocumento0_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvDocumento0.RowDataBound
        Dim ctrlType As String = String.Empty
        Dim i As Integer = 0
        Dim activeCell = False

        If (e.Row.RowType = DataControlRowType.DataRow) Then
            If gvRegDoc.Rows.Count > 0 Then
                activeCell = True
            End If

            If (e.Row.RowState = 4 Or e.Row.RowState = 5) Then
                DirectCast(e.Row.Cells(4).Controls.Item(1), CheckBox).Enabled = False
                Exit Sub
            End If

            'For i = 0 To gvDocumento0.Columns.Count - 1
            Dim dk As DataKey = gvDocumento0.DataKeys(e.Row.DataItemIndex)
            DirectCast(e.Row.Cells(4).Controls.Item(1), CheckBox).Attributes.Add("onclick", "ActCheck(" + (e.Row.RowIndex + 1).ToString() + ")")
            DirectCast(e.Row.Cells(4).Controls.Item(1), CheckBox).Enabled = activeCell
            DirectCast(e.Row.Cells(5).Controls.Item(1), ImageButton).Enabled = activeCell
            'e.Row.Cells(i).Attributes.Add("onclick", "EditCell('" + dk.Values("IDSiniestroDoc").ToString() + "','" + e.Row.Cells(i).Text + "','" + ctrlType + "','" + (e.Row.RowIndex + 1).ToString() + "','" + i.ToString() + "');")
            'Next
        End If
    End Sub

    Protected Sub btn_AllDoc_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_AllDoc.Click
        btn_refresDoc_Click(sender, e)
    End Sub

    Protected Sub btn_refresDoc_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_refresDoc.Click
        Try
            Dim id As Integer = CInt(IIf(String.IsNullOrEmpty(idDocSin.Value), 0, idDocSin.Value))
            Dim pEndtidad As New BESiniestroDocumento
            Dim fecha As DateTime

            fecha = Date.Now
            id = id - 1
            For i = 0 To gvDocumento0.Rows.Count - 1
                If gvRegDoc.Rows.Count = 0 Then
                    DirectCast(gvDocumento0.Rows.Item(i).Cells(4).Controls.Item(1), CheckBox).Checked = False
                End If
            Next
            If id < 0 Then
                Exit Sub
            End If

            If (gvDocumento0.Rows.Item(id).RowState = 5) Then
                DirectCast(gvDocumento0.Rows.Item(id).Cells(4).Controls.Item(1), CheckBox).Enabled = False
                Me.gvDocumento0.EditIndex = -1
                Me.gvDocumento0.SelectedIndex = -1
                gvDocumento0.DataBind()
                Exit Sub
            End If

            'Deshabilito o habilito la edicion dependiendo si esta chekeado o no el registro
            DirectCast(Me.gvDocumento0.Rows.Item(id).Cells.Item(5).Controls.Item(1), ImageButton).Enabled = DirectCast(gvDocumento0.Rows.Item(id).Cells(4).Controls.Item(1), CheckBox).Checked
            pEndtidad.FechaEntrega = fecha
            pEndtidad.Entregado = DirectCast(gvDocumento0.Rows.Item(id).Cells(4).Controls.Item(1), CheckBox).Checked
            pEndtidad.IDSiniestro = CInt(gvListaSiniestro.SelectedDataKey.Item(0))
            pEndtidad.UsuarioModificacion = Session("IDUsuario")
            pEndtidad.IDSiniestroDoc = Convert.ToInt32(DirectCast(Me.gvDocumento0.Rows.Item(id).Cells.Item(5).Controls.Item(1), ImageButton).CommandArgument)
            pEndtidad.Adjunto = ""
            SaveData(pEndtidad)
            gvDocumento0.DataBind()
        Catch ex As Exception
            RegisterLog(ex)
        End Try
    End Sub

    Private Sub RegisterLog(ByVal ex As Exception)

    End Sub

    'Protected Sub gvDocumento0_RowCommand(sender As Object, e As GridViewCommandEventArgs)
    '    If (e.CommandName = "Edit") Then
    '        Dim row As GridViewRow = DirectCast((DirectCast(e.CommandSource, Control)).NamingContainer, GridViewRow)
    '        'Me.gvDocumento0.EditIndex = -1
    '        'Me.gvDocumento0.SelectedIndex = -1
    '        'gvDocumento0.DataBind()

    '        DirectCast(gvDocumento0.Rows.Item(row.RowIndex).Cells(4).Controls.Item(1), CheckBox).Enabled = False
    '    End If
    'End Sub
End Class