﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pAdjuntarDoc.aspx.vb" Inherits="VANSiniestros.AdjuntarDoc" culture="es-PE" uiCulture="es-PE"%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="~/Controles/CargandoDiv.ascx" tagname="CargandoDiv" tagprefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 78px;
        }
        .style2
        {
            height: 19px;
            width: 78px;
        }
    </style>
      <script type="text/javascript">
          function Cargando() {
              if (typeof (Page_ClientValidate) == 'function') {
                  //Page_ClientValidate();
                  var isPageValid = Page_ClientValidate();
                  if (isPageValid) {
                      document.getElementById('frmRegistro').style.display = 'none';
                      document.getElementById('Loading').style.display = 'block';
                  }
              }
              
              
              
          }
    </script>
</head>
<body>
    <form id="frmAdDoc" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

<div id="frmRegistro">
<asp:Panel ID="pSubirImagen" runat="server" Width="631px">

    
    <table cellpadding="3" style="width: 631px" class="TablaMarco">
        <tr>
            <td class="TablaTitulo" colspan="5">
                Adjuntar Documento<asp:HiddenField ID="hfIDSiniestroDoc" runat="server" 
                    Value="0" />
                <asp:HiddenField ID="hNRandom" runat="server" 
                    Value="0" />
                 <asp:HiddenField ID="hrowCount" runat="server" 
                    Value="0" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                    ShowMessageBox="True" ShowSummary="False" ValidationGroup="addoc" />
            </td>
        </tr>
        <tr>
            <td class="style1" style="width: 93px">
                Fecha Ingreso:<asp:RequiredFieldValidator ID="RequiredFieldValidator2" 
                    runat="server" ControlToValidate="txtFechaEntrega" 
                    ErrorMessage="Ingrese la Fecha de Entrega" ForeColor="Red" 
                    ValidationGroup="addoc" Display="Dynamic">*</asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator1" runat="server" 
                    ControlToValidate="txtFechaEntrega" Display="Dynamic" 
                    ErrorMessage="Ingrese una Fecha de Entrega válida" ForeColor="Red" 
                    Operator="DataTypeCheck" Type="Date" ValidationGroup="addoc">*</asp:CompareValidator>
    <asp:MaskedEditExtender ID="me" runat="server" ClearTextOnInvalid="True" 
                                                                Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaEntrega">
                                                            </asp:MaskedEditExtender>

            </td>
            <td style="width: 74px">
                <asp:TextBox ID="txtFechaEntrega" runat="server" Width="65px" MaxLength="10" ReadOnly="True"></asp:TextBox>


            </td>
            <td style="width: 42px">
                Documento:</td>
            <td style="width: 275px">
                <asp:FileUpload ID="fuDocumento" runat="server" Width="273px" />
                </td>
            <td>
                <asp:Button ID="btnCargar" runat="server" Text="Grabar"
                    Width="84px" ValidationGroup="addoc" OnClientClick="Cargando()" 
                    CausesValidation="False"/></td>
        </tr>
        <tr>
            <td class="style2" style="width: 93px">
            </td>
            <td style="width: 74px; height: 19px;">
                &nbsp;</td>
            <td style="height: 19px;" colspan="3">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    </asp:Panel>
  </div>  

  <div id="Loading" style="margin-top: 20px;margin-left:230px; display:none">
       
        
        <uc1:CargandoDiv ID="CargandoDiv1" runat="server" />
    </div>
    
    
    </form>
</body>
</html>
