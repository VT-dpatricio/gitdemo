﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pRegLlamada.aspx.vb" Inherits="VANSiniestros.RegLlamada" culture="es-PE" uiCulture="es-PE"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
       
        <table style="width:486px;" class="TablaMarco">
            <tr>
                <td class="TablaTitulo" colspan="4">
                    Registro de Llamada<asp:HiddenField ID="hfIDSiniestro" runat="server" 
                    Value="0" />
                </td>
            </tr>
            <tr>
                <td style="width: 79px">
                    Fecha<asp:RequiredFieldValidator ID="RequiredFieldValidator2" 
                    runat="server" ControlToValidate="txtFecha" 
                    ErrorMessage="Ingrese la Fecha" ForeColor="Red" 
                    ValidationGroup="adL" Display="Dynamic">*</asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator1" runat="server" 
                    ControlToValidate="txtFecha" Display="Dynamic" 
                    ErrorMessage="Ingrese una Fecha válida" ForeColor="Red" 
                    Operator="DataTypeCheck" Type="Date" ValidationGroup="adL">*</asp:CompareValidator>
                    <asp:MaskedEditExtender ID="me" runat="server" ClearTextOnInvalid="True" 
                                                                Mask="99/99/9999" MaskType="Date" TargetControlID="txtFecha">
                                                            </asp:MaskedEditExtender>
                </td>
                <td style="width: 51px">
                    Hora
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" 
                    runat="server" ControlToValidate="txtHora" 
                    ErrorMessage="Ingrese la Hora" ForeColor="Red" 
                    ValidationGroup="adL" Display="Dynamic">*</asp:RequiredFieldValidator>
                      <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" ClearTextOnInvalid="True" 
                                                                Mask="99:99" MaskType="Time" 
                        TargetControlID="txtHora">
                                                            </asp:MaskedEditExtender>
                </td>
                <td style="width: 43px">
                    &nbsp;</td>
                <td>
                    Observaciones</td>
            </tr>
            <tr>
                <td style="width: 79px; vertical-align: top;">
                    <asp:TextBox ID="txtFecha" runat="server" Width="60px" MaxLength="10"></asp:TextBox>
                </td>
                <td style="width: 51px; vertical-align: top;">
                    <asp:TextBox ID="txtHora" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
                </td>
                <td style="width: 43px; vertical-align: top;">
                    <asp:DropDownList ID="ddlT" runat="server" Width="45px">
                        <asp:ListItem>AM</asp:ListItem>
                        <asp:ListItem>PM</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox ID="txtObservacion" runat="server" Height="53px" 
                        TextMode="MultiLine" Width="298px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 79px">
                    &nbsp;</td>
                <td style="width: 51px">
                    &nbsp;</td>
                <td style="width: 43px">
                    &nbsp;</td>
                <td style="text-align: right">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                    ShowMessageBox="True" ShowSummary="False" ValidationGroup="adL" />
                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                    <asp:Button ID="btnGrabar" runat="server" Text="Grabar" Width="80px" 
                        ValidationGroup="adL" />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
