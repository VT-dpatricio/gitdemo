﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Cargando.ascx.vb" Inherits="VANSiniestros.Cargando" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>



    <asp:Panel ID="PCar" runat="server" Width="117px">
    <table class="CLoading" style="width: 121px">
        <tr>
            <td>
                <asp:Image ID="Image1" runat="server" ImageUrl="~/img/wait.gif" />
                </td>
            <td style="width: 183px">
                Procesando...</td>
        </tr>
    </table>
    </asp:Panel>

<asp:AlwaysVisibleControlExtender ID="ave" runat="server" TargetControlID="PCar" VerticalSide="Middle" HorizontalSide="Center" HorizontalOffset=50>
</asp:AlwaysVisibleControlExtender>
