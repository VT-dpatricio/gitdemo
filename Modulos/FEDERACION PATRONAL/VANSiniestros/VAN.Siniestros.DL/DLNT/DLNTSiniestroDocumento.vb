﻿Imports VAN.Siniestros.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLNTSiniestroDocumento
    Inherits DLBase
    Implements IDLNT


    Public Function Seleccionar() As System.Collections.IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pEntidad As BE.BEBase) As System.Collections.IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As System.Collections.IList Implements IDLNT.Seleccionar
        Dim cn As New SqlConnection(CadenaConexion)
        Dim cm As New SqlCommand("SS_PRC_SEL_SiniestroDocumento", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@IDSiniestro", SqlDbType.Int).Value = pCodigo
        Dim oBE As BESiniestroDocumento
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BESiniestroDocumento
                oBE.IDSiniestroDoc = rd.GetInt32(rd.GetOrdinal("IDSiniestroDoc"))
                oBE.IDDocumento = rd.GetInt32(rd.GetOrdinal("IDDocumento"))
                oBE.Documento = rd.GetString(rd.GetOrdinal("Documento"))
                oBE.Adjunto = rd.GetString(rd.GetOrdinal("Adjunto"))
                oBE.FechaEntrega = Convert.ToDateTime(rd.GetString(rd.GetOrdinal("FechaEntrega")))
                oBE.Entregado = rd.GetBoolean(rd.GetOrdinal("Entregado"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function ListarPorCobertura(ByVal pIdCoberturas As String) As System.Collections.IList
        Dim cn As New SqlConnection(CadenaConexion)
        Dim cm As New SqlCommand("SS_ListarCoberturaDocumento", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@IDCobertura", SqlDbType.VarChar, 1000).Value = pIdCoberturas
        Dim oBE As BESiniestroDocumento
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BESiniestroDocumento
                oBE.IDSiniestroDoc = rd.GetInt32(rd.GetOrdinal("IDSiniestroDoc"))
                oBE.IDDocumento = rd.GetInt32(rd.GetOrdinal("IDDocumento"))
                oBE.Documento = rd.GetString(rd.GetOrdinal("Documento"))
                oBE.Adjunto = rd.GetString(rd.GetOrdinal("Adjunto"))
                oBE.FechaEntrega = Convert.ToDateTime(rd.GetString(rd.GetOrdinal("FechaEntrega")))
                oBE.Entregado = rd.GetBoolean(rd.GetOrdinal("Entregado"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function


    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BE.BEBase Implements IDLNT.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function
End Class
