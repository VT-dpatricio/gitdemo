﻿Imports VAN.Siniestros.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLTSiniestroLlamada
    Inherits DLBase
    Implements IDLT


    Public Function Actualizar(ByVal pEntidad As BE.BEBase) As Boolean Implements IDLT.Actualizar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Eliminar(ByVal pCodigo As Integer) As Boolean Implements IDLT.Eliminar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Insertar(ByVal pEntidad As BE.BEBase) As Boolean Implements IDLT.Insertar
        Dim cn As New SqlConnection(CadenaConexion)
        Dim cm As New SqlCommand("SS_InsertarSiniestroLlamada", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm = Me.LlenarEstructura(pEntidad, cm, "I")
        Dim oBE As BESiniestroLlamada = DirectCast(pEntidad, BESiniestroLlamada)
        Dim resultado As Boolean = False
        Try
            cn.Open()
            cm.ExecuteNonQuery()
            resultado = True
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return resultado
    End Function

    Public Function LlenarEstructura(ByVal pEntidad As BE.BEBase, ByVal pcm As System.Data.SqlClient.SqlCommand, ByVal TipoTransaccion As String) As System.Data.SqlClient.SqlCommand Implements IDLT.LlenarEstructura
        Dim oBE As BESiniestroLlamada = DirectCast(pEntidad, BESiniestroLlamada)
        If (TipoTransaccion = "I") Then
            pcm.Parameters.Add("@IDSiniestro", SqlDbType.Int).Value = oBE.IDSiniestro
            pcm.Parameters.Add("@FechaHora", SqlDbType.DateTime).Value = oBE.FechaHora
            pcm.Parameters.Add("@Adjunto", SqlDbType.VarChar, 250).Value = oBE.Adjunto
            pcm.Parameters.Add("@Observacion", SqlDbType.VarChar, 1000).Value = oBE.Observacion
            pcm.Parameters.Add("@UsuarioCreacion", SqlDbType.VarChar, 50).Value = oBE.UsuarioCreacion
        End If
        Return pcm
    End Function
End Class
