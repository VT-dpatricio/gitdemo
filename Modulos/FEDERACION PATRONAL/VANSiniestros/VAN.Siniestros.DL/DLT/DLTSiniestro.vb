﻿Imports VAN.Siniestros.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLTSiniestro
    Inherits DLBase
    Implements IDLT

    Public Function Evaluacion(ByVal pEntidad As BE.BEBase)
        Dim cn As New SqlConnection(CadenaConexion)
        Dim cm As New SqlCommand("SS_ActualizarSiniestroEval", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm = Me.LlenarEstructura(pEntidad, cm, "EVAL")
        Dim oBE As BESiniestro = DirectCast(pEntidad, BESiniestro)
        Dim resultado As Boolean = False
        Try
            cn.Open()
            cm.ExecuteNonQuery()
            resultado = True
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return resultado
    End Function

    Public Function Actualizar(ByVal pEntidad As BE.BEBase) As Boolean Implements IDLT.Actualizar
        Dim cn As New SqlConnection(CadenaConexion)
        Dim cm As New SqlCommand("SS_ActualizarSiniestro", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm = Me.LlenarEstructura(pEntidad, cm, "A")
        Dim oBE As BESiniestro = DirectCast(pEntidad, BESiniestro)
        Dim resultado As Boolean = False
        Try
            cn.Open()
            cm.ExecuteNonQuery()
            resultado = True
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return resultado
    End Function

    Public Function Eliminar(ByVal pCodigo As Integer) As Boolean Implements IDLT.Eliminar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function InsertarS(ByVal pEntidad As BESiniestro) As Integer
        Dim cn As New SqlConnection(CadenaConexion)
        Dim cm As New SqlCommand("SS_PRC_INS_Siniestro", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm = Me.LlenarEstructura(pEntidad, cm, "I")
        Dim oBE As BESiniestro = DirectCast(pEntidad, BESiniestro)
        Dim resultado As Integer = 0
        Dim sqlTranx As SqlTransaction
        Try
            cn.Open()
            sqlTranx = cn.BeginTransaction("Transaccion")
            cm.Transaction = sqlTranx
            Dim nDA As Integer = cm.ExecuteNonQuery()
            If nDA > 0 Then
                resultado = cm.Parameters("@IDSiniestro").Value()
                'Insertar Conberturas
                Dim IDSiniestroI As Int32
                Dim oDLTSinCober As New DLTSiniestroCobertura
                Dim oBESinCober As BESiniestroCobertura
                For i As Integer = 0 To oBE.ListaCobertura.Count - 1
                    oBESinCober = New BESiniestroCobertura
                    oBESinCober.IDSiniestro = resultado
                    oBESinCober.IDCobertura = CInt(oBE.ListaCobertura(i))
                    'Si se produce un error al insertar
                    If Not oDLTSinCober.Insertar(oBESinCober, cn, sqlTranx) Then
                        resultado = 0
                        Exit For
                    End If
                Next
                '------------------.-

                'Insertar los Documentos Solicitados
                Dim oDLTSinDoc As New DLTSiniestroDocumento
                Dim oBESinDoc As New BESiniestroDocumento
                IDSiniestroI = resultado
                'oBESinDoc.IDSiniestro = resultado
                If resultado <> 0 Then
                    For Each oBESd As BESiniestroDocumento In oBE.ListaCoberturaDocumento
                        oBESinDoc = oBESd
                        oBESinDoc.IDSiniestro = IDSiniestroI
                        If Not oDLTSinDoc.Insertar(oBESinDoc, cn, sqlTranx) Then
                            resultado = 0
                        End If
                    Next
                End If

                'Inserto los archivos fisicos que se suben
                Dim oBEA As BEArchivo
                Dim oDLNTArchivo As New DLNTArchivo
                For Each oBEA In oBE.ListaArchivos
                    Dim oNBEA = New BEArchivo

                    oBEA.IdSiniestro = IDSiniestroI
                    oBEA.NombreArchivo = oBEA.NombreArchivo
                    oBEA.Estado = True
                    oBEA.FechaCreacion = Date.Now
                    oDLNTArchivo.Insertar(oBEA, cn, sqlTranx)
                Next

                '-----------------
                If resultado <> 0 Then
                    sqlTranx.Commit()
                Else
                    sqlTranx.Rollback()
                End If
            Else
                resultado = 0
                sqlTranx.Rollback()
            End If
        Catch ex As Exception
            Throw ex
            sqlTranx.Rollback()
            'Try
            '    sqlTranx.Rollback()
            'Catch ex2 As Exception
            '    Throw ex2
            'End Try
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return resultado
    End Function

    Public Function Insertar(ByVal pEntidad As BE.BEBase) As Boolean Implements IDLT.Insertar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function LlenarEstructura(ByVal pEntidad As BE.BEBase, ByVal pcm As System.Data.SqlClient.SqlCommand, ByVal TipoTransaccion As String) As System.Data.SqlClient.SqlCommand Implements IDLT.LlenarEstructura
        Dim oBE As BESiniestro = DirectCast(pEntidad, BESiniestro)
        pcm.Parameters.Add("@IDSiniestro", SqlDbType.Int).Value = oBE.IDSiniestro
        If (TipoTransaccion = "I") Then
            ' pcm.Parameters.Add("@IDSiniestro", SqlDbType.Int).Direction = ParameterDirection.Output
            pcm.Parameters("@IDSiniestro").Direction = ParameterDirection.Output
            pcm.Parameters.Add("@IDCertificado", SqlDbType.VarChar, 25).Value = oBE.IDCertificado
            pcm.Parameters.Add("@IDTipoSiniestro", SqlDbType.Int).Value = oBE.IDTipoSiniestro
            pcm.Parameters.Add("@UsuarioCreacion", SqlDbType.VarChar, 50).Value = oBE.UsuarioCreacion
            pcm.Parameters.Add("@NumeroSiniestro", SqlDbType.VarChar, 2000).Value = oBE.NroSiniestroAseguradora
        End If

        If (TipoTransaccion = "I") Or (TipoTransaccion = "A") Then
            pcm.Parameters.Add("@FechaSiniestro", SqlDbType.Date).Value = oBE.FechaSiniestro
            pcm.Parameters.Add("@MontoSiniestro", SqlDbType.Decimal).Value = oBE.MontoSiniestro
            pcm.Parameters.Add("@IDMonedaMSiniestro", SqlDbType.VarChar, 5).Value = oBE.IDMonedaMSiniestro
            pcm.Parameters.Add("@AseDireccion", SqlDbType.VarChar, 250).Value = oBE.AseDireccion
            pcm.Parameters.Add("@AseTelefono", SqlDbType.VarChar, 50).Value = oBE.AseTelefono
            pcm.Parameters.Add("@AseEmail", SqlDbType.VarChar, 100).Value = oBE.AseEmail
            pcm.Parameters.Add("@AseObservacion", SqlDbType.VarChar, 2000).Value = oBE.AseObservacion
        End If

        If (TipoTransaccion = "A") Then

            pcm.Parameters.Add("@MontoPago", SqlDbType.Decimal).Value = IIf(oBE.MontoPago = -1, DBNull.Value, oBE.MontoPago)
            pcm.Parameters.Add("@IDMonedaMPago", SqlDbType.VarChar, 5).Value = IIf(oBE.IDMonedaMPago = "0", DBNull.Value, oBE.IDMonedaMPago)
            pcm.Parameters.Add("@MontoPagoAse", SqlDbType.Decimal).Value = IIf(oBE.MontoPagoAse = -1, DBNull.Value, oBE.MontoPagoAse)
            pcm.Parameters.Add("@IDMonedaMPagoAse", SqlDbType.VarChar, 5).Value = IIf(oBE.IDMonedaMPagoAse = "0", DBNull.Value, oBE.IDMonedaMPagoAse)

            pcm.Parameters.Add("@UsuarioModificacion", SqlDbType.VarChar, 50).Value = oBE.UsuarioModificacion
        End If

        If (TipoTransaccion = "EVAL") Then
            pcm.Parameters.Add("@IDEstadoSiniestro", SqlDbType.Int).Value = oBE.IDEstadoSiniestro
            pcm.Parameters.Add("@ObservacionEvaluacion", SqlDbType.VarChar, 2000).Value = oBE.ObservacionEvaluacion
            pcm.Parameters.Add("@UsuarioModificacion", SqlDbType.VarChar, 50).Value = oBE.UsuarioModificacion
        End If
        Return pcm
    End Function
End Class
