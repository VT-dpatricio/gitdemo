﻿Imports VAN.Siniestros.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLTReclamo
    Inherits DLBase
    Implements IDLT

    Public Function Actualizar(ByVal pEntidad As BE.BEBase) As Boolean Implements IDLT.Actualizar
        Dim cn As New SqlConnection(CadenaConexion)
        Dim cm As New SqlCommand("SS_ActualizarReclamo", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm = Me.LlenarEstructura(pEntidad, cm, "A")
        Dim resultado As Boolean = False
        Try
            cn.Open()
            cm.ExecuteNonQuery()
            resultado = True
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return resultado
    End Function

    Public Function Eliminar(ByVal pCodigo As Integer) As Boolean Implements IDLT.Eliminar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Insertar(ByVal pEntidad As BE.BEBase) As Boolean Implements IDLT.Insertar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function InsertarR(ByVal pEntidad As BE.BEBase) As Int32
        Dim cn As New SqlConnection(CadenaConexion)
        Dim cm As New SqlCommand("SS_InsertarReclamo", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm = Me.LlenarEstructura(pEntidad, cm, "I")

        Dim resultado As Integer = 0
        Try
            cn.Open()
            cm.ExecuteNonQuery()
            resultado = cm.Parameters("@IDReclamo").Value()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return resultado
    End Function

    Public Function LlenarEstructura(ByVal pEntidad As BE.BEBase, ByVal pcm As System.Data.SqlClient.SqlCommand, ByVal TipoTransaccion As String) As System.Data.SqlClient.SqlCommand Implements IDLT.LlenarEstructura
        Dim oBE As BEReclamo = DirectCast(pEntidad, BEReclamo)
        If (TipoTransaccion = "I") Then
            pcm.Parameters.Add("@IDReclamo", SqlDbType.Int).Direction = ParameterDirection.Output
            pcm.Parameters.Add("@UsuarioCreacion", SqlDbType.VarChar, 50).Value = oBE.UsuarioCreacion
        End If

        If (TipoTransaccion = "I" Or TipoTransaccion = "A") Then
            pcm.Parameters.Add("@IDCertificado", SqlDbType.VarChar, 25).Value = oBE.IDCertificado
            pcm.Parameters.Add("@FechaReclamo", SqlDbType.Date).Value = oBE.FechaReclamo
            If oBE.FechaAtencion.ToString("dd/MM/yyyy") = "01/01/1900" Then
                pcm.Parameters.Add("@FechaAtencion", SqlDbType.SmallDateTime).Value = DBNull.Value
            Else
                pcm.Parameters.Add("@FechaAtencion", SqlDbType.SmallDateTime).Value = oBE.FechaAtencion
            End If

            If oBE.FechaFinAtencion.ToString("dd/MM/yyyy") = "01/01/1900" Then
                pcm.Parameters.Add("@FechaFinAtencion", SqlDbType.SmallDateTime).Value = DBNull.Value
            Else
                pcm.Parameters.Add("@FechaFinAtencion", SqlDbType.SmallDateTime).Value = oBE.FechaFinAtencion
            End If
            pcm.Parameters.Add("@NroReclamo", SqlDbType.VarChar, 50).Value = oBE.NroReclamo
            pcm.Parameters.Add("@Imagen", SqlDbType.Bit).Value = oBE.Imagen
            pcm.Parameters.Add("@NroCobro", SqlDbType.Int).Value = oBE.NroCobro
            pcm.Parameters.Add("@Siniestro", SqlDbType.Bit).Value = oBE.Siniestro
            pcm.Parameters.Add("@IDMotivoReclamo", SqlDbType.Int).Value = oBE.IDMotivoReclamo
            pcm.Parameters.Add("@IDEstadoReclamo", SqlDbType.Int).Value = oBE.IDEstadoReclamo

            If oBE.GenerarDevolucion Then
                'Aprobado
                pcm.Parameters.Add("@CobroInicioPeriodo", SqlDbType.SmallDateTime).Value = oBE.CobroInicioPeriodo
                pcm.Parameters.Add("@CobroFinPeriodo", SqlDbType.SmallDateTime).Value = oBE.CobroFinPeriodo
                pcm.Parameters.Add("@NroCobroDevolver", SqlDbType.Int).Value = oBE.NroCobroDevolver
                pcm.Parameters.Add("@MontoTotal", SqlDbType.Decimal).Value = oBE.MontoTotal
                pcm.Parameters.Add("@IDMoneda", SqlDbType.VarChar, 5).Value = oBE.IDMoneda
                If oBE.FechaExtorno.ToString("dd/MM/yyyy") = "01/01/1900" Then
                    pcm.Parameters.Add("@FechaExtorno", SqlDbType.Date).Value = DBNull.Value
                Else
                    pcm.Parameters.Add("@FechaExtorno", SqlDbType.Date).Value = oBE.FechaExtorno
                End If
                pcm.Parameters.Add("@OperacionExtorno", SqlDbType.VarChar, 200).Value = oBE.OperacionExtorno
                'pcm.Parameters.Add("@MedioDevolucion", SqlDbType.VarChar, 200).Value = oBE.MedioDevolucion
            Else
                'Rechazado
                pcm.Parameters.Add("@CobroInicioPeriodo", SqlDbType.SmallDateTime).Value = DBNull.Value
                pcm.Parameters.Add("@CobroFinPeriodo", SqlDbType.SmallDateTime).Value = DBNull.Value
                pcm.Parameters.Add("@NroCobroDevolver", SqlDbType.Int).Value = DBNull.Value
                pcm.Parameters.Add("@MontoTotal", SqlDbType.Decimal).Value = DBNull.Value
                pcm.Parameters.Add("@IDMoneda", SqlDbType.VarChar, 5).Value = DBNull.Value
                pcm.Parameters.Add("@FechaExtorno", SqlDbType.Date).Value = DBNull.Value
                pcm.Parameters.Add("@OperacionExtorno", SqlDbType.VarChar, 200).Value = DBNull.Value
                'pcm.Parameters.Add("@MedioDevolucion", SqlDbType.VarChar, 200).Value = DBNull.Value
            End If
            pcm.Parameters.Add("@AseDireccion", SqlDbType.VarChar, 250).Value = oBE.AseDireccion
            pcm.Parameters.Add("@AseTelefono", SqlDbType.VarChar, 50).Value = oBE.AseTelefono
            pcm.Parameters.Add("@AseEmail", SqlDbType.VarChar, 100).Value = oBE.AseEmail
            pcm.Parameters.Add("@Observacion", SqlDbType.VarChar, 2000).Value = oBE.Observacion
            pcm.Parameters.Add("@ObservacionAseg", SqlDbType.VarChar, 2000).Value = oBE.ObservacionAsegurado

            pcm.Parameters.Add("@Raiz", SqlDbType.VarChar, 2000).Value = oBE.RAIZ
            pcm.Parameters.Add("@Cbu", SqlDbType.VarChar, 2000).Value = oBE.CBU
            pcm.Parameters.Add("@MedioDevolucion", SqlDbType.VarChar, 200).Value = IIf(oBE.MedioDevolucion = String.Empty, DBNull.Value, oBE.MedioDevolucion)
            pcm.Parameters.Add("@CuentaExterna", SqlDbType.Bit).Value = IIf(oBE.CuentaExterna, 1, 0)
            If oBE.CuentaExterna Then
                pcm.Parameters.Add("@MedioDevolucionCipher", SqlDbType.VarBinary).Value = IIf(oBE.MedioDevolucionCipher Is Nothing, System.DBNull.Value, oBE.MedioDevolucionCipher)
            Else
                pcm.Parameters.Add("@MedioDevolucionCipher", SqlDbType.VarBinary).Value = DBNull.Value
            End If
            pcm.Parameters.Add("@IdMedioPago", SqlDbType.VarChar, 2).Value = IIf(oBE.IDMedioPago = String.Empty, DBNull.Value, oBE.IDMedioPago)
        End If
        If (TipoTransaccion = "A") Then
            pcm.Parameters.Add("@IDReclamo", SqlDbType.Int).Value = oBE.IDReclamo
            pcm.Parameters.Add("@UsuarioModificacion", SqlDbType.VarChar, 50).Value = oBE.UsuarioModificacion
            pcm.Parameters.Add("@MedioDevolucionOriginal", SqlDbType.VarChar, 200).Value = IIf(oBE.MedioDevolucionOriginal = String.Empty, DBNull.Value, oBE.MedioDevolucionOriginal)
        End If
        Return pcm
    End Function

   
End Class
