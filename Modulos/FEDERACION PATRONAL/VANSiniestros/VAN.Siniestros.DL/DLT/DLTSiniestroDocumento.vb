﻿Imports VAN.Siniestros.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLTSiniestroDocumento
    Inherits DLBase


    Public Function Actualizar(ByVal pEntidad As BESiniestroDocumento) As Boolean
        Dim cn As New SqlConnection(CadenaConexion)
        Dim cm As New SqlCommand("SS_PRC_UPD_SiniestroDocumento", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm = Me.LlenarEstructura(pEntidad, cm, "A")
        Dim resultado As Boolean = False
        Try
            cn.Open()
            Dim nDA As Integer = cm.ExecuteNonQuery()
            If nDA > 0 Then
                resultado = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function Insertar(ByVal pEntidad As BESiniestroDocumento, ByVal cn As SqlConnection, ByVal sqlTranx As SqlTransaction) As Boolean
        Dim cm As New SqlCommand("SS_PRC_INS_SiniestroDocumento", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm = Me.LlenarEstructura(pEntidad, cm, "I")
        Dim resultado As Boolean = False
        Try
            cm.Transaction = sqlTranx
            Dim nDA As Integer = cm.ExecuteNonQuery()
            If nDA > 0 Then
                resultado = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function LlenarEstructura(ByVal oBE As BESiniestroDocumento, ByVal pcm As SqlCommand, ByVal TipoTransaccion As String) As SqlCommand
        If (TipoTransaccion = "I") Then
            'pcm.Parameters.Add("@IDSiniestroDoc", SqlDbType.Int).Value = oBE.IDSiniestroDoc
            pcm.Parameters.Add("@IDDocumento", SqlDbType.Int).Value = oBE.IDDocumento
            pcm.Parameters.Add("@IDSiniestro", SqlDbType.Int).Value = oBE.IDSiniestro
            If (oBE.FechaEntrega.ToShortDateString = New DateTime(1900, 1, 1)) Then
                pcm.Parameters.Add("@FechaEntrega", SqlDbType.Date).Value = System.DBNull.Value
            Else
                pcm.Parameters.Add("@FechaEntrega", SqlDbType.Date).Value = oBE.FechaEntrega
            End If
        End If
        If (TipoTransaccion = "A") Then
            pcm.Parameters.Add("@IDSiniestroDoc", SqlDbType.Int).Value = oBE.IDSiniestroDoc
            pcm.Parameters.Add("@FechaEntrega", SqlDbType.Date).Value = oBE.FechaEntrega
            pcm.Parameters.Add("@estado", SqlDbType.Bit).Value = oBE.Entregado
            pcm.Parameters.Add("@Adjunto", SqlDbType.VarChar, 250).Value = oBE.Adjunto
            pcm.Parameters.Add("@UsuarioModificacion", SqlDbType.VarChar, 50).Value = oBE.UsuarioModificacion
        End If
        Return pcm
    End Function
End Class

