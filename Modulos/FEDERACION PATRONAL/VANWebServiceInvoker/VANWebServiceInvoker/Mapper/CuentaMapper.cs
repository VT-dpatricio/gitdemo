﻿using System;
using System.Collections.Generic;
using System.Text;
using BaisWebServiceInvoker.Interfaces;
using BaisWebServiceInvoker.Model;
using BaisWebServiceInvoker.ITAU_SERVICES;
using BaisWebServiceInvoker.Constant;

namespace BaisWebServiceInvoker.Mapper
{
    public class CuentaMapper
    {
        public List<Model.MedioPago> FillModel(Model.Cliente oCliente, consultaResponse objCuentasResponse)
        {
            List<Model.MedioPago> lstMedioDePagoCuentas = new List<MedioPago>();
            if (objCuentasResponse.response.codigo.Trim() == Constant.Constant.RESPONSE_OK)
            {
                foreach(Raiz objRaiz in objCuentasResponse.response.raiz)
                {
                    if (ValidatePackage(objRaiz))
                    {
                        foreach (Cuenta objCuenta in objRaiz.cuenta)
                        {
                            if (objCuenta.vinculo == "N" && objCuenta.estado != "2" && objCuenta.estado != "3")
                            {
                                MedioPago mpcuenta = new MedioPago();
                                mpcuenta.Raiz = objRaiz.raiz;
                                mpcuenta.Tipo = Constant.Constant.MEDIO_PAGO_CUENTA;
                                mpcuenta.IDMedioPago = objCuenta.tipoCuenta;
                                mpcuenta.Vinculo = objCuenta.vinculo;
                                mpcuenta.Estado = objCuenta.estado;
                                mpcuenta.IDMoneda = objCuenta.divisa;
                                mpcuenta.Numero = objCuenta.numero;
                                mpcuenta.CBU = string.Empty;
                                mpcuenta.Paquete = objRaiz.codigoPaquete;
                                mpcuenta.Sucursal = objRaiz.codigoSucursal;
                                lstMedioDePagoCuentas.Add(mpcuenta);
                            }
                        }
                    }
                }

                ITAU_SERVICES.ITAU_SERVICES wrapperService = new ITAU_SERVICES.ITAU_SERVICES();
                BaisWebServiceInvoker.ITAU_SERVICES.listadoResponse objTarjetaResponse = null;

                foreach (Raiz objRaiz in objCuentasResponse.response.raiz)
                {
                    if(!string.IsNullOrEmpty(objRaiz.raiz.Trim()))
                    {
                        foreach (Cuenta objCuenta in objRaiz.cuenta)
                        {
                            if (objCuenta.vinculo == "N" && objCuenta.estado != "2")
                            {
                                 wrapperService
                            }
                        }
                    }
                }
            }
            return lstMedioDePagoCuentas;
        }

        private bool ValidatePackage(Raiz oRaiz)
        {
            bool status = true;
            string[] paquetes = new string[3];

            paquetes[0] = "310";
            paquetes[1] = "340";
            paquetes[2] = "110";

            if (String.IsNullOrEmpty(oRaiz.raiz.Trim()))
            {
                return false;
            }

            foreach (string pkg in paquetes)
            {
                if (oRaiz.codigoPaquete.Trim() == pkg)
                {
                    status = false;
                }
            }
            return status;
        }
    }
}
