﻿using System;
using System.Collections.Generic;
using System.Text;
using VANWebServiceInvoker.Interfaces;
using VANWebServiceInvoker.Model;
using VANWebServiceInvoker.ITAU_SERVICES;
using VANWebServiceInvoker.Constant;
using System.Linq;


namespace VANWebServiceInvoker.Mapper
{
    public class CuentaTarjetaMapper
    {
        public List<Model.MedioPago> FillModel(Model.Cliente oCliente, consultaResponse objCuentasResponse, int idProducto)
        {

            int idMedioDePago = 0;
            List<Model.MedioPago> lstMedioDePago = new List<MedioPago>();
            if (objCuentasResponse.response.codigo.Trim() == Constant.Constant.RESPONSE_OK)
            {
                Model.BEListaEquivalencia oBEMedioPago;
                List<Model.BEListaEquivalencia> lstMedioPago = new List<BEListaEquivalencia>();
                lstMedioPago = Common.Common.GetEquivalencias(Constant.Constant.T_MEDIO_PAGO, idProducto);

                if (objCuentasResponse.response.raiz != null)
                {
                    foreach (Raiz objRaiz in objCuentasResponse.response.raiz)
                    {
                        if (ValidatePackage(objRaiz) && ValidatePackageForProduct(idProducto,objRaiz))
                        {
                            if (objRaiz.cuenta != null)
                            {
                                foreach (Cuenta objCuenta in objRaiz.cuenta)
                                {
                                    if (objCuenta.vinculo == "N" && objCuenta.estado != "2" && objCuenta.estado != "3")
                                    {
                                        idMedioDePago++;
                                        MedioPago mpcuenta = new MedioPago();
                                        
                                        mpcuenta.ID = idMedioDePago;
                                        mpcuenta.Raiz = objRaiz.raiz;
                                        mpcuenta.Tipo = Constant.Constant.MEDIO_PAGO_CUENTA;
                                        mpcuenta.IDMedioPago = objCuenta.tipoCuenta;

                                        oBEMedioPago = Common.Common.getValorAON(lstMedioPago, objCuenta.tipoCuenta);
                                        mpcuenta.Descripcion = oBEMedioPago.Descripcion;
                                        mpcuenta.IDMedioPagoBAIS = Common.Common.GetStringSelectValue(oBEMedioPago.ValorAON);
                                        mpcuenta.Vinculo = objCuenta.vinculo;
                                        mpcuenta.Estado = objCuenta.estado;
                                        mpcuenta.IDMoneda = objCuenta.divisa;
                                        mpcuenta.Numero = objCuenta.numero;
                                        mpcuenta.NumeroCipher = objCuenta.numeroCipher;
                                        mpcuenta.CBU = string.Empty;
                                        mpcuenta.Paquete = objRaiz.codigoPaquete;
                                        mpcuenta.Sucursal = objRaiz.codigoSucursal;
                                        lstMedioDePago.Add(mpcuenta);
                                    }
                                }
                            }
                           
                        }
                    }

                    VANWebServiceInvoker.ITAU_SERVICES.listadoResponse objTarjetaResponse = null;
                    List<MedioPago> pLista = new List<MedioPago>();
                    foreach (Raiz objRaiz in objCuentasResponse.response.raiz)
                    {
                        if (!string.IsNullOrEmpty(objRaiz.raiz.Trim()) && ValidatePackageForProduct(idProducto, objRaiz))
                        {
                            if (objRaiz.cuenta != null)
                            {
                                foreach (Cuenta objCuenta in objRaiz.cuenta)
                                {
                                    if (objCuenta.vinculo == "N" && objCuenta.estado != "2")
                                    {
                                        objTarjetaResponse = GetTarjetasPorCodCliente(oCliente.Codigo, objRaiz.raiz, objCuenta.numeroCipher, objCuenta.tipoCuenta);


                                        if (objTarjetaResponse != null && 
                                                    objTarjetaResponse.retornoListado.respuesta.codRetorno != Constant.Constant.RESPONSE_ERROR)
                                        {
                                            foreach (Tarjeta objTarjeta in objTarjetaResponse.retornoListado.tarjetas)
                                            {
                                                if (objTarjeta.idFuncion == "C")
                                                {
                                                    if (ValidateDuplicate(objTarjeta.entidadProductora.Trim(), objTarjeta.numero, objRaiz.raiz, pLista))
                                                    {
                                                        MedioPago mpTarjeta = new MedioPago();
                                                        idMedioDePago++;
                                                        mpTarjeta.ID = idMedioDePago;
                                                        mpTarjeta.Raiz = objRaiz.raiz;
                                                        mpTarjeta.Tipo = Constant.Constant.MEDIO_PAGO_TARJETA;
                                                        mpTarjeta.IDMedioPago = objTarjeta.entidadProductora.Trim();
                                                        
                                                        oBEMedioPago = Common.Common.getValorAON(lstMedioPago, objTarjeta.entidadProductora.Trim());
                                                        mpTarjeta.Descripcion = oBEMedioPago.Descripcion;
                                                        mpTarjeta.IDMedioPagoBAIS = oBEMedioPago.ValorAON;
                                                        mpTarjeta.Vinculo = objCuenta.vinculo;
                                                        mpTarjeta.Estado = objCuenta.estado;
                                                        mpTarjeta.IDMoneda = objCuenta.divisa;
                                                        mpTarjeta.Numero = objTarjeta.numero;
                                                        mpTarjeta.NumeroCipher = objTarjeta.numeroCipher;
                                                        mpTarjeta.CBU = string.Empty;
                                                        mpTarjeta.Paquete = objRaiz.codigoPaquete;
                                                        mpTarjeta.Sucursal = objRaiz.codigoSucursal;
                                                        lstMedioDePago.Add(mpTarjeta);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }
                }   
            }
            return lstMedioDePago;
        }

        private VANWebServiceInvoker.ITAU_SERVICES.listadoResponse GetTarjetasPorCodCliente(string sCodigo, string sRaiz, string sNumero, string sTipoCuenta)
        {
            VANWebServiceInvoker.ITAU_SERVICES.listadoResponse objTarjetaResponse = null;
            try
            {
            if (Common.Common.IsMock())
            {
                objTarjetaResponse = MockFactory.LoadTarjetas(sCodigo);
            }
            else
            {
                ITAU_SERVICES.ITAU_SERVICES wrapperService = new ITAU_SERVICES.ITAU_SERVICES();
                objTarjetaResponse = wrapperService.GetTarjetasPorCodCliente(sCodigo, sRaiz, sNumero, sTipoCuenta);
            }
            }
            catch(Exception ex)
            {
                objTarjetaResponse=null;
            }
            return objTarjetaResponse;
        }

        private bool ValidateDuplicate(string pIdMedioPago, string pNumero, string pRaiz, List<MedioPago> pLista)
        {
            bool respuesta = true;
            foreach (MedioPago mp in pLista)
            {
                if (mp.IDMedioPago == pIdMedioPago && mp.Numero == pNumero && mp.Raiz == pRaiz)
                {
                    respuesta = false;
                    break;
                }
            }

            if (respuesta)
            {
                MedioPago mp = new MedioPago();
                mp.IDMedioPago = pIdMedioPago;
                mp.Numero = pNumero;
                mp.Raiz = pRaiz;
                pLista.Add(mp);
            }
            return respuesta;
        }

        private bool ValidatePackage(Raiz oRaiz)
        {
            bool status = true;
            string[] paquetes = new string[3];

            paquetes[0] = "310";
            paquetes[1] = "340";
            paquetes[2] = "110";

            if (String.IsNullOrEmpty(oRaiz.raiz.Trim()))
            {
                return false;
            }

            foreach (string pkg in paquetes)
            {
                if (oRaiz.codigoPaquete.Trim() == pkg)
                {
                    status = false;
                }
            }
            return status;
        }

        private bool ValidatePackageForProduct(int idProducto, Raiz oRaiz)
        {
            bool status = true;
            char delimiter = ',';

            if (idProducto != null)
            {
                string values = Common.Common.GetFilterPackage(idProducto);
                String[] paquetes = new String[] { };
                if (!string.IsNullOrEmpty(values))
                {
                    paquetes = values.ToString().Split(delimiter);
                }
                else
                {
                    return true;
                }

                if (String.IsNullOrEmpty(oRaiz.raiz.Trim()))
                {
                    return false;
                }
                status = paquetes.Contains(oRaiz.codigoPaquete.Trim());
            }
            return status;
        }

    }
}
