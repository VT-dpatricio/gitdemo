﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using VANWebServiceInvoker.Mapper;
using System.Xml;
using System.Xml.Serialization;


namespace VANWebServiceInvoker
{

    public class WebServiceInvoker
    {
        public Model.Cliente GetClient(string codTipoDocumento, string ccCliente, int idProducto)
        {

            XmlDocument xmlRespCliente = new XmlDocument();
            Model.Cliente objCliente = null;
            VANWebServiceInvoker.ITAU_SERVICES.consultaPersonaFisicaResponse clienteResponse = null;
            if (Common.Common.IsMock())
            {
                clienteResponse = MockFactory.LoadCliente(ccCliente);
            }
            else
            {
                ITAU_SERVICES.ITAU_SERVICES wrapperService = new ITAU_SERVICES.ITAU_SERVICES();
                clienteResponse = wrapperService.GetCliente(codTipoDocumento, ccCliente);
            }

            if (clienteResponse != null)
            {
                ClienteMapper mapper = new ClienteMapper();
                objCliente = mapper.FillModel(clienteResponse, idProducto);
            }
          
            return objCliente;
        }

        //public Model.Cliente GetClient(string codTipoDocumento, string ccCliente)
        //{
        //    return GetClient(codTipoDocumento, ccCliente, null);
        //}

        public Model.Cbu GetCBUCuenta(string numero)
        {
            VANWebServiceInvoker.ITAU_SERVICES.consultaCBUResponse cbuResponse = null;
            Model.Cbu objCbu = new Model.Cbu();
            if (Common.Common.IsMock())
            {
                cbuResponse = MockFactory.LoadCbu(AONArchitectural.Common.Security.Encrypter.Decrypt(numero));
            }
            else
            {
                ITAU_SERVICES.ITAU_SERVICES wrapperService = new ITAU_SERVICES.ITAU_SERVICES();
                cbuResponse = wrapperService.GetCBUCuenta(numero);
            }

            if (cbuResponse != null)
            {
                CbuMapper mapper = new CbuMapper();
                objCbu = mapper.FillModel(cbuResponse);
            }

            return objCbu;
        }

        public List<Model.MedioPago> GetCuentasTarjetas(Model.Cliente cliente, int idProducto)
        {
            List<Model.MedioPago> lstMedioDePagos = new List<Model.MedioPago>();

            VANWebServiceInvoker.ITAU_SERVICES.consultaResponse cuentasResponse = null;
            if (Common.Common.IsMock())
            {
                cuentasResponse = MockFactory.LoadCuentas(cliente.Codigo);
            }
            else
            {
                ITAU_SERVICES.ITAU_SERVICES wrapperService = new ITAU_SERVICES.ITAU_SERVICES();
                cuentasResponse = wrapperService.GetCuentasPorCodCliente(cliente.Codigo);
            }

            if (cuentasResponse != null)
            {
                CuentaTarjetaMapper mapper = new CuentaTarjetaMapper();
                lstMedioDePagos = mapper.FillModel(cliente, cuentasResponse, idProducto);
            }

            return lstMedioDePagos;
        }

        public Model.Cliente GetFullClient(string codTipoDocumento, string ccCliente, int idProducto)
        {
            Model.Cliente cliente = new Model.Cliente();
            try
            {
                cliente = GetClient(codTipoDocumento, ccCliente, idProducto);
                if (cliente != null)
                {
                    cliente.MediosDePago = GetCuentasTarjetas(cliente, idProducto);
                    if (cliente.MediosDePago != null)
                    {
                        ITAU_SERVICES.ITAU_SERVICES wrapperService = new ITAU_SERVICES.ITAU_SERVICES();
                        foreach (Model.MedioPago mp in cliente.MediosDePago)
                        {
                            if (mp.Tipo == Constant.Constant.MEDIO_PAGO_CUENTA)
                            {
                                Model.Cbu cbuResponse = GetCBUCuenta(mp.NumeroCipher);
                                if (cbuResponse != null && cbuResponse.Codigo == Constant.Constant.RESPONSE_OK)
                                {
                                    mp.CBU = cbuResponse.NroCbu;
                                    mp.CBUCipher = cbuResponse.NroCbuCipher;
                                }
                            }
                        }
                    }
                    cliente.EstadoRespuesta = Constant.Constant.RESPONSE_OK;
                }
                else
                {
                    //El servicio respondio correctamente pero no encontro el cliente
                    cliente = new Model.Cliente();
                    cliente.Codigo = string.Empty;
                    cliente.EstadoRespuesta = Constant.Constant.RESPONSE_ERROR;
                }
            }
            catch (Exception ex)
            {
                //Error al consultar el servicio de clientes
                cliente = new Model.Cliente();
                cliente.Codigo = "-1";
                cliente.EstadoRespuesta = Constant.Constant.RESPONSE_ERROR;
            }
            return cliente;
        }

    }
}