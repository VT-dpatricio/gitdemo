﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace VANWebServiceInvoker.Common
{
    static public  class Common
    {
        private const string SP_BW_FILTRO_PAQUETE_PRODUCTO = "WS_PRC_SEL_FiltroPaqueteProducto";
        private const string SP_BW_EQUIVALENCIA = "BW_ListarEquivalencia";
        private const string SP_CIUDAD = "BW_SeleccionarUbigeo";

        public static bool IsMock()
        {
             return ConvertToBoolean(ConfigurationManager.AppSettings["IsMock"]);
        }

        public static bool ConvertToBoolean(string value)
        {
            bool booleanValue = false;
            try
            {
                int n;
                bool isNumeric = int.TryParse(value, out n);
                if (isNumeric)
                {
                    booleanValue = Convert.ToBoolean(int.Parse(value));
                }
                else
                {
                    booleanValue = Convert.ToBoolean(value);
                }   
            }
            catch(Exception ex)
            {
                booleanValue = false;
            }
            return booleanValue;
        }

        private static string cnn ()
        {
            string cnnBais = string.Empty;
            try
            {
                cnnBais = ConfigurationManager.ConnectionStrings["BAIS"].ConnectionString;
            }
            catch(Exception ex)
            {
                cnnBais = string.Empty;
            }
            return cnnBais;
        }

        public static string GetStringSelectValue(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return value.Trim().ToUpper();
            }
            return string.Empty;
        }


        public static List<Model.BEListaEquivalencia> GetEquivalencias(string NombreTabla, int idProducto)
        {
            List<Model.BEListaEquivalencia> lstEquivalencias = new List<Model.BEListaEquivalencia>();
            try
            {
                DataSet ds = new DataSet();

                ds = SqlHelper.ExecuteDataset(cnn(), SP_BW_EQUIVALENCIA,
                                        idProducto,
                                        NombreTabla);

                if (ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        Model.BEListaEquivalencia objEQ = new Model.BEListaEquivalencia();
                        objEQ.Descripcion = dr["Descripcion"].ToString();
                        objEQ.ValorAON = dr["ValorAON"].ToString();
                        objEQ.ValorEquivalencia = dr["ValorEquivalencia"].ToString();
                        lstEquivalencias.Add(objEQ);
                    }
                }
            }
            catch (Exception)
            { }

            return lstEquivalencias;
        }


        public static Model.BEListaEquivalencia getValorAON(IList<Model.BEListaEquivalencia> pLista, String pValoEquivalente)
        {
            Model.BEListaEquivalencia oBE = new Model.BEListaEquivalencia();

            try
            {
                if (pLista != null)
                {
                    foreach (Model.BEListaEquivalencia oBEEQ in pLista)
                    {
                        if (oBEEQ.ValorEquivalencia == pValoEquivalente)
                        {
                            oBE = oBEEQ;
                            break;
                        }
                    }
                }
            }
            catch (Exception)
            { }
            return oBE;
        }


        public static Model.BECiudad SeleccionarxCodPostal(int pIDProducto, string pCodPostal)
        {
            Model.BECiudad oBE = new Model.BECiudad();

            try
            {
                DataSet ds = new DataSet();

                ds = SqlHelper.ExecuteDataset(cnn(), SP_CIUDAD,
                                        pIDProducto,
                                        0,
                                        pCodPostal);
                if (ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        oBE.IdCiudad = dr["IdCiudad"] != System.DBNull.Value ? Convert.ToInt32(dr["IdCiudad"].ToString()) : 0;
                        oBE.IdDepartamento = dr["IdDepartamento"] != System.DBNull.Value ? Convert.ToInt32(dr["IdDepartamento"].ToString()) : 0;
                        oBE.CodPostal = dr["CodPostal"] != System.DBNull.Value ? dr["CodPostal"].ToString() : string.Empty;
                        oBE.Ciudad = dr["Ciudad"] != System.DBNull.Value ? dr["Ciudad"].ToString() : string.Empty;
                        oBE.Provincia = dr["Provincia"] != System.DBNull.Value ? dr["Provincia"].ToString() : string.Empty;
                        oBE.Departamento = dr["Departamento"] != System.DBNull.Value ? dr["Departamento"].ToString() : string.Empty;
                    }
                }
            }
            catch (Exception)
            {
                oBE = null;
            }
            return oBE;
        }

        public static string GetFilterPackage(int IDProducto)
        {
            string package = string.Empty;
            try
            {
                DataSet ds = new DataSet();

                ds = SqlHelper.ExecuteDataset(cnn(), SP_BW_FILTRO_PAQUETE_PRODUCTO,
                                        IDProducto);
                if (ds.Tables[0].Rows.Count > 0)
                {
                   package = ds.Tables[0].Rows[0]["Paquetes"].ToString();
                }

            }
            catch (Exception)
            {
                package = string.Empty;
            }
            return package;
        }
    }
}
