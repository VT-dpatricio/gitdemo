﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VANWebServiceInvoker.Model
{
    public sealed class Domicilio
    {
        public string Calle { get; set; }
        public string Numero { get; set; }
        public string Piso { get; set; }
        public string Departamento { get; set; }
        public string CodigoPostal { get; set; }
        public string Tipo { get; set; }
        public int IdCiudad { get; set; }
        public string Localidad { get; set; } //Localidad se obtiene a partir del codigo postal desde las tablas de BAIS
        public string Provincia { get; set; } //Provincia se obtiene a partir del codigo postal desde las tablas de BAIS
    }
}
