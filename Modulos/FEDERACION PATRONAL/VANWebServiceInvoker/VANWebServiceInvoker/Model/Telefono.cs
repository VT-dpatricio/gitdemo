﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VANWebServiceInvoker.Model
{
    public sealed class Telefono
    {
        public string Prefijo { get; set; }
        public string Numero { get; set; }
    }
}
