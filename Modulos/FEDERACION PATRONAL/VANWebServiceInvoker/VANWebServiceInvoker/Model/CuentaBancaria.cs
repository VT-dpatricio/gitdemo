﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VANWebServiceInvoker.Model
{
    public sealed class CuentaBancaria
    {
        public CuentaBancaria()
        {
            //MedioPagoCuenta = new MedioPago[0];
            TarjetaCredito = new TarjetaCredito[0];
        }

        public string Vinculo { get; set; }
        public string Estado { get; set; }
        //public string CodRaiz { get; set; }
        public string CuentaOrigen { get; set; }
        public string TipoCuentaOrigen { get; set; }
        public TarjetaCredito[] TarjetaCredito { get; set; }
        //public MedioPago[] MedioPagoCuenta { get; set; }

        public int ID { get; set; }
        public string Raiz { get; set; }
        public string Tipo { get; set; }
        public string IDMedioPago { get; set; }
        public string IDMoneda { get; set; }
        public string Numero { get; set; }
        public string CBU { get; set; }
        public string Paquete { get; set; }

    }
}
