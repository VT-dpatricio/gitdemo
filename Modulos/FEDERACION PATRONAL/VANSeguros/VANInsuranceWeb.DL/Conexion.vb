﻿Imports Microsoft.Practices.EnterpriseLibrary.Data

Public Class Conexion
    Private Shared _Instancia As Conexion
    Private _Database As Database

    Private Sub New()
        _Database = DatabaseFactory.CreateDatabase("BD")
    End Sub

    Public Shared Function Instancia() As Conexion

        If _Instancia Is Nothing Then
            _Instancia = New Conexion()
        End If

        Return _Instancia
    End Function

    Public Function Database() As Database
        Return _Database
    End Function

End Class


Public Class ConexionBAIS
    Private Shared _Instancia As ConexionBAIS
    Private _Database As Database

    Private Sub New()
        _Database = DatabaseFactory.CreateDatabase("BAIS")
    End Sub

    Public Shared Function Instancia() As ConexionBAIS

        If _Instancia Is Nothing Then
            _Instancia = New ConexionBAIS()
        End If

        Return _Instancia
    End Function

    Public Function Database() As Database
        Return _Database
    End Function

End Class
