﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports Microsoft.Practices.EnterpriseLibrary.Data

Public Class ProductoEntidadDA

    Dim _db As Database = Nothing

    Public Sub New()
        _db = Conexion.Instancia.Database()
    End Sub

    Function Agregar(ByVal Objeto As ProductoEntidadBE, Optional ByVal Transac As DbTransaction = Nothing) As String
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_ProductoEntidad_Agregar", Nothing, Objeto.IdProducto, Objeto.IdEntidad, Objeto.Estado, Objeto.UsuarioReg, Objeto.FechaReg, Objeto.UsuarioMod, Objeto.FechaMod)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Modificar(ByVal Objeto As ProductoEntidadBE, Optional ByVal Transac As DbTransaction = Nothing) As Integer
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_ProductoEntidad_Modificar", Objeto.IdProductoEntidad, Objeto.IdProducto, Objeto.IdEntidad, Objeto.Estado, Objeto.UsuarioMod, Objeto.FechaMod)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Eliminar(ByVal Objeto As ProductoEntidadBE, Optional ByVal Transac As DbTransaction = Nothing) As Integer
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_ProductoEntidad_Eliminar", Objeto.IdProductoEntidad, Objeto.Estado, Objeto.UsuarioMod, Objeto.FechaMod)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Eliminar_IdProducto(ByVal Objeto As ProductoEntidadBE, Optional ByVal Transac As DbTransaction = Nothing) As Integer
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_ProductoEntidad_Eliminar_IdProducto", Objeto.IdProducto)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of ProductoEntidadBE)
        Dim Lista As New List(Of ProductoEntidadBE)

        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_Oficina_IdOficina")
                While dr.Read()
                    Lista.Add(Populate.ProductoEntidad_Lista(dr))
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_Oficina_IdOficina")
                While dr.Read()
                    Lista.Add(Populate.ProductoEntidad_Lista(dr))
                End While
            End Using
        End If
        Return Lista
    End Function

    Function Listar_Id(ByVal Objeto As ProductoEntidadBE, Optional ByVal Transaction As DbTransaction = Nothing) As ProductoEntidadBE
        Dim _ProductoEntidadBE As New ProductoEntidadBE

        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_Oficina_IdOficina", Objeto.IdProductoEntidad)
                While dr.Read()
                    _ProductoEntidadBE = Populate.ProductoEntidad_Lista(dr)
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_Oficina_IdOficina", Objeto.IdProductoEntidad)
                While dr.Read()
                    _ProductoEntidadBE = Populate.ProductoEntidad_Lista(dr)
                End While
            End Using
        End If
        Return _ProductoEntidadBE
    End Function


    Function Listar_IdProducto_Check(ByVal Objeto As ProductoEntidadBE, Optional ByVal Transaction As DbTransaction = Nothing) As List(Of EntidadBE)
        Dim _List As New List(Of EntidadBE)
        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_ProductoEntidad_IdProducto_Check", Objeto.IdProducto)
                While dr.Read()
                    _List.Add(Populate.ProductoEntidad_Check_Lista(dr))
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_ProductoEntidad_IdProducto_Check", Objeto.IdProducto)
                While dr.Read()
                    _List.Add(Populate.ProductoEntidad_Check_Lista(dr))
                End While
            End Using
        End If
        Return _List
    End Function

    Function Listar_IdProducto(ByVal Objeto As ProductoEntidadBE, Optional ByVal Transaction As DbTransaction = Nothing) As List(Of EntidadBE)
        Dim _List As New List(Of EntidadBE)
        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_ProductoEntidad_IdProducto", Objeto.IdProducto)
                While dr.Read()
                    _List.Add(Populate.ProductoEntidad_Check_Lista(dr))
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_ProductoEntidad_IdProducto", Objeto.IdProducto)
                While dr.Read()
                    _List.Add(Populate.ProductoEntidad_Check_Lista(dr))
                End While
            End Using
        End If
        Return _List
    End Function


End Class
