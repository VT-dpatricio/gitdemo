Imports VAN.InsuranceWeb.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLNTCiudad
    Inherits DLBase
    Implements IDLNTDataEntry
    Public Function Seleccionar() As IList Implements IDLNTDataEntry.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pEntidad As BE.BEBase) As IList Implements IDLNTDataEntry.Seleccionar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("BW_BuscarUbigeo", cn)
        cm.CommandType = CommandType.StoredProcedure
        Dim oBECiudad As BECiudad = DirectCast(pEntidad, BECiudad)
        cm.Parameters.Add("@IdProducto", SqlDbType.Int).Value = oBECiudad.IdProducto
        cm.Parameters.Add("@Buscar", SqlDbType.VarChar, 50).Value = oBECiudad.Buscar
        Dim oBE As BECiudad
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BECiudad
                oBE.IdCiudad = rd.GetInt32(rd.GetOrdinal("IdCiudad"))
                'oBE.IdProvincia = rd.GetInt32(rd.GetOrdinal("IdProvincia"))
                oBE.CodPostal = rd.GetString(rd.GetOrdinal("CodPostal"))
                oBE.IdDepartamento = rd.GetInt32(rd.GetOrdinal("IdDepartamento"))
                oBE.Ciudad = rd.GetString(rd.GetOrdinal("Ciudad"))
                oBE.Provincia = rd.GetString(rd.GetOrdinal("Provincia"))
                oBE.Departamento = rd.GetString(rd.GetOrdinal("Departamento"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As IList Implements IDLNTDataEntry.Seleccionar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("BW_SeleccionarCiudad", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@IdProvincia", SqlDbType.Int).Value = pCodigo
        Dim oBE As BECiudad
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BECiudad
                oBE.IdCiudad = rd.GetInt32(rd.GetOrdinal("IdCiudad"))
                oBE.Nombre = rd.GetString(rd.GetOrdinal("Nombre"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BEBase Implements IDLNTDataEntry.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(ByVal pBECiudad As BECiudad) As BECiudad
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("BW_SeleccionarUbigeo", cn)
        cm.CommandType = CommandType.StoredProcedure
        Dim oBECiu As BECiudad = DirectCast(pBECiudad, BECiudad)
        cm.Parameters.Add("@IDProducto", SqlDbType.Int).Value = oBECiu.IdProducto
        cm.Parameters.Add("@IDCiudad", SqlDbType.Int).Value = oBECiu.IdCiudad
        cm.Parameters.Add("@CodPostal", SqlDbType.VarChar, 20).Value = oBECiu.CodPostal
        Dim oBE As New BECiudad
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            If rd.Read Then
                oBE.IdCiudad = rd.GetInt32(rd.GetOrdinal("IdCiudad"))
                oBE.CodPostal = rd.GetString(rd.GetOrdinal("CodPostal"))
                oBE.IdDepartamento = rd.GetInt32(rd.GetOrdinal("IdDepartamento"))
                oBE.Ciudad = rd.GetString(rd.GetOrdinal("Ciudad"))
                oBE.Provincia = rd.GetString(rd.GetOrdinal("Provincia"))
                oBE.Departamento = rd.GetString(rd.GetOrdinal("Departamento"))
            End If
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return oBE
    End Function
End Class
