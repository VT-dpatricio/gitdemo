﻿Imports VAN.InsuranceWeb.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO
Imports Microsoft.VisualBasic
Imports VAN.InsuranceWeb.Comun

Public Class DLNTCrossSelling
    Inherits DLBase

    Public Function BuscarConfigCrossSelling(ByVal pEntidad As BE.BEBase) As IList(Of BECrossSelling)
        Dim cn As New SqlConnection(CadenaConexion)
        Dim cm As New SqlCommand("BW_PRC_SEL_ProductosCrossSelling", cn)
        Dim oBE As BECrossSelling = DirectCast(pEntidad, BECrossSelling)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@IDUsuario", SqlDbType.VarChar).Value = oBE.SIDUsuario

        Dim dtMediodePago As DataTable = Common.MedioPago_Cliente

        For Each mp As BEMedioPagoCliente In oBE.SIdsMediosDePagoCliente
            Dim row As DataRow = dtMediodePago.NewRow
            row("idMedioPago") = mp.IdMedioPago
            row("idMoneda") = mp.IdMoneda
            dtMediodePago.Rows.Add(row)
        Next

        cm.Parameters.Add("@IdsMediosDePagoCliente", SqlDbType.Structured).Value = dtMediodePago
        cm.Parameters.Add("@IDProducto", SqlDbType.Int).Value = IIf(String.IsNullOrEmpty(oBE.SIDProducto), DBNull.Value, oBE.SIDProducto)
        cm.Parameters.Add("@IDTipoDocumento", SqlDbType.VarChar).Value = oBE.SIDTipoDocumento
        cm.Parameters.Add("@NumeroDocumento", SqlDbType.VarChar).Value = oBE.SNumeroDocumento
        cm.Parameters.Add("@IDMedioPagoUtilizado", SqlDbType.VarChar).Value = IIf(String.IsNullOrEmpty(oBE.SIDMedioPagoUtilizado), DBNull.Value, oBE.SIDMedioPagoUtilizado)
        cm.Parameters.Add("@NumeroDeCuentaUtilizado", SqlDbType.VarChar).Value = IIf(String.IsNullOrEmpty(oBE.SNumeroDeCuentaUtilizado), DBNull.Value, oBE.SNumeroDeCuentaUtilizado)
        cm.Parameters.Add("@IDMonedaCobroUtilizado", SqlDbType.VarChar).Value = IIf(String.IsNullOrEmpty(oBE.SIDMonedaCobroUtilizado), DBNull.Value, oBE.SIDMonedaCobroUtilizado)

        Dim lista As New List(Of BECrossSelling)
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Dim serializer As New XmlSerializer(GetType(BECrossSelling))
            Do While rd.Read
                oBE = New BECrossSelling
                Try
                    Dim infoXML As System.Data.SqlTypes.SqlXml = rd.GetSqlXml(rd.GetOrdinal("InformacionAdicional"))
                    If (Not infoXML Is Nothing) Then
                        oBE = CType(serializer.Deserialize(infoXML.CreateReader()), BECrossSelling)
                    End If
                Catch ex As Exception

                End Try
                If (oBE Is Nothing) Then
                    oBE = New BECrossSelling
                End If
                oBE.IDProducto = rd.GetInt32(rd.GetOrdinal("IdProducto"))
                oBE.TipoProducto = rd.GetString(rd.GetOrdinal("TipoProducto"))
                oBE.Nombre = rd.GetString(rd.GetOrdinal("Nombre"))
                oBE.NombreTipoProducto = rd.GetString(rd.GetOrdinal("NombreTipoProducto"))
                oBE.VentaHabilitada = rd.GetBoolean(rd.GetOrdinal("VentaHabilitada"))
                oBE.TieneSolicitudContacto = rd.GetBoolean(rd.GetOrdinal("TieneSolicitudContacto"))
                oBE.TieneOtroCertificado = rd.GetBoolean(rd.GetOrdinal("TieneOtroCertificado"))
                oBE.TieneMedioPagoHabilitado = rd.GetBoolean(rd.GetOrdinal("TieneMedioPagoHabilitado"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

End Class
