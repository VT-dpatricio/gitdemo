﻿Imports VAN.InsuranceWeb.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO


Public Class DLTPostExpedirCert
    Public Function EjecutarPostExpedir(ByVal pIdCertificado As String, ByVal pSqlCon As SqlConnection, ByVal pSqlTranx As SqlTransaction) As Int32
        Dim filasAfectadas As Int32 = -1
        Try
            Using sqlCmdInsInfAsegC As New SqlCommand("BW_EjecutarPostExpedirCert", pSqlCon)
                sqlCmdInsInfAsegC.Transaction = pSqlTranx
                sqlCmdInsInfAsegC.CommandType = CommandType.StoredProcedure
                Dim sqlIdCertificado As SqlParameter = sqlCmdInsInfAsegC.Parameters.Add("@idCertificado", SqlDbType.VarChar, 25)
                sqlIdCertificado.Direction = ParameterDirection.Input
                sqlIdCertificado.Value = pIdCertificado
                sqlCmdInsInfAsegC.ExecuteNonQuery()
                filasAfectadas = 1
            End Using
        Catch ex As Exception
            filasAfectadas = -1
        End Try
        Return filasAfectadas
    End Function
End Class
