﻿Imports VAN.InsuranceWeb.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLTAsegurado
    ''' <summary>
    ''' Inserta un asegurado en la base de datos.
    ''' </summary>
    ''' <creador>
    ''' Diego Morales Rivero
    ''' </creador>
    ''' <fechacreacion>
    ''' 2010-11-29
    ''' </fechacreacion>
    ''' <param name="pBEAsegurado">Asegurado a insertar.</param>
    ''' <param name="pSqlCon">Objeto SqlConnection</param>
    ''' <param name="pSqlTranx">Objeto Transaction</param>
    ''' <returns>
    ''' En caso exitoso devuelve 1, caso contrario -1.
    ''' </returns>
    ''' 
    Public Function DAInsertarAsegurado(ByVal pBEAsegurado As BEAsegurado, ByVal pSqlCon As SqlConnection, ByVal pSqlTranx As SqlTransaction) As Int32
        Dim filasAfectadas As Int32 = -1

        Using sqlCmdInsAsegurado As New SqlCommand("BW_InsertarAsegurado", pSqlCon)
            sqlCmdInsAsegurado.Transaction = pSqlTranx
            sqlCmdInsAsegurado.CommandType = CommandType.StoredProcedure

            Dim sqlIdCertificado As SqlParameter = sqlCmdInsAsegurado.Parameters.Add("@idCertificado", SqlDbType.VarChar, 25)
            sqlIdCertificado.Direction = ParameterDirection.Input
            sqlIdCertificado.Value = pBEAsegurado.IdCertificado

            Dim sqlConsecutivo As SqlParameter = sqlCmdInsAsegurado.Parameters.Add("@consecutivo", SqlDbType.SmallInt)
            sqlConsecutivo.Direction = ParameterDirection.Input
            sqlConsecutivo.Value = pBEAsegurado.Consecutivo

            Dim sqlIdTipoDocumento As SqlParameter = sqlCmdInsAsegurado.Parameters.Add("@idTipoDocumento", SqlDbType.VarChar, 3)
            sqlIdTipoDocumento.Direction = ParameterDirection.Input
            sqlIdTipoDocumento.Value = pBEAsegurado.IdTipodocumento

            Dim sqlCcAseg As SqlParameter = sqlCmdInsAsegurado.Parameters.Add("@ccAseg", SqlDbType.VarChar, 20)
            sqlCcAseg.Direction = ParameterDirection.Input
            sqlCcAseg.Value = pBEAsegurado.Ccaseg

            Dim sqlNombre1 As SqlParameter = sqlCmdInsAsegurado.Parameters.Add("@nombre1", SqlDbType.VarChar, 60)
            sqlNombre1.Direction = ParameterDirection.Input
            sqlNombre1.Value = pBEAsegurado.Nombre1

            Dim sqlNombre2 As SqlParameter = sqlCmdInsAsegurado.Parameters.Add("@nombre2", SqlDbType.VarChar, 60)
            sqlNombre2.Direction = ParameterDirection.Input

            If pBEAsegurado.Nombre2 = [String].Empty Then
                sqlNombre2.Value = DBNull.Value
            Else
                sqlNombre2.Value = pBEAsegurado.Nombre2
            End If

            Dim sqlApellido1 As SqlParameter = sqlCmdInsAsegurado.Parameters.Add("@apellido1", SqlDbType.VarChar, 60)
            sqlApellido1.Direction = ParameterDirection.Input
            sqlApellido1.Value = pBEAsegurado.Apellido1

            Dim sqlApellido2 As SqlParameter = sqlCmdInsAsegurado.Parameters.Add("@apellido2", SqlDbType.VarChar, 60)
            sqlApellido2.Direction = ParameterDirection.Input

            If pBEAsegurado.Apellido2 = [String].Empty Then
                sqlApellido2.Value = DBNull.Value
            Else
                sqlApellido2.Value = pBEAsegurado.Apellido2
            End If

            Dim sqlDireccion As SqlParameter = sqlCmdInsAsegurado.Parameters.Add("@direccion", SqlDbType.VarChar, 250)
            sqlDireccion.Direction = ParameterDirection.Input

            If pBEAsegurado.Direccion = [String].Empty Then
                sqlDireccion.Value = DBNull.Value
            Else
                sqlDireccion.Value = pBEAsegurado.Direccion
            End If

            Dim sqlTelefono As SqlParameter = sqlCmdInsAsegurado.Parameters.Add("@telefono", SqlDbType.VarChar, 15)
            sqlTelefono.Direction = ParameterDirection.Input

            If pBEAsegurado.Telefono = [String].Empty Then
                sqlTelefono.Value = DBNull.Value
            Else
                sqlTelefono.Value = pBEAsegurado.Telefono
            End If

            Dim sqlIdCiudad As SqlParameter = sqlCmdInsAsegurado.Parameters.Add("@idciudad", SqlDbType.Int)
            sqlIdCiudad.Direction = ParameterDirection.Input

            If pBEAsegurado.IdCiudad = -1 Then
                sqlIdCiudad.Value = DBNull.Value
            Else
                sqlIdCiudad.Value = pBEAsegurado.IdCiudad
            End If

            Dim sqlFechaNacimiento As SqlParameter = sqlCmdInsAsegurado.Parameters.Add("@fechanacimiento", SqlDbType.SmallDateTime)
            sqlFechaNacimiento.Direction = ParameterDirection.Input

            If pBEAsegurado.FechaNacimiento = DateTime.Parse("1900-01-01") Then
                sqlFechaNacimiento.Value = DBNull.Value
            Else
                sqlFechaNacimiento.Value = pBEAsegurado.FechaNacimiento
            End If

            Dim sqlIdParentesco As SqlParameter = sqlCmdInsAsegurado.Parameters.Add("@idparentesco", SqlDbType.SmallInt)
            sqlIdParentesco.Direction = ParameterDirection.Input

            If pBEAsegurado.IdParentesco = -1 Then

                sqlIdParentesco.Value = DBNull.Value
            Else
                sqlIdParentesco.Value = pBEAsegurado.IdParentesco
            End If

            Dim sqlActivo As SqlParameter = sqlCmdInsAsegurado.Parameters.Add("@activo", SqlDbType.Bit)
            sqlActivo.Direction = ParameterDirection.Input
            sqlActivo.Value = pBEAsegurado.Activo

            filasAfectadas = sqlCmdInsAsegurado.ExecuteNonQuery()
        End Using

        If filasAfectadas <= 0 Then
            Return -1
        Else
            Return filasAfectadas
        End If
    End Function

End Class
