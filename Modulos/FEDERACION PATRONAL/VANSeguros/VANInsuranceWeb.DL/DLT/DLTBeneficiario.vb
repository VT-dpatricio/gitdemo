﻿Imports VAN.InsuranceWeb.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient


Public Class DLTBeneficiario
    ''' <summary>
    ''' Inserta un beneficiario en la base de datos.
    ''' </summary>
    ''' <creador>
    ''' Diego Morales Rivero
    ''' </creador>
    ''' <fechacreacion>
    ''' 2010-11-18
    ''' </fechacreacion>
    ''' <param name="pObjBEBeneficiario">Objeto BEBeneficiario con la data del beneficiario.</param>
    ''' <param name="pSqlCon">Objeto SqlConnection</param>
    ''' <returns>
    ''' Retorna la cantidad de filas afectadas.
    ''' </returns>
    Public Function DAInsertarBeneficiario(ByVal pObjBEBeneficiario As BEBeneficiario, ByVal pSqlCon As SqlConnection, ByVal pSqlTranx As SqlTransaction) As Int32
        Dim filasAfectadas As Int32 = -1

        Using sqlCmdInsBeneficiario As New SqlCommand("BW_InsertarBeneficiario", pSqlCon)
            sqlCmdInsBeneficiario.Transaction = pSqlTranx
            sqlCmdInsBeneficiario.CommandType = CommandType.StoredProcedure

            Dim sqlIdCertificado As SqlParameter = sqlCmdInsBeneficiario.Parameters.Add("@idCertificado", SqlDbType.VarChar, 25)
            sqlIdCertificado.Direction = ParameterDirection.Input
            sqlIdCertificado.Value = pObjBEBeneficiario.IdCertificado

            Dim sqlConsecutivo As SqlParameter = sqlCmdInsBeneficiario.Parameters.Add("@consecutivo", SqlDbType.SmallInt)
            sqlConsecutivo.Direction = ParameterDirection.Input
            sqlConsecutivo.Value = pObjBEBeneficiario.Consecutivo

            Dim sqlCCBenef As SqlParameter = sqlCmdInsBeneficiario.Parameters.Add("@ccBenef", SqlDbType.VarChar, 20)
            sqlCCBenef.Direction = ParameterDirection.Input
            If pObjBEBeneficiario.CcBenef = [String].Empty Then

                sqlCCBenef.Value = DBNull.Value
            Else
                sqlCCBenef.Value = pObjBEBeneficiario.CcBenef
            End If

            Dim sqlIdTipoDocumento As SqlParameter = sqlCmdInsBeneficiario.Parameters.Add("@idTipoDocumento", SqlDbType.VarChar, 3)
            sqlIdTipoDocumento.Direction = ParameterDirection.Input
            If pObjBEBeneficiario.IdTipoDocumento = [String].Empty Then
                sqlIdTipoDocumento.Value = DBNull.Value
            Else
                sqlIdTipoDocumento.Value = pObjBEBeneficiario.IdTipoDocumento
            End If

            Dim sqlPorcentaje As SqlParameter = sqlCmdInsBeneficiario.Parameters.Add("@porcentaje", SqlDbType.[Decimal])
            sqlPorcentaje.Direction = ParameterDirection.Input
            sqlPorcentaje.Value = pObjBEBeneficiario.Porcentaje

            Dim sqlNombre1 As SqlParameter = sqlCmdInsBeneficiario.Parameters.Add("@nombre1", SqlDbType.VarChar, 60)
            sqlNombre1.Direction = ParameterDirection.Input
            sqlNombre1.Value = pObjBEBeneficiario.Nombre1

            Dim sqlNombre2 As SqlParameter = sqlCmdInsBeneficiario.Parameters.Add("@nombre2", SqlDbType.VarChar, 60)
            sqlNombre2.Direction = ParameterDirection.Input
            If pObjBEBeneficiario.Nombre2 = [String].Empty Then
                sqlNombre2.Value = DBNull.Value
            Else
                sqlNombre2.Value = pObjBEBeneficiario.Nombre2
            End If

            Dim sqlApellido1 As SqlParameter = sqlCmdInsBeneficiario.Parameters.Add("@apellido1", SqlDbType.VarChar, 60)
            sqlApellido1.Direction = ParameterDirection.Input
            sqlApellido1.Value = pObjBEBeneficiario.Apellido1

            Dim sqlApellido2 As SqlParameter = sqlCmdInsBeneficiario.Parameters.Add("@apellido2", SqlDbType.VarChar, 60)
            sqlApellido2.Direction = ParameterDirection.Input
            If pObjBEBeneficiario.Apellido2 = [String].Empty Then
                sqlApellido2.Value = DBNull.Value
            Else
                sqlApellido2.Value = pObjBEBeneficiario.Apellido2
            End If

            Dim sqlIdParentesco As SqlParameter = sqlCmdInsBeneficiario.Parameters.Add("@idparentesco", SqlDbType.SmallInt)
            sqlIdParentesco.Direction = ParameterDirection.Input
            sqlIdParentesco.Value = pObjBEBeneficiario.IdParentesco

            Dim sqlActivo As SqlParameter = sqlCmdInsBeneficiario.Parameters.Add("@activo", SqlDbType.Bit)
            sqlActivo.Direction = ParameterDirection.Input
            sqlActivo.Value = pObjBEBeneficiario.Activo

            Dim sqlDomicilio As SqlParameter = sqlCmdInsBeneficiario.Parameters.Add("@domicilio", SqlDbType.VarChar, 100)
            sqlDomicilio.Direction = ParameterDirection.Input
            If pObjBEBeneficiario.Domicilio = [String].Empty Then
                sqlDomicilio.Value = DBNull.Value
            Else
                sqlDomicilio.Value = pObjBEBeneficiario.Domicilio
            End If

            Dim sqlFechaNacimiento As SqlParameter = sqlCmdInsBeneficiario.Parameters.Add("@fechanacimiento", SqlDbType.SmallDateTime)
            sqlFechaNacimiento.Direction = ParameterDirection.Input
            If pObjBEBeneficiario.FechaNacimiento = DateTime.Parse("1900-01-01") Then
                sqlFechaNacimiento.Value = DBNull.Value
            Else
                sqlFechaNacimiento.Value = pObjBEBeneficiario.FechaNacimiento
            End If

            Dim sqlOcupacion As SqlParameter = sqlCmdInsBeneficiario.Parameters.Add("@ocupacion", SqlDbType.VarChar, 80)
            sqlOcupacion.Direction = ParameterDirection.Input
            If pObjBEBeneficiario.Ocupacion = [String].Empty Then
                sqlOcupacion.Value = DBNull.Value
            Else
                sqlOcupacion.Value = pObjBEBeneficiario.Ocupacion
            End If

            Dim sqlTelefono As SqlParameter = sqlCmdInsBeneficiario.Parameters.Add("@telefono", SqlDbType.VarChar, 30)
            sqlTelefono.Direction = ParameterDirection.Input
            If pObjBEBeneficiario.Telefono = [String].Empty Then
                sqlTelefono.Value = DBNull.Value
            Else
                sqlTelefono.Value = pObjBEBeneficiario.Telefono
            End If

            Dim sqlSexo As SqlParameter = sqlCmdInsBeneficiario.Parameters.Add("@sexo", SqlDbType.VarChar, 5)
            sqlSexo.Direction = ParameterDirection.Input
            If pObjBEBeneficiario.Sexo = [String].Empty Then
                sqlSexo.Value = DBNull.Value
            Else
                sqlSexo.Value = pObjBEBeneficiario.Sexo
            End If

            filasAfectadas = sqlCmdInsBeneficiario.ExecuteNonQuery()
        End Using

        If filasAfectadas <= 0 Then
            Return -1
        Else
            Return filasAfectadas
        End If
    End Function
End Class
