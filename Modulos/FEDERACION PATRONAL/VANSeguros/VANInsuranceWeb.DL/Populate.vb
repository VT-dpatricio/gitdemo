﻿Imports VAN.InsuranceWeb.BE

Public NotInheritable Class Populate

#Region " CertificadoBE "
    Public Shared Function Certificado_Lista(ByVal dr As IDataRecord) As CertificadoBE
        Dim Entidad As New CertificadoBE
        If Not dr("IdCertificado") Is DBNull.Value Then
            Entidad.IdCertificado = CType(dr("IdCertificado"), Integer)
        End If
        If Not dr("IdProducto") Is DBNull.Value Then
            Entidad.IdProducto = CType(dr("IdProducto"), Integer)
        End If
        If Not dr("IdAseguradora") Is DBNull.Value Then
            Entidad.IdAseguradora = CType(dr("IdAseguradora"), Integer)
        End If
        If Not dr("NumeroCertificado") Is DBNull.Value Then
            Entidad.NumeroCertificado = CType(dr("NumeroCertificado"), String)
        End If
        If Not dr("FechaEmision") Is DBNull.Value Then
            Entidad.FechaEmision = CType(dr("FechaEmision"), DateTime)
        End If
        If Not dr("FechaInicio") Is DBNull.Value Then
            Entidad.FechaInicio = CType(dr("FechaInicio"), DateTime)
        End If
        If Not dr("FechaFin") Is DBNull.Value Then
            Entidad.FechaFin = CType(dr("FechaFin"), DateTime)
        End If
        If Not dr("PrimaNetaMensual") Is DBNull.Value Then
            Entidad.PrimaNetaMensual = CType(dr("PrimaNetaMensual"), Decimal)
        End If
        If Not dr("PrimaBrutaMensual") Is DBNull.Value Then
            Entidad.PrimaBrutaMensual = CType(dr("PrimaBrutaMensual"), Decimal)
        End If
        If Not dr("PrimaNetaAnual") Is DBNull.Value Then
            Entidad.PrimaNetaAnual = CType(dr("PrimaNetaAnual"), Decimal)
        End If
        If Not dr("PrimanBrutaAnual") Is DBNull.Value Then
            Entidad.PrimanBrutaAnual = CType(dr("PrimanBrutaAnual"), Decimal)
        End If
        If Not dr("Poliza") Is DBNull.Value Then
            Entidad.Poliza = CType(dr("Poliza"), String)
        End If
        If Not dr("Obs") Is DBNull.Value Then
            Entidad.Obs = CType(dr("Obs"), String)
        End If
        If Not dr("Estado") Is DBNull.Value Then
            Entidad.Estado = CType(dr("Estado"), String)
        End If
        If Not dr("UsuarioReg") Is DBNull.Value Then
            Entidad.UsuarioReg = CType(dr("UsuarioReg"), String)
        End If
        If Not dr("FechaReg") Is DBNull.Value Then
            Entidad.FechaReg = CType(dr("FechaReg"), DateTime)
        End If
        If Not dr("UsuarioMod") Is DBNull.Value Then
            Entidad.UsuarioMod = CType(dr("UsuarioMod"), String)
        End If
        If Not dr("FechaMod") Is DBNull.Value Then
            Entidad.FechaMod = CType(dr("FechaMod"), DateTime)
        End If
        Return Entidad
    End Function
#End Region

#Region " CertificadoDetalleInfoProductoBE "
    Public Shared Function CertificadoDetalleInfoProducto_Lista(ByVal dr As IDataRecord) As CertificadoDetalleInfoProductoBE
        Dim Entidad As New CertificadoDetalleInfoProductoBE

        If Not dr("IdCertificadoDetalleInfoProducto") Is DBNull.Value Then
            Entidad.IdCertificadoDetalleInfoProducto = CType(dr("IdCertificadoDetalleInfoProducto"), Integer)
        End If
        If Not dr("IdProducto") Is DBNull.Value Then
            Entidad.IdProducto = CType(dr("IdProducto"), Integer)
        End If
        If Not dr("IdInfoProducto") Is DBNull.Value Then
            Entidad.IdInfoProducto = CType(dr("IdInfoProducto"), Integer)
        End If
        If Not dr("EstaIdDetalleInfoProductodo") Is DBNull.Value Then
            Entidad.IdDetalleInfoProducto = CType(dr("IdDetalleInfoProducto"), Integer)
        End If
        If Not dr("IdCertificado") Is DBNull.Value Then
            Entidad.IdCertificado = CType(dr("IdCertificado"), Integer)
        End If
        If Not dr("Valor") Is DBNull.Value Then
            Entidad.Valor = CType(dr("Valor"), String)
        End If

        If Not dr("Estado") Is DBNull.Value Then
            Entidad.Estado = CType(dr("Estado"), String)
        End If
        If Not dr("UsuarioReg") Is DBNull.Value Then
            Entidad.UsuarioReg = CType(dr("UsuarioReg"), String)
        End If
        If Not dr("FechaReg") Is DBNull.Value Then
            Entidad.FechaReg = CType(dr("FechaReg"), DateTime)
        End If
        If Not dr("UsuarioMod") Is DBNull.Value Then
            Entidad.UsuarioMod = CType(dr("UsuarioMod"), String)
        End If
        If Not dr("FechaMod") Is DBNull.Value Then
            Entidad.FechaMod = CType(dr("FechaMod"), DateTime)
        End If
        Return Entidad
    End Function
#End Region

#Region " CertificadoInfoProductoBE "
    Public Shared Function CertificadoInfoProducto_Lista(ByVal dr As IDataRecord) As CertificadoInfoProductoBE
        Dim Entidad As New CertificadoInfoProductoBE


        If Not dr("IdCertificadoInfoProducto") Is DBNull.Value Then
            Entidad.IdCertificadoInfoProducto = CType(dr("IdCertificadoInfoProducto"), Integer)
        End If

        If Not dr("IdProducto") Is DBNull.Value Then
            Entidad.IdProducto = CType(dr("IdProducto"), Integer)
        End If

        If Not dr("IdInfoProducto") Is DBNull.Value Then
            Entidad.IdInfoProducto = CType(dr("IdInfoProducto"), Integer)
        End If


        If Not dr("IdCertificado") Is DBNull.Value Then
            Entidad.IdCertificado = CType(dr("IdCertificado"), Integer)
        End If


        If Not dr("Estado") Is DBNull.Value Then
            Entidad.Estado = CType(dr("Estado"), String)
        End If
        If Not dr("UsuarioReg") Is DBNull.Value Then
            Entidad.UsuarioReg = CType(dr("UsuarioReg"), String)
        End If
        If Not dr("FechaReg") Is DBNull.Value Then
            Entidad.FechaReg = CType(dr("FechaReg"), DateTime)
        End If
        If Not dr("UsuarioMod") Is DBNull.Value Then
            Entidad.UsuarioMod = CType(dr("UsuarioMod"), String)
        End If
        If Not dr("FechaMod") Is DBNull.Value Then
            Entidad.FechaMod = CType(dr("FechaMod"), DateTime)
        End If

        Return Entidad
    End Function
#End Region

#Region " DetalleInforProductoBE "
    Public Shared Function DetalleInfoProducto_Lista(ByVal dr As IDataRecord) As DetalleInfoProductoBE
        Dim Entidad As New DetalleInfoProductoBE
        If Not dr("IdDetalleInfoProducto") Is DBNull.Value Then
            Entidad.IdDetalleInfoProducto = CType(dr("IdDetalleInfoProducto"), Integer)
        End If
        If Not dr("IdInfoProducto") Is DBNull.Value Then
            Entidad.IdInfoProducto = CType(dr("IdInfoProducto"), Integer)
        End If
        If Not dr("IdTipoValor") Is DBNull.Value Then
            Entidad.IdTipoValor = CType(dr("IdTipoValor"), Integer)
        End If

        If Not dr("IdTipoValor") Is DBNull.Value Then
            Entidad.IdTipoValor = CType(dr("IdTipoValor"), Integer)
            Entidad.TipoValorBE.IdTipoValor = Entidad.IdTipoValor
        End If

        Try
            If Not dr("TipoValor") Is DBNull.Value Then
                Entidad.TipoValor = CType(dr("TipoValor"), String)
                Entidad.TipoValorBE.Descripcion = Entidad.Descripcion
            End If
        Catch ex As Exception
        End Try

        If Not dr("Descripcion") Is DBNull.Value Then
            Entidad.Descripcion = CType(dr("Descripcion"), String)
        End If



        If Not dr("Estado") Is DBNull.Value Then
            Entidad.Estado = CType(dr("Estado"), String)
        End If
        If Not dr("UsuarioReg") Is DBNull.Value Then
            Entidad.UsuarioReg = CType(dr("UsuarioReg"), String)
        End If
        If Not dr("FechaReg") Is DBNull.Value Then
            Entidad.FechaReg = CType(dr("FechaReg"), DateTime)
        End If
        If Not dr("UsuarioMod") Is DBNull.Value Then
            Entidad.UsuarioMod = CType(dr("UsuarioMod"), String)
        End If
        If Not dr("FechaMod") Is DBNull.Value Then
            Entidad.FechaMod = CType(dr("FechaMod"), DateTime)
        End If
        Return Entidad
    End Function
#End Region

#Region " EntidadBE "
    Public Shared Function Entidad_Lista(ByVal dr As IDataRecord) As EntidadBE
        Dim Entidad As New EntidadBE
        If Not dr("IdEntidad") Is DBNull.Value Then
            Entidad.IdEntidad = CType(dr("IdEntidad"), Integer)
        End If
        If Not dr("Descripcion") Is DBNull.Value Then
            Entidad.Descripcion = CType(dr("Descripcion"), String)
        End If
        If Not dr("Abrev") Is DBNull.Value Then
            Entidad.Abrev = CType(dr("Abrev"), String)
        End If


        If Not dr("Estado") Is DBNull.Value Then
            Entidad.Estado = CType(dr("Estado"), String)
        End If
        If Not dr("UsuarioReg") Is DBNull.Value Then
            Entidad.UsuarioReg = CType(dr("UsuarioReg"), String)
        End If
        If Not dr("FechaReg") Is DBNull.Value Then
            Entidad.FechaReg = CType(dr("FechaReg"), DateTime)
        End If
        If Not dr("UsuarioMod") Is DBNull.Value Then
            Entidad.UsuarioMod = CType(dr("UsuarioMod"), String)
        End If
        If Not dr("FechaMod") Is DBNull.Value Then
            Entidad.FechaMod = CType(dr("FechaMod"), DateTime)
        End If
        Return Entidad
    End Function
#End Region

#Region " FormaPagoBE "
    Public Shared Function FormaPago_Lista(ByVal dr As IDataRecord) As FormaPagoBE
        Dim Entidad As New FormaPagoBE
        If Not dr("IdFormaPago") Is DBNull.Value Then
            Entidad.IdFormaPago = CType(dr("IdFormaPago"), Integer)
        End If
        If Not dr("IdCertificado") Is DBNull.Value Then
            Entidad.IdCertificado = CType(dr("IdCertificado"), Integer)
        End If
        If Not dr("TipoFormaPago") Is DBNull.Value Then
            Entidad.TipoFormaPago = CType(dr("TipoFormaPago"), String)
        End If
        If Not dr("Banco") Is DBNull.Value Then
            Entidad.Banco = CType(dr("Banco"), String)
        End If
        If Not dr("Documento") Is DBNull.Value Then
            Entidad.Documento = CType(dr("Documento"), String)
        End If

        If Not dr("Estado") Is DBNull.Value Then
            Entidad.Estado = CType(dr("Estado"), String)
        End If
        If Not dr("UsuarioReg") Is DBNull.Value Then
            Entidad.UsuarioReg = CType(dr("UsuarioReg"), String)
        End If
        If Not dr("FechaReg") Is DBNull.Value Then
            Entidad.FechaReg = CType(dr("FechaReg"), DateTime)
        End If
        If Not dr("UsuarioMod") Is DBNull.Value Then
            Entidad.UsuarioMod = CType(dr("UsuarioMod"), String)
        End If
        If Not dr("FechaMod") Is DBNull.Value Then
            Entidad.FechaMod = CType(dr("FechaMod"), DateTime)
        End If
        Return Entidad
    End Function
#End Region

#Region " InfoProductoBE "
    Public Shared Function InfoProducto_Lista(ByVal dr As IDataRecord) As InfoProductoBE
        Dim Entidad As New InfoProductoBE

        If Not dr("IdInfoProducto") Is DBNull.Value Then
            Entidad.IdInfoProducto = CType(dr("IdInfoProducto"), Integer)
        End If
        If Not dr("IdTipoInfoProducto") Is DBNull.Value Then
            Entidad.IdTipoInfoProducto = CType(dr("IdTipoInfoProducto"), Integer)
        End If
        If Not dr("Descripcion") Is DBNull.Value Then
            Entidad.Descripcion = CType(dr("Descripcion"), String)
        End If
        If Not dr("Abrev") Is DBNull.Value Then
            Entidad.Abrev = CType(dr("Abrev"), String)
        End If
        If Not dr("Panel") Is DBNull.Value Then
            Entidad.Panel = CType(dr("Panel"), Boolean)
        End If
        If Not dr("Estado") Is DBNull.Value Then
            Entidad.Estado = CType(dr("Estado"), String)
        End If
        If Not dr("UsuarioReg") Is DBNull.Value Then
            Entidad.UsuarioReg = CType(dr("UsuarioReg"), String)
        End If
        If Not dr("FechaReg") Is DBNull.Value Then
            Entidad.FechaReg = CType(dr("FechaReg"), DateTime)
        End If
        If Not dr("UsuarioMod") Is DBNull.Value Then
            Entidad.UsuarioMod = CType(dr("UsuarioMod"), String)
        End If
        If Not dr("FechaMod") Is DBNull.Value Then
            Entidad.FechaMod = CType(dr("FechaMod"), DateTime)
        End If
        Return Entidad
    End Function

    Public Shared Function InfoProducto_ListaBDJanus(ByVal dr As IDataRecord) As InfoProductoBE
        Dim Entidad As New InfoProductoBE
        If Not dr("Descripcion") Is DBNull.Value Then
            Entidad.Descripcion = CType(dr("Descripcion"), String)
        End If
        If Not dr("Abrev") Is DBNull.Value Then
            Entidad.Abrev = CType(dr("Abrev"), String)
        End If
        If Not dr("Producto") Is DBNull.Value Then
            Entidad.Filler = CType(dr("Producto"), String)
        End If
        Return Entidad
    End Function
#End Region

#Region " ObjetoEntidadBE "
    Public Shared Function ObjetoEntidad_Lista(ByVal dr As IDataRecord) As ObjetoEntidadBE
        Dim Entidad As New ObjetoEntidadBE
        If Not dr("IdObjetoEntidad") Is DBNull.Value Then
            Entidad.IdObjetoEntidad = CType(dr("IdObjetoEntidad"), Integer)
        End If
        If Not dr("IdEntidad") Is DBNull.Value Then
            Entidad.IdEntidad = CType(dr("IdEntidad"), Integer)
        End If
        If Not dr("Objeto") Is DBNull.Value Then
            Entidad.Objeto = CType(dr("Objeto"), String)
        End If


        If Not dr("Estado") Is DBNull.Value Then
            Entidad.Estado = CType(dr("Estado"), String)
        End If
        If Not dr("UsuarioReg") Is DBNull.Value Then
            Entidad.UsuarioReg = CType(dr("UsuarioReg"), String)
        End If
        If Not dr("FechaReg") Is DBNull.Value Then
            Entidad.FechaReg = CType(dr("FechaReg"), DateTime)
        End If
        If Not dr("UsuarioMod") Is DBNull.Value Then
            Entidad.UsuarioMod = CType(dr("UsuarioMod"), String)
        End If
        If Not dr("FechaMod") Is DBNull.Value Then
            Entidad.FechaMod = CType(dr("FechaMod"), DateTime)
        End If
        Return Entidad
    End Function
#End Region

#Region " PersonaBE "
    Public Shared Function Persona_Lista(ByVal dr As IDataRecord) As PersonaBE
        Dim Entidad As New PersonaBE

        If Not dr("IdPersona") Is DBNull.Value Then
            Entidad.IdPersona = CType(dr("IdPersona"), Integer)
        End If
        If Not dr("TipoDocumento") Is DBNull.Value Then
            Entidad.TipoDocumento = CType(dr("TipoDocumento"), String)
        End If
        If Not dr("NumeroDocumento") Is DBNull.Value Then
            Entidad.NumeroDocumento = CType(dr("NumeroDocumento"), String)
        End If
        If Not dr("Nombre") Is DBNull.Value Then
            Entidad.Nombre = CType(dr("Nombre"), String)
        End If
        If Not dr("Nombre2") Is DBNull.Value Then
            Entidad.Nombre2 = CType(dr("Nombre2"), String)
        End If
        If Not dr("ApellidoPaterno") Is DBNull.Value Then
            Entidad.ApellidoPaterno = CType(dr("ApellidoPaterno"), String)
        End If
        If Not dr("ApellidoMaterno") Is DBNull.Value Then
            Entidad.ApellidoMaterno = CType(dr("ApellidoMaterno"), String)
        End If
        If Not dr("NombreCompleto") Is DBNull.Value Then
            Entidad.NombreCompleto = CType(dr("NombreCompleto"), String)
        End If
        If Not dr("RazonSocial") Is DBNull.Value Then
            Entidad.RazonSocial = CType(dr("RazonSocial"), String)
        End If
        If Not dr("FechaNacimiento") Is DBNull.Value Then
            Entidad.FechaNacimiento = CType(dr("FechaNacimiento"), DateTime)
        End If
        If Not dr("Direccion") Is DBNull.Value Then
            Entidad.Direccion = CType(dr("Direccion"), String)
        End If
        If Not dr("Telefono") Is DBNull.Value Then
            Entidad.Telefono = CType(dr("Telefono"), String)
        End If

        If Not dr("Sexo") Is DBNull.Value Then
            Entidad.Sexo = CType(dr("Sexo"), String)
        End If
        If Not dr("Estado") Is DBNull.Value Then
            Entidad.Estado = CType(dr("Estado"), String)
        End If
        If Not dr("UsuarioReg") Is DBNull.Value Then
            Entidad.UsuarioReg = CType(dr("UsuarioReg"), String)
        End If
        If Not dr("FechaReg") Is DBNull.Value Then
            Entidad.FechaReg = CType(dr("FechaReg"), DateTime)
        End If
        If Not dr("UsuarioMod") Is DBNull.Value Then
            Entidad.UsuarioMod = CType(dr("UsuarioMod"), String)
        End If
        If Not dr("FechaMod") Is DBNull.Value Then
            Entidad.FechaMod = CType(dr("FechaMod"), DateTime)
        End If
        Return Entidad
    End Function
#End Region

#Region " ProductoBE "
    Public Shared Function Producto_Lista(ByVal dr As IDataRecord) As ProductoBE
        Dim Entidad As New ProductoBE

        If Not dr("IdProducto") Is DBNull.Value Then
            Entidad.IdProducto = CType(dr("IdProducto"), Integer)
        End If
        If Not dr("Descripcion") Is DBNull.Value Then
            Entidad.Descripcion = CType(dr("Descripcion"), String)
        End If
        If Not dr("Abrev") Is DBNull.Value Then
            Entidad.Abrev = CType(dr("Abrev"), String)
        End If
      
        If Not dr("Logo") Is DBNull.Value Then
            Entidad.Logo = CType(dr("Logo"), String)
        End If
        If Not dr("Banner") Is DBNull.Value Then
            Entidad.Banner = CType(dr("Banner"), String)
        End If
        If Not dr("Reporte") Is DBNull.Value Then
            Entidad.Reporte = CType(dr("Reporte"), String)
        End If
        If Not dr("Estado") Is DBNull.Value Then
            Entidad.Estado = CType(dr("Estado"), String)
        End If
        If Not dr("UsuarioReg") Is DBNull.Value Then
            Entidad.UsuarioReg = CType(dr("UsuarioReg"), String)
        End If
        If Not dr("FechaReg") Is DBNull.Value Then
            Entidad.FechaReg = CType(dr("FechaReg"), DateTime)
        End If
        If Not dr("UsuarioMod") Is DBNull.Value Then
            Entidad.UsuarioMod = CType(dr("UsuarioMod"), String)
        End If
        If Not dr("FechaMod") Is DBNull.Value Then
            Entidad.FechaMod = CType(dr("FechaMod"), DateTime)
        End If

        Try
            If Not dr("vEdad") Is DBNull.Value Then
                Entidad.vEdad = CType(dr("vEdad"), Boolean)
            End If

            If Not dr("vMonto") Is DBNull.Value Then
                Entidad.vMonto = CType(dr("vMonto"), Boolean)
            End If

        Catch ex As Exception

        End Try

        Return Entidad
    End Function

    Public Shared Function Producto_ListaBDJanus(ByVal dr As IDataRecord) As ProductoBE
        Dim Entidad As New ProductoBE
        If Not dr("IdProducto") Is DBNull.Value Then
            Entidad.IdProducto = CType(dr("IdProducto"), Integer)
        End If
        If Not dr("Producto") Is DBNull.Value Then
            Entidad.Descripcion = CType(dr("Producto"), String)
        End If
        Return Entidad
    End Function


    Public Shared Function Producto_ListaBDJanusBAIS(ByVal dr As IDataRecord) As ProductoBE
        Dim Entidad As New ProductoBE

        If Not dr("IdProducto") Is DBNull.Value Then
            Entidad.IdProducto = CType(dr("IdProducto"), Integer)
        End If
        If Not dr("nombre") Is DBNull.Value Then
            Entidad.Descripcion = CType(dr("nombre"), String)
        End If

        Try

            Entidad.IdAseguradora = CType(dr("idAsegurador"), Integer)
            Entidad.NombreAsegurador = CType(dr("NombreAseguradora"), String)
            Entidad.idTipoProducto = CType(dr("idTipoProducto"), String)
            Entidad.activo = CType(dr("activo"), Boolean)
            Entidad.maxAsegurados = CType(dr("maxAsegurados"), Integer)
            Entidad.nacimientoRequerido = CType(dr("nacimientoRequerido"), Boolean)
            Entidad.validarParentesco = CType(dr("validarParentesco"), Boolean)
            Entidad.obligaCuentaHabiente = CType(dr("obligaCuentaHabiente"), Boolean)
            Entidad.cumuloMaximo = CType(dr("cumuloMaximo"), Integer)
            Entidad.edadMaxPermanencia = CType(dr("edadMaxPermanencia"), Integer)
            Entidad.diasAnulacion = CType(dr("diasAnulacion"), Integer)
            Entidad.generaCobro = CType(dr("generaCobro"), Boolean)
            Entidad.idEntidad = CType(dr("idEntidad"), Integer)
            Entidad.NombreEntidad = CType(dr("NombreEntidad"), String)
            Entidad.vigenciaCobro = CType(dr("vigenciaCobro"), Boolean)
            Entidad.maxPolizasAsegurado = CType(dr("maxPolizasAsegurado"), Integer)
            Entidad.maxPolizasCuentahabiente = CType(dr("maxPolizasCuentahabiente"), Integer)
            Entidad.maxPolizasRegalo = CType(dr("maxPolizasRegalo"), Integer)
            Entidad.idReglaCobro = CType(dr("idReglaCobro"), Integer)
            Entidad.maxCuentasTitular = CType(dr("maxCuentasTitular"), Integer)
            Entidad.maxPolizasAseguradoSinDNI = CType(dr("maxPolizasAseguradoSinDNI"), Integer)
            Entidad.validaDireccionCer = CType(dr("validaDireccionCer"), Boolean)
            Entidad.validaDireccionAse = CType(dr("validaDireccionAse"), Boolean)
            Entidad.cuentaHabienteigualAsegurado = CType(dr("cuentaHabienteigualAsegurado"), Boolean)

        Catch ex As Exception

        End Try

        Return Entidad
    End Function

#End Region

#Region " ProductoEntidad "
    Public Shared Function ProductoEntidad_Lista(ByVal dr As IDataRecord) As ProductoEntidadBE
        Dim Entidad As New ProductoEntidadBE
        If Not dr("IdProductoEntidad") Is DBNull.Value Then
            Entidad.IdProductoEntidad = CType(dr("IdProductoEntidad"), Integer)
        End If
        If Not dr("IdProducto") Is DBNull.Value Then
            Entidad.IdProducto = CType(dr("IdProducto"), Integer)
        End If
        If Not dr("IdEntidad") Is DBNull.Value Then
            Entidad.IdEntidad = CType(dr("IdEntidad"), Integer)
        End If
        If Not dr("Estado") Is DBNull.Value Then
            Entidad.Estado = CType(dr("Estado"), String)
        End If
        If Not dr("UsuarioReg") Is DBNull.Value Then
            Entidad.UsuarioReg = CType(dr("UsuarioReg"), String)
        End If
        If Not dr("FechaReg") Is DBNull.Value Then
            Entidad.FechaReg = CType(dr("FechaReg"), DateTime)
        End If
        If Not dr("UsuarioMod") Is DBNull.Value Then
            Entidad.UsuarioMod = CType(dr("UsuarioMod"), String)
        End If
        If Not dr("FechaMod") Is DBNull.Value Then
            Entidad.FechaMod = CType(dr("FechaMod"), DateTime)
        End If
        Return Entidad
    End Function

    Public Shared Function ProductoEntidad_Check_Lista(ByVal dr As IDataRecord) As EntidadBE
        Dim Entidad As New EntidadBE

        If Not dr("IdEntidad") Is DBNull.Value Then
            Entidad.IdEntidad = CType(dr("IdEntidad"), Integer)
        End If
        If Not dr("Descripcion") Is DBNull.Value Then
            Entidad.Descripcion = CType(dr("Descripcion"), String)
        End If

        If Not dr("Chek") Is DBNull.Value Then
            If CType(dr("Chek"), Integer) = 0 Then
                Entidad.Check = False
            Else
                Entidad.Check = True
            End If
        End If
        Return Entidad
    End Function

#End Region

#Region " ProductoEntidadValidacionBE "
    Public Shared Function ProductoEntidadValidacion_ProductoEntidad_Lista(ByVal dr As IDataRecord) As EntidadDetalleBE
        Dim Entidad As New EntidadDetalleBE

        If Not dr("IdEntidadDetalle") Is DBNull.Value Then
            Entidad.IdEntidadDetalle = CType(dr("IdEntidadDetalle"), Integer)
        End If
        If Not dr("IdEntidad") Is DBNull.Value Then
            Entidad.IdEntidad = CType(dr("IdEntidad"), Integer)
        End If
        If Not dr("IdTipoValidacion") Is DBNull.Value Then
            Entidad.IdTipoValidacion = CType(dr("IdTipoValidacion"), Integer)
        End If
        If Not dr("TipoValidacion") Is DBNull.Value Then
            Entidad.TipoValidacion = CType(dr("TipoValidacion"), String)
        End If
        If Not dr("Entidad") Is DBNull.Value Then
            Entidad.Descripcion = CType(dr("Entidad"), String)
        End If

        If Not dr("chek") Is DBNull.Value Then
            If CType(dr("chek"), Integer) = 0 Then
                Entidad.Check = False
            Else
                Entidad.Check = True
            End If
        End If

        If Not dr("ValorMinimo") Is DBNull.Value Then
            Entidad.ValorMinimo = CType(dr("ValorMinimo"), String)
        End If

        If Not dr("ValorMaximo") Is DBNull.Value Then
            Entidad.ValorMaximo = CType(dr("ValorMaximo"), String)
        End If

        If Not dr("obs") Is DBNull.Value Then
            Entidad.Obs = CType(dr("obs"), String)
        End If

        Return Entidad
    End Function


    Public Shared Function ProductoEntidadValidacion_Lista(ByVal dr As IDataRecord) As ProductoEntidadValidacionBE
        Dim Entidad As New ProductoEntidadValidacionBE

        If Not dr("IdProductoEntidadValidacion") Is DBNull.Value Then
            Entidad.IdProductoEntidadValidacion = CType(dr("IdProductoEntidadValidacion"), Integer)
        End If
        If Not dr("IdProducto") Is DBNull.Value Then
            Entidad.IdProducto = CType(dr("IdProducto"), Integer)
        End If
        If Not dr("IdEntidad") Is DBNull.Value Then
            Entidad.IdEntidad = CType(dr("IdEntidad"), Integer)
        End If
        If Not dr("IdEntidadDetalle") Is DBNull.Value Then
            Entidad.IdEntidadDetalle = CType(dr("IdEntidadDetalle"), Integer)
        End If
        If Not dr("IdTipoValidacion") Is DBNull.Value Then
            Entidad.IdTipoValidacion = CType(dr("IdTipoValidacion"), Integer)
        End If
        If Not dr("Obs") Is DBNull.Value Then
            Entidad.Obs = CType(dr("Obs"), String)
        End If
        If Not dr("Estado") Is DBNull.Value Then
            Entidad.Estado = CType(dr("Estado"), String)
        End If
        If Not dr("UsuarioReg") Is DBNull.Value Then
            Entidad.UsuarioReg = CType(dr("UsuarioReg"), String)
        End If
        If Not dr("FechaReg") Is DBNull.Value Then
            Entidad.FechaReg = CType(dr("FechaReg"), DateTime)
        End If
        If Not dr("UsuarioMod") Is DBNull.Value Then
            Entidad.UsuarioMod = CType(dr("UsuarioMod"), String)
        End If
        If Not dr("FechaMod") Is DBNull.Value Then
            Entidad.FechaMod = CType(dr("FechaMod"), DateTime)
        End If

        Return Entidad
    End Function


#End Region

#Region " ProductoInfoProductoBE "
    Public Shared Function ProductoInfoProducto_Lista(ByVal dr As IDataRecord) As ProductoInfoProductoBE
        Dim Entidad As New ProductoInfoProductoBE

        If Not dr("IdProductoInfoProducto") Is DBNull.Value Then
            Entidad.IdProductoInfoProducto = CType(dr("IdProductoInfoProducto"), Integer)
        End If
        If Not dr("IdProducto") Is DBNull.Value Then
            Entidad.IdProducto = CType(dr("IdProducto"), Integer)
        End If
        If Not dr("IdInfoProducto") Is DBNull.Value Then
            Entidad.IdInfoProducto = CType(dr("IdInfoProducto"), Integer)
        End If
        If Not dr("Estado") Is DBNull.Value Then
            Entidad.Estado = CType(dr("Estado"), String)
        End If
        If Not dr("UsuarioReg") Is DBNull.Value Then
            Entidad.UsuarioReg = CType(dr("UsuarioReg"), String)
        End If
        If Not dr("FechaReg") Is DBNull.Value Then
            Entidad.FechaReg = CType(dr("FechaReg"), DateTime)
        End If
        If Not dr("UsuarioMod") Is DBNull.Value Then
            Entidad.UsuarioMod = CType(dr("UsuarioMod"), String)
        End If
        If Not dr("FechaMod") Is DBNull.Value Then
            Entidad.FechaMod = CType(dr("FechaMod"), DateTime)
        End If

        Return Entidad
    End Function


    Public Shared Function ProductoInfoProducto_Check_Lista(ByVal dr As IDataRecord) As InfoProductoBE
        Dim Entidad As New InfoProductoBE

        If Not dr("IdInfoProducto") Is DBNull.Value Then
            Entidad.IdInfoProducto = CType(dr("IdInfoProducto"), Integer)
        End If
        If Not dr("Descripcion") Is DBNull.Value Then
            Entidad.Descripcion = CType(dr("Descripcion"), String)
        End If

        If Not dr("Chek") Is DBNull.Value Then
            If CType(dr("Chek"), Integer) = 0 Then
                Entidad.Check = False
            Else
                Entidad.Check = True
            End If
        End If

        Try
            If Not dr("Panel") Is DBNull.Value Then
                Entidad.Panel = CType(dr("Panel"), Boolean)
            End If
        Catch ex As Exception

        End Try

        Return Entidad
    End Function
#End Region

#Region " RelacionBE "
    Public Shared Function Relacion_Lista(ByVal dr As IDataRecord) As RelacionBE
        Dim Entidad As New RelacionBE

        If Not dr("IdRelacion") Is DBNull.Value Then
            Entidad.UsuarioReg = CType(dr("IdRelacion"), Integer)
        End If
        If Not dr("IdCertificado") Is DBNull.Value Then
            Entidad.IdCertificado = CType(dr("IdCertificado"), Integer)
        End If
        If Not dr("IdPersona") Is DBNull.Value Then
            Entidad.IdPersona = CType(dr("IdPersona"), Integer)
        End If
        If Not dr("IdTipoRelacion") Is DBNull.Value Then
            Entidad.IdTipoRelacion = CType(dr("IdTipoRelacion"), Integer)
        End If
        If Not dr("Estado") Is DBNull.Value Then
            Entidad.Estado = CType(dr("Estado"), String)
        End If
        If Not dr("UsuarioReg") Is DBNull.Value Then
            Entidad.UsuarioReg = CType(dr("UsuarioReg"), String)
        End If
        If Not dr("FechaReg") Is DBNull.Value Then
            Entidad.FechaReg = CType(dr("FechaReg"), DateTime)
        End If
        If Not dr("UsuarioMod") Is DBNull.Value Then
            Entidad.UsuarioMod = CType(dr("UsuarioMod"), String)
        End If
        If Not dr("FechaMod") Is DBNull.Value Then
            Entidad.FechaMod = CType(dr("FechaMod"), DateTime)
        End If

        Return Entidad
    End Function
#End Region

#Region " TipoInfoProductoBE "
    Public Shared Function TipoInfoProducto_Lista(ByVal dr As IDataRecord) As TipoInfoProductoBE
        Dim Entidad As New TipoInfoProductoBE

        If Not dr("IdTipoInfoProducto") Is DBNull.Value Then
            Entidad.IdTipoInfoProducto = CType(dr("IdTipoInfoProducto"), Integer)
        End If
        If Not dr("Descripcion") Is DBNull.Value Then
            Entidad.Descripcion = CType(dr("Descripcion"), String)
        End If
        If Not dr("Estado") Is DBNull.Value Then
            Entidad.Estado = CType(dr("Estado"), String)
        End If
        If Not dr("UsuarioReg") Is DBNull.Value Then
            Entidad.UsuarioReg = CType(dr("UsuarioReg"), String)
        End If
        If Not dr("FechaReg") Is DBNull.Value Then
            Entidad.FechaReg = CType(dr("FechaReg"), DateTime)
        End If
        If Not dr("UsuarioMod") Is DBNull.Value Then
            Entidad.UsuarioMod = CType(dr("UsuarioMod"), String)
        End If
        If Not dr("FechaMod") Is DBNull.Value Then
            Entidad.FechaMod = CType(dr("FechaMod"), DateTime)
        End If

        Return Entidad
    End Function
#End Region

#Region " TipoRelacionBE "
    Public Shared Function TipoRelacion_Lista(ByVal dr As IDataRecord) As TipoRelacionBE
        Dim Entidad As New TipoRelacionBE

        If Not dr("IdTipoRelacion") Is DBNull.Value Then
            Entidad.IdTipoRelacion = CType(dr("IdTipoRelacion"), Integer)
        End If
        If Not dr("Descripcion") Is DBNull.Value Then
            Entidad.Descripcion = CType(dr("Descripcion"), String)
        End If

        If Not dr("Estado") Is DBNull.Value Then
            Entidad.Estado = CType(dr("Estado"), String)
        End If
        If Not dr("UsuarioReg") Is DBNull.Value Then
            Entidad.UsuarioReg = CType(dr("UsuarioReg"), String)
        End If
        If Not dr("FechaReg") Is DBNull.Value Then
            Entidad.FechaReg = CType(dr("FechaReg"), DateTime)
        End If
        If Not dr("UsuarioMod") Is DBNull.Value Then
            Entidad.UsuarioMod = CType(dr("UsuarioMod"), String)
        End If
        If Not dr("FechaMod") Is DBNull.Value Then
            Entidad.FechaMod = CType(dr("FechaMod"), DateTime)
        End If

        Return Entidad
    End Function
#End Region

#Region " TipoValidacionBE "
    Public Shared Function TipoValidacion_Lista(ByVal dr As IDataRecord) As TipoValidacionBE
        Dim Entidad As New TipoValidacionBE

        If Not dr("IdTipoValidacion") Is DBNull.Value Then
            Entidad.IdTipoValidacion = CType(dr("IdTipoValidacion"), Integer)
        End If
        If Not dr("Descripcion") Is DBNull.Value Then
            Entidad.Descripcion = CType(dr("Descripcion"), String)
        End If

        If Not dr("Estado") Is DBNull.Value Then
            Entidad.Estado = CType(dr("Estado"), String)
        End If
        If Not dr("UsuarioReg") Is DBNull.Value Then
            Entidad.UsuarioReg = CType(dr("UsuarioReg"), String)
        End If
        If Not dr("FechaReg") Is DBNull.Value Then
            Entidad.FechaReg = CType(dr("FechaReg"), DateTime)
        End If
        If Not dr("UsuarioMod") Is DBNull.Value Then
            Entidad.UsuarioMod = CType(dr("UsuarioMod"), String)
        End If
        If Not dr("FechaMod") Is DBNull.Value Then
            Entidad.FechaMod = CType(dr("FechaMod"), DateTime)
        End If


        Return Entidad
    End Function
#End Region

#Region " TipoValorBE "
    Public Shared Function TipoValor_Lista(ByVal dr As IDataRecord) As TipoValorBE
        Dim Entidad As New TipoValorBE

        If Not dr("IdTipoValor") Is DBNull.Value Then
            Entidad.IdTipoValor = CType(dr("IdTipoValor"), Integer)
        End If
        If Not dr("Descripcion") Is DBNull.Value Then
            Entidad.Descripcion = CType(dr("Descripcion"), String)
        End If

        If Not dr("Estado") Is DBNull.Value Then
            Entidad.Estado = CType(dr("Estado"), String)
        End If
        If Not dr("UsuarioReg") Is DBNull.Value Then
            Entidad.UsuarioReg = CType(dr("UsuarioReg"), String)
        End If
        If Not dr("FechaReg") Is DBNull.Value Then
            Entidad.FechaReg = CType(dr("FechaReg"), DateTime)
        End If
        If Not dr("UsuarioMod") Is DBNull.Value Then
            Entidad.UsuarioMod = CType(dr("UsuarioMod"), String)
        End If
        If Not dr("FechaMod") Is DBNull.Value Then
            Entidad.FechaMod = CType(dr("FechaMod"), DateTime)
        End If

        Return Entidad
    End Function
#End Region

#Region " UsuarioConfig "
    Public Shared Function UsuarioConfig_Lista(ByVal dr As IDataRecord) As UsuarioConfigBE
        Dim Entidad As New UsuarioConfigBE

        If Not dr("IdUsuario") Is DBNull.Value Then
            Entidad.IdUsuario = CType(dr("IdUsuario"), String)
        End If
        If Not dr("Usuario") Is DBNull.Value Then
            Entidad.Usuario = CType(dr("Usuario"), String)
        End If
        If Not dr("IdProducto") Is DBNull.Value Then
            Entidad.IdProducto = CType(dr("IdProducto"), String)
        End If
        If Not dr("Producto") Is DBNull.Value Then
            Entidad.Producto = CType(dr("Producto"), String)
        End If
        If Not dr("IdAseguradora") Is DBNull.Value Then
            Entidad.IdAseguradora = CType(dr("IdAseguradora"), String)
        End If
        If Not dr("Aseguradora") Is DBNull.Value Then
            Entidad.Aseguradora = CType(dr("Aseguradora"), String)
        End If
        If Not dr("Abrev") Is DBNull.Value Then
            Entidad.CodBais = CType(dr("Abrev"), String)
        End If
     
        Return Entidad
    End Function
#End Region

End Class
