﻿Imports VAN.InsuranceWeb.BE
Imports System
Imports System.Collections

Public Interface IDLNTDataEntry
    Function Seleccionar() As IList
    Function Seleccionar(ByVal pEntidad As BEBase) As IList
    Function Seleccionar(ByVal pCodigo As Integer) As IList
    Function SeleccionarBE(ByVal pCodigo As Integer) As BEBase
End Interface


