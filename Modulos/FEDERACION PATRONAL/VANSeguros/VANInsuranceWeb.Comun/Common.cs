﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace VAN.InsuranceWeb.Comun
{
    public static class Common
    {
        public static DataTable MedioPago_Cliente()
        {
            DataTable tabla = new DataTable();
            tabla.TableName = "MedioPago_Cliente";
            tabla.Columns.Add("idMedioPago", typeof(string));
            tabla.Columns.Add("idMoneda", typeof(string));
            return tabla;
        }

        public static DataTable Lista_KeyValue()
        {
            DataTable tabla = new DataTable();
            tabla.TableName = "Lista_KeyValue";
            tabla.Columns.Add("NameKey", typeof(string));
            tabla.Columns.Add("Value", typeof(string));
            tabla.Columns.Add("DataType", typeof(char));
            tabla.Columns.Add("Source", typeof(string));
            return tabla;
        }
    }
}
