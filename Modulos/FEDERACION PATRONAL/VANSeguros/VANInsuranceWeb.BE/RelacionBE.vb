﻿Public Class RelacionBE
    Inherits GenericEntity

    Sub New()

    End Sub

    Private _IdRelacion As Integer
    Private _IdCertificado As Integer
    Private _IdPersona As Integer
    Private _IdTipoRelacion As Integer


    Property IdRelacion() As Integer
        Get
            Return _IdRelacion
        End Get
        Set(ByVal value As Integer)
            _IdRelacion = value
        End Set
    End Property
    Property IdCertificado() As Integer
        Get
            Return _IdCertificado
        End Get
        Set(ByVal value As Integer)
            _IdCertificado = value
        End Set
    End Property
    Property IdPersona() As Integer
        Get
            Return _IdPersona
        End Get
        Set(ByVal value As Integer)
            _IdPersona = value
        End Set
    End Property
    Property IdTipoRelacion() As Integer
        Get
            Return _IdTipoRelacion
        End Get
        Set(ByVal value As Integer)
            _IdTipoRelacion = value
        End Set
    End Property
    

End Class
