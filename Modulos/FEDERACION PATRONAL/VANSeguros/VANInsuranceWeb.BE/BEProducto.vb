Public Class BEProducto
    Inherits BEBase
    Private _nombre As String = String.Empty
    Private _idProducto As Int32 = 0
    Private _idAsegurador As Int32 = 0
    Private _buscar As String = String.Empty
    Private _entidad As String = String.Empty
    Private _tipoProducto As String = String.Empty
    Private _asegurador As String = String.Empty
    Private _idTipoProducto As String = String.Empty
    Private _activo As Boolean = True
    Private _maxAsegurados As Byte = 0
    Private _nacimientoRequerido As Boolean = True
    Private _validarParentesco As Boolean = True
    Private _obligaCuentaHabiente As Boolean = True
    Private _cumuloMaximo As Decimal = 0
    Private _edadMaxPermanencia As Byte = 0
    Private _diasAnulacion As Byte = 0
    Private _generaCobro As Boolean = True
    Private _idEntidad As Int32 = 0
    Private _vigenciaCobro As Boolean = True
    Private _maxPolizasAsegurado As Byte = 0
    Private _maxPolizasCuentahabiente As Byte = 0
    Private _maxPolizasRegalo As Byte = 0
    Private _idReglaCobro As Byte = 0
    Private _maxCuentasTitular As Byte = 0
    Private _maxPolizasAseguradoSinDNI As Byte = 0
    Private _validaDireccionCer As Boolean = True
    Private _validaDireccionAse As Boolean = True
    Private _cuentaHabienteigualAsegurado As Boolean = True

    Private _FrmCertificado As String = String.Empty
    Private _FrmAsegurado As String = String.Empty
    Private _NroAsegurado As Int32 = 0
    Private _NroBeneficiario As Int32 = 0

    Private _ReporteCertificado As String = String.Empty
    Private _ReporteSolicitud As String = String.Empty
    Private _ReporteCondicion As String = String.Empty
    Private _ReporteDeclaracionJurada As String = String.Empty

    Public Property NroAsegurado() As Int32
        Get
            Return _NroAsegurado
        End Get
        Set(ByVal value As Int32)
            _NroAsegurado = value
        End Set
    End Property

    Public Property NroBeneficiario() As Int32
        Get
            Return _NroBeneficiario
        End Get
        Set(ByVal value As Int32)
            _NroBeneficiario = value
        End Set
    End Property

    Public Property IdProducto() As Int32
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Int32)
            _idProducto = value
        End Set
    End Property

    Public Property IdAsegurador() As Int32
        Get
            Return _idAsegurador
        End Get
        Set(ByVal value As Int32)
            _idAsegurador = value
        End Set
    End Property

    Public Property Entidad() As String
        Get
            Return _entidad
        End Get
        Set(ByVal value As String)
            _entidad = value
        End Set
    End Property

    Public Property TipoProducto() As String
        Get
            Return _tipoProducto
        End Get
        Set(ByVal value As String)
            _tipoProducto = value
        End Set
    End Property

    Public Property Asegurador() As String
        Get
            Return _asegurador
        End Get
        Set(ByVal value As String)
            _asegurador = value
        End Set
    End Property

    Public Property Buscar() As String
        Get
            Return _buscar
        End Get
        Set(ByVal value As String)
            _buscar = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Public Property IdTipoProducto() As String
        Get
            Return _idTipoProducto
        End Get
        Set(ByVal value As String)
            _idTipoProducto = value
        End Set
    End Property

    Public Property Activo() As Boolean
        Get
            Return _activo
        End Get
        Set(ByVal value As Boolean)
            _activo = value
        End Set
    End Property

    Public Property MaxAsegurados() As Byte
        Get
            Return _maxAsegurados
        End Get
        Set(ByVal value As Byte)
            _maxAsegurados = value
        End Set
    End Property

    Public Property NacimientoRequerido() As Boolean
        Get
            Return _nacimientoRequerido
        End Get
        Set(ByVal value As Boolean)
            _nacimientoRequerido = value
        End Set
    End Property

    Public Property ValidarParentesco() As Boolean
        Get
            Return _validarParentesco
        End Get
        Set(ByVal value As Boolean)
            _validarParentesco = value
        End Set
    End Property

    Public Property ObligaCuentaHabiente() As Boolean
        Get
            Return _obligaCuentaHabiente
        End Get
        Set(ByVal value As Boolean)
            _obligaCuentaHabiente = value
        End Set
    End Property

    Public Property CumuloMaximo() As Decimal
        Get
            Return _cumuloMaximo
        End Get
        Set(ByVal value As Decimal)
            _cumuloMaximo = value
        End Set
    End Property

    Public Property EdadMaxPermanencia() As Byte
        Get
            Return _edadMaxPermanencia
        End Get
        Set(ByVal value As Byte)
            _edadMaxPermanencia = value
        End Set
    End Property

    Public Property DiasAnulacion() As Byte
        Get
            Return _diasAnulacion
        End Get
        Set(ByVal value As Byte)
            _diasAnulacion = value
        End Set
    End Property

    Public Property GeneraCobro() As Boolean
        Get
            Return _generaCobro
        End Get
        Set(ByVal value As Boolean)
            _generaCobro = value
        End Set
    End Property

    Public Property IdEntidad() As Int32
        Get
            Return _idEntidad
        End Get
        Set(ByVal value As Int32)
            _idEntidad = value
        End Set
    End Property

    Public Property VigenciaCobro() As Boolean
        Get
            Return _vigenciaCobro
        End Get
        Set(ByVal value As Boolean)
            _vigenciaCobro = value
        End Set
    End Property

    Public Property MaxPolizasAsegurado() As Byte
        Get
            Return _maxPolizasAsegurado
        End Get
        Set(ByVal value As Byte)
            _maxPolizasAsegurado = value
        End Set
    End Property

    Public Property MaxPolizasCuentahabiente() As Byte
        Get
            Return _maxPolizasCuentahabiente
        End Get
        Set(ByVal value As Byte)
            _maxPolizasCuentahabiente = value
        End Set
    End Property

    Public Property MaxPolizasRegalo() As Byte
        Get
            Return _maxPolizasRegalo
        End Get
        Set(ByVal value As Byte)
            _maxPolizasRegalo = value
        End Set
    End Property

    Public Property IdReglaCobro() As Byte
        Get
            Return _idReglaCobro
        End Get
        Set(ByVal value As Byte)
            _idReglaCobro = value
        End Set
    End Property

    Public Property MaxCuentasTitular() As Byte
        Get
            Return _maxCuentasTitular
        End Get
        Set(ByVal value As Byte)
            _maxCuentasTitular = value
        End Set
    End Property

    Public Property MaxPolizasAseguradoSinDNI() As Byte
        Get
            Return _maxPolizasAseguradoSinDNI
        End Get
        Set(ByVal value As Byte)
            _maxPolizasAseguradoSinDNI = value
        End Set
    End Property

    Public Property ValidaDireccionCer() As Boolean
        Get
            Return _validaDireccionCer
        End Get
        Set(ByVal value As Boolean)
            _validaDireccionCer = value
        End Set
    End Property

    Public Property ValidaDireccionAse() As Boolean
        Get
            Return _validaDireccionAse
        End Get
        Set(ByVal value As Boolean)
            _validaDireccionAse = value
        End Set
    End Property

    Public Property CuentaHabienteigualAsegurado() As Boolean
        Get
            Return _cuentaHabienteigualAsegurado
        End Get
        Set(ByVal value As Boolean)
            _cuentaHabienteigualAsegurado = value
        End Set
    End Property

    Public Property FrmCertificado() As String
        Get
            Return _FrmCertificado
        End Get
        Set(ByVal value As String)
            _FrmCertificado = value
        End Set
    End Property

    Public Property FrmAsegurado() As String
        Get
            Return _FrmAsegurado
        End Get
        Set(ByVal value As String)
            _FrmAsegurado = value
        End Set
    End Property


    Public Property ReporteCertificado() As String
        Get
            Return _ReporteCertificado
        End Get
        Set(ByVal value As String)
            _ReporteCertificado = value
        End Set
    End Property

    Public Property ReporteSolicitud() As String
        Get
            Return _ReporteSolicitud
        End Get
        Set(ByVal value As String)
            _ReporteSolicitud = value
        End Set
    End Property

    Public Property ReporteCondicion() As String
        Get
            Return _ReporteCondicion
        End Get
        Set(ByVal value As String)
            _ReporteCondicion = value
        End Set
    End Property

    Public Property ReporteDeclaracionJurada() As String
        Get
            Return _ReporteDeclaracionJurada
        End Get
        Set(ByVal value As String)
            _ReporteDeclaracionJurada = value
        End Set
    End Property

End Class
