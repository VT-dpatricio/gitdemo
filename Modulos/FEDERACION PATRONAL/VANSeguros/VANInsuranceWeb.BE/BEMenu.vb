﻿Imports System


Public Class BEMenu
    Inherits BEBase

    ' Properties
    Public Property IDUsuario() As String
        Get
            Return Me._iDUsuario
        End Get
        Set(ByVal value As String)
            Me._iDUsuario = value
        End Set
    End Property
    Public Property IDRol() As Integer
        Get
            Return Me._iDRol
        End Get
        Set(ByVal value As Integer)
            Me._iDRol = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._descripcion
        End Get
        Set(ByVal value As String)
            Me._descripcion = value
        End Set
    End Property

    Public Property Estado() As Boolean
        Get
            Return Me._estado
        End Get
        Set(ByVal value As Boolean)
            Me._estado = value
        End Set
    End Property

    Public Property IDMenu() As Integer
        Get
            Return Me._iDMenu
        End Get
        Set(ByVal value As Integer)
            Me._iDMenu = value
        End Set
    End Property

    Public Property IDMenuPadre() As Integer
        Get
            Return Me._iDMenuPadre
        End Get
        Set(ByVal value As Integer)
            Me._iDMenuPadre = value
        End Set
    End Property

    Public Property Orden() As Integer
        Get
            Return Me._orden
        End Get
        Set(ByVal value As Integer)
            Me._orden = value
        End Set
    End Property

    Public Property Url() As String
        Get
            Return Me._url
        End Get
        Set(ByVal value As String)
            Me._url = value
        End Set
    End Property


    ' Fields
    Private _descripcion As String = String.Empty
    Private _estado As Boolean = True
    Private _iDMenu As Integer = 0
    Private _iDMenuPadre As Integer = 0
    Private _orden As Integer = 0
    Private _url As String = String.Empty
    Private _iDRol As Integer = 0
    Private _iDUsuario As String = String.Empty
End Class


