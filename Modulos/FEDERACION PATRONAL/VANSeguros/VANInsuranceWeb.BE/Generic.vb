﻿Imports System
<Serializable()> _
Public MustInherit Class GenericEntity

    Private _Estado As String
    Private _UsuarioReg As String
    Private _FechaReg As DateTime
    Private _UsuarioMod As String
    Private _FechaMod As DateTime


    Private _Filler As String
    Public Property Filler() As String
        Get
            Return _Filler
        End Get
        Set(ByVal value As String)
            _Filler = value
        End Set
    End Property


    Property Estado() As String
        Get
            Return _Estado
        End Get
        Set(ByVal value As String)
            _Estado = value
        End Set
    End Property
    Property UsuarioReg() As String
        Get
            Return _UsuarioReg
        End Get
        Set(ByVal value As String)
            _UsuarioReg = value
        End Set
    End Property
    Property FechaReg() As DateTime
        Get
            Return _FechaReg
        End Get
        Set(ByVal value As DateTime)
            _FechaReg = value
        End Set
    End Property
    Property UsuarioMod() As String
        Get
            Return _UsuarioMod
        End Get
        Set(ByVal value As String)
            _UsuarioMod = value
        End Set
    End Property
    Property FechaMod() As DateTime
        Get
            Return _FechaMod
        End Get
        Set(ByVal value As DateTime)
            _FechaMod = value
        End Set
    End Property

End Class