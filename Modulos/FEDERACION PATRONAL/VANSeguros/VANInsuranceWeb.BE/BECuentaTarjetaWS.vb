﻿Public Class BECuentaTarjetaWS
    Private _id As Int32 = 0
    Private _raiz As String = String.Empty
    Private _idmediopago As String = String.Empty
    Private _tipo As String = String.Empty
    Private _numero As String = String.Empty
    Private _idmoneda As String = String.Empty
    Private _CBU As String = String.Empty

    Public Property CBU() As String
        Get
            Return _CBU
        End Get
        Set(ByVal value As String)
            _CBU = value
        End Set
    End Property


    Public Property ID() As String
        Get
            Return _id
        End Get
        Set(ByVal value As String)
            _id = value
        End Set
    End Property

    Public Property Raiz() As String
        Get
            Return _raiz
        End Get
        Set(ByVal value As String)
            _raiz = value
        End Set
    End Property

    Public Property IDMedioPago() As String
        Get
            Return _idmediopago
        End Get
        Set(ByVal value As String)
            _idmediopago = value
        End Set
    End Property

    Public Property Tipo() As String
        Get
            Return _tipo
        End Get
        Set(ByVal value As String)
            _tipo = value
        End Set
    End Property

    Public Property Numero() As String
        Get
            Return _numero
        End Get
        Set(ByVal value As String)
            _numero = value
        End Set
    End Property

    Public Property IDmoneda() As String
        Get
            Return _idmoneda
        End Get
        Set(ByVal value As String)
            _idmoneda = value
        End Set
    End Property

End Class
