﻿Public Class ProductoInfoProductoBE
    Inherits GenericEntity

    Sub New()

    End Sub

    Private _IdProductoInfoProducto As Integer
    Private _IdProducto As Integer
    Private _IdInfoProducto As Integer
  

    Property IdProductoInfoProducto() As Integer
        Get
            Return _IdProductoInfoProducto
        End Get
        Set(ByVal value As Integer)
            _IdProductoInfoProducto = value
        End Set
    End Property
    Property IdProducto() As Integer
        Get
            Return _IdProducto
        End Get
        Set(ByVal value As Integer)
            _IdProducto = value
        End Set
    End Property
    Property IdInfoProducto() As Integer
        Get
            Return _IdInfoProducto
        End Get
        Set(ByVal value As Integer)
            _IdInfoProducto = value
        End Set
    End Property
    

End Class
