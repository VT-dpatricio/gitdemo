﻿Public Class TipoInfoProductoBE
    Inherits GenericEntity

    Sub New()

    End Sub

    Private _IdTipoInfoProducto As Integer
    Private _Descripcion As String
    

    Property IdTipoInfoProducto() As Integer
        Get
            Return _IdTipoInfoProducto
        End Get
        Set(ByVal value As Integer)
            _IdTipoInfoProducto = value
        End Set
    End Property
    Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property
   


End Class
