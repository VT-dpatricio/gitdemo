﻿Public Class ProductoConfiguracionBE
    Inherits GenericEntity

    Sub New()

    End Sub

    Private _IdProducto As Integer
    Private _IdFrecuencias As String
    Private _Puntos As Integer
    Private _TieneAsegurados As Boolean
    Private _VigenciaCertificado As Integer
    Private _Tasa As Decimal


    Property IdProducto() As Integer
        Get
            Return _IdProducto
        End Get
        Set(ByVal value As Integer)
            _IdProducto = value
        End Set
    End Property
    Property IdFrecuencias() As String
        Get
            Return _IdFrecuencias
        End Get
        Set(ByVal value As String)
            _IdFrecuencias = value
        End Set
    End Property
    Property Puntos() As Integer
        Get
            Return _Puntos
        End Get
        Set(ByVal value As Integer)
            _Puntos = value
        End Set
    End Property
    Property TieneAsegurados() As Boolean
        Get
            Return _TieneAsegurados
        End Get
        Set(ByVal value As Boolean)
            _TieneAsegurados = value
        End Set
    End Property
    Property VigenciaCertificado() As Integer
        Get
            Return _VigenciaCertificado
        End Get
        Set(ByVal value As Integer)
            _VigenciaCertificado = value
        End Set
    End Property
    Property Tasa() As Decimal
        Get
            Return _Tasa
        End Get
        Set(ByVal value As Decimal)
            _Tasa = value
        End Set
    End Property



End Class
