﻿Public Class BESolicitudContactoCliente
    Inherits BE.BEBase

    Public Property IdSolicitudContacto As Integer
    Public Property IdTipoDocumento As String
    Public Property ccCliente As String
    Public Property Nombre1 As String
    Public Property Nombre2 As String
    Public Property Apellido1 As String
    Public Property Apellido2 As String
    Public Property Email As String
    Public Property Telefono As String
    Public Property Celular As String
    Public Property IDInformador As String
    Public Property Observaciones As String
    Public Property IDUsuarioCreacion As String
    Public Property IDCertificadoOrigen As String
    Public Property IdEstadoSolicitudContacto As Integer
    Public Property FechaCreacion As DateTime
    Public Property FechaModificacion As DateTime
    Public Property Productos As New List(Of BESolicitudContactoProducto)
    Public Property NombreCompleto As String
    Public Property CantidadDiasContacto As Integer
    Public Property IdTipoFrecuenciaContacto As Integer
    Public Property ValorFrecuenciaContacto As String
    Public Property CodigoTipoFrecuencia As String
End Class
