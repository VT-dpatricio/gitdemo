﻿Public Class InfoAseguradoBE
    Inherits GenericEntity

    Sub New()

    End Sub

    Private _IdInfoAsegurado As Integer
    Private _IdTipoInfoAsegurado As Integer
    Private _Descripcion As String
    Private _Abrev As String
    Private _Panel As Boolean
    Private _idProducto As Integer
    Private _nombre As String
    Private _idTipoDato As String
    Private _TipoDato As String
    Private _obligatorio As Boolean
    Private _minimo As Integer
    Private _maximo As Integer
    Private _numCaracteres As Integer
    Private _regex As String
    Private _formato As String



    Private _Id As Integer
    Public Property Id() As Integer
        Get
            Return _Id
        End Get
        Set(ByVal value As Integer)
            _Id = value
        End Set
    End Property

    Public Property Panel() As Boolean
        Get
            Return _Panel
        End Get
        Set(ByVal value As Boolean)
            _Panel = value
        End Set
    End Property


    Private _Check As Boolean
    Public Property Check() As Boolean
        Get
            Return _Check
        End Get
        Set(ByVal value As Boolean)
            _Check = value
        End Set
    End Property

    Property idInfoAsegurado() As Integer
        Get
            Return _idInfoAsegurado
        End Get
        Set(ByVal value As Integer)
            _idInfoAsegurado = value
        End Set
    End Property
    Property idProducto() As Integer
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Integer)
            _idProducto = value
        End Set
    End Property
    Property nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property
    Property descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property
    Property idTipoDato() As String
        Get
            Return _idTipoDato
        End Get
        Set(ByVal value As String)
            _idTipoDato = value
        End Set
    End Property
    Property TipoDato() As String
        Get
            Return _TipoDato
        End Get
        Set(ByVal value As String)
            _TipoDato = value
        End Set
    End Property
    Property obligatorio() As Boolean
        Get
            Return _obligatorio
        End Get
        Set(ByVal value As Boolean)
            _obligatorio = value
        End Set
    End Property
    ReadOnly Property obligatorioTxt() As String
        Get
            Return IIf(_obligatorio = True, "SI", "NO")
        End Get
    End Property
    Property minimo() As Integer
        Get
            Return _minimo
        End Get
        Set(ByVal value As Integer)
            _minimo = value
        End Set
    End Property
    Property maximo() As Integer
        Get
            Return _maximo
        End Get
        Set(ByVal value As Integer)
            _maximo = value
        End Set
    End Property
    Property numCaracteres() As Integer
        Get
            Return _numCaracteres
        End Get
        Set(ByVal value As Integer)
            _numCaracteres = value
        End Set
    End Property
    Property regex() As String
        Get
            Return _regex
        End Get
        Set(ByVal value As String)
            _regex = value
        End Set
    End Property
    Property formato() As String
        Get
            Return _formato
        End Get
        Set(ByVal value As String)
            _formato = value
        End Set
    End Property


End Class
