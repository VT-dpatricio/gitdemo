Public Class BETipoDocumento
    Inherits BEBase
    Private _idTipoDocumento As String
    Private _nombre As String

    Public Property IdTipoDocumento() As String
        Get
            Return _idTipoDocumento
        End Get
        Set(ByVal value As String)
            _idTipoDocumento = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property
End Class
