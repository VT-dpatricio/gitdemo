﻿Public Class ProductoMedioPagoBE
    Inherits GenericEntity

    Sub New()

    End Sub

    Private _idProducto As Integer
    Private _idMedioPago As String
    Private _IdMonedaCobro As String
    Private _MedioPago As String
    Private _MonedaCobro As String
    Private _codigoComercio As String
    Private _cuentaRecaudadora As String
    Private _codigoArchivo As String
    Private _codigoInterno As String
    Private _monedaCuenta As String
    Private _servicio As String
    Private _origensolicitud As String

    Private _Id As Integer
    Public Property Id() As Integer
        Get
            Return _Id
        End Get
        Set(ByVal value As Integer)
            _Id = value
        End Set
    End Property

    Property idProducto() As Integer
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Integer)
            _idProducto = value
        End Set
    End Property
    Property idMedioPago() As String
        Get
            Return _idMedioPago
        End Get
        Set(ByVal value As String)
            _idMedioPago = value
        End Set
    End Property
    Property MedioPago() As String
        Get
            Return _MedioPago
        End Get
        Set(ByVal value As String)
            _MedioPago = value
        End Set
    End Property
    Property IdMonedaCobro() As String
        Get
            Return _IdMonedaCobro
        End Get
        Set(ByVal value As String)
            _IdMonedaCobro = value
        End Set
    End Property
    Property MonedaCobro() As String
        Get
            Return _MonedaCobro
        End Get
        Set(ByVal value As String)
            _MonedaCobro = value
        End Set
    End Property
    Property codigoComercio() As String
        Get
            Return _codigoComercio
        End Get
        Set(ByVal value As String)
            _codigoComercio = value
        End Set
    End Property
    Property cuentaRecaudadora() As String
        Get
            Return _cuentaRecaudadora
        End Get
        Set(ByVal value As String)
            _cuentaRecaudadora = value
        End Set
    End Property
    Property codigoArchivo() As String
        Get
            Return _codigoArchivo
        End Get
        Set(ByVal value As String)
            _codigoArchivo = value
        End Set
    End Property
    Property codigoInterno() As String
        Get
            Return _codigoInterno
        End Get
        Set(ByVal value As String)
            _codigoInterno = value
        End Set
    End Property
    Property monedaCuenta() As String
        Get
            Return _monedaCuenta
        End Get
        Set(ByVal value As String)
            _monedaCuenta = value
        End Set
    End Property
    Property servicio() As String
        Get
            Return _servicio
        End Get
        Set(ByVal value As String)
            _servicio = value
        End Set
    End Property
    Property origensolicitud() As String
        Get
            Return _origensolicitud
        End Get
        Set(ByVal value As String)
            _origensolicitud = value
        End Set
    End Property



End Class
