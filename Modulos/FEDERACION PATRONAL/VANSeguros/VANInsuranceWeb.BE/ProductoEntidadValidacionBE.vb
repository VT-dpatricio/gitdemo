﻿Public Class ProductoEntidadValidacionBE
    Inherits GenericEntity

    Sub New()

    End Sub

    Private _IdProductoEntidadValidacion As Integer
    Private _IdProducto As Integer
    Private _IdEntidad As Integer
    Private _IdEntidadDetalle As Integer
    Private _IdTipoValidacion As Integer
    Private _Obs As String


    Private _ValorMinimo As String
    Public Property ValorMinimo() As String
        Get
            Return _ValorMinimo
        End Get
        Set(ByVal value As String)
            _ValorMinimo = value
        End Set
    End Property


    Private _ValorMaximo As String
    Public Property ValorMaximo() As String
        Get
            Return _ValorMaximo
        End Get
        Set(ByVal value As String)
            _ValorMaximo = value
        End Set
    End Property




    Property IdProductoEntidadValidacion() As Integer
        Get
            Return _IdProductoEntidadValidacion
        End Get
        Set(ByVal value As Integer)
            _IdProductoEntidadValidacion = value
        End Set
    End Property
    Property IdProducto() As Integer
        Get
            Return _IdProducto
        End Get
        Set(ByVal value As Integer)
            _IdProducto = value
        End Set
    End Property
    Property IdEntidad() As Integer
        Get
            Return _IdEntidad
        End Get
        Set(ByVal value As Integer)
            _IdEntidad = value
        End Set
    End Property
    Property IdEntidadDetalle() As Integer
        Get
            Return _IdEntidadDetalle
        End Get
        Set(ByVal value As Integer)
            _IdEntidadDetalle = value
        End Set
    End Property
    Property IdTipoValidacion() As Integer
        Get
            Return _IdTipoValidacion
        End Get
        Set(ByVal value As Integer)
            _IdTipoValidacion = value
        End Set
    End Property
    Property Obs() As String
        Get
            Return _Obs
        End Get
        Set(ByVal value As String)
            _Obs = value
        End Set
    End Property


End Class
