Public Class BECiudad
    Inherits BEBase

    Private _idCiudad As Int32
    Private _idProvincia As Int32
    Private _idDepartamento As Int32
    Private _idProducto As Int32
    Private _nombre As String
    Private _buscar As String
    Private _ciudad As String
    Private _provincia As String
    Private _departamento As String
    Private _codPostal As String

    Public Property IdCiudad() As Int32
        Get
            Return _idCiudad
        End Get
        Set(ByVal value As Int32)
            _idCiudad = value
        End Set
    End Property

    Public Property IdProvincia() As Int32
        Get
            Return _idProvincia
        End Get
        Set(ByVal value As Int32)
            _idProvincia = value
        End Set
    End Property

    Public Property IdDepartamento() As Int32
        Get
            Return _idDepartamento
        End Get
        Set(ByVal value As Int32)
            _idDepartamento = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Public Property Ciudad() As String
        Get
            Return _ciudad
        End Get
        Set(ByVal value As String)
            _ciudad = value
        End Set
    End Property

    Public Property Provincia() As String
        Get
            Return _provincia
        End Get
        Set(ByVal value As String)
            _provincia = value
        End Set
    End Property

    Public Property Departamento() As String
        Get
            Return _departamento
        End Get
        Set(ByVal value As String)
            _departamento = value
        End Set
    End Property

    Public Property CodPostal() As String
        Get
            Return _codPostal
        End Get
        Set(ByVal value As String)
            _codPostal = value
        End Set
    End Property

    Public Property IdProducto() As Int32
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Int32)
            _idProducto = value
        End Set
    End Property

    Public Property Buscar() As String
        Get
            Return _buscar
        End Get
        Set(ByVal value As String)
            _buscar = value
        End Set
    End Property


End Class
