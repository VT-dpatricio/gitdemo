Public Class BEOficina
    Inherits BEBase
    Private _idOficina As Int32
    Private _codOficina As Int32
    Private _idProducto As Int32
    Private _nombre As String
    Private _buscar As String

    Public Property IdOficina() As Int32
        Get
            Return _idOficina
        End Get
        Set(ByVal value As Int32)
            _idOficina = value
        End Set
    End Property

    Public Property CodOficina() As Int32
        Get
            Return _codOficina
        End Get
        Set(ByVal value As Int32)
            _codOficina = value
        End Set
    End Property

    Public Property IdProducto() As Int32
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Int32)
            _idProducto = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Public Property Buscar() As String
        Get
            Return _buscar
        End Get
        Set(ByVal value As String)
            _buscar = value
        End Set
    End Property
End Class
