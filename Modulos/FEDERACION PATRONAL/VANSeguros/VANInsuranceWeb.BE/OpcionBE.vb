﻿Public Class OpcionBE
    Inherits GenericEntity

    Sub New()

    End Sub

    Private _idProducto As Integer
    Private _opcion As String
    Private _activo As Boolean
    Private _puntos As Integer
    Private _puntosValor As Decimal

    Private _Id As Integer
    Public Property Id() As Integer
        Get
            Return _Id
        End Get
        Set(ByVal value As Integer)
            _Id = value
        End Set
    End Property

    Property idProducto() As Integer
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Integer)
            _idProducto = value
        End Set
    End Property
    Property opcion() As String
        Get
            Return _opcion
        End Get
        Set(ByVal value As String)
            _opcion = value
        End Set
    End Property
    ReadOnly Property activoTxt() As String
        Get
            Return IIf(_activo = True, "SI", "NO")
        End Get
    End Property
    Property activo() As Boolean
        Get
            Return _activo
        End Get
        Set(ByVal value As Boolean)
            _activo = value
        End Set
    End Property
    Property puntos() As Integer
        Get
            Return _puntos
        End Get
        Set(ByVal value As Integer)
            _puntos = value
        End Set
    End Property
    Property puntosValor() As Decimal
        Get
            Return _puntosValor
        End Get
        Set(ByVal value As Decimal)
            _puntosValor = value
        End Set
    End Property



End Class
