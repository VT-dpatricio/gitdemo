﻿Public Class BECliente
    Inherits BEBase

    Private _idTipoDocumento As String
    Private _tipoDocumento As String
    Private _nroDocumento As Int32
    Private _nombreCompleto As String
    Private _direccion As String

    Public Property IdTipoDocumento() As String
        Get
            Return _idTipoDocumento
        End Get
        Set(ByVal value As String)
            _idTipoDocumento = value
        End Set
    End Property

    Public Property NroDocumento() As Int32
        Get
            Return _nroDocumento
        End Get
        Set(ByVal value As Int32)
            _nroDocumento = value
        End Set
    End Property

    Public Property TipoDocumento() As String
        Get
            Return _tipoDocumento
        End Get
        Set(ByVal value As String)
            _tipoDocumento = value
        End Set
    End Property

    Public Property NombreCompleto() As String
        Get
            Return _nombreCompleto
        End Get
        Set(ByVal value As String)
            _nombreCompleto = value
        End Set
    End Property

    Public Property Direccion() As String
        Get
            Return _direccion
        End Get
        Set(ByVal value As String)
            _direccion = value
        End Set
    End Property

End Class
