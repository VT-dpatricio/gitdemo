﻿Public Class EntidadBE
    Inherits GenericEntity

    Sub New()

    End Sub

    Private _IdEntidad As Integer
    Private _Descripcion As String
    Private _Abrev As String
    Private _Check As Boolean
  
    Public Property Check() As Boolean
        Get
            Return _Check
        End Get
        Set(ByVal value As Boolean)
            _Check = value
        End Set
    End Property

    Property IdEntidad() As Integer
        Get
            Return _IdEntidad
        End Get
        Set(ByVal value As Integer)
            _IdEntidad = value
        End Set
    End Property
    Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property
    Property Abrev() As String
        Get
            Return _Abrev
        End Get
        Set(ByVal value As String)
            _Abrev = value
        End Set
    End Property
   
End Class
