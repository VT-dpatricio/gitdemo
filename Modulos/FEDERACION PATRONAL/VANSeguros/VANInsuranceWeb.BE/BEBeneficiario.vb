﻿Public Class BEBeneficiario
#Region "Campos"
    Private m_idbeneficiario As Int32
    Private m_idcertificado As [String]
    Private m_consecutivo As Int16
    Private m_ccbenef As [String]
    Private m_idtipodocumento As [String]
    Private m_porcentaje As [Decimal]
    Private m_nombre1 As [String]
    Private m_nombre2 As [String]
    Private m_apellido1 As [String]
    Private m_apellido2 As [String]
    Private m_idparentesco As Int16
    Private m_activo As [Boolean] = True
    Private m_domicilio As [String]
    Private m_fechanacimiento As DateTime
    Private m_ocupacion As [String]
    Private m_telefono As [String]
    Private m_sexo As [String]
    Private m_nombres As [String]
    Private m_apellidos As [String]
    Private m_nombrecompleto As [String]
    Private m_nombreestado As [String]
#End Region

#Region "CamposConsulta"
    Private m_nombredocumento As [String]
#End Region

#Region "Propiedades"
    Public Property IdBeneficiario() As Int32
        Get
            Return m_idbeneficiario
        End Get
        Set(ByVal value As Int32)
            m_idbeneficiario = value
        End Set
    End Property
    Public Property IdCertificado() As [String]
        Get
            Return m_idcertificado
        End Get
        Set(ByVal value As [String])
            m_idcertificado = value
        End Set
    End Property
    Public Property Consecutivo() As Int16
        Get
            Return m_consecutivo
        End Get
        Set(ByVal value As Int16)
            m_consecutivo = value
        End Set
    End Property
    Public Property CcBenef() As [String]
        Get
            Return m_ccbenef
        End Get
        Set(ByVal value As [String])
            m_ccbenef = value
        End Set
    End Property
    Public Property IdTipoDocumento() As [String]
        Get
            Return m_idtipodocumento
        End Get
        Set(ByVal value As [String])
            m_idtipodocumento = value
        End Set
    End Property
    Public Property Porcentaje() As [Decimal]
        Get
            Return m_porcentaje
        End Get
        Set(ByVal value As [Decimal])
            m_porcentaje = value
        End Set
    End Property
    Public Property Nombre1() As [String]
        Get
            Return m_nombre1
        End Get
        Set(ByVal value As [String])
            m_nombre1 = value
        End Set
    End Property
    Public Property Nombre2() As [String]
        Get
            Return m_nombre2
        End Get
        Set(ByVal value As [String])
            m_nombre2 = value
        End Set
    End Property
    Public Property Apellido1() As [String]
        Get
            Return m_apellido1
        End Get
        Set(ByVal value As [String])
            m_apellido1 = value
        End Set
    End Property
    Public Property Apellido2() As [String]
        Get
            Return m_apellido2
        End Get
        Set(ByVal value As [String])
            m_apellido2 = value
        End Set
    End Property
    Public Property IdParentesco() As Int16
        Get
            Return m_idparentesco
        End Get
        Set(ByVal value As Int16)
            m_idparentesco = value
        End Set
    End Property
    Public Property Activo() As [Boolean]
        Get
            Return m_activo
        End Get
        Set(ByVal value As [Boolean])
            m_activo = value
        End Set
    End Property
    Public Property Domicilio() As [String]
        Get
            Return m_domicilio
        End Get
        Set(ByVal value As [String])
            m_domicilio = value
        End Set
    End Property
    Public Property FechaNacimiento() As DateTime
        Get
            Return m_fechanacimiento
        End Get
        Set(ByVal value As DateTime)
            m_fechanacimiento = value
        End Set
    End Property
    Public Property Ocupacion() As [String]
        Get
            Return m_ocupacion
        End Get
        Set(ByVal value As [String])
            m_ocupacion = value
        End Set
    End Property
    Public Property Telefono() As [String]
        Get
            Return m_telefono
        End Get
        Set(ByVal value As [String])
            m_telefono = value
        End Set
    End Property
    Public Property Sexo() As [String]
        Get
            Return m_sexo
        End Get
        Set(ByVal value As [String])
            m_sexo = value
        End Set
    End Property
    Public Property NombreDocumento() As [String]
        Get
            Return m_nombredocumento
        End Get
        Set(ByVal value As [String])
            m_nombredocumento = value
        End Set
    End Property
    Public Property Nombres() As [String]
        Get
            Return Nombre1 + " " + Nombre2
        End Get
        Set(ByVal value As [String])
            m_nombres = value
        End Set
    End Property
    Public Property Apellidos() As [String]
        Get
            Return Apellido1 + " " + Apellido2
        End Get
        Set(ByVal value As [String])
            m_apellidos = value
        End Set
    End Property
    Public Property NombreCompleto() As [String]
        Get
            Return Apellidos + " " + Nombres
        End Get
        Set(ByVal value As [String])
            m_nombrecompleto = value
        End Set
    End Property
    Public Property NombreEstado() As [String]
        Get
            Return m_nombreestado
        End Get
        Set(ByVal value As [String])
            m_nombreestado = value
        End Set
    End Property
#End Region
End Class
