﻿Public Class CertificadoDetalleInfoProductoBE
    Inherits GenericEntity

    Sub New()

    End Sub


    Private _IdCertificadoDetalleInfoProducto As Integer
    Private _IdProducto As Integer
    Private _IdInfoProducto As Integer
    Private _IdDetalleInfoProducto As Integer
    Private _IdCertificado As Integer
    Private _Valor As String




    Property IdCertificadoDetalleInfoProducto() As Integer
        Get
            Return _IdCertificadoDetalleInfoProducto
        End Get
        Set(ByVal value As Integer)
            _IdCertificadoDetalleInfoProducto = value
        End Set
    End Property
    Property IdProducto() As Integer
        Get
            Return _IdProducto
        End Get
        Set(ByVal value As Integer)
            _IdProducto = value
        End Set
    End Property
    Property IdInfoProducto() As Integer
        Get
            Return _IdInfoProducto
        End Get
        Set(ByVal value As Integer)
            _IdInfoProducto = value
        End Set
    End Property
    Property IdDetalleInfoProducto() As Integer
        Get
            Return _IdDetalleInfoProducto
        End Get
        Set(ByVal value As Integer)
            _IdDetalleInfoProducto = value
        End Set
    End Property
    Property IdCertificado() As Integer
        Get
            Return _IdCertificado
        End Get
        Set(ByVal value As Integer)
            _IdCertificado = value
        End Set
    End Property
    Property Valor() As String
        Get
            Return _Valor
        End Get
        Set(ByVal value As String)
            _Valor = value
        End Set
    End Property

End Class
