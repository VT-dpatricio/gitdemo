﻿Public Class FormaPagoBE
    Inherits GenericEntity

    Sub New()

    End Sub

    Private _IdFormaPago As Integer
    Private _IdCertificado As Integer
    Private _TipoFormaPago As String
    Private _Banco As String
    Private _Documento As String


    Property IdFormaPago() As Integer
        Get
            Return _IdFormaPago
        End Get
        Set(ByVal value As Integer)
            _IdFormaPago = value
        End Set
    End Property
    Property IdCertificado() As Integer
        Get
            Return _IdCertificado
        End Get
        Set(ByVal value As Integer)
            _IdCertificado = value
        End Set
    End Property
    Property TipoFormaPago() As String
        Get
            Return _TipoFormaPago
        End Get
        Set(ByVal value As String)
            _TipoFormaPago = value
        End Set
    End Property
    Property Banco() As String
        Get
            Return _Banco
        End Get
        Set(ByVal value As String)
            _Banco = value
        End Set
    End Property
    Property Documento() As String
        Get
            Return _Documento
        End Get
        Set(ByVal value As String)
            _Documento = value
        End Set
    End Property


    Private _Comentario As String
    Public Property Comentario() As String
        Get
            Return _Comentario
        End Get
        Set(ByVal value As String)
            _Comentario = value
        End Set
    End Property



End Class

