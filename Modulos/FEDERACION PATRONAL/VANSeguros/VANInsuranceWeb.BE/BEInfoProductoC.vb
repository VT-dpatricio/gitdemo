﻿<Serializable()> _
Public Class BEInfoProductoC
#Region "Campos"
    Private m_idcertificado As String
    Private m_idinfoproducto As Int32
    Private m_valornum As Decimal
    Private m_valordate As DateTime
    Private m_valorstring As String
    Private m_nombre As String
    Private m_idtipodato As String
    Private m_descripcion As String
    Private m_campo As String


#End Region

#Region "Propiedades"
    Public Property IdCertificado() As String
        Get
            Return m_idcertificado
        End Get
        Set(ByVal value As String)
            m_idcertificado = value
        End Set
    End Property
    Public Property IdInfoProducto() As Int32
        Get
            Return m_idinfoproducto
        End Get
        Set(ByVal value As Int32)
            m_idinfoproducto = value
        End Set
    End Property
    Public Property ValorNum() As Decimal
        Get
            Return m_valornum
        End Get
        Set(ByVal value As Decimal)
            m_valornum = value
        End Set
    End Property
    Public Property ValorDate() As DateTime
        Get
            Return m_valordate
        End Get
        Set(ByVal value As DateTime)
            m_valordate = value
        End Set
    End Property
    Public Property ValorString() As String
        Get
            Return m_valorstring
        End Get
        Set(ByVal value As String)
            m_valorstring = value
        End Set
    End Property

    ''' <summary>
    ''' Nombre del infoproducto
    ''' </summary>
    Public Property Nombre() As String
        Get
            Return m_nombre
        End Get
        Set(ByVal value As String)
            m_nombre = value
        End Set
    End Property

    ''' <summary>
    ''' Descripcion del infoproducto
    ''' </summary>
    Public Property Descripcion() As String
        Get
            Return m_descripcion
        End Get
        Set(ByVal value As String)
            m_descripcion = value
        End Set
    End Property

    ''' <summary>
    ''' Tipo de Dato del InfoProducto
    ''' </summary>
    Public Property IdTipoDato() As String
        Get
            Return m_idtipodato
        End Get
        Set(ByVal value As String)
            m_idtipodato = value
        End Set
    End Property


    ''' <summary>
    ''' Campo en el que se guardará el InfoProductoC (ValorString,ValorDate,ValorDecimal)
    ''' </summary>
    Public Property Campo() As String
        Get
            Return m_campo
        End Get
        Set(ByVal value As String)
            m_campo = value
        End Set
    End Property

#End Region
End Class