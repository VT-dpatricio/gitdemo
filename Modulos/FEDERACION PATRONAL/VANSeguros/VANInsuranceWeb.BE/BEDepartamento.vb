Public Class BEDepartamento
    Inherits BEBase

    Private _idDepartamento As Int32
    Private _idPais As Int32
    Private _nombre As String

    Public Property IdDepartamento() As Int32
        Get
            Return _idDepartamento
        End Get
        Set(ByVal value As Int32)
            _idDepartamento = value
        End Set
    End Property

    Public Property IdPais() As Int32
        Get
            Return _idPais
        End Get
        Set(ByVal value As Int32)
            _idPais = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property
End Class
