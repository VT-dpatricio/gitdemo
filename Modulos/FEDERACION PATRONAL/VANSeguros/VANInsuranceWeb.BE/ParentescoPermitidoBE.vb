﻿Public Class ParentescoPermitidoBE
    Inherits GenericEntity

    Sub New()

    End Sub


    Private _idParentesco As Integer
    Private _Parentesco As String
    Private _idProducto As Integer
    Private _maxMontoAsegurado As Integer
    Private _edadMin As Integer
    Private _edadMax As Integer


    Private _Id As Integer
    Public Property Id() As Integer
        Get
            Return _Id
        End Get
        Set(ByVal value As Integer)
            _Id = value
        End Set
    End Property

    Private _IdParentescoPermitidoBE As Integer
    Public Property IdParentescoPermitidoBE() As Integer
        Get
            Return _IdParentescoPermitidoBE
        End Get
        Set(ByVal value As Integer)
            _IdParentescoPermitidoBE = value
        End Set
    End Property

    Property idParentesco() As Integer
        Get
            Return _idParentesco
        End Get
        Set(ByVal value As Integer)
            _idParentesco = value
        End Set
    End Property

    Property Parentesco() As String
        Get
            Return _Parentesco
        End Get
        Set(ByVal value As String)
            _Parentesco = value
        End Set
    End Property

    Property idProducto() As Integer
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Integer)
            _idProducto = value
        End Set
    End Property
    Property maxMontoAsegurado() As Integer
        Get
            Return _maxMontoAsegurado
        End Get
        Set(ByVal value As Integer)
            _maxMontoAsegurado = value
        End Set
    End Property
    Property edadMin() As Integer
        Get
            Return _edadMin
        End Get
        Set(ByVal value As Integer)
            _edadMin = value
        End Set
    End Property
    Property edadMax() As Integer
        Get
            Return _edadMax
        End Get
        Set(ByVal value As Integer)
            _edadMax = value
        End Set
    End Property



End Class
