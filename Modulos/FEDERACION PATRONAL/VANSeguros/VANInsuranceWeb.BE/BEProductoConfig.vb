﻿Public Class BEProductoConfig
    Inherits BEBase

    Private _idProducto As Int32 = 0
    Private _FrmCertificado As String = String.Empty
    Private _FrmAsegurado As String = String.Empty

    Public Property IdProducto() As Int32
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Int32)
            _idProducto = value
        End Set
    End Property

    Public Property FrmCertificado() As String
        Get
            Return _FrmCertificado
        End Get
        Set(ByVal value As String)
            _FrmCertificado = value
        End Set
    End Property

    Public Property FrmAsegurado() As String
        Get
            Return _FrmAsegurado
        End Get
        Set(ByVal value As String)
            _FrmAsegurado = value
        End Set
    End Property
   
End Class
