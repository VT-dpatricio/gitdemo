Public Class BEOpcionFrecuencia
    Inherits BEBase
    Private _idFrecuencia As Int32
    Private _nombre As String
    Private _idProducto As Int32
    Private _opcion As String

    Public Property IdFrecuencia() As Int32
        Get
            Return _idFrecuencia
        End Get
        Set(ByVal value As Int32)
            _idFrecuencia = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Public Property IdProducto() As Int32
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Int32)
            _idProducto = value
        End Set
    End Property

    Public Property Opcion() As String
        Get
            Return _opcion
        End Get
        Set(ByVal value As String)
            _opcion = value
        End Set
    End Property
End Class
