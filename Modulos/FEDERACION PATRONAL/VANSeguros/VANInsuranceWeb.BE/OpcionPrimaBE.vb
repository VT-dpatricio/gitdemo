﻿Public Class OpcionPrimaBE
    Inherits GenericEntity

    Sub New()

    End Sub

    Private _IdProducto As Integer
    Private _opcion As String
    Private _IdFrecuencia As Integer
    Private _Frecuencia As String
    Private _prima As Decimal
    Private _montoAsegurado As Decimal
    Private _idMonedaPrima As String
    Private _MonedaPrima As String
    Private _edadMin As Integer
    Private _edadMax As Integer
    Private _idPlanes As Integer
    Private _activo As Boolean

    Private _Id As Integer
    Public Property Id() As Integer
        Get
            Return _Id
        End Get
        Set(ByVal value As Integer)
            _Id = value
        End Set
    End Property

    Property IdProducto() As Integer
        Get
            Return _IdProducto
        End Get
        Set(ByVal value As Integer)
            _IdProducto = value
        End Set
    End Property
    Property activo() As Boolean
        Get
            Return _activo
        End Get
        Set(ByVal value As Boolean)
            _activo = value
        End Set
    End Property
    ReadOnly Property activoTxt() As String
        Get
            Return IIf(_activo = True, "SI", "NO")
        End Get
    End Property
    Property opcion() As String
        Get
            Return _opcion
        End Get
        Set(ByVal value As String)
            _opcion = value
        End Set
    End Property
    Property IdFrecuencia() As Integer
        Get
            Return _IdFrecuencia
        End Get
        Set(ByVal value As Integer)
            _IdFrecuencia = value
        End Set
    End Property
    Property Frecuencia() As String
        Get
            Return _Frecuencia
        End Get
        Set(ByVal value As String)
            _Frecuencia = value
        End Set
    End Property
    Property prima() As Decimal
        Get
            Return _prima
        End Get
        Set(ByVal value As Decimal)
            _prima = value
        End Set
    End Property
    Property montoAsegurado() As Decimal
        Get
            Return _montoAsegurado
        End Get
        Set(ByVal value As Decimal)
            _montoAsegurado = value
        End Set
    End Property
    Property idMonedaPrima() As String
        Get
            Return _idMonedaPrima
        End Get
        Set(ByVal value As String)
            _idMonedaPrima = value
        End Set
    End Property
    Property MonedaPrima() As String
        Get
            Return _MonedaPrima
        End Get
        Set(ByVal value As String)
            _MonedaPrima = value
        End Set
    End Property
    Property edadMin() As Integer
        Get
            Return _edadMin
        End Get
        Set(ByVal value As Integer)
            _edadMin = value
        End Set
    End Property
    Property edadMax() As Integer
        Get
            Return _edadMax
        End Get
        Set(ByVal value As Integer)
            _edadMax = value
        End Set
    End Property
    Property idPlanes() As Integer
        Get
            Return _idPlanes
        End Get
        Set(ByVal value As Integer)
            _idPlanes = value
        End Set
    End Property


End Class
