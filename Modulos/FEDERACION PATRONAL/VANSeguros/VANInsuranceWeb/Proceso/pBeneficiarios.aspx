﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pBeneficiarios.aspx.vb" Inherits="VANInsurance.pBeneficiario" EnableEventValidation="false"  culture="es-PE" uiCulture="es-PE"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register src="../Controles/Cargando.ascx" tagname="Cargando" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <meta id="FirstCtrlID" http-equiv="X-UA-Compatible" content="IE=8" />
    <title>Beneficiarios</title>

</head>
<body>
    <script type="text/javascript">
        function ValidaNroDoc(v) {
            if (v == 'L') {
                document.getElementById('<%=txtNroDocumento.ClientID%>').maxLength = '8';

            }
            else {
                document.getElementById('<%=txtNroDocumento.ClientID%>').maxLength = '20';

            }
        }

        function Numero(e) {
            key = (document.all) ? e.keyCode : e.which;
            if (key < 48 || key > 57) {
                if (key == 8 || key == 0) {
                    return true
                }
                else
                    return false;
            }

        }

    </script>

    <form id="form1" runat="server">        
        
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
        <table border="0" cellpadding="0" cellspacing="7" style="margin: auto; width: 645px">
            <tr>
                <td class="MarcoTabla">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="TablaTitulo">
                                Datos del Beneficiario</td>
                        </tr>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="4" style="width: 100%">
                        <tr>
                            <td class="CampoCelda" style="width: 119px; height: 13px">
                                Tipo Documento</td>
                            <td class="CampoCelda" style="width: 100px; height: 13px">
                                Nº Documento<asp:RequiredFieldValidator ID="RequiredFieldValidator7" 
                                    runat="server" ControlToValidate="txtNroDocumento" Display="None" 
                                    ErrorMessage="Ingrese N° Documento" InitialValue=" " SetFocusOnError="True">*</asp:RequiredFieldValidator>
                                <asp:Label ID="Label12" runat="server" EnableViewState="False" ForeColor="Red" 
                                    Text="*"></asp:Label>
                            </td>
                            <td class="CampoCelda" style="height: 13px">
                                &nbsp;</td>
                            <td class="CampoCelda" style="width: 100px; height: 13px">
                                Parentesco<asp:Label ID="Label10" runat="server" EnableViewState="False" ForeColor="Red"
                                    Text="*"></asp:Label></td>
                            <td class="CampoCelda" style="height: 13px">
                                Porcentaje<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                    ControlToValidate="txtPorcentaje" ErrorMessage="Ingrese el Porcentaje" InitialValue=" " Display="None" SetFocusOnError="True">*</asp:RequiredFieldValidator><asp:Label
                                        ID="Label4" runat="server" EnableViewState="False" ForeColor="Red" Text="*"></asp:Label></td>
                            <td class="CampoCelda" style="height: 13px">
                                %Acum.</td>
                        </tr>
                        <tr>
                            <td style="width: 119px">
                                <asp:DropDownList ID="cboTipoDoc" runat="server" Width="121px" onChange="ValidaNroDoc(this.value);">
                                </asp:DropDownList></td>
                            <td colspan="2">
                                <asp:TextBox ID="txtNroDocumento" onkeypress="return Numero(event)" 
                                    runat="server" Width="225px" MaxLength="20"></asp:TextBox></td>
                            <td style="width: 100px">
                                <asp:DropDownList ID="cboParentesco" runat="server" Width="117px">
                                </asp:DropDownList></td>
                            <td>
                                <asp:TextBox ID="txtPorcentaje" runat="server" Width="32px"></asp:TextBox>%</td>
                            <td style="text-align: center">
                                <asp:Label ID="lblAcumulado" runat="server" Text="Label"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="CampoCelda" style="width: 119px; height: 13px;">
                                Fecha Nacimiento<asp:Label ID="Label13" runat="server" EnableViewState="False" 
                                    ForeColor="Red" Text="*"></asp:Label>
                            </td>
                            <td class="CampoCelda" style="width: 110px; height: 13px;">
                                Apellido Paterno<asp:RequiredFieldValidator ID="RequiredFieldValidator6" 
                                    runat="server" ControlToValidate="txtApePat" Display="None" 
                                    ErrorMessage="Ingrese Apellido Paterno" InitialValue=" " SetFocusOnError="True">*</asp:RequiredFieldValidator>
                                <asp:Label ID="Label11" runat="server" EnableViewState="False" ForeColor="Red" 
                                    Text="*"></asp:Label>
                            </td>
                            <td class="CampoCelda" style="height: 13px;">
                                Apellido Materno</td>
                            <td class="CampoCelda" style="width: 100px; height: 13px;">
                                Primer Nombre<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server"
                                    ControlToValidate="txtPrimerNom" ErrorMessage="Ingrese Primer Nombre" InitialValue=" "
                                    SetFocusOnError="True" Display="None">*</asp:RequiredFieldValidator><asp:Label ID="Label3"
                                        runat="server" EnableViewState="False" ForeColor="Red" Text="*"></asp:Label></td>
                            <td class="CampoCelda" colspan="2" style="height: 13px">
                                Segundo Nombre</td>
                        </tr>
                        <tr>
                            <td style="width: 119px; height: 11px">
                                <asp:TextBox ID="txtFechaNac" runat="server" Width="90px"></asp:TextBox><asp:CompareValidator
                                        ID="CompareValidator3" runat="server" ControlToValidate="txtFechaNac" ErrorMessage="Ingrese una Fecha Nacimiento válida"
                                        Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" Display="None">*</asp:CompareValidator><asp:RangeValidator
                                            ID="rvFechaN" runat="server" ControlToValidate="txtFechaNac" ErrorMessage="La Fecha de Nacimiento no puede ser mayor a la actual."
                                            MaximumValue="01/01/2013" MinimumValue="01/01/1900" 
                                    SetFocusOnError="True" Type="Date" Display="None">*</asp:RangeValidator>
                            
                                <cc1:maskededitextender id="meeFechaNac" runat="server" cleartextoninvalid="True"
                                    mask="99/99/9999" masktype="Date" targetcontrolid="txtFechaNac">
  </cc1:maskededitextender>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
                                    ControlToValidate="txtFechaNac" Display="None" 
                                    ErrorMessage="Ingrese Fecha Nacimiento" InitialValue=" " SetFocusOnError="True">*</asp:RequiredFieldValidator>
                                </td>
                            <td style="width: 100px; height: 11px">
                                <asp:TextBox ID="txtApePat" runat="server" Width="110px" SkinID="txtM"></asp:TextBox></td>
                            <td style="height: 11px">
                                <asp:TextBox ID="txtApeMat" runat="server" Width="110px" SkinID="txtM"></asp:TextBox></td>
                            <td style="width: 100px; height: 11px">
                                <asp:TextBox ID="txtPrimerNom" runat="server" Width="110px" SkinID="txtM"></asp:TextBox></td>
                            <td colspan="2" style="height: 11px">
                                <asp:TextBox ID="txtSegundoNom" runat="server" Width="110px" SkinID="txtM"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td class="CampoCelda" style="width: 119px; height: 11px">
                                Domicilio</td>
                            <td style="width: 100px; height: 11px">
                            </td>
                            <td style="height: 11px">
                            </td>
                            <td style="width: 100px; height: 11px">
                            </td>
                            <td colspan="2" style="height: 11px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height: 11px">
                                <asp:TextBox ID="txtDireccion" runat="server" Width="360px" SkinID="txtM"></asp:TextBox></td>
                            <td colspan="3" style="height: 11px; text-align: right">
                                &nbsp;<asp:HiddenField ID="ID" runat="server" Value="0" />
                                <asp:Button ID="btnGrabar" runat="server" Text="Agregar" Width="68px" />&nbsp;
                                <asp:Button ID="btnCancelar" runat="server" CausesValidation="False" OnClick="btnCancelar_Click"
                                    Text="Cancelar" Width="70px" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="MarcoTabla">
                    <asp:GridView ID="gvBeneficiario" runat="server" AutoGenerateColumns="False" 
                        DataKeyNames="ID" OnRowDeleting="gvBeneficiario_RowDeleting" 
                        OnSelectedIndexChanged="gvBeneficiario_SelectedIndexChanged" 
                        EnableModelValidation="True">
                        <Columns>
                            <asp:TemplateField HeaderText="ID" Visible="False">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("ID") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="IDTipoDocumento" HeaderText="IDTipoDocumento" Visible="False" />
                            <asp:BoundField DataField="NroDocumento" HeaderText="N&#186; Documento" >
                                <ItemStyle Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Parentesco" HeaderText="Parentesco" >
                                <ItemStyle Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Porcentaje" HeaderText="Porcentaje %" >
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Apellidos y Nombre">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ApePaterno") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("ApePaterno") + " " +  Eval("ApeMaterno") + " " +  Eval("PrimerNom") + " " +  Eval("SegundoNom") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="False" 
                                        CommandName="Select" ImageUrl="~/Img/Grid/editar.gif" Text="Modificar" 
                                        ToolTip="Modificar" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="25px" />
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" CommandArgument='<%# Bind("ID") %>'
                                        ImageUrl="~/Img/Grid/anular.gif" OnClientClick="return confirm('Está seguro que desea eliminar el Beneficiario?');"
                                        Text="Delete" CommandName="delete" 
                                        Visible='<%# Convert.ToBoolean(Convert.ToDecimal(Session("EstForm"))=0) %>' 
                                        ToolTip="Eliminar" />
                                </ItemTemplate>
                                <ItemStyle Width="25px" HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            No se ha registrado ningún Beneficiario...
                        </EmptyDataTemplate>
                        <RowStyle Height="23px" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Button ID="btnAceptar" runat="server"
                        Text="Aceptar" CausesValidation="False" 
                        />
                    
                </td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="Label5" runat="server" EnableViewState="False" ForeColor="Red" Text="(*) Campos Obligatorios. "></asp:Label></td>
            </tr>
        </table>
            </ContentTemplate>
        </asp:UpdatePanel>
            <asp:UpdateProgress id="UpdateProgress1" runat="server" DisplayAfter="0">
        <progresstemplate>
<uc1:Cargando id="Cargando1" runat="server"></uc1:Cargando>
</progresstemplate>
    </asp:UpdateProgress>
        &nbsp;
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
        &nbsp;&nbsp;
        
    </form>
</body>
</html>
