﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.Master" CodeBehind="EmisionCertificado.aspx.vb" Inherits="VANInsurance.EmisionCertificado" Culture="es-AR" UICulture="es-PE" EnableEventValidation="false" %>

<%@ Register Src="~/Controles/Cargando.ascx" TagName="Cargando" TagPrefix="uc1" %>
<%@ Register Src="Controles/ccPrima.ascx" TagName="ccPrima" TagPrefix="asp" %>
<%@ Register Src="~/Controles/ccCrossSelling.ascx" TagPrefix="asp" TagName="ccCrossSelling" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../Scripts/jquery.simplemodal.1.4.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            //Activo el color hover de la tabla que contiene los links
            $("#tb_headerProduct").addClass('active');
            function close_accordion_section() {

                $('.accordion .accordion-section-title').removeClass('active');
                $('.accordion .accordion-section-content').slideUp(200).removeClass('open');

                //Remuevo el color hover de la tabla que contiene los links en caso de que haga un click
                $("#tb_headerProduct").removeClass('active');
            }


            $('.accordion-section-title').click(function (e) {
                // Grab current anchor value
                var currentAttrValue = $(this).attr('href');

                if ($(e.target).is('.active')) {
                    close_accordion_section();

                } else {
                    close_accordion_section();
                    // Add active class to section title
                    $(this).addClass('active');

                    //Verifico si es el click sobre el panel donde esta contenida la tabla
                    if (currentAttrValue == "#accordion-1") {
                        $("#tb_headerProduct").addClass('active');
                    }

                    // Open up the hidden content panel
                    $('.accordion ' + currentAttrValue).slideDown(200).addClass('open');
                }

                e.preventDefault();
            });


            jQuery('.tabs .tab-links a').on('click', function (e) {
                var currentAttrValue = jQuery(this).attr('href');

                // Show/Hide Tabs
                jQuery('.tabs ' + currentAttrValue).show().siblings().hide();

                // Change/remove current tab to active
                jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

                e.preventDefault();
            });

            $('#ctl00_contenedor_ccCrossSelling_hlinksTabs').val("");
        });

        function displayCrossSellingPanel() {
            //$('#ctl00_contenedor_lblMensaje').text()
            //"El certificado fue expedido satisfactoriamente."
            //alert($('<%= lblProducto_header.ClientID%>').val());
            $('#ctl00_contenedor_lblMensajeHeader').text(' - ' + $('#ctl00_contenedor_lblMensaje').text());
            $('.accordion-section-title').click();
            $("#crossSellingSection").show();
            ClearCrossSelling();
        }

        function cancelBack() {
            if ((event.keyCode == 8 ||
            (event.keyCode == 37 && event.altKey) ||
            (event.keyCode == 39 && event.altKey))
            &&
            (event.srcElement.form == null || event.srcElement.isTextEdit == false)
            ) {
                event.cancelBubble = true;
                event.returnValue = false;
            }
        }
        document.onkeydown = cancelBack;


        var pReturnUbigeo;

        function BuscarUbigeo(pReturn) {
            pReturnUbigeo = pReturn;
            BuscarModal("#frmBuscarUbigeo", 590, 380);
        }

        function SeleccionarUbigeo(pID, pNombre) {
            if (pReturnUbigeo == 'InfoTitular') {
                document.getElementById("ctl00_contenedor_C_IdCiudad").value = pID;
                document.getElementById("ctl00_contenedor_txtCiudad").value = pNombre;
            }
            if (pReturnUbigeo == 'DomicilioRiesgo') {
                document.getElementById("ctl00_contenedor_C_IdCiudad1").value = pID;
                document.getElementById("ctl00_contenedor_txtCiudad1").value = pNombre;
            }
            $.modal.close();
        }

        function BuscarOficina() {
            BuscarModal("#frmBuscarOficina", 590, 380);
        }

        function BuscarRamo() {
            BuscarModal("#frmBuscarRamo", 590, 380);
        }

        function SeleccionarOficina(pID, pNombre) {
            document.getElementById("ctl00_contenedor_C_IdOficina").value = pID;
            document.getElementById("ctl00_contenedor_txtOficina").value = pNombre;
            $.modal.close();
        }

        function SeleccionarRamo(pID, pNombre) {
            document.getElementById("ctl00_contenedor_C_IdRamo").value = pID;
            document.getElementById("ctl00_contenedor_txtRamo").value = pNombre;
            $.modal.close();
        }

        function BuscarInformador() {
            BuscarModal("#frmBuscarInformador", 590, 380);
        }

        function SeleccionarInformador(pID, pNombre, pIDOficina, pOficina) {
            document.getElementById("ctl00_contenedor_C_IdInformador").value = pID;
            document.getElementById("ctl00_contenedor_txtInformador").value = pID + ' - ' + pNombre;
            SeleccionarOficina(pIDOficina, pOficina);
        }

        function BuscarModal(pDiv, pWidth, pHeight, bgColor) {
            $(pDiv).modal({
                closeHTML: "<a href='#' title='Cerrar' class='modal-close'><img src='../Img/cerrar.gif' alt='Cerrar' width='16' height='16'/></a>",
                containerCss: {
                    backgroundColor: bgColor,
                    borderColor: "#565C63",
                    height: pHeight,
                    padding: 0,
                    width: pWidth
                },
                overlayClose: false,
                appendTo: '#aspnetForm',
                persist: true
            });
        }

        function BuscarModal(pDiv, pWidth, pHeight) {
            $(pDiv).modal({
                closeHTML: "<a href='#' title='Cerrar' class='modal-close'><img src='../Img/cerrar.gif' alt='Cerrar' width='16' height='16'/></a>",
                containerCss: {
                    backgroundColor: "#FFF",
                    borderColor: "#565C63",
                    height: pHeight,
                    padding: 0,
                    width: pWidth
                },
                overlayClose: false,
                appendTo: '#aspnetForm',
                persist: true
            });
        }

        function BuscarClienteA() {

            if (!isNaN(parseFloat($get('ctl00_contenedor_C_Cccliente').value))) {
                //Valido que sea un numero parasacarle el problema de los ceros a izquierda
                document.getElementById('ctl00_contenedor_C_Cccliente').value = parseFloat($get('ctl00_contenedor_C_Cccliente').value);
                document.getElementById('<%= txtNroDocCliente.ClientID %>').value = parseFloat($get('ctl00_contenedor_C_Cccliente').value);
            }

            BuscarModal("#frmBuscarCliente", 810, 493);
            document.getElementById('<%= ddlTipoDocCliente.ClientID %>').value = $get('ctl00_contenedor_C_IdTipoDocumento').value;
            document.getElementById('<%= txtNroDocCliente.ClientID %>').value = $get('ctl00_contenedor_C_Cccliente').value;
            $get('<%= btnBuscarCliente.ClientID %>').click();
        }

        function CloseSucessContactSave() {
            cerrarModal();
            alert('Solicitud de Contacto registrada correctamente');
            $('input:checkbox[id^="checkbox-7-"]:checked').each(function () {
                $(this).prop("checked", false);
            });
        }

        function OpenContact(idProducto) {
            var content="ctl00_contenedor_"
            var sIDproduct = "";
            var sNameproduct = "";
            $('input:checkbox[id^="checkbox-7-"]:checked').each(function () {
                sIDproduct = sIDproduct + $(this).attr('data-name') + ",";
                sNameproduct = sNameproduct.trim() + $(this).attr('title') + ",";
            });
            if (sNameproduct && sNameproduct.length > 0 && sNameproduct.indexOf(",", sNameproduct.length - 1) !== -1)
            {
                sNameproduct = sNameproduct.substring(0, sNameproduct.length - 1);
            }
           
            document.getElementById('<%= txtProductosInteres.ClientID%>').value = sNameproduct;
            document.getElementById('<%= txtDocumentoContacto.ClientID%>').value = document.getElementById(content + 'C_Cccliente').value;
            document.getElementById('<%= txtNombreContacto.ClientID%>').value = document.getElementById(content + 'C_Apellido1').value + ' ' + document.getElementById(content + 'C_Apellido2').value + ' ' + document.getElementById(content + 'C_Nombre1').value + ' ' + document.getElementById(content + 'C_Nombre2').value;
            document.getElementById('<%= txtEmailContacto.ClientID%>').value = document.getElementById(content + 'IA_Email').value;
            document.getElementById('<%= txtCelularContacto.ClientID%>').value = document.getElementById(content + 'IA_NroCelular').value;
            document.getElementById('<%= txtTelefonoContacto.ClientID%>').value = document.getElementById(content + 'C_CATelefono').value + '-' + document.getElementById(content + 'C_Telefono').value;
            document.getElementById('<%= hfIDProductoSelectCrossSelling.ClientID%>').value = sIDproduct.substring(0, sIDproduct.length - 1);
            $('#' + content + 'ddlTipoDocumentoContacto').val($('#' + content + 'C_IdTipoDocumento').val());
            BuscarModal("#frmSolicitudContacto", 569, 380, "#EFEFF2");
        }
        
        function OpenSolicitudContacto(idProducto) {
            document.getElementById('<%= hfIDProductoSelectCrossSelling.ClientID%>').value = idProducto
            document.getElementById('<%= btnListaSolicitudesContacto.ClientID%>').click();
            BuscarModal("#frmListaSolicitudesContacto", 740, 350);
        }

        function NewSale() {
            $get('<%= btnNuevaVenta.ClientID%>').click();
        }
        
        function OpenSale(idProduct, nameProduct) {
            document.getElementById('<%= hfIDProductoCrossSelling.ClientID %>').value = idProduct;
            try
            {
                window.open('EmisionCertificado.aspx?IdProducto=' + idProduct + '&IsCrossSelling=1', '_self');
            }
            catch (err)
            {
                alert(err.message);
            }

        }

        function ClearCrossSelling() {
            $('#ctl00_contenedor_ccCrossSelling_hlinksTabs').val("");
        }

     function BuscarCliente() {

        if (!isNaN(parseFloat($get('ctl00_contenedor_C_Cccliente').value))) {
            //Valido que sea un numero parasacarle el problema de los ceros a izquierda
            document.getElementById('ctl00_contenedor_C_Cccliente').value = parseFloat($get('ctl00_contenedor_C_Cccliente').value);
            document.getElementById('<%= txtNroDocCliente.ClientID %>').value = parseFloat($get('ctl00_contenedor_C_Cccliente').value);
            }

         BuscarModal("#frmBuscarCliente", 805, 493);
         document.getElementById('<%= ddlTipoDocCliente.ClientID %>').value = $get('ctl00_contenedor_C_IdTipoDocumento').value;
    }


    function modal(src) {
        $.modal('<iframe frameborder="0" src=' + src + ' height="400" width="650" style="border:0">', {
            closeHTML: "<a href='#' title='Cerrar' class='modal-close'><img src='../Img/cerrar.gif' alt='Cerrar' width='16' height='16'/></a>",
            containerCss: {
                backgroundColor: "#FFF",
                borderColor: "#565C63",
                height: 400,
                padding: 0,
                width: 650
            },
            overlayClose: false,
            appendTo: '#aspnetForm',
            persist: true
        });
    };

    function cerrarModal() {
        $.modal.close();
    }

    function Imprimir(ID, Tipo) {
        var src;
        //src = 'ImprimirCertificado.aspx?ID=' + ID;
        src = 'ImprimirCertificado.aspx?ID=' + ID + '&Tipo=' + Tipo;
        $.modal('<iframe frameborder="0" src=' + src + ' height="582" width="1000" style="border:0">', {
            closeHTML: "<div class='modal-DivclosePrint'> <a href='#' title='Cerrar' class='modal-closePrint'><img src='../Img/cerrarCer.gif' alt='Cerrar' width='55' height='18'/></a></div>",
            containerCss: {
                backgroundColor: "#FFF",
                borderColor: "#2C483C",
                height: 600,
                padding: 0,
                width: 1000
            },
            overlayClose: true,
            appendTo: '#aspnetForm',
            persist: true
        });

    }

    function ValidaNroDoc(txt, v) {
        if (v == 'L') {
            //            if (document.getElementById(txt).length > 8)
            //            {
            //                document.getElementById(txt).value = '' ;
            //            };

            document.getElementById(txt).maxLength = '8';
        }
        else {
            document.getElementById(txt).maxLength = '20';

        }
    }

    function Numero(e) {
        key = (document.all) ? e.keyCode : e.which;
        if (key < 48 || key > 57) {
            if (key == 8 || key == 0) {
                return true
            }
            else
                return false;
        }

    }

    function EmailPoliza() {

        if (document.getElementById('ctl00_contenedor_IA_EnviarMail').checked == true) {
            document.getElementById('ctl00_contenedor_IA_Email').disabled = false;
        }
        else {
            document.getElementById('ctl00_contenedor_IA_Email').disabled = true;
        }
    }

    function Print() {
        $get('<%= btnImprimir.ClientID%>').click();
    }

    function changeFrecuenciaContacto()
    {
        var selectedValue = $get('<%= ddlFrecuenciaContacto.ClientID%>').value;
        if (selectedValue !== undefined && selectedValue != null && selectedValue == "F") {
            $('#' + '<%= txtValorFrecuenciaContacto.ClientID%>').val("");
            $('#' + '<%= txtValorDateFrecuenciaContacto.ClientID%>').val("");
            $('#' + '<%= txtValorFrecuenciaContacto.ClientID%>').css("display", "none");
            $('#' + '<%= txtValorDateFrecuenciaContacto.ClientID%>').css("display", "inline");
        } else {
            $('#' + '<%= txtValorFrecuenciaContacto.ClientID%>').css("display", "inline");
            $('#' + '<%= txtValorDateFrecuenciaContacto.ClientID%>').css("display", "none");
        }
    }

    </script>


    <style type="text/css">
        .auto-style1 {
            height: 55px;
            width: 844px;
        }
        .auto-style2 {
            width: 844px;
        }
        .auto-style3 {
            width: 4px;
            text-align: right;
            font-size: 11px;
            height: 2px;
        }
        .auto-style4 {
            height: 3px;
        }
        .auto-style5 {
            width: 844px;
            height: 2px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenedor" runat="server">

    <!--begin .accordion-->
    <div class="accordion">
			<div class="accordion-section">
                    <table id="tb_headerProduct" width="100%" border="0" cellspacing="0" class="headerTableCrossSelling">
                        <tr>
                        <td><a class="accordion-section-title active" style="color:#fff;" href="#accordion-1"><asp:Label ID="lblProducto_header" runat="server"></asp:Label><asp:Label ID="lblMensajeHeader" runat="server"></asp:Label></a></td>
                        <td width="68px"><div class="bg_iconHeader"><asp:Image ID="img_new_sale" CssClass="iPrint" ToolTip="Nueva Venta" runat="server" ImageUrl="~/Img/Icons/CrossSelling/iNewSale.png" /></div></td>
                        <td width="68px"><div class="bg_iconHeader"><asp:Image ID="img_print" CssClass="iPrint" ToolTip="Imprimir Certificado" runat="server" ImageUrl="~/Img/Icons/CrossSelling/iPrint.png" /></div></td>
                        </tr>
                    </table>
				<div id="accordion-1" class="accordion-section-content open" style="display: block;">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table style="width: 800px; margin: auto">
                                <tr>
                                    <td style="text-align: center; font-weight: bold; height: 26px;">
                                        <asp:Label CssClass="titleProduct" ID="lblProducto" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hfIDProducto" runat="server" Value="0" />
                                        <asp:HiddenField ID="hfIDProductoCrossSelling" runat="server" Value="0" />
                                        <asp:HiddenField ID="hfIDProductoSelectCrossSelling" runat="server" Value="0" />
                                        <asp:HiddenField ID="hfEstForm" runat="server" Value="0" />
                                        <asp:HiddenField ID="hfEstadoCetificado" runat="server" Value="1" />
                                        <asp:HiddenField ID="hfCodAsegurador" runat="server" Value="1" />

                                    </td>
                                </tr>
                            </table>

                            <asp:Panel ID="pFrmVenta" runat="server">
                                <asp:PlaceHolder ID="phFormulario" runat="server"></asp:PlaceHolder>
                            </asp:Panel>

                            <table style="width: 850px; margin: auto">
                                <tr>
                                    <td style="text-align: left" class="auto-style2">
                        
                                        <asp:Panel ID="pMensaje" runat="server" Visible="False" >
                                                <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right" class="auto-style1">

                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
                                        <asp:Button ID="btnNuevaVenta" runat="server" CausesValidation="False" Enabled="False" Text="Nueva Venta"  />
                                        <asp:Button ID="btnBeneficiario" runat="server" CausesValidation="False" Text="Beneficiarios"  />
                                        <asp:Button ID="btnAsegurado" runat="server" CausesValidation="False" Text="Asegurados" />
                                        <asp:Button ID="btnGrabar" runat="server" Text="Guardar Venta" />
                                        <asp:Button ID="btnImprimir" runat="server" Enabled="False" Text="Imprimir"  />
                                        <asp:Button ID="btnCancelar" runat="server" CausesValidation="False" OnClientClick="return confirm('¿Está seguro que desea cancelar la Venta?');" Text="Cancelar" Visible="False" />
                                    </td>
                                </tr>
                            </table>

                            <table>
                                    <tr>
                                        <td class="auto-style27" style="text-align: left">
                                            <asp:Label ID="lblResult" runat="server"></asp:Label>
                                            <asp:Button ID="btnCambiarVenta" runat="server" CausesValidation="False" CssClass="BotonOculto" />
                                            <asp:Button ID="btnSeleccionarMarca" runat="server" CausesValidation="False" CssClass="BotonOculto" />
                                            <asp:Button ID="btn_reloadPlanRamo" runat="server" CssClass="BotonOculto" Text="Reload" ValidationGroup="BuscarRamo" Width="60px" />
                                        </td>
                                    </tr>
  
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnActivaPreventa" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="BCbtnAceptar" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="BCbtnCargarPreventa" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <br />
                    &nbsp;<asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0">
                        <ProgressTemplate>
                            <uc1:Cargando ID="Cargando1" runat="server" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
				</div><!--end .accordion-section-content-->
			</div><!--end .accordion-section-->
            <div id="crossSellingSection" class="accordion-section" style="display: none;">
                <a class="accordion-section-title" style="color: #fff;" href="#accordion-3">CROSS SELLING</a>
                <div id="accordion-3" class="accordion-section-content" style="display: none;">
                    <asp:UpdatePanel ID="UpdatePanelCrossSelling" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table id="Table2" width="100%" border="0" cellspacing="0">
                                <tr>
                                <td width="100%"></td>
                                <td width="68px">&nbsp;</td>
                                <td width="68px"><div class="bg_btnContactHeader"><asp:Image ID="img_contact" ToolTip="Contactar" CssClass="iPrint" runat="server" ImageUrl="~/Img/Icons/CrossSelling/iPhone.png" /></div></td>
                                </tr>
                            </table>
                            <asp:ccCrossSelling runat="server" ID="ccCrossSelling" />
                        </ContentTemplate>
                        <Triggers>
                           <asp:AsyncPostBackTrigger ControlID="btnGrabar" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <!--end .accordion-section-content-->
            </div>
            <!--end .accordion-section-->
    </div>
    <!--end .accordion-->

    <!--begin forms search-->
    <div id="frmBuscarUbigeo" style="width: 590px; height: 200px; display: none; margin: auto;">
        <asp:UpdatePanel ID="upBuscarUbigeo" runat="server" UpdateMode="Conditional">
            <ContentTemplate>

                <table class="TablaMarco" style="width: 100%">
                    <tr>
                        <td class="TablaTitulo">Buscar Localidad</td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 38px">Filtro:</td>
                                    <td style="width: 196px">
                                        <asp:TextBox ID="BUtxtBuscarUbigeo" runat="server" Width="200px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Button ID="BUbtnBuscarUbigeo" runat="server" Text="Buscar" Width="60px"
                                            ValidationGroup="BuscarUbigeo" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:GridView ID="gvBuscarUbigeo" runat="server" AutoGenerateColumns="False" Width="100%"
                                            EnableModelValidation="True" AllowPaging="True">
                                            <Columns>
                                                <asp:BoundField DataField="IDCiudad" HeaderText="IDCiudad" Visible="False">
                                                    <ItemStyle HorizontalAlign="Center" Width="90px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CodPostal" HeaderText="Código">
                                                    <ItemStyle HorizontalAlign="Center" Width="90px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Ciudad" HeaderText="Localidad" />
                                                <asp:BoundField DataField="Provincia" HeaderText="Provincia">
                                                    <ItemStyle Width="150px" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ibtnSelUbigeo" runat="server"
                                                            ImageUrl="~/Img/hand.gif" CausesValidation="False" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle Height="25px" />
                                        </asp:GridView>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="uProBuscarUbigeo" runat="server" DisplayAfter="0">
            <ProgressTemplate>
                <uc1:Cargando ID="CargandoBU" runat="server" />
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>

    <div id="frmBuscarOficina"
        style="width: 590px; height: 200px; display: none; margin: auto;">
        <asp:UpdatePanel ID="upOficina" runat="server" UpdateMode="Conditional">
            <ContentTemplate>

                <table class="TablaMarco" style="width: 100%">
                    <tr>
                        <td class="TablaTitulo">Buscar Sucursal</td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 38px">Filtro:</td>
                                    <td style="width: 196px">
                                        <asp:TextBox ID="txtBuscarOficina" runat="server" Width="200px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnBuscarOficina" runat="server" Text="Buscar" Width="60px"
                                            ValidationGroup="BuscarOficina" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:GridView ID="gvBuscarOficina" runat="server" AutoGenerateColumns="False" Width="100%"
                                            EnableModelValidation="True" AllowPaging="True" DataKeyNames="IDOficina">
                                            <Columns>
                                                <asp:BoundField DataField="CodOficina" HeaderText="N°">
                                                    <ItemStyle HorizontalAlign="Center" Width="70px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ibtnSelOficina" runat="server"
                                                            ImageUrl="~/Img/hand.gif" CausesValidation="False" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle Height="25px" />
                                        </asp:GridView>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress2" runat="server" DisplayAfter="0">
            <ProgressTemplate>
                <uc1:Cargando ID="Cargando2" runat="server" />
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>

    <div id="frmBuscarRamo"
        style="width: 590px; height: 200px; display: none; margin: auto;">
        <asp:UpdatePanel ID="upRamo" runat="server" UpdateMode="Conditional">
            <ContentTemplate>

                <table class="TablaMarco" style="width: 100%">
                    <tr>
                        <td class="TablaTitulo">Buscar Ramo</td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 38px">Filtro:</td>
                                    <td style="width: 196px">
                                        <asp:TextBox ID="txtBuscarRamo" runat="server" Width="200px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnBuscarRamo" runat="server" Text="Buscar" Width="60px"
                                            ValidationGroup="BuscarRamo" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:GridView ID="gvBuscarRamo" runat="server" AutoGenerateColumns="False"
                                            EnableModelValidation="True" AllowPaging="True" DataKeyNames="IDRamo"  OnSelectedIndexChanged="gvBuscarRamo_SelectedIndexChanged" 
                                            >
                                            <Columns>
                                                <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                         <asp:ImageButton ID="ibtnSelRamo" runat="server" ImageUrl="~/Img/seleccionar.gif" CausesValidation="False"  CommandArgument='<%# Bind("IDRamo") %>'/>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle Height="25px" />
                                        </asp:GridView>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
     
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress3" runat="server" DisplayAfter="0">
            <ProgressTemplate>
                <uc1:Cargando ID="CargandoRamo" runat="server" />
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>

    <div id="frmBuscarInformador"
        style="width: 590px; height: 200px; display: none; margin: auto;">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table class="TablaMarco" style="width: 100%">
                    <tr>
                        <td class="TablaTitulo">Buscar Vendedor</td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 38px">Filtro:</td>
                                    <td style="width: 196px">
                                        <asp:TextBox ID="txtBuscarInformador" runat="server" Width="200px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Button ID="ibtnBuscarInformador" runat="server" Text="Buscar" Width="60px"
                                            ValidationGroup="Buscar" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:GridView ID="gvBuscarInformador" runat="server" AutoGenerateColumns="False"
                                            EnableModelValidation="True" AllowPaging="True" Width="100%">
                                            <Columns>
                                                <asp:BoundField DataField="IDInformador" HeaderText="N°">
                                                    <ItemStyle HorizontalAlign="Center" Width="90px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ibtnSelInformador" runat="server"
                                                            ImageUrl="~/Img/hand.gif" CausesValidation="False" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle Height="25px" />
                                        </asp:GridView>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress30" runat="server" DisplayAfter="0">
            <ProgressTemplate>
                <uc1:Cargando ID="Cargando3" runat="server" />
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>

    <div id="frmListaSolicitudesContacto" style="width: 745px; display: none; margin: auto">
        <asp:Button ID="btnListaSolicitudesContacto" runat="server" CausesValidation="False" CssClass="BotonOculto"/>
        <asp:UpdatePanel ID="updPanelGridSolicitudes" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:GridView ID="gvSolicitudesContacto" runat="server" AutoGenerateColumns="False"
                                                                    EnableModelValidation="False" Caption="Solicitudes Contacto">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="FechaCreacion" HeaderText="Fecha Solicitud" dataformatstring="{0:dd-M-yyyy}">
                                                                                <ItemStyle HorizontalAlign="Center" Width="42px"></ItemStyle>
                                                                         </asp:BoundField>
                                                                         <asp:BoundField DataField="CantidadDiasContacto" HeaderText="Cantidad días">
                                                                                <ItemStyle HorizontalAlign="Center" Width="11px"></ItemStyle>
                                                                         </asp:BoundField>
                                                                          <asp:BoundField DataField="ccCliente" HeaderText="Documento">
                                                                                <ItemStyle HorizontalAlign="Center" Width="90px"></ItemStyle>
                                                                         </asp:BoundField>
                                                                          <asp:BoundField DataField="NombreCompleto" HeaderText="Nombre y Apellido">
                                                                                <ItemStyle HorizontalAlign="Center" Width="132px"></ItemStyle>
                                                                         </asp:BoundField>
                                                                        <asp:BoundField DataField="Observaciones" HeaderText="Observaciones">
                                                                                <ItemStyle HorizontalAlign="Left" Width="200px"></ItemStyle>
                                                                         </asp:BoundField> 
                                                                    </Columns>
                                                                    <EmptyDataTemplate>
                                                                        No se encontraron solicitudes de contacto
                                                                    </EmptyDataTemplate>
                                                                    <RowStyle Height="23px" />
                                                                </asp:GridView>
                </ContentTemplate>
                <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnListaSolicitudesContacto" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>

        </div>

    <div id="frmSolicitudContacto"
        style="background-color:#EFEFF2; width: 250px; display: none;">
        <asp:UpdatePanel ID="updSolicitudContacto" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table width="569px" border="0" style="background-color:#EFEFF2; height: 346px;">
                      <tr>
                        <td colspan="7"><div class="bg-dcontact"><h7>SOLICITUD DE CONTACTO</h7></div></td>
                      </tr>
                      <tr>
                          <td class="cell-data">&nbsp;</td>
                          <td>&nbsp;</td>
                          <td class="cell-data">&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td class="auto-style2">&nbsp;</td>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="cell-data">Tipo</td>
                        <td>
                            <asp:DropDownList CssClass="ddlDisable" ID="ddlTipoDocumentoContacto" runat="server" Width="100px" Enabled="False">
                                        </asp:DropDownList>
                          </td>
                        <td class="cell-data">Documento</td>
                        <td>
                            <asp:TextBox SkinID="txtDisable" ID="txtDocumentoContacto" Enabled="False" runat="server" Width="135px"></asp:TextBox>
                          </td>
                        <td>&nbsp;</td>
                        <td class="auto-style2">&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="cell-data">Nombre</td>
                        <td colspan="3">
                            <asp:TextBox SkinID="txtDisable" ID="txtNombreContacto" runat="server" Enabled="False"  ReadOnly="True" Width="100%"></asp:TextBox>
                          </td>
                        <td>&nbsp;</td>
                        <td class="auto-style2">&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="cell-data">Email</td>
                        <td colspan="3">
                            <asp:TextBox ID="txtEmailContacto" runat="server" ReadOnly="False"  Width="100%"></asp:TextBox>
                          </td>
                        <td>&nbsp;</td>
                        <td class="auto-style2">&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="cell-data">Tel</td>
                        <td colspan="3">
                            <asp:TextBox ID="txtTelefonoContacto" runat="server" ReadOnly="False" Width="100%"></asp:TextBox>
                          </td>
                        <td>&nbsp;</td>
                        <td class="auto-style2">&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="cell-data">Cel</td>
                        <td colspan="3">
                            <asp:TextBox ID="txtCelularContacto" runat="server" ReadOnly="False"  Width="100%"></asp:TextBox>
                          </td>
                        <td>&nbsp;</td>
                        <td class="auto-style2">&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="cell-data">Productos de Interes</td>
                        <td colspan="3" rowspan="2">
                            <asp:TextBox SkinID="txtDisable" ID="txtProductosInteres" runat="server" Enabled="False" ReadOnly="True"  Width="100%" Height="51px" TextMode="MultiLine"></asp:TextBox>
                          </td>
                        <td>&nbsp;</td>
                        <td class="auto-style2">&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="cell-data">&nbsp;</td>

                        <td>&nbsp;</td>
                        <td class="auto-style2">&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
						<tr>
                        <td class="cell-data">Contactar en:</td>
                        <td>
                          <asp:DropDownList ID="ddlFrecuenciaContacto" runat="server" Width="100px" Enabled="True">
                              </asp:DropDownList>
                          </td>
                        <td class="cell-data-left" colspan="2"> <asp:TextBox ID="txtValorDateFrecuenciaContacto" runat="server" Width="61px" style="display:none"></asp:TextBox><asp:TextBox ID="txtValorFrecuenciaContacto" MaxLength="10" runat="server" Width="61px"  Visible="true"></asp:TextBox>
                             <cc1:MaskedEditExtender ID="MEE2" runat="server" ClearTextOnInvalid="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtValorDateFrecuenciaContacto"></cc1:MaskedEditExtender>
                            <asp:CompareValidator ID="CV_FD" runat="server" ControlToValidate="txtValorDateFrecuenciaContacto" ErrorMessage="Ingrese una Fecha válida" Operator="DataTypeCheck" Type="Date">*</asp:CompareValidator>
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="cell-data">Observaciones</td>
                        <td colspan="3" rowspan="4">
                            <asp:TextBox ID="txtObservacionesContacto" runat="server" ReadOnly="false" Width="100%" Height="89px" MaxLength="8000" TextMode="MultiLine"></asp:TextBox>
                          </td>
                        <td>&nbsp;</td>
                        <td class="auto-style2">
                            <asp:Button ID="btnGuardarSolicitudContacto" runat="server" CausesValidation="false" Text="Guardar" ValidationGroup="CONT" />
                          </td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="cell-data"></td>
                        <td></td>
                        <td class="auto-style2">
                            <asp:Button ID="btnCancelarSolicitudContacto" runat="server" CausesValidation="False" OnClientClick="$.modal.close(); return false;"  Text="Cancelar" />
                          </td>
                        <td></td>
                      </tr>
                      <tr>
                          <td class="cell-data">&nbsp;</td>
                          <td>&nbsp;</td>
                          <td class="auto-style2">&nbsp;</td>
                          <td>&nbsp;</td>
                      </tr>
                    </table>
             </ContentTemplate>
                <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnGuardarSolicitudContacto" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="updateProgressBarSolicitudContacto" runat="server" DisplayAfter="0">
                <ProgressTemplate>
                    <uc1:Cargando ID="CargandoSolicitudContacto" runat="server" />
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>

    <div id="frmBuscarCliente" style="width: 810px; display: none;">
       
        <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdateMode="Conditional">
            <ContentTemplate>

                <table class="TablaMarco" style="width: 740px">
                    <tr>
                        <td class="TablaTitulo">Buscar Cliente</td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width: 793px">
                                <tr>
                                    <td style="width: 98px">
                                        <asp:DropDownList ID="ddlTipoDocCliente" runat="server" Width="120px">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 99px">
                                        <asp:TextBox ID="txtNroDocCliente" runat="server" Width="150px"></asp:TextBox>
                                    </td>
                                    <td style="width: 476px">
                                        <asp:Button ID="btnBuscarCliente" runat="server" Text="Buscar" Width="60px"
                                            ValidationGroup="Buscar" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:MultiView ID="mvBCliente" runat="server">
                                            <asp:View ID="View1" runat="server">
                                                <asp:HiddenField ID="BChfIDEstadoCivil" runat="server" />
                                                <asp:HiddenField ID="BChfIDLocalidad" runat="server" />
                                                <table style="width: 654px">
                                                    <tr>
                                                        <td colspan="2">N° Cliente</td>
                                                        <td style="width: 54px">N° CUIT</td>
                                                        <td>&nbsp;</td>
                                                        <td>Apellido Paterno</td>
                                                        <td style="width: 100px">Apellido Materno</td>
                                                        <td colspan="2">Primer Nombre</td>
                                                        <td>Segundo Nombre</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="BCtxtNroCliente" runat="server" ReadOnly="True" Width="100px"></asp:TextBox>
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="BCtxtNroCUIT" runat="server" ReadOnly="True" Width="110px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="BCtxtApellidoPaterno" runat="server" ReadOnly="True"
                                                                Width="100px"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 100px">
                                                            <asp:TextBox ID="BCtxtApellidoMaterno" runat="server" ReadOnly="True"
                                                                Width="100px"></asp:TextBox>
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="BCtxtPrimerNombre" runat="server" ReadOnly="True"
                                                                Width="110px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="BCtxtSegundoNombre" runat="server" ReadOnly="True"
                                                                Width="120px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">Fecha Nac.</td>
                                                        <td style="width: 54px">Sexo</td>
                                                        <td>&nbsp;</td>
                                                        <td>Estado Civil</td>
                                                        <td style="width: 100px">Nacionalidad</td>
                                                        <td colspan="3">Dirección / Calle</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="BCtxtFechaNacimiento" runat="server" ReadOnly="True"
                                                                Width="100px"></asp:TextBox>
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="BCtxtSexo" runat="server" ReadOnly="True" Width="110px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="BCtxtEstadoCivil" runat="server" ReadOnly="True" Width="100px"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 100px">
                                                            <asp:TextBox ID="BCtxtNacionalidad" runat="server" ReadOnly="True"
                                                                Width="100px"></asp:TextBox>
                                                        </td>
                                                        <td colspan="3">
                                                            <asp:TextBox ID="BCtxtCalle" runat="server" ReadOnly="True" Width="235px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 49px">Número</td>
                                                        <td style="width: 77px">&nbsp;</td>
                                                        <td style="width: 54px">Piso</td>
                                                        <td>Dpto.</td>
                                                        <td>Cod. Postal</td>
                                                        <td colspan="4">Localidad / Provincia</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="BCtxtDirNumero" runat="server" ReadOnly="True" Width="100px"></asp:TextBox>
                                                        </td>
                                                        <td colspan="0">
                                                            <asp:TextBox ID="BCtxtDirPiso" runat="server" ReadOnly="True" Width="50px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="BCtxtDirDpto" runat="server" ReadOnly="True" Width="50px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="BCtxtDirCodPostal" runat="server" ReadOnly="True"
                                                                Width="100px"></asp:TextBox>
                                                        </td>
                                                        <td colspan="4">
                                                            <asp:TextBox ID="BCtxtLocalidadPro" runat="server" ReadOnly="True"
                                                                Width="342px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 49px">Cod Área</td>
                                                        <td colspan="2">Teléfono</td>
                                                        <td>Cod. Área</td>
                                                        <td>Otro Teléfono</td>
                                                        <td style="width: 100px">Email</td>
                                                        <td>&nbsp;</td>
                                                        <td colspan="2">Profesión</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 49px">
                                                            <asp:TextBox ID="BCtxtCAreaTel" runat="server" ReadOnly="True" Width="50px"></asp:TextBox>
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="BCtxtTelefono" runat="server" ReadOnly="True" Width="100px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="BCtxtCAreaOtroTel" runat="server" ReadOnly="True" Width="50px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="BCtxtOtroTelefono" runat="server" ReadOnly="True"
                                                                Width="100px"></asp:TextBox>
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="BCtxtEmail" runat="server" ReadOnly="True" Width="160px"></asp:TextBox>
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="BCtxtProfesion" runat="server" ReadOnly="True" Width="170px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">Lugar de Nacimiento</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td colspan="2">&nbsp;</td>
                                                        <td colspan="0">&nbsp;</td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <asp:TextBox ID="BCtxtLNacimiento" runat="server" ReadOnly="True" Width="160px"></asp:TextBox>
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td colspan="2">&nbsp;</td>
                                                        <td colspan="0">&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="9">
                                                                <asp:GridView ID="gvCuentaTarjeta" runat="server" AutoGenerateColumns="False" Width="100%"
                                                                    EnableModelValidation="True" Caption="Medio Pago">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="ID" Visible="False"><ItemTemplate><asp:Label ID="Label1" runat="server" Text='<%# Bind("ID") %>'></asp:Label></ItemTemplate><EditItemTemplate><asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("ID") %>'></asp:TextBox></EditItemTemplate></asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="IDMedioPago" Visible="False"><ItemTemplate><asp:Label ID="Label2" runat="server" Text='<%# Bind("IDMedioPagoBAIS")%>'></asp:Label></ItemTemplate><EditItemTemplate><asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("IDMedioPagoBAIS") %>'></asp:TextBox></EditItemTemplate></asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Tipo"><ItemTemplate><asp:Label ID="Label3" runat="server" Text='<%# Bind("Descripcion")%>'></asp:Label></ItemTemplate><EditItemTemplate><asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox></EditItemTemplate><ItemStyle Width="130px" /></asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Moneda"><ItemTemplate><asp:Label ID="Label4" runat="server" Text='<%# Bind("IDMoneda") %>'></asp:Label></ItemTemplate><EditItemTemplate><asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("IDMoneda") %>'></asp:TextBox></EditItemTemplate><ItemStyle HorizontalAlign="Center" Width="80px" /></asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Número"><ItemTemplate><asp:Label ID="Label5" runat="server" Text='<%# Bind("Numero") %>'></asp:Label></ItemTemplate><EditItemTemplate><asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("Numero") %>'></asp:TextBox></EditItemTemplate></asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Raíz"><EditItemTemplate><asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("raiz") %>'></asp:TextBox></EditItemTemplate><ItemTemplate><asp:Label ID="Label6" runat="server" Text='<%# Bind("raiz") %>'></asp:Label></ItemTemplate><ItemStyle HorizontalAlign="Center" Width="140px" /></asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="CBU"><ItemTemplate><asp:Label ID="Label7" runat="server" Text='<%# Bind("CBU") %>'></asp:Label></ItemTemplate><EditItemTemplate><asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("CBU") %>'></asp:TextBox></EditItemTemplate></asp:TemplateField>
                                                                        <asp:TemplateField><ItemTemplate><asp:ImageButton ID="ibSeleccionar" runat="server" CausesValidation="False"
                                                                                    CommandName="Select" ImageUrl="~/Img/hand.gif" ToolTip="Seleccionar" /></ItemTemplate><EditItemTemplate><asp:TextBox ID="TextBox1" runat="server"></asp:TextBox></EditItemTemplate><ItemStyle HorizontalAlign="Center" Width="30px" /></asp:TemplateField>
                                                                    </Columns>
                                                                    <EmptyDataTemplate>
                                                                        No se encontraron Cuentas/Tarjetas
                                                                    </EmptyDataTemplate>
                                                                    <RowStyle Height="23px" />
                                                                </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="9">&nbsp;</td>
                                                        btnActivaPreventa
                                                    </tr>
                                                </table>
                                                &nbsp;<table style="width: 100%;">
                                                    <tr>
                                                        <td style="text-align: center">

                                                            <asp:Button ID="BCbtnAceptar" runat="server" Text="Aceptar"
                                                                CausesValidation="False" />

                                                        </td>

                                                           <td style="text-align:center">           
                                                             <asp:Button ID="BCbtnCargarPreventa" runat="server" Text="Cargar Pre-Venta" 
                                                                         CausesValidation="False" />
                                                            </td> 
                                                    </tr>

                                                </table>

                                            </asp:View>

                                            <asp:View ID="View2" runat="server">
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td style="text-align: center">La persona que busca, no es cliente del banco. Desea Vender el seguro como Pre-Venta
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center">

                                                            <asp:Button ID="btnActivaPreventa" runat="server" Text="SI"
                                                                CausesValidation="False" />
                                                            &nbsp;<asp:Button ID="btnCancelarBCliente" runat="server" Text="NO"
                                                                CausesValidation="False" OnClientClick="$.modal.close(); return false;" />

                                                        </td>
                                                    </tr>

                                                </table>
                                            </asp:View>
                                        </asp:MultiView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress8" runat="server" DisplayAfter="0">
            <ProgressTemplate>
                <uc1:Cargando ID="Cargando8" runat="server" />
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
    <!--end forms search-->
</asp:Content>
