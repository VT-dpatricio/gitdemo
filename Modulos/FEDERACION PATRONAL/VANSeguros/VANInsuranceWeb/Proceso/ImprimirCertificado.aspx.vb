﻿Imports Microsoft.Reporting.WebForms
Imports System.IO
Imports VAN.InsuranceWeb.BL
Imports VAN.InsuranceWeb.BE

Public Class Imprimir
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim rv As New ReportViewer
            Dim warnings As Warning() = Nothing
            Dim streamids As String() = Nothing
            Dim mimeType As String = Nothing
            Dim encoding As String = Nothing
            Dim extension As String = Nothing
            Dim bytes As Byte()
            Dim deviceInf As String = Nothing
            'Dim rpt As String = "CertificadoITAU2"
            Dim rpt As String = ""
            Dim idCertificado As String = Request.QueryString("ID")
            Dim TipoReporte As String = Request.QueryString("Tipo")
            Dim oBLPro As New BLProducto
            Dim oBEPro As BEProducto = oBLPro.Seleccionar(CInt(idCertificado.Substring(0, 4)))

            'Dim idCertificado As String = "5327-111111"
            Dim ServidorRS As String = ConfigurationManager.AppSettings("ReportServerUri").ToString()
            Dim ServidorDirApp As String = ConfigurationManager.AppSettings("ServidorRSDirApp").ToString()

            Select Case TipoReporte
                Case "C"
                    'Certificado
                    rpt = oBEPro.ReporteCertificado
                Case "S"
                    'Solicitud
                    rpt = oBEPro.ReporteSolicitud
                Case "G"
                    'Condicion
                    rpt = oBEPro.ReporteCondicion
                    ServidorRS = ConfigurationManager.AppSettings("UrlDocumentosBAIS").ToString()
                    Response.Redirect(ServidorRS & "/Condiciones/" & rpt)
                    Exit Sub
                Case "D"
                    'Declaracion Jurada
                    rpt = oBEPro.ReporteDeclaracionJurada
                    ServidorRS = ConfigurationManager.AppSettings("UrlDocumentosBAIS").ToString()
                    Response.Redirect(ServidorRS & "/DeclaracionJurada/" & rpt)
                    Exit Sub
            End Select

            ReportAuthentication.ConfigureAuthentication(rv)

            rv.ProcessingMode = ProcessingMode.Remote
            rv.ServerReport.ReportServerUrl = New Uri(ServidorRS)
            rv.ServerReport.ReportPath = ServidorDirApp + rpt
            Dim paramList As Generic.List(Of ReportParameter) = New Generic.List(Of ReportParameter)()
            paramList.Add(New ReportParameter("IDCertificado", idCertificado))
            rv.ServerReport.SetParameters(paramList)
            bytes = rv.ServerReport.Render("PDF", deviceInf, mimeType, encoding, extension, streamids, warnings)
            Response.ClearContent()
            Response.ContentType = "application/pdf"
            Response.BinaryWrite(bytes)
            Response.Flush()
        End If
    End Sub

End Class