﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.Master" CodeBehind="SolicitudesContacto.aspx.vb" Inherits="VANInsurance.SolicitudesContacto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contenedor" runat="server">

    <table class="TablaMarco" style="width: 100%">
                    <tr>
                        <td class="TablaTitulo">Solicitudes Contacto Clientes</td>
                    </tr>
                    <tr>
                        <td>
                        <asp:GridView ID="gvListaSolicitudesContacto"  AutoGenerateColumns="true" runat="server" AllowPaging="True" 
                            OnRowDataBound="gvListaSolicitudesContacto_RowDataBound">
                            <RowStyle Height="25px" />
                              <EmptyDataTemplate>
                                 No se encontraron solicitudes de contacto registradas
                               </EmptyDataTemplate>
                         </asp:GridView>
                        </td>
                    </tr>
    </table>
 </asp:Content>