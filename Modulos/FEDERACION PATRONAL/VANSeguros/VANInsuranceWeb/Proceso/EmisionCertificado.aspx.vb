﻿Imports System.Xml
Imports AjaxControlToolkit
Imports VAN.InsuranceWeb.BL
Imports VAN.InsuranceWeb.BE
Imports System.IO
Imports VAN.InsuranceWeb.BF
Imports WSInvoker = VANWebServiceInvoker

Public Class EmisionCertificado
    Inherits PaginaBase
    Dim IDPRODUCTO As Int32 = 0
    Dim IDRAMO As Int32 = 1
    Dim cc As ccPrima
    Dim pCondicionPlan As String
    Dim FinVigenciaAbierta As New Dictionary(Of Integer, Integer)

    Public Const REGULAR_EXPRESION_MAIL_ASSURANT As String = "(\s)*([a-zA-Z0-9ñÑ_])+(\.([a-zA-Z0-9ñÑ_])+)*@([a-zA-Z0-9_])+\.([a-zA-Z]{3})(\.([a-zA-Z]){2})?(\s)*"
    Public Const REGULAR_EXPRESION_MAIL_DEFAULT As String = "\w+([-+.'']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
    Public Const REGULAR_EXPRESION_ALL As String = ".*" 'Permite cualquier caracter
    Public Const COD_SIGLA_ASSU = "ASSU"

    Private Const SESSION_XMLFRMCERTIFICADO As String = "XMLFrmCertificado"
    Private Const SESSION_IDPRODUCTO As String = "IDPRODUCTO"
    Private Const SESSION_IS_CROSS_SELLING As String = "IsCrossSelling"
    Private Const SESSION_NROASEGURADOS As String = "NroAsegurados"
    Private Const SESSION_NROBENEFICIARIOS As String = "NroBeneficiarios"
    Private Const SESSION_DTBENEFICIARIO As String = "DTBeneficiario"
    Private Const SESSION_DTASEGURADO As String = "DTAsegurado"
    Private Const SESSION_CERTIFICADOCONTAINER As String = "ObjBECertificadoContainer"
    Private Const SESSION_ESTADOFORM As String = "EstForm"
    Private Const SESSION_MEDIOS_PAGO_CLIENTE As String = "MediosPagoCliente"


#Region "Inicio"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session(SESSION_BEUSUARIO) Is Nothing Then
            Response.Redirect("~/Login.aspx")
        End If

        If Not IsPostBack Then
            img_print.Attributes.Add("onclick", "Print()")
            img_contact.Attributes.Add("onclick", "OpenContact()")
            img_new_sale.Attributes.Add("onclick", "NewSale()")

            lblMensajeHeader.Text = ""
            If ValidarEstadoClaveUsuario() Then
                Response.Redirect("~/Seguridad/CambiarClave.aspx", False)
            End If
            Try
                IDPRODUCTO = Request.QueryString("IdProducto")
                If IDPRODUCTO = 0 Then
                    Response.Redirect("~/Default.aspx")
                End If
            Catch ex As Exception
                Response.Redirect("~/Default.aspx")
            End Try
            'IDPRODUCTO = 5327
            Dim oBLPro As New BLProducto
            Dim oBEPro As BEProducto = oBLPro.Seleccionar(IDPRODUCTO)
            hfIDProducto.Value = oBEPro.IdProducto
            lblProducto.Text = oBEPro.Nombre
            lblProducto_header.Text = oBEPro.Nombre

            'Variables de Configuración      
            '--------------------------
            If oBEPro.NroAsegurado > 0 Then
                btnAsegurado.Visible = True
                btnAsegurado.Enabled = False

                ''NUEVOS
                'btn_Asegurado.Visible = True
                'btn_Asegurado.Disabled = False
            Else
                btnAsegurado.Visible = False

                ''NUEVOS
                'btn_Asegurado.Visible = False
            End If
            If oBEPro.NroBeneficiario > 0 Then
                btnBeneficiario.Visible = True
                btnBeneficiario.Enabled = False

                ''NUEVOS
                'btn_Beneficiario.Visible = True
                'btn_Beneficiario.Disabled = False
            Else
                btnBeneficiario.Visible = False

                ''NUEVOS
                'btn_Beneficiario.Visible = False
            End If
            Session(SESSION_XMLFRMCERTIFICADO) = oBEPro.FrmCertificado
            Session(SESSION_IDPRODUCTO) = IDPRODUCTO
            Session(SESSION_NROASEGURADOS) = oBEPro.NroAsegurado
            Session(SESSION_NROBENEFICIARIOS) = oBEPro.NroBeneficiario
            CargarTipoDocBuscaCliente(IDPRODUCTO)


        End If

        IDPRODUCTO = CInt(hfIDProducto.Value)

        Dim doc As New XmlDocument()

        doc.LoadXml(Session(SESSION_XMLFRMCERTIFICADO).ToString())
        For Each nodo As XmlNode In doc.SelectNodes("Control")
            Dim t As Control = construirControl(nodo)
            phFormulario.Controls.Add(t)
        Next

        If Not IsPostBack Then

            ObtenerNroCertificado()
            DirectCast(phFormulario.FindControl("C_Venta"), TextBox).Text = Date.Now.ToShortDateString
            Dim oBEUsuario As BEUsuario = DirectCast(Session(SESSION_BEUSUARIO), BEUsuario)
            If oBEUsuario.IDOficina <> 0 Then
                DirectCast(phFormulario.FindControl("C_IdInformador"), HiddenField).Value = oBEUsuario.IDInformador
                DirectCast(phFormulario.FindControl("txtInformador"), TextBox).Text = oBEUsuario.IDInformador & " - " & oBEUsuario.Nombre
                DirectCast(phFormulario.FindControl("C_IdOficina"), HiddenField).Value = oBEUsuario.IDOficina
                DirectCast(phFormulario.FindControl("txtOficina"), TextBox).Text = oBEUsuario.CodOficina & " - " & oBEUsuario.Oficina
            Else
                DirectCast(phFormulario.FindControl("rfvOficina"), RequiredFieldValidator).InitialValue = "Seleccionar"
                DirectCast(phFormulario.FindControl("rfvInformador"), RequiredFieldValidator).InitialValue = "Seleccionar"
                DirectCast(phFormulario.FindControl("txtOficina"), TextBox).Text = "Seleccionar"
                DirectCast(phFormulario.FindControl("txtInformador"), TextBox).Text = "Seleccionar"

            End If
            'En Modo Venta
            EstadoControlesVenta(False)
            DirectCast(phFormulario.FindControl("IP_NroRaiz"), TextBox).Enabled = False
            DirectCast(phFormulario.FindControl("IA_NroCBU"), TextBox).Enabled = False
            DirectCast(phFormulario.FindControl("C_Venta"), TextBox).Enabled = False
            DirectCast(phFormulario.FindControl("C_NumCertificado"), TextBox).Enabled = False
            '------Información de Pago--------------------------------------------------------
            'DirectCast(phFormulario.FindControl("C_IdMedioPago"), DropDownList).Enabled = False
            DirectCast(phFormulario.FindControl("C_NumeroCuenta"), TextBox).Enabled = False
            '---------------------------------------------------------------------------------

            Dim C_Cccliente As TextBox = DirectCast(phFormulario.FindControl("C_Cccliente"), TextBox)
            C_Cccliente.Attributes.Add("onkeypress", "return Numero(event);")
            C_Cccliente.MaxLength = 8
            DirectCast(phFormulario.FindControl("C_IdTipoDocumento"), DropDownList).Attributes.Add("onChange", "ValidaNroDoc('" & C_Cccliente.ClientID & "', this.value);")
            'Línea temporal(Borrar este control de los xml)
            DirectCast(phFormulario.FindControl("rfvC_NumeroCuenta"), RequiredFieldValidator).Enabled = False

            txtNroDocCliente.MaxLength = 8
            txtNroDocCliente.Attributes.Add("onkeypress", "return Numero(event);")
            ddlTipoDocCliente.Attributes.Add("onChange", "ValidaNroDoc('" & txtNroDocCliente.ClientID & "', this.value);")

            Dim superficieComercio As TextBox = DirectCast(phFormulario.FindControl("SuperficieComercio"), TextBox)
            If Not superficieComercio Is Nothing Then
                superficieComercio.Attributes.Add("onkeypress", "return Numero(event);")
                superficieComercio.MaxLength = 8
            End If



            DirectCast(phFormulario.FindControl("C_Cccliente"), TextBox).Focus()

            'Limpiar Lista de Beneficiarios y/o asegurados en session
            Session(SESSION_DTBENEFICIARIO) = Nothing
            Session(SESSION_DTASEGURADO) = Nothing


            If DirectCast(phFormulario.FindControl("C_NumCertificado"), TextBox).Text = "0" Then
                '---Validad si existe pap_disponible
                pFrmVenta.Enabled = False
                btnNuevaVenta.Enabled = False
                btnCancelar.Enabled = False
                btnGrabar.Enabled = False
                btnImprimir.Enabled = False

                ''NUEVOS
                'btn_NuevaVenta.Disabled = False
                'btn_Cancelar.Disabled = False
                'btn_Grabar.Disabled = False
                'btn_Imprimir.Disabled = False

                ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M12", "alert('No existe un Número de certificado disponible.\nConsulte con el administrador');", True)
                '-----------
            End If

            Try
                Dim objBLContactoCliente As New BLSolicitudConctactoCliente
                Dim lstFrecuencias As ArrayList = objBLContactoCliente.ListarTipoFrecuenciaContacto()
                ddlFrecuenciaContacto.DataSource = lstFrecuencias
                ddlFrecuenciaContacto.DataValueField = "Codigo"
                ddlFrecuenciaContacto.DataTextField = "Nombre"
                ddlFrecuenciaContacto.Attributes.Add("onchange", "changeFrecuenciaContacto();")
                ddlFrecuenciaContacto.DataBind()
                ddlFrecuenciaContacto.SelectedValue = "NA"

            Catch ex As Exception

            End Try

            If Not DirectCast(phFormulario.FindControl("txtRamo"), TextBox) Is Nothing Then
                SeleccionRamo(True)
            End If
            Session(SESSION_IS_CROSS_SELLING) = 0
            If (Request.QueryString("IsCrossSelling") = "1") Then
                CargarCrossSelling()
                Session(SESSION_IS_CROSS_SELLING) = 1
            Else
                Session(SESSION_CERTIFICADOCONTAINER) = Nothing

                If Not Request.QueryString("tipoDoc") Is Nothing Then
                    DirectCast(phFormulario.FindControl("C_IdTipoDocumento"), DropDownList).SelectedValue = Request.QueryString("tipoDoc")
                End If
                If Not Request.QueryString("nroDoc") Is Nothing Then
                    DirectCast(phFormulario.FindControl("C_Cccliente"), TextBox).Text = Request.QueryString("nroDoc")
                End If
            End If
        End If
    End Sub

    Private Sub CargarTipoDocBuscaCliente(ByVal IDPRODUCTO As Int32)
        Dim tipoDocumento As ArrayList = ListaGenerica(IDPRODUCTO, "TipoDocumento")
        ddlTipoDocCliente.DataSource = tipoDocumento
        ddlTipoDocCliente.DataValueField = "ID"
        ddlTipoDocCliente.DataTextField = "Descripcion"
        ddlTipoDocCliente.DataBind()

        Try
            ddlTipoDocCliente.SelectedValue = "L"

            ddlTipoDocumentoContacto.DataSource = tipoDocumento
            ddlTipoDocumentoContacto.DataValueField = "ID"
            ddlTipoDocumentoContacto.DataTextField = "Descripcion"
            ddlTipoDocumentoContacto.DataBind()

        Catch ex As Exception

        End Try
    End Sub

    Public Sub ObtenerNroCertificado()
        Dim oBLCertificado As New BLCertificado
        Dim IDc As String = oBLCertificado.ObtenerNumCertificado(7092, Session(SESSION_IDPRODUCTO))
        'Dim IDc As String = oBLCertificado.ObtenerNumCertificado(0, Session("IDPRODUCTO"))
        DirectCast(phFormulario.FindControl("C_NumCertificado"), TextBox).Text = IDc
    End Sub

#End Region

#Region "Controles Dinámicos"

    Private Function construirControl(ByVal nodoControl As XmlNode) As Control
        Dim WebCtrl As Control = Nothing

        Select Case nodoControl.Attributes("Type").Value
            'Controles Estándar
            Case "Table"
                WebCtrl = GetTable(nodoControl)
                Exit Select
            Case "Label"
                WebCtrl = GetLabel(nodoControl)
                Exit Select
            Case "TextBox"
                WebCtrl = GetTextbox(nodoControl)
                Exit Select
            Case "DropDownList"
                WebCtrl = GetDropDownList(nodoControl)
                Exit Select
            Case "Button"
                WebCtrl = GetButton(nodoControl)
                Exit Select
            Case "ImageButton"
                WebCtrl = GetImageButton(nodoControl)
                Exit Select
            Case "CheckBox"
                WebCtrl = GetCheckBox(nodoControl)
                Exit Select
            Case "HiddenField"
                WebCtrl = GetHiddenField(nodoControl)
                Exit Select
                'Control Personalizado ccPrima
            Case "ccPrima"
                WebCtrl = GetccPrima(nodoControl)
                Exit Select

                'Controles Validación
            Case "RequiredFieldValidator"
                WebCtrl = GetRequiredFieldValidator(nodoControl)
                Exit Select

            Case "RangeValidator"
                WebCtrl = GetRangeValidator(nodoControl)
                Exit Select

            Case "CompareValidator"
                WebCtrl = GetCompareValidator(nodoControl)
                Exit Select

            Case "RegularExpressionValidator"
                WebCtrl = GetRegularExpressionValidator(nodoControl)
                Exit Select

                'Controles Exstensión AJAX
            Case "ListSearchExtender"
                WebCtrl = GetListSearchExtender(nodoControl)
                Exit Select
            Case "MaskedEditExtender"
                WebCtrl = GetMaskedEditExtender(nodoControl)
                Exit Select

        End Select

        Return WebCtrl
    End Function

    Private Function GetTable(ByVal nodoTable As XmlNode) As Table
        Dim Tabla As New Table()
        'Asignar Propiedades de la Tabla
        For Each propierty As XmlAttribute In nodoTable.Attributes
            Select Case propierty.Name
                Case "CssClass"
                    Tabla.CssClass = propierty.Value
                Case "Width"
                    Tabla.Width = Unit.Parse(propierty.Value)
            End Select
        Next
        For Each nodo As XmlNode In nodoTable.SelectNodes("TableRow")
            Dim Fila As TableRow = GetTableRow(nodo)
            Tabla.Rows.Add(Fila)
        Next
        Return Tabla
    End Function

    Private Function GetTableRow(ByVal nodoFila As XmlNode) As TableRow
        Dim Fila As New TableRow()
        'Asignar Propiedades de la Fila
        For Each propierty As XmlAttribute In nodoFila.Attributes
            Select Case propierty.Name
                Case "CssClass"
                    Fila.CssClass = propierty.Value
            End Select
        Next
        For Each nodo As XmlNode In nodoFila.SelectNodes("TableCell")
            Dim c As TableCell = GetTableCell(nodo)
            Fila.Cells.Add(c)
        Next
        Return Fila
    End Function

    Private Function GetTableCell(ByVal nodoCelda As XmlNode) As TableCell
        Dim Celda As New TableCell()
        'Asignar Propiedades de la celda
        For Each propierty As XmlAttribute In nodoCelda.Attributes
            Select Case propierty.Name
                Case "ColumnSpan"
                    Celda.ColumnSpan = Integer.Parse(propierty.Value)
                Case "RowSpan"
                    Celda.RowSpan = Integer.Parse(propierty.Value)
                Case "CssClass"
                    Celda.CssClass = propierty.Value
                Case "Width"
                    Celda.Width = Unit.Parse(propierty.Value)
            End Select
        Next
        Celda.Text = nodoCelda.InnerText
        For Each nodo As XmlNode In nodoCelda.SelectNodes("Control")
            Dim WebCtrl As Control = construirControl(nodo)
            Celda.Controls.Add(WebCtrl)
        Next
        Return Celda
    End Function

    Private Function GetDropDownList(ByVal nodo As XmlNode) As DropDownList
        Dim ddl As New DropDownList()
        Dim Selected As String = ""
        'Asignar Propiedades al control
        For Each propierty As XmlAttribute In nodo.Attributes
            Select Case propierty.Name
                Case "ID"
                    ddl.ID = propierty.Value
                Case "AutoPostBack"
                    ddl.AutoPostBack = Boolean.Parse(propierty.Value)
                    If (nodo.Attributes("ID").Value = "IA_Modelo") Then
                        AddHandler ddl.SelectedIndexChanged, AddressOf ddlModelo_SelectedIndexChanged
                    End If
                Case "Width"
                    ddl.Width = Unit.Parse(propierty.Value)
                Case "LoadData"
                    Dim DataTable As String = nodo.Attributes("DataTable").Value
                    ddl.DataSource = ListaGenerica(IDPRODUCTO, DataTable)
                    ddl.DataValueField = "ID"
                    ddl.DataTextField = "Descripcion"
                    ddl.DataBind()
                Case "onchange"
                    ddl.Attributes.Add("onchange", propierty.Value)
                Case "Selected"
                    Selected = propierty.Value
                Case "Visible"
                    ddl.Visible = Boolean.Parse(propierty.Value)
            End Select
        Next
        For Each Item As XmlNode In nodo.SelectNodes("ListItem")
            ddl.Items.Add(New ListItem(Item.InnerText, Item.Attributes("Value").Value))
        Next
        If Selected <> "" Then
            ddl.SelectedValue = Selected
        End If
        ' t.SelectedIndexChanged += New EventHandler(btn_Click)
        Return ddl
    End Function

    Private Function GetLabel(ByVal nodo As XmlNode) As Label
        Dim lbl As New Label()
        'Asignar Propiedades al control
        For Each propierty As XmlAttribute In nodo.Attributes
            Select Case propierty.Name
                Case "ID"
                    lbl.ID = propierty.Value
                Case "Text"
                    lbl.Text = propierty.Value
                Case "Width"
                    lbl.Width = Unit.Parse(propierty.Value)
            End Select
        Next
        Return lbl
    End Function

    Private Function GetTextbox(ByVal nodo As XmlNode) As TextBox
        Dim txt As New TextBox()
        'Asignar Propiedades al control
        txt.CssClass = "txtTexto"
        txt.Attributes.Add("onfocus", "this.className='txtTextoFocus';")
        txt.Attributes.Add("onblur", "this.className='txtTexto';")
        For Each propierty As XmlAttribute In nodo.Attributes
            Select Case propierty.Name
                Case "ID"
                    txt.ID = propierty.Value
                Case "Columns"
                    txt.Columns = Integer.Parse(propierty.Value)
                Case "Rows"
                    txt.Rows = Integer.Parse(propierty.Value)
                Case "MaxLength"
                    txt.Rows = Integer.Parse(propierty.Value)
                Case "Width"
                    txt.Width = Unit.Parse(propierty.Value)
                Case "ReadOnly"
                    txt.ReadOnly = Boolean.Parse(propierty.Value)
                Case "CssClass"
                    txt.CssClass = propierty.Value
                    txt.Attributes.Add("onfocus", "this.className='" & propierty.Value & "Focus';")
                    txt.Attributes.Add("onblur", "this.className='" & propierty.Value & "';")
                Case "Visible"
                    txt.Visible = Boolean.Parse(propierty.Value)
                Case "Enabled"
                    txt.Enabled = Boolean.Parse(propierty.Value)
                Case "TextChanged"
                    If (propierty.Value = "CalcularEdad") Then
                        txt.AutoPostBack = True
                        AddHandler txt.TextChanged, AddressOf txtFechaNac_TextChanged
                    End If
                Case "onchange"
                    txt.Attributes.Add("onchange", propierty.Value)
                Case "TextMode"
                    Select Case propierty.Value
                        Case "SingleLine"
                            txt.TextMode = TextBoxMode.SingleLine
                        Case "MultiLine"
                            txt.TextMode = TextBoxMode.MultiLine
                    End Select
            End Select
        Next
        txt.Text = nodo.InnerText

        Return txt
    End Function

    Private Function GetButton(ByVal nodo As XmlNode) As Button
        Dim btn As New Button()
        'Asignar Propiedades al control
        For Each propierty As XmlAttribute In nodo.Attributes
            Select Case propierty.Name
                Case "ID"
                    btn.ID = propierty.Value
                Case "Text"
                    btn.Text = propierty.Value
                Case "PostBackUrl"
                    btn.PostBackUrl = propierty.Value
                Case "Enabled"
                    btn.Enabled = Boolean.Parse(propierty.Value)
                Case "Width"
                    btn.Width = Unit.Parse(propierty.Value)
            End Select
        Next
        btn.CssClass = "btnStyle"
        btn.Attributes.Add("onmouseover", "this.className='btnStyleSobre';")
        btn.Attributes.Add("onmouseout", "this.className='btnStyle';")
        'b.Click += new EventHandler(btn_Click);
        'b.OnClientClick = nodo.Attributes["OnClientClick"].Value;
        'Eval(nodo.Attributes("OnClientClick").Value)
        Dim er As New EventArgs()
        'b.Click += new EventHandler(er);
        Return btn
    End Function

    Private Function GetImageButton(ByVal nodo As XmlNode) As ImageButton
        Dim ibtn As New ImageButton()
        'Asignar Propiedades al control
        For Each propierty As XmlAttribute In nodo.Attributes
            Select Case propierty.Name
                Case "ID"
                    ibtn.ID = propierty.Value
                Case "ImageUrl"
                    ibtn.ImageUrl = propierty.Value
                Case "ToolTip"
                    ibtn.ToolTip = propierty.Value
                Case "onclientclick"
                    ibtn.OnClientClick = propierty.Value
                Case "Enabled"
                    ibtn.Enabled = Boolean.Parse(propierty.Value)
                Case "Visible"
                    ibtn.Visible = Boolean.Parse(propierty.Value)
            End Select
        Next
        Return ibtn
    End Function

    Private Function GetHiddenField(ByVal nodo As XmlNode) As HiddenField
        Dim hf As New HiddenField()
        'Asignar Propiedades al control
        For Each propierty As XmlAttribute In nodo.Attributes
            Select Case propierty.Name
                Case "ID"
                    hf.ID = propierty.Value
                Case "Value"
                    hf.Value = propierty.Value
            End Select
        Next
        Return hf
    End Function

    Private Function GetCheckBox(ByVal nodo As XmlNode) As CheckBox
        Dim chk As New CheckBox()
        'Asignar Propiedades al control
        For Each propierty As XmlAttribute In nodo.Attributes
            Select Case propierty.Name
                Case "ID"
                    chk.ID = propierty.Value
                Case "Text"
                    chk.Text = propierty.Value
                Case "Checked"
                    chk.Checked = propierty.Value
                Case "onClick"
                    chk.Attributes.Add("onClick", propierty.Value)
                Case "CheckedChanged"
                    If (propierty.Value = "CopiarDireccionHogar") Then
                        chk.AutoPostBack = True
                        AddHandler chk.CheckedChanged, AddressOf chkCopiarDireccionHogar_CheckedChanged
                    End If
            End Select
        Next
        Return chk
    End Function
    'CONTROL PERSONALIZADO
    Private Function GetccPrima(ByVal nodo As XmlNode) As ccPrima
        'Control que permite seleccionar el plan, frecuencia, moneda y prima del producto
        Dim cc As ccPrima = Page.LoadControl("~/Proceso/Controles/ccPrima.ascx")
        'Asignar Propiedades al control
        For Each propierty As XmlAttribute In nodo.Attributes
            Select Case propierty.Name
                Case "ID"
                    cc.ID = propierty.Value
                Case "EnabledFrecuencia"
                    cc.EnabledFrecuencia = Boolean.Parse(propierty.Value)
                Case "EnabledMoneda"
                    cc.EnabledMoneda = Boolean.Parse(propierty.Value)
            End Select
        Next
        cc.IDProducto = IDPRODUCTO
        cc.IDRamo = IDRAMO

        'AddHandler cc.SelectedIndexChanged, AddressOf ccPrimaPlan_SelectedIndexChanged
        Return cc
    End Function

    Protected Sub ccPrimaPlan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        UpdatePanel1.Update()
    End Sub


    'VALIDATOR
    Private Function GetRequiredFieldValidator(ByVal nodo As XmlNode) As RequiredFieldValidator
        Dim rfv As New RequiredFieldValidator()
        'Asignar Propiedades al control
        For Each propierty As XmlAttribute In nodo.Attributes
            Select Case propierty.Name
                Case "ID"
                    rfv.ID = propierty.Value
                Case "ControlToValidate"
                    rfv.ControlToValidate = propierty.Value
                Case "ErrorMessage"
                    rfv.ErrorMessage = propierty.Value
                Case "SetFocusOnError"
                    rfv.SetFocusOnError = Boolean.Parse(propierty.Value)
                Case "InitialValue"
                    rfv.InitialValue = propierty.Value
                Case "Display"
                    Select Case propierty.Value
                        Case "Dynamic"
                            rfv.Display = ValidatorDisplay.Dynamic
                        Case "None"
                            rfv.Display = ValidatorDisplay.None
                        Case "Static"
                            rfv.Display = ValidatorDisplay.Static
                    End Select
                Case "Enabled"
                    rfv.Enabled = Boolean.Parse(propierty.Value)
            End Select
        Next
        rfv.Text = nodo.InnerText
        Return rfv
    End Function

    Private Function GetRangeValidator(ByVal nodo As XmlNode) As RangeValidator
        Dim rv As New RangeValidator()
        'Asignar Propiedades al control
        For Each propierty As XmlAttribute In nodo.Attributes
            Select Case propierty.Name
                Case "ID"
                    rv.ID = propierty.Value
                Case "Display"
                    Select Case propierty.Value
                        Case "Dynamic"
                            rv.Display = ValidatorDisplay.Dynamic
                        Case "None"
                            rv.Display = ValidatorDisplay.None
                        Case "Static"
                            rv.Display = ValidatorDisplay.Static
                    End Select
                Case "DataType"
                    Select Case propierty.Value
                        Case "Currency"
                            rv.Type = ValidationDataType.Currency
                        Case "Date"
                            rv.Type = ValidationDataType.Date
                        Case "Double"
                            rv.Type = ValidationDataType.Double
                        Case "Integer"
                            rv.Type = ValidationDataType.Integer
                        Case "String"
                            rv.Type = ValidationDataType.String
                    End Select
                Case "ControlToValidate"
                    rv.ControlToValidate = propierty.Value
                Case "ErrorMessage"
                    rv.ErrorMessage = propierty.Value
                Case "MaximumValue"
                    If propierty.Value = "GetDate" Then
                        rv.MaximumValue = Now.Date.ToShortDateString
                    Else
                        rv.MaximumValue = propierty.Value
                    End If
                Case "MinimumValue"
                    rv.MinimumValue = propierty.Value
                Case "SetFocusOnError"
                    rv.SetFocusOnError = Boolean.Parse(propierty.Value)
                Case "Enabled"
                    rv.Enabled = Boolean.Parse(propierty.Value)
            End Select
        Next
        rv.Text = nodo.InnerText

        Return rv
    End Function

    Private Function GetCompareValidator(ByVal nodo As XmlNode) As CompareValidator
        Dim cv As New CompareValidator()
        'Asignar Propiedades al control
        For Each propierty As XmlAttribute In nodo.Attributes
            Select Case propierty.Name
                Case "ID"
                    cv.ID = propierty.Value
                Case "Display"
                    Select Case propierty.Value
                        Case "Dynamic"
                            cv.Display = ValidatorDisplay.Dynamic
                        Case "None"
                            cv.Display = ValidatorDisplay.None
                        Case "Static"
                            cv.Display = ValidatorDisplay.Static
                    End Select
                Case "ControlToValidate"
                    cv.ControlToValidate = propierty.Value
                Case "ControlToCompare"
                    cv.ControlToCompare = propierty.Value
                Case "ErrorMessage"
                    cv.ErrorMessage = propierty.Value
                Case "SetFocusOnError"
                    cv.SetFocusOnError = Boolean.Parse(propierty.Value)
                Case "DataType"
                    Select Case propierty.Value
                        Case "Currency"
                            cv.Type = ValidationDataType.Currency
                        Case "Date"
                            cv.Type = ValidationDataType.Date
                        Case "Double"
                            cv.Type = ValidationDataType.Double
                        Case "Integer"
                            cv.Type = ValidationDataType.Integer
                        Case "String"
                            cv.Type = ValidationDataType.String
                    End Select
                Case "Operator"
                    Select Case propierty.Value
                        Case "DataTypeCheck"
                            cv.Operator = ValidationCompareOperator.DataTypeCheck
                    End Select
                Case "Enabled"
                    cv.Enabled = Boolean.Parse(propierty.Value)
            End Select
        Next
        cv.Text = nodo.InnerText
        Return cv
    End Function

    Private Function GetRegularExpressionValidator(ByVal nodo As XmlNode) As RegularExpressionValidator
        Dim rev As New RegularExpressionValidator()
        'Asignar Propiedades al control
        For Each propierty As XmlAttribute In nodo.Attributes
            Select Case propierty.Name
                Case "ID"
                    rev.ID = propierty.Value
                Case "Display"
                    Select Case propierty.Value
                        Case "Dynamic"
                            rev.Display = ValidatorDisplay.Dynamic
                        Case "None"
                            rev.Display = ValidatorDisplay.None
                        Case "Static"
                            rev.Display = ValidatorDisplay.Static
                    End Select
                Case "ControlToValidate"
                    rev.ControlToValidate = propierty.Value
                Case "ErrorMessage"
                    rev.ErrorMessage = propierty.Value
                Case "ValidationExpression"
                    rev.ValidationExpression = GetRegularExpresionPersonalizate(nodo, propierty.Value)
                    'rev.ValidationExpression = propierty.Value
                Case "SetFocusOnError"
                    rev.SetFocusOnError = Boolean.Parse(propierty.Value)
                Case "Enabled"
                    rev.Enabled = Boolean.Parse(propierty.Value)
            End Select
        Next
        rev.Text = nodo.InnerText
        Return rev
    End Function

    Private Function GetRegularExpresionPersonalizate(ByVal nodos As XmlNode, ByVal propertyValue As String) As String
        Dim sRegularExpression As String = REGULAR_EXPRESION_MAIL_DEFAULT
        Try
            Dim propierty As XmlAttribute
            propierty = nodos.Attributes("ValidationGroup")

            Select Case propierty.Value
                Case COD_SIGLA_ASSU
                    hfCodAsegurador.Value = COD_SIGLA_ASSU
                    sRegularExpression = REGULAR_EXPRESION_MAIL_ASSURANT
                Case Else
                    sRegularExpression = propertyValue
            End Select
        Catch ex As Exception
            'No tiene asignado un ValidationGroup con lo cual tomo el default
        End Try
        Return sRegularExpression
    End Function


    'AJAX CONTROL
    Private Function GetListSearchExtender(ByVal nodo As XmlNode) As ListSearchExtender
        Dim lse As New ListSearchExtender()
        'Asignar Propiedades al control
        For Each propierty As XmlAttribute In nodo.Attributes
            Select Case propierty.Name
                Case "ID"
                    lse.ID = propierty.Value
                Case "TargetControlID"
                    lse.TargetControlID = propierty.Value
                Case "PromptCssClass"
                    lse.PromptCssClass = propierty.Value
                Case "PromptText"
                    lse.PromptText = propierty.Value
                Case "IsSorted"
                    lse.IsSorted = Boolean.Parse(propierty.Value)
                Case "Enabled"
                    lse.Enabled = Boolean.Parse(propierty.Value)
            End Select
        Next
        Return lse
    End Function

    Private Function GetMaskedEditExtender(ByVal nodo As XmlNode) As MaskedEditExtender
        Dim mee As New MaskedEditExtender()
        'Asignar Propiedades al control
        For Each propierty As XmlAttribute In nodo.Attributes
            Select Case propierty.Name
                Case "ID"
                    mee.ID = propierty.Value
                Case "TargetControlID"
                    mee.TargetControlID = propierty.Value
                Case "Mask"
                    mee.Mask = propierty.Value
                Case "MaskType"
                    Select Case propierty.Value
                        Case "Date"
                            mee.MaskType = AjaxControlToolkit.MaskedEditType.Date
                        Case "DateTime"
                            mee.MaskType = AjaxControlToolkit.MaskedEditType.DateTime
                        Case "None"
                            mee.MaskType = AjaxControlToolkit.MaskedEditType.None
                        Case "Number"
                            mee.MaskType = AjaxControlToolkit.MaskedEditType.Number
                        Case "Time"
                            mee.MaskType = AjaxControlToolkit.MaskedEditType.Time
                    End Select
                Case "AutoComplete"
                    mee.AutoComplete = Boolean.Parse(propierty.Value)
                    'Case "InputDirection"
                    '    Select Case propierty.Value
                    '        Case "LeftToRight"
                    '            mee.InputDirection = MaskedEditInputDirection.LeftToRight
                    '        Case "RightToLeft"
                    '            mee.InputDirection = MaskedEditInputDirection.RightToLeft
                    '    End Select
                Case "ClearTextOnInvalid"
                    mee.ClearTextOnInvalid = Boolean.Parse(propierty.Value)
                Case "ClearMaskOnLostFocus"
                    mee.ClearMaskOnLostFocus = Boolean.Parse(propierty.Value)
                Case "PromptCharacter"
                    mee.PromptCharacter = propierty.Value
                Case "Enabled"
                    mee.Enabled = Boolean.Parse(propierty.Value)
            End Select
        Next
        'mee.AutoCompleteValue = " "
        Return mee
    End Function


#End Region

#Region "Funciones"
    'Funciones Genéricas
    Private Function ListaGenerica(ByVal pIDProducto As Integer, ByVal pTabla As String) As ArrayList
        Dim oBL As New BLListaGenerica
        Dim r As New ArrayList
        Return oBL.Seleccionar(pIDProducto, pTabla)
    End Function

    Private Sub SetValueControl(ByVal pIDControl As String, ByVal pIDTipoControl As String, pTipoCampo As String, pValorNum As Decimal, pValorDate As Date, pValorString As String)
        Dim valor As String = NullTypes.CadenaNull

        Select Case pTipoCampo
            Case "N"
                valor = pValorNum
                If (valor = NullTypes.DecimalNull) Then
                    Exit Sub
                End If
                Exit Select
            Case "D"
                valor = pValorDate
                Exit Select
            Case "S"
                valor = pValorString
                Exit Select
        End Select


        Select Case pIDTipoControl
            Case "Label"
                DirectCast(phFormulario.FindControl(pIDControl), Label).Text = valor
                Exit Select
            Case "TextBox"
                DirectCast(phFormulario.FindControl(pIDControl), TextBox).Text = valor
                Exit Select
            Case "DropDownList"
                DirectCast(phFormulario.FindControl(pIDControl), DropDownList).SelectedValue = valor
                Exit Select
            Case "CheckBox"
                Dim checkValue As Boolean = False
                If (Not (String.IsNullOrEmpty(valor)) AndAlso IsNumeric(valor) AndAlso CInt(valor) > 0) Then
                    checkValue = True
                End If
                DirectCast(phFormulario.FindControl(pIDControl), CheckBox).Checked = checkValue
                Exit Select
            Case "HiddenField"
                DirectCast(phFormulario.FindControl(pIDControl), HiddenField).Value = valor
                Exit Select
        End Select
    End Sub

    Private Function GetValueControl(ByVal pIDControl As String, ByVal pIDTipoControl As String) As String
        Dim valor As String = ""
        Select Case pIDTipoControl
            Case "Label"
                valor = DirectCast(phFormulario.FindControl(pIDControl), Label).Text.Trim
                Exit Select
            Case "TextBox"
                valor = DirectCast(phFormulario.FindControl(pIDControl), TextBox).Text.Trim
                Exit Select
            Case "DropDownList"
                valor = DirectCast(phFormulario.FindControl(pIDControl), DropDownList).SelectedValue
                Exit Select
            Case "CheckBox"
                valor = 0
                If DirectCast(phFormulario.FindControl(pIDControl), CheckBox).Checked Then
                    valor = 1
                End If
                Exit Select
            Case "HiddenField"
                valor = DirectCast(phFormulario.FindControl(pIDControl), HiddenField).Value
                Exit Select
        End Select
        Return valor
    End Function

    Public Function GetInfoControl(ByVal pIdInfoTabla As String, ByVal XMLFrm As String) As List(Of BEInfoControl)
        'Retorna una lista de los Control Info
        Dim LInfoControl As New List(Of BEInfoControl)
        Try
            Using reader As XmlReader = XmlReader.Create(New StringReader(XMLFrm))
                While reader.Read()
                    If reader.NodeType = XmlNodeType.Element Then
                        If reader.Name = "Control" Then
                            If reader.HasAttributes Then
                                If reader.GetAttribute(pIdInfoTabla) <> "" Then
                                    Dim oBE As New BEInfoControl
                                    oBE.IDInfoD = reader.GetAttribute(pIdInfoTabla).ToString().Trim
                                    'oBE.IDInfo = CInt(reader.GetAttribute(pIdInfoTabla))
                                    oBE.IDControl = reader.GetAttribute("ID")
                                    oBE.TipoControl = reader.GetAttribute("Type")
                                    LInfoControl.Add(oBE)
                                End If
                                reader.MoveToElement()
                            End If
                        End If
                    End If
                End While
            End Using
        Catch ex As Exception
            lblResult.Text = "An Exception occurred: " + ex.Message
        End Try
        Return LInfoControl
    End Function
    '///---------

    Private Sub ClearInputs(ByVal ctrls As ControlCollection)
        For Each ctrl As Control In ctrls
            If TypeOf ctrl Is TextBox Then
                DirectCast(ctrl, TextBox).Text = String.Empty
            ElseIf TypeOf ctrl Is DropDownList Then
                DirectCast(ctrl, DropDownList).ClearSelection()
            End If
            ClearInputs(ctrl.Controls)
        Next
    End Sub

    Private Sub EstadoControlesVenta(ByVal pEstado As Boolean)
        Try
            '----Información del Titular-----------------------------------------------------
            DirectCast(phFormulario.FindControl("C_Apellido1"), TextBox).Enabled = pEstado 'x
            DirectCast(phFormulario.FindControl("C_Apellido2"), TextBox).Enabled = pEstado
            DirectCast(phFormulario.FindControl("C_Nombre1"), TextBox).Enabled = pEstado 'x
            DirectCast(phFormulario.FindControl("C_Nombre2"), TextBox).Enabled = pEstado
            DirectCast(phFormulario.FindControl("C_Direccion"), TextBox).Enabled = pEstado
            DirectCast(phFormulario.FindControl("IA_DireccionNro"), TextBox).Enabled = pEstado
            DirectCast(phFormulario.FindControl("IA_DireccionPiso"), TextBox).Enabled = pEstado
            DirectCast(phFormulario.FindControl("IA_DireccionDpto"), TextBox).Enabled = pEstado
            DirectCast(phFormulario.FindControl("IA_DireccionCPostal"), TextBox).Enabled = pEstado
            DirectCast(phFormulario.FindControl("IA_Email"), TextBox).Enabled = pEstado
            DirectCast(phFormulario.FindControl("C_CATelefono"), TextBox).Enabled = pEstado
            DirectCast(phFormulario.FindControl("C_Telefono"), TextBox).Enabled = pEstado
            DirectCast(phFormulario.FindControl("IA_CAOtroNroTelefono"), TextBox).Enabled = pEstado
            DirectCast(phFormulario.FindControl("IA_OtroNroTelefono"), TextBox).Enabled = pEstado
            DirectCast(phFormulario.FindControl("IA_NroCelular"), TextBox).Enabled = pEstado
            '--------------------------------------------------------------------------------

            '------Información Adicional del Titular-----------------------------------------
            DirectCast(phFormulario.FindControl("IA_EstadoCivil"), DropDownList).Enabled = pEstado
            DirectCast(phFormulario.FindControl("IA_Sexo"), DropDownList).Enabled = pEstado 'x
            DirectCast(phFormulario.FindControl("A_FechaNacimiento"), TextBox).Enabled = pEstado 'x
            'Nº CUIL / CUIT / CDI
            DirectCast(phFormulario.FindControl("IP_NroDocCDI"), TextBox).Enabled = pEstado
            DirectCast(phFormulario.FindControl("IA_LugarNac"), TextBox).Enabled = pEstado 'x
            DirectCast(phFormulario.FindControl("IA_Nacionalidad"), TextBox).Enabled = pEstado 'x
            DirectCast(phFormulario.FindControl("IA_Profesion"), TextBox).Enabled = pEstado
            '---------------------------------------------------------------------------------

            ''------Información de Pago--------------------------------------------------------
            DirectCast(phFormulario.FindControl("C_IdMedioPago"), DropDownList).Enabled = pEstado
            'DirectCast(phFormulario.FindControl("C_NumeroCuenta"), TextBox).Enabled = pEstado
            ''---------------------------------------------------------------------------------

            DirectCast(phFormulario.FindControl("ibtnBUbigeoITitular"), ImageButton).Visible = pEstado
            'Try
            '    'Requerido Domicilio Hogar
            '    DirectCast(phFormulario.FindControl("rfvLocalidad1"), RequiredFieldValidator).Enabled = Not pEstado
            'Catch ex As Exception

            'End Try

        Catch ex As Exception

        End Try
    End Sub

#End Region

#Region "Frm Principal Certificado"

    Protected Sub btnGrabar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGrabar.Click

        'VALIDACIONES--------
        'Verifica Beneficiario------------------
        Dim objDTBeneficiario As New DataTable()
        objDTBeneficiario = Session(SESSION_DTBENEFICIARIO)

        If (DirectCast(phFormulario.FindControl("IA_EnviarMail"), CheckBox).Checked And String.IsNullOrEmpty(DirectCast(phFormulario.FindControl("IA_Email"), TextBox).Text) And hfCodAsegurador.Value = COD_SIGLA_ASSU) Or
            (DirectCast(phFormulario.FindControl("IA_EnviarMail"), CheckBox).Checked And String.IsNullOrEmpty(DirectCast(phFormulario.FindControl("IA_Email"), TextBox).Text) And ValidaProductoMail(IDPRODUCTO)) Then
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('Debe ingresar el mail para poder realizar el envío de la Poliza');", True)
            Exit Sub
        End If

        If Not (objDTBeneficiario Is Nothing) Then
            If objDTBeneficiario.Rows.Count <> 0 Then
                If CalcularPorcentaje(objDTBeneficiario) < 100 Then
                    ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('El porcentaje de Beneficiarios debe ser igual al 100%.');", True)
                    Exit Sub
                End If
            End If
        End If

        Dim condicionIVA As DropDownList = DirectCast(phFormulario.FindControl("ddIA_CondicionIva"), DropDownList)
        If Not condicionIVA Is Nothing AndAlso condicionIVA.SelectedValue = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('Ingrese la condicion de IVA del Asegurado.');", True)
            Exit Sub
        End If

        Dim pIMEI As String = String.Empty
        If Not DirectCast(phFormulario.FindControl("IA_ValorDeducible"), TextBox) Is Nothing Then
            pIMEI = DirectCast(phFormulario.FindControl("IA_NroIMEI"), TextBox).Text
            Dim mIMEI As String = ValidaIMEI(DirectCast(phFormulario.FindControl("IA_NroIMEI"), TextBox).Text)
            If Not String.IsNullOrEmpty(mIMEI) Then
                ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('" + mIMEI + "');", True)
                Exit Sub
            End If
        End If

        'Validar Edad---------------------
        Dim ccPrima As ccPrima = DirectCast(phFormulario.FindControl("ccPrima"), ccPrima)
        Dim txtFechaNac As TextBox = DirectCast(phFormulario.FindControl("A_FechaNacimiento"), TextBox)
        Dim edad As Int32 = Math.Truncate(CalcularEdad(CDate(txtFechaNac.Text)))

        'Validar Cliente Domicilio Asegurado (Hogar-5264)
        If CInt(hfIDProducto.Value) = 5264 Then
            Dim oBECer As New BECertificado
            Dim oBLCer As New BLCertificado
            oBECer.IdTipoDocumento = DirectCast(phFormulario.FindControl("C_IdTipoDocumento"), DropDownList).SelectedValue
            oBECer.Cccliente = DirectCast(phFormulario.FindControl("C_Cccliente"), TextBox).Text.Trim()
            oBECer.Direccion = DirectCast(phFormulario.FindControl("C_Direccion1"), TextBox).Text.Trim
            oBECer.DirNumero = DirectCast(phFormulario.FindControl("IA_DireccionNro1"), TextBox).Text.Trim
            oBECer.DirPiso = DirectCast(phFormulario.FindControl("IA_DireccionPiso1"), TextBox).Text.Trim
            oBECer.DirDpto = DirectCast(phFormulario.FindControl("IA_DireccionDpto1"), TextBox).Text.Trim
            oBECer.DirCodPostal = DirectCast(phFormulario.FindControl("IA_DireccionCPostal1"), TextBox).Text.Trim
            oBECer.IdCiudad = CInt(DirectCast(phFormulario.FindControl("C_IdCiudad1"), HiddenField).Value)
            Dim pMensaje As String = oBLCer.ValidarClienteDomicilio(oBECer)
            If pMensaje <> "" Then
                ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('" & pMensaje & ".');", True)
                Exit Sub
            End If
        End If


        Dim oBLOpcionPrima As New BLOpcionPrima
        Dim pIDProducto As Int32 = CInt(hfIDProducto.Value)
        Dim pOpcion As String = ccPrima.Opcion
        Dim pIdFrecuencia As Int32 = ccPrima.IdFrecuencia
        Dim pIdMonedad As String = ccPrima.IdMonedaPrima
        Dim lstBEOpcionPrima As IList = oBLOpcionPrima.Seleccionar(IDPRODUCTO, pOpcion, pIdFrecuencia, pCondicionPlan, edad)
        For Each objBEOpcionPrima As BEOpcionPrima In lstBEOpcionPrima
            If (objBEOpcionPrima.Opcion = pOpcion And objBEOpcionPrima.IdFrecuencia = pIdFrecuencia And objBEOpcionPrima.IdMonedaPrima = pIdMonedad) Then
                If (edad < objBEOpcionPrima.EdadMin) And objBEOpcionPrima.EdadMin > 0 Then ' valida solo si existe como configurada
                    ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('El titular o contratante no puede ser menor de " & objBEOpcionPrima.EdadMin.ToString() & " años.');", True)
                    Exit Sub
                End If
                If (edad > objBEOpcionPrima.Edadmax) And objBEOpcionPrima.Edadmax > 0 Then ' valida solo si existe como configurada
                    ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('El titular o contratante no puede ser mayor de " & objBEOpcionPrima.Edadmax.ToString() & " años.');", True)
                    Exit Sub
                End If
            End If
        Next
        '------


        Dim objBECertificado As New BECertificado
        Dim lstBEAsegurado As New List(Of BEAsegurado)()
        Dim lstBEBeneficiario As New List(Of BEBeneficiario)()
        Dim lstBEInfoProductoC As New List(Of BEInfoProductoC)()
        Dim lstBEInfoAseguradoC As New List(Of BEInfoAseguradoC)()
        Dim lstBEInfoProducto As New List(Of BEInfoProductoC)()
        Dim lstBEInfoAsegurado As New List(Of BEAsegurado)()
        Dim NumFisico As Decimal
        'Carga Config. del Producto
        Dim xmlFrmCer As String = Session(SESSION_XMLFRMCERTIFICADO).ToString()
        '------------
        CargarCertificado(objBECertificado)
        CargarAsegurado(lstBEAsegurado, objBECertificado)
        CargarBeneficiario(lstBEBeneficiario, objBECertificado, objDTBeneficiario)
        CargarInfoProductoC(lstBEInfoProductoC, objBECertificado, xmlFrmCer)
        CargarInfoAseguradoC(lstBEInfoAseguradoC, objBECertificado, xmlFrmCer)
        '------------
        Dim objBLExpedir As New BLExpedir()
        Dim lstBEExpedir As List(Of BEExpedir) = objBLExpedir.BLExpedirCertificado(objBECertificado, lstBEAsegurado, lstBEBeneficiario, lstBEInfoProductoC, lstBEInfoAseguradoC, NumFisico)

        Dim cadena As String = ""
        pMensaje.Visible = True
        If lstBEExpedir(0).Codigo = 0 Then
            lblMensaje.Text = "El certificado fue expedido satisfactoriamente."
            pMensaje.CssClass = "exito"
            lblMensajeHeader.Text = "-" + lblMensaje.Text
            hfEstForm.Value = "1"

            pFrmVenta.Enabled = False
            btnNuevaVenta.Enabled = True
            btnCancelar.Enabled = False
            btnGrabar.Enabled = False
            btnImprimir.Enabled = True

            ''NUEVOS
            'btn_NuevaVenta.Disabled = True
            'btn_Cancelar.Disabled = False
            'btn_Grabar.Disabled = False
            'btn_Imprimir.Disabled = True

            Dim objCerticadoContainer As New BECertificadoContainer
            objCerticadoContainer.DescripcionCiudad = DirectCast(phFormulario.FindControl("txtCiudad"), TextBox).Text
            objCerticadoContainer.DescripcionInformador = DirectCast(phFormulario.FindControl("txtInformador"), TextBox).Text
            objCerticadoContainer.DescripcionOficina = DirectCast(phFormulario.FindControl("txtOficina"), TextBox).Text
            Dim txtDireccionRiesgo As TextBox = DirectCast(phFormulario.FindControl("txtCiudad1"), TextBox)
            If (Not txtDireccionRiesgo Is Nothing) Then
                objCerticadoContainer.DescripcionCiudadRiesgo = txtDireccionRiesgo.Text
            End If

            objCerticadoContainer.ObjBECertificado = objBECertificado
            objCerticadoContainer.LstBEAsegurado = lstBEAsegurado
            objCerticadoContainer.LstBEInfoProductoC = lstBEInfoProductoC
            objCerticadoContainer.LstBEInfoAseguradoC = lstBEInfoAseguradoC
            Session(SESSION_CERTIFICADOCONTAINER) = objCerticadoContainer

            ' Dim ccCrossSelling As ccCrossSelling = DirectCast(phFormulario.FindControl("ccCrossSelling"), ccCrossSelling)
            Dim oBEUsuario As BEUsuario = DirectCast(Session(SESSION_BEUSUARIO), BEUsuario)
            Dim objBECrossSellingSearch As New BECrossSelling
            Dim objMedioPagoCliente As New BEMedioPagoCliente

            With objBECrossSellingSearch

                If (Not Session(SESSION_MEDIOS_PAGO_CLIENTE) Is Nothing) Then
                    Dim aListaMediosPago As ArrayList = DirectCast(Session(SESSION_MEDIOS_PAGO_CLIENTE), ArrayList)
                    For Each beCuentaTarjeta As BECuentaTarjetaWS In aListaMediosPago
                        objMedioPagoCliente = New BEMedioPagoCliente
                        objMedioPagoCliente.IdMedioPago = beCuentaTarjeta.IDMedioPago
                        objMedioPagoCliente.IdMoneda = beCuentaTarjeta.IDmoneda
                        .SIdsMediosDePagoCliente.Add(objMedioPagoCliente)
                    Next
                End If
                .SIDUsuario = oBEUsuario.IDUsuario
                .SIDProducto = objBECertificado.IdProducto
                .SIDTipoDocumento = objBECertificado.IdTipoDocumento
                .SNumeroDocumento = objBECertificado.Cccliente
                .SIDMedioPagoUtilizado = objBECertificado.IdMedioPago
                .SNumeroDeCuentaUtilizado = objBECertificado.NumeroCuenta
                .SIDMonedaCobroUtilizado = objBECertificado.IdMonedaCobro
            End With
            Try

                ccCrossSelling.DrawCrossSellingPanel(objBECrossSellingSearch)
                img_contact.Visible = True
                ccCrossSelling.Visible = True
                ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "DC", "displayCrossSellingPanel();", True)

            Catch ex As Exception
                Session(SESSION_CERTIFICADOCONTAINER) = Nothing
                img_contact.Visible = False
                ccCrossSelling.Visible = False
            End Try
        Else
            For Each objBEExpedir As BEExpedir In lstBEExpedir
                cadena = cadena & objBEExpedir.Descripcion & Environment.NewLine
            Next
            pMensaje.CssClass = "error"
            lblMensaje.Text = cadena
            lblMensajeHeader.Text = "-" + lblMensaje.Text
        End If
    End Sub

    Protected Function GetEdadAseguradoFromSession() As Double
        Dim edadAsegurado As Decimal = 0
        Try
            edadAsegurado = DirectCast(Session("EdadAsegurado"), Double)
        Catch ex As Exception
            edadAsegurado = Decimal.Parse(0)
        End Try
        Return edadAsegurado
    End Function

    Protected Sub btnImprimir_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnImprimir.Click
        Dim IDNumeroCer As Int32 = CInt(DirectCast(phFormulario.FindControl("C_NumCertificado"), TextBox).Text)
        Dim IDProducto As Int32 = CInt(hfIDProducto.Value)
        Dim IDCertificado As String = IDProducto & "-" & IDNumeroCer

        ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M2", "Imprimir('" & IDCertificado & "','C');", True)
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancelar.Click
        'pFrmVenta.Enabled = False
        LiberarNumCertificado()
        Session(SESSION_DTBENEFICIARIO) = Nothing
        Session(SESSION_DTASEGURADO) = Nothing
	Session("EdadAsegurado") = Decimal.Parse(0)
        Response.Redirect("EmisionCertificado.aspx?IdProducto=" & hfIDProducto.Value)
    End Sub

    Private Sub LiberarNumCertificado()
        Dim oBLCertificado As New BLCertificado
        Dim NumCertificado As Decimal = DirectCast(phFormulario.FindControl("C_NumCertificado"), TextBox).Text
        oBLCertificado.LiberarNumCertificado(7092, CInt(hfIDProducto.Value), NumCertificado)
    End Sub

    Protected Sub btnNuevaVenta_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNuevaVenta.Click
        Session(SESSION_DTBENEFICIARIO) = Nothing
        Session(SESSION_DTASEGURADO) = Nothing
        Session(SESSION_CERTIFICADOCONTAINER) = Nothing
        Response.Redirect("EmisionCertificado.aspx?IdProducto=" & hfIDProducto.Value)
    End Sub

    Private Sub CargarCertificado(ByRef objBECertificado As BECertificado)
        Dim pIDCertificado As Int32 = CInt(DirectCast(phFormulario.FindControl("C_NumCertificado"), TextBox).Text)
        Dim pIDProducto As Int32 = CInt(hfIDProducto.Value)
        Dim pIdInformador As String = DirectCast(phFormulario.FindControl("C_IdInformador"), HiddenField).Value
        Dim pIdOficina As Int32 = CInt(DirectCast(phFormulario.FindControl("C_IdOficina"), HiddenField).Value)
        Dim pVenta As Date = CDate(DirectCast(phFormulario.FindControl("C_Venta"), TextBox).Text)
        Dim pIdTipoDocumento As String = DirectCast(phFormulario.FindControl("C_IdTipoDocumento"), DropDownList).SelectedValue
        Dim pCccliente As String = DirectCast(phFormulario.FindControl("C_Cccliente"), TextBox).Text.Trim()
        Dim pApellido1 As String = DirectCast(phFormulario.FindControl("C_Apellido1"), TextBox).Text.Trim()
        Dim pApellido2 As String = DirectCast(phFormulario.FindControl("C_Apellido2"), TextBox).Text.Trim()
        Dim pNombre1 As String = DirectCast(phFormulario.FindControl("C_Nombre1"), TextBox).Text.Trim()
        Dim pNombre2 As String = DirectCast(phFormulario.FindControl("C_Nombre2"), TextBox).Text.Trim()
        Dim pDireccion As String = DirectCast(phFormulario.FindControl("C_Direccion"), TextBox).Text.Trim()
        Dim pIdCiudad As Int32 = CInt(DirectCast(phFormulario.FindControl("C_IdCiudad"), HiddenField).Value)
        Dim pTelefono As String = DirectCast(phFormulario.FindControl("C_Telefono"), TextBox).Text.Trim()

        Dim ccPrima As ccPrima = DirectCast(phFormulario.FindControl("ccPrima"), ccPrima)

        Dim pIdMonedaCobro As String = DirectCast(phFormulario.FindControl("C_IdMonedaCobro"), DropDownList).SelectedValue

        Dim pIdMedioPago As String = ""
        Dim pNumeroCuenta As String = ""
        Dim pVencimiento As Date = Date.MinValue
        'If ValidaMedioPago(Convert.ToInt32(hfIDProducto.Value)) Then
        pIdMedioPago = DirectCast(phFormulario.FindControl("C_IdMedioPago"), DropDownList).SelectedValue
        pNumeroCuenta = DirectCast(phFormulario.FindControl("C_NumeroCuenta"), TextBox).Text.Trim()
        pVencimiento = CDate(DirectCast(phFormulario.FindControl("C_Vencimiento"), TextBox).Text)
        'End If

        objBECertificado.IdCertificado = pIDProducto & "-" & pIDCertificado
        objBECertificado.IdProducto = pIDProducto
        objBECertificado.NumCertificado = pIDCertificado
        objBECertificado.IdInformador = pIdInformador
        objBECertificado.IdOficina = pIdOficina
        objBECertificado.Venta = DateTime.Now
        'pVenta

        Dim oBLInfoParametro = New BLInfoParametro()
        Dim mesVigencia As Integer = oBLInfoParametro.GetMesVegencia(IDPRODUCTO, FinVigenciaAbierta)
        objBECertificado.Vigencia = Convert.ToDateTime(objBECertificado.Venta).AddDays(1)
        objBECertificado.FinVigencia = Convert.ToDateTime(objBECertificado.Venta).AddMonths(mesVigencia)
        objBECertificado.Digitacion = DateTime.Now

        objBECertificado.IdTipoDocumento = pIdTipoDocumento
        objBECertificado.Cccliente = pCccliente
        objBECertificado.Apellido1 = pApellido1
        objBECertificado.Apellido2 = pApellido2
        objBECertificado.Nombre1 = pNombre1
        objBECertificado.Nombre2 = pNombre2

        objBECertificado.Direccion = pDireccion
        objBECertificado.IdCiudad = pIdCiudad
        objBECertificado.Telefono = pTelefono

        objBECertificado.IdFrecuencia = ccPrima.IdFrecuencia
        objBECertificado.Opcion = ccPrima.Opcion
        objBECertificado.IdMonedaPrima = ccPrima.IdMonedaPrima
        objBECertificado.IdMonedaCobro = pIdMonedaCobro
        objBECertificado.PrimaBruta = ccPrima.Prima
        objBECertificado.PrimaCobrar = ccPrima.Prima
        objBECertificado.PrimaTotal = objBECertificado.PrimaCobrar * CalculoPrimaTotal(mesVigencia, IDPRODUCTO, FinVigenciaAbierta)

        'If ValidaMedioPago(Convert.ToInt32(hfIDProducto.Value)) Then
        objBECertificado.IdMedioPago = pIdMedioPago
        objBECertificado.NumeroCuenta = pNumeroCuenta
        objBECertificado.Vencimiento = pVencimiento
        'End If
        objBECertificado.UsuarioCreacion = User.Identity.Name

        objBECertificado.IdEstadoCertificado = hfEstadoCetificado.Value
        objBECertificado.IdMotivoAnulacion = NullTypes.DecimalNull
        objBECertificado.Incentivo = NullTypes.DecimalNull
        objBECertificado.SaldoIncentivo = NullTypes.DecimalNull
        objBECertificado.IdCiclo = NullTypes.DecimalNull
        objBECertificado.DiaGenera = NullTypes.DecimalNull
        objBECertificado.MesGenera = NullTypes.DecimalNull
        objBECertificado.DigitacionAnulacion = NullTypes.FechaNull
        objBECertificado.FechaModificacion = NullTypes.FechaNull
        objBECertificado.EfectuarAnulacion = NullTypes.FechaNull
        objBECertificado.SolicitudAnulacion = NullTypes.FechaNull
        objBECertificado.AfiliadoHasta = NullTypes.FechaNull
        objBECertificado.MontoAsegurado = 0

        objBECertificado.IdCertificadoCrossSelling = NullTypes.CadenaNull
        If (Not Session(SESSION_IS_CROSS_SELLING) Is Nothing AndAlso Session(SESSION_IS_CROSS_SELLING) = 1) Then
            Dim objBECertificadoContainer As BECertificadoContainer = DirectCast(Session(SESSION_CERTIFICADOCONTAINER), BECertificadoContainer)
            If (Not (objBECertificadoContainer Is Nothing)) Then
                Dim objBECrossCertificado As BECertificado = objBECertificadoContainer.ObjBECertificado
                If (Not objBECrossCertificado Is Nothing) Then
                    objBECertificado.IdCertificadoCrossSelling = objBECrossCertificado.IdCertificado
                End If
            End If
        End If
    End Sub

    Private Sub CargarAsegurado(ByRef lstBEAsegurado As List(Of BEAsegurado), ByVal objBECertificado As BECertificado)
        Dim objBEAsegurado As New BEAsegurado()
        Dim pFechaNacimiento As Date = CDate(DirectCast(phFormulario.FindControl("A_FechaNacimiento"), TextBox).Text)
        objBEAsegurado.IdCertificado = objBECertificado.IdCertificado
        objBEAsegurado.Consecutivo = 1
        objBEAsegurado.IdTipodocumento = objBECertificado.IdTipoDocumento
        objBEAsegurado.Ccaseg = objBECertificado.Cccliente
        objBEAsegurado.Nombre1 = objBECertificado.Nombre1
        objBEAsegurado.Nombre2 = objBECertificado.Nombre2
        objBEAsegurado.Apellido1 = objBECertificado.Apellido1
        objBEAsegurado.Apellido2 = objBECertificado.Apellido2
        objBEAsegurado.Direccion = objBECertificado.Direccion
        objBEAsegurado.Telefono = objBECertificado.Telefono
        objBEAsegurado.IdCiudad = objBECertificado.IdCiudad
        objBEAsegurado.FechaNacimiento = pFechaNacimiento
        objBEAsegurado.IdParentesco = 1
        objBEAsegurado.Activo = True
        lstBEAsegurado.Add(objBEAsegurado)
    End Sub

    Private Sub CargarBeneficiario(ByRef lstBEBeneficiario As List(Of BEBeneficiario), ByVal objBECertificado As BECertificado, ByVal objDT As DataTable)
        If Not (objDT Is Nothing) Then
            Dim oBEBen As BEBeneficiario
            For Each objDR As DataRow In objDT.Rows
                oBEBen = New BEBeneficiario
                oBEBen.IdCertificado = objBECertificado.IdCertificado
                oBEBen.Consecutivo = 1
                oBEBen.IdTipoDocumento = objDR("IDTipoDocumento")
                oBEBen.CcBenef = objDR("NroDocumento")
                oBEBen.IdParentesco = CShort(objDR("IDParentesco"))
                oBEBen.Porcentaje = CDec(objDR("Porcentaje"))
                oBEBen.Apellido1 = objDR("ApePaterno")
                oBEBen.Apellido2 = objDR("ApeMaterno")
                oBEBen.Nombre1 = objDR("PrimerNom")
                oBEBen.Nombre2 = objDR("SegundoNom")
                If objDR("FechaNac") = "" Then
                    oBEBen.FechaNacimiento = NullTypes.FechaNull
                Else
                    oBEBen.FechaNacimiento = CDate(objDR("FechaNac"))
                End If
                oBEBen.Domicilio = objDR("Direccion")
                oBEBen.Activo = True
                lstBEBeneficiario.Add(oBEBen)
            Next
        End If
    End Sub

    Private Sub CargarInfoProductoC(ByRef lstBEInfoProductoC As List(Of BEInfoProductoC), ByVal objBECertificado As BECertificado, ByVal xmlFrmCer As String)
        Dim lstInfoControl As New List(Of BEInfoControl)
        lstInfoControl = GetInfoControl("IdInfoProducto", xmlFrmCer)

        '--Obtine los InfoProducto Configurados para el Producto en la BD
        Dim oBLInfoPro As New BLInfoProducto
        Dim lstInfoProducto As ArrayList = oBLInfoPro.Seleccionar(objBECertificado.IdProducto)

        For Each objInfControl As BEInfoControl In lstInfoControl
            For Each objInfProducto As BEInfoProducto In lstInfoProducto
                If objInfControl.IDInfoD.ToUpper = objInfProducto.Nombre.ToUpper Then
                    Dim oBEInfoProductoC As New BEInfoProductoC
                    oBEInfoProductoC.IdCertificado = objBECertificado.IdCertificado
                    oBEInfoProductoC.Nombre = objInfProducto.Nombre
                    oBEInfoProductoC.IdInfoProducto = objInfProducto.IdInfoProducto
                    oBEInfoProductoC.ValorNum = NullTypes.IntegerNull
                    oBEInfoProductoC.ValorDate = NullTypes.FechaNull
                    oBEInfoProductoC.ValorString = NullTypes.CadenaNull

                    Select Case objInfProducto.Campo
                        Case "N"
                            Dim valorNum As String = GetValueControl(objInfControl.IDControl, objInfControl.TipoControl)

                            If valorNum <> "" AndAlso Not valorNum Is String.Empty Then
                                oBEInfoProductoC.ValorNum = valorNum
                            End If
                        Case "D"
                            oBEInfoProductoC.ValorDate = GetValueControl(objInfControl.IDControl, objInfControl.TipoControl)
                        Case "S"
                            oBEInfoProductoC.ValorString = GetValueControl(objInfControl.IDControl, objInfControl.TipoControl)
                            If objInfControl.TipoControl = "DropDownList" And objInfControl.GuardarIDValue = True Then
                                'Según SaveIDValue="True", el SelectedValue del DropDownList se guarda en el "ValorNum" y el .SelectedItem.Text en "ValorString"
                                oBEInfoProductoC.ValorNum = Decimal.Parse(GetValueControl(objInfControl.IDControl, objInfControl.TipoControl))
                                oBEInfoProductoC.ValorString = DirectCast(phFormulario.FindControl(objInfControl.IDControl), DropDownList).SelectedItem.Text
                            Else
                                oBEInfoProductoC.ValorString = GetValueControl(objInfControl.IDControl, objInfControl.TipoControl)
                            End If
                    End Select

                    lstBEInfoProductoC.Add(oBEInfoProductoC)
                    Exit For
                End If
            Next
        Next
    End Sub

    Private Sub CargarInfoAseguradoC(ByRef lstBEInfoAseguradoC As List(Of BEInfoAseguradoC), ByVal objBECertificado As BECertificado, ByVal xmlFrmCer As String)

        '--Obtiene los campos IdInfoAsegurado
        Dim lstInfoControl As New List(Of BEInfoControl)
        lstInfoControl = GetInfoControl("IdInfoAsegurado", xmlFrmCer)

        '--Obtine los IdInfoAsegurado Configurados para el Producto en la BD
        Dim oBLInfoAse As New BLInfoAsegurado
        Dim lstInfoAsegurado As ArrayList = oBLInfoAse.Seleccionar(objBECertificado.IdProducto)

        For Each objInfControl As BEInfoControl In lstInfoControl
            For Each objInfProducto As BEInfoAsegurado In lstInfoAsegurado
                If objInfControl.IDInfoD.ToUpper = objInfProducto.Nombre.ToUpper Then
                    Dim oBEInfoAseguradoC As New BEInfoAseguradoC
                    oBEInfoAseguradoC.IdCertificado = objBECertificado.IdCertificado
                    oBEInfoAseguradoC.IdInfoAsegurado = objInfProducto.IdInfoAsegurado
                    oBEInfoAseguradoC.Consecutivo = 1
                    oBEInfoAseguradoC.ValorNum = NullTypes.IntegerNull
                    oBEInfoAseguradoC.ValorDate = NullTypes.FechaNull
                    oBEInfoAseguradoC.ValorString = NullTypes.CadenaNull

                    Select Case objInfProducto.Campo
                        Case "N"
                            oBEInfoAseguradoC.ValorNum = GetValueControl(objInfControl.IDControl, objInfControl.TipoControl)
                        Case "D"
                            oBEInfoAseguradoC.ValorDate = GetValueControl(objInfControl.IDControl, objInfControl.TipoControl)
                        Case "S"
                            oBEInfoAseguradoC.ValorString = GetValueControl(objInfControl.IDControl, objInfControl.TipoControl)
                    End Select
                    lstBEInfoAseguradoC.Add(oBEInfoAseguradoC)
                    Exit For
                End If
            Next
        Next

    End Sub


    'Eventos
    Protected Sub btnBeneficiario_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBeneficiario.Click
        Session(SESSION_ESTADOFORM) = hfEstForm.Value
        ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M1", "modal('pBeneficiarios.aspx');", True)
    End Sub

    Protected Sub btnAsegurado_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAsegurado.Click
        'Session("IDCertificado") = HFIdCertificado.Value
        Session(SESSION_ESTADOFORM) = hfEstForm.Value
        ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M1", "modal('pAsegurados.aspx');", True)
    End Sub

    Protected Sub ibtnBuscarInformador_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ibtnBuscarInformador.Click
        BuscarInformador()
    End Sub

    Protected Sub chkCopiarDireccionHogar_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim C_Direccion1 As TextBox = DirectCast(phFormulario.FindControl("C_Direccion1"), TextBox)
        Dim IA_DireccionNro1 As TextBox = DirectCast(phFormulario.FindControl("IA_DireccionNro1"), TextBox)
        Dim IA_DireccionPiso1 As TextBox = DirectCast(phFormulario.FindControl("IA_DireccionPiso1"), TextBox)
        Dim IA_DireccionDpto1 As TextBox = DirectCast(phFormulario.FindControl("IA_DireccionDpto1"), TextBox)
        Dim IA_DireccionCPostal1 As TextBox = DirectCast(phFormulario.FindControl("IA_DireccionCPostal1"), TextBox)
        Dim C_IdCiudad1 As HiddenField = DirectCast(phFormulario.FindControl("C_IdCiudad1"), HiddenField)
        Dim txtCiudad1 As TextBox = DirectCast(phFormulario.FindControl("txtCiudad1"), TextBox)

        Dim pEstado As Boolean = DirectCast(sender, CheckBox).Checked
        If pEstado Then
            C_Direccion1.Text = DirectCast(phFormulario.FindControl("C_Direccion"), TextBox).Text
            IA_DireccionNro1.Text = DirectCast(phFormulario.FindControl("IA_DireccionNro"), TextBox).Text
            IA_DireccionPiso1.Text = DirectCast(phFormulario.FindControl("IA_DireccionPiso"), TextBox).Text
            IA_DireccionDpto1.Text = DirectCast(phFormulario.FindControl("IA_DireccionDpto"), TextBox).Text
            IA_DireccionCPostal1.Text = DirectCast(phFormulario.FindControl("IA_DireccionCPostal"), TextBox).Text
            C_IdCiudad1.Value = DirectCast(phFormulario.FindControl("C_IdCiudad"), HiddenField).Value
            txtCiudad1.Text = DirectCast(phFormulario.FindControl("txtCiudad"), TextBox).Text
        Else
            C_Direccion1.Text = ""
            IA_DireccionNro1.Text = ""
            IA_DireccionPiso1.Text = ""
            IA_DireccionDpto1.Text = ""
            IA_DireccionCPostal1.Text = ""
            C_IdCiudad1.Value = ""
            txtCiudad1.Text = ""
        End If

        C_Direccion1.Enabled = Not pEstado
        IA_DireccionNro1.Enabled = Not pEstado
        IA_DireccionPiso1.Enabled = Not pEstado
        IA_DireccionDpto1.Enabled = Not pEstado
        IA_DireccionCPostal1.Enabled = Not pEstado
        txtCiudad1.Enabled = Not pEstado

    End Sub


    Protected Sub txtFechaNac_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim txtFechaNac As TextBox = DirectCast(phFormulario.FindControl("A_FechaNacimiento"), TextBox)
        Dim txtEdad As TextBox = DirectCast(phFormulario.FindControl("txtEdad"), TextBox)
        If Not IsDate(txtFechaNac.Text) Then
            txtEdad.Text = "0"
        Else
            Dim edad As Double = Math.Truncate(CalcularEdad(CDate(txtFechaNac.Text)))
            Session("EdadAsegurado") = edad
            txtEdad.Text = edad
            Dim ccPrima As ccPrima = DirectCast(phFormulario.FindControl("ccPrima"), ccPrima)
            If (Not ccPrima Is Nothing) Then
                ccPrima.EdadAsegurado = CInt(edad)
                ccPrima.RecalcularPrima()
            End If
        End If
    End Sub

    Public Function CalcularEdad(ByVal FechaNacimiento As DateTime) As Double
        Dim dblEdad As Double = DateTime.Now.Subtract(FechaNacimiento).TotalDays / 365.25
        Return dblEdad
    End Function

    Public Function ValidaIMEI(ByVal pIMEI As String) As String
        Dim oBLModeloMarca As New BLMarcaModelo

        Return oBLModeloMarca.ValidaIMEI(pIMEI)

    End Function

    Public Function CalcularPorcentaje(ByVal objDT As DataTable) As Double
        Dim p As Decimal = 0
        If objDT Is Nothing Then
            p = 0
        Else
            For Each objDR As DataRow In objDT.Rows
                p += CDec(objDR("Porcentaje"))
            Next
        End If
        Return p
    End Function

    Private Sub CargarDatosModelos()
        Dim oBL As New BLMarcaModelo
        Dim eBL As New BEMarcaModelo

        Session(S_DATOS_MODELOS) = oBL.SeleccionarTodos(IIf(String.IsNullOrEmpty(DirectCast(phFormulario.FindControl("IA_Marca"), DropDownList).SelectedValue), "-1", DirectCast(phFormulario.FindControl("IA_Marca"), DropDownList).SelectedValue))
    End Sub

    Protected Sub btnSeleccionarMarca_Click(sender As Object, e As EventArgs) Handles btnSeleccionarMarca.Click
        Try
            Dim oBL As New BLMarcaModelo
            Dim eBL As New BEMarcaModelo

            Session(S_MODELOS) = oBL.Seleccionar(DirectCast(phFormulario.FindControl("IA_Marca"), DropDownList).SelectedValue)
            CargarDatosModelos()
            DirectCast(phFormulario.FindControl("IA_Modelo"), DropDownList).DataSource = DirectCast(Session(S_MODELOS), IList)
            DirectCast(phFormulario.FindControl("IA_Modelo"), DropDownList).DataValueField = "Modelo"
            DirectCast(phFormulario.FindControl("IA_Modelo"), DropDownList).DataTextField = "Modelo"
            DirectCast(phFormulario.FindControl("IA_Modelo"), DropDownList).DataBind()

            cc.IDProducto = IDPRODUCTO
            Session(S_CONDICION_PLAN) = DirectCast(phFormulario.FindControl("IA_Marca"), DropDownList).SelectedValue + "," + DirectCast(phFormulario.FindControl("IA_Modelo"), DropDownList).SelectedValue
            Session(S_PH_FORM) = phFormulario
            cc.Condicion = DirectCast(phFormulario.FindControl("IA_Modelo"), DropDownList).SelectedValue
            cc.RecargarPlanes()
            ddlModelo_SelectedIndexChanged(sender, e)
            'AddHandler DirectCast(phFormulario.FindControl("IA_Modelo"), DropDownList).SelectedIndexChanged, AddressOf ddlModelo_SelectedIndexChanged

        Catch ex As Exception
            Dim s As String = ""
        End Try
    End Sub

    Protected Sub ddlModelo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lista As ArrayList
        lista = DirectCast(Session(S_MODELOS), ArrayList)
        For Each element As BEMarcaModelo In DirectCast(Session(S_DATOS_MODELOS), ArrayList)
            If element.Modelo = DirectCast(phFormulario.FindControl("IA_Modelo"), DropDownList).SelectedValue And element.Opcion = cc.Opcion Then
                DirectCast(phFormulario.FindControl("IA_ValorDeducible"), TextBox).Text = element.Deducible
                DirectCast(phFormulario.FindControl("IA_ProductCode"), HiddenField).Value = element.ProductCode
                DirectCast(phFormulario.FindControl("hMontoAsegurado"), HiddenField).Value = element.ProductPrice
                Exit For
            End If
        Next
        Session(S_CONDICION_PLAN) = DirectCast(phFormulario.FindControl("IA_Marca"), DropDownList).SelectedValue + "," + DirectCast(phFormulario.FindControl("IA_Modelo"), DropDownList).SelectedValue
        Session(S_PH_FORM) = phFormulario
        cc.Condicion = DirectCast(phFormulario.FindControl("IA_Modelo"), DropDownList).SelectedValue
        cc.RecargarPlanes()
    End Sub

#End Region

#Region "Buscar Oficina"

    Protected Sub btnBuscarOficina_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscarOficina.Click
        BuscarOficina()
    End Sub

    Private Sub BuscarOficina()
        Dim oBL As New BLOficina
        gvBuscarOficina.DataSource = oBL.Buscar(CInt(hfIDProducto.Value), txtBuscarOficina.Text.Trim())
        gvBuscarOficina.DataBind()
    End Sub

    Protected Sub gvBuscarOficina_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBuscarOficina.PageIndexChanging
        gvBuscarOficina.PageIndex = e.NewPageIndex
        BuscarOficina()
    End Sub

    Protected Sub gvBuscarOficina_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBuscarOficina.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim btn = CType(e.Row.FindControl("ibtnSelOficina"), ImageButton)
            Dim id As String = DataBinder.Eval(e.Row.DataItem, "IDOficina")
            Dim cod As String = DataBinder.Eval(e.Row.DataItem, "CodOficina")
            Dim nom As String = cod & " - " & DataBinder.Eval(e.Row.DataItem, "Nombre")
            btn.Attributes.Add("onclick", "SeleccionarOficina(" & id & ",'" & nom & "'); return false;")
            'btn.Attributes.Add(" CommandArgument='<%# Bind("IDRamo") %>' CommandName="SeleccionRamo"

        End If
    End Sub
#End Region

#Region "Buscar Ramo"

    Protected Sub SeleccionRamo(Optional ByVal isLoadDefault As Boolean = False)
        Dim tieneRamo As Boolean = True
        Try
            Dim valor As String = DirectCast(phFormulario.FindControl("txtRamo"), TextBox).Text
        Catch ex As Exception
            tieneRamo = False
        End Try

        If tieneRamo Then
            Dim oBL As New BLRamo
            Dim eBL As New BERamo
            Try
                If isLoadDefault Then
                    eBL = DirectCast(oBL.Buscar("").Item(0), BERamo)
                    DirectCast(phFormulario.FindControl("txtRamo"), TextBox).Text = eBL.Nombre.ToString
                    DirectCast(phFormulario.FindControl("C_IdRamo"), HiddenField).Value = eBL.IdRamo
                Else
                    eBL.Nombre = DirectCast(phFormulario.FindControl("txtRamo"), TextBox).Text
                    eBL.IdRamo = DirectCast(phFormulario.FindControl("C_IdRamo"), HiddenField).Value
                End If
            Catch ex As Exception
                eBL.Nombre = ""
                eBL.IdRamo = 0
            End Try

            Dim ccPrima As ccPrima = DirectCast(phFormulario.FindControl("ccPrima"), ccPrima)
            IDRAMO = eBL.IdRamo
            If Not (ccPrima Is Nothing) Then
                ccPrima.IDRamo = eBL.IdRamo
                ccPrima.Parametros.Clear()
                ccPrima.Parametros.Add(eBL.IdRamo)
                ccPrima.RecargarPlanes()
            End If
        End If
    End Sub

    Protected Sub btnBuscarRamo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscarRamo.Click
        BuscarRamo()
    End Sub

    Private Sub BuscarRamo()
        Dim oBL As New BLRamo
        gvBuscarRamo.DataSource = oBL.Buscar(txtBuscarRamo.Text.Trim())
        gvBuscarRamo.DataBind()
    End Sub

    Protected Sub gvBuscarRamo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBuscarOficina.PageIndexChanging, gvBuscarRamo.PageIndexChanging
        gvBuscarRamo.PageIndex = e.NewPageIndex
        BuscarRamo()
    End Sub

    Protected Sub gvBuscarRamo_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBuscarRamo.RowDataBound, gvBuscarRamo.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim btn = CType(e.Row.FindControl("ibtnSelRamo"), ImageButton)
            Dim id As String = DataBinder.Eval(e.Row.DataItem, "IDRamo")
            Dim nom As String = DataBinder.Eval(e.Row.DataItem, "Nombre")
            btn.Attributes.Add("onclick", "SeleccionarRamo(" & id & ",'" & nom & "'); return false;")
        End If
    End Sub

    Protected Sub gvBuscarRamo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles gvBuscarRamo.SelectedIndexChanged
        SeleccionRamo()
    End Sub

    Protected Sub btn_reloadPlanRamo_Click(sender As Object, e As EventArgs) Handles btn_reloadPlanRamo.Click
        SeleccionRamo()
    End Sub
#End Region

#Region "Buscar Informador"

    Private Sub BuscarInformador()
        'System.Threading.Thread.Sleep(5000)
        Dim oBL As New BLInformador
        gvBuscarInformador.DataSource = oBL.Buscar(CInt(hfIDProducto.Value), txtBuscarInformador.Text.Trim())
        gvBuscarInformador.DataBind()
    End Sub

    Protected Sub gvBuscarInformador_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBuscarInformador.PageIndexChanging
        gvBuscarInformador.PageIndex = e.NewPageIndex
        BuscarInformador()
    End Sub

    Protected Sub gvBuscarInformador_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBuscarInformador.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim btn = CType(e.Row.FindControl("ibtnSelInformador"), ImageButton)
            Dim id As String = DataBinder.Eval(e.Row.DataItem, "IDInformador")
            Dim nom As String = DataBinder.Eval(e.Row.DataItem, "Nombre")
            Dim IDOficina As String = DataBinder.Eval(e.Row.DataItem, "IDOficina")
            Dim Oficina As String = DataBinder.Eval(e.Row.DataItem, "Oficina")
            btn.Attributes.Add("onclick", "SeleccionarInformador('" & id & "','" & nom & "','" & IDOficina & "','" & Oficina & "'); return false;")
        End If
    End Sub


#End Region

#Region "Buscar Cliente Banco"

    Private Sub InitializeDataClient()
        BCtxtNroCUIT.Text = String.Empty
        BCtxtFechaNacimiento.Text = String.Empty
        BCtxtEstadoCivil.Text = String.Empty
        BCtxtNacionalidad.Text = String.Empty
        BCtxtLNacimiento.Text = String.Empty
        BCtxtProfesion.Text = String.Empty
        BCtxtEmail.Text = String.Empty
        BCtxtCalle.Text = String.Empty
        BCtxtDirNumero.Text = String.Empty
        BCtxtDirPiso.Text = String.Empty
        BCtxtDirCodPostal.Text = String.Empty
        BCtxtDirDpto.Text = String.Empty
        BCtxtLocalidadPro.Text = String.Empty
        BCtxtCAreaTel.Text = String.Empty
        BCtxtTelefono.Text = String.Empty
        BCtxtCAreaOtroTel.Text = String.Empty
        BCtxtOtroTelefono.Text = String.Empty
        BCtxtEmail.Text = String.Empty
    End Sub

    Private Sub SetDataClient(Cliente As WSInvoker.Model.Cliente)
        Dim oBLEquivalencia As New BLListaEquivalencia
        Dim separatorInfoCiudad As String = " / "

        InitializeDataClient()

        If Cliente.Documentos.Length > 1 Then
            BCtxtNroCUIT.Text = Cliente.Documentos(1).Numero
        End If

        BCtxtApellidoPaterno.Text = If(Cliente.Apellido.Length > 0, Cliente.Apellido(0).ToString, String.Empty)
        BCtxtApellidoMaterno.Text = If(Cliente.Apellido.Length > 1, Cliente.Apellido(1).ToString, String.Empty)
        BCtxtPrimerNombre.Text = If(Cliente.Nombre.Length > 0, Cliente.Nombre(0).ToString, String.Empty)
        BCtxtSegundoNombre.Text = If(Cliente.Nombre.Length > 1, Cliente.Nombre(1).ToString, String.Empty)
        BCtxtFechaNacimiento.Text = Cliente.FechaNacimiento
        BCtxtSexo.Text = Cliente.Sexo
        BChfIDEstadoCivil.Value = Cliente.EstadoCivil
        BCtxtNacionalidad.Text = Cliente.Naturalizacion.Trim
        BCtxtLNacimiento.Text = Cliente.ProvinciaNacimiento.Trim
        BCtxtProfesion.Text = IIf(Not Cliente.Profesion Is Nothing, Cliente.Profesion.Descripcion1.Trim, String.Empty)

        BChfIDLocalidad.Value = WSInvoker.Constant.Constant.CIUDAD_DEFAULT_ID.ToString()
        If Not Cliente.Domicilios Is Nothing Then
            If Cliente.Domicilios.Length > 0 Then
                BCtxtCalle.Text = Cliente.Domicilios(0).Calle
                BCtxtDirNumero.Text = Cliente.Domicilios(0).Numero
                BCtxtDirPiso.Text = Cliente.Domicilios(0).Piso
                BCtxtDirCodPostal.Text = Cliente.Domicilios(0).CodigoPostal
                BCtxtDirDpto.Text = Cliente.Domicilios(0).Departamento
                BChfIDLocalidad.Value = Cliente.Domicilios(0).IdCiudad
                BCtxtLocalidadPro.Text = String.Empty
                If Cliente.Domicilios(0).IdCiudad <> 0 Then
                    BCtxtLocalidadPro.Text = Cliente.Domicilios(0).Localidad & separatorInfoCiudad & Cliente.Domicilios(0).Provincia
                End If
            End If
        End If

        If Not Cliente.Telefonos Is Nothing Then
            'Teléfono------------
            If Cliente.Telefonos.Length > 0 Then
                BCtxtCAreaTel.Text = Cliente.Telefonos(0).Prefijo
                BCtxtTelefono.Text = Cliente.Telefonos(0).Numero
            End If

            'Otro Teléfono-------
            If Cliente.Telefonos.Length > 1 Then
                BCtxtCAreaOtroTel.Text = Cliente.Telefonos(1).Prefijo
                BCtxtOtroTelefono.Text = Cliente.Telefonos(1).Numero
            End If
        End If

        If Not Cliente.DatosContacto Is Nothing Then
            If Cliente.DatosContacto.Length > 0 Then
                BCtxtEmail.Text = Cliente.DatosContacto(0).Email
            End If
        End If

        ValidateDataClient()
    End Sub


    Private Sub ValidateDataClient()
        Dim DatoIncompleto As String = String.Empty

        '--Se excluyen las personas fisicas ya que no posee esta informacion
        If ddlTipoDocCliente.SelectedValue.Trim <> "R" Then
            'Datos Incompletos------------------------
            If (BCtxtPrimerNombre.Text = "" Or String.IsNullOrEmpty(BCtxtPrimerNombre.Text)) Then
                DatoIncompleto &= " - Primer Nombre\n"
            End If
            If (BCtxtApellidoPaterno.Text = "" Or String.IsNullOrEmpty(BCtxtApellidoPaterno.Text)) Then
                DatoIncompleto &= " - Primer Apellido\n"
            End If
            If (BChfIDLocalidad.Value = "0" Or BChfIDLocalidad.Value = "" Or String.IsNullOrEmpty(BChfIDLocalidad.Value)) Then
                DatoIncompleto &= " - Ciudad\n"
            End If
            If (BChfIDEstadoCivil.Value = "" Or String.IsNullOrEmpty(BChfIDEstadoCivil.Value)) Then
                DatoIncompleto &= " - Estado Civil\n"
            End If
            If (BCtxtFechaNacimiento.Text = "" Or String.IsNullOrEmpty(BCtxtFechaNacimiento.Text)) Then
                DatoIncompleto &= " - Fecha Nacimiento\n"
            End If
            If (BCtxtSexo.Text = "" Or String.IsNullOrEmpty(BCtxtSexo.Text)) Then
                DatoIncompleto &= " - Sexo\n"
            End If
            If (DatoIncompleto <> "") Then
                ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('Los siguientes datos no fueron encontrados:\n" & DatoIncompleto & "Por favor ingresarlos en la pantalla principal.\n ');", True)
            End If
        End If
    End Sub

    Protected Sub btnBuscarCliente_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscarCliente.Click
        Try
            mvBCliente.ActiveViewIndex = 1
            BCbtnCargarPreventa.Visible = True

            Dim SrvInvoker As New WSInvoker.WebServiceInvoker()
            Dim ClientITAU As New WSInvoker.Model.Cliente()
            Dim oBLEquivalencia As New BLListaEquivalencia
            Dim IDTipoDocumento As String = oBLEquivalencia.getValorEquivalencia(CInt(hfIDProducto.Value), "TipoDocumento", ddlTipoDocCliente.SelectedValue).ValorEquivalencia
            Dim NroDocumento As String = txtNroDocCliente.Text.Trim

            ClientITAU = SrvInvoker.GetFullClient(IDTipoDocumento, NroDocumento, IDPRODUCTO)

            If ClientITAU.EstadoRespuesta = WSInvoker.Constant.Constant.RESPONSE_OK Then
                If ClientITAU.Codigo <> String.Empty Then
                    mvBCliente.ActiveViewIndex = 0
                    '--Informacion Contratante
                    SetDataClient(ClientITAU)

                    '--Cuentas y Tarjetas
                    gvCuentaTarjeta.DataSource = ClientITAU.MediosDePago
                    gvCuentaTarjeta.DataBind()
                    gvCuentaTarjeta.SelectedIndex = -1
                Else
                    mvBCliente.ActiveViewIndex = 1
                End If
            Else
                If String.IsNullOrEmpty(ClientITAU.Codigo) Then
                    mvBCliente.ActiveViewIndex = 1
                    ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('No se encontró ningún cliente para el tipo y número de documento ingresado.');", True)
                Else
                    mvBCliente.ActiveViewIndex = -1
                    ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('El servicio no retorno una respuesta valida.');", True)
                End If
            End If
        Catch ex As Exception
            mvBCliente.ActiveViewIndex = -1
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('Error de inconsistencia de datos.');", True)
        End Try
    End Sub

    Private Function SepararApellido(ByVal pApellido As String) As String()
        Dim aApellido As New ArrayList
        Dim tabla() As String
        tabla = Split(pApellido, " ")
        Return tabla
    End Function


    Private Function ValidarTarjetaRepetida(ByVal pIDMedioPago As String, ByVal pNumero As String, ByVal pRaiz As String, ByRef pLista As ArrayList) As Boolean
        Dim respuesta As Boolean = True
        For Each oBETemp As BECuentaTarjetaWS In pLista
            If oBETemp.IDMedioPago = pIDMedioPago And oBETemp.Numero = pNumero And oBETemp.Raiz = pRaiz Then
                respuesta = False
                Exit For
            End If
        Next
        If respuesta Then
            Dim oBE As New BECuentaTarjetaWS
            oBE.IDMedioPago = pIDMedioPago
            oBE.Numero = pNumero
            oBE.Raiz = pRaiz
            pLista.Add(oBE)
        End If
        Return respuesta
    End Function


    Protected Sub gvCuentaTarjeta_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gvCuentaTarjeta.SelectedIndexChanged
        Try
            BCbtnCargarPreventa.Visible = False
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub btnActivaPreventa_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnActivaPreventa.Click
        'Session("VentaporWS") = True
        'Pre-Venta
        EstadoControlesVenta(True)
        DirectCast(phFormulario.FindControl("C_IdTipoDocumento"), DropDownList).Enabled = True
        DirectCast(phFormulario.FindControl("C_Cccliente"), TextBox).Enabled = True
        'Requerido Número del Medio de Pago
        DirectCast(phFormulario.FindControl("rfvC_NumeroCuenta"), RequiredFieldValidator).Enabled = False
        hfEstadoCetificado.Value = 6

        'Activar Botones
        btnAsegurado.Enabled = True
        btnBeneficiario.Enabled = True

        ''NUEVOS
        'btn_Asegurado.Disabled = True
        'btn_Beneficiario.Disabled = True
        ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "AC", "$.modal.close();", True)
    End Sub

    'Se puede cargar como preventa aunque no seleccione ninguna tarjeta o el cliente no tiene ninguna tarjeta cargada.
    'Si tiene tarjeta y el usuario selecciona una tarjeta el boton de CargaPreventa se oculta. En caso de que por algun
    'motivo quedo visible, se valida que no tenga ninguna tarjeta cargada.
    Protected Sub BCbtnCargarPreventa_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BCbtnCargarPreventa.Click
        If gvCuentaTarjeta.SelectedIndex <> -1 Then
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('No se puede cargar una Pre-venta con un Medio de Pago asignado');", True)
            Exit Sub
        End If
        Try
            FillCommonFields()
        Catch ex As Exception
            Dim s As String = ex.Message
        End Try


        '------Información de Pago--------------------------------------------------------
        DirectCast(phFormulario.FindControl("C_IdMedioPago"), DropDownList).Enabled = True
        DirectCast(phFormulario.FindControl("C_NumeroCuenta"), TextBox).Enabled = True
        DirectCast(phFormulario.FindControl("C_Vencimiento"), TextBox).Enabled = True

        DirectCast(phFormulario.FindControl("rfvC_NumeroCuenta"), RequiredFieldValidator).Enabled = False

        DirectCast(phFormulario.FindControl("C_NumeroCuenta"), TextBox).Text = ""

        'Modo Pre-Venta
        hfEstadoCetificado.Value = 6
        btnBeneficiario.Enabled = True
        'btn_Beneficiario.Disabled = True
        ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "AC", "$.modal.close();", True)

    End Sub

    Protected Sub BCbtnAceptar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BCbtnAceptar.Click
        If gvCuentaTarjeta.SelectedIndex = -1 Then
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('No ha seleccionado el medio de pago');", True)
            Exit Sub
        End If

        Try
            DirectCast(phFormulario.FindControl("C_IdMedioPago"), DropDownList).SelectedValue = DirectCast(gvCuentaTarjeta.SelectedRow.Cells(1).Controls(0), Label).Text
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('El medio de pago seleccionado no es válido para este producto');", True)
            Exit Sub
        End Try
        'End If

        FillCommonFields()

        DirectCast(phFormulario.FindControl("C_NumeroCuenta"), TextBox).Text = DirectCast(gvCuentaTarjeta.SelectedRow.Cells(4).Controls(0), Label).Text
        DirectCast(phFormulario.FindControl("IP_NroRaiz"), TextBox).Text = DirectCast(gvCuentaTarjeta.SelectedRow.Cells(5).Controls(0), Label).Text
        DirectCast(phFormulario.FindControl("IA_NroCBU"), TextBox).Text = DirectCast(gvCuentaTarjeta.SelectedRow.Cells(6).Controls(0), Label).Text

        '---------------------------------------------------------------------------------
        '------Información de Pago--------------------------------------------------------
        DirectCast(phFormulario.FindControl("C_IdMedioPago"), DropDownList).Enabled = False
        DirectCast(phFormulario.FindControl("C_NumeroCuenta"), TextBox).Enabled = False
        '---------------------------------------------------------------------------------

        'Requerido Número del Medio de Pago
        DirectCast(phFormulario.FindControl("rfvC_NumeroCuenta"), RequiredFieldValidator).Enabled = True

        'Modo Venta
        hfEstadoCetificado.Value = 1

        'Activar Botones
        btnAsegurado.Enabled = True
        btnBeneficiario.Enabled = True

        ''NUEVOS
        'btn_Asegurado.Disabled = True
        'btn_Beneficiario.Disabled = True

        ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "AC", "$.modal.close();", True)
    End Sub

    Private Sub SetValueControlFormulario(ByVal pIDControl As String, ByVal value As Object)
        Try
            If ValidateExistControl(pIDControl) Then
                Dim ctrl As System.Web.UI.Control = DirectCast(phFormulario.FindControl(pIDControl), System.Web.UI.Control)
                Select Case True
                    Case TypeOf ctrl Is TextBox
                        DirectCast(phFormulario.FindControl(pIDControl), TextBox).Text = Convert.ToString(value)
                        Exit Select
                    Case TypeOf ctrl Is RequiredFieldValidator
                        DirectCast(phFormulario.FindControl(pIDControl), RequiredFieldValidator).Text = Convert.ToString(value)
                        Exit Select
                    Case TypeOf ctrl Is DropDownList
                        DirectCast(phFormulario.FindControl(pIDControl), DropDownList).SelectedValue = value
                        Exit Select
                    Case TypeOf ctrl Is CheckBox
                        DirectCast(phFormulario.FindControl(pIDControl), CheckBox).Checked = Convert.ToBoolean(value)
                        Exit Select
                    Case TypeOf ctrl Is HiddenField
                        DirectCast(phFormulario.FindControl(pIDControl), HiddenField).Value = value
                        Exit Select
                End Select
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ClearControlFormulario(ByVal pIDControl As String)
        Try
            If ValidateExistControl(pIDControl) Then
                Dim ctrl As System.Web.UI.Control = DirectCast(phFormulario.FindControl(pIDControl), System.Web.UI.Control)
                Select Case True
                    Case TypeOf ctrl Is TextBox Or TypeOf ctrl Is RequiredFieldValidator
                        SetValueControlFormulario(pIDControl, String.Empty)
                        Exit Select
                End Select
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Function ValidateExistControl(ByVal nameControl As String)
        Dim oBj As Object = Nothing
        Dim existe As Boolean = True
        Try
            oBj = DirectCast(phFormulario.FindControl(nameControl), System.Web.UI.Control)
        Catch ex As Exception
            existe = False
        End Try
        If oBj Is Nothing Then
            existe = False
        End If
        Return existe
    End Function

    Protected Sub FillCommonFields()
        'Try
        DirectCast(phFormulario.FindControl("C_IdTipoDocumento"), DropDownList).Enabled = False
        DirectCast(phFormulario.FindControl("C_Cccliente"), TextBox).Enabled = False

        DirectCast(phFormulario.FindControl("C_IdTipoDocumento"), DropDownList).SelectedValue = ddlTipoDocCliente.SelectedValue
        'DirectCast(phFormulario.FindControl("C_Cccliente"), TextBox).Text = txtNroDocCliente.Text
        DirectCast(phFormulario.FindControl("C_Cccliente"), TextBox).Text = Int32.Parse(txtNroDocCliente.Text)
        DirectCast(phFormulario.FindControl("IP_NroDocCDI"), TextBox).Text = BCtxtNroCUIT.Text
        DirectCast(phFormulario.FindControl("C_Apellido1"), TextBox).Text = BCtxtApellidoPaterno.Text
        DirectCast(phFormulario.FindControl("C_Apellido2"), TextBox).Text = BCtxtApellidoMaterno.Text
        DirectCast(phFormulario.FindControl("C_Nombre1"), TextBox).Text = BCtxtPrimerNombre.Text
        DirectCast(phFormulario.FindControl("C_Nombre2"), TextBox).Text = BCtxtSegundoNombre.Text
        DirectCast(phFormulario.FindControl("A_FechaNacimiento"), TextBox).Text = BCtxtFechaNacimiento.Text
        If (Not String.IsNullOrEmpty(BCtxtFechaNacimiento.Text)) Then
            DirectCast(phFormulario.FindControl("txtEdad"), TextBox).Text = Math.Truncate(CalcularEdad(CDate(BCtxtFechaNacimiento.Text)))
        Else
            DirectCast(phFormulario.FindControl("txtEdad"), TextBox).Text = 0
        End If

        DirectCast(phFormulario.FindControl("IA_Sexo"), DropDownList).SelectedValue = Trim(BCtxtSexo.Text).ToUpper()

        'Dim edad As String = "0"
        'If Not String.IsNullOrEmpty(BCtxtFechaNacimiento.Text) Then
        '    edad = Math.Truncate(CalcularEdad(CDate(BCtxtFechaNacimiento.Text)))
        'End If
        'DirectCast(phFormulario.FindControl("txtEdad"), TextBox).Text = edad

        If BCtxtFechaNacimiento.Text <> "" Then
            Dim edad As Double = Math.Truncate(CalcularEdad(CDate(BCtxtFechaNacimiento.Text)))
            Session("EdadAsegurado") = edad
            SetValueControlFormulario("txtEdad", edad)
            Dim ccPrima As ccPrima = DirectCast(phFormulario.FindControl("ccPrima"), ccPrima)
            If (Not ccPrima Is Nothing) Then
                ccPrima.EdadAsegurado = CInt(edad)
                ccPrima.RecalcularPrima()
            End If
        Else
            ClearControlFormulario("txtEdad")
        End If


        If (Not DirectCast(phFormulario.FindControl("IA_Sexo"), DropDownList).Items.FindByValue(BCtxtSexo.Text.ToUpper()) Is Nothing) Then
            DirectCast(phFormulario.FindControl("IA_Sexo"), DropDownList).SelectedValue = BCtxtSexo.Text.ToUpper()
        End If

        If Not String.IsNullOrEmpty(BChfIDEstadoCivil.Value.Trim()) AndAlso Not DirectCast(phFormulario.FindControl("IA_EstadoCivil"), DropDownList).Items.FindByValue(BChfIDEstadoCivil.Value.ToUpper()) Is Nothing Then
            DirectCast(phFormulario.FindControl("IA_EstadoCivil"), DropDownList).SelectedValue = BChfIDEstadoCivil.Value
        End If
        DirectCast(phFormulario.FindControl("IA_Nacionalidad"), TextBox).Text = BCtxtNacionalidad.Text
        DirectCast(phFormulario.FindControl("C_Direccion"), TextBox).Text = BCtxtCalle.Text
        DirectCast(phFormulario.FindControl("IA_DireccionNro"), TextBox).Text = BCtxtDirNumero.Text
        DirectCast(phFormulario.FindControl("IA_DireccionPiso"), TextBox).Text = BCtxtDirPiso.Text
        DirectCast(phFormulario.FindControl("IA_DireccionDpto"), TextBox).Text = BCtxtDirDpto.Text
        DirectCast(phFormulario.FindControl("IA_DireccionCPostal"), TextBox).Text = BCtxtDirCodPostal.Text
        DirectCast(phFormulario.FindControl("C_CATelefono"), TextBox).Text = BCtxtCAreaTel.Text
        DirectCast(phFormulario.FindControl("C_Telefono"), TextBox).Text = BCtxtTelefono.Text
        DirectCast(phFormulario.FindControl("IA_CAOtroNroTelefono"), TextBox).Text = BCtxtCAreaOtroTel.Text
        DirectCast(phFormulario.FindControl("IA_OtroNroTelefono"), TextBox).Text = BCtxtOtroTelefono.Text
        DirectCast(phFormulario.FindControl("IA_Email"), TextBox).Text = BCtxtEmail.Text.Trim()
        DirectCast(phFormulario.FindControl("IA_Profesion"), TextBox).Text = BCtxtProfesion.Text
        DirectCast(phFormulario.FindControl("IA_LugarNac"), TextBox).Text = BCtxtLNacimiento.Text
        DirectCast(phFormulario.FindControl("C_IdCiudad"), HiddenField).Value = BChfIDLocalidad.Value
        DirectCast(phFormulario.FindControl("txtCiudad"), TextBox).Text = BCtxtLocalidadPro.Text


        EstadoControlesVenta(True)

        '///////////////------Activar Campos---------------------------------////////////
        '----Información del Titular-----------------------------------------------------
        If BCtxtApellidoPaterno.Text.Trim.Length <> 0 Then
            DirectCast(phFormulario.FindControl("C_Apellido1"), TextBox).Enabled = False 'x
        End If

        If BCtxtPrimerNombre.Text.Trim.Length <> 0 Then
            DirectCast(phFormulario.FindControl("C_Nombre1"), TextBox).Enabled = False 'x
        End If
        '--------------------------------------------------------------------------------
        '------Información Adicional del Titular-----------------------------------------
        DirectCast(phFormulario.FindControl("IA_Sexo"), DropDownList).Enabled = False 'x
        DirectCast(phFormulario.FindControl("A_FechaNacimiento"), TextBox).Enabled = False 'x
        'DirectCast(phFormulario.FindControl("IA_LugarNac"), TextBox).Enabled = pEstado 'x
        If BCtxtNacionalidad.Text.Trim.Length <> 0 Then
            DirectCast(phFormulario.FindControl("IA_Nacionalidad"), TextBox).Enabled = False 'x
        End If
    End Sub

    Protected Function ValidaMedioPago(ByVal idProd As Integer)
        Dim valido = True

        If idProd = 5220 Or idProd = 5221 Then
            valido = False
        End If

        Return valido

    End Function

#End Region

#Region "Buscar Ubigeo"

    Protected Sub BUbtnBuscarUbigeo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BUbtnBuscarUbigeo.Click
        BuscarUbigeo()
    End Sub

    Private Sub BuscarUbigeo()
        Dim oBL As New BLCiudad
        gvBuscarUbigeo.DataSource = oBL.Buscar(CInt(hfIDProducto.Value), BUtxtBuscarUbigeo.Text.Trim())
        gvBuscarUbigeo.DataBind()
    End Sub

    Protected Sub gvBuscarUbigeo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBuscarUbigeo.PageIndexChanging
        gvBuscarUbigeo.PageIndex = e.NewPageIndex
        BuscarUbigeo()
    End Sub

    Protected Sub gvBuscarUbigeo_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBuscarUbigeo.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim btn = CType(e.Row.FindControl("ibtnSelUbigeo"), ImageButton)
            Dim id As String = DataBinder.Eval(e.Row.DataItem, "IDCiudad")
            Dim Ciudad As String = DataBinder.Eval(e.Row.DataItem, "Ciudad")
            Dim Provincia As String = DataBinder.Eval(e.Row.DataItem, "Provincia")
            Dim nom As String = Ciudad & " / " & Provincia
            btn.Attributes.Add("onclick", "SeleccionarUbigeo(" & id & ",'" & nom & "'); return false;")
        End If
    End Sub



#End Region



    Protected Sub btn_NuevaVenta_Click(sender As Object, e As EventArgs)
        Session(SESSION_DTBENEFICIARIO) = Nothing
        Session(SESSION_DTASEGURADO) = Nothing
        Session(SESSION_CERTIFICADOCONTAINER) = Nothing
        Response.Redirect("EmisionCertificado.aspx?IdProducto=" & hfIDProducto.Value)
    End Sub

    Protected Sub btn_Beneficiario_Click(sender As Object, e As EventArgs)
        Session(SESSION_ESTADOFORM) = hfEstForm.Value
        ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M1", "modal('pBeneficiarios.aspx');", True)
    End Sub

    Protected Sub btn_Asegurado_Click(sender As Object, e As EventArgs)
        'Session("IDCertificado") = HFIdCertificado.Value
        Session(SESSION_ESTADOFORM) = hfEstForm.Value
        ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M1", "modal('pAsegurados.aspx');", True)
    End Sub

    Protected Sub btn_Grabar_Click(sender As Object, e As EventArgs)

        'VALIDACIONES--------
        'Verifica Beneficiario------------------
        Dim objDTBeneficiario As New DataTable()
        objDTBeneficiario = Session(SESSION_DTBENEFICIARIO)

        If DirectCast(phFormulario.FindControl("IA_EnviarMail"), CheckBox).Checked AndAlso DirectCast(phFormulario.FindControl("IA_Email"), TextBox).Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('Debe ingresar el mail para poder realizar el envío de la Poliza');", True)
            Exit Sub
        End If

        If Not (objDTBeneficiario Is Nothing) Then
            If objDTBeneficiario.Rows.Count <> 0 Then
                If CalcularPorcentaje(objDTBeneficiario) < 100 Then
                    ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('El porcentaje de Beneficiarios debe ser igual al 100%.');", True)
                    Exit Sub
                End If
            End If
        End If

        Dim condicionIVA As DropDownList = DirectCast(phFormulario.FindControl("ddIA_CondicionIva"), DropDownList)
        If Not condicionIVA Is Nothing AndAlso condicionIVA.SelectedValue = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('Ingrese la condicion de IVA del Asegurado.');", True)
            Exit Sub
        End If

        'Validar Edad---------------------
        Dim ccPrima As ccPrima = DirectCast(phFormulario.FindControl("ccPrima"), ccPrima)
        Dim txtFechaNac As TextBox = DirectCast(phFormulario.FindControl("A_FechaNacimiento"), TextBox)
        Dim edad As Int32 = Math.Truncate(CalcularEdad(CDate(txtFechaNac.Text)))

        'Validar Cliente Domicilio Asegurado (Hogar-5264)
        If CInt(hfIDProducto.Value) = 5264 Then
            Dim oBECer As New BECertificado
            Dim oBLCer As New BLCertificado
            oBECer.IdTipoDocumento = DirectCast(phFormulario.FindControl("C_IdTipoDocumento"), DropDownList).SelectedValue
            oBECer.Cccliente = DirectCast(phFormulario.FindControl("C_Cccliente"), TextBox).Text.Trim()
            oBECer.Direccion = DirectCast(phFormulario.FindControl("C_Direccion1"), TextBox).Text.Trim
            oBECer.DirNumero = DirectCast(phFormulario.FindControl("IA_DireccionNro1"), TextBox).Text.Trim
            oBECer.DirPiso = DirectCast(phFormulario.FindControl("IA_DireccionPiso1"), TextBox).Text.Trim
            oBECer.DirDpto = DirectCast(phFormulario.FindControl("IA_DireccionDpto1"), TextBox).Text.Trim
            oBECer.DirCodPostal = DirectCast(phFormulario.FindControl("IA_DireccionCPostal1"), TextBox).Text.Trim
            oBECer.IdCiudad = CInt(DirectCast(phFormulario.FindControl("C_IdCiudad1"), HiddenField).Value)
            Dim pMensaje As String = oBLCer.ValidarClienteDomicilio(oBECer)
            If pMensaje <> "" Then
                ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('" & pMensaje & ".');", True)
                Exit Sub
            End If
        End If


        Dim oBLOpcionPrima As New BLOpcionPrima
        Dim pIDProducto As Int32 = CInt(hfIDProducto.Value)
        Dim pOpcion As String = ccPrima.Opcion
        Dim pIdFrecuencia As Int32 = ccPrima.IdFrecuencia
        Dim pIdMonedad As String = ccPrima.IdMonedaPrima
        Dim lstBEOpcionPrima As IList = oBLOpcionPrima.Seleccionar(IDPRODUCTO, pOpcion, pIdFrecuencia, pCondicionPlan, edad)
        For Each objBEOpcionPrima As BEOpcionPrima In lstBEOpcionPrima
            If (objBEOpcionPrima.Opcion = pOpcion And objBEOpcionPrima.IdFrecuencia = pIdFrecuencia And objBEOpcionPrima.IdMonedaPrima = pIdMonedad) Then
                If (edad < objBEOpcionPrima.EdadMin) And objBEOpcionPrima.EdadMin > 0 Then ' valida solo si existe como configurada
                    ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('El titular o contratante no puede ser menor de " & objBEOpcionPrima.EdadMin.ToString() & " años.');", True)
                    Exit Sub
                End If
                If (edad > objBEOpcionPrima.Edadmax) And objBEOpcionPrima.Edadmax > 0 Then ' valida solo si existe como configurada
                    ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('El titular o contratante no puede ser mayor de " & objBEOpcionPrima.Edadmax.ToString() & " años.');", True)
                    Exit Sub
                End If
            End If
        Next
        '------


        Dim objBECertificado As New BECertificado
        Dim lstBEAsegurado As New List(Of BEAsegurado)()
        Dim lstBEBeneficiario As New List(Of BEBeneficiario)()
        Dim lstBEInfoProductoC As New List(Of BEInfoProductoC)()
        Dim lstBEInfoAseguradoC As New List(Of BEInfoAseguradoC)()
        Dim lstBEInfoProducto As New List(Of BEInfoProductoC)()
        Dim lstBEInfoAsegurado As New List(Of BEAsegurado)()
        Dim NumFisico As Decimal
        'Carga Config. del Producto
        Dim xmlFrmCer As String = Session(SESSION_XMLFRMCERTIFICADO).ToString()
        '------------
        CargarCertificado(objBECertificado)
        CargarAsegurado(lstBEAsegurado, objBECertificado)
        CargarBeneficiario(lstBEBeneficiario, objBECertificado, objDTBeneficiario)
        CargarInfoProductoC(lstBEInfoProductoC, objBECertificado, xmlFrmCer)
        CargarInfoAseguradoC(lstBEInfoAseguradoC, objBECertificado, xmlFrmCer)
        '------------
        Dim objBLExpedir As New BLExpedir()
        Dim lstBEExpedir As List(Of BEExpedir) = objBLExpedir.BLExpedirCertificado(objBECertificado, lstBEAsegurado, lstBEBeneficiario, lstBEInfoProductoC, lstBEInfoAseguradoC, NumFisico)

        Dim cadena As String = ""
        pMensaje.Visible = True
        If lstBEExpedir(0).Codigo = 0 Then
            lblMensaje.Text = "El certificado fue expedido satisfactoriamente."
            pMensaje.CssClass = "exito"
            lblMensajeHeader.Text = "-" + lblMensaje.Text
            hfEstForm.Value = "1"

            pFrmVenta.Enabled = False
            btnNuevaVenta.Enabled = True
            btnCancelar.Enabled = False
            btnGrabar.Enabled = False
            btnImprimir.Enabled = True

            ''NUEVOS
            'btn_NuevaVenta.Disabled = True
            'btn_Cancelar.Disabled = False
            'btn_Grabar.Disabled = False
            'btn_Imprimir.Disabled = True

            Dim objCerticadoContainer As New BECertificadoContainer
            objCerticadoContainer.DescripcionCiudad = DirectCast(phFormulario.FindControl("txtCiudad"), TextBox).Text
            objCerticadoContainer.DescripcionInformador = DirectCast(phFormulario.FindControl("txtInformador"), TextBox).Text
            objCerticadoContainer.DescripcionOficina = DirectCast(phFormulario.FindControl("txtOficina"), TextBox).Text
            Dim txtDireccionRiesgo As TextBox = DirectCast(phFormulario.FindControl("txtCiudad1"), TextBox)
            If (Not txtDireccionRiesgo Is Nothing) Then
                objCerticadoContainer.DescripcionCiudadRiesgo = txtDireccionRiesgo.Text
            End If

            objCerticadoContainer.ObjBECertificado = objBECertificado
            objCerticadoContainer.LstBEAsegurado = lstBEAsegurado
            objCerticadoContainer.LstBEInfoProductoC = lstBEInfoProductoC
            objCerticadoContainer.LstBEInfoAseguradoC = lstBEInfoAseguradoC
            Session(SESSION_CERTIFICADOCONTAINER) = objCerticadoContainer

            ' Dim ccCrossSelling As ccCrossSelling = DirectCast(phFormulario.FindControl("ccCrossSelling"), ccCrossSelling)
            Dim oBEUsuario As BEUsuario = DirectCast(Session(SESSION_BEUSUARIO), BEUsuario)
            Dim objBECrossSellingSearch As New BECrossSelling
            Dim objMedioPagoCliente As New BEMedioPagoCliente

            With objBECrossSellingSearch

                If (Not Session(SESSION_MEDIOS_PAGO_CLIENTE) Is Nothing) Then
                    Dim aListaMediosPago As ArrayList = DirectCast(Session(SESSION_MEDIOS_PAGO_CLIENTE), ArrayList)
                    For Each beCuentaTarjeta As BECuentaTarjetaWS In aListaMediosPago
                        objMedioPagoCliente = New BEMedioPagoCliente
                        objMedioPagoCliente.IdMedioPago = beCuentaTarjeta.IDMedioPago
                        objMedioPagoCliente.IdMoneda = beCuentaTarjeta.IDmoneda
                        .SIdsMediosDePagoCliente.Add(objMedioPagoCliente)
                    Next
                End If
                .SIDUsuario = oBEUsuario.IDUsuario
                .SIDProducto = objBECertificado.IdProducto
                .SIDTipoDocumento = objBECertificado.IdTipoDocumento
                .SNumeroDocumento = objBECertificado.Cccliente
                .SIDMedioPagoUtilizado = objBECertificado.IdMedioPago
                .SNumeroDeCuentaUtilizado = objBECertificado.NumeroCuenta
                .SIDMonedaCobroUtilizado = objBECertificado.IdMonedaCobro
            End With
            Try

                ccCrossSelling.DrawCrossSellingPanel(objBECrossSellingSearch)
                img_contact.Visible = True
                ccCrossSelling.Visible = True
                ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "DC", "displayCrossSellingPanel();", True)

            Catch ex As Exception
                Session(SESSION_CERTIFICADOCONTAINER) = Nothing
                img_contact.Visible = False
                ccCrossSelling.Visible = False
            End Try
        Else
            For Each objBEExpedir As BEExpedir In lstBEExpedir
                cadena = cadena & objBEExpedir.Descripcion & Environment.NewLine
            Next
            lblMensaje.Text = cadena
            pMensaje.CssClass = "error"
            lblMensajeHeader.Text = "-" + lblMensaje.Text
        End If
    End Sub

    Protected Sub btn_Imprimir_Click(sender As Object, e As EventArgs)
        Dim IDNumeroCer As Int32 = CInt(DirectCast(phFormulario.FindControl("C_NumCertificado"), TextBox).Text)
        Dim IDProducto As Int32 = CInt(hfIDProducto.Value)
        Dim IDCertificado As String = IDProducto & "-" & IDNumeroCer

        ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M2", "Imprimir('" & IDCertificado & "','C');", True)
    End Sub

    Protected Sub CargarCrossSelling()
        Dim objBECertificadoContainer As BECertificadoContainer = DirectCast(Session(SESSION_CERTIFICADOCONTAINER), BECertificadoContainer)
        If (Not (objBECertificadoContainer Is Nothing)) Then
            Dim objBECertificado As BECertificado = objBECertificadoContainer.ObjBECertificado
            Dim objBEAsegurado As BEAsegurado = objBECertificadoContainer.LstBEAsegurado(0)
            Dim xmlFrmCer As String = Session(SESSION_XMLFRMCERTIFICADO).ToString()
            If (Not objBECertificado Is Nothing) Then
                hfEstadoCetificado.Value = objBECertificado.IdEstadoCertificado
                CargarControlesCertificado(objBECertificado, objBECertificadoContainer)
                CargarControlesAsegurado(objBEAsegurado)
                CargarControlesInfoProducto(objBECertificadoContainer.LstBEInfoProductoC, objBECertificadoContainer, xmlFrmCer)
                CargarControlesInfoAsegurado(objBECertificadoContainer.LstBEInfoAseguradoC, objBECertificadoContainer, xmlFrmCer)
                CargarControlesVendedor(objBECertificado, objBECertificadoContainer)
                EstadoControlesVenta(True)
            End If
        End If
    End Sub

    Protected Sub CargarControlesCertificado(ByVal objBECertificado As BECertificado, objBECertificadoContainer As BECertificadoContainer)
        With objBECertificado
            DirectCast(phFormulario.FindControl("C_IdTipoDocumento"), DropDownList).SelectedValue = .IdTipoDocumento
            DirectCast(phFormulario.FindControl("C_Cccliente"), TextBox).Text = .Cccliente
            DirectCast(phFormulario.FindControl("C_Apellido1"), TextBox).Text = .Apellido1
            DirectCast(phFormulario.FindControl("C_Apellido2"), TextBox).Text = .Apellido2
            DirectCast(phFormulario.FindControl("C_Nombre1"), TextBox).Text = .Nombre1
            DirectCast(phFormulario.FindControl("C_Nombre2"), TextBox).Text = .Nombre2
            DirectCast(phFormulario.FindControl("C_Direccion"), TextBox).Text = .Direccion
            DirectCast(phFormulario.FindControl("C_IdCiudad"), HiddenField).Value = .IdCiudad
            DirectCast(phFormulario.FindControl("txtCiudad"), TextBox).Text = objBECertificadoContainer.DescripcionCiudad
            DirectCast(phFormulario.FindControl("C_Telefono"), TextBox).Text = .Telefono
            Dim txtDireccionRiesgo As TextBox = DirectCast(phFormulario.FindControl("txtCiudad1"), TextBox)
            If (Not txtDireccionRiesgo Is Nothing) Then
                txtDireccionRiesgo.Text = objBECertificadoContainer.DescripcionCiudadRiesgo
            End If
        End With
    End Sub

    Protected Sub CargarControlesAsegurado(ByVal objAsegurado As BEAsegurado)
        DirectCast(phFormulario.FindControl("A_FechaNacimiento"), TextBox).Text = objAsegurado.FechaNacimiento
        txtFechaNac_TextChanged(Nothing, Nothing)
    End Sub

    Protected Sub CargarControlesInfoProducto(ByVal LstBEInfoProductoC As List(Of BEInfoProductoC), objBECertificadoContainer As BECertificadoContainer, xmlFrmCer As String)
        Dim lstInfoControl As New List(Of BEInfoControl)
        lstInfoControl = GetInfoControl("IdInfoProducto", xmlFrmCer)
        For Each objInfControl As BEInfoControl In lstInfoControl
            For Each objInfProductoC As BEInfoProductoC In LstBEInfoProductoC
                If objInfControl.IDControl = objInfProductoC.Nombre Then
                    SetValueControl(objInfControl.IDControl, objInfControl.TipoControl, objInfProductoC.Campo, objInfProductoC.ValorNum, objInfProductoC.ValorDate, objInfProductoC.ValorString)
                    Exit For
                End If
            Next
        Next
    End Sub

    Protected Sub CargarControlesInfoAsegurado(ByVal LstBEInfoAseguradoC As List(Of BEInfoAseguradoC), objBECertificadoContainer As BECertificadoContainer, xmlFrmCer As String)
        Dim lstInfoControl As New List(Of BEInfoControl)
        lstInfoControl = GetInfoControl("IdInfoAsegurado", xmlFrmCer)
        For Each objInfControl As BEInfoControl In lstInfoControl
            For Each objInfProductoC As BEInfoAseguradoC In LstBEInfoAseguradoC
                If objInfControl.IDControl = objInfProductoC.Nombre Then
                    SetValueControl(objInfControl.IDControl, objInfControl.TipoControl, objInfProductoC.Campo, objInfProductoC.ValorNum, objInfProductoC.ValorDate, objInfProductoC.ValorString)
                    Exit For
                End If
            Next
        Next
    End Sub

    Protected Sub CargarControlesVendedor(ByVal objBECertificado As BECertificado, objBECertificadoContainer As BECertificadoContainer)
        DirectCast(phFormulario.FindControl("txtInformador"), TextBox).Text = objBECertificadoContainer.DescripcionInformador
        DirectCast(phFormulario.FindControl("txtOficina"), TextBox).Text = objBECertificadoContainer.DescripcionOficina
        DirectCast(phFormulario.FindControl("C_IdInformador"), HiddenField).Value = objBECertificado.IdInformador
        DirectCast(phFormulario.FindControl("C_IdOficina"), HiddenField).Value = objBECertificado.IdOficina
    End Sub

    Protected Sub btnGuardarSolicitudContacto_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardarSolicitudContacto.Click

        Dim pIdTipoDocumentoContacto As String = DirectCast(phFormulario.FindControl("ddlTipoDocumentoContacto"), DropDownList).SelectedValue
        Dim pDocumentoContacto As String = DirectCast(phFormulario.FindControl("txtDocumentoContacto"), TextBox).Text.Trim()
        Dim pNombreContacto As String = DirectCast(phFormulario.FindControl("txtNombreContacto"), TextBox).Text.Trim()
        Dim pEmailContacto As String = DirectCast(phFormulario.FindControl("txtEmailContacto"), TextBox).Text.Trim()
        Dim pTelefonoContacto As String = DirectCast(phFormulario.FindControl("txtTelefonoContacto"), TextBox).Text.Trim()
        Dim pCelularContacto As String = DirectCast(phFormulario.FindControl("txtCelularContacto"), TextBox).Text.Trim()
        Dim pObservaciones As String = DirectCast(phFormulario.FindControl("txtObservacionesContacto"), TextBox).Text.Trim()
        Dim pTipoFrecuenciaContacto As String = DirectCast(phFormulario.FindControl("ddlFrecuenciaContacto"), DropDownList).SelectedValue
        Dim pValorFrecuencia As String = String.Empty

        If (pTipoFrecuenciaContacto = "F") Then
            pValorFrecuencia = DirectCast(phFormulario.FindControl("txtValorDateFrecuenciaContacto"), TextBox).Text.Trim()
        Else
            pValorFrecuencia = DirectCast(phFormulario.FindControl("txtValorFrecuenciaContacto"), TextBox).Text.Trim()
        End If


        If (String.IsNullOrEmpty(pTelefonoContacto) AndAlso String.IsNullOrEmpty(pCelularContacto) AndAlso String.IsNullOrEmpty(pEmailContacto)) Then
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('Debe indicar por lo menos algun medio de contacto (Telefono, Celular o Email)');", True)
            Exit Sub
        End If

        If (String.IsNullOrEmpty(pObservaciones)) Then
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('Por favor indique las observaciones');", True)
            Exit Sub
        End If

        Dim objBESolicitudContacto As New BESolicitudContactoCliente
        Dim objBLContactoCliente As New BLSolicitudConctactoCliente

        Dim objBECertificadoContainer As BECertificadoContainer = DirectCast(Session(SESSION_CERTIFICADOCONTAINER), BECertificadoContainer)
        If (Not (objBECertificadoContainer Is Nothing)) Then
            Dim objBECertificado As BECertificado = objBECertificadoContainer.ObjBECertificado
            With objBESolicitudContacto
                .IdTipoDocumento = pIdTipoDocumentoContacto
                .ccCliente = pDocumentoContacto
                .Nombre1 = objBECertificado.Nombre1
                .Nombre2 = objBECertificado.Nombre2
                .Apellido1 = objBECertificado.Apellido1
                .Apellido2 = objBECertificado.Apellido2
                .Email = pEmailContacto
                .Telefono = pTelefonoContacto
                .Celular = pCelularContacto
                .IDCertificadoOrigen = objBECertificado.IdCertificado
                .Observaciones = pObservaciones
                .IDUsuarioCreacion = User.Identity.Name
                .IDInformador = objBECertificado.IdInformador
                .CodigoTipoFrecuencia = pTipoFrecuenciaContacto
                .ValorFrecuenciaContacto = pValorFrecuencia
                Dim productosdeInteres As String = hfIDProductoSelectCrossSelling.Value
                If (Not String.IsNullOrEmpty(productosdeInteres)) Then
                    If (productosdeInteres <> "0") Then
                        Dim objBeSolicitudProducto As BESolicitudContactoProducto
                        Dim seleccionados() As String = productosdeInteres.Split(",")
                        For Each p As String In seleccionados
                            If (Not String.IsNullOrEmpty(p)) Then
                                If (IsNumeric(p)) Then
                                    objBeSolicitudProducto = New BESolicitudContactoProducto
                                    objBeSolicitudProducto.IdProducto = CInt(p)
                                    .Productos.Add(objBeSolicitudProducto)
                                End If
                            End If
                        Next
                    End If
                End If
            End With

            If (objBLContactoCliente.Guardar(objBESolicitudContacto)) Then
                ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Close", "CloseSucessContactSave()", True)
            Else
                ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('Error: no se pudo registrar la solicitud de contacto');", True)
            End If
        Else
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('No se encuentran los datos de la venta con los datos del cliente');", True)
        End If
    End Sub

    Protected Sub btnListaSolicitudesContacto_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnListaSolicitudesContacto.Click
        Dim oBL As New BLSolicitudConctactoCliente
        Dim idProducto As Integer = CInt(hfIDProductoSelectCrossSelling.Value)
        Dim objBECertificadoContainer As BECertificadoContainer = DirectCast(Session(SESSION_CERTIFICADOCONTAINER), BECertificadoContainer)
        If (Not (objBECertificadoContainer Is Nothing)) Then
            Dim objBECertificado As BECertificado = objBECertificadoContainer.ObjBECertificado
            If (Not objBECertificado Is Nothing) Then
                gvSolicitudesContacto.DataSource = oBL.ListarSolicitudes(idProducto, objBECertificado.IdTipoDocumento, objBECertificado.Cccliente)
                gvSolicitudesContacto.DataBind()
            End If
        End If
    End Sub
End Class



Public Class NullTypes
    Public Shared CadenaNull As String = String.Empty
    Public Shared FechaNull As Date = DateTime.Parse("1900-01-01")
    Public Shared DecimalNull As Decimal = -1
    Public Shared IntegerNull As Integer = -1
    Public Shared ShortNull As Int16 = -1
    Public Shared LongNull As Int64 = -1
    Public Shared BoolNull As Boolean = False
    Public Shared FloatNull As Single = -1
    Public Shared DoubleNull As Double = -1
End Class