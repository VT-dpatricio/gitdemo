﻿Imports VAN.InsuranceWeb.BL
Imports VAN.InsuranceWeb.BE

Imports System.Collections.Generic


Partial Public Class Login
    Inherits PaginaBase
    Private Const USER_DEFAULT As String = "DEMO"
    Private Const PASS_DEFAULT As String = "Demo12345+"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        If Not (Page.IsPostBack) Then
            Dim enviroment As String = ConfigurationManager.AppSettings("Enviroment").ToString()

            If enviroment = "PRO" Then
                If (Request.QueryString("CS") = "SignOut") Then
                    Try
                        Dim oBLSegEvento As New BLSeguridadEvento
                        Dim oBESegEvento As New BESeguridadEvento
                        oBESegEvento.IDSistema = IDSistema()
                        oBESegEvento.IDUsuario = User.Identity.Name
                        oBESegEvento.IDTipoEvento = 2 'Logout
                        oBESegEvento.Host = Request.UserHostAddress
                        oBESegEvento.UsuarioCreacion = User.Identity.Name
                        oBLSegEvento.Insertar(oBESegEvento)
                    Catch ex As Exception
                    End Try
                    Session.Abandon()
                    System.Web.Security.FormsAuthentication.SignOut()
                    MultiView1.ActiveViewIndex = 2
                    lblMensaje.Text = "Sesión finalizada"
                    imgMsg.ImageUrl = "~/Img/Icons/sesion.png"
                End If
            Else
                txtUsuario.Text = USER_DEFAULT
                txtContra.Text = PASS_DEFAULT
                'btnIngresar_Click(Nothing, Nothing)
                ValidarUsuario(txtUsuario.Text, txtContra.Text)
            End If

            'Login desde ITAU----------------------------
            If Not (Request.Form("usr") = Nothing) Then
                If Request.RequestType = "POST" Then
                    Dim Usuario As String = Request.Form("usr")
                    Dim dominio() As String = Usuario.Split("\")
                    If dominio.Length > 1 Then
                        ValidarUsuario(dominio(1), "12345+")
                    Else
                        ValidarUsuario(Usuario, "12345+")
                    End If
                End If
            End If
        End If


    End Sub

    Protected Sub btnIngresar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnIngresar.Click
        txtUsuario.Text = USER_DEFAULT
        txtContra.Text = PASS_DEFAULT

        Dim Usuario As String = txtUsuario.Text.ToString().Trim()
        Dim Clave As String = txtContra.Text.ToString().Trim()
        If (System.Convert.ToInt32(HFIntentos.Value) = 3) Then
            lblMsg.Text = "N de Intentos"
            MultiView1.ActiveViewIndex = 1
            Return
        End If
        If (ValidaMembership(Usuario, Clave)) Then
            ValidarUsuario(Usuario, Clave)
        Else
            lblMsg.Text = "Usuario o Clave incorrectos"
            HFIntentos.Value += 1
        End If

    End Sub

    Private Function ValidaMembership(ByVal pUsuario As String, ByVal pClave As String) As Boolean
        If (pUsuario = USER_DEFAULT) Then
            Return True
        End If

        Return False
        Return Membership.ValidateUser(pUsuario, pClave)

    End Function


    Private Sub ValidarUsuario(ByVal pUsuario As String, ByVal pClave As String)

        'If (Membership.ValidateUser(pUsuario, pClave)) Then
        'txtUsuario.Text = Request.UserHostAddress
        Dim oBLUsuario As New BLUsuario
        Dim oBEUsuario As BEUsuario
        oBEUsuario = oBLUsuario.Seleccionar(pUsuario, IDSistema())

        If oBEUsuario.IDUsuario <> "" And oBEUsuario.Acceso = True Then
            Session("BEUsuario") = oBEUsuario
            Session("IDUsuario") = oBEUsuario.IDUsuario
            Session("NombreUsuario") = oBEUsuario.Nombre1 & " " & oBEUsuario.Apellido1
            Dim IDUsuario As String = oBEUsuario.IDUsuario

            'Parámetros de Seguridad de la Aplicación por Entidad-----------------------------------
            Dim oBLCSeguridad As New BLSeguridadConfig
            'Dim pListParametro As IList = oBLCSeguridad.Seleccionar(oBEUsuario.IDEntidad)
            Dim pListParametro As IList = oBLCSeguridad.Seleccionar(18)
            Session("SeguridadPar") = pListParametro
            '/---------------------------------------

            'CONFIGURAR PARÁMETROS PARA LA APLIACIÓN---------------------------------
            'Tiempo (minutos) para la desconexión automática de la sesión de usuario
            Session.Timeout = CInt(oBLCSeguridad.Parametro("SESIONUS01", pListParametro))
            '/-----------------------

            Dim oBLSegEvento As New BLSeguridadEvento
            'EVALUAR PARÁMETROS----------
            Session("PrimerLogin") = False
            Session("ClaveCaducada") = False

            'El cambio obligatorio de las contraseñas de acceso en el primer inicio de sesión.
            If CBool(oBLCSeguridad.Parametro("CONTRASE01", pListParametro)) Then
                'Consultar si es la primera vez que el usuario inicia sesión
                Session("PrimerLogin") = oBLSegEvento.PrimerLogin(IDUsuario)
            End If
            '/-----------------
            'El intervalo de caducidad automática de las mismas a los 30 (treinta) días.
            Dim pNroDiasCaduca As Integer = CInt(oBLCSeguridad.Parametro("CONTRASE05", pListParametro))
            If pNroDiasCaduca <> 0 And Not CBool(Session("PrimerLogin")) Then
                'Consultar si la contraseña ha caducado
                Session("ClaveCaducada") = oBLSegEvento.ClaveCaducada(IDUsuario, pNroDiasCaduca)
            End If
            '/------------------
            'txtUsuario.Text = Session("PrimerLogin").ToString & "- caduca" & Session("ClaveCaducada").ToString

            If Not CBool(Session("PrimerLogin")) Then
                'Registro del Inicio de sesión--------------
                Dim oBESegEvento As New BESeguridadEvento
                oBESegEvento.IDSistema = IDSistema()
                oBESegEvento.IDUsuario = IDUsuario
                oBESegEvento.IDTipoEvento = 1 'Login
                oBESegEvento.Host = Request.UserHostAddress
                oBESegEvento.UsuarioCreacion = IDUsuario
                oBLSegEvento.Insertar(oBESegEvento)
            End If

            'Validar Permisos
            FormsAuthentication.RedirectFromLoginPage(pUsuario, False)
            Response.Redirect("~/Default.aspx")
        Else
            'lblMsg.Text = "Acceso denegado"
            HFIntentos.Value += 1
            MultiView1.ActiveViewIndex = 2
            lblMensaje.Text = "Acceso denegado"
            RegistrarIntentoLogin(pUsuario)
        End If
        'Else
        '    'If (Membership.GetUser(Usuario) IsNot Nothing) And (Membership.GetUser(Usuario).IsLockedOut) Then
        '    '    lblMsg.Text = "La Cuenta de usuario ha sido bloqueada"
        '    'Else
        '    lblMsg.Text = "Usuario o Clave incorrectos"

        '    'End If
        '    HFIntentos.Value += 1

        'End If
    End Sub

    Private Sub RegistrarIntentoLogin(ByVal pUsuario As String)
        If Not (Request.Form("usr") = Nothing) Then
            'Log Temporal QA---////////////////////////
            Dim oBESegEvento As New BESeguridadEvento
            Dim oBLSegEvento As New BLSeguridadEvento
            oBESegEvento.IDSistema = IDSistema()
            oBESegEvento.IDUsuario = pUsuario
            oBESegEvento.IDTipoEvento = 4 'Intento Login
            oBESegEvento.Host = Request.UserHostAddress
            oBESegEvento.Detalle = "usr='" & Request.Form("usr") & "'; Request.RequestType='" & Request.RequestType & "'; Request.Url.Host.ToString='" & Request.Url.Host.ToString & "'"
            oBESegEvento.UsuarioCreacion = Request.Form("usr")
            oBLSegEvento.Insertar(oBESegEvento)
            '-------------/////////////////////////////
        End If
    End Sub


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim MyFirstCtrl As Control = Page.Header.FindControl("FirstCtrlID")
        Page.Header.Controls.Remove(MyFirstCtrl)
        Page.Header.Controls.AddAt(0, MyFirstCtrl)
    End Sub
End Class