﻿Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.BL



#Region "TipoInfoProducto"
Partial Public Class TipoInfoProducto
    Private _TipoInfoProductoBE As TipoInfoProductoBE = Nothing
    Private _TipoInfoProductoBL As TipoInfoProductoBL = Nothing

    Public Function Agregar(ByVal Descripcion As String, ByVal Usuario As String, ByVal Fecha As DateTime) As String
        _TipoInfoProductoBE.Descripcion = Descripcion
        _TipoInfoProductoBE.Estado = "A"
        _TipoInfoProductoBE.UsuarioReg = Usuario
        _TipoInfoProductoBE.FechaReg = Fecha
        _TipoInfoProductoBE.UsuarioReg = Usuario
        _TipoInfoProductoBE.FechaMod = Fecha
        Return _TipoInfoProductoBL.Agregar(_TipoInfoProductoBE)
    End Function

    Public Function Modificar(ByVal IdTipoInfoProducto As Integer, ByVal Descripcion As String, ByVal Usuario As String, ByVal Fecha As DateTime) As String
        _TipoInfoProductoBE.IdTipoInfoProducto = IdTipoInfoProducto
        _TipoInfoProductoBE.Descripcion = Descripcion
        _TipoInfoProductoBE.Estado = "A"
        _TipoInfoProductoBE.UsuarioReg = Usuario
        _TipoInfoProductoBE.FechaMod = Fecha
        Return _TipoInfoProductoBL.Modificar(_TipoInfoProductoBE)
    End Function

    Public Function Eliminar(ByVal IdTipoInfoProducto As Integer, ByVal Usuario As String, ByVal Fecha As DateTime) As String
        _TipoInfoProductoBE.IdTipoInfoProducto = IdTipoInfoProducto
        _TipoInfoProductoBE.Estado = "I"
        _TipoInfoProductoBE.UsuarioReg = Usuario
        _TipoInfoProductoBE.FechaMod = Fecha
        Return _TipoInfoProductoBL.Eliminar(_TipoInfoProductoBE)
    End Function

    Public Function Listar_Todos() As List(Of TipoInfoProductoBE)
        Return _TipoInfoProductoBL.Listar_Todos()
    End Function

    Public Function Listar_Id(ByVal IdTipoInfoProducto As Integer) As TipoInfoProductoBE
        _TipoInfoProductoBE.IdTipoInfoProducto = IdTipoInfoProducto
        Return _TipoInfoProductoBL.Listar_Id(_TipoInfoProductoBE)
    End Function

    Public Function Listar_IdTipoInfoProducto(ByVal IdTipoInfoProducto As Integer) As TipoInfoProductoBE
        _TipoInfoProductoBE.IdTipoInfoProducto = IdTipoInfoProducto
        Return _TipoInfoProductoBL.Listar_Id(_TipoInfoProductoBE)
    End Function

    Public Sub New()
        _TipoInfoProductoBE = New TipoInfoProductoBE
        _TipoInfoProductoBL = New TipoInfoProductoBL
    End Sub

End Class
#End Region

#Region "InfoProducto"
Partial Public Class InfoProducto
    Private _InfoProductoBE As InfoProductoBE = Nothing
    Private _InfoProductoBL As InfoProductoBL = Nothing

    Public Function Agregar(ByVal IdTipoInfoProducto As Integer, ByVal Descripcion As String, ByVal Abrev As String, ByVal Panel As Boolean, ByVal Usuario As String, ByVal Fecha As DateTime) As String
        _InfoProductoBE.IdTipoInfoProducto = IdTipoInfoProducto
        _InfoProductoBE.Descripcion = Descripcion
        _InfoProductoBE.Abrev = Abrev
        _InfoProductoBE.Estado = "A"
        _InfoProductoBE.Panel = False
        _InfoProductoBE.UsuarioReg = Usuario
        _InfoProductoBE.FechaReg = Fecha
        _InfoProductoBE.UsuarioMod = Usuario
        _InfoProductoBE.FechaMod = Fecha
        Return _InfoProductoBL.Agregar(_InfoProductoBE)
    End Function

    Public Function Modificar(ByVal IdInfoProducto As Integer, ByVal IdTipoInfoProducto As Integer, ByVal Descripcion As String, ByVal Abrev As String, ByVal Panel As Boolean, ByVal Usuario As String, ByVal Fecha As DateTime) As String
        _InfoProductoBE.IdInfoProducto = IdInfoProducto
        _InfoProductoBE.IdTipoInfoProducto = IdTipoInfoProducto
        _InfoProductoBE.Descripcion = Descripcion
        _InfoProductoBE.Abrev = Abrev
        _InfoProductoBE.Panel = False
        _InfoProductoBE.Estado = "A"
        _InfoProductoBE.UsuarioMod = Usuario
        _InfoProductoBE.FechaMod = Fecha
        Return _InfoProductoBL.Modificar(_InfoProductoBE)
    End Function

    Public Function Eliminar(ByVal IdInfoProducto As Integer, ByVal Usuario As String, ByVal Fecha As DateTime) As String
        _InfoProductoBE.IdInfoProducto = IdInfoProducto
        _InfoProductoBE.Estado = "I"
        _InfoProductoBE.UsuarioMod = Usuario
        _InfoProductoBE.FechaMod = Fecha
        Return _InfoProductoBL.Eliminar(_InfoProductoBE)
    End Function

    Public Function Listar_Todos() As List(Of InfoProductoBE)
        Return _InfoProductoBL.Listar_Todos()
    End Function

    Public Function Listar_Id(ByVal IdInfoProducto As Integer) As InfoProductoBE
        _InfoProductoBE.IdInfoProducto = IdInfoProducto
        Return _InfoProductoBL.Listar_Id(_InfoProductoBE)
    End Function

    Public Function Listar_IdTipoInfoProducto(ByVal IdTipoInfoProducto As Integer) As List(Of InfoProductoBE)
        _InfoProductoBE.IdTipoInfoProducto = IdTipoInfoProducto
        Return _InfoProductoBL.Listar_IdTipoInfoProducto(_InfoProductoBE)
    End Function

    Public Function Listar_BDJanus(ByVal _IdTipo As Integer, ByVal _Descripcion As String) As List(Of InfoProductoBE)
        Return _InfoProductoBL.Listar_BDJanus(_IdTipo, _Descripcion)
    End Function

    Public Sub New()
        _InfoProductoBE = New InfoProductoBE
        _InfoProductoBL = New InfoProductoBL
    End Sub

End Class
#End Region

#Region "DetalleInfoProducto"
Partial Public Class DetalleInfoProducto
    Private _DetalleInfoProductoBE As DetalleInfoProductoBE = Nothing
    Private _DetalleInfoProductoBL As DetalleInfoProductoBL = Nothing

    Public Function Agregar(ByVal IdInfoProducto As Integer, ByVal IdTipoValor As Integer, ByVal Descripcion As String, ByVal Usuario As String, ByVal Fecha As DateTime) As String
        _DetalleInfoProductoBE.IdInfoProducto = IdInfoProducto
        _DetalleInfoProductoBE.IdTipoValor = IdTipoValor
        _DetalleInfoProductoBE.Descripcion = Descripcion
        _DetalleInfoProductoBE.Estado = "A"
        _DetalleInfoProductoBE.UsuarioReg = Usuario
        _DetalleInfoProductoBE.FechaReg = Fecha
        _DetalleInfoProductoBE.UsuarioMod = Usuario
        _DetalleInfoProductoBE.FechaMod = Fecha
        Return _DetalleInfoProductoBL.Agregar(_DetalleInfoProductoBE)
    End Function


    Public Function AgregarList(ByVal Lista As List(Of DetalleInfoProductoBE))
        Try
            For Each O As DetalleInfoProductoBE In Lista
                Agregar(O.IdInfoProducto, O.IdTipoValor, O.Descripcion, O.UsuarioReg, O.FechaReg)
            Next
        Catch ex As Exception

        End Try
    End Function



    Public Function Modificar(ByVal IdDetalleInfoProducto As Integer, ByVal IdInfoProducto As Integer, ByVal IdTipoValor As Integer, ByVal Descripcion As String, ByVal Usuario As String, ByVal Fecha As DateTime) As String
        _DetalleInfoProductoBE.IdDetalleInfoProducto = IdDetalleInfoProducto
        _DetalleInfoProductoBE.IdInfoProducto = IdInfoProducto
        _DetalleInfoProductoBE.IdTipoValor = IdTipoValor
        _DetalleInfoProductoBE.Descripcion = Descripcion
        _DetalleInfoProductoBE.Estado = "A"
        _DetalleInfoProductoBE.UsuarioMod = Usuario
        _DetalleInfoProductoBE.FechaMod = Fecha
        Return _DetalleInfoProductoBL.Modificar(_DetalleInfoProductoBE)
    End Function

    Public Function Eliminar(ByVal IdDetalleInfoProducto As Integer, ByVal Usuario As String, ByVal Fecha As DateTime) As String
        _DetalleInfoProductoBE.IdDetalleInfoProducto = IdDetalleInfoProducto
        _DetalleInfoProductoBE.Estado = "I"
        _DetalleInfoProductoBE.UsuarioMod = Usuario
        _DetalleInfoProductoBE.FechaMod = Fecha
        Return _DetalleInfoProductoBL.Eliminar(_DetalleInfoProductoBE)
    End Function

    Public Function Listar_Todos() As List(Of DetalleInfoProductoBE)
        Return _DetalleInfoProductoBL.Listar_Todos()
    End Function

    Public Function Listar_Id(ByVal IdDetalleInfoProducto As Integer) As DetalleInfoProductoBE
        _DetalleInfoProductoBE.IdDetalleInfoProducto = IdDetalleInfoProducto
        Return _DetalleInfoProductoBL.Listar_Id(_DetalleInfoProductoBE)
    End Function

    Public Function Listar_IdInfoProducto(ByVal IdInfoProducto As Integer) As List(Of DetalleInfoProductoBE)
        _DetalleInfoProductoBE.IdInfoProducto = IdInfoProducto
        Return _DetalleInfoProductoBL.Listar_IdInfoProducto(_DetalleInfoProductoBE)
    End Function

    Public Sub New()
        _DetalleInfoProductoBE = New DetalleInfoProductoBE
        _DetalleInfoProductoBL = New DetalleInfoProductoBL
    End Sub
End Class
#End Region

#Region "Producto"
Partial Public Class Producto
    Private _ProductoBE As ProductoBE = Nothing
    Private _ProductoBL As ProductoBL = Nothing

    Public Function Agregar(ByVal Descripcion As String, ByVal Abrev As String, ByVal Usuario As String, ByVal Fecha As DateTime, ByVal ListEntidad As List(Of EntidadBE), ByVal ListInfoProducto As List(Of InfoProductoBE), ByVal vEdad As Boolean, ByVal vMonto As Boolean) As String
        _ProductoBE.Descripcion = Descripcion
        _ProductoBE.Abrev = Abrev
        _ProductoBE.Logo = ""
        _ProductoBE.Banner = ""
        _ProductoBE.Reporte = ""
        _ProductoBE.Estado = "A"
        _ProductoBE.UsuarioReg = Usuario
        _ProductoBE.FechaReg = Fecha
        _ProductoBE.UsuarioMod = Usuario
        _ProductoBE.FechaMod = Fecha
        _ProductoBE.ListEntidadBE = ListEntidad
        _ProductoBE.ListInfoProducto = ListInfoProducto
        _ProductoBE.vEdad = vEdad
        _ProductoBE.vMonto = vMonto

        Return _ProductoBL.Agregar(_ProductoBE)
    End Function

    Public Function AgregarBAIS(ByVal Producto As ProductoBE, ByVal ListParentescoPermitidoBE As List(Of ParentescoPermitidoBE), ByVal ListTipoDocProductoBE As List(Of TipoDocProductoBE), ByVal ListInfoProductoBE As List(Of InfoProductoBE), ByVal ListInfoAseguradoBE As List(Of InfoAseguradoBE), ByVal ListOpcionBE As List(Of OpcionBE), ByVal ListOpcionPrimaBE As List(Of OpcionPrimaBE), ByVal ListProductoMedioPagoBE As List(Of ProductoMedioPagoBE)) As String
        Return _ProductoBL.AgregarBAIS(Producto, ListParentescoPermitidoBE, ListTipoDocProductoBE, ListInfoProductoBE, ListInfoAseguradoBE, ListOpcionBE, ListOpcionPrimaBE, ListProductoMedioPagoBE)
    End Function

    Public Function Modificar(ByVal IdProducto As Integer, ByVal Descripcion As String, ByVal Abrev As String, ByVal Usuario As String, ByVal Fecha As DateTime, ByVal ListEntidad As List(Of EntidadBE), ByVal ListInfoProducto As List(Of InfoProductoBE), ByVal vEdad As Boolean, ByVal vMonto As Boolean) As String
        _ProductoBE.IdProducto = IdProducto
        _ProductoBE.Descripcion = Descripcion
        _ProductoBE.Abrev = Abrev
        _ProductoBE.Logo = ""
        _ProductoBE.Banner = ""
        _ProductoBE.Reporte = ""
        _ProductoBE.Estado = "A"
        _ProductoBE.UsuarioMod = Usuario
        _ProductoBE.FechaMod = Fecha
        _ProductoBE.ListEntidadBE = ListEntidad
        _ProductoBE.ListInfoProducto = ListInfoProducto
        _ProductoBE.vEdad = vEdad
        _ProductoBE.vMonto = vMonto

        Return _ProductoBL.Modificar(_ProductoBE)
    End Function

    Public Function ModificarDiseno(ByVal IdProducto As Integer, ByVal Logo As String, ByVal Banner As String, ByVal Reporte As String, ByVal Usuario As String, ByVal Fecha As DateTime) As String
        _ProductoBE.IdProducto = IdProducto
        _ProductoBE.Logo = Logo
        _ProductoBE.Banner = Banner
        _ProductoBE.Reporte = Reporte
        _ProductoBE.Estado = "A"
        _ProductoBE.UsuarioMod = Usuario
        _ProductoBE.FechaMod = Fecha
        Return _ProductoBL.Modificar(_ProductoBE)
    End Function

    Public Function Eliminar(ByVal IdProducto As Integer, ByVal Usuario As String, ByVal Fecha As DateTime) As String
        _ProductoBE.IdProducto = IdProducto
        _ProductoBE.Estado = "I"
        _ProductoBE.UsuarioMod = Usuario
        _ProductoBE.FechaMod = Fecha
        Return _ProductoBL.Eliminar(_ProductoBE)
    End Function

    Public Function Listar_Todos() As List(Of ProductoBE)
        Return _ProductoBL.Listar_Todos()
    End Function

    Public Function Listar_TodosBAIS(ByVal IdAsegurador As Integer, ByVal IdEntidad As Integer) As List(Of ProductoBE)
        Return _ProductoBL.Listar_TodosBAIS(IdAsegurador, IdEntidad)
    End Function

    Public Function Listar_Id(ByVal IdProducto As Integer) As ProductoBE
        _ProductoBE.IdProducto = IdProducto
        Return _ProductoBL.Listar_Id(_ProductoBE)
    End Function

    Public Function Listar_BDJanus(ByVal Nombre As String) As List(Of ProductoBE)
        Return _ProductoBL.Listar_BDJanus(Nombre)
    End Function

    Public Function ModificarDisenio(ByVal IdProducto As Integer, ByVal Logo As String, ByVal Banner As String, ByVal Reporte As String, ByVal Usuario As String, ByVal Fecha As DateTime) As String
        _ProductoBE.IdProducto = IdProducto
        _ProductoBE.Logo = Logo
        _ProductoBE.Banner = Banner
        _ProductoBE.Reporte = Reporte
        _ProductoBE.Estado = "A"
        _ProductoBE.UsuarioMod = Usuario
        _ProductoBE.FechaMod = Fecha
        Return _ProductoBL.Modificar(_ProductoBE)
    End Function

    Public Sub New()
        _ProductoBE = New ProductoBE
        _ProductoBL = New ProductoBL
    End Sub
End Class
#End Region

#Region "Certificado"
Partial Public Class Certificado
    Private _CertificadoBE As CertificadoBE = Nothing
    Private _CertificadoBL As CertificadoBL = Nothing

    Public Function ConsultarPrimaCertificado(ByVal IdProducto As Integer, ByVal Opcion As String, ByVal IdFrecuancia As Integer, ByVal IdMoneda As String, ByVal Edad As Integer) As Decimal
        Return _CertificadoBL.ConsultarPrimaCertificado(IdProducto, Opcion, IdFrecuancia, IdMoneda, Edad)
    End Function

    Public Sub New()
        _CertificadoBE = New CertificadoBE
        _CertificadoBL = New CertificadoBL
    End Sub

End Class
#End Region

#Region "ProductoEntidad"
Partial Public Class ProductoEntidad
    Private _ProductoEntidadBE As ProductoEntidadBE = Nothing
    Private _ProductoEntidadBL As ProductoEntidadBL = Nothing

    Public Function Listar_IdProducto_Check(ByVal IdProducto As Integer) As List(Of EntidadBE)
        _ProductoEntidadBE.IdProducto = IdProducto
        Return _ProductoEntidadBL.Listar_IdProductoCheck(_ProductoEntidadBE)
    End Function

    Public Function Listar_IdProducto(ByVal IdProducto As Integer) As List(Of EntidadBE)
        _ProductoEntidadBE.IdProducto = IdProducto
        Return _ProductoEntidadBL.Listar_IdProducto(_ProductoEntidadBE)
    End Function


    Public Sub New()
        _ProductoEntidadBE = New ProductoEntidadBE
        _ProductoEntidadBL = New ProductoEntidadBL
    End Sub
End Class
#End Region

#Region "Entidad"
Partial Public Class Entidad
    Private _EntidadBE As EntidadBE = Nothing
    Private _EntidadBL As EntidadBL = Nothing

    Public Function Listar_Id(ByVal IdEntidad As Integer) As EntidadBE
        _EntidadBE.IdEntidad = IdEntidad
        Return _EntidadBL.Listar_Id(_EntidadBE)
    End Function

    Public Function Listar_Todos() As List(Of EntidadBE)
        Return _EntidadBL.Listar_Todos(Nothing)
    End Function


    Public Sub New()
        _EntidadBE = New EntidadBE
        _EntidadBL = New EntidadBL
    End Sub
End Class
#End Region

#Region "ProductoInfoProducto"
Partial Public Class ProductoInfoProducto
    Private _ProductoInfoProductoBE As ProductoInfoProductoBE = Nothing
    Private _ProductoInfoProductoBL As ProductoInfoProductoBL = Nothing

    Public Function Listar_IdProducto_Check(ByVal IdProducto As Integer) As List(Of InfoProductoBE)
        _ProductoInfoProductoBE.IdProducto = IdProducto
        Return _ProductoInfoProductoBL.Listar_IdProducto_Check(_ProductoInfoProductoBE)
    End Function

    Public Function Listar_IdProducto_X_Check(ByVal IdProducto As Integer) As List(Of InfoProductoBE)
        _ProductoInfoProductoBE.IdProducto = IdProducto
        Return _ProductoInfoProductoBL.Listar_IdProducto_X_Check(_ProductoInfoProductoBE)
    End Function

    Public Sub New()
        _ProductoInfoProductoBE = New ProductoInfoProductoBE
        _ProductoInfoProductoBL = New ProductoInfoProductoBL
    End Sub
End Class
#End Region

#Region "ProductoEntidadValidacion"
Partial Public Class ProductoEntidadValidacion
    Private _ProductoEntidadValidacionBE As ProductoEntidadValidacionBE = Nothing
    Private _ProductoEntidadValidacionBL As ProductoEntidadValidacionBL = Nothing

    Public Function Listar_IdProductoEntidad_Check(ByVal IdProducto As Integer, ByVal IdEntidad As Integer) As List(Of EntidadDetalleBE)
        _ProductoEntidadValidacionBE.IdProducto = IdProducto
        _ProductoEntidadValidacionBE.IdEntidad = IdEntidad
        Return _ProductoEntidadValidacionBL.Listar_IdProductoIdEntidad(_ProductoEntidadValidacionBE)
    End Function

    Public Function Agregar(ByVal ListDEtalle As List(Of ProductoEntidadValidacionBE)) As String
        Return _ProductoEntidadValidacionBL.AgregarT(ListDEtalle)
    End Function


    Public Sub New()
        _ProductoEntidadValidacionBE = New ProductoEntidadValidacionBE
        _ProductoEntidadValidacionBL = New ProductoEntidadValidacionBL
    End Sub
End Class
#End Region

#Region "TipoValor"
Partial Public Class TipoValor

    Private _TipoValorBE As TipoValorBE = Nothing
    Private _TipoValorBL As TipoValorBL = Nothing

    Public Sub New()
        _TipoValorBE = New TipoValorBE
        _TipoValorBL = New TipoValorBL
    End Sub

    Public Function Listar_Todos() As List(Of TipoValorBE)
        Return _TipoValorBL.Listar_Todos()
    End Function


End Class
#End Region

#Region "TipoValor"
Partial Public Class TipoValidacion

    Private _TipoValidacionBE As TipoValidacionBE = Nothing
    Private _TipoValidacionBL As TipoValidacionBL = Nothing

    Public Sub New()
        _TipoValidacionBE = New TipoValidacionBE
        _TipoValidacionBL = New TipoValidacionBL
    End Sub

    Public Function Listar_Todos() As List(Of TipoValidacionBE)
        Return _TipoValidacionBL.Listar_Todos()
    End Function


End Class
#End Region

#Region "DatosDetalleInfoProducto"
Partial Public Class DatosDetalleInfoProducto
    Private _CertificadoDetalleInfoProductoBE As CertificadoDetalleInfoProductoBE = Nothing
    'Private _CertificadoDetalleInfoProductoBL As _CertificadoDetalleInfoProductoBL = Nothing


    Public Function Listar_IdProducto(ByVal IdProducto As Integer) As List(Of ValoresBE)
        '_DetalleInfoProductoBE.IdInfoProducto = IdInfoProducto
        Return Nothing
    End Function

    Public Sub New()
        _CertificadoDetalleInfoProductoBE = New CertificadoDetalleInfoProductoBE
        '_DetalleInfoProductoBL = New DetalleInfoProductoBL
    End Sub
End Class
#End Region

#Region "Usuarios"

'Partial Public Class Usuario

'    Private _UsuarioBE As Global.DataEntry.Login.UsuarioBE
'    Private _UsuarioBL As Global.DataEntry.Login.UsuarioBL

'    Public Sub New()
'        _UsuarioBE = New Global.DataEntry.Login.UsuarioBE
'        _UsuarioBL = New Global.DataEntry.Login.UsuarioBL
'    End Sub

'    Public Function Listar_TipoUsuario(ByVal IdProyecto As Integer) As List(Of Global.DataEntry.Login.UsuarioBE)
'        Return CType(_UsuarioBL.Listar_TipoUsuario(IdProyecto), List(Of Global.DataEntry.Login.UsuarioBE))
'    End Function

'    Public Sub Agregar(ByVal IdUsuario As String, ByVal Nombre As String, ByVal ApellidoPaterno As String, ByVal ApellidoMaterno As String, ByVal Clave As String, ByVal Correo As String, ByVal IdRol As Integer, ByVal IdProyecto As Integer)
'        With _UsuarioBE
'            .IdUsuario = IdUsuario
'            .Nombre = Nombre
'            .ApellidoPaterno = ApellidoPaterno
'            .ApellidoMaterno = ApellidoMaterno
'            .Clave = Clave
'            .Correo = Correo
'            .IdRol = IdRol
'            .IdProyecto = IdProyecto
'        End With
'        _UsuarioBL.Agregar(_UsuarioBE)
'    End Sub

'    Public Sub Modificar(ByVal IdUsuario As String, ByVal Nombre As String, ByVal ApellidoPaterno As String, ByVal ApellidoMaterno As String, ByVal Clave As String, ByVal Correo As String, ByVal IdRol As Integer, ByVal IdProyecto As Integer)
'        With _UsuarioBE
'            .IdUsuario = IdUsuario
'            .Nombre = Nombre
'            .ApellidoPaterno = ApellidoPaterno
'            .ApellidoMaterno = ApellidoMaterno
'            .Clave = Clave
'            .Correo = Correo
'            .IdRol = IdRol
'            .IdProyecto = IdProyecto
'        End With
'        _UsuarioBL.Modificar(_UsuarioBE)
'    End Sub

'    Public Sub Eliminar(ByVal IdUsuario As String)
'        _UsuarioBE.IdUsuario = IdUsuario
'        _UsuarioBL.Eliminar(_UsuarioBE)
'    End Sub

'    Public Function Buscar(ByVal IdUsuario As String) As Global.DataEntry.Login.UsuarioBE
'        Return _UsuarioBL.Buscar(IdUsuario)
'    End Function



'End Class


#End Region

#Region "UsuarioConfig"

Partial Public Class UsuarioConfig

    Private _UsuarioConfigBE As UsuarioConfigBE = Nothing
    Private _UsuarioConfigBL As UsuarioConfigBL = Nothing

    Public Function Agregar(ByVal IdUsuario As String, ByVal IdProducto As String, ByVal IdAseguradora As String) As String
        _UsuarioConfigBE.IdUsuario = IdUsuario
        _UsuarioConfigBE.IdProducto = IdProducto
        _UsuarioConfigBE.IdAseguradora = IdAseguradora
        Return _UsuarioConfigBL.Agregar(_UsuarioConfigBE)
    End Function

    Public Function Eliminar(ByVal IdUsuario As String, ByVal IdProducto As String, ByVal IdAseguradora As String) As String
        _UsuarioConfigBE.IdUsuario = IdUsuario
        _UsuarioConfigBE.IdProducto = IdProducto
        _UsuarioConfigBE.IdAseguradora = IdAseguradora
        Return _UsuarioConfigBL.Eliminar(_UsuarioConfigBE)
    End Function

    Public Function Listar_Id(ByVal IdUsuario As String) As List(Of UsuarioConfigBE)
        _UsuarioConfigBE.IdUsuario = IdUsuario
        Return _UsuarioConfigBL.Listar_IdUsuario(_UsuarioConfigBE)
    End Function

    Public Sub New()
        _UsuarioConfigBE = New UsuarioConfigBE
        _UsuarioConfigBL = New UsuarioConfigBL
    End Sub

End Class
#End Region