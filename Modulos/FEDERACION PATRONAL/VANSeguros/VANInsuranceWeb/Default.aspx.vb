﻿Imports System.Xml
Imports AjaxControlToolkit
Imports VAN.InsuranceWeb.BL
Imports VAN.InsuranceWeb.BE
Imports System.IO
Imports VAN.InsuranceWeb.BF
Imports WSInvoker = VANWebServiceInvoker

Partial Public Class WebForm2
    Inherits PaginaBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not ValidarAcceso(Page.AppRelativeVirtualPath) Then
                Response.Redirect("~/Login.aspx", False)
            End If
            If ValidarEstadoClaveUsuario() Then
                Response.Redirect("~/Seguridad/CambiarClave.aspx", False)
            End If
            CargarTipoDoc()
        End If
    End Sub

    'Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
    '    Response.Redirect("Proceso/EmisionCertificado.aspx?ID=5112")
    'End Sub

    Private Sub CargarTipoDoc()
        Dim oBL As New BLTipoDocumento
        ddlTipoDocCliente.DataSource = oBL.Seleccionar()
        ddlTipoDocCliente.DataTextField = "Nombre"
        ddlTipoDocCliente.DataValueField = "idTipoDocumento"
        ddlTipoDocCliente.DataBind()
        ddlTipoDocCliente.SelectedValue = "L"
    End Sub

    Private Sub CargarCrossSelling()
        Dim oBEUsuario As BEUsuario = DirectCast(Session(SESSION_BEUSUARIO), BEUsuario)
        Dim objBECrossSellingSearch As New BECrossSelling
        Dim objMedioPagoCliente As New BEMedioPagoCliente

        With objBECrossSellingSearch

            .SIDUsuario = oBEUsuario.IDUsuario
            .SIDProducto = Nothing
            .SIDTipoDocumento = ddlTipoDocCliente.SelectedValue
            .SNumeroDocumento = txtNroDocCliente.Text
            .SIDMedioPagoUtilizado = Nothing
            .SNumeroDeCuentaUtilizado = Nothing
            .SIDMonedaCobroUtilizado = Nothing
        End With

        ccCrossSelling.DrawCrossSellingPanel(objBECrossSellingSearch, False)
    End Sub

    Private Function SetDataClient(Cliente As WSInvoker.Model.Cliente) As BECliente
        Dim oBECliente As New BECliente

        oBECliente.TipoDocumento = ddlTipoDocCliente.SelectedItem.Text
        oBECliente.NroDocumento = txtNroDocCliente.Text.Trim
        oBECliente.NombreCompleto = If(Cliente.Apellido.Length > 0, Cliente.Apellido(0).ToString, String.Empty) + " " + If(Cliente.Apellido.Length > 1, Cliente.Apellido(1).ToString, String.Empty) + " " + If(Cliente.Nombre.Length > 0, Cliente.Nombre(0).ToString, String.Empty) + " " + If(Cliente.Nombre.Length > 1, Cliente.Nombre(1).ToString, String.Empty)

        If Not Cliente.Domicilios Is Nothing Then
            If Cliente.Domicilios.Length > 0 Then
                oBECliente.Direccion = Cliente.Domicilios(0).Calle + " " + Cliente.Domicilios(0).Numero + " " + Cliente.Domicilios(0).Piso
            End If
        End If

        Return oBECliente
    End Function

    Protected Sub btn_find_Click(sender As Object, e As EventArgs)

        Dim oBECliente As New BECliente
        Dim lstClient As New List(Of BECliente)
        Dim SrvInvoker As New WSInvoker.WebServiceInvoker()
        Dim ClientITAU As New WSInvoker.Model.Cliente()
        Dim oBLEquivalencia As New BLListaEquivalencia
        Dim IDTipoDocumento As String = oBLEquivalencia.getValorEquivalencia(5214, "TipoDocumento", ddlTipoDocCliente.SelectedValue).ValorEquivalencia
        Dim NroDocumento As String = txtNroDocCliente.Text.Trim

        ClientITAU = SrvInvoker.GetClient(IDTipoDocumento, NroDocumento, 0)

        If ClientITAU.EstadoRespuesta = WSInvoker.Constant.Constant.RESPONSE_OK Then
            If ClientITAU.Codigo <> String.Empty Then
                lstClient.Add(SetDataClient(ClientITAU))
            End If
        End If

        gvBusqueda.DataSource = lstClient
        gvBusqueda.DataBind()
        CargarCrossSelling()

        ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "ClearCrossSelling();", True)
    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click

    End Sub

    Protected Sub btnGuardarSolicitudContacto_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardarSolicitudContacto.Click

        Dim pIdTipoDocumentoContacto As String = ddlTipoDocumentoContacto.SelectedValue
        Dim pDocumentoContacto As String = txtDocumentoContacto.Text.Trim()
        Dim pEmailContacto As String = txtEmailContacto.Text.Trim()
        Dim pTelefonoContacto As String = txtTelefonoContacto.Text.Trim()
        Dim pCelularContacto As String = txtCelularContacto.Text.Trim()
        Dim pObservaciones As String = txtObservacionesContacto.Text.Trim()
        Dim pTipoFrecuenciaContacto As String = ddlFrecuenciaContacto.SelectedValue
        Dim pValorFrecuencia As String = String.Empty

        If (pTipoFrecuenciaContacto = "F") Then
            pValorFrecuencia = txtValorDateFrecuenciaContacto.Text.Trim()
        Else
            pValorFrecuencia = txtValorFrecuenciaContacto.Text.Trim()
        End If


        If (String.IsNullOrEmpty(pTelefonoContacto) AndAlso String.IsNullOrEmpty(pCelularContacto) AndAlso String.IsNullOrEmpty(pEmailContacto)) Then
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('Debe indicar por lo menos algun medio de contacto (Telefono, Celular o Email)');", True)
            Exit Sub
        End If

        If (String.IsNullOrEmpty(pObservaciones)) Then
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('Por favor indique las observaciones');", True)
            Exit Sub
        End If

        If (String.IsNullOrEmpty(txtNombreContacto1.Text.Trim())) Then
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('Por favor indique el Primer Nombre');", True)
            Exit Sub
        End If

        If (String.IsNullOrEmpty(txtApellidoContacto1.Text.Trim())) Then
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('Por favor indique el Apellido Paterno');", True)
            Exit Sub
        End If


        Dim objBESolicitudContacto As New BESolicitudContactoCliente
        Dim objBLContactoCliente As New BLSolicitudConctactoCliente

        Dim oBEUsuario As BEUsuario = DirectCast(Session(SESSION_BEUSUARIO), BEUsuario)
        With objBESolicitudContacto
            .IdTipoDocumento = pIdTipoDocumentoContacto
            .ccCliente = pDocumentoContacto
            .Nombre1 = txtApellidoContacto1.Text.Trim()
            .Nombre2 = txtApellidoContacto2.Text.Trim()
            .Apellido1 = txtApellidoContacto1.Text.Trim()
            .Apellido2 = txtApellidoContacto2.Text.Trim()
            .Email = pEmailContacto
            .Telefono = pTelefonoContacto
            .Celular = pCelularContacto
            .IDCertificadoOrigen = Nothing
            .Observaciones = pObservaciones
            .IDUsuarioCreacion = User.Identity.Name
            .IDInformador = oBEUsuario.IDInformador
            .CodigoTipoFrecuencia = pTipoFrecuenciaContacto
            .ValorFrecuenciaContacto = pValorFrecuencia
            Dim productosdeInteres As String = hfIDProductoSelectCrossSelling.Value
            If (Not String.IsNullOrEmpty(productosdeInteres)) Then
                If (productosdeInteres <> "0") Then
                    Dim objBeSolicitudProducto As BESolicitudContactoProducto
                    Dim seleccionados() As String = productosdeInteres.Split(",")
                    For Each p As String In seleccionados
                        If (Not String.IsNullOrEmpty(p)) Then
                            If (IsNumeric(p)) Then
                                objBeSolicitudProducto = New BESolicitudContactoProducto
                                objBeSolicitudProducto.IdProducto = CInt(p)
                                .Productos.Add(objBeSolicitudProducto)
                            End If
                        End If
                    Next
                End If
            End If
        End With

        If (objBLContactoCliente.Guardar(objBESolicitudContacto)) Then
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Close", "CloseSucessContactSave()", True)
        Else
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('Error: no se pudo registrar la solicitud de contacto');", True)
        End If

    End Sub

    Protected Sub btnListaSolicitudesContacto_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnListaSolicitudesContacto.Click
        Dim oBL As New BLSolicitudConctactoCliente
        Dim idProducto As Integer = CInt(hfIDProductoSelectCrossSelling.Value)
        Dim pIdTipoDocumentoContacto As String = ddlTipoDocCliente.SelectedValue
        Dim ccCliente As String = txtNroDocCliente.Text.Trim
        If (Not String.IsNullOrEmpty(ccCliente)) Then
            gvSolicitudesContacto.DataSource = oBL.ListarSolicitudes(idProducto, pIdTipoDocumentoContacto, ccCliente)
            gvSolicitudesContacto.DataBind()
        End If
    End Sub
End Class