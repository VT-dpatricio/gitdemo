﻿Imports VAN.InsuranceWeb.BL
Imports VAN.InsuranceWeb.BE
Partial Public Class Producto1
    Inherits PaginaBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not ValidarAcceso(Page.AppRelativeVirtualPath) Then
                Response.Redirect("~/Login.aspx", False)
            End If
            If ValidarEstadoClaveUsuario() Then
                Response.Redirect("~/Seguridad/CambiarClave.aspx", False)
            End If


            CargarEntidad(ddlEntidad)
            listarProducto()
            Dim r As String = Request.QueryString("r")
            If r <> "" Then
                If r = 1 Then
                    lblResultado.Text = "Producto Registrado con Éxito"
                    lblResultado.ForeColor = Drawing.Color.Blue
                Else
                    lblResultado.Text = "Producto no se registo, favor de revisar."
                    lblResultado.ForeColor = Drawing.Color.Red
                End If
            End If
        End If
    End Sub
    Private Sub listarProducto()
        Dim oBLProducto As New BLProducto
        gvListaProducto.SelectedIndex = -1
        gvListaProducto.DataSource = oBLProducto.Seleccionar(txtBuscar.Text.Trim, CInt(ddlEntidad.SelectedValue))
        gvListaProducto.DataBind()
    End Sub

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        listarProducto()
    End Sub

    Protected Sub ddlEntidad_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlEntidad.SelectedIndexChanged
        listarProducto()
    End Sub

    Protected Sub gvListaProducto_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListaProducto.PageIndexChanging
        gvListaProducto.PageIndex = e.NewPageIndex
        listarProducto()
    End Sub

    Protected Sub gvListaProducto_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gvListaProducto.SelectedIndexChanged
        Dim pIDProducto As Integer = Convert.ToInt32(gvListaProducto.SelectedDataKey.Item(0).ToString)
        ScriptManager.RegisterStartupScript(Me, MyBase.GetType, "frmDetalle", "DetalleProducto(" & pIDProducto & ");", True)
    End Sub

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        Response.Redirect("~/MantBAIS/ProductoRegistro.aspx")
    End Sub
End Class