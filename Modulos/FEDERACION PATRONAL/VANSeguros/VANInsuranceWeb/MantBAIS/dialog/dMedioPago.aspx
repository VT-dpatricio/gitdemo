﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="dMedioPago.aspx.vb" Inherits="VANInsurance.dMedioPago" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>BNP PARIBAS CARDIF - Venta Seguros</title>
    <link href="../../Estilos/Default.css" type="text/css" rel="stylesheet" />
    <link href="../../Estilos/General.css" type="text/css" rel="stylesheet" />
    <link href="../../Estilos/Menu.css" type="text/css" rel="stylesheet" />

    <script src="../../Scripts/General.js" type="text/javascript"></script>

    <script type="text/javascript">
       function CerrarPagina(page) {
           var parentWindow = window.parent;
           parentWindow.OcultarPCModal(page);
       }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div class="DivHeight096">
        <table cellspacing="4" cellpadding="0" border="0" style="width: 580px" class="BodyMP">
            <tr>
                <td colspan="4" class="Titulo">
                    MEDIO DE PAGO - MONEDA
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Panel ID="Panel1" runat="server" GroupingText="Datos Basicos" Width="90%">
                        <table width="100%">
                            <tr>
                                <td align="left">
                                    Medio Pago :
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlMedioPago" runat="server" CssClass="Body" DataSourceID="sqlMedioPago"
                                        DataTextField="nombre" DataValueField="idMedioPago" Width="200px">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="sqlMedioPago" runat="server" ConnectionString="<%$ ConnectionStrings:BAIS %>"
                                        SelectCommand="select idMedioPago, Nombre from MedioPago"></asp:SqlDataSource>
                                    <asp:RequiredFieldValidator ID="rfvddlMedioPago" runat="server" ErrorMessage="(*)"
                                        ControlToValidate="ddlMedioPago"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left">
                                    Moneda Cobro :
                                </td>
                                <td colspan="3" align="left">
                                    <asp:DropDownList ID="ddlMonedaCobro" runat="server" CssClass="Body" DataSourceID="sqlMoneda"
                                        DataTextField="nombre" DataValueField="idMoneda" Width="200px">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="sqlMoneda" runat="server" ConnectionString="<%$ ConnectionStrings:BAIS %>"
                                        SelectCommand="select idMoneda, Nombre from Moneda"></asp:SqlDataSource>
                                    <asp:RequiredFieldValidator ID="rfvddlMonedaCobro" runat="server" ErrorMessage="(*)"
                                        ControlToValidate="ddlMonedaCobro"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="right">
                                    <asp:Button ID="btnGrabar" runat="server" Text="Grabar" class="ButtonGrid" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
