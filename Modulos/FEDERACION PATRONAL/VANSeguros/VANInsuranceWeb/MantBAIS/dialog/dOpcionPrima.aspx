﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="dOpcionPrima.aspx.vb"
    Inherits="VANInsurance.dOpcionPrima" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>BNP PARIBAS CARDIF - Venta Seguros</title>
    <link href="../../Estilos/Default.css" type="text/css" rel="stylesheet" />
    <link href="../../Estilos/General.css" type="text/css" rel="stylesheet" />
    <link href="../../Estilos/Menu.css" type="text/css" rel="stylesheet" />

    <script src="../../Scripts/General.js" type="text/javascript"></script>

    <script type="text/javascript">
        function CerrarPagina(page) {
            var parentWindow = window.parent;
            parentWindow.OcultarPCModal(page);
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="DivHeight096">
        <table cellspacing="4" cellpadding="0" border="0" style="width: 580px" class="BodyMP">
            <tr>
                <td colspan="4" class="Titulo">
                    PLAN - PRIMA
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Panel ID="Panel1" runat="server" GroupingText="Datos Basicos" Width="90%">
                        <table width="100%">
                            <tr>
                                <td align="left">
                                    Plan :
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlOpcion" runat="server" CssClass="Body" DataTextField="opcion"
                                        DataValueField="opcion" Width="100px">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvddlOpcion" runat="server" ErrorMessage="(*)" ControlToValidate="ddlOpcion"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left">
                                    Frecuencia :
                                </td>
                                <td colspan="3" align="left">
                                    <asp:DropDownList ID="ddlFrecuencia" runat="server" CssClass="Body" DataSourceID="sqlFrecuencia"
                                        DataTextField="nombre" DataValueField="idFrecuencia" Width="100px">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="sqlFrecuencia" runat="server" ConnectionString="<%$ ConnectionStrings:BAIS %>"
                                        SelectCommand="select idFrecuencia, nombre from Frecuencia"></asp:SqlDataSource>
                                    <asp:RequiredFieldValidator ID="rfvddlFrecuencia" runat="server" ErrorMessage="(*)"
                                        ControlToValidate="ddlFrecuencia"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    Prima :
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtPrima" runat="server" Width="70" MaxLength="9"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtPrima" runat="server" ErrorMessage="(*)" ControlToValidate="txtPrima"></asp:RequiredFieldValidator>
                                    <%--<asp:RegularExpressionValidator ID="revtxtPrima" runat="server" ErrorMessage="(*)" ControlToValidate="txtPrima" ValidationExpression="[0-9]"></asp:RegularExpressionValidator>--%>
                                </td>
                                <td align="left">
                                    Activo :
                                </td>
                                <td colspan="3" align="left">
                                    <asp:CheckBox ID="cbActivo" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    Monto Asegurado :
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtMontoAsegurado" runat="server" Width="70" MaxLength="9"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtMontoAsegurado" runat="server" ErrorMessage="(*)"
                                        ControlToValidate="txtMontoAsegurado"></asp:RequiredFieldValidator>
                                    <%--<asp:RegularExpressioValidator ID="revtxtMontoAsegurado" runat="server" ErrorMessage="(*)" ControlToValidate="txtMontoAsegurado" ValidationExpression="[0-9]"></asp:RegularExpressionValidator>--%>
                                </td>
                                <td align="left">
                                    Moneda Prima :
                                </td>
                                <td colspan="3" align="left">
                                    <asp:DropDownList ID="ddlMonedaPrima" runat="server" CssClass="Body" DataSourceID="sqlMoneda"
                                        DataTextField="nombre" DataValueField="idMoneda" Width="100px">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="sqlMoneda" runat="server" ConnectionString="<%$ ConnectionStrings:BAIS %>"
                                        SelectCommand="select idMoneda, Nombre from Moneda"></asp:SqlDataSource>
                                    <asp:RequiredFieldValidator ID="rfvddlMonedaPrima" runat="server" ErrorMessage="(*)"
                                        ControlToValidate="ddlMonedaPrima"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    Edad Mininma :
                                </td>
                                <td align="left">
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtEdadMinima" runat="server" Width="50" MaxLength="4"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:RequiredFieldValidator ID="rfvtxtEdadMinima" runat="server" ErrorMessage="(*)"
                                        ControlToValidate="txtEdadMinima"></asp:RequiredFieldValidator>
                                    <%--<asp:RegularExpressionValidator ID="revtxtEdadMinima" runat="server" ErrorMessage="(*)" ControlToValidate="txtEdadMinima" ValidationExpression="[0-9]"></asp:RegularExpressionValidator>--%>
                                </td>
                                <td align="left">
                                    Edad Maxima :
                                </td>
                                <td colspan="3" align="left">
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtEdadMaxima" runat="server" Width="50" MaxLength="4"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:RequiredFieldValidator ID="rfvtxtEdadMaxima" runat="server" ErrorMessage="(*)"
                                        ControlToValidate="txtEdadMaxima"></asp:RequiredFieldValidator>
                                    <%--<asp:RegularExpressionValidator ID="revtxtEdadMaxima" runat="server" ErrorMessage="(*)" ControlToValidate="txtEdadMaxima" ValidationExpression="[0-9]"></asp:RegularExpressionValidator>--%>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="left">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:CheckBox ID="cbSinEdad" runat="server" Text="La prima, no toma en cuenta las edades."
                                                AutoPostBack="true" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="right">
                                    <asp:Button ID="btnGrabar" runat="server" Text="Grabar" class="ButtonGrid" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
