﻿Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.BL
Partial Public Class dProductoDetalle
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            MostrarData(Request.QueryString("Id"))
        Catch ex As Exception
        End Try
    End Sub

    Sub MostrarData(ByVal pIDProducto As Integer)

        Dim oBLProducto As New BLProducto
        Dim oBEProducto As BEProducto

        oBEProducto = oBLProducto.Seleccionar(pIDProducto)
        txtCodigoProducto.Text = oBEProducto.IdProducto
        txtAsegurador.Text = oBEProducto.Asegurador
        txtEntidad.Text = oBEProducto.Entidad
        txtNombre.Text = oBEProducto.Nombre
        txtTipoProducto.Text = oBEProducto.TipoProducto

        cbActivo.Checked = oBEProducto.Activo

        txtMaxAsegurados.Text = oBEProducto.MaxAsegurados
        cbNacReq.Checked = oBEProducto.NacimientoRequerido
        cbValParsco.Checked = oBEProducto.ValidarParentesco
        cbOblCtaHabiente.Checked = oBEProducto.ObligaCuentaHabiente

        txtMaxEdadPerm.Text = oBEProducto.EdadMaxPermanencia
        txtMaxDiasAnu.Text = oBEProducto.DiasAnulacion
        cbGeneracobro.Checked = oBEProducto.GeneraCobro

        cbVigCobro.Checked = oBEProducto.VigenciaCobro
        txtMaxPolizasAseg.Text = oBEProducto.MaxPolizasAsegurado
        txtMaxPolizasCtaHb.Text = oBEProducto.MaxPolizasCuentahabiente
        txtMaxPolizasRegalo.Text = oBEProducto.MaxPolizasRegalo

        txtMaxCtaTitular.Text = oBEProducto.MaxCuentasTitular
        txtMaxPolizasSDNI.Text = oBEProducto.MaxPolizasAseguradoSinDNI
        cbValDirCer.Checked = oBEProducto.ValidaDireccionCer
        cbValDirAseg.Checked = oBEProducto.ValidaDireccionAse
        cbCtaHbAsegurado.Checked = oBEProducto.CuentaHabienteigualAsegurado


    End Sub


End Class