﻿Imports VAN.InsuranceWeb.BE
Partial Public Class dInfoProducto
    Inherits System.Web.UI.Page

#Region " Propiedades "

    ReadOnly Property IdPosicion() As Integer
        Get
            Return Request.QueryString("Id")
        End Get
    End Property

    Dim _Entity As New InfoProductoBE
    Public Property Entity() As InfoProductoBE
        Get
            Return _Entity
        End Get
        Set(ByVal value As InfoProductoBE)
            _Entity = value
        End Set
    End Property


#End Region

#Region " Eventos "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Expires = 0
        'btnGrabar.Attributes.Add("OnClick", "CerrarPagina('InfoProducto');")
        If Not Page.IsPostBack Then
            If IdPosicion <> 0 Then
                Entity = DirectCast(Session("ListInfoProductoBE"), List(Of InfoProductoBE)).Item(Request.QueryString("Id") - 1)
                txtNombre.Text = Entity.nombre.ToUpper
                txtDescripcion.Text = Entity.Descripcion.ToUpper
                ddlTipoDato.SelectedValue = Entity.idTipoDato
                'ddlTipoDato.SelectedItem.Text = Entity.TipoDato
                cbObligatorio.Checked = Entity.obligatorio
            End If
        End If

    End Sub

    Private Sub btnGrabar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        Try
            Entity.nombre = txtNombre.Text
            Entity.descripcion = txtDescripcion.Text
            Entity.idTipoDato = ddlTipoDato.SelectedValue
            Entity.TipoDato = ddlTipoDato.SelectedItem.Text
            Entity.obligatorio = cbObligatorio.Checked


            If IdPosicion = 0 Then
                DirectCast(Session("ListInfoProductoBE"), List(Of InfoProductoBE)).Add(Entity)
            Else
                DirectCast(Session("ListInfoProductoBE"), List(Of InfoProductoBE)).Item(IdPosicion - 1) = Entity
            End If
            OrdernarLista()
            Dim strScript As String = "<Script language=""JavaScript"">CerrarPagina('InfoProducto');</Script>"
            ClientScript.RegisterStartupScript(GetType(String), "clientScript", strScript)
        Catch ex As Exception

        End Try
    End Sub

    Sub OrdernarLista()
        For i As Integer = 0 To DirectCast(Session("ListInfoProductoBE"), List(Of InfoProductoBE)).Count - 1
            DirectCast(Session("ListInfoProductoBE"), List(Of InfoProductoBE)).Item(i).Id = i + 1
        Next
    End Sub

#End Region


End Class