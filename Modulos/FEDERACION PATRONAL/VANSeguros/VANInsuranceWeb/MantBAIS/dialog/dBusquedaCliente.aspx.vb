﻿Public Partial Class dBusquedaCliente
    Inherits System.Web.UI.Page

    Private _departamento As String
    Private _provincia As String
    Private _distrito As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim nombre As String = ""
        Response.Expires = 0
        If Not Page.IsPostBack Then
            nombre = Request.QueryString("p")
            lblNombreDocumento.Text = nombre
        End If
    End Sub

    Private Sub btnBuscar1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar1.Click
        Try
            Dim campos As Hashtable
            Dim data As String = LecturaData()


            Dim msnAlert As String = ""


            If data Is Nothing Then msnAlert = "El hay busqueda."
            If (data.Trim = "") Then
                msnAlert = "No existe DNI, favor de realizar otra busqueda."
            ElseIf (data.Length < 255) Then
                msnAlert = "No hay resultado tras esta busqueda."
            ElseIf (data.Contains("DNI no existe en nuestra")) Then
                msnAlert = "No existe DNI, favor de realizar otra busqueda."
            ElseIf (data.Contains("Cliente no tiene cuentas de ahorros o tarjetas")) Then
                msnAlert = "Cliente no tiene cuentas de ahorros o tarjetas."
            End If

            If msnAlert <> "" Then
                Dim strScript As String = "<Script language=""JavaScript"">alert('" & msnAlert & "');</Script>"
                ClientScript.RegisterStartupScript(GetType(String), "clientScript", strScript)
                Exit Sub
            End If


            campos = splitRecaudo(data)
            MostarData(campos)

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub


    Function LecturaData() As String
        'Dim envio As String = txtNroDocumento.Text
        'Dim retorno As String = ""
        'Dim fecha As String = String.Format("{0:ddmmyyyy}", Date.Now)
        'Dim hora As String = String.Format("{0:HHMMss}", Date.Now)
        'Dim posicion As Integer = 0
        'Try
        '    Dim ws As New BAISConsultar.Consulta
        '    retorno = ws.ConsultarCliente(envio)
        'Catch ex As Exception
        '    lblError.Text = "No hay conexion con el servicio." & vbCrLf & ex.Message
        'End Try

        'Return retorno
    End Function

    Function splitRecaudo(ByVal cadena As String) As Hashtable
        Dim nroDetalle As Integer = cadena.Substring(252, 2).ToString.Trim()
        Dim ltipo As Integer = 254
        Dim lmon As Integer = 260
        Dim lnro As Integer = 263
        Dim lfecha As Integer = 279


        Dim camposTrama As New Hashtable()
        camposTrama.Add("Identificador_trace", cadena.Substring(0, 6).ToString.Trim())
        camposTrama.Add("NroDNI", cadena.Substring(6, 19).ToString.Trim())
        camposTrama.Add("ApellidoPaterno", cadena.Substring(25, 25).ToString.Trim())
        camposTrama.Add("ApellidoMaterno", cadena.Substring(50, 25).ToString.Trim())
        camposTrama.Add("PrimerNombre", cadena.Substring(75, 20).ToString.Trim())
        camposTrama.Add("SegundoNombre", cadena.Substring(95, 30).ToString.Trim())
        camposTrama.Add("Direccion", cadena.Substring(125, 70).ToString.Trim())
        camposTrama.Add("CodigoDistrito", cadena.Substring(195, 2).ToString.Trim())
        camposTrama.Add("CódigoProvincia", cadena.Substring(197, 2).ToString.Trim())
        camposTrama.Add("CodigoDepartamento", cadena.Substring(199, 2).ToString.Trim())
        camposTrama.Add("Telefono", cadena.Substring(201, 12).ToString.Trim())
        camposTrama.Add("Ocupacion", cadena.Substring(213, 15).ToString.Trim())
        camposTrama.Add("LugarNacimiento", cadena.Substring(228, 16).ToString.Trim())
        camposTrama.Add("FechaNacimiento", cadena.Substring(244, 8).ToString.Trim())
        camposTrama.Add("CantidaddeCuentas", cadena.Substring(252, 2).ToString.Trim())

        For i As Integer = 1 To nroDetalle

            If (i > 1) Then
                ltipo = ltipo + 33
                lmon = lmon + 33
                lnro = lnro + 33
                lfecha = lfecha + 33
            End If


            camposTrama.Add("TipoCuenta" & i, cadena.Substring(ltipo, 6).ToString.Trim())
            camposTrama.Add("MonedaCuenta" & i, cadena.Substring(lmon, 3).ToString.Trim())
            camposTrama.Add("NroCuentaNroTarjeta" & i, cadena.Substring(lnro, 16).ToString.Trim())
            camposTrama.Add("FechaVencimiento" & i, cadena.Substring(lfecha, 8).ToString.Trim())
        Next


        Return camposTrama
    End Function

    Private Sub MostarData(ByVal Data As Hashtable)

        txtFecNac.Text = Data("FechaNacimiento").ToString()
        txtFecNac.Text = txtFecNac.Text.Substring(6, 2) + "/" + txtFecNac.Text.Substring(4, 2) + "/" + txtFecNac.Text.Substring(0, 4)
        txtNombre1.Text = Data("PrimerNombre").ToString()
        txtNombre2.Text = Data("SegundoNombre").ToString()
        txtApellido1.Text = Data("ApellidoPaterno").ToString()
        txtApellido2.Text = Data("ApellidoMaterno").ToString()
        txtTelefono.Text = Data("Telefono").ToString()
        txtOcupacion.Text = Data("Ocupacion").ToString()
        txtDireccion.Text = Data("Direccion").ToString()

        _departamento = Data("CodigoDepartamento").ToString()
        _provincia = Data("CódigoProvincia").ToString()
        _distrito = Data("CodigoDistrito").ToString()
        'txtCodigoINEI.Text = _departamento & _provincia & _distrito

        lbCuentas.Items.Clear()

        Dim i As Integer = 0
        i = Data("CantidaddeCuentas").ToString()
        For x As Integer = 0 To i - 1
            Dim dato As String = Data("TipoCuenta" & (x + 1)).ToString() & " - " & Data("MonedaCuenta" & (x + 1)).ToString() & " - " & Data("NroCuentaNroTarjeta" & (x + 1)).ToString() & " - " & Data("FechaVencimiento" & (x + 1)).ToString()
            lbCuentas.Items.Add(dato)
        Next
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click

        Dim cadena As String = ""

        cadena += txtNroDocumento.Text & "|"
        cadena += txtNombre1.Text & "|"
        cadena += txtNombre2.Text & "|"
        cadena += txtApellido1.Text & "|"
        cadena += txtApellido2.Text & "|"
        cadena += txtTelefono.Text & "|"
        cadena += txtOcupacion.Text & "|"
        cadena += txtDireccion.Text & "|"
        cadena += _departamento & "|"
        cadena += _provincia & "|"
        cadena += _distrito & "|"
        cadena += txtFecNac.Text & "|"

        Dim tarjeta As String = lbCuentas.SelectedItem.Text
        Dim cSplit As Char() = "-"
        Dim splittarjeta() = tarjeta.Split(cSplit, 4)

        cadena += splittarjeta(0).ToString().Trim() & "|"
        cadena += splittarjeta(1).ToString.Trim() & "|"
        cadena += splittarjeta(2).ToString.Trim() & "|"
        cadena += splittarjeta(3).ToString.Substring(5, 2) + "/" + splittarjeta(3).ToString.Trim().Substring(0, 4) & "|"


        Dim strScript As String = "<Script language=""JavaScript"">CerrarPagina('" & cadena & "');</Script>"
        ClientScript.RegisterStartupScript(GetType(String), "clientScript", strScript)
    End Sub
End Class