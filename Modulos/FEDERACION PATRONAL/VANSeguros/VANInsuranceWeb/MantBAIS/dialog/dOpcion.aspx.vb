﻿Imports VAN.InsuranceWeb.BE
Partial Public Class dOpcion
    Inherits System.Web.UI.Page

#Region " Propiedades "


    ReadOnly Property IdPosicion() As Integer
        Get
            Return Request.QueryString("Id")
        End Get
    End Property

    Dim _Entity As New OpcionBE
    Public Property Entity() As OpcionBE
        Get
            Return _Entity
        End Get
        Set(ByVal value As OpcionBE)
            _Entity = value
        End Set
    End Property


#End Region

#Region " Eventos "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Expires = 0

        If Not Page.IsPostBack Then
            If IdPosicion <> 0 Then
                Entity = DirectCast(Session("ListOpcionBE"), List(Of OpcionBE)).Item(Request.QueryString("Id") - 1)
                txtCodigo.Text = Entity.opcion
                txtPuntos.Text = Entity.puntos
                txtPuntosValor.Text = Entity.puntosValor
                cbObligatorio.Checked = Entity.activo
            End If
        End If

    End Sub

    Private Sub btnGrabar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        Try
            Entity.opcion = txtCodigo.Text.ToUpper
            Entity.puntos = txtPuntos.Text.ToUpper
            Entity.puntosValor = txtPuntosValor.Text
            Entity.activo = cbObligatorio.Checked


            If IdPosicion = 0 Then

                For i As Integer = 0 To DirectCast(Session("ListOpcionBE"), List(Of OpcionBE)).Count - 1
                    If DirectCast(Session("ListOpcionBE"), List(Of OpcionBE)).Item(i).opcion = Entity.opcion Then
                        Dim msn As String = "<Script language=""JavaScript"">alert('El Plan ya se encuentra registrado.');</Script>"
                        ClientScript.RegisterStartupScript(GetType(String), "clientScript", msn)
                        Exit Sub
                    End If
                Next

                DirectCast(Session("ListOpcionBE"), List(Of OpcionBE)).Add(Entity)
            Else

                For i As Integer = 0 To DirectCast(Session("ListOpcionBE"), List(Of OpcionBE)).Count - 1
                    If (IdPosicion - 1) <> 0 AndAlso DirectCast(Session("ListOpcionBE"), List(Of OpcionBE)).Item(i).opcion = Entity.opcion Then
                        Dim msn As String = "<Script language=""JavaScript"">alert('El Plan ya se encuentra registrado.');</Script>"
                        ClientScript.RegisterStartupScript(GetType(String), "clientScript", msn)
                        Exit Sub
                    End If
                Next


                DirectCast(Session("ListOpcionBE"), List(Of OpcionBE)).Item(IdPosicion - 1) = Entity



            End If

            OrdernarLista()
            Dim strScript As String = "<Script language=""JavaScript"">CerrarPagina('opcion');</Script>"
            ClientScript.RegisterStartupScript(GetType(String), "clientScript", strScript)
           
        Catch ex As Exception

        End Try
    End Sub


    Sub OrdernarLista()
        For i As Integer = 0 To DirectCast(Session("ListOpcionBE"), List(Of OpcionBE)).Count - 1
            DirectCast(Session("ListOpcionBE"), List(Of OpcionBE)).Item(i).Id = i + 1
        Next
    End Sub

#End Region

End Class