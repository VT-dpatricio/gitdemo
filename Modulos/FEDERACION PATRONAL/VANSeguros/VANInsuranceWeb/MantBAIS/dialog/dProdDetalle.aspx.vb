﻿Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.BL
Partial Public Class dProdDetalle
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            MostrarData(Request.QueryString("Id"))
        Catch ex As Exception
            'txtMaxAsegurados.Text = ex.Message
        End Try
    End Sub

    Sub MostrarData(ByVal pIDProducto As Integer)
        Dim oBLProducto As New BLProducto
        Dim oBEProducto As BEProducto

        oBEProducto = oBLProducto.Seleccionar(pIDProducto)
        txtCodigoProducto.Text = oBEProducto.IdProducto
        txtAsegurador.Text = oBEProducto.Asegurador
        txtEntidad.Text = oBEProducto.Entidad
        txtNombre.Text = oBEProducto.Nombre
        txtTipoProducto.Text = oBEProducto.TipoProducto

        cbActivo.Checked = oBEProducto.Activo

        txtMaxAsegurados.Text = pIDProducto
        'Dim Entity As New ProductoBE
        'Entity = DirectCast(Session("ListProductoBE"), List(Of ProductoBE)).Item(i)

        'txtCodigoProducto.Text = Entity.idProducto
        'txtAsegurador.Text = Entity.NombreAsegurador
        'txtEntidad.Text = Entity.NombreEntidad
        'txtNombre.Text = Entity.Descripcion
        'txtTipoProducto.Text = Entity.idTipoProducto

        'cbActivo.Checked = Entity.activo
        'txtMaxAsegurados.Text = Entity.maxAsegurados
        'cbNacReq.Checked = Entity.nacimientoRequerido
        'cbValParsco.Checked = Entity.validarParentesco
        'cbOblCtaHabiente.Checked = Entity.obligaCuentaHabiente

        'txtMaxEdadPerm.Text = Entity.edadMaxPermanencia
        'txtMaxDiasAnu.Text = Entity.diasAnulacion
        'cbGeneracobro.Checked = Entity.generaCobro

        'cbVigCobro.Checked = Entity.vigenciaCobro
        'txtMaxPolizasAseg.Text = Entity.maxPolizasAsegurado
        'txtMaxPolizasCtaHb.Text = Entity.maxPolizasCuentahabiente
        'txtMaxPolizasRegalo.Text = Entity.maxPolizasRegalo

        'txtMaxCtaTitular.Text = Entity.maxCuentasTitular
        'txtMaxPolizasSDNI.Text = Entity.maxPolizasAseguradoSinDNI
        'cbValDirCer.Checked = Entity.validaDireccionCer
        'cbValDirAseg.Checked = Entity.validaDireccionAse
        'cbCtaHbAsegurado.Checked = Entity.cuentaHabienteigualAsegurado




    End Sub


End Class