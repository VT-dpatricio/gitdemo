﻿Imports VAN.InsuranceWeb.BE
Partial Public Class dOpcionPrima
    Inherits System.Web.UI.Page

#Region " Propiedades "

    ReadOnly Property IdPosicion() As Integer
        Get
            Return Request.QueryString("Id")
        End Get
    End Property

    Dim _Entity As New OpcionPrimaBE
    Public Property Entity() As OpcionPrimaBE
        Get
            Return _Entity
        End Get
        Set(ByVal value As OpcionPrimaBE)
            _Entity = value
        End Set
    End Property


#End Region


#Region " Cargar "

    Sub CargarSession()
        ddlOpcion.DataSource = DirectCast(Session("ListOpcionBE"), List(Of OpcionBE))
        ddlOpcion.DataBind()
    End Sub

    Sub KeyPressCajas()
        txtEdadMaxima.Attributes.Add("onkeypress", "return SoloNumeros(event);")
        txtEdadMinima.Attributes.Add("onkeypress", "return SoloNumeros(event);")
        txtMontoAsegurado.Attributes.Add("onkeypress", "return SoloNumerosPunto(event);")
        txtPrima.Attributes.Add("onkeypress", "return SoloNumerosPunto(event);")
    End Sub


#End Region



#Region " Eventos "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Expires = 0
        KeyPressCajas()


        If Not Page.IsPostBack Then
            CargarSession()
            If IdPosicion <> 0 Then
                Entity = DirectCast(Session("ListOpcionPrimaBE"), List(Of OpcionPrimaBE)).Item(Request.QueryString("Id") - 1)

                ddlOpcion.SelectedValue = Entity.opcion
                ddlFrecuencia.SelectedValue = Entity.IdFrecuencia
                txtPrima.Text = Entity.prima
                cbActivo.Checked = Entity.activo
                txtMontoAsegurado.Text = Entity.montoAsegurado
                ddlMonedaPrima.SelectedValue = Entity.idMonedaPrima
                txtEdadMinima.Text = Entity.edadMin
                txtEdadMaxima.Text = Entity.edadMax



                If Entity.edadMin = -1 Then
                    cbSinEdad.Checked = True
                    txtEdadMinima.ReadOnly = True
                    txtEdadMaxima.ReadOnly = True
                Else
                    cbSinEdad.Checked = False
                    txtEdadMinima.ReadOnly = False
                    txtEdadMaxima.ReadOnly = False
                End If

            End If
        End If


        
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        Try

            Entity.opcion = ddlOpcion.SelectedValue
            Entity.IdFrecuencia = ddlFrecuencia.SelectedValue
            Entity.Frecuencia = ddlFrecuencia.SelectedItem.Text
            Entity.prima = txtPrima.Text
            Entity.activo = cbActivo.Checked
            Entity.montoAsegurado = txtMontoAsegurado.Text
            Entity.idMonedaPrima = ddlMonedaPrima.SelectedValue
            Entity.MonedaPrima = ddlMonedaPrima.SelectedItem.Text
            Entity.edadMin = txtEdadMinima.Text
            Entity.edadMax = txtEdadMaxima.Text

            If IdPosicion = 0 Then

                For i As Integer = 0 To DirectCast(Session("ListOpcionPrimaBE"), List(Of OpcionPrimaBE)).Count - 1
                    If DirectCast(Session("ListOpcionPrimaBE"), List(Of OpcionPrimaBE)).Item(i).opcion = Entity.opcion And _
                    DirectCast(Session("ListOpcionPrimaBE"), List(Of OpcionPrimaBE)).Item(i).IdFrecuencia = Entity.IdFrecuencia Then
                        Dim msn As String = "<Script language=""JavaScript"">alert('El Plan y Frecuencia ya se encuentra registrado.');</Script>"
                        ClientScript.RegisterStartupScript(GetType(String), "clientScript", msn)
                        Exit Sub
                    End If
                Next

                DirectCast(Session("ListOpcionPrimaBE"), List(Of OpcionPrimaBE)).Add(Entity)
            Else


                For i As Integer = 0 To DirectCast(Session("ListOpcionPrimaBE"), List(Of OpcionPrimaBE)).Count - 1
                    If (IdPosicion - 1) <> 0 AndAlso DirectCast(Session("ListOpcionPrimaBE"), List(Of OpcionPrimaBE)).Item(i).opcion = Entity.opcion And _
                    DirectCast(Session("ListOpcionPrimaBE"), List(Of OpcionPrimaBE)).Item(i).IdFrecuencia = Entity.IdFrecuencia Then
                        Dim msn As String = "<Script language=""JavaScript"">alert('El Plan y Frecuencia ya se encuentra registrado.');</Script>"
                        ClientScript.RegisterStartupScript(GetType(String), "clientScript", msn)
                        Exit Sub
                    End If
                Next


                DirectCast(Session("ListOpcionPrimaBE"), List(Of OpcionPrimaBE)).Item(IdPosicion - 1) = Entity
            End If
            OrdernarLista()
            Dim strScript As String = "<Script language=""JavaScript"">CerrarPagina('OpcionPrima');</Script>"
            ClientScript.RegisterStartupScript(GetType(String), "clientScript", strScript)

        Catch ex As Exception

        End Try
    End Sub


    Private Sub cbSinEdad_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbSinEdad.CheckedChanged
        If cbSinEdad.Checked Then
            txtEdadMinima.Text = "-1"
            txtEdadMaxima.Text = "-1"
            txtEdadMinima.ReadOnly = True
            txtEdadMaxima.ReadOnly = True
        Else
            txtEdadMinima.Text = ""
            txtEdadMaxima.Text = ""
            txtEdadMinima.ReadOnly = False
            txtEdadMaxima.ReadOnly = False
        End If
    End Sub

    Sub OrdernarLista()
        For i As Integer = 0 To DirectCast(Session("ListOpcionPrimaBE"), List(Of OpcionPrimaBE)).Count - 1
            DirectCast(Session("ListOpcionPrimaBE"), List(Of OpcionPrimaBE)).Item(i).Id = i + 1
        Next
    End Sub

#End Region


    
End Class