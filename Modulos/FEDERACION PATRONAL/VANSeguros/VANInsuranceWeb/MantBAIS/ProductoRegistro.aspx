<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage2.Master"
    CodeBehind="ProductoRegistro.aspx.vb" Inherits="VANInsurance.ProductoRegistro" EnableEventValidation="false"%>
<%@ Register assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI" tagprefix="asp" %>

<%@ Register src="../Controles/Cargando.ascx" tagname="Cargando" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Estilos/Default.css" type="text/css" rel="stylesheet" />
    <link href="../Estilos/General.css" type="text/css" rel="stylesheet" />
    <link href="../Estilos/Menu.css" type="text/css" rel="stylesheet" />

    <script src="../Scripts/General.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.7.1.min.js" type="text/javascript"></script>  
    <script src="../Scripts/jquery.simplemodal.1.4.2.min.js" type="text/javascript"></script> 

    <script language="javascript" type="text/javascript">
        document.onkeydown = function(evt) { return (evt ? evt.which : event.keyCode) != 13; }
</script>
    <script type="text/javascript">

        function MostrarPCModal(ruta, pos) {
            var contentUrl = 'dialog/' + ruta + '.aspx?id=' + pos;
//            pcOpcion.contentUrl = 'dialog/' + ruta + '.aspx?id=' + pos;
//            pcOpcion.Show();

            $.modal('<iframe frameborder="0" src=' + contentUrl + ' height="400" width="650" style="border:0">', {
                closeHTML: "<a href='#' title='Cerrar' class='modal-close'><img src='../Img/cerrar.gif' alt='Cerrar' width='16' height='16'/></a>",
                containerCss: {
                    backgroundColor: "#FFF",
                    borderColor: "#2C483C",
                    height: 400,
                    padding: 0,
                    width: 650
                },
                overlayClose: false,
                appendTo: '#aspnetForm',
                persist: false
            });
        }

        function EliminarPCModal(page, pos) {

            var contentUrl = 'dialog/dEliminar.aspx?page=' + page + '&id=' + pos;
            $.modal('<iframe frameborder="0" src=' + contentUrl + ' height="400" width="650" style="border:0">', {
                closeHTML: "<a href='#' title='Cerrar' class='modal-close'><img src='../Img/cerrar.gif' alt='Cerrar' width='16' height='16'/></a>",
                containerCss: {
                    backgroundColor: "#FFF",
                    borderColor: "#2C483C",
                    height: 400,
                    padding: 0,
                    width: 650
                },
                overlayClose: false,
                appendTo: '#aspnetForm',
                persist: false,
                focus:false
            });


//            pcEliminar.contentUrl = 'dialog/dEliminar.aspx?page=' + page + '&id=' + pos;
//            pcEliminar.Show();
        }

        function OcultarEliminarPCModal(page) {
            //pcEliminar.Hide();
            //
            if (page == 'Opcion') {
                __doPostBack('<%=upOpcion.ClientID%>', '');
            } else if (page == 'OpcionPrima') {
                __doPostBack('<%=upPlanes.ClientID%>', '');
            } else if (page == 'MedioPago') {
                __doPostBack('<%=upMedioPago.ClientID%>', '');
            } else if (page == 'InfoProducto') {
                __doPostBack('<%=upInfoProducto.ClientID%>', '');
            } else if (page == 'InfoAsegurado') {
                __doPostBack('<%=upInfoAsegurado.ClientID%>', '');
            }
            $.modal.close();
        }

        function OcultarPCModal(page) {
//            pcOpcion.Hide();
            $.modal.close();
            if (page == 'opcion') {
                __doPostBack('<%=upOpcion.ClientID%>', '');
            } else if (page == 'OpcionPrima') {
                __doPostBack('<%=upPlanes.ClientID%>', '');
            } else if (page == 'MedioPago') {
                __doPostBack('<%=upMedioPago.ClientID%>', '');
            } else if (page == 'InfoProducto') {
                __doPostBack('<%=upInfoProducto.ClientID%>', '');
            } else if (page == 'InfoAsegurado') {
                __doPostBack('<%=upInfoAsegurado.ClientID%>', '');
            }
        }
function btnMedioPagoNuevo_onclick() {

}

    </script>

    <style type="text/css">
        .style1
        {
            width: 1000px;
        }
        .style2
        {
            height: 26px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenedor" runat="server">  
    <table cellspacing="0" cellpadding="0" style="width: 100%; height: 100%" border="0">
        <tr>
            <td valign="top" align="left" style="height: 5%">
                <br />
                <br />
                <br />
                <table class="style1" style="margin:auto">
                    <tr>
                        <td class="style2">
                            REGISTRO DE PRODUCTO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:HyperLink ID="HyperLink2" runat="server" 
                                NavigateUrl="~/MantBAIS/Producto.aspx">Regresar</asp:HyperLink>
                        
                        
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="style1 MarcoTabla">
                                <tr>
                                    <td class="TablaTitulo">
                                        Datos Básicos</td>
                                </tr>
                                <tr>
                                    <td>
                                <table __designer:mapid="90e" style="width: 990px">
                                    <tr __designer:mapid="90f">
                                        <td align="left" __designer:mapid="910">
                                            Codigo Producto :
                                        </td>
                                        <td align="left" __designer:mapid="911">
                                            <asp:TextBox ID="txtCodigoProducto" runat="server" MaxLength="4" Width="60px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtCodigoProducto" runat="server" ErrorMessage="(*)"
                                                ControlToValidate="txtCodigoProducto" ValidationGroup="rfvDatosBasico"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left" __designer:mapid="914">
                                            Nombre :
                                        </td>
                                        <td colspan="3" align="left" __designer:mapid="915">
                                            <asp:TextBox ID="txtNombre" runat="server" MaxLength="50" Width="400px" 
                                                Style="text-transform: uppercase"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtNombre" runat="server" ErrorMessage="(*)" ControlToValidate="txtNombre"
                                                ValidationGroup="rfvDatosBasico"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr __designer:mapid="918">
                                        <td align="left" __designer:mapid="919">
                                            Asegurador :
                                        </td>
                                        <td align="left" __designer:mapid="91a">
                                            <asp:DropDownList ID="ddlAseguradora" runat="server" CssClass="Body" DataSourceID="sqlAseguradora"
                                                DataTextField="Nombre" DataValueField="IdAsegurador" Width="200px">
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="sqlAseguradora" runat="server" ConnectionString="<%$ ConnectionStrings:BAIS %>"
                                                
                                                
                                                SelectCommand="SELECT IdAsegurador, upper(Nombre) as nombre FROM Asegurador">
                                            </asp:SqlDataSource>
                                            <asp:RequiredFieldValidator ID="rfvddlAseguradora" runat="server" ErrorMessage="(*)"
                                                ControlToValidate="ddlAseguradora" ValidationGroup="rfvDatosBasico"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left" __designer:mapid="91e">
                                            Entidad :
                                        </td>
                                        <td align="left" __designer:mapid="91f">
                                            <asp:DropDownList ID="ddlEntidad" runat="server" CssClass="Body" DataSourceID="sqlEntidad"
                                                DataTextField="Nombre" DataValueField="idEntidad" Width="200px">
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="sqlEntidad" runat="server" ConnectionString="<%$ ConnectionStrings:BAIS %>"
                                                SelectCommand="SELECT idEntidad, upper(Nombre) as nombre FROM Entidad"></asp:SqlDataSource>
                                            <asp:RequiredFieldValidator ID="rfvddlEntidad" runat="server" ErrorMessage="(*)"
                                                ControlToValidate="ddlEntidad" ValidationGroup="rfvDatosBasico"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left" __designer:mapid="923">
                                            <%--Regla Cobro :--%>
                                        </td>
                                        <td align="left" __designer:mapid="925">
                                            <%--<asp:DropDownList ID="ddlRegla" runat="server" Width="100px">
                                            </asp:DropDownList>--%>
                                        </td>
                                    </tr>
                                    <tr __designer:mapid="927">
                                        <td align="left" __designer:mapid="928">
                                            Tipo Producto :
                                        </td>
                                        <td align="left" __designer:mapid="929">
                                            <asp:DropDownList ID="ddlTipoProducto" runat="server" CssClass="Body" DataSourceID="sqlTipoProducto"
                                                DataTextField="Nombre" DataValueField="idTipoProducto" Width="200px">
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="sqlTipoProducto" runat="server" ConnectionString="<%$ ConnectionStrings:BAIS %>"
                                                
                                                
                                                SelectCommand="select idTipoProducto, upper(Nombre) as nombre from TipoProducto">
                                            </asp:SqlDataSource>
                                            <asp:RequiredFieldValidator ID="rfvddlTipoProducto" runat="server" ErrorMessage="(*)"
                                                ControlToValidate="ddlTipoProducto" ValidationGroup="rfvDatosBasico"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left" __designer:mapid="92d">
                                            Max Asegurados :
                                        </td>
                                        <td align="left" __designer:mapid="92e">
                                            <asp:TextBox ID="txtMaxAsegurados" runat="server" MaxLength="4" Width="40px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtMaxAsegurados" runat="server" ErrorMessage="(*)"
                                                ControlToValidate="txtMaxAsegurados" ValidationGroup="rfvDatosBasico"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left" __designer:mapid="931">
                                            Max Edad Perm :
                                        </td>
                                        <td align="left" __designer:mapid="932">
                                            <asp:TextBox ID="txtMaxEdadPerm" runat="server" MaxLength="4" Width="40px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtMaxEdadPerm" runat="server" ErrorMessage="(*)"
                                                ControlToValidate="txtMaxEdadPerm" ValidationGroup="rfvDatosBasico"></asp:RequiredFieldValidator>
                                            <%--<asp:RegularExpressionValidator ID="revtxtMaxEdadPerm" runat="server" ErrorMessage="(*)"
                                                ControlToValidate="txtMaxEdadPerm" ValidationExpression="[1-99]"></asp:RegularExpressionValidator>--%>
                                        </td>
                                    </tr>
                                    <tr __designer:mapid="936">
                                        <td align="left" __designer:mapid="937">
                                            Max Polizas Aseg :
                                        </td>
                                        <td align="left" __designer:mapid="938">
                                            <asp:TextBox ID="txtMaxPolizasAseg" runat="server" MaxLength="4" Width="40px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtMaxPolizasAseg" runat="server" ErrorMessage="(*)"
                                                ControlToValidate="txtMaxPolizasAseg" ValidationGroup="rfvDatosBasico"></asp:RequiredFieldValidator>
                                            <%-- <asp:RegularExpressionValidator ID="revtxtMaxPolizasAseg" runat="server" ErrorMessage="(*)"
                                                ControlToValidate="txtMaxPolizasAseg" ValidationExpression="[1-99]"></asp:RegularExpressionValidator>--%>
                                        </td>
                                        <td align="left" __designer:mapid="93c">
                                            Max Polizas Cta Hb :
                                        </td>
                                        <td align="left" __designer:mapid="93d">
                                            <asp:TextBox ID="txtMaxPolizasCtaHb" runat="server" MaxLength="4" Width="40px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtMaxPolizasCtaHb" runat="server" ErrorMessage="(*)"
                                                ControlToValidate="txtMaxPolizasCtaHb" ValidationGroup="rfvDatosBasico"></asp:RequiredFieldValidator>
                                            <%-- <asp:RegularExpressionValidator ID="revtxtMaxPolizasCtaHb" runat="server" ErrorMessage="(*)"
                                                ControlToValidate="txtMaxPolizasCtaHb" ValidationExpression="[1-99]"></asp:RegularExpressionValidator>--%>
                                        </td>
                                        <td align="left" __designer:mapid="941">
                                            Max Polizas Regalo :
                                        </td>
                                        <td align="left" __designer:mapid="942">
                                            <asp:TextBox ID="txtMaxPolizasRegalo" runat="server" MaxLength="4" Width="40px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtMaxPolizasRegalo" runat="server" ErrorMessage="(*)"
                                                ControlToValidate="txtMaxPolizasRegalo" ValidationGroup="rfvDatosBasico"></asp:RequiredFieldValidator>
                                            <%--<asp:RegularExpressionValidator ID="revtxtMaxPolizasRegalo" runat="server" ErrorMessage="(*)"
                                                ControlToValidate="txtMaxPolizasRegalo" ValidationExpression="[1-99]"></asp:RegularExpressionValidator>--%>
                                        </td>
                                    </tr>
                                    <tr __designer:mapid="946">
                                        <td align="left" __designer:mapid="947">
                                            Max Cta Titular :
                                        </td>
                                        <td align="left" __designer:mapid="948">
                                            <asp:TextBox ID="txtMaxCtaTitular" runat="server" MaxLength="4" Width="40px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtMaxCtaTitular" runat="server" ErrorMessage="(*)"
                                                ControlToValidate="txtMaxCtaTitular" ValidationGroup="rfvDatosBasico"></asp:RequiredFieldValidator>
                                            <%--  <asp:RegularExpressionValidator ID="revtxtMaxCtaTitular" runat="server" ErrorMessage="(*)"
                                                ControlToValidate="txtMaxCtaTitular" ValidationExpression="[1-99]"></asp:RegularExpressionValidator>--%>
                                        </td>
                                        <td align="left" __designer:mapid="94c">
                                            Max Polizas S/DNI :
                                        </td>
                                        <td align="left" __designer:mapid="94d">
                                            <asp:TextBox ID="txtMaxPolizasSDNI" runat="server" MaxLength="4" Width="40px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtMaxPolizasSDNI" runat="server" ErrorMessage="(*)"
                                                ControlToValidate="txtMaxPolizasSDNI" ValidationGroup="rfvDatosBasico"></asp:RequiredFieldValidator>
                                            <%--<asp:RegularExpressionValidator ID="revtxtMaxPolizasSDNI" runat="server" ErrorMessage="(*)"
                                                ControlToValidate="txtMaxPolizasSDNI" ValidationExpression="[1-9]"></asp:RegularExpressionValidator>--%>
                                        </td>
                                        <td align="left" __designer:mapid="951">
                                            <%--Cumulo Maximo :--%>
                                        </td>
                                        <td align="left" __designer:mapid="953">
                                            <%-- <asp:TextBox ID="txtCumuloMaximo" runat="server" MaxLength="10" Width="80px"></asp:TextBox>--%><%--<asp:RequiredFieldValidator ID="rfvtxtCumuloMaximo" runat="server" ErrorMessage="(*)"
                                                ControlToValidate="txtCumuloMaximo" ValidationGroup="rfvDatosBasico"></asp:RequiredFieldValidator>--%><%--  <asp:RegularExpressionValidator ID="revtxtCumuloMaximo" runat="server" ErrorMessage="(*)"
                                                ControlToValidate="txtCumuloMaximo" ValidationExpression="[0-99999999.]"></asp:RegularExpressionValidator>--%>
                                        </td>
                                    </tr>
                                    <tr __designer:mapid="957">
                                        <td align="left" __designer:mapid="958">
                                            Max Dias Anu :
                                        </td>
                                        <td align="left" __designer:mapid="959">
                                            <asp:TextBox ID="txtMaxDiasAnu" runat="server" MaxLength="4" Width="40px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtMaxDiasAnu" runat="server" ErrorMessage="(*)"
                                                ControlToValidate="txtMaxDiasAnu" ValidationGroup="rfvDatosBasico"></asp:RequiredFieldValidator>
                                            <%--<asp:RegularExpressionValidator ID="revtxtMaxDiasAnu" runat="server" ErrorMessage="(*)"
                                                ControlToValidate="txtMaxDiasAnu" ValidationExpression="[1-99]"></asp:RegularExpressionValidator>--%>
                                        </td>
                                        <td align="left" __designer:mapid="95d">
                                            <asp:CheckBox ID="cbNacReq" runat="server" Text="Fecha Nac Requerido" />
                                        </td>
                                        <td align="left" __designer:mapid="95f">
                                            <asp:CheckBox ID="cbValParsco" runat="server" Text="Valida Parentesco" />
                                        </td>
                                        <td align="left" __designer:mapid="961">
                                            <asp:CheckBox ID="cbOblCtaHabiente" runat="server" 
                                                Text="Obligacion Cta Habiente" />
                                        </td>
                                        <td align="left" __designer:mapid="963">
                                            <asp:CheckBox ID="cbGeneracobro" runat="server" Text="Genera cobro" />
                                        </td>
                                    </tr>
                                    <tr __designer:mapid="965">
                                        <td align="left" __designer:mapid="966">
                                            <asp:CheckBox ID="cbVigCobro" runat="server" Text="Vig. Cobro" />
                                        </td>
                                        <td align="left" __designer:mapid="968">
                                            <asp:CheckBox ID="cbCtaHbAsegurado" runat="server" Text="Cta Hb Asegurado" />
                                        </td>
                                        <td align="left" __designer:mapid="96a">
                                            <asp:CheckBox ID="cbActivo" runat="server" Text="Activo" />
                                        </td>
                                        <td align="left" __designer:mapid="96c">
                                            <asp:CheckBox ID="cbValDirCer" runat="server" 
                                                Text="Valida Dirreccion Certificado" />
                                        </td>
                                        <td align="left" __designer:mapid="96e">
                                            <asp:CheckBox ID="cbValDirAseg" runat="server" 
                                                Text="Valida Dirireccion Asegegurado" />
                                        </td>
                                        <td align="left" __designer:mapid="970">
                                        </td>
                                    </tr>
                                    <tr __designer:mapid="971">
                                        <td align="left" colspan="6" __designer:mapid="972">
                                            <table __designer:mapid="973">
                                                <tr __designer:mapid="974">
                                                    <td __designer:mapid="975">
                                                        Disponibilidad de Certificados :
                                                    </td>
                                                    <td __designer:mapid="976">
                                                        <asp:TextBox ID="txtMinimo" runat="server" MaxLength="6" Width="40px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvtxtMinimo" runat="server" ErrorMessage="(*)" ControlToValidate="txtMinimo"
                                                            ValidationGroup="rfvDatosBasico"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td __designer:mapid="979">
                                                        -
                                                    </td>
                                                    <td __designer:mapid="97a">
                                                        <asp:TextBox ID="txtMaximo" runat="server" MaxLength="6" Width="40px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvtxtMaximo" runat="server" ErrorMessage="(*)" ControlToValidate="txtMaximo"
                                                            ValidationGroup="rfvDatosBasico"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="style1 MarcoTabla">
                                <tr>
                                    <td class="TablaTitulo">
                                        Parentesco Permitido</td>
                                </tr>
                                <tr>
                                    <td>
                                <table width="100%" __designer:mapid="6cf">
                                    <tr __designer:mapid="6d0">
                                        <td align="center" __designer:mapid="6d1">
                                            <table __designer:mapid="6d2">
                                                <tr __designer:mapid="6d3">
                                                    <td align="left" __designer:mapid="6d4">
                                                        Parentescos :
                                                    </td>
                                                    <td align="left" __designer:mapid="6d5">
                                                        Parentescos al Producto :
                                                    </td>
                                                </tr>
                                                <tr __designer:mapid="6d6">
                                                    <td colspan="2" align="center" __designer:mapid="6d7">
                                                        <asp:Label ID="lblRegistrosParentescos" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr __designer:mapid="6d9">
                                                    <td align="left" __designer:mapid="6da">
                                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                            <ContentTemplate>
                                                                <asp:ListBox ID="lbParentescos" runat="server" CssClass="Body" DataSourceID="sqlParentesco"
                                                                    DataTextField="Nombre" DataValueField="idparentesco" Width="200px" 
                                                                    Rows="6">
                                                                </asp:ListBox>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                        <asp:SqlDataSource ID="sqlParentesco" runat="server" ConnectionString="<%$ ConnectionStrings:BAIS %>"
                                                            
                                                            
                                                            SelectCommand="select idparentesco, upper(Nombre) as nombre from parentesco">
                                                        </asp:SqlDataSource>
                                                    </td>
                                                    <td __designer:mapid="6df">
                                                        <table __designer:mapid="6e0">
                                                            <tr __designer:mapid="6e1">
                                                                <td __designer:mapid="6e2">
                                                                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                                        <ContentTemplate>
                                                                            <asp:ImageButton ID="ibtnAdelante1" runat="server" 
                                                                                ImageUrl="~/Imagenes/adelante.png" />
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                            <tr __designer:mapid="6e6">
                                                                <td __designer:mapid="6e7">
                                                                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                                                        <ContentTemplate>
                                                                            <asp:ImageButton ID="ibtnAtraz1" runat="server" 
                                                                                ImageUrl="~/Imagenes/atraz.png" />
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td align="left" __designer:mapid="6eb">
                                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                            <ContentTemplate>
                                                                <asp:ListBox ID="lbParentescosProducto" runat="server" CssClass="Body" DataTextField="parentesco"
                                                                    DataValueField="idparentesco" Width="200px" Rows="6"></asp:ListBox>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="style1 MarcoTabla">
                                <tr>
                                    <td class="TablaTitulo">
                                        Documentos Permitos</td>
                                </tr>
                                <tr>
                                    <td>
                                            <table __designer:mapid="6f2" style="margin: auto">
                                                <tr __designer:mapid="6f3">
                                                    <td align="left" __designer:mapid="6f4">
                                                        Tipos Documentos :
                                                    </td>
                                                    <td align="left" __designer:mapid="6f5">
                                                        Documentos al Producto :
                                                    </td>
                                                </tr>
                                                <tr __designer:mapid="6f6">
                                                    <td colspan="2" align="center" __designer:mapid="6f7">
                                                        <asp:Label ID="lblRegistrosDocumentos" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr __designer:mapid="6f9">
                                                    <td align="left" __designer:mapid="6fa">
                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                            <ContentTemplate>
                                                                <asp:ListBox ID="lbTipoDocumento" runat="server" CssClass="Body" DataSourceID="sqlTipoDocumento"
                                                                    DataTextField="Nombre" DataValueField="idTipoDocumento" Width="200px" 
                                                                    Rows="6">
                                                                </asp:ListBox>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                        <asp:SqlDataSource ID="sqlTipoDocumento" runat="server" ConnectionString="<%$ ConnectionStrings:BAIS %>"
                                                            
                                                            
                                                            SelectCommand="select idTipoDocumento, upper(Nombre) as nombre from TipoDocumento">
                                                        </asp:SqlDataSource>
                                                    </td>
                                                    <td __designer:mapid="6ff">
                                                        <table __designer:mapid="700">
                                                            <tr __designer:mapid="701">
                                                                <td __designer:mapid="702">
                                                                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                                                        <ContentTemplate>
                                                                            <asp:ImageButton ID="ibtnAdelante2" runat="server" 
                                                                                ImageUrl="~/Imagenes/adelante.png" />
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                            <tr __designer:mapid="706">
                                                                <td __designer:mapid="707">
                                                                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                                                        <ContentTemplate>
                                                                            <asp:ImageButton ID="ibtnAtraz2" runat="server" 
                                                                                ImageUrl="~/Imagenes/atraz.png" />
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td align="left" __designer:mapid="70b">
                                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                            <ContentTemplate>
                                                                <asp:ListBox ID="lbTipoDocumentoProducto" runat="server" CssClass="Body" DataTextField="TipoDocumento"
                                                                    DataValueField="idTipoDocumento" Width="200px" Rows="6"></asp:ListBox>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="style1 MarcoTabla">
                                <tr>
                                    <td class="TablaTitulo">
                                        Medio Pago</td>
                                </tr>
                                <tr>
                                    <td>
                                <table width="100%" __designer:mapid="8a3">
                                    <tr __designer:mapid="8a4">
                                        <td align="right" __designer:mapid="8a5">
                                            &nbsp;<input ID="btnMedioPagoNuevo" __designer:mapid="8a6" class="ButtonGrid" 
                                                                        onclick="javascript:MostrarPCModal(&quot;dMedioPago&quot;,&quot;0&quot;);" 
                                                                        onclick="return btnMedioPagoNuevo_onclick()" type="button" value=" Nuevo " /></td>
                                    </tr>
                                    <tr __designer:mapid="8a7">
                                        <td align="center" __designer:mapid="8a8">
                                            <asp:UpdatePanel ID="upMedioPago" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gvMediosPago" runat="server" AutoGenerateColumns="False"
                                                        Width="800px">
                                                        <Columns>
                                                            <asp:BoundField DataField="id" HeaderText="#" />
                                                            <asp:BoundField DataField="MedioPago" HeaderText="MedioPago" />
                                                            <asp:BoundField DataField="MonedaCobro" HeaderText="MonedaCobro" />
                                                            <asp:TemplateField ItemStyle-Width="80px">
                                                                <HeaderTemplate>
                                                                    &nbsp;
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <input ID="btNuevoOpcion" 
                                                                        onclick='javascript:MostrarPCModal("dMedioPago","<%# Eval("id") %>");' 
                                                                        src="../Img/Grid/editar.gif" type="image" />
                                                                    <input ID="btEliminar" 
                                                                        onclick='javascript:EliminarPCModal("MedioPago","<%# Eval("id") %>");' 
                                                                        src="../Img/Grid/anular.gif" type="image" />
                                                                </ItemTemplate>
                                                                <ItemStyle Width="80px" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            No medios de pago configurados!</EmptyDataTemplate>
                                                      
                                                       
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="style1 MarcoTabla">
                                <tr>
                                    <td class="TablaTitulo">
                                        Planes</td>
                                </tr>
                                <tr>
                                    <td>
                                <table width="100%" __designer:mapid="8b6">
                                    <tr __designer:mapid="8b7">
                                        <td align="right" __designer:mapid="8b8">
                                            <input id="btNuevoOpcion" type="button" value=" Nuevo " onclick='javascript:MostrarPCModal("dOpcion","0");'
                                                class="ButtonGrid" __designer:mapid="8b9" />
                                        </td>
                                    </tr>
                                    <tr __designer:mapid="8ba">
                                        <td align="center" __designer:mapid="8bb">
                                            <asp:UpdatePanel ID="upOpcion" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gvOpcion" runat="server" AutoGenerateColumns="False"
                                                        Width="800px" DataKeyNames="Opcion,id" EnableModelValidation="True">
                                                        <Columns>
                                                            <asp:BoundField DataField="id" HeaderText="#" />
                                                            <asp:BoundField DataField="Opcion" HeaderText="Opcion" 
                                                                SortExpression="Opcion" />
                                                            <asp:BoundField DataField="Puntos" HeaderText="Puntos" 
                                                                SortExpression="Puntos" />
                                                            <asp:BoundField DataField="PuntosValor" HeaderText="PuntosValor" 
                                                                SortExpression="PuntosValor" />
                                                            <asp:BoundField DataField="activoTxt" HeaderText="Activo" 
                                                                SortExpression="Activo" />
                                                            <asp:TemplateField ItemStyle-Width="80px">
                                                                <ItemTemplate>
                                                                    <input ID="btNuevoOpcion" 
                                                                        onclick='javascript:MostrarPCModal("dOpcion","<%# Eval("id") %>");' 
                                                                        src="../Img/Grid/editar.gif" type="image" />
                                                                    <input ID="btEliminar" 
                                                                        onclick='javascript:EliminarPCModal("Opcion","<%# Eval("id") %>");' 
                                                                        src="../Img/Grid/anular.gif" type="image" />
                                                                </ItemTemplate>
                                                                <ItemStyle Width="80px" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            No existen opciones configuradas!</EmptyDataTemplate>
                                                        
                                                        
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="style1 MarcoTabla">
                                <tr>
                                    <td class="TablaTitulo">
                                        Primas</td>
                                </tr>
                                <tr>
                                    <td>
                                <table width="100%" __designer:mapid="8cb">
                                    <tr __designer:mapid="8cc">
                                        <td align="right" __designer:mapid="8cd">
                                            <input id="btnOpcioinPrimaNuevo" type="button" value=" Nuevo " onclick='javascript:MostrarPCModal("dOpcionPrima","0");'
                                                class="ButtonGrid" __designer:mapid="8ce" />
                                        </td>
                                    </tr>
                                    <tr __designer:mapid="8cf">
                                        <td align="center" __designer:mapid="8d0">
                                            <asp:UpdatePanel ID="upPlanes" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gvPlanes" runat="server" AutoGenerateColumns="False"
                                                        Width="800px" DataKeyNames="Opcion,Prima">
                                                        <Columns>
                                                            <asp:BoundField DataField="id" HeaderText="#" />
                                                            <asp:BoundField DataField="opcion" HeaderText="Opcion" />
                                                            <asp:BoundField DataField="Frecuencia" HeaderText="Frecuencia" />
                                                            <asp:BoundField DataField="prima" HeaderText="Prima" />
                                                            <asp:BoundField DataField="montoAsegurado" HeaderText="MontoAsegurado" />
                                                            <asp:BoundField DataField="MonedaPrima" HeaderText="MonedaPrima" 
                                                                SortExpression="EdadMax" />
                                                            <asp:BoundField DataField="edadMin" HeaderText="EdadMin" />
                                                            <asp:BoundField DataField="edadMax" HeaderText="EdadMax" />
                                                            <asp:BoundField DataField="activoTxt" HeaderText="Activo" />
                                                            <asp:TemplateField ItemStyle-Width="80px">
                                                                <ItemTemplate>
                                                                    <input ID="btNuevoOpcion" 
                                                                        onclick='javascript:MostrarPCModal("dOpcionPrima","<%# Eval("id") %>");' 
                                                                        src="../Img/Grid/editar.gif" type="image" />
                                                                    <input ID="btEliminar" 
                                                                        onclick='javascript:EliminarPCModal("OpcionPrima","<%# Eval("id") %>");' 
                                                                        src="../Img/Grid/anular.gif" type="image" />
                                                                </ItemTemplate>
                                                                <ItemStyle Width="80px" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            No existen planes configurados!</EmptyDataTemplate>
                                                    
                                         
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="style1 MarcoTabla">
                                <tr>
                                    <td class="TablaTitulo">
                                        Información Adicional del Asegurado</td>
                                </tr>
                                <tr>
                                    <td>
                                <table width="100%" __designer:mapid="8e4">
                                    <tr __designer:mapid="8e5">
                                        <td align="right" __designer:mapid="8e6">
                                            <input id="btnInfoAseguradoNuevo" type="button" value=" Nuevo " onclick='javascript:MostrarPCModal("dInfoAsegurado","0");'
                                                class="ButtonGrid" __designer:mapid="8e7" />
                                        </td>
                                    </tr>
                                    <tr __designer:mapid="8e8">
                                        <td align="center" __designer:mapid="8e9">
                                            <asp:UpdatePanel ID="upInfoAsegurado" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gvInfoAsegurado" runat="server" AutoGenerateColumns="False"
                                                        Width="800px">
                                                        <Columns>
                                                            <asp:BoundField DataField="id" HeaderText="#" />
                                                            <asp:BoundField DataField="nombre" HeaderText="Nombre" />
                                                            <asp:BoundField DataField="descripcion" HeaderText="Descripcion" />
                                                            <asp:BoundField DataField="TipoDato" HeaderText="TipoDato" />
                                                            <asp:BoundField DataField="obligatorioTxt" HeaderText="Obligatorio" />
                                                            <asp:TemplateField ItemStyle-Width="80px">
                                                                <ItemTemplate>
                                                                    <input ID="btNuevoOpcion" 
                                                                        onclick='javascript:MostrarPCModal("dInfoAsegurado","<%# Eval("id") %>");' 
                                                                        src="../Img/Grid/editar.gif" type="image" />
                                                                    <input ID="btEliminar" 
                                                                        onclick='javascript:EliminarPCModal("InfoAsegurado","<%# Eval("id") %>");' 
                                                                        src="../Img/Grid/anular.gif" type="image" />
                                                                </ItemTemplate>
                                                                <ItemStyle Width="80px" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            No hay infoasegurados configurados!</EmptyDataTemplate>
                                                      
                                                       
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="style1 MarcoTabla">
                                <tr>
                                    <td class="TablaTitulo">
                                        Información Adicional del Contratante</td>
                                </tr>
                                <tr>
                                    <td>
                                <table width="100%" __designer:mapid="8f9">
                                    <tr __designer:mapid="8fa">
                                        <td align="right" __designer:mapid="8fb">
                                            <input id="btnInfoProductoNuevo" type="button" value=" Nuevo " onclick='javascript:MostrarPCModal("dInfoProducto","0");'
                                                class="ButtonGrid" __designer:mapid="8fc" />
                                        </td>
                                    </tr>
                                    <tr __designer:mapid="8fd">
                                        <td align="center" __designer:mapid="8fe">
                                            <asp:UpdatePanel ID="upInfoProducto" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gvInfoProducto" runat="server" AutoGenerateColumns="False"
                                                        Width="800px">
                                                        <Columns>
                                                            <asp:BoundField DataField="id" HeaderText="#" />
                                                            <asp:BoundField DataField="nombre" HeaderText="Nombre" />
                                                            <asp:BoundField DataField="descripcion" HeaderText="Descripcion" />
                                                            <asp:BoundField DataField="TipoDato" HeaderText="TipoDato" />
                                                            <asp:BoundField DataField="obligatorioTxt" HeaderText="Obligatorio" />
                                                            <asp:TemplateField ItemStyle-Width="80px">
                                                                <ItemTemplate>
                                                                    <input ID="btNuevoOpcion" 
                                                                        onclick='javascript:MostrarPCModal("dInfoProducto","<%# Eval("id") %>");' 
                                                                        src="../Img/Grid/editar.gif" type="image" />
                                                                    <input ID="btEliminar" 
                                                                        onclick='javascript:EliminarPCModal("InfoProducto","<%# Eval("id") %>");' 
                                                                        src="../Img/Grid/anular.gif" type="image" />
                                                                </ItemTemplate>
                                                                <ItemStyle Width="80px" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            No hay infoproducto configurados!</EmptyDataTemplate>
                                                      
                                                    
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0">
                                <ProgressTemplate>
                                    <uc1:Cargando ID="Cargando1" runat="server" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:Button ID="btnGrabar" runat="server" Text=" .: Grabar :. " class="ButtonGrid"
                                ValidationGroup="rfvDatosBasico" Height="24px" />
                        </td>
                    </tr>
                    </table>
            </td>
        </tr>
        <tr>
            <td valign="top" align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="left" colspan="4">
                <hr />
            </td>
        </tr>
    </table>
   
    <asp:ObjectDataSource ID="odsProducto" runat="server" InsertMethod="AgregarBAIS"
        TypeName="DataEntry.Web.Producto">
        <InsertParameters>
            <asp:Parameter Name="Producto" Type="Object" />
            <asp:Parameter Name="ListParentescoPermitidoBE" Type="Object" />
            <asp:Parameter Name="ListTipoDocProductoBE" Type="Object" />
            <asp:Parameter Name="ListInfoProductoBE" Type="Object" />
            <asp:Parameter Name="ListInfoAseguradoBE" Type="Object" />
            <asp:Parameter Name="ListOpcionBE" Type="Object" />
            <asp:Parameter Name="ListOpcionPrimaBE" Type="Object" />
            <asp:Parameter Name="ListProductoMedioPagoBE" Type="Object" />
        </InsertParameters>
    </asp:ObjectDataSource>
    
    
    
    
    
</asp:Content>
