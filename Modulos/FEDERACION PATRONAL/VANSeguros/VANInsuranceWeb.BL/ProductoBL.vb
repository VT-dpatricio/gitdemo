﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL

Public Class ProductoBL

    Dim _Productoda As Productoda

    Public Sub New()
        _Productoda = New Productoda
    End Sub


    Function Agregar(ByVal Objeto As ProductoBE) As String
        Return _Productoda.AgregarT(Objeto)
    End Function

    Function AgregarBAIS(ByVal Producto As ProductoBE, ByVal ListParentescoPermitidoBE As List(Of ParentescoPermitidoBE), ByVal ListTipoDocProductoBE As List(Of TipoDocProductoBE), ByVal ListInfoProductoBE As List(Of InfoProductoBE), ByVal ListInfoAseguradoBE As List(Of InfoAseguradoBE), ByVal ListOpcionBE As List(Of OpcionBE), ByVal ListOpcionPrimaBE As List(Of OpcionPrimaBE), ByVal ListProductoMedioPagoBE As List(Of ProductoMedioPagoBE)) As String
        Return _Productoda.AgregarBAIS(Producto, ListParentescoPermitidoBE, ListTipoDocProductoBE, ListInfoProductoBE, ListInfoAseguradoBE, ListOpcionBE, ListOpcionPrimaBE, ListProductoMedioPagoBE)
    End Function

    Function Modificar(ByVal Objeto As ProductoBE) As Integer
        Return _Productoda.ModificarT(Objeto)
    End Function

    Function ModificarDiseno(ByVal Objeto As ProductoBE) As Integer
        Return _Productoda.ModificarDiseno(Objeto, Nothing)
    End Function

    Function Eliminar(ByVal Objeto As ProductoBE) As Integer
        Return _Productoda.Eliminar(Objeto, Nothing)
    End Function

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of ProductoBE)
        Return _Productoda.Listar_Todos(Nothing)
    End Function

    Function Listar_TodosBAIS(ByVal IdAsegurador As Integer, ByVal IdEntidad As Integer) As List(Of ProductoBE)
        Return _Productoda.Listar_TodosBAIS(IdAsegurador, IdEntidad)
    End Function

    Function Listar_Id(ByVal Objeto As ProductoBE, Optional ByVal Transaction As DbTransaction = Nothing) As ProductoBE
        Return _Productoda.Listar_Id(Objeto, Nothing)
    End Function

    Function Listar_BDJanus(ByVal Nombre As String, Optional ByVal Transaction As DbTransaction = Nothing) As List(Of ProductoBE)
        Return _Productoda.Listar_BDJanus(Nombre, Nothing)
    End Function







    Function Listar_BAIS_Informador(ByVal IdProducto As Integer) As List(Of GenericoBAIS)
        Return _Productoda.Listar_BAIS_Informador(IdProducto)
    End Function

    Function Listar_BAIS_Oficinas(ByVal IdProducto As Integer) As List(Of GenericoBAIS)
        Return _Productoda.Listar_BAIS_Oficinas(IdProducto)
    End Function

    Function Listar_BAIS_TipoDocumento(ByVal IdProducto As Integer) As List(Of GenericoBAIS)
        Return _Productoda.Listar_BAIS_TipoDocumento(IdProducto)
    End Function

    Function Listar_BAIS_Plan(ByVal IdProducto As Integer) As List(Of GenericoBAIS)
        Return _Productoda.Listar_BAIS_Plan(IdProducto)
    End Function

    Function Listar_BAIS_MonedaPrima(ByVal IdProducto As Integer, ByVal Opcion As String) As List(Of GenericoBAIS)
        Return _Productoda.Listar_BAIS_MonedaPrima(IdProducto, Opcion)
    End Function

    Function Listar_BAIS_MedioPago(ByVal IdProducto As Integer) As List(Of GenericoBAIS)
        Return _Productoda.Listar_BAIS_MedioPago(IdProducto)
    End Function

    Function Listar_BAIS_MonedaCTA(ByVal IdProducto As Integer, ByVal Moneda As String) As List(Of GenericoBAIS)
        Return _Productoda.Listar_BAIS_MonedaCTA(IdProducto, Moneda)
    End Function


    Function Listar_BAIS_Frecuencia(ByVal IdProducto As Integer) As List(Of GenericoBAIS)
        Return _Productoda.Listar_BAIS_Frecuencia(IdProducto)
    End Function


End Class
