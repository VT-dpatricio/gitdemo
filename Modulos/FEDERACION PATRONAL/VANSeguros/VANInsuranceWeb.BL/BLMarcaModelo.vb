﻿Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL
Imports System
Imports System.Collections

Public Class BLMarcaModelo

    Public Function Seleccionar(ByVal IdMarca As String) As IList
        Dim oDL As New DLNTMarcaModelo
        Return oDL.Seleccionar(IdMarca)
    End Function

    Public Function SeleccionarTodos(ByVal pMarca As String) As IList
        Dim oDL As New DLNTMarcaModelo
        Return oDL.SeleccionarTodos(pMarca)
    End Function

    Public Function ValidaIMEI(ByVal pValor As String) As String
        Dim oDL As New DLNTMarcaModelo
        Return oDL.ValidaIMEI(pValor)
    End Function

End Class
