Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL
Imports System
Imports System.Collections
Public Class BLCiudad
    Public Function Seleccionar(ByVal pIDProvincia As Integer) As IList
        Dim oDL As New DLNTCiudad
        Return oDL.Seleccionar(pIDProvincia)
    End Function

    Public Function Buscar(ByVal pIDProducto As Integer, ByVal pBuscar As String) As IList
        Dim oDL As New DLNTCiudad
        Dim oBE As New BECiudad
        oBE.IdProducto = pIDProducto
        oBE.Buscar = pBuscar
        Return oDL.Seleccionar(oBE)
    End Function

    Public Function SeleccionarxCodPostal(ByVal pIDProducto As Integer, ByVal pCodPostal As String) As BECiudad
        Dim oDL As New DLNTCiudad
        Dim oBE As New BECiudad
        oBE.IdProducto = pIDProducto
        oBE.CodPostal = pCodPostal
        oBE.IdCiudad = 0
        Return oDL.SeleccionarBE(oBE)
    End Function

    Public Function SeleccionarxIDCiudad(ByVal pIDProducto As Integer, ByVal pIDCiudad As Int32) As BECiudad
        Dim oDL As New DLNTCiudad
        Dim oBE As New BECiudad
        oBE.IdProducto = pIDProducto
        oBE.IdCiudad = pIDCiudad
        oBE.CodPostal = ""
        Return oDL.SeleccionarBE(oBE)
    End Function

End Class
