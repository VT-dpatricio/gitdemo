﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL

Public Class FormaPagoBL

    Dim _FormaPagoda As FormaPagoda

    Public Sub New()
        _FormaPagoda = New FormaPagoda
    End Sub

    Function Agregar(ByVal Objeto As FormaPagoBE) As String
        Return _FormaPagoda.Agregar(Objeto, Nothing)
    End Function

    Function Modificar(ByVal Objeto As FormaPagoBE) As Integer
        Return _FormaPagoda.Modificar(Objeto, Nothing)
    End Function

    Function Eliminar(ByVal Objeto As FormaPagoBE) As Integer
        Return _FormaPagoda.Eliminar(Objeto, Nothing)
    End Function

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of FormaPagoBE)
        Return _FormaPagoda.Listar_Todos(Nothing)
    End Function

    Function Listar_Id(ByVal Objeto As FormaPagoBE, Optional ByVal Transaction As DbTransaction = Nothing) As FormaPagoBE
        Return _FormaPagoda.Listar_Id(Objeto, Nothing)
    End Function

End Class

