﻿Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL
Imports System
Imports System.Collections

Public Class BLInfoParametro
    Private Const COD_MES_VIGENCIA As String = "CalculaMesVigencia"

    Public Function Seleccionar(ByVal pIDProducto As Integer, ByVal pCodParametro As String) As List(Of BEInfoParametro)
        Dim oDL As New DLNTInfoParametro
        Return oDL.Seleccionar(pIDProducto, pCodParametro)
    End Function

    Public Function GetMesVegencia(ByVal pIDProducto As Integer, ByRef FinVigenciaAbierta As Dictionary(Of Integer, Integer)) As Int32
        Dim lst As List(Of BEInfoParametro) = New List(Of BEInfoParametro)
        Dim frecuenciaMensual As Int32 = 12
        Dim oBj = New BEInfoParametro()
        Dim valor As Int32


        Try
            If FinVigenciaAbierta.ContainsKey(pIDProducto) Then
                If FinVigenciaAbierta(pIDProducto) <> 0 Then
                    frecuenciaMensual = FinVigenciaAbierta(pIDProducto)
                End If

            ElseIf Not FinVigenciaAbierta.ContainsKey(pIDProducto) Then
                lst = DirectCast(Seleccionar(pIDProducto, COD_MES_VIGENCIA), List(Of BEInfoParametro))

                oBj = lst.Find(Function(value As BEInfoParametro)
                                   Return value.ValorNum
                               End Function)
                If IsNothing(oBj) Then
                    valor = 0
                ElseIf Not IsNothing(oBj) Then
                    valor = oBj.ValorNum
                    frecuenciaMensual = valor
                End If


                FinVigenciaAbierta.Add(pIDProducto, valor)
            End If
        Catch ex As Exception

        End Try
        Return frecuenciaMensual

    End Function

End Class
