﻿Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL

Public Class BLSolicitudConctactoCliente
    Public Function Guardar(objBESolicitudContacto As BESolicitudContactoCliente) As Boolean
        Dim objDLSolicitudContacto As New DLTSolicitudConctactoCliente
        Return objDLSolicitudContacto.Guardar(objBESolicitudContacto)
    End Function

    Public Function ListarSolicitudes(pIdProducto As Integer, pTipoDocumento As String, pccCliente As String) As IList
        Dim objDLSolicitudContacto As New DLTSolicitudConctactoCliente
        Dim objBESolicitudContacto As New BESolicitudContactoCliente
        objBESolicitudContacto.IdTipoDocumento = pTipoDocumento
        objBESolicitudContacto.ccCliente = pccCliente
        Return objDLSolicitudContacto.ListarSolicitudesContacto(pIdProducto, objBESolicitudContacto)
    End Function

    Public Function ListarTipoFrecuenciaContacto() As IList
        Dim objDLSolicitudContacto As New DLTSolicitudConctactoCliente
        Return objDLSolicitudContacto.ListarTipoFrecuenciaContacto
    End Function

    Public Function ListarSolicitudesContacto(ByVal pEntidad As BEFiltroSolicitudContacto) As DataTable
        Dim objDLSolicitudContacto As New DLTSolicitudConctactoCliente
        Return objDLSolicitudContacto.ListarSolicitudesContacto(pEntidad)
    End Function
End Class
