﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL

Public Class InfoProductoBL

    Dim _InfoProductoda As InfoProductoda

    Public Sub New()
        _InfoProductoda = New InfoProductoda
    End Sub

    Function Agregar(ByVal Objeto As InfoProductoBE) As String
        Return _InfoProductoda.Agregar(Objeto, Nothing)
    End Function

    Function Modificar(ByVal Objeto As InfoProductoBE) As Integer
        Return _InfoProductoda.Modificar(Objeto, Nothing)
    End Function

    Function Eliminar(ByVal Objeto As InfoProductoBE) As Integer
        Return _InfoProductoda.Eliminar(Objeto, Nothing)
    End Function

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of InfoProductoBE)
        Return _InfoProductoda.Listar_Todos(Nothing)
    End Function

    Function Listar_Id(ByVal Objeto As InfoProductoBE, Optional ByVal Transaction As DbTransaction = Nothing) As InfoProductoBE
        Return _InfoProductoda.Listar_Id(Objeto, Nothing)
    End Function

    Function Listar_IdTipoInfoProducto(ByVal Objeto As InfoProductoBE, Optional ByVal Transaction As DbTransaction = Nothing) As List(Of InfoProductoBE)
        Return _InfoProductoda.Listar_IdTipoInfoProducto(Objeto, Nothing)
    End Function

    Function Listar_BDJanus(ByVal IdTipo As Integer, ByVal Descripcion As String, Optional ByVal Transaction As DbTransaction = Nothing) As List(Of InfoProductoBE)
        Return _InfoProductoda.Listar_BDJanus(IdTipo, Descripcion, Nothing)
    End Function


End Class
