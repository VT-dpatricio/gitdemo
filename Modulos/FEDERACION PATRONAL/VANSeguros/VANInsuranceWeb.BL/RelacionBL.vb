﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL

Public Class RelacionBL

    Dim _Relacionda As Relacionda

    Public Sub New()
        _Relacionda = New Relacionda
    End Sub


    Function Agregar(ByVal Objeto As RelacionBE) As String
        Return _Relacionda.Agregar(Objeto, Nothing)
    End Function

    Function Modificar(ByVal Objeto As RelacionBE) As Integer
        Return _Relacionda.Modificar(Objeto, Nothing)
    End Function

    Function Eliminar(ByVal Objeto As RelacionBE) As Integer
        Return _Relacionda.Eliminar(Objeto, Nothing)
    End Function

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of RelacionBE)
        Return _Relacionda.Listar_Todos(Nothing)
    End Function

    Function Listar_Id(ByVal Objeto As RelacionBE, Optional ByVal Transaction As DbTransaction = Nothing) As RelacionBE
        Return _Relacionda.Listar_Id(Objeto, Nothing)
    End Function

End Class
