﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL

Public Class CertificadoInfoProductoBL

    Dim _CertificadoInfoProductoDA As CertificadoInfoProductoda

    Public Sub New()
        _CertificadoInfoProductoDA = New CertificadoInfoProductoda
    End Sub

    Function Agregar(ByVal Objeto As CertificadoInfoProductoBE) As String
        Return _CertificadoInfoProductoDA.Agregar(Objeto, Nothing)
    End Function

    Function Modificar(ByVal Objeto As CertificadoInfoProductoBE) As Integer
        Return _CertificadoInfoProductoDA.Modificar(Objeto, Nothing)
    End Function

    Function Eliminar(ByVal Objeto As CertificadoInfoProductoBE) As Integer
        Return _CertificadoInfoProductoDA.Eliminar(Objeto, Nothing)
    End Function

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of CertificadoInfoProductoBE)
        Return _CertificadoInfoProductoDA.Listar_Todos(Nothing)
    End Function

    Function Listar_Id(ByVal Objeto As CertificadoInfoProductoBE, Optional ByVal Transaction As DbTransaction = Nothing) As CertificadoInfoProductoBE
        Return _CertificadoInfoProductoDA.Listar_Id(Objeto, Nothing)
    End Function

End Class
