﻿Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL
Imports System
Imports System.Collections

Public Class BLProducto 
    Public Function Seleccionar(ByVal pBuscar As String, ByVal pIDEntidad As Integer) As IList
        Dim oDL As New DLNTProducto
        Dim oBE As New BEProducto
        oBE.Buscar = pBuscar
        oBE.IdEntidad = pIDEntidad
        Return oDL.Seleccionar(oBE)
    End Function

    Public Function Seleccionar(ByVal pIDProducto As Integer) As BEProducto
        Dim oDL As New DLNTProducto
        Return DirectCast(oDL.SeleccionarBE(pIDProducto), BEProducto)
    End Function

End Class


