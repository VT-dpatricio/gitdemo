﻿Public Class BEParametersApp

    Private _usuario As BEUsuario
    Private _listParametro As IList
    Private _expiredPsw As Boolean
    Private _firstLogin As Boolean
    Private _timeOut As Integer

    Public Property Usuario() As BEUsuario
        Get
            Return Me._usuario
        End Get
        Set(ByVal value As BEUsuario)
            Me._usuario = value
        End Set
    End Property

    Public Property ListParametro() As IList
        Get
            Return Me._listParametro
        End Get
        Set(ByVal value As IList)
            Me._listParametro = value
        End Set
    End Property

    Public Property ExpiredPsw() As Boolean
        Get
            Return Me._expiredPsw
        End Get
        Set(ByVal value As Boolean)
            Me._expiredPsw = value
        End Set
    End Property

    Public Property FirstLogin() As Boolean
        Get
            Return Me._firstLogin
        End Get
        Set(ByVal value As Boolean)
            Me._firstLogin = value
        End Set
    End Property

    Public Property TimeOut() As Integer
        Get
            Return Me._timeOut
        End Get
        Set(ByVal value As Integer)
            Me._timeOut = value
        End Set
    End Property

End Class
