﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VANWebServiceInvoker.Interfaces;
using VANWebServiceInvoker.Model;
using VANWebServiceInvoker.ITAU_SERVICES;

namespace VANWebServiceInvoker.Mapper
{
    public class CbuMapper
    {
        public Model.Cbu FillModel(consultaCBUResponse objCbu)
        {
            Cbu cbu = new Cbu();
            cbu.NroCbu = objCbu.response.cbu.Trim();
            cbu.Codigo = objCbu.response.codigo;
            cbu.NroCbuCipher = objCbu.response.numeroCipher;
            return cbu;
        }
    }
}
