﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using VANWebServiceInvoker.Interfaces;
using VANWebServiceInvoker.Model;
using VANWebServiceInvoker.ITAU_SERVICES;


namespace VANWebServiceInvoker.Mapper
{
    public class ClienteMapper
    {
        public Model.Cliente FillModel(consultaPersonaFisicaResponse objCliente, int idProducto)
        {
            Model.Cliente cliente = null;
            if (objCliente.response.codigo == Constant.Constant.RESPONSE_OK)
            {
                if ((objCliente.response.cliente != null && !(string.IsNullOrEmpty(objCliente.response.cliente.codigo) ||
                                                             string.IsNullOrWhiteSpace(objCliente.response.cliente.codigo))))
                {
                    cliente = new Model.Cliente();

                    FillModelClienteDocumentos(cliente, objCliente.response.cliente.documento);
                    cliente.Apellido = SplitData(objCliente.response.cliente.apellido.Trim(), ' ');
                    cliente.Codigo = GetStringValue(objCliente.response.cliente.codigo);
                    FillModelClienteDatosContacto(cliente, objCliente.response.cliente.datosContacto);
                    FillModelClienteDomicilios(cliente, objCliente.response.cliente.domicilio, idProducto);

                    String fechaNac = String.Empty;
                    if (IsValidDateServiceITAU(objCliente.response.cliente.fechaNacimiento))
                    {
                        fechaNac = GetDatePart(objCliente.response.cliente.fechaNacimiento, Constant.Constant.S_FORMAT_YYYYMMDD);
                        if (!String.IsNullOrEmpty(fechaNac))
                        {
                            cliente.FechaNacimiento = fechaNac;
                        }
                    }
                    cliente.IndicadorCrediticio = GetStringValue(objCliente.response.cliente.indicadorCrediticio);
                    cliente.IndicadorFuncionario = GetStringValue(objCliente.response.cliente.indicadorFuncionario);
                    cliente.Naturalizacion = GetStringValue(objCliente.response.cliente.naturalizacion);
                    cliente.Nombre = SplitData(objCliente.response.cliente.nombre.Trim(), ' ');

                    List<Model.BEListaEquivalencia> lstEstadoCivil = new List<BEListaEquivalencia>();
                    lstEstadoCivil = Common.Common.GetEquivalencias(Constant.Constant.T_ESTADO_CIVIL, idProducto);
                    BEListaEquivalencia oBEEstadoCivil = Common.Common.getValorAON(lstEstadoCivil, Common.Common.GetStringSelectValue(objCliente.response.cliente.estadoCivil));
                    cliente.EstadoCivil = Common.Common.GetStringSelectValue(oBEEstadoCivil.ValorAON);

                    FillModelClienteProfesion(cliente, objCliente.response.cliente.profesion);

                    cliente.ProvinciaNacimiento = GetStringValue(objCliente.response.cliente.provinciaNacimiento);
                    cliente.Sexo = Common.Common.GetStringSelectValue(objCliente.response.cliente.sexo);

                    FillModelClienteTelefonos(cliente, objCliente.response.cliente.telefono);

                    cliente.Tipo = GetStringValue(objCliente.response.cliente.tipo);
                }

            }
            return cliente;
        }

        private string[] SplitData(string valor, char separator)
        {
            string[] tabla;
            tabla = GetStringValue(valor).Split(separator);
            return tabla;
        }

        private bool IsValidDateServiceITAU(String sDate)
        {
            return IsValidDate(sDate, Constant.Constant.S_FORMAT_YYYYMMDD);
        }

        private bool IsValidDate(String sDate, String format)
        {
            DateTime dt;
            return DateTime.TryParseExact(sDate, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
        }

        private string GetDatePart(String sDate, String format, String separator = "/")
        {
            String fAux = String.Empty;
            if (!String.IsNullOrEmpty(sDate) && IsValidDate(sDate, format))
            {
                switch (format)
                {
                    case Constant.Constant.S_FORMAT_DDMMYYYY:
                        fAux = sDate.Substring(0, 2) + separator + sDate.Substring(2, 2) + separator + sDate.Substring(4, 4);
                        break;
                    case Constant.Constant.S_FORMAT_MMDDYYYY:
                        fAux = sDate.Substring(2, 2) + separator + sDate.Substring(0, 2) + separator + sDate.Substring(4, 4);
                        break;
                    case Constant.Constant.S_FORMAT_YYYYMMDD:
                        fAux = sDate.Substring(6, 2) + separator + sDate.Substring(4, 2) + separator + sDate.Substring(0, 4);
                        break;
                    default:
                        fAux = "01" + separator + "01" + separator + "1900";
                        break;
                }
            }
            return fAux;
        }

        private string GetStringValue(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return value.Trim();
            }
            return string.Empty;
        }




        /// <summary>
        /// Carga los domicilios en el modelo del cliente
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="domicilios"></param>
        private void FillModelClienteDomicilios(Model.Cliente cliente, VANWebServiceInvoker.ITAU_SERVICES.Domicilio[] domicilios, int idProducto)
        {
            List<Model.Domicilio> listaDomicilios = new List<Model.Domicilio>();

            if (domicilios != null)
            {
                foreach (var item in domicilios)
                {
                    Model.Domicilio domicilio = new Model.Domicilio();
                    domicilio.Calle = GetStringValue(item.calle);
                    domicilio.CodigoPostal = GetStringValue(item.codigoPostal);
                    if (domicilio.CodigoPostal.Length > 3)
                    {
                        domicilio.CodigoPostal = domicilio.CodigoPostal.Substring(0,4);
                    }
                    else
                    {
                        domicilio.CodigoPostal = domicilio.CodigoPostal;
                    }

                    domicilio.Departamento = GetStringValue(item.departamento);
                    domicilio.Numero = GetStringValue(item.numero);
                    domicilio.Piso = GetStringValue(item.piso);
                    domicilio.Tipo = GetStringValue(item.tipo);

                    Model.BECiudad oBECiudad = Common.Common.SeleccionarxCodPostal(idProducto, GetStringValue(item.codigoPostal));

                    domicilio.Provincia = string.Empty;
                    domicilio.Localidad = string.Empty;
                    domicilio.IdCiudad = Constant.Constant.CIUDAD_DEFAULT_ID;
                    if (oBECiudad != null)
                    {
                        domicilio.Provincia = oBECiudad.Provincia;
                        domicilio.Localidad = oBECiudad.Ciudad;
                        domicilio.IdCiudad = oBECiudad.IdCiudad;
                    }

                    listaDomicilios.Add(domicilio);
                }
            }
           
            cliente.Domicilios = listaDomicilios.ToArray();
        }

        private void FillModelClienteDocumentos(Model.Cliente cliente, VANWebServiceInvoker.ITAU_SERVICES.Documento[] documentos)
        {
            List<Model.Documento> listaDocumentos = new List<Model.Documento>();

            if (documentos != null)
            {
                foreach (var item in documentos)
                {
                    Model.Documento documento = new Model.Documento();
                    documento.Codigo = GetStringValue(item.codigo);
                    documento.Numero = GetStringValue(item.numero);

                    listaDocumentos.Add(documento);
                }
            }
            
            cliente.Documentos = listaDocumentos.ToArray();
        }

        /// <summary>
        /// Carga los datos del contacto al model del cliente.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="datosContacto"></param>
        private void FillModelClienteDatosContacto(Model.Cliente cliente, VANWebServiceInvoker.ITAU_SERVICES.DatosContacto[] datosContacto)
        {
            List<Model.DatosContacto> listaDatosContacto = new List<Model.DatosContacto>();

            if (datosContacto != null)
            {
                foreach (var item in datosContacto)
                {
                    Model.DatosContacto datoContacto = new Model.DatosContacto();
                    datoContacto.Email = GetStringValue(item.email);
                    datoContacto.Numero = GetStringValue(item.numero);
                    datoContacto.Prefijo = GetStringValue(item.prefijo);

                    listaDatosContacto.Add(datoContacto);
                }
            }
            cliente.DatosContacto = listaDatosContacto.ToArray();

        }

        /// <summary>
        /// Carga los datos de los telefonos en el modelo del cliente.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="telefonos"></param>
        private void FillModelClienteTelefonos(Model.Cliente cliente, VANWebServiceInvoker.ITAU_SERVICES.Telefono[] telefonos)
        {
            List<Model.Telefono> listaTelefonos = new List<Model.Telefono>();

            if (telefonos != null)
            {
                foreach (var item in telefonos)
                {
                    Model.Telefono telefono = new Model.Telefono();
                    telefono.Numero = GetStringValue(item.numero);
                    telefono.Prefijo = GetStringValue(item.prefijo);

                    listaTelefonos.Add(telefono);
                }
            }
            
            cliente.Telefonos = listaTelefonos.ToArray();
            

        }

        /// <summary>
        /// Carga los datos de la profesion en el modelo del cliente
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="profesion"></param>
        private void FillModelClienteProfesion(Model.Cliente cliente, VANWebServiceInvoker.ITAU_SERVICES.Profesion profesion)
        {
            cliente.Profesion = new Model.Profesion();
            if (profesion != null)
            {
                cliente.Profesion.Codigo = profesion.codigo;
                cliente.Profesion.Descripcion1 = GetStringValue(profesion.descripcion1);
                cliente.Profesion.Descripcion2 = GetStringValue(profesion.descripcion2);
                cliente.Profesion.Descripcion3 = GetStringValue(profesion.descripcion3);
            }
        }
    }
}
