﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VANWebServiceInvoker.Model
{
    public sealed class DatosContacto
    {
        public string Prefijo { get; set; }

        public string Numero { get; set; }

        public string Email { get; set; }
    }
}
