﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VANWebServiceInvoker.Model
{
    public sealed class Documento
    {
        public string Codigo { get; set; }
        public string Numero { get; set; }

    }
}
