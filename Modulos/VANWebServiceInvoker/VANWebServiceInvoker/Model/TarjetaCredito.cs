﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VANWebServiceInvoker.Model
{
    public sealed class TarjetaCredito
    {
        public TarjetaCredito()
        {
            MedioPagoTarjeta = new MedioPago[0];
        }

        public string IdFuncion { get; set; }
        public MedioPago[] MedioPagoTarjeta { get; set; }
    }
}
