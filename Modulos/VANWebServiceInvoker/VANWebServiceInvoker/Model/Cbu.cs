﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VANWebServiceInvoker.Model
{
    public class Cbu
    {
        public string NroCbu { get; set; }
        public string NroCbuCipher { get; set; }
        public string Codigo { get; set; }
        public string EstadoRespuesta { get; set; }
    }
}
