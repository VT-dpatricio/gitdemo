﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VANWebServiceInvoker.Model
{
    public class BECiudad
    {
        public int IdCiudad { get; set; }
        public int IdProvincia { get; set; }
        public int IdDepartamento { get; set; }
        public int IdProducto { get; set; }
        public string Nombre { get; set; }
        public string Buscar { get; set; }
        public string Ciudad { get; set; }
        public string Provincia { get; set; }
        public string Departamento { get; set; }
        public string CodPostal { get; set; }
    }
}
