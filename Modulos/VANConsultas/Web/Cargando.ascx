<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Cargando.ascx.vb" Inherits="Cargando" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Panel ID="PCar" runat="server" Width="117px">
<table class="divLoading" style="width: 121px">
    <tr>
        <td rowspan="3">
            <img src="img/wait.gif" /></td>
        <td colspan="2" rowspan="3" style="width: 183px">
            Procesando...</td>
    </tr>
    <tr>
    </tr>
    <tr>
    </tr>
</table>
</asp:Panel>
<cc1:AlwaysVisibleControlExtender ID="AVCE" runat="server" TargetControlID="PCar" VerticalSide="Middle" HorizontalSide="Center" HorizontalOffset=50>
</cc1:AlwaysVisibleControlExtender>