﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CambiarClave.aspx.vb" Inherits="Seguridad_CambiarClave" MasterPageFile="~/MasterPage.master" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <table style="width: 413px; margin:auto">
        <tr>
            <td style="height: 24px">
                </td>
        </tr>
        <tr>
            <td style="height: 20px">
                <asp:Label ID="lblEstadoClave" runat="server"></asp:Label>
                </td>
        </tr>
        <tr>
            <td>
    
                <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                    <asp:View ID="View1" runat="server">
                        <table cellpadding="5" cellspacing="0" class="MarcoTabla" 
                            style="width:392px; margin:20px auto; ">
                            <tr>
                                <td class="TablaTitulo" colspan="3">
                                    Cambiar la contraseña</td>
                            </tr>
                            <tr>
                                <td class="CampoCeldaR" style="width: 287px">
                                    Contraseña Actual:</td>
                                <td style="width:auto">
                                    <asp:TextBox ID="txtClaveActual" runat="server" MaxLength="50" 
                                        TextMode="Password" Width="150px"></asp:TextBox>
                                </td>
                                <td style="width:57px">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                        ControlToValidate="txtClaveActual" 
                                        ErrorMessage="La contraseña actual es obligatoria.">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class=" CampoCeldaR" style="width: 287px">
                                    Nueva Contraseña:</td>
                                <td>
                                    <asp:TextBox ID="txtNuevaClave" runat="server" MaxLength="50" 
                                        TextMode="Password" Width="150px"></asp:TextBox>
                                </td>
                                <td style="width: 57px">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                        ControlToValidate="txtNuevaClave" Display="Dynamic" 
                                        ErrorMessage="La nueva contraseña es obligatoria.">*</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                        ControlToCompare="txtNuevaClave" ControlToValidate="txtNuevaClaveConfir" 
                                        Display="Dynamic" 
                                        ErrorMessage="La nueva contraseña y la confirmación no son iguales">*</asp:CompareValidator>
                                    <asp:RegularExpressionValidator ID="revSeguridadClave" runat="server" 
                                        ControlToValidate="txtNuevaClave" Enabled="False" 
                                        ErrorMessage="La Nueva Contraseña debe contener al menos una letra mayúscula, una letra minúscula y un número o caracter especial (Ej. Salud2)" 
                                        ValidationExpression="((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$">*</asp:RegularExpressionValidator>
                                    <asp:RegularExpressionValidator ID="revLongitudNuevaC" runat="server" 
                                        ControlToValidate="txtNuevaClave" Enabled="False" 
                                        ErrorMessage="La nueva contraseña debe tener una longitud mínima de ... caracteres" 
                                        ValidationExpression="[\S\s]{6,10}">*</asp:RegularExpressionValidator>
                                    <asp:CompareValidator ID="CompareValidator2" runat="server" 
                                        ControlToCompare="txtClaveActual" ControlToValidate="txtNuevaClave" 
                                        ErrorMessage="La nueva contraseña debe ser diferente a la actual" 
                                        Operator="NotEqual" SetFocusOnError="True">*</asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class=" CampoCeldaR" style="width: 287px">
                                    Confirmar Contraseña:</td>
                                <td>
                                    <asp:TextBox ID="txtNuevaClaveConfir" runat="server" MaxLength="50" 
                                        TextMode="Password" Width="150px"></asp:TextBox>
                                </td>
                                <td style="width: 57px">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                        ControlToValidate="txtNuevaClaveConfir" 
                                        ErrorMessage="Confirmar contraseña es obligatoria.">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class=" CampoCeldaR" style="width: 287px">
                                    &nbsp;</td>
                                <td>
                                    <asp:Button ID="btnCambiar" runat="server" Text="Cambiar" Width="80px" />
                                    &nbsp;<asp:Button ID="btnCancelar" runat="server" CausesValidation="False" 
                                        Text="Cancelar" Width="67px" />
                                </td>
                                <td style="width: 57px">
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <br />
                        <br />
                        <br />
                        <table style="width: auto text-align: center; font-weight: bold;">
                            <tr>
                                <td style="width: 397px; text-align: center;">
                                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Img/check.png" />
                                    &nbsp;Contraseña actualizada con éxito<br />
                                    <br />
                                    <asp:Button ID="btnContinuar" runat="server" Text="Continuar" />
                                </td>
                            </tr>
                        </table>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </asp:View>
                    <asp:View ID="View3" runat="server">
                    <br />
                        <br />
                        <br />
                        <table style="width: auto text-align: center; font-weight: bold;">
                            <tr>
                                <td style="width: 397px; text-align: center;">
                                    &nbsp;Solo puede cambiar la clave una vez en el día.<br />
                                    <br />
                                    <asp:Button ID="Button1" runat="server" Text="Continuar" />
                                </td>
                            </tr>
                        </table>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </asp:View>
                </asp:MultiView>
    
    
            </td>
        </tr>
        <tr>
            <td>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                <asp:Label ID="Msg" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    
</asp:Content>
