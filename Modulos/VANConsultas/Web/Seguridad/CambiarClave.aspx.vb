﻿Imports VAN.Consulta.BL
Imports VAN.Consulta.BE
Imports System.Web.Security
Imports AONSEC = VAN.Common.BL
Imports AONSBE = VAN.Common.BE

Partial Class Seguridad_CambiarClave
    Inherits PaginaBase
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            Try

                If CBool(Session("PrimerLogin")) Then
                    lblEstadoClave.Text = "Por favor actualice la contraseña"
                End If

                If CBool(Session("ClaveCaducada")) Then
                    lblEstadoClave.Text = "Contraseña caducada, Por favor actualice la contraseña"
                End If

                '----Cargar Parámetros de Seguridad
                Dim pListParametro As IList = CType(Session("SeguridadPar"), IList)
                Dim oBLCSeguridad As New AONSEC.BLSeguridadConfig

                'Longitud de caracteres mínima para la contraseña (Número)
                Dim pLongMinClave As Int32 = CInt(oBLCSeguridad.Parametro(Constantes.SS_LENGTH_PSW, pListParametro))
                If pLongMinClave = 0 Then
                    '0 No válida la longitud
                    revLongitudNuevaC.Enabled = False
                Else
                    revLongitudNuevaC.Enabled = True
                    revLongitudNuevaC.ValidationExpression = "[\S\s]{" & pLongMinClave.ToString & ",50}"
                    revLongitudNuevaC.ErrorMessage = "La nueva contraseña debe tener una longitud mínima de " & pLongMinClave.ToString & " caracteres"
                End If

                'Contraseña Segura
                revSeguridadClave.Enabled = CBool(oBLCSeguridad.Parametro(Constantes.SS_SECURITY_PSW, pListParametro))

                'Modificar la Clave solo una vez al día
                If CBool(oBLCSeguridad.Parametro(Constantes.SS_CHANGE_PSW_ONCE_A_DAY, pListParametro)) Then
                    Dim oBLSegEvento As New BLSeguridadEvento
                    If oBLSegEvento.ClaveModificadaHoy(User.Identity.Name) Then
                        MultiView1.ActiveViewIndex = 2
                    End If
                End If


            Catch ex As Exception
                Session.Abandon()
                Response.Redirect("~/Login.aspx", False)
            End Try


        End If
    End Sub

    Protected Sub btnCambiar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCambiar.Click
        Dim u As MembershipUser = Membership.GetUser(User.Identity.Name)
        Try
            If u.ChangePassword(txtClaveActual.Text, txtNuevaClave.Text) Then
                'Dim NClave As String = FormsAuthentication.HashPasswordForStoringInConfigFile(txtNuevaClave.Text, "md5") '.GetHashCode()
                'Registro Cambio de Contraseña --------------
                Dim oBESec As New AONSEC.BLSecurity()

                oBESec.RegistrarCambioClave(IDSistema, User.Identity.Name, Request.UserHostAddress)
                If CBool(Session(Constantes.SS_APP_FIRST_LOGIN)) Then
                    oBESec.RegistrarPrimerLogin(IDSistema, User.Identity.Name, Request.UserHostAddress)
                End If
                MultiView1.ActiveViewIndex = 1
                Session(Constantes.SS_APP_FIRST_LOGIN) = False
                Session(Constantes.SS_APP_EXPIRED_PSW) = False
                lblEstadoClave.Text = ""
                Msg.Text = ""
            Else
                Msg.Text = "Contraseña actual incorrecta"
            End If
        Catch ex As Exception
            Msg.Text = "Ocurrió un error: " & Server.HtmlEncode(ex.Message) & ". Por favor vuelva a intentar."
        End Try
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancelar.Click
        txtClaveActual.Text = ""
        txtNuevaClave.Text = ""
        txtNuevaClaveConfir.Text = ""
    End Sub

    Protected Sub btnContinuar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnContinuar.Click, Button1.Click
        Response.Redirect("~/Consulta.aspx", False)
    End Sub
End Class
