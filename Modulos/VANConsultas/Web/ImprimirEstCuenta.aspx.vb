Imports Microsoft.Reporting.WebForms
Partial Class ImprimirEstCuenta
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim rpt As String = "EstadoCuenta"
            Dim idCertificado As String = Request.QueryString("IDCer")
            'Dim idCertificado As String = "5901-000000037"
            Dim ServidorRS As String = ConfigurationManager.AppSettings("ReportServerUri").ToString()
            Dim ServidorDirApp As String = ConfigurationManager.AppSettings("ServidorRSDirApp").ToString()

            ReportAunthentication.ConfigureAuthentication(RptV)

            RptV.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote
            RptV.ServerReport.ReportServerUrl = New Uri(ServidorRS)
            RptV.ServerReport.ReportPath = ServidorDirApp + rpt
            Dim paramList As Generic.List(Of ReportParameter) = New Generic.List(Of ReportParameter)()
            paramList.Add(New ReportParameter("IDCertificado", idCertificado))
            RptV.ServerReport.SetParameters(paramList)
            RptV.ShowParameterPrompts = False
        End If

    End Sub
End Class
