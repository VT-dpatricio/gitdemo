﻿Imports Microsoft.VisualBasic
Imports System.Drawing
Imports VAN.Consulta.BE
Imports VAN.Consulta.BL
Imports AONArchitectural.Common

Public Class PaginaBase
    Inherits System.Web.UI.Page

    Public Function ValidarEstadoClaveUsuario() As Boolean
        Dim valida As Boolean = False
        Try
            'If ) Or CBool(Session("ClaveCaducada")) Then
            valida = CBool(Session("PrimerLogin")) Or CBool(Session("ClaveCaducada"))
            'End If
        Catch ex As Exception
            valida = False
        End Try
        Return valida

    End Function


    Public Function IDSistema() As Int32
        Return Constantes.SISTEMA_ID 'ConfigurationManager.AppSettings("IDSistema").ToString()
    End Function

    Public Function AnexarPDF(ByVal requestRdl As Byte(), ByVal IDCertificado As String, ByVal idProducto As Integer, ByVal IdImpresionPadre As Integer) As Byte()
        Dim bytesRequest As Byte() = requestRdl
        Dim oBLImpEdad As New BLProductoImpresionDetalleEdad
        Dim lista As New ArrayList

        lista = oBLImpEdad.Seleccionar(IdImpresionPadre, IDCertificado)

        If lista.Count > 0 Then
            Dim pdf As New Pdf.PdfFunction
            Dim lstPdfAditional(lista.Count) As String
            Dim index As Integer = 0

            For Each oBE As BEProductoImpresionDetalleEdad In lista
                lstPdfAditional(index) = oBE.Directorio & "\" & oBE.NombreArchivo
                index = index + 1
            Next
            bytesRequest = pdf.MergePage(requestRdl, lstPdfAditional)
        End If
        Return bytesRequest
    End Function

    Public Sub AccessActiveDirectory(ByRef sUsr As String, ByRef sPsw As String, ByVal sRq As HttpRequest)
        Dim entorno As String = "PRO"
        Dim psw As String = "12345+"
        Dim Usuario As String = sUsr
        'Login desde ITAU----------------------------

        If Not (sRq.Form("usr") = Nothing) Then
            If sRq.RequestType = "POST" Then
                Usuario = sRq.Form("usr")
                Dim dominio() As String = Usuario.Split("\")
                If dominio.Length > 1 Then
                    Usuario = dominio(1)
                End If
            End If
        End If
        Try
            If Not IsPrivateAccess() Then
                psw = sPsw
                Usuario = sUsr
            End If
        Catch ex As Exception

        End Try
        sPsw = psw
        sUsr = Usuario

        If (Not (sPsw Is Nothing)) Then
            sPsw = sPsw.Trim()
        End If

        If (Not (sUsr Is Nothing)) Then
            sUsr = sUsr.Trim()
        End If

    End Sub

    Public Function IsPrivateAccess() As Boolean
        Dim result As Boolean = True
        Try
            Dim entorno As String
            entorno = ConfigurationManager.AppSettings.Item("Enviroment")
            If entorno = "DEV" Then
                result = False
            Else
                result = True
            End If
        Catch ex As Exception

        End Try
        Return result
    End Function

  

End Class
