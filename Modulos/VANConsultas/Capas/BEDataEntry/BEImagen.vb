Public Class BEImagen
    Inherits BEBase

    Private _idCertificado As String = String.Empty
    Private _nombreImagen As String = String.Empty
    Private _rutaImagen As String = String.Empty
    Private _usuarioCreacion As String = String.Empty
    Private _fechaCreacion As DateTime
    Private _usuarioModificacion As String = String.Empty
    Private _fechaModificacion As DateTime
    Private _descripcion As String = String.Empty

    Public Property IdCertificado() As String
        Get
            Return _idCertificado
        End Get
        Set(ByVal value As String)
            _idCertificado = value
        End Set
    End Property

    Public Property NombreImagen() As String
        Get
            Return _nombreImagen
        End Get
        Set(ByVal value As String)
            _nombreImagen = value
        End Set
    End Property

    Public Property RutaImagen() As String
        Get
            Return _rutaImagen
        End Get
        Set(ByVal value As String)
            _rutaImagen = value
        End Set
    End Property

    Public Property UsuarioCreacion() As String
        Get
            Return _usuarioCreacion
        End Get
        Set(ByVal value As String)
            _usuarioCreacion = value
        End Set
    End Property

    Public Property FechaCreacion() As DateTime
        Get
            Return _fechaCreacion
        End Get
        Set(ByVal value As DateTime)
            _fechaCreacion = value
        End Set
    End Property

    Public Property UsuarioModificacion() As String
        Get
            Return _usuarioModificacion
        End Get
        Set(ByVal value As String)
            _usuarioModificacion = value
        End Set
    End Property

    Public Property FechaModificacion() As DateTime
        Get
            Return _fechaModificacion
        End Get
        Set(ByVal value As DateTime)
            _fechaModificacion = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property
End Class
