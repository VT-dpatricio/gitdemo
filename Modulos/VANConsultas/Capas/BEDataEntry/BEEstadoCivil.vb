Public Class BEEstadoCivil
    Inherits BEBase

    Private _idEstadoCivil As String = String.Empty
    Private _nombre As String = String.Empty

    Public Property IdEstadoCivil() As String
        Get
            Return _idEstadoCivil
        End Get
        Set(ByVal value As String)
            _idEstadoCivil = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

End Class
