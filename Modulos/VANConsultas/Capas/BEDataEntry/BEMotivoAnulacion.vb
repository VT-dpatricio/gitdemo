Public Class BEMotivoAnulacion
    Inherits BEBase
    Private _idMotivoAnulacion As Int32
    Private _nombre As String = String.Empty

    Public Property IdMotivoAnulacion() As Int32
        Get
            Return _idMotivoAnulacion
        End Get
        Set(ByVal value As Int32)
            _idMotivoAnulacion = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

End Class
