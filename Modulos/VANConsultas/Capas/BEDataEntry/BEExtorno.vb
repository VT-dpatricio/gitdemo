Public Class BEExtorno
    Inherits BEBase
    Private _idCertificado As String = String.Empty
    Private _idCobro As Int32 = 0
    Private _monto As Decimal = 0
    Private _idmoneda As String = String.Empty
    Private _tipoCambio As Decimal = 0
    Private _motivo As String = String.Empty
    Private _fechaSolicitud As DateTime
    Private _fechaExtorno As DateTime
    Private _fechaCierre As DateTime
    Private _fechaRegistro As DateTime
    Private _activo As Int32 = 0
    Private _estadoExtorno As Int32 = 0

    Public Property IdCertificado() As String
        Get
            Return _idCertificado
        End Get
        Set(ByVal value As String)
            _idCertificado = value
        End Set
    End Property

    Public Property IdCobro() As Int32
        Get
            Return _idCobro
        End Get
        Set(ByVal value As Int32)
            _idCobro = value
        End Set
    End Property

    Public Property Monto() As Decimal
        Get
            Return _monto
        End Get
        Set(ByVal value As Decimal)
            _monto = value
        End Set
    End Property

    Public Property Idmoneda() As String
        Get
            Return _idmoneda
        End Get
        Set(ByVal value As String)
            _idmoneda = value
        End Set
    End Property

    Public Property TipoCambio() As Decimal
        Get
            Return _tipoCambio
        End Get
        Set(ByVal value As Decimal)
            _tipoCambio = value
        End Set
    End Property

    Public Property Motivo() As String
        Get
            Return _motivo
        End Get
        Set(ByVal value As String)
            _motivo = value
        End Set
    End Property

    Public Property FechaSolicitud() As DateTime
        Get
            Return _fechaSolicitud
        End Get
        Set(ByVal value As DateTime)
            _fechaSolicitud = value
        End Set
    End Property

    Public Property FechaExtorno() As DateTime
        Get
            Return _fechaExtorno
        End Get
        Set(ByVal value As DateTime)
            _fechaExtorno = value
        End Set
    End Property

    Public Property FechaCierre() As DateTime
        Get
            Return _fechaCierre
        End Get
        Set(ByVal value As DateTime)
            _fechaCierre = value
        End Set
    End Property

    Public Property FechaRegistro() As DateTime
        Get
            Return _fechaRegistro
        End Get
        Set(ByVal value As DateTime)
            _fechaRegistro = value
        End Set
    End Property

    Public Property Activo() As Int32
        Get
            Return _activo
        End Get
        Set(ByVal value As Int32)
            _activo = value
        End Set
    End Property

    Public Property EstadoExtorno() As Int32
        Get
            Return _estadoExtorno
        End Get
        Set(ByVal value As Int32)
            _estadoExtorno = value
        End Set
    End Property



End Class
