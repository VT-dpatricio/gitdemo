Public Class BECobroLog
    Inherits BEBase

    Private _idCobro As Int32
    Private _idCertificado As String = String.Empty
    Private _fechaCobro As DateTime
    Private _fechaPago As DateTime
    Private _idMedioPago As String = String.Empty
    Private _numeroCuenta As String = String.Empty
    Private _valor As Decimal
    Private _idEstadoCobro As Int32
    Private _fechaUltimoCobro As DateTime
    Private _inicioPeriodo As DateTime
    Private _finPeriodo As DateTime
    Private _montoAsegurado As Decimal
    Private _esActivo As Boolean
    Private _fechaProceso As String
    Private _medioPago As String
    Private _estadoCobro As String
    Private _valormoneda As String
    Private _referencia As Int32

    Public Property Referencia() As Int32
        Get
            Return _referencia
        End Get
        Set(ByVal value As Int32)
            _referencia = value
        End Set
    End Property
    Public Property IdCobro() As Int32
        Get
            Return _idCobro
        End Get
        Set(ByVal value As Int32)
            _idCobro = value
        End Set
    End Property

    Public Property IdCertificado() As String
        Get
            Return _idCertificado
        End Get
        Set(ByVal value As String)
            _idCertificado = value
        End Set
    End Property

    Public Property FechaCobro() As DateTime
        Get
            Return _fechaCobro
        End Get
        Set(ByVal value As DateTime)
            _fechaCobro = value
        End Set
    End Property

    Public Property FechaPago() As DateTime
        Get
            Return _fechaPago
        End Get
        Set(ByVal value As DateTime)
            _fechaPago = value
        End Set
    End Property

    Public Property IdMedioPago() As String
        Get
            Return _idMedioPago
        End Get
        Set(ByVal value As String)
            _idMedioPago = value
        End Set
    End Property

    Public Property NumeroCuenta() As String
        Get
            Return _numeroCuenta
        End Get
        Set(ByVal value As String)
            _numeroCuenta = value
        End Set
    End Property

    Public Property Valor() As Decimal
        Get
            Return _valor
        End Get
        Set(ByVal value As Decimal)
            _valor = value
        End Set
    End Property

    Public Property IdEstadoCobro() As Int32
        Get
            Return _idEstadoCobro
        End Get
        Set(ByVal value As Int32)
            _idEstadoCobro = value
        End Set
    End Property

    Public Property FechaUltimoCobro() As DateTime
        Get
            Return _fechaUltimoCobro
        End Get
        Set(ByVal value As DateTime)
            _fechaUltimoCobro = value
        End Set
    End Property

    Public Property InicioPeriodo() As DateTime
        Get
            Return _inicioPeriodo
        End Get
        Set(ByVal value As DateTime)
            _inicioPeriodo = value
        End Set
    End Property

    Public Property FinPeriodo() As DateTime
        Get
            Return _finPeriodo
        End Get
        Set(ByVal value As DateTime)
            _finPeriodo = value
        End Set
    End Property

    Public Property MontoAsegurado() As Decimal
        Get
            Return _montoAsegurado
        End Get
        Set(ByVal value As Decimal)
            _montoAsegurado = value
        End Set
    End Property

    Public Property EsActivo() As Boolean
        Get
            Return _esActivo
        End Get
        Set(ByVal value As Boolean)
            _esActivo = value
        End Set
    End Property

    Public Property FechaProceso() As String
        Get
            Return _fechaProceso
        End Get
        Set(ByVal value As String)
            _fechaProceso = value
        End Set
    End Property

    Public Property MedioPago() As String
        Get
            Return _medioPago
        End Get
        Set(ByVal value As String)
            _medioPago = value
        End Set
    End Property

    Public Property EstadoCobro() As String
        Get
            Return _estadoCobro
        End Get
        Set(ByVal value As String)
            _estadoCobro = value
        End Set
    End Property

    Public Property ValorMoneda() As String
        Get
            Return _valormoneda
        End Get
        Set(ByVal value As String)
            _valormoneda = value
        End Set
    End Property
End Class
