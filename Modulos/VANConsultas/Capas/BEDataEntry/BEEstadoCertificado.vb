Public Class BEEstadoCertificado
    Inherits BEBase
    Private _idEstadoCertificado As Int32
    Private _nombre As String = String.Empty

    Public Property IdEstadoCertificado() As Int32
        Get
            Return _idEstadoCertificado
        End Get
        Set(ByVal value As Int32)
            _idEstadoCertificado = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property
End Class
