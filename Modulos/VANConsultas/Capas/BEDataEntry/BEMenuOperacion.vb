Public Class BEMenuOperacion
    Inherits BEBase
    Private _iDUsuario As String = String.Empty
    Private _iDOperacion As Integer = 0
    Private _iDMenu As Integer = 0
    Private _operacion As String = String.Empty

    Public Property IDUsuario() As String
        Get
            Return _iDUsuario
        End Get
        Set(ByVal value As String)
            _iDUsuario = value
        End Set
    End Property
    Public Property IDOperacion() As Integer
        Get
            Return _iDOperacion
        End Get
        Set(ByVal value As Integer)
            _iDOperacion = value
        End Set
    End Property
    Public Property IDMenu() As Integer
        Get
            Return _iDMenu
        End Get
        Set(ByVal value As Integer)
            _iDMenu = value
        End Set
    End Property
    Public Property Operacion() As String
        Get
            Return _operacion
        End Get
        Set(ByVal value As String)
            _operacion = value
        End Set
    End Property
End Class
