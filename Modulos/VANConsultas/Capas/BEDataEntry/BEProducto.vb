Public Class BEProducto
    Inherits BEBase

    Private _iDProducto As Integer = 0
    Private _iDEntidad As Integer = 0
    Private _descripcion As String = String.Empty
    Private _iDUsuario As String = String.Empty
    Private _opcion As String = String.Empty

    Private _ReporteCertificado As String = String.Empty
    Private _ReporteSolicitud As String = String.Empty
    Private _ReporteCondicion As String = String.Empty
    Private _ReporteDeclaracionJurada As String = String.Empty

    Private _idImpresionCertificado As Int32 = 0
    Private _idImpresionSolicitud As Int32 = 0
    Private _idImpresionCondicion As Int32 = 0
    Private _idImpresionDeclaracionJurada As Int32 = 0

    Public Property IDProducto() As String
        Get
            Return Me._iDProducto
        End Get
        Set(ByVal value As String)
            Me._iDProducto = value
        End Set
    End Property

    Public Property IDUsuario() As String
        Get
            Return Me._iDUsuario
        End Get
        Set(ByVal value As String)
            Me._iDUsuario = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return Me._descripcion
        End Get
        Set(ByVal value As String)
            Me._descripcion = value
        End Set
    End Property

    Public Property IDEntidad() As Integer
        Get
            Return _iDEntidad
        End Get
        Set(ByVal value As Integer)
            _iDEntidad = value
        End Set
    End Property

    Public Property Opcion() As String
        Get
            Return Me._opcion
        End Get
        Set(ByVal value As String)
            Me._opcion = value
        End Set
    End Property

    Public Property ReporteCertificado() As String
        Get
            Return _ReporteCertificado
        End Get
        Set(ByVal value As String)
            _ReporteCertificado = value
        End Set
    End Property

    Public Property ReporteSolicitud() As String
        Get
            Return _ReporteSolicitud
        End Get
        Set(ByVal value As String)
            _ReporteSolicitud = value
        End Set
    End Property

    Public Property ReporteCondicion() As String
        Get
            Return _ReporteCondicion
        End Get
        Set(ByVal value As String)
            _ReporteCondicion = value
        End Set
    End Property

    Public Property ReporteDeclaracionJurada() As String
        Get
            Return _ReporteDeclaracionJurada
        End Get
        Set(ByVal value As String)
            _ReporteDeclaracionJurada = value
        End Set
    End Property

    Public Property IDImpresionCertificado() As Integer
        Get
            Return _idImpresionCertificado
        End Get
        Set(ByVal value As Integer)
            _idImpresionCertificado = value
        End Set
    End Property

    Public Property IDImpresionSolicitud() As Integer
        Get
            Return _idImpresionSolicitud
        End Get
        Set(ByVal value As Integer)
            _idImpresionSolicitud = value
        End Set
    End Property

    Public Property IDImpresionCondicion() As Integer
        Get
            Return _idImpresionCondicion
        End Get
        Set(ByVal value As Integer)
            _idImpresionCondicion = value
        End Set
    End Property

    Public Property IDImpresionDeclaracionJurada() As Integer
        Get
            Return _idImpresionDeclaracionJurada
        End Get
        Set(ByVal value As Integer)
            _idImpresionDeclaracionJurada = value
        End Set
    End Property

End Class
