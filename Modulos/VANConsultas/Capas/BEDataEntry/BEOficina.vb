Public Class BEOficina
    Inherits BEBase
    Private _idOficina As Int32
    Private _nombre As String

    Public Property IdOficina() As Int32
        Get
            Return _idOficina
        End Get
        Set(ByVal value As Int32)
            _idOficina = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property
End Class
