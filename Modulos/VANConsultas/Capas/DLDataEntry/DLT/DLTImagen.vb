Imports VAN.Consulta.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLTImagen
    Inherits DLBase
    Implements IDLT

    Public Function Actualizar(ByVal pEntidad As BE.BEBase) As Boolean Implements IDLT.Actualizar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Eliminar(ByVal pCodigo As Integer) As Boolean Implements IDLT.Eliminar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Insertar(ByVal pEntidad As BE.BEBase) As Boolean Implements IDLT.Insertar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SC_InsertarImagen", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm = Me.LlenarEstructura(pEntidad, cm, "Insertar")
        Dim resultado As Boolean = False
        Try
            cn.Open()
            cm.ExecuteNonQuery()
            resultado = cm.Parameters("ReturnValue").Value()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return resultado
    End Function

    Public Function LlenarEstructura(ByVal pEntidad As BE.BEBase, ByVal pcm As System.Data.SqlClient.SqlCommand, ByVal TipoTransaccion As String) As System.Data.SqlClient.SqlCommand Implements IDLT.LlenarEstructura
        Dim oBE As BEImagen = DirectCast(pEntidad, BEImagen)
        pcm.Parameters.Add("@IdCertificado", SqlDbType.VarChar, 25).Value = oBE.IdCertificado
        pcm.Parameters.Add("@NombreImagen", SqlDbType.VarChar, 20).Value = oBE.NombreImagen
        pcm.Parameters.Add("@RutaImagen", SqlDbType.VarChar, 100).Value = oBE.RutaImagen
        pcm.Parameters.Add("ReturnValue", SqlDbType.Bit).Direction = ParameterDirection.ReturnValue
        pcm.Parameters.Add("@UsuarioCreacion", SqlDbType.VarChar, 50).Value = oBE.UsuarioCreacion
        pcm.Parameters.Add("@Descripcion", SqlDbType.VarChar, 250).Value = oBE.Descripcion
        Return pcm
    End Function

End Class
