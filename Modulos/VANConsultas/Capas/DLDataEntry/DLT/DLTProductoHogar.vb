﻿Imports VAN.Consulta.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLTProductoHogar

    Inherits DLBase
    Implements IDLT

    Public Function Eliminar(ByVal pCodigo As Integer) As Boolean Implements IDLT.Eliminar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Insertar(ByVal pEntidad As BE.BEBase) As Boolean Implements IDLT.Insertar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Actualizar(ByVal pEntidad As BE.BEBase) As Boolean Implements IDLT.Actualizar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SC_ActualizarProductoHogar", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm = Me.LlenarEstructura(pEntidad, cm, "Actualizar")
        Dim resultado As Boolean = False
        Try
            cn.Open()
            cm.ExecuteNonQuery()
            resultado = cm.Parameters("ReturnValue").Value()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return resultado
    End Function


    Public Function LlenarEstructura(ByVal pEntidad As BE.BEBase, ByVal pcm As System.Data.SqlClient.SqlCommand, ByVal TipoTransaccion As String) As System.Data.SqlClient.SqlCommand Implements IDLT.LlenarEstructura
        Dim oBE As BEProductoHogar = DirectCast(pEntidad, BEProductoHogar)
        pcm.Parameters.Add("@IdCertificado", SqlDbType.VarChar, 25).Value = oBE.IdCertificado
        pcm.Parameters.Add("@idRamo ", SqlDbType.Int).Value = oBE.IdRamo
        pcm.Parameters.Add("@detalleBienes", SqlDbType.VarChar, 8000).Value = oBE.DetalleBienes
        pcm.Parameters.Add("ReturnValue", SqlDbType.Bit).Direction = ParameterDirection.ReturnValue
        pcm.Parameters.Add("@superficie", SqlDbType.Int).Value = oBE.Superficie
        pcm.Parameters.Add("@usuarioModificacion", SqlDbType.VarChar, 250).Value = oBE.UsuarioModificacion
        Return pcm
    End Function
End Class
