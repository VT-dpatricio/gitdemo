Imports VAN.Consulta.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLTCertificado
    Inherits DLBase
    Implements IDLT

    Public Function Actualizar(ByVal pEntidad As BE.BEBase) As Boolean Implements IDLT.Actualizar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SCAR_ActualizarCertificado", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm = Me.LlenarEstructura(pEntidad, cm, "Actualizar")
        Dim resultado As Boolean = False
        Try
            cn.Open()
            cm.ExecuteNonQuery()
            resultado = cm.Parameters("ReturnValue").Value()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return resultado
    End Function

    Public Function Eliminar(ByVal pCodigo As Integer) As Boolean Implements IDLT.Eliminar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Insertar(ByVal pEntidad As BE.BEBase) As Boolean Implements IDLT.Insertar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function InsertarCertificado(ByVal pEntidad As BE.BEBase) As String
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("DE_InsertarCertifiado", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm = Me.LlenarEstructura(pEntidad, cm, "I")
        Dim resultado As String = "0"
        Try
            cn.Open()
            cm.ExecuteNonQuery()
            resultado = cm.Parameters("@idCertificado").Value()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return resultado
    End Function

    Public Function Activar(ByVal pEntidad As BE.BEBase) As Boolean
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SC_ActivarCertificado", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm = Me.LlenarEstructura(pEntidad, cm, "Activar")
        Dim resultado As Boolean = False
        Try
            cn.Open()
            cm.ExecuteNonQuery()
            resultado = cm.Parameters("ReturnValue").Value()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return resultado
    End Function

    Public Function Anular(ByVal pEntidad As BE.BEBase) As Boolean
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SC_AnularCertificado", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm = Me.LlenarEstructura(pEntidad, cm, "Anular")
        Dim resultado As Boolean = False
        Try
            cn.Open()
            cm.ExecuteNonQuery()
            resultado = cm.Parameters("ReturnValue").Value()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return resultado
    End Function

    Public Function LlenarEstructura(ByVal pEntidad As BE.BEBase, ByVal pcm As System.Data.SqlClient.SqlCommand, ByVal TipoTransaccion As String) As System.Data.SqlClient.SqlCommand Implements IDLT.LlenarEstructura
        Dim oBE As BECertificado = DirectCast(pEntidad, BECertificado)
        If (TipoTransaccion = "Actualizar") Then
            pcm.Parameters.Add("@idProducto", SqlDbType.Int).Value = oBE.IdProducto
            pcm.Parameters.Add("@idOficina", SqlDbType.Int).Value = oBE.IdOficina
            pcm.Parameters.Add("@idInformador", SqlDbType.VarChar, 15).Value = oBE.IdInformador
            'If (oBE.Vencimiento = String.Empty) Then
            '    pcm.Parameters.Add("@vencimiento", SqlDbType.DateTime).Value = DBNull.Value
            'Else
            '    pcm.Parameters.Add("@vencimiento", SqlDbType.DateTime).Value = DateTime.Parse(oBE.Vencimiento)
            'End If

            '//Información del Titular---------
            pcm.Parameters.Add("@idTipoDocumento", SqlDbType.VarChar, 3).Value = oBE.IdTipoDocumento
            pcm.Parameters.Add("@ccCliente", SqlDbType.VarChar, 20).Value = oBE.CcCliente 'Nro Documento
            pcm.Parameters.Add("@nombre1", SqlDbType.VarChar, 60).Value = oBE.Nombre1
            pcm.Parameters.Add("@nombre2", SqlDbType.VarChar, 60).Value = oBE.Nombre2
            pcm.Parameters.Add("@apellido1", SqlDbType.VarChar, 60).Value = oBE.Apellido1
            pcm.Parameters.Add("@apellido2", SqlDbType.VarChar, 60).Value = oBE.Apellido2
            pcm.Parameters.Add("@Direccion", SqlDbType.VarChar, 250).Value = oBE.Direccion
            pcm.Parameters.Add("@DirNumero", SqlDbType.VarChar, 250).Value = oBE.DirNumero
            pcm.Parameters.Add("@DirPiso", SqlDbType.VarChar, 250).Value = oBE.DirPiso
            pcm.Parameters.Add("@DirDpto", SqlDbType.VarChar, 250).Value = oBE.DirDpto
            pcm.Parameters.Add("@DirCodPostal", SqlDbType.VarChar, 250).Value = oBE.DirCodPostal
            pcm.Parameters.Add("@idCiudad", SqlDbType.Int).Value = oBE.IdCiudad
            pcm.Parameters.Add("@Email", SqlDbType.VarChar, 250).Value = oBE.Email
            pcm.Parameters.Add("@CodAreaTelefono", SqlDbType.VarChar, 250).Value = oBE.CodAreaTelefono
            pcm.Parameters.Add("@Telefono", SqlDbType.VarChar, 15).Value = oBE.Telefono
            pcm.Parameters.Add("@CodAreaOtroTelefono", SqlDbType.VarChar, 250).Value = oBE.CodAreaOtroTelefono
            pcm.Parameters.Add("@OtroTelefono", SqlDbType.VarChar, 250).Value = oBE.OtroTelefono
            pcm.Parameters.Add("@Celular", SqlDbType.VarChar, 250).Value = oBE.Celular

            '//Domicilio del Riesgo Asegurado----------------
            pcm.Parameters.Add("@DRADireccion", SqlDbType.VarChar, 250).Value = oBE.DRADireccion
            pcm.Parameters.Add("@DRADirNumero", SqlDbType.VarChar, 250).Value = oBE.DRADirNumero
            pcm.Parameters.Add("@DRADirPiso", SqlDbType.VarChar, 250).Value = oBE.DRADirPiso
            pcm.Parameters.Add("@DRADirDpto", SqlDbType.VarChar, 250).Value = oBE.DRADirDpto
            pcm.Parameters.Add("@DRADirCodPostal", SqlDbType.VarChar, 250).Value = oBE.DRADirCodPostal
            pcm.Parameters.Add("@DRAIDCiudad", SqlDbType.Int).Value = oBE.DRAIDCiudad

            '//Información Adicional del Titular---
            If oBE.EstadoCivil = "0" Then
                pcm.Parameters.Add("@EstadoCivil", SqlDbType.VarChar, 3).Value = DBNull.Value
            Else
                pcm.Parameters.Add("@EstadoCivil", SqlDbType.VarChar, 3).Value = oBE.EstadoCivil
            End If
            If oBE.Sexo = "0" Then
                pcm.Parameters.Add("@Sexo", SqlDbType.Char, 1).Value = DBNull.Value
            Else
                pcm.Parameters.Add("@Sexo", SqlDbType.Char, 1).Value = oBE.Sexo
            End If
            If (oBE.FechaNacimiento = String.Empty) Then
                pcm.Parameters.Add("@FechaNacimiento", SqlDbType.DateTime).Value = DBNull.Value
            Else
                pcm.Parameters.Add("@FechaNacimiento", SqlDbType.DateTime).Value = oBE.FechaNacimiento
            End If
            pcm.Parameters.Add("@CUIT", SqlDbType.VarChar, 250).Value = oBE.CUIT
            pcm.Parameters.Add("@Nacimiento", SqlDbType.VarChar, 250).Value = oBE.Nacimiento
            pcm.Parameters.Add("@Nacionalidad", SqlDbType.VarChar, 250).Value = oBE.Nacionalidad
            pcm.Parameters.Add("@Profesion", SqlDbType.VarChar, 250).Value = oBE.Profesion

            '//Información de Pago---------------------------------
            pcm.Parameters.Add("@idMedioPago", SqlDbType.VarChar, 2).Value = oBE.IdMedioPago
            pcm.Parameters.Add("@idEstadoCertificado", SqlDbType.Int).Value = oBE.IdEstadoCertificado
            'pcm.Parameters.Add("@numeroCuenta", SqlDbType.VarChar, 20).Value = oBE.NumeroCuenta
            pcm.Parameters.Add("@CondicionIva", SqlDbType.VarChar, 25).Value = oBE.CondicionIVA
            pcm.Parameters.Add("@EnviaMailPoliza", SqlDbType.VarChar, 2).Value = oBE.EnvioMailPoliza
            'pcm.Parameters.Add("@NumTarjAsegurada", SqlDbType.Char, 250).Value = oBE.NumTarjAsegurada
            'pcm.Parameters.Add("@MonedaCuenta", SqlDbType.Char, 250).Value = oBE.MonedaCuenta
            'If (oBE.FechaActivacion = String.Empty) Then
            '    pcm.Parameters.Add("@FechaActivacion", SqlDbType.DateTime).Value = DBNull.Value
            'Else
            '    pcm.Parameters.Add("@FechaActivacion", SqlDbType.DateTime).Value = oBE.FechaActivacion
            'End If
        End If
        If (TipoTransaccion = "Activar") Or (TipoTransaccion = "Anular") Then
        End If
        If (TipoTransaccion = "Anular") Then
            pcm.Parameters.Add("@IdMotivoAnulacion", SqlDbType.Int).Value = oBE.IdMotivoAnulacion
        End If
        pcm.Parameters.Add("ReturnValue", SqlDbType.Bit).Direction = ParameterDirection.ReturnValue
        pcm.Parameters.Add("@UsuarioModificacion", SqlDbType.VarChar, 50).Value = oBE.UsuarioModificacion
        pcm.Parameters.Add("@Observacion", SqlDbType.VarChar, 500).Value = oBE.Observacion
        pcm.Parameters.Add("@idCertificado", SqlDbType.VarChar, 25).Value = oBE.IdCertificado

        Return pcm
    End Function

End Class
