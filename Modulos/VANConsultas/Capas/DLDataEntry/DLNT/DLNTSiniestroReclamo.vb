Imports VAN.Consulta.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLNTSiniestroReclamo
    Inherits DLBase
    Implements IDLNT

    Public Function Seleccionar() As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pEntidad As BE.BEBase) As IList Implements IDLNT.Seleccionar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SC_SeleccionarSiniestroReclamo", cn)
        cm.CommandType = CommandType.StoredProcedure
        Dim oBE As BESiniestroReclamo = DirectCast(pEntidad, BESiniestroReclamo)
        cm.Parameters.Add("@IDCertificado", SqlDbType.VarChar, 25).Value = oBE.IdCertificado
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BESiniestroReclamo
                oBE.Letra = rd.GetString(rd.GetOrdinal("Letra"))
                oBE.NroSolicitud = rd.GetInt32(rd.GetOrdinal("NroSolicitud"))
                oBE.TipoCaso = rd.GetString(rd.GetOrdinal("tipocaso"))
                oBE.TramiteSolicitado = rd.GetString(rd.GetOrdinal("tramitesolicitado"))
                oBE.MedioIngreso = rd.GetString(rd.GetOrdinal("medioingreso"))
                oBE.Dictamen = rd.GetString(rd.GetOrdinal("dictamen"))
                oBE.Estado = rd.GetString(rd.GetOrdinal("Estado"))
                oBE.FechaRegistro = rd.GetString(rd.GetOrdinal("FechaRegistro"))
                oBE.FechaRespuesta = rd.GetString(rd.GetOrdinal("FechaRespuesta"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BEBase Implements IDLNT.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function

End Class
