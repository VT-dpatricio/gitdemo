Imports VAN.Consulta.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLNTReclamo
    Inherits DLBase
    Implements IDLNT

    Public Function Seleccionar() As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pEntidad As BE.BEBase) As IList Implements IDLNT.Seleccionar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SC_ListarReclamo", cn)
        cm.CommandType = CommandType.StoredProcedure
        Dim oBE As BEReclamo = DirectCast(pEntidad, BEReclamo)
        cm.Parameters.Add("@IDCertificado", SqlDbType.VarChar, 25).Value = oBE.IdCertificado
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BEReclamo
                oBE.IDCertificado = rd.GetString(rd.GetOrdinal("idcertificado"))
                oBE.IDReclamo = rd.GetInt32(rd.GetOrdinal("IDReclamo"))
                oBE.NroTicket = rd.GetString(rd.GetOrdinal("NroTicket"))
                oBE.FechaAtencion = rd.GetString(rd.GetOrdinal("FechaAtencion"))
                oBE.FechaFinAtencion = rd.GetString(rd.GetOrdinal("FechaFinAtencion"))
                oBE.FechaReclamo = rd.GetString(rd.GetOrdinal("FechaReclamo"))
                oBE.EstadoReclamo = rd.GetString(rd.GetOrdinal("EstadoReclamo"))
                oBE.MotivoReclamo = rd.GetString(rd.GetOrdinal("MotivoReclamo"))
                oBE.NroReclamo = rd.GetString(rd.GetOrdinal("NroReclamo"))
                oBE.MedioDevolucion = rd.GetString(rd.GetOrdinal("MedioDevolucion"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BEBase Implements IDLNT.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function

End Class
