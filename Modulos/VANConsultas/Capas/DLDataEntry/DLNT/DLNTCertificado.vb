Imports VAN.Consulta.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLNTCertificado
    Inherits DLBase
    Implements IDLNT

    Public Function getNroCertificado(ByVal pCodigo As Integer) As BECertificado
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("DE_NroCertificado", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@IDProducto", SqlDbType.Int).Value = pCodigo
        Dim oBE As New BECertificado
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            If rd.Read Then
                oBE.IdCertificado = rd.GetString(rd.GetOrdinal("NroCertificado"))
            End If
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return oBE
    End Function

    Public Function Imprimir(ByVal pIdCertificado As String) As BECertificado
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("DE_ImprimirCertificado", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@idCertificado", SqlDbType.VarChar, 25).Value = pIdCertificado
        Dim oBE As New BECertificado
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            If rd.Read Then
                oBE.NumCertificado = rd.GetDecimal(rd.GetOrdinal("NumCertificado"))
                oBE.Vigencia = rd.GetDateTime(rd.GetOrdinal("Vigencia"))
                oBE.FinVigencia = rd.GetDateTime(rd.GetOrdinal("hasta"))
                oBE.PrimaTotalS = rd.GetString(rd.GetOrdinal("primaTotal"))
                oBE.Nombre1 = rd.GetString(rd.GetOrdinal("Asegurado"))
            End If
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return oBE
    End Function

    Public Function Seleccionar() As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pEntidad As BE.BEBase) As IList Implements IDLNT.Seleccionar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SC_Buscar", cn)
        cm.CommandType = CommandType.StoredProcedure
        Dim oBE As BECertificado = DirectCast(pEntidad, BECertificado)
        cm.Parameters.Add("@IDFiltro", SqlDbType.VarChar, 15).Value = oBE.IDFiltro
        cm.Parameters.Add("@IDUsuario", SqlDbType.VarChar, 50).Value = oBE.Usuario
        cm.Parameters.Add("@IDEntidad", SqlDbType.Int).Value = oBE.IDEntidad
        cm.Parameters.Add("@IDProducto", SqlDbType.Int).Value = oBE.IdProducto
        cm.Parameters.Add("@ValorABuscar", SqlDbType.VarChar, 20).Value = oBE.Numero
        cm.Parameters.Add("@Apellido", SqlDbType.VarChar, 200).Value = oBE.Apellido1
        cm.Parameters.Add("@Nombre", SqlDbType.VarChar, 200).Value = oBE.Nombre1
        cm.Parameters.Add("@FiltroAsegurado", SqlDbType.Bit).Value = oBE.FiltroAsegurado
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BECertificado
                oBE.IdCertificado = rd.GetString(rd.GetOrdinal("IdCertificado"))
                oBE.IdProducto = rd.GetInt32(rd.GetOrdinal("IDProducto"))
                oBE.Producto = rd.GetString(rd.GetOrdinal("Producto"))
                oBE.NumCertificado = rd.GetDecimal(rd.GetOrdinal("NumCertificado"))
                oBE.Estado = rd.GetString(rd.GetOrdinal("Estado"))
                oBE.TipoDocumento = rd.GetString(rd.GetOrdinal("TipoDocumento"))
                oBE.CcCliente = rd.GetString(rd.GetOrdinal("Nrodocumento"))
                oBE.Tipo = rd.GetString(rd.GetOrdinal("Tipo"))
                oBE.Nombre1 = rd.GetString(rd.GetOrdinal("Nombre"))
                oBE.Apellido1 = rd.GetString(rd.GetOrdinal("Apellido"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function SeleccionarCer(ByVal pIDCertificado As String) As BEBase
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SCAR_SeleccionarCertificadoxID", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@idCertificado", SqlDbType.VarChar, 25).Value = pIDCertificado
        Dim oBE As New BECertificado
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            If rd.Read Then
                'oBE.IdCertificado = rd.GetString(rd.GetOrdinal("IdCertificado"))

                '//Datos del Certificado-----------
                oBE.NumCertificado = rd.GetDecimal(rd.GetOrdinal("NumCertificado"))
                oBE.Raiz = rd.GetString(rd.GetOrdinal("Raiz"))
                oBE.NroFisico = rd.GetString(rd.GetOrdinal("NroFisico"))
                oBE.IdOficina = rd.GetInt32(rd.GetOrdinal("IdOficina"))
                oBE.CodOficina = rd.GetInt32(rd.GetOrdinal("CodOficina"))
                oBE.Oficina = rd.GetString(rd.GetOrdinal("Oficina"))
                oBE.IdInformador = rd.GetString(rd.GetOrdinal("IdInformador"))
                oBE.Informador = rd.GetString(rd.GetOrdinal("Informador"))
                oBE.IdEstadoCertificado = rd.GetInt32(rd.GetOrdinal("IdEstadoCertificado"))
                oBE.Venta = rd.GetDateTime(rd.GetOrdinal("Venta"))
                oBE.Digitacion = rd.GetDateTime(rd.GetOrdinal("Digitacion"))
                oBE.Vigencia = rd.GetString(rd.GetOrdinal("Vigencia"))
                oBE.FinVigencia = rd.GetString(rd.GetOrdinal("FinVigencia"))
                oBE.SolicitudAnulacion = rd.GetString(rd.GetOrdinal("SolicitudAnulacion"))
                oBE.DigitacionAnulacion = rd.GetString(rd.GetOrdinal("DigitacionAnulacion"))
                oBE.EfectuarAnulacion = rd.GetString(rd.GetOrdinal("EfectuarAnulacion"))
                oBE.IdMotivoAnulacion = rd.GetInt32(rd.GetOrdinal("IdMotivoAnulacion"))
                oBE.Observacion = rd.GetString(rd.GetOrdinal("Observacion"))
                oBE.UsuarioModificacion = rd.GetString(rd.GetOrdinal("UsuarioModificacion"))
                oBE.IdTipoProducto = rd.GetString(rd.GetOrdinal("idTipoProducto"))


                '//Información del Titular--------
                oBE.IdTipoDocumento = rd.GetString(rd.GetOrdinal("IdTipoDocumento"))
                oBE.TipoDocumento = rd.GetString(rd.GetOrdinal("TipoDocumento"))
                oBE.CcCliente = rd.GetString(rd.GetOrdinal("CcCliente"))
                oBE.Nombre1 = rd.GetString(rd.GetOrdinal("Nombre1"))
                oBE.Nombre2 = rd.GetString(rd.GetOrdinal("Nombre2"))
                oBE.Apellido1 = rd.GetString(rd.GetOrdinal("Apellido1"))
                oBE.Apellido2 = rd.GetString(rd.GetOrdinal("Apellido2"))
                oBE.Direccion = rd.GetString(rd.GetOrdinal("Direccion"))
                oBE.DirNumero = rd.GetString(rd.GetOrdinal("DirNumero"))
                oBE.DirPiso = rd.GetString(rd.GetOrdinal("DirPiso"))
                oBE.DirDpto = rd.GetString(rd.GetOrdinal("DirDpto"))
                oBE.DirCodPostal = rd.GetString(rd.GetOrdinal("DirCodPostal"))
                oBE.IdCiudad = rd.GetInt32(rd.GetOrdinal("IdCiudad"))
                oBE.IDProvincia = rd.GetInt32(rd.GetOrdinal("IDProvincia"))
                oBE.IDDepartamento = rd.GetInt32(rd.GetOrdinal("IDDepartamento"))
                oBE.IDPais = rd.GetInt32(rd.GetOrdinal("IDPais"))
                oBE.Ciudad = rd.GetString(rd.GetOrdinal("Ciudad"))
                oBE.Provincia = rd.GetString(rd.GetOrdinal("Provincia"))
                oBE.Email = rd.GetString(rd.GetOrdinal("Email"))
                oBE.CodAreaTelefono = rd.GetString(rd.GetOrdinal("CodAreaTelefono"))
                oBE.Telefono = rd.GetString(rd.GetOrdinal("Telefono"))
                oBE.CodAreaOtroTelefono = rd.GetString(rd.GetOrdinal("CodAreaOtroTelefono"))
                oBE.OtroTelefono = rd.GetString(rd.GetOrdinal("OtroTelefono"))
                oBE.Celular = rd.GetString(rd.GetOrdinal("Celular"))
                oBE.EnvioMailPoliza = rd.GetString(rd.GetOrdinal("EnviaMailPoliza"))
                oBE.Sigla = rd.GetString(rd.GetOrdinal("Sigla"))


                '//Domicilio del Riesgo Asegurado---------
                oBE.DRADireccion = rd.GetString(rd.GetOrdinal("DRADireccion"))
                oBE.DRADirNumero = rd.GetString(rd.GetOrdinal("DRADirNumero"))
                oBE.DRADirPiso = rd.GetString(rd.GetOrdinal("DRADirPiso"))
                oBE.DRADirDpto = rd.GetString(rd.GetOrdinal("DRADirDpto"))
                oBE.DRADirCodPostal = rd.GetString(rd.GetOrdinal("DRADirCodPostal"))
                oBE.DRALocalidad = rd.GetString(rd.GetOrdinal("DRALocalidad"))
                oBE.DRAProvincia = rd.GetString(rd.GetOrdinal("DRAProvincia"))
                oBE.DRAIDCiudad = rd.GetInt32(rd.GetOrdinal("DRAIDCiudad"))


                '//Información Adicional del Asegurado-------
                oBE.EstadoCivil = rd.GetString(rd.GetOrdinal("EstadoCivil"))
                oBE.Sexo = rd.GetString(rd.GetOrdinal("Sexo"))
                oBE.FechaNacimiento = rd.GetString(rd.GetOrdinal("FechaNacimiento"))
                oBE.CUIT = rd.GetString(rd.GetOrdinal("CUIT"))
                oBE.Nacimiento = rd.GetString(rd.GetOrdinal("Nacimiento"))
                oBE.Nacionalidad = rd.GetString(rd.GetOrdinal("Nacionalidad"))
                oBE.Profesion = rd.GetString(rd.GetOrdinal("Profesion"))
                oBE.CondicionIVA = rd.GetString(rd.GetOrdinal("CondicionIva"))

                '//Información de Pago--------------
                oBE.Producto = rd.GetString(rd.GetOrdinal("Producto"))
                oBE.IdProducto = rd.GetInt32(rd.GetOrdinal("IdProducto"))
                oBE.Opcion = rd.GetString(rd.GetOrdinal("Opcion"))
                oBE.Frecuencia = rd.GetString(rd.GetOrdinal("Frecuencia"))
                oBE.IdMonedaPrima = rd.GetString(rd.GetOrdinal("IdMonedaPrima"))
                oBE.PrimaCobrar = rd.GetDecimal(rd.GetOrdinal("PrimaCobrar"))
                oBE.PrimaBruta = rd.GetDecimal(rd.GetOrdinal("PrimaBruta"))
                oBE.PrimaTotal = rd.GetDecimal(rd.GetOrdinal("PrimaTotal"))
                oBE.IdMedioPago = rd.GetString(rd.GetOrdinal("IdMedioPago"))
                oBE.MedioPago = rd.GetString(rd.GetOrdinal("MedioPago"))
                oBE.NumeroCuenta = rd.GetString(rd.GetOrdinal("NumeroCuenta"))
                oBE.SimboloMoneda = rd.GetString(rd.GetOrdinal("SimboloMoneda"))
                oBE.EstadoVenta = rd.GetString(rd.GetOrdinal("EstadoVenta"))
                oBE.CBU = rd.GetString(rd.GetOrdinal("CBU"))











                oBE.MontoAsegurado = rd.GetDecimal(rd.GetOrdinal("MontoAsegurado"))
                'oBE.IVA = rd.GetDecimal(rd.GetOrdinal("IVA"))
                'oBE.Vencimiento = rd.GetString(rd.GetOrdinal("Vencimiento"))
                'oBE.Consistente = rd.GetBoolean(rd.GetOrdinal("Consistente"))
                'oBE.IdMonedaCobro = rd.GetString(rd.GetOrdinal("IdMonedaCobro"))
                'oBE.NumTarjAsegurada = rd.GetString(rd.GetOrdinal("NumTarjAsegurada"))
                'oBE.MonedaCuenta = rd.GetString(rd.GetOrdinal("MonedaCuenta"))
                'oBE.FechaActivacion = rd.GetString(rd.GetOrdinal("FechaActivacion"))
          


                'oBE. = rd.GetInt32(rd.GetOrdinal(""))
                ' oBE.= rd.GetDecimal(rd.GetOrdinal(""))
                'oBE. = rd.GetString(rd.GetOrdinal(""))

                'Información de Pago
                

            End If
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return oBE
    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BEBase Implements IDLNT.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function
End Class
