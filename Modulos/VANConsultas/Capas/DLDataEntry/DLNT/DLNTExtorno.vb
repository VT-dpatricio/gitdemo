Imports VAN.Consulta.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLNTExtorno
    Inherits DLBase
    Implements IDLNT



    Public Function Seleccionar() As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(ByVal pIDCertificado As String, ByVal pConsecutivo As Integer) As BEAsegurado
              Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pEntidad As BE.BEBase) As IList Implements IDLNT.Seleccionar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SC_SeleccionarExtorno", cn)
        cm.CommandType = CommandType.StoredProcedure
        Dim oBE As BEExtorno = DirectCast(pEntidad, BEExtorno)
        cm.Parameters.Add("@IDCertificado", SqlDbType.VarChar, 25).Value = oBE.IdCertificado
        cm.Parameters.Add("@IdCobro", SqlDbType.Int).Value = oBE.IdCobro
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BEExtorno
                oBE.IdCertificado = rd.GetString(rd.GetOrdinal("IdCertificado"))
                oBE.IdCobro = rd.GetInt32(rd.GetOrdinal("IdCobro"))
                oBE.Monto = rd.GetDecimal(rd.GetOrdinal("Monto"))
                oBE.Idmoneda = rd.GetString(rd.GetOrdinal("Idmoneda"))
                oBE.TipoCambio = rd.GetDecimal(rd.GetOrdinal("TipoCambio"))
                oBE.Motivo = rd.GetString(rd.GetOrdinal("Motivo"))
                oBE.FechaSolicitud = rd.GetDateTime(rd.GetOrdinal("FechaSolicitud"))
                oBE.FechaExtorno = rd.GetDateTime(rd.GetOrdinal("FechaExtorno"))
                oBE.FechaCierre = rd.GetDateTime(rd.GetOrdinal("FechaCierre"))
                oBE.Activo = rd.GetInt32(rd.GetOrdinal("Activo"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BEBase Implements IDLNT.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function

End Class
