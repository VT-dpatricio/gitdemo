Imports VAN.Consulta.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLNTSiniestro
    Inherits DLBase
    Implements IDLNT

    Public Function Seleccionar() As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pEntidad As BE.BEBase) As IList Implements IDLNT.Seleccionar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SC_ListarSiniestro", cn)
        cm.CommandType = CommandType.StoredProcedure
        Dim oBE As BESiniestro = DirectCast(pEntidad, BESiniestro)
        cm.Parameters.Add("@IDCertificado", SqlDbType.VarChar, 25).Value = oBE.IdCertificado
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BESiniestro
                oBE.IDCertificado = rd.GetString(rd.GetOrdinal("idcertificado"))
                oBE.NroTicket = rd.GetString(rd.GetOrdinal("NroTicket"))
                oBE.IDSiniestro = rd.GetInt32(rd.GetOrdinal("IDSiniestro"))
                oBE.EstadoSiniestro = rd.GetString(rd.GetOrdinal("EstadoSiniestro"))
                oBE.TipoSiniestro = rd.GetString(rd.GetOrdinal("TipoSiniestro"))
                oBE.FechaSiniestro = rd.GetString(rd.GetOrdinal("FechaSiniestro"))
                oBE.MontoSiniestro = rd.GetDecimal(rd.GetOrdinal("MontoSiniestro"))
                oBE.IDMonedaMSiniestro = rd.GetString(rd.GetOrdinal("IDMonedaMSiniestro"))
                oBE.MontoPago = rd.GetDecimal(rd.GetOrdinal("MontoPago"))
                oBE.IDMonedaMPago = rd.GetString(rd.GetOrdinal("IDMonedaMPago"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BEBase Implements IDLNT.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function

End Class
