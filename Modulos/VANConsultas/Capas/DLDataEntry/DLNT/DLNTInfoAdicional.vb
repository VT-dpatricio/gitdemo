﻿Imports VAN.Consulta.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLNTInfoAdicional
    Inherits DLBase
    Implements IDLNT


    Public Function Seleccionar() As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(pEntidad As BEBase) As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(pCodigo As Integer) As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(pCodigo As Integer) As BEBase Implements IDLNT.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarDatosAdicionales(ByVal pIDCertificado As String, ByVal tipoInfo As String) As IList
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SC_PRC_SEL_InfoAdicionalAsegurado", cn)
        cm.CommandType = CommandType.StoredProcedure
        Dim oBE As BEInfoAdicional
        cm.Parameters.Add("@IDCertificado", SqlDbType.VarChar, 25).Value = pIDCertificado
        cm.Parameters.Add("@IDGrupo", SqlDbType.VarChar, 3).Value = tipoInfo
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BEInfoAdicional
                oBE.Valor = rd.GetString(rd.GetOrdinal("Valor"))
                oBE.Nombre = rd.GetString(rd.GetOrdinal("Nombre"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function
End Class
