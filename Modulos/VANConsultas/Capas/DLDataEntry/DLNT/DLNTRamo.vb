﻿Imports VAN.Consulta.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLNTRamo
    Inherits DLBase
    Implements IDLNT

    Public Function Seleccionar() As IList Implements IDLNT.Seleccionar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("SC_SeleccionarRamo", cn)
        cm.CommandType = CommandType.StoredProcedure
        'cm.Parameters.Add("@IdRamo", SqlDbType.Int).Value = pCodigo
        Dim oBE As BERamo
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BERamo
                oBE.IdRamo = rd.GetInt32(rd.GetOrdinal("IdRamo"))
                oBE.Nombre = rd.GetString(rd.GetOrdinal("Nombre"))
                oBE.Grupo = rd.GetString(rd.GetOrdinal("Grupo"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista

    End Function

    Public Function Seleccionar(ByVal pEntidad As BE.BEBase) As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As IList Implements IDLNT.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BEBase Implements IDLNT.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function
End Class
