﻿Imports VAN.Consulta.BE
Imports VAN.Consulta.DL
Imports System
Imports System.Collections

Public Class BLProducto

    Public Function Seleccionar(ByVal pIDEntidad As Integer, ByVal pIDUsuario As String) As IList
        Dim oDL As New DLNTProducto
        Dim oBE As New BEProducto
        oBE.IDEntidad = pIDEntidad
        oBE.IDUsuario = pIDUsuario
        oBE.Opcion = "SeleccionarActivos"
        Return oDL.Seleccionar(oBE)
    End Function

    Public Function SeleccionarTodos(ByVal pIDEntidad As Integer, ByVal pIDUsuario As String) As IList
        Dim oDL As New DLNTProducto
        Dim oBE As New BEProducto
        oBE.IDEntidad = pIDEntidad
        oBE.IDUsuario = pIDUsuario
        oBE.Opcion = "SeleccionarTodos"
        Return oDL.Seleccionar(oBE)
    End Function

    Public Function Seleccionar(ByVal pIDProducto As Integer) As BEProducto
        Dim oDL As New DLNTProducto
        Return DirectCast(oDL.SeleccionarBE(pIDProducto), BEProducto)
    End Function

End Class


