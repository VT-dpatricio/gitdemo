Imports VAN.Consulta.BE
Imports VAN.Consulta.DL
Imports System
Imports System.Collections
Public Class BLExtorno
    Public Function Seleccionar(ByVal pIDCobro As Int32, ByVal pIDCertificado As String) As IList
        Dim oDL As New DLNTExtorno
        Dim oBE As New BEExtorno
        oBE.IdCobro = pIDCobro
        oBE.IdCertificado = pIDCertificado
        Return oDL.Seleccionar(oBE)
    End Function
End Class
