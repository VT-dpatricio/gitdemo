Imports VAN.Consulta.BE
Imports VAN.Consulta.DL
Imports System
Imports System.Collections
Imports System.Configuration
Public Class BLSiniestro

    Public Function Listar(ByVal pIDCertificado As String) As IList
        Dim oDL As New DLNTSiniestro
        Dim oBE As New BESiniestro
        oBE.IDCertificado = pIDCertificado
        Return oDL.Seleccionar(oBE)
    End Function

End Class
