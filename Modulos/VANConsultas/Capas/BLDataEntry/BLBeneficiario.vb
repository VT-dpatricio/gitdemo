Imports VAN.Consulta.BE
Imports VAN.Consulta.DL
Imports System
Imports System.Collections
Public Class BLBeneficiario
    Public Function Actualizar(ByVal pIDEntidad As BEBeneficiario) As Boolean
        Dim oDL As New DLTBeneficiario
        Return oDL.Actualizar(pIDEntidad)
    End Function

    Public Function Anular(ByVal pIDBeneficiario As Integer, ByVal pIDCertificado As String, ByVal pIDConsecutivo As Integer, ByVal pIdUsuarioMod As String, ByVal pObservacion As String) As Boolean
        Dim oDL As New DLTBeneficiario
        Dim oBE As New BEBeneficiario
        oBE.IdCertificado = pIDCertificado
        oBE.IdBeneficiario = pIDBeneficiario
        oBE.UsuarioModificacion = pIdUsuarioMod
        oBE.Observacion = pObservacion
        oBE.Consecutivo = pIDConsecutivo
        Return oDL.Anular(oBE)
    End Function

    Public Function Seleccionar(ByVal pIdBeneficiario As Integer) As BEBeneficiario
        Dim oDL As New DLNTBeneficiario
        Return oDL.SeleccionarBEN(pIdBeneficiario)
    End Function

    Public Function Seleccionar(ByVal pIDCertificado As String) As IList
        Dim oDL As New DLNTBeneficiario
        Dim oBE As New BEBeneficiario
        oBE.IdCertificado = pIDCertificado
        Return oDL.Seleccionar(oBE)
    End Function
End Class
