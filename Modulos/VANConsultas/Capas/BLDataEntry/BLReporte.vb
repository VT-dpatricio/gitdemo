﻿Imports VAN.Consulta.BE
Imports VAN.Consulta.DL
Imports System
Imports System.Collections

Public Class BLReporte
    Public Function Seleccionar(ByVal pIDCertificado As String, ByVal tipo As String) As BEReporte
        Dim oDL As New DLNTReporte
        Dim oBE As New BEReporte

        If tipo = "N" Then
            Return oDL.SeleccionarNota(pIDCertificado)
        Else
            Return oBE
        End If
    End Function
End Class
