Imports VAN.Consulta.BE
Imports VAN.Consulta.DL
Imports System
Imports System.Collections
Imports System.Configuration
Public Class BLReclamo

    Public Function Listar(ByVal pIDCertificado As String) As IList
        Dim oDL As New DLNTReclamo
        Dim oBE As New BEReclamo
        oBE.IDCertificado = pIDCertificado
        Return oDL.Seleccionar(oBE)
    End Function

End Class
