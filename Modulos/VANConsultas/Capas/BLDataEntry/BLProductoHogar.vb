﻿Imports VAN.Consulta.BE
Imports VAN.Consulta.DL
Imports System
Imports System.Collections

Public Class BLProductoHogar
    Inherits BEBase

    Public Function Actualizar(ByVal pIDEntidad As BEProductoHogar) As Boolean
        Dim oDL As New DLTProductoHogar
        Return oDL.Actualizar(pIDEntidad)
    End Function

    Public Function Seleccionar(ByVal pIDCertificado As String) As IList
        Dim oDL As New DLNTProductoHogar
        Return oDL.Seleccionar(pIDCertificado)
    End Function
End Class
