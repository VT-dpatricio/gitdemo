﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using AONArchitectural.Common.Processor.Validation;
using AONArchitectural.Common.Processor.Flow;

namespace AONArchitectural.Common.Processor.Manager
{
    public class ProcessFlowManager<PR> 
        where PR: IValidator,new()
    {

        private static ProcessFlowManager<PR> instance;

        public static ProcessFlowManager<PR> GetInstance()
        {
            if (instance == null)
            {
                instance = new ProcessFlowManager<PR>();
            }
            return instance;
        }

        public IValidator RunProcess(IProcessFlow processFlow)
        {
            IValidator completeProcessResult = new PR();
            IValidator preProcessResult = new PR();
            IValidator principalProcessResult = new PR();
            IValidator postProcessResult = new PR();

            preProcessResult = processFlow.PreProcess();
            if (preProcessResult.IsOk)
            {
                principalProcessResult = processFlow.Process();
                if (principalProcessResult.IsOk)
                {
                    postProcessResult = processFlow.PostProcess();
                }
            }

            completeProcessResult.AddChildResult(preProcessResult);
            completeProcessResult.AddChildResult(principalProcessResult);
            completeProcessResult.AddChildResult(postProcessResult);
            completeProcessResult.IsOk = preProcessResult.IsOk && principalProcessResult.IsOk && postProcessResult.IsOk;

            return completeProcessResult;
        }

    }
}
