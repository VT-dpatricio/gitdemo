﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AONArchitectural.Common.Processor.Formatters
{
    public class StringDateFormatter : Formatter
    {
        public StringDateFormatter() : base() { }
        public StringDateFormatter(int errorCode) : base(errorCode) { }

        public override string Format(string value)
        {
            /// <summary>
            /// =========================================================================
            /// El formato de fecha esperado como parametro es AAAAMMDD el formater
            /// =========================================================================
            /// </summary>

            //DateTime AuxDate = new DateTime(1900, 01, 01);

            string aux = value.Substring(6, 2) + "/" + value.Substring(4, 2) + "/" + value.Substring(0, 4);
            //if (DateTime.TryParse(aux, out AuxDate))
            //{
            //    AuxDate = DateTime.Parse(aux);
            //}
            //return AuxDate.ToString();

            DateTime.Parse(aux);
            return aux;
        }
    }
}
