﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AONArchitectural.Common.Processor.Formatters
{
    public class StringDecimalFormatter : Formatter
    {
                public StringDecimalFormatter() : base() { }
                public StringDecimalFormatter(int errorCode) : base(errorCode) { }
        
        public override string Format(string value)
        {
            decimal valorAux = decimal.Parse(value);

            return value;
        }
    }
}
