﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace AONArchitectural.Common.Pdf
{
    public class PdfFunction
    {

        public Byte[] MergePage(Byte[] pdfResponse, string[] sourceFiles)
        {
            byte[] mergedPdf = null;
            using (MemoryStream ms = new MemoryStream())
            {
                using (Document document = new Document())
                {
                    using (PdfCopy copy = new PdfCopy(document, ms))
                    {
                        document.Open();

                        PdfReader reader = null;
                        int n = 0;
                        if (pdfResponse != null && pdfResponse.Length > 0)
                        {
                            //En este conjunto de byte se procesa el request de reporting service con el rdl devuelto
                            reader = new PdfReader(pdfResponse);
                            n = reader.NumberOfPages;
                            for (int page = 0; page < n; )
                            {
                                copy.AddPage(copy.GetImportedPage(reader, ++page));
                            }
                        }
                        for (int i = 0; i < sourceFiles.Length -1; ++i)
                        {
                            //Documento que se van a anexar en forma encadenada
                            reader = new PdfReader(sourceFiles[i]);
                            // loop over the pages in that document
                            n = reader.NumberOfPages;
                            for (int page = 0; page < n; )
                            {
                                copy.AddPage(copy.GetImportedPage(reader, ++page));
                            }
                        }
                    }
                }
                mergedPdf = ms.ToArray();
            }
            return mergedPdf;
        }
        
    }
}
