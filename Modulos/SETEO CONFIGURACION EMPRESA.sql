USE BDJanus


UPDATE dbo.Entidad SET nombre='BNP Paribas CARDIF', sigla='BNP'

UPDATE dbo.Asegurador SET nombre='BNP CARDIF', Sigla='BNPC'

UPDATE dbo.Producto SET nombre='BNP - Accidentes Personales' where idProducto='5216'
UPDATE dbo.Producto SET nombre='BNP - Bolso Protegido' where idProducto='5447'
UPDATE dbo.Producto SET nombre='BNP - Hogar' where idProducto='5457'


--ACTUALIZO LAS IMPRESIONES CON EL NOMBRE DEL RDL CORRESPONDIENTE
DELETE dbo.BW_ProductoImpresion WHERE IDProducto NOT IN (5220,5437,5448, 5216, 5447, 5457)

UPDATE dbo.BW_ProductoImpresion set IDProducto=5216, NombreArchivo='Certificado_BNP'  where IDProducto=5220 and TipoImpresion='C'

UPDATE dbo.BW_ProductoImpresion set IDProducto=5447, IDTipoProducto='P', NombreArchivo='Certificado_BNP' where IDProducto=5437 and TipoImpresion='C'

UPDATE dbo.BW_ProductoImpresion set IDProducto=5457, IDTipoProducto='PP', NombreArchivo='Certificado_BNP'  where IDProducto=5448 and TipoImpresion='C'


UPDATE dbo.BW_ProductoImpresion set IDProducto=5216, NombreArchivo='Certificado_BNP'  where IDProducto=5216 and TipoImpresion='C'

UPDATE dbo.BW_ProductoImpresion set IDProducto=5447, IDTipoProducto='P', NombreArchivo='Certificado_BNP' where IDProducto=5447 and TipoImpresion='C'

UPDATE dbo.BW_ProductoImpresion set IDProducto=5457, IDTipoProducto='PP', NombreArchivo='Certificado_BNP'  where IDProducto=5457 and TipoImpresion='C'
