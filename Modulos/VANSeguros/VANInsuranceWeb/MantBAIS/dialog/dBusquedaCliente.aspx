﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="dBusquedaCliente.aspx.vb"
    Inherits="VANInsurance.dBusquedaCliente" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BNP PARIBAS CARDIF - Venta Seguros</title>
    <link href="../../Estilos/Default.css" type="text/css" rel="stylesheet" />
    <link href="../../Estilos/General.css" type="text/css" rel="stylesheet" />
    <link href="../../Estilos/Menu.css" type="text/css" rel="stylesheet" />

    <script src="../../Scripts/General.js" type="text/javascript"></script>

    <script type="text/javascript">
        function CerrarPagina(valor) {
            var parentWindow = window.parent;
            parentWindow.OcultarPCModalBusqueda(valor);
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div class="BodyMP">
        <asp:Panel ID="Panel1" runat="server" Width="90%" GroupingText="Busqueda de Cliente">
            <table width="600px" class="BodyMP">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    Nro
                                </td>
                                <td>
                                    <asp:Label ID="lblNombreDocumento" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <asp:TextBox ID="txtNroDocumento" runat="server" MaxLength="8" Width="120px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvtxtNroDocumento" runat="server" ErrorMessage="(*)"
                            ControlToValidate="txtNroDocumento" ValidationGroup="rfvBuscar"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:Button ID="btnBuscar1" runat="server" Text="Buscar" class="ButtonGrid" ValidationGroup="rfvBuscar" />
                    </td>
                </tr>
            </table>
            <hr />
            <table width="600px" class="BodyMP">
                <tr>
                    <td>
                        Nombre 1 :
                    </td>
                    <td>
                        <asp:TextBox ID="txtNombre1" runat="server" Width="150px" Style="text-transform: uppercase"></asp:TextBox>
                    </td>
                    <td>
                        Nombre 2 :
                    </td>
                    <td>
                        <asp:TextBox ID="txtNombre2" runat="server" Width="150px" Style="text-transform: uppercase"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Apellido 1 :
                    </td>
                    <td>
                        <asp:TextBox ID="txtApellido1" runat="server" Width="150px" Style="text-transform: uppercase"></asp:TextBox>
                    </td>
                    <td>
                        Apellido 2 :
                    </td>
                    <td>
                        <asp:TextBox ID="txtApellido2" runat="server" Width="150px" Style="text-transform: uppercase"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Fec. Nacimiento :
                    </td>
                    <td>
                        <asp:TextBox ID="txtFecNac" runat="server" Width="150px"></asp:TextBox>
                    </td>
                    <td>
                        <%--Codigo INEI--%>
                    </td>
                    <td>
                        <%-- <asp:TextBox ID="txtCodigoINEI" runat="server" Width="150px"></asp:TextBox>--%>
                    </td>
                </tr>
                <tr>
                    <td>
                        Telefono
                    </td>
                    <td>
                        <asp:TextBox ID="txtTelefono" runat="server" Width="120px"></asp:TextBox>
                    </td>
                    <td>
                        Ocupacion
                    </td>
                    <td>
                        <asp:TextBox ID="txtOcupacion" runat="server" Width="120px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Direccion
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="txtDireccion" runat="server" Width="250px" Style="text-transform: uppercase"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="left">
                        Cuentas :
                        <br />
                        <asp:ListBox ID="lbCuentas" runat="server" Width="100%"></asp:ListBox>
                        <asp:RequiredFieldValidator ID="rfvlbCuentas" runat="server" ErrorMessage="(*)" ControlToValidate="lbCuentas"
                            ValidationGroup="rfvSeleccion"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="right">
                        <asp:Button ID="btnAceptar" runat="server" Text="Aceptar" class="ButtonGrid" ValidationGroup="rfvSeleccion" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Label ID="lblError" runat="server" Text="" class="ButtonGrid"></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
