﻿Imports VAN.InsuranceWeb.BE
Partial Public Class dMedioPago
    Inherits System.Web.UI.Page

#Region " Propiedades "


    ReadOnly Property IdPosicion() As Integer
        Get
            Return Request.QueryString("Id")
        End Get
    End Property

    Dim _Entity As New ProductoMedioPagoBE
    Public Property Entity() As ProductoMedioPagoBE
        Get
            Return _Entity
        End Get
        Set(ByVal value As ProductoMedioPagoBE)
            _Entity = value
        End Set
    End Property


#End Region

#Region " Eventos "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Expires = 0
        If Not Page.IsPostBack Then
            If IdPosicion <> 0 Then
                Entity = DirectCast(Session("ListProductoMedioPagoBE"), List(Of ProductoMedioPagoBE)).Item(Request.QueryString("Id") - 1)
                ddlMedioPago.SelectedValue = Entity.idMedioPago
                ddlMonedaCobro.SelectedValue = Entity.IdMonedaCobro
            End If
        End If

    End Sub

    Private Sub btnGrabar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        Try

            Entity.idMedioPago = ddlMedioPago.SelectedValue
            Entity.MedioPago = ddlMedioPago.SelectedItem.Text
            Entity.IdMonedaCobro = ddlMonedaCobro.SelectedValue
            Entity.MonedaCobro = ddlMonedaCobro.SelectedItem.Text


            If IdPosicion = 0 Then

                For i As Integer = 0 To DirectCast(Session("ListProductoMedioPagoBE"), List(Of ProductoMedioPagoBE)).Count - 1
                    If DirectCast(Session("ListProductoMedioPagoBE"), List(Of ProductoMedioPagoBE)).Item(i).idMedioPago = Entity.idMedioPago And _
                    DirectCast(Session("ListProductoMedioPagoBE"), List(Of ProductoMedioPagoBE)).Item(i).IdMonedaCobro = Entity.IdMonedaCobro Then
                        Dim msn As String = "<Script language=""JavaScript"">alert('Medio y Moneda ya se encuentra registrado.');</Script>"
                        ClientScript.RegisterStartupScript(GetType(String), "clientScript", msn)
                        Exit Sub
                    End If
                Next

                DirectCast(Session("ListProductoMedioPagoBE"), List(Of ProductoMedioPagoBE)).Add(Entity)
            Else

                For i As Integer = 0 To DirectCast(Session("ListProductoMedioPagoBE"), List(Of ProductoMedioPagoBE)).Count - 1
                    If (IdPosicion - 1) <> 0 AndAlso DirectCast(Session("ListProductoMedioPagoBE"), List(Of ProductoMedioPagoBE)).Item(i).idMedioPago = Entity.idMedioPago And _
                    DirectCast(Session("ListProductoMedioPagoBE"), List(Of ProductoMedioPagoBE)).Item(i).IdMonedaCobro = Entity.IdMonedaCobro Then
                        Dim msn As String = "<Script language=""JavaScript"">alert('Medio y Moneda ya se encuentra registrado.');</Script>"
                        ClientScript.RegisterStartupScript(GetType(String), "clientScript", msn)
                        Exit Sub
                    End If
                Next



                DirectCast(Session("ListProductoMedioPagoBE"), List(Of ProductoMedioPagoBE)).Item(IdPosicion - 1) = Entity
            End If
            OrdernarLista()

            Dim strScript As String = "<Script language=""JavaScript"">CerrarPagina('MedioPago');</Script>"
            ClientScript.RegisterStartupScript(GetType(String), "clientScript", strScript)

        Catch ex As Exception

        End Try
    End Sub

    Sub OrdernarLista()
        For i As Integer = 0 To DirectCast(Session("ListProductoMedioPagoBE"), List(Of ProductoMedioPagoBE)).Count - 1
            DirectCast(Session("ListProductoMedioPagoBE"), List(Of ProductoMedioPagoBE)).Item(i).Id = i + 1
        Next
    End Sub

#End Region

End Class