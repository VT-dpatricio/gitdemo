﻿Imports VAN.InsuranceWeb.BE

Partial Public Class ProductoRegistro
    Inherits PaginaBase

#Region " Property "


    Private _EntProductoBE As ProductoBE
    Public Property EntProductoBE() As ProductoBE
        Get
            Return _EntProductoBE
        End Get
        Set(ByVal value As ProductoBE)
            _EntProductoBE = value
        End Set
    End Property


    Public ReadOnly Property ItemSubmitted() As String
        Get
            Return Request.QueryString("ItemSubmitted")
        End Get
    End Property

    Public ReadOnly Property IdProducto() As Byte
        Get
            Return Convert.ToByte(Request.QueryString("IdProducto"))
        End Get
    End Property
#End Region

#Region " Sessiones "

    Sub IniciarSessions()

        Dim ListParentescoPermitidoBE As New List(Of ParentescoPermitidoBE)
        Dim ListTipoDocProductoBE As New List(Of TipoDocProductoBE)

        Dim ListInfoProductoBE As New List(Of InfoProductoBE)
        Dim ListInfoAseguradoBE As New List(Of InfoAseguradoBE)
        Dim ListOpcionBE As New List(Of OpcionBE)
        Dim ListOpcionPrimaBE As New List(Of OpcionPrimaBE)
        Dim ListProductoMedioPagoBE As New List(Of ProductoMedioPagoBE)


        Session("ListParentescoPermitidoBE") = ListParentescoPermitidoBE
        Session("ListTipoDocProductoBE") = ListTipoDocProductoBE

        Session("ListInfoProductoBE") = ListInfoProductoBE
        Session("ListInfoAseguradoBE") = ListInfoAseguradoBE
        Session("ListOpcionBE") = ListOpcionBE
        Session("ListOpcionPrimaBE") = ListOpcionPrimaBE
        Session("ListProductoMedioPagoBE") = ListProductoMedioPagoBE



    End Sub


#End Region

#Region " Carga de Grillas "


    Sub KeyPressCajas()
        txtMaxAsegurados.Attributes.Add("onkeypress", "return SoloNumeros(event);")
        txtMaxEdadPerm.Attributes.Add("onkeypress", "return SoloNumeros(event);")
        txtMaxPolizasAseg.Attributes.Add("onkeypress", "return SoloNumeros(event);")
        txtMaxPolizasCtaHb.Attributes.Add("onkeypress", "return SoloNumeros(event);")
        txtMaxPolizasRegalo.Attributes.Add("onkeypress", "return SoloNumeros(event);")
        txtMaxCtaTitular.Attributes.Add("onkeypress", "return SoloNumeros(event);")
        txtMaxPolizasSDNI.Attributes.Add("onkeypress", "return SoloNumeros(event);")
        'txtCumuloMaximo.Attributes.Add("onkeypress", "return SoloNumerosPunto(event);")
        txtMaxDiasAnu.Attributes.Add("onkeypress", "return SoloNumeros(event);")

        txtMinimo.Attributes.Add("onkeypress", "return SoloNumeros(event);")
        txtMaximo.Attributes.Add("onkeypress", "return SoloNumeros(event);")

    End Sub

    Sub CargaGrillas()

        gvInfoProducto.DataSource = Session("ListInfoProductoBE")
        gvInfoProducto.DataBind()


        gvInfoAsegurado.DataSource = Session("ListInfoAseguradoBE")
        gvInfoAsegurado.DataBind()

        gvOpcion.DataSource = Session("ListOpcionBE")
        gvOpcion.DataBind()

        gvPlanes.DataSource = Session("ListOpcionPrimaBE")
        gvPlanes.DataBind()

        gvMediosPago.DataSource = Session("ListProductoMedioPagoBE")
        gvMediosPago.DataBind()
    End Sub

#End Region

#Region " Page "



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        KeyPressCajas()


        'MyMaster = CType(Master, Mantenimiento)
        'MyMaster.Editar_RenderDisabled()
        'MyMaster.OcultarMenu()

        If Not Page.IsPostBack Then
            If Not ValidarAcceso(Page.AppRelativeVirtualPath) Then
                Response.Redirect("~/Login.aspx", False)
            End If
            If ValidarEstadoClaveUsuario() Then
                Response.Redirect("~/Seguridad/CambiarClave.aspx", False)
            End If

            IniciarSessions()
        End If
        CargaGrillas()


        'If ItemSubmitted.Equals("Modificar") Then
        '    odsProducto.Select()
        'End If



    End Sub

#End Region



#Region " Eventos "

    Private Sub ibtnAdelante1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnAdelante1.Click

        If lbParentescos.SelectedValue Is Nothing Then
            Exit Sub
        End If

        If lbParentescos.SelectedValue = "" Then
            Exit Sub
        End If

        Dim Entity As New ParentescoPermitidoBE
        Entity.idParentesco = lbParentescos.SelectedValue
        Entity.Parentesco = lbParentescos.SelectedItem.Text


        For i As Integer = 0 To lbParentescosProducto.Items.Count - 1
            If lbParentescosProducto.Items(i).Value = Entity.idParentesco Then
                Exit Sub
            End If
        Next

        If Entity.idParentesco = 1 Then
            lbParentescos.Enabled = False
            lbParentescosProducto.Items.Clear()
            lbParentescosProducto.Items.Insert(0, New System.Web.UI.WebControls.ListItem(Entity.Parentesco, Entity.idParentesco))
            Exit Sub
        End If


        lbParentescosProducto.Items.Insert(0, New System.Web.UI.WebControls.ListItem(Entity.Parentesco, Entity.idParentesco))




        'DirectCast(Session("ListParentescoPermitidoBE"), List(Of ParentescoPermitidoBE)).Add(Entity)

        'lbParentescosProducto.DataMember = Session("ListParentescoPermitidoBE")
        'lbParentescosProducto.DataBind()



    End Sub

    Private Sub ibtnAdelante2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnAdelante2.Click

        If lbTipoDocumento.SelectedValue Is Nothing Then
            Exit Sub
        End If

        If lbTipoDocumento.SelectedValue = "" Then
            Exit Sub
        End If

        Dim Entity As New TipoDocProductoBE
        Entity.idTipoDocumento = lbTipoDocumento.SelectedValue
        Entity.TipoDocumento = lbTipoDocumento.SelectedItem.Text

        For i As Integer = 0 To lbTipoDocumentoProducto.Items.Count - 1
            If lbTipoDocumentoProducto.Items(i).Value = Entity.idTipoDocumento Then
                Exit Sub
            End If
        Next

        lbTipoDocumentoProducto.Items.Insert(0, New System.Web.UI.WebControls.ListItem(Entity.TipoDocumento, Entity.idTipoDocumento))

        'DirectCast(Session("ListTipoDocProductoBE"), List(Of TipoDocProductoBE)).Add(Entity)

        'lbTipoDocumentoProducto.DataMember = Session("ListTipoDocProductoBE")
        'lbTipoDocumentoProducto.DataBind()


    End Sub

    Private Sub ibtnAtraz1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnAtraz1.Click
        If lbParentescosProducto.SelectedValue Is Nothing Then
            Exit Sub
        End If

        If lbParentescosProducto.SelectedValue = "" Then
            Exit Sub
        End If

        If lbParentescosProducto.SelectedValue = 1 Then
            lbParentescos.Enabled = True
        End If
        lbParentescosProducto.Items.RemoveAt(lbParentescosProducto.SelectedIndex)
        'Session("ListParentescoPermitidoBE") = lbParentescosProducto.DataSource

    End Sub

    Private Sub ibtnAtraz2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnAtraz2.Click
        If lbTipoDocumentoProducto.SelectedValue Is Nothing Then
            Exit Sub
        End If

        If lbTipoDocumentoProducto.SelectedValue = "" Then
            Exit Sub
        End If


        lbTipoDocumentoProducto.Items.RemoveAt(lbTipoDocumentoProducto.SelectedIndex)
        'Session("ListTipoDocProductoBE") = lbTipoDocumentoProducto.DataSource
    End Sub

#End Region

#Region " ObjectDataSource "

    Private Sub odsProducto_Inserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs) Handles odsProducto.Inserted
        Dim r As String = e.ReturnValue
        If r <> "" Then
            If Integer.TryParse(r, 0) Then
                r = "1"
            Else
                r = "-1"
            End If
        End If
        Response.Redirect("Producto.aspx?r=" & r)
    End Sub

    Private Sub odsProducto_Inserting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceMethodEventArgs) Handles odsProducto.Inserting
        e.InputParameters(0) = EntidadProductoBE()
        e.InputParameters(1) = ListaParentestoPermitidoBE()
        e.InputParameters(2) = ListaTipoDocProductoBE()

        e.InputParameters(3) = Session("ListInfoProductoBE")
        e.InputParameters(4) = Session("ListInfoAseguradoBE")
        e.InputParameters(5) = Session("ListOpcionBE")
        e.InputParameters(6) = Session("ListOpcionPrimaBE")
        e.InputParameters(7) = Session("ListProductoMedioPagoBE")
    End Sub

#End Region

#Region " Metodo "

    Function EntidadProductoBE() As ProductoBE

        Dim Entity As New ProductoBE

        Entity.idProducto = txtCodigoProducto.Text.ToUpper
        Entity.idAsegurador = ddlAseguradora.SelectedValue
        Entity.nombre = txtNombre.Text.ToUpper
        Entity.idTipoProducto = ddlTipoProducto.SelectedValue
        Entity.activo = cbActivo.Checked
        Entity.maxAsegurados = txtMaxAsegurados.Text
        Entity.nacimientoRequerido = cbNacReq.Checked
        Entity.validarParentesco = cbValParsco.Checked
        Entity.obligaCuentaHabiente = cbOblCtaHabiente.Checked
        Entity.cumuloMaximo = 0 'txtCumuloMaximo.Text
        Entity.edadMaxPermanencia = txtMaxEdadPerm.Text
        Entity.diasAnulacion = txtMaxDiasAnu.Text
        Entity.generaCobro = cbGeneracobro.Checked
        Entity.idEntidad = ddlEntidad.SelectedValue
        Entity.vigenciaCobro = cbVigCobro.Checked
        Entity.maxPolizasAsegurado = txtMaxPolizasAseg.Text
        Entity.maxPolizasCuentahabiente = txtMaxPolizasCtaHb.Text
        Entity.maxPolizasRegalo = txtMaxPolizasRegalo.Text
        Entity.idReglaCobro = 0
        Entity.maxCuentasTitular = txtMaxCtaTitular.Text
        Entity.maxPolizasAseguradoSinDNI = txtMaxPolizasSDNI.Text
        Entity.validaDireccionCer = cbValDirCer.Checked
        Entity.validaDireccionAse = cbValDirAseg.Checked
        Entity.cuentaHabienteigualAsegurado = cbCtaHbAsegurado.Checked

        Entity.Minimo = txtMinimo.Text
        Entity.Maximo = txtMaximo.Text

        Return Entity
    End Function

    Function ListaParentestoPermitidoBE() As List(Of ParentescoPermitidoBE)
        Dim List As New List(Of ParentescoPermitidoBE)

        For i As Integer = 0 To lbParentescosProducto.Items.Count - 1
            Dim Entity As New ParentescoPermitidoBE
            Entity.idParentesco = lbParentescosProducto.Items(i).Value
            List.Add(Entity)
        Next
        Return List
    End Function

    Function ListaTipoDocProductoBE() As List(Of TipoDocProductoBE)
        Dim List As New List(Of TipoDocProductoBE)

        For i As Integer = 0 To lbTipoDocumentoProducto.Items.Count - 1
            Dim Entity As New TipoDocProductoBE
            Entity.idTipoDocumento = lbTipoDocumentoProducto.Items(i).Value
            List.Add(Entity)
        Next
        Return List
    End Function

#End Region


   

End Class