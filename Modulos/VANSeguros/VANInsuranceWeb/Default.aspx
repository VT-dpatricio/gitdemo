﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb" Inherits="VANInsurance.WebForm2" MasterPageFile="MasterPage.Master"  %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controles/Cargando.ascx" TagName="Cargando" TagPrefix="uc1" %>
<%@ Register src="~/Controles/ccCrossSelling.ascx" tagname="ccCrossSelling" tagprefix="uc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

            $(document).ready(function () {
                ClearCrossSelling();
            });

            function ClearCrossSelling() {
                $('#ctl00_contenedor_ccCrossSelling_hlinksTabs').val("");
            }

            function OpenSale(idProduct) {
                var nroDoc = document.getElementById('<%= txtNroDocCliente.ClientID%>').value
                var objCombo = document.getElementById('ctl00_contenedor_ddlTipoDocCliente');
                var tipoDoc = objCombo.options[objCombo.selectedIndex].value;
                try {
                    window.open('Proceso/EmisionCertificado.aspx?IdProducto=' + idProduct + '&nroDoc=' + nroDoc + '&tipoDoc=' + tipoDoc + '&IsCrossSelling=0', '_self');
                }
                catch (err) {
                    alert(err.message);
                }

            }


        function CloseSucessContactSave() {
            cerrarModal();
            alert('Solicitud de Contacto registrada correctamente');
            $('input:checkbox[id^="checkbox-7-"]:checked').each(function () {
                $(this).prop("checked", false);
            });
        }

        function OpenContact(idProducto) {
            var content = "ctl00_contenedor_"
            var nroDoc = document.getElementById('<%= txtNroDocCliente.ClientID%>').value
            var sIDproduct = "";
            var sNameproduct = "";
            $('input:checkbox[id^="checkbox-7-"]:checked').each(function () {
                sIDproduct = sIDproduct + $(this).attr('data-name') + ",";
                sNameproduct = sNameproduct.trim() + $(this).attr('title') + ",";
            });
            if (sNameproduct && sNameproduct.length > 0 && sNameproduct.indexOf(",", sNameproduct.length - 1) !== -1) {
                sNameproduct = sNameproduct.substring(0, sNameproduct.length - 1);
            }

            document.getElementById('<%= txtProductosInteres.ClientID%>').value = sNameproduct;
            document.getElementById('<%= txtDocumentoContacto.ClientID%>').value = nroDoc
            
            document.getElementById('<%= txtEmailContacto.ClientID%>').value = ''
            document.getElementById('<%= txtCelularContacto.ClientID%>').value = ''
            document.getElementById('<%= txtTelefonoContacto.ClientID%>').value = ''
            document.getElementById('<%= hfIDProductoSelectCrossSelling.ClientID%>').value = sIDproduct.substring(0, sIDproduct.length - 1);
            $('#' + content + 'ddlTipoDocumentoContacto').val($('#' + content + 'ddlTipoDocCliente').val());
            BuscarModal("#frmSolicitudContacto", 569, 380, "#EFEFF2");
        }

        function OpenSolicitudContacto(idProducto) {
            document.getElementById('<%= hfIDProductoSelectCrossSelling.ClientID%>').value = idProducto
            document.getElementById('<%= btnListaSolicitudesContacto.ClientID%>').click();
            BuscarModal("#frmListaSolicitudesContacto", 619, 350);
        }


        function Numero(e) {
            key = (document.all) ? e.keyCode : e.which;
            if (key < 48 || key > 57) {
                if (key == 8 || key == 0 || key == 45) {
                    return true
                }
                else
                    return false;
            }

        }

    </script>
    <link href="Estilos/assets/bootstrap/css/boostrap-buttons.css" rel="stylesheet" />

    <style type="text/css">
        .auto-style1 {
            width: 106px;
        }
        .auto-style7 {
            width: 130px;
            text-align:right;
        }
    .auto-style8 {
        width: 41px;
    }
    .auto-style9 {
        width: 130px;
        text-align:right;
    }
    .auto-style10 {
        width: 241px;
    }
    </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenedor" runat="server">

 <div style="height:400px;margin:auto; margin-top:20px; margin-bottom:50px;">
     

                <asp:UpdatePanel id="upParBuscar" runat="server" UpdateMode="Conditional">
                    <contenttemplate>
	<table style="margin: auto; WIDTH: 950px" class="MarcoTabla" cellspacing="4">
    <tbody>
                            <tr>
                                <td class="auto-style9">Tipo Documento:</td>
                                <td class="auto-style8">
                                    <asp:DropDownList ID="ddlTipoDocCliente" runat="server" AutoPostBack="false" Width="169px">
                                    </asp:DropDownList>
                                </td>
                                <td class="auto-style7">
                                    <asp:Label ID="lblBuscarx" runat="server" 
                             Text="Nº Documento:"></asp:Label>
                                </td>
                                <td class="auto-style10">
                                    <asp:TextBox ID="txtNroDocCliente" runat="server"  onkeypress="return Numero(event)"
                             ValidationGroup="BUS" Width="170px" MaxLength="8"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
                            runat="server"  ControlToValidate="txtNroDocCliente" 
                            ErrorMessage="Ingrese el N° de Documento" SetFocusOnError="True" 
                            ValidationGroup="BUS">*</asp:RequiredFieldValidator>
                                </td>
                                    <td style="WIDTH: 56px" class="TilCeldaD">
                                        <asp:Button ID="btnBuscar" runat="server" Visible="true" SkinID="btnDefault" Text="Buscar" ValidationGroup="BUS" Width="80px" />
                                    </td>
                                <td style="WIDTH: 148px">
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="7" class="auto-style1">
                                    <asp:GridView ID="gvBusqueda" runat="server" AutoGenerateColumns="False"
                                        Caption="Busqueda de Clientes" EnableModelValidation="True" Height="16px" Width="100%">
                                        <RowStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" Height="23px"></RowStyle>
                                        <Columns>
                                            <asp:BoundField DataField="NombreCompleto" HeaderText="Nombre Completo">
                                            <ItemStyle HorizontalAlign="Center" Width="120px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="TipoDocumento" HeaderText="Tipo Documento">
                                            <ItemStyle HorizontalAlign="Center" Width="120px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NroDocumento" HeaderText="N° Documento">
                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Direccion" HeaderText="Dirección">
                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                            </asp:BoundField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No se encontraron registros...
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style1" colspan="7">
                                    <asp:ValidationSummary ID="ValidationSummary4" runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup="BUS" />
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style1" colspan="7">
                                    <asp:HiddenField ID="hfIDProductoCrossSelling" runat="server" Value="0" />
                                    <asp:HiddenField ID="hfIDProductoSelectCrossSelling" runat="server" Value="0" />
                                    <uc1:ccCrossSelling ID="ccCrossSelling" runat="server" />
                                </td>
                            </tr>
                        </tbody>
            </table>
        </contenttemplate>
                     <Triggers>
                           <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                        </Triggers>
    </asp:UpdatePanel>
    <br />
    <asp:UpdatePanel id="upLista" runat="server" UpdateMode="Conditional">
                    <triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    </triggers>
    </asp:UpdatePanel>
         <asp:UpdateProgress ID="updateProgressBuscar" runat="server" DisplayAfter="0">
                <ProgressTemplate>
                    <uc1:Cargando ID="CargandoBusquedaCliente" runat="server" />
            </ProgressTemplate>
                </asp:UpdateProgress>
   

             <div id="frmListaSolicitudesContacto" style="width: 622px; display: none; margin: auto">
        <asp:Button ID="btnListaSolicitudesContacto" runat="server" CausesValidation="False" CssClass="BotonOculto"/>
        <asp:UpdatePanel ID="updPanelGridSolicitudes" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:GridView ID="gvSolicitudesContacto" runat="server" AutoGenerateColumns="False"
                                                                    EnableModelValidation="False" Caption="Solicitudes Contacto">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="FechaCreacion" HeaderText="Fecha Solicitud" dataformatstring="{0:dd-M-yyyy}">
                                                                                <ItemStyle HorizontalAlign="Center" Width="42px"></ItemStyle>
                                                                         </asp:BoundField>
                                                                         <asp:BoundField DataField="CantidadDiasContacto" HeaderText="Cantidad días">
                                                                                <ItemStyle HorizontalAlign="Center" Width="11px"></ItemStyle>
                                                                         </asp:BoundField>
                                                                          <asp:BoundField DataField="ccCliente" HeaderText="Documento">
                                                                                <ItemStyle HorizontalAlign="Center" Width="90px"></ItemStyle>
                                                                         </asp:BoundField>
                                                                          <asp:BoundField DataField="NombreCompleto" HeaderText="Nombre y Apellido">
                                                                                <ItemStyle HorizontalAlign="Center" Width="132px"></ItemStyle>
                                                                         </asp:BoundField>
                                                                        <asp:BoundField DataField="Observaciones" HeaderText="Observaciones">
                                                                                <ItemStyle HorizontalAlign="Left" Width="200px"></ItemStyle>
                                                                         </asp:BoundField> 
                                                                    </Columns>
                                                                    <EmptyDataTemplate>
                                                                        No se encontraron solicitudes de contacto
                                                                    </EmptyDataTemplate>
                                                                    <RowStyle Height="23px" />
                                                                </asp:GridView>
                </ContentTemplate>
                <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnListaSolicitudesContacto" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>

        </div>

      <div id="frmSolicitudContacto"
        style="background-color:#EFEFF2; width: 250px; display:none">
        <asp:UpdatePanel ID="updSolicitudContacto" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table width="569px" border="0" style="background-color:#EFEFF2; height: 346px;">
                      <tr>
                        <td colspan="7"><div class="bg-dcontact"><h7>SOLICITUD DE CONTACTO</h7></div></td>
                      </tr>
                      <tr>
                          <td class="cell-data">&nbsp;</td>
                          <td>&nbsp;</td>
                          <td class="cell-data">&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td class="auto-style2">&nbsp;</td>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="cell-data">Tipo</td>
                        <td>
                            <asp:DropDownList CssClass="ddlDisable" ID="ddlTipoDocumentoContacto" runat="server" Width="100px" Enabled="False">
                                        </asp:DropDownList>
                          </td>
                        <td class="cell-data">Documento</td>
                        <td>
                            <asp:TextBox SkinID="txtDisable" ID="txtDocumentoContacto" Enabled="False" runat="server" Width="135px"></asp:TextBox>
                          </td>
                        <td>&nbsp;</td>
                        <td class="auto-style2">&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="cell-data">Primer Nombre</td>
                        <td colspan="2">
                            <asp:TextBox ID="txtNombreContacto1" runat="server" Width="100%"></asp:TextBox>
                          </td>
                        <td class="cell-data">Segundo Nombre</td>
                          <td class="cell-data">
                              <asp:TextBox ID="txtNombreContacto2" runat="server" Width="100%"></asp:TextBox>
                          </td>
                        <td class="auto-style2">&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td class="cell-data">Apellido Paterno:</td>
                          <td colspan="2">
                              <asp:TextBox ID="txtApellidoContacto1" runat="server" Width="100%"></asp:TextBox>
                          </td>
                          <td class="cell-data">Apellido Materno:</td>
                          <td class="cell-data">
                              <asp:TextBox ID="txtApellidoContacto2" runat="server" Width="100%"></asp:TextBox>
                          </td>
                          <td class="auto-style2">&nbsp;</td>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="cell-data">Email</td>
                        <td colspan="3">
                            <asp:TextBox ID="txtEmailContacto" runat="server" ReadOnly="False"  Width="100%"></asp:TextBox>
                          </td>
                        <td>&nbsp;</td>
                        <td class="auto-style2">&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="cell-data">Tel</td>
                        <td colspan="3">
                            <asp:TextBox ID="txtTelefonoContacto" runat="server" ReadOnly="False" Width="100%"></asp:TextBox>
                          </td>
                        <td>&nbsp;</td>
                        <td class="auto-style2">&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="cell-data">Cel</td>
                        <td colspan="3">
                            <asp:TextBox ID="txtCelularContacto" runat="server" ReadOnly="False"  Width="100%"></asp:TextBox>
                          </td>
                        <td>&nbsp;</td>
                        <td class="auto-style2">&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="cell-data">Productos de Interes</td>
                        <td colspan="3" rowspan="2">
                            <asp:TextBox SkinID="txtDisable" ID="txtProductosInteres" runat="server" Enabled="False" ReadOnly="True"  Width="100%" Height="51px" TextMode="MultiLine"></asp:TextBox>
                          </td>
                        <td>&nbsp;</td>
                        <td class="auto-style2">&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="cell-data">&nbsp;</td>

                        <td>&nbsp;</td>
                        <td class="auto-style2">&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
						<tr>
                        <td class="cell-data">Contactar en:</td>
                        <td>
                          <asp:DropDownList ID="ddlFrecuenciaContacto" runat="server" Width="100px" Enabled="True">
                              </asp:DropDownList>
                          </td>
                        <td class="cell-data-left" colspan="2"> <asp:TextBox ID="txtValorDateFrecuenciaContacto" runat="server" Width="61px" style="display:none"></asp:TextBox><asp:TextBox ID="txtValorFrecuenciaContacto" MaxLength="10" runat="server" Width="61px"  Visible="true"></asp:TextBox>
                             <cc1:MaskedEditExtender ID="MEE2" runat="server" ClearTextOnInvalid="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtValorDateFrecuenciaContacto"></cc1:MaskedEditExtender>
                            <asp:CompareValidator ID="CV_FD" runat="server" ControlToValidate="txtValorDateFrecuenciaContacto" ErrorMessage="Ingrese una Fecha válida" Operator="DataTypeCheck" Type="Date">*</asp:CompareValidator>
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="cell-data">Observaciones</td>
                        <td colspan="3" rowspan="4">
                            <asp:TextBox ID="txtObservacionesContacto" runat="server" ReadOnly="false" Width="100%" Height="89px" MaxLength="8000" TextMode="MultiLine"></asp:TextBox>
                          </td>
                        <td>&nbsp;</td>
                        <td class="auto-style2">
                            <asp:Button ID="btnGuardarSolicitudContacto" runat="server" CausesValidation="false" Text="Guardar" ValidationGroup="CONT" />
                          </td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="cell-data"></td>
                        <td></td>
                        <td class="auto-style2">
                            <asp:Button ID="btnCancelarSolicitudContacto" runat="server" CausesValidation="False" OnClientClick="$.modal.close(); return false;"  Text="Cancelar" />
                          </td>
                        <td></td>
                      </tr>
                      <tr>
                          <td class="cell-data">&nbsp;</td>
                          <td>&nbsp;</td>
                          <td class="auto-style2">&nbsp;</td>
                          <td>&nbsp;</td>
                      </tr>
                    </table>
             </ContentTemplate>
                <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnGuardarSolicitudContacto" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="updateProgressBarSolicitudContacto" runat="server" DisplayAfter="0">
                <ProgressTemplate>
                    <uc1:Cargando ID="CargandoSolicitudContacto" runat="server" />
            </ProgressTemplate>
                </asp:UpdateProgress>

</div>
  </div>  
</asp:Content>
