﻿Imports Microsoft.VisualBasic
Imports System.Drawing
Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.BL
Imports System.Globalization

Public Class PaginaBase
    Inherits System.Web.UI.Page

    Protected Const SESSION_BEUSUARIO As String = "BEUsuario"
    Public Const S_PH_FORM As String = "phFormulario"
    Public Const S_MODELOS As String = "SESION_MODELOS"
    Public Const S_DATOS_MODELOS As String = "SESION_DATOS_MODELOS"
    Public Const S_CONDICION_PLAN As String = "SESSION_CONDICION_PLAN"
    Public Const S_FORMAT_YYYYMMDD = "yyyyMMdd"
    Public Const S_FORMAT_DDMMYYYY = "ddMMyyyy"
    Public Const S_FORMAT_MMDDYYYY = "MMddyyyy"
    Public Const S_PARAMETER_PAGE_PRODUCTO = "IdProducto"

    Public Sub CargarEntidad(ByVal ddl As DropDownList)
        Dim oBLEntidad As New BLEntidad
        ddl.DataSource = oBLEntidad.Seleccionar
        ddl.DataTextField = "Nombre"
        ddl.DataValueField = "IDEntidad"
        ddl.DataBind()
    End Sub

    Public Function ValidarAcceso(ByVal pUrl As String) As Boolean
        Dim valida As Boolean = False
        Try
            Dim oBLMenu As New BLMenu
            Dim Lista As ArrayList = oBLMenu.ListarMenu(Session("IDUsuario").ToString)
            For Each oBEMenu As BEMenu In Lista
                If oBEMenu.Url = pUrl Then
                    valida = True
                    Exit For
                End If
            Next
        Catch ex As Exception
            valida = False
        End Try
        Return valida
    End Function

    Public Function ValidarEstadoClaveUsuario() As Boolean
        Dim valida As Boolean = False
        Try
            'If ) Or CBool(Session("ClaveCaducada")) Then
            valida = CBool(Session("PrimerLogin")) Or CBool(Session("ClaveCaducada"))
            'End If
        Catch ex As Exception
            valida = False
        End Try
        Return valida

    End Function


    Public Function IDSistema() As Int32
        Return ConfigurationManager.AppSettings("IDSistema").ToString()
    End Function


    Public Function ValidatePermisionFrm(ByVal sPage As Page, Optional ByVal pIDProducto As Int32 = 0) As Boolean
        Dim valida As Boolean = False
        Try
            Dim IDProducto As Int32 = 0
            If Not Request.QueryString(S_PARAMETER_PAGE_PRODUCTO) Is Nothing AndAlso Not String.IsNullOrWhiteSpace(Request.QueryString(S_PARAMETER_PAGE_PRODUCTO)) Then
                IDProducto = Request.QueryString(S_PARAMETER_PAGE_PRODUCTO)
            End If

            Dim pUrl As String = HttpUtility.UrlDecode(HttpContext.Current.Request.RawUrl()) 'GetParamUrl(sPage)
            Dim oBLMenu As New BLMenu
            Dim Lista As ArrayList = oBLMenu.ListarMenu(Session("IDUsuario").ToString)

            Dim urlValidate As String = IIf(pIDProducto <> 0, pIDProducto, GetUrlValidation(pUrl.Trim().ToUpper(), IDProducto))
            For Each oBEMenu As BEMenu In Lista
                If GetUrlValidation(oBEMenu.Url.ToUpper().Trim().Replace("~", Request.ApplicationPath).Replace("//", "/"), IIf(pIDProducto <> 0, pIDProducto, IDProducto)) = urlValidate Then
                    valida = True
                    Exit For
                End If
            Next
        Catch ex As Exception
            valida = False
        End Try
        Return valida
    End Function

    Private Function GetUrlValidation(ByVal sUrl As String, ByVal idProducto As Int32) As String
        Dim sValue As String = String.Empty
        Dim aux As String = String.Empty
        If idProducto <> 0 AndAlso sUrl.IndexOf(S_PARAMETER_PAGE_PRODUCTO.ToUpper()) >= 0 Then
            sValue = sUrl.Substring(sUrl.IndexOf(S_PARAMETER_PAGE_PRODUCTO.ToUpper()) + Len(S_PARAMETER_PAGE_PRODUCTO.ToUpper()) + 1, 4)
        Else
            sValue = sUrl
        End If
        Return sValue
    End Function

    Private Function GetParamUrl(ByVal sPage As Page) As String
        Dim sParam As String = String.Empty
        If Not Request.QueryString("IdProducto") Is Nothing AndAlso Not String.IsNullOrWhiteSpace(Request.QueryString("IdProducto")) Then
            sParam = "?IdProducto=" & Request.QueryString("IdProducto") & "&Nombre=" & Request.QueryString("Nombre") & "&cb=" & Request.QueryString("cb")
        End If

        Return sPage.Request.Url.AbsolutePath & sParam
    End Function

    Public Function IsValidDateServiceITAU(ByVal sDate As String) As Boolean

        Return IsValidDate(sDate, S_FORMAT_YYYYMMDD)
    End Function

    Private Function IsValidDate(ByVal sDate As String, ByVal format As String) As Boolean
        Dim ds As String = sDate
        Dim dt As DateTime

        Return DateTime.TryParseExact(ds, format, System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, dt)
    End Function

    Public Function GetDatePart(ByVal sDate As String, ByVal format As String, Optional ByVal separator As String = "/")
        Dim fAux As String = String.Empty
        If Not String.IsNullOrEmpty(sDate) AndAlso IsValidDate(sDate, format) Then
            Select Case format
                Case S_FORMAT_DDMMYYYY
                    fAux = sDate.Substring(0, 2) & separator & sDate.Substring(2, 2) & separator & sDate.Substring(4, 4)
                Case S_FORMAT_MMDDYYYY
                    fAux = sDate.Substring(2, 2) & separator & sDate.Substring(0, 2) & separator & sDate.Substring(4, 4)
                Case S_FORMAT_YYYYMMDD
                    fAux = sDate.Substring(6, 2) & separator & sDate.Substring(4, 2) & separator & sDate.Substring(0, 4)
                Case Else
                    fAux = "01" & separator & "01" & separator & "1900"
            End Select

        End If
        Return fAux
    End Function

    'Public Function AnexarPDF(ByVal requestRdl As Byte(), ByVal IDCertificado As String, ByVal idProducto As Integer, ByVal IdImpresionPadre As Integer) As Byte()
    '    Dim bytesRequest As Byte() = requestRdl
    '    Dim oBLImpEdad As New BLProductoImpresionDetalleEdad
    '    Dim lista As New ArrayList

    '    lista = oBLImpEdad.Seleccionar(IdImpresionPadre, IDCertificado)

    '    If lista.Count > 0 Then
    '        Dim pdf As New Pdf.PdfFunction
    '        Dim lstPdfAditional(lista.Count) As String
    '        Dim index As Integer = 0

    '        For Each oBE As BEProductoImpresionDetalleEdad In lista
    '            lstPdfAditional(index) = oBE.Directorio & "\" & oBE.NombreArchivo
    '            index = index + 1
    '        Next
    '        bytesRequest = pdf.MergePage(requestRdl, lstPdfAditional)
    '    End If
    '    Return bytesRequest
    'End Function

    Public Sub AccessActiveDirectory(ByRef sUsr As String, ByRef sPsw As String, ByVal sRq As HttpRequest)
        Dim entorno As String = "PRO"
        Dim psw As String = "12345+"
        Dim Usuario As String = sUsr
        'Login desde ITAU----------------------------
        If Not (sRq.Form("usr") = Nothing) Then
            If sRq.RequestType = "POST" Then
                Usuario = sRq.Form("usr")
                Dim dominio() As String = Usuario.Split("\")
                If dominio.Length > 1 Then
                    Usuario = dominio(1)
                End If
            End If
        End If
        Try
            If Not IsPrivateAccess() Then
                psw = sPsw
                Usuario = sUsr
            End If
        Catch ex As Exception

        End Try
        sPsw = psw
        sUsr = Usuario
    End Sub

    Public Function IsPrivateAccess() As Boolean
        Dim result As Boolean = True
        Try
            Dim entorno As String
            entorno = ConfigurationManager.AppSettings.Item("Enviroment")
            If entorno = "DEV" Then
                result = False
            Else
                result = True
            End If
        Catch ex As Exception

        End Try
        Return result
    End Function

    Public Function ValidaProductoMail(ByVal IDProducto As Integer) As Boolean
        'Mejorar la obtencion de la configuracion del mail
        Dim result As Boolean = False
        Try
            Select Case IDProducto
                Case 5448
                    result = True
            End Select
        Catch ex As Exception

        End Try
        Return result
    End Function

    Public Function CalculoPrimaTotal(ByVal mesVigencia As Integer, ByVal pIDProducto As Integer, ByRef FinVigenciaAbierta As Dictionary(Of Integer, Integer)) As Integer
        'Mejorar la obtencion de la configuracion del mail
        Dim result As Int32 = 1
        If FinVigenciaAbierta(pIDProducto) <> 0 And FinVigenciaAbierta(pIDProducto) = mesVigencia Then
            result = mesVigencia
        End If

        'If mesVigencia < 12 AndAlso mesVigencia > 1 Then
        '    result = mesVigencia
        'End If

        Return result
    End Function

End Class
