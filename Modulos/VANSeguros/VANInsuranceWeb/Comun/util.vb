﻿Imports System.Globalization

Public Class util
#Region "SINGLETON"
    Private Shared obj As util
    Private Sub New()
    End Sub
    Shared ReadOnly Property Instance() As util
        Get
            If util.obj Is Nothing Then util.obj = New util
            Return util.obj
        End Get
    End Property
#End Region


    'Public Sub SetDBLogonForReport(ByVal ServidorBD As String, ByVal BaseDatos As String, ByVal UsuarioBD As String, ByVal ClaveBD As String, ByVal myReportDocument As ReportDocument)
    '    Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
    '    Dim ubicacion As String

    '    myConnectionInfo.ServerName = ServidorBD
    '    myConnectionInfo.DatabaseName = BaseDatos
    '    myConnectionInfo.UserID = UsuarioBD
    '    myConnectionInfo.Password = ClaveBD

    '    Dim myTables As Tables = myReportDocument.Database.Tables
    '    For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
    '        ubicacion = myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
    '        Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
    '        myTableLogonInfo.ConnectionInfo = myConnectionInfo
    '        myTable.ApplyLogOnInfo(myTableLogonInfo)
    '        myTable.Location = BaseDatos & ".dbo." & ubicacion
    '    Next
    'End Sub

    'Public Sub SetCurrentValuesForParameterField(ByVal myParameterFields As ParameterFields, ByVal myArrayList As ArrayList)
    '    Dim i As Integer = 0
    '    For Each myParameterField As ParameterField In myParameterFields
    '        Dim currentParameterValues As ParameterValues = New ParameterValues()
    '        Dim myParameterDiscreteValue As ParameterDiscreteValue = New ParameterDiscreteValue()
    '        myParameterDiscreteValue.Value = myArrayList(i)
    '        currentParameterValues.Add(myParameterDiscreteValue)
    '        myParameterField.CurrentValues = currentParameterValues
    '        i = i + 1
    '    Next
    'End Sub

    Public Function validarTransaccion(ByVal valor As String) As String
        If Integer.TryParse(valor, 0) Then
            Return "Operacion exitosa!."
        Else
            Return "La operacion no se pudo realizar, intentelo luego!."
        End If
        Return "error!"
    End Function

    Public Shared Function DecToStrRegional(ByVal pNumero As Decimal) As String
        Dim nfi As NumberFormatInfo = New CultureInfo("es-PY", False).NumberFormat
        Dim r As String = ""
        Try
            r = pNumero.ToString("N2", nfi)
        Catch ex As Exception

        End Try
        Return r
    End Function

    Public Shared Function StrToDecRegional(ByVal pNumero As String) As Decimal
        Dim nfi As NumberFormatInfo = New CultureInfo("es-PY", False).NumberFormat
        Dim r As Decimal = 0
        Try
            r = Decimal.Parse(pNumero, nfi)
        Catch ex As Exception

        End Try
        Return r
    End Function
End Class