﻿Imports VAN.InsuranceWeb.BL
Imports VAN.InsuranceWeb.BE
Imports System.Web.Security
Partial Public Class CambiarClave
    Inherits PaginaBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            If Not ValidarAcceso(Page.AppRelativeVirtualPath) Then
                Response.Redirect("~/Login.aspx", False)
            End If

            Try

                If CBool(Session("PrimerLogin")) Then
                    lblEstadoClave.Text = "Por favor actualice la contraseña"
                End If

                If CBool(Session("ClaveCaducada")) Then
                    lblEstadoClave.Text = "Contraseña caducada, Por favor actualice la contraseña"
                End If

                '----Cargar Parámetros de Seguridad
                Dim pListParametro As IList = CType(Session("SeguridadPar"), IList)
                Dim oBLCSeguridad As New BLSeguridadConfig

                'Longitud de caracteres mínima para la contraseña (Número)
                Dim pLongMinClave = CInt(oBLCSeguridad.Parametro("CONTRASE02", pListParametro))
                If pLongMinClave = 0 Then
                    '0 No válida la longitud
                    revLongitudNuevaC.Enabled = False
                Else
                    revLongitudNuevaC.Enabled = True
                    revLongitudNuevaC.ValidationExpression = "[\S\s]{" & pLongMinClave.ToString & ",50}"
                    revLongitudNuevaC.ErrorMessage = "La nueva contraseña debe tener una longitud mínima de " & pLongMinClave.ToString & " caracteres"
                End If

                'Contraseña Segura
                revSeguridadClave.Enabled = CBool(oBLCSeguridad.Parametro("CONTRASE03", pListParametro))

                'Modificar la Clave solo una vez al día
                If CBool(oBLCSeguridad.Parametro("CONTRASE06", pListParametro)) Then
                    Dim oBLSegEvento As New BLSeguridadEvento
                    If oBLSegEvento.ClaveModificadaHoy(User.Identity.Name) Then
                        MultiView1.ActiveViewIndex = 2
                    End If
                End If


            Catch ex As Exception
                Response.Redirect("~/Login.aspx", False)
            End Try


        End If
    End Sub

    Protected Sub btnCambiar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCambiar.Click
        Dim u As MembershipUser = Membership.GetUser(User.Identity.Name)
        Try
            If u.ChangePassword(txtClaveActual.Text, txtNuevaClave.Text) Then
                'Dim NClave As String = FormsAuthentication.HashPasswordForStoringInConfigFile(txtNuevaClave.Text, "md5") '.GetHashCode()
                'Registro Cambio de Contraseña --------------
                Dim oBESegEvento As New BESeguridadEvento
                Dim oBLSegEvento As New BLSeguridadEvento
                oBESegEvento.IDSistema = IDSistema()
                oBESegEvento.IDUsuario = User.Identity.Name
                oBESegEvento.IDTipoEvento = 3 'Cambio de Contraseña
                oBESegEvento.Host = Request.UserHostAddress
                'oBESegEvento.Detalle = u.
                oBLSegEvento.Insertar(oBESegEvento)
                If CBool(Session("PrimerLogin")) Then
                    oBESegEvento.IDTipoEvento = 1 'Registra Primer Login
                    oBLSegEvento.Insertar(oBESegEvento)
                End If

                MultiView1.ActiveViewIndex = 1
                Session("PrimerLogin") = False
                Session("ClaveCaducada") = False
                lblEstadoClave.Text = ""
            Else
                Msg.Text = "Contraseña actual incorrecta"
            End If
        Catch ex As Exception
            Msg.Text = "Ocurrió un error: " & Server.HtmlEncode(ex.Message) & ". Por favor vuelva a intentar."
        End Try
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancelar.Click
        txtClaveActual.Text = ""
        txtNuevaClave.Text = ""
        txtNuevaClaveConfir.Text = ""
    End Sub

    Protected Sub btnContinuar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnContinuar.Click, Button1.Click
        Response.Redirect("~/Default.aspx", False)
    End Sub
End Class