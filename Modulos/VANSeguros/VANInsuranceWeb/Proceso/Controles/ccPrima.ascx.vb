﻿Imports VAN.InsuranceWeb.BL
Imports VAN.InsuranceWeb.BE
Imports VANInsurance.util

Public Class ccPrima
    Inherits System.Web.UI.UserControl

    Private Const S_MODELOS As String = "SESION_MODELOS"
    Private Const S_DATOS_MODELOS As String = "SESION_DATOS_MODELOS"
    Private Const PH_FORM As String = "phFormulario"
    Private Const S_CONDICION_PLAN As String = "SESSION_CONDICION_PLAN"
    Public EdadAsegurado As Integer
    Public IDProducto As Int32 = 0
    Public IDRamo As Int32 = 0
    Public Condicion As String = ""
    Public EnabledFrecuencia As Boolean = True
    Public EnabledMoneda As Boolean = True
    Public Parametros As New ArrayList
    Public Event SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim oBL As New BLListaGenerica
            CargarPlan()
            CargarFrecuencia()
            CargarMonedaPrima()
            ddlFrecuencia.Enabled = EnabledFrecuencia
            ddlMonedaPrima.Enabled = EnabledMoneda
            If IDProducto = 5264 Then
                ddlPlan.Width = Unit.Pixel(255)
            End If

        End If
    End Sub

    Public Function GetParameters(ByVal lParametros As ArrayList) As String
        Dim intCount As Integer
        Dim valores As String = ""

        For intCount = 0 To lParametros.Count - 1 Step intCount + 1
            valores = valores + lParametros.Item(intCount).ToString() + ","
        Next

        If (valores.Length > 1) Then
            valores = valores.Substring(0, Len(valores) - 1)
        End If
        Return valores
    End Function

    Private Sub SeleccionarOpcionPrima()
        If Not String.IsNullOrEmpty(ddlPlan.SelectedValue) AndAlso Not String.IsNullOrEmpty(ddlFrecuencia.SelectedValue) Then
            Dim oBLOpcionPrima As New BLOpcionPrima
            Dim oBE As BEOpcionPrima = oBLOpcionPrima.SeleccionarBE(IDProducto, ddlPlan.SelectedValue, ddlFrecuencia.SelectedValue, ddlMonedaPrima.SelectedItem.Text, EdadAsegurado)
            Dim NroCoutas As Int32 = 12

            If (oBE.IdFrecuencia < 1 OrElse oBE.IdFrecuencia > 12) Then
                NroCoutas = 1
            Else
                NroCoutas = 12 / IdFrecuencia
            End If

            Dim MontoPrima As Decimal = oBE.Prima
            txtPrima.Text = DecToStrRegional(CDec(MontoPrima.ToString()))
            hfPrima.Value = MontoPrima
            hfMontoAsegurado.Value = oBE.MontoAsegurado
            Dim PrimaTotal As Decimal = NroCoutas * MontoPrima
            hfPrimaTotal.Value = PrimaTotal
        End If
    End Sub

    Public Sub RecalcularPrima()
        Try
            SeleccionarOpcionPrima()
        Catch ex As Exception
            Dim s As String = ex.Message
        End Try

    End Sub

    Public Sub CargarPlan()
        Dim oBLOpcion As New BLOpcion

        Parametros.Clear()
        Parametros.Add(IDRamo)
        ddlPlan.DataSource = oBLOpcion.SeleccionarOpciones(IDProducto, GetParameters(Parametros), Condicion)
        ' ddlPlan.DataSource = oBLOpcion.Seleccionar(IDProducto)
        ddlPlan.DataTextField = "DescripcionOpcion"
        ddlPlan.DataValueField = "Opcion"
        ddlPlan.DataBind()
    End Sub

    Public Sub CargarFrecuencia()
        Dim oBLOpcionFrecuencia As New BLOpcionFrecuencia
        ddlFrecuencia.DataSource = oBLOpcionFrecuencia.Seleccionar(IDProducto, ddlPlan.SelectedValue)
        ddlFrecuencia.DataTextField = "Nombre"
        ddlFrecuencia.DataValueField = "IdFrecuencia"
        ddlFrecuencia.DataBind()
    End Sub

    Public Sub CargarMonedaPrima()
        Dim oBLOpcionPrima As New BLOpcionPrima
        Try
            If Not String.IsNullOrEmpty(ddlPlan.SelectedValue) AndAlso Not String.IsNullOrEmpty(ddlFrecuencia.SelectedValue) Then
                Dim ListaMonedaPrima As IList = oBLOpcionPrima.Seleccionar(IDProducto, ddlPlan.SelectedValue, ddlFrecuencia.SelectedValue, IIf(Session(S_CONDICION_PLAN) Is Nothing, "", Session(S_CONDICION_PLAN)), EdadAsegurado)
                ddlMonedaPrima.DataSource = ListaMonedaPrima
                ddlMonedaPrima.DataTextField = "IdMonedaPrima"
                ddlMonedaPrima.DataValueField = "Prima"
                ddlMonedaPrima.DataBind()
                SeleccionarOpcionPrima()
                txtPrima.Text = Format(CDec(ddlMonedaPrima.SelectedValue.ToString()), "#.00").Replace(".", ",")
                hfPrima.Value = ddlMonedaPrima.SelectedValue
            End If
        Catch ex As Exception
            Dim s As String = ex.Message
        End Try
    End Sub

    Public Sub RecargarPlanes()
        CargarPlan()
        CargarFrecuencia()
        CargarMonedaPrima()
    End Sub
    Protected Sub ddlPlan_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlPlan.SelectedIndexChanged
        CargarFrecuencia()
        CargarMonedaPrima()

        If IDProducto = 5431 AndAlso Not (Session(PH_FORM) Is Nothing) AndAlso Not DirectCast(DirectCast(Session(PH_FORM), System.Web.UI.WebControls.PlaceHolder).FindControl("IA_Modelo"), DropDownList) Is Nothing Then
            Dim pDeducible As String = String.Empty
            Dim pProducCode As String = String.Empty
            Dim pProductPrice As String = String.Empty
            GetInfoAdicional(pDeducible, pProducCode, pProductPrice)
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "P12", "RefreshDeducible('" + pDeducible + "','" + pProducCode + "','" + pProductPrice + "');", True)
        End If
        RaiseEvent SelectedIndexChanged(sender, e)
    End Sub

    Private Sub GetInfoAdicional(ByRef pDeducible As String, ByRef pProductCode As String, ByRef pProductPrice As String)
        Dim lista As ArrayList
        lista = DirectCast(Session(S_MODELOS), ArrayList)
        For Each element As BEMarcaModelo In DirectCast(Session(S_DATOS_MODELOS), ArrayList)
            If element.Modelo = DirectCast(DirectCast(Session(PH_FORM), System.Web.UI.WebControls.PlaceHolder).FindControl("IA_Modelo"), DropDownList).SelectedValue And element.Opcion = Opcion() Then
                pDeducible = element.Deducible
                pProductCode = element.ProductCode
                pProductPrice = element.ProductPrice
                Exit For
            End If
        Next
    End Sub


    Protected Sub ddlFrecuencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlFrecuencia.SelectedIndexChanged
        CargarMonedaPrima()
    End Sub

    Protected Sub ddlMonedaPrima_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlMonedaPrima.SelectedIndexChanged
        txtPrima.Text = ddlMonedaPrima.SelectedValue
    End Sub

    'Propiedades
    Public ReadOnly Property Opcion() As String
        Get
            Return ddlPlan.SelectedValue
        End Get
    End Property

    Public ReadOnly Property IdFrecuencia() As String
        Get
            Return ddlFrecuencia.SelectedValue
        End Get
    End Property

    Public ReadOnly Property IdMonedaPrima() As String
        Get
            Return ddlMonedaPrima.SelectedItem.Text
        End Get
    End Property

    Public ReadOnly Property Prima() As Decimal
        Get
            Return CDec(hfPrima.Value)
        End Get
    End Property

End Class