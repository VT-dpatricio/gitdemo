﻿Imports VAN.InsuranceWeb.BL
Public Class pBeneficiario
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Header.DataBind()
        If Not IsPostBack Then
            rvFechaN.MaximumValue = Now.Date.ToShortDateString
            CargarTipoDocumento()
            CargarParentesco()
            Dim objDT As New DataTable()
            If Session("EstForm") = "0" And Session("DTBeneficiario") Is Nothing Then 'Trabajando en Memoria
                objDT = CrearTablaAsegurado()
                Session("DTBeneficiario") = objDT
            Else
                objDT = Session("DTBeneficiario")
            End If
            LlenargvBeneficiario(objDT)

            If Session("EstForm") = "0" Then 'Form no grabado aún
                btnGrabar.Enabled = True
            Else
                btnGrabar.Enabled = False
            End If

        End If

    End Sub

    Public Function CrearTablaAsegurado() As DataTable
        Dim objDT As New DataTable("DTBeneficiario")
        objDT.Columns.Add("ID", System.Type.GetType("System.Int32"))
        objDT.Columns("ID").AutoIncrement = True
        objDT.Columns("ID").AutoIncrementSeed = 1
        objDT.Columns.Add("IDTipoDocumento", System.Type.GetType("System.String"))
        objDT.Columns.Add("NroDocumento", System.Type.GetType("System.String"))
        objDT.Columns.Add("IDParentesco", System.Type.GetType("System.String"))
        objDT.Columns.Add("Parentesco", System.Type.GetType("System.String"))
        objDT.Columns.Add("Porcentaje", System.Type.GetType("System.String"))
        objDT.Columns.Add("ApePaterno", System.Type.GetType("System.String"))
        objDT.Columns.Add("ApeMaterno", System.Type.GetType("System.String"))
        objDT.Columns.Add("PrimerNom", System.Type.GetType("System.String"))
        objDT.Columns.Add("SegundoNom", System.Type.GetType("System.String"))
        objDT.Columns.Add("FechaNac", System.Type.GetType("System.String"))
        objDT.Columns.Add("Direccion", System.Type.GetType("System.String"))
        Return objDT
    End Function

    Public Sub CargarTipoDocumento()
        Dim oBLTipoDoc As New BLListaGenerica
        cboTipoDoc.DataSource = oBLTipoDoc.Seleccionar(Session("IDPRODUCTO"), "TipoDocumento")
        cboTipoDoc.DataTextField = "Descripcion"
        cboTipoDoc.DataValueField = "ID"
        cboTipoDoc.DataBind()
        Try
            cboTipoDoc.SelectedValue = "L"
        Catch ex As Exception

        End Try

    End Sub

    Public Sub CargarParentesco()
        Dim oBLParentesco As New BLListaGenerica
        cboParentesco.DataSource = oBLParentesco.Seleccionar(Session("IDPRODUCTO"), "Parentesco")
        cboParentesco.DataTextField = "Descripcion"
        cboParentesco.DataValueField = "ID"
        cboParentesco.DataBind()
    End Sub

    Protected Sub btnGrabar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrabar.Click

        If Not IsNumeric(txtPorcentaje.Text.Trim()) Then
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('Ingrese un porcentaje válido.');", True)
            Exit Sub
        Else
            If CDec(txtPorcentaje.Text) <= 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('Ingrese un porcentaje mayor a cero');", True)
                Exit Sub
            End If
        End If


        Dim objDT As New DataTable()
        Dim NroBeneficiarios As Int32 = CInt(Session("NroBeneficiarios"))
        objDT = Session("DTBeneficiario")
        Dim sPorcentaje As Decimal = txtPorcentaje.Text.Trim()
        Dim pID As Int32 = CInt(ID.Value.ToString())
        If CalcularPorcentaje(objDT, pID) + sPorcentaje > 100 Then
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('El porcentaje total de Beneficiarios no puede ser mayor al 100% .');", True)
            Exit Sub
        End If

        'Validar doble ingreso de NroDocumento
        If ContarIngresado(objDT, pID, "NroDocumento", cboTipoDoc.SelectedValue, txtNroDocumento.Text) > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('Ya ingresó un Beneficiario con el mismo Número y Tipo de Documento');", True)
            Exit Sub
        End If

        'Validar doble ingreso de Padre
        If cboParentesco.SelectedValue = "3" Then
            If ContarIngresado(objDT, pID, "IDParentesco", "3", "") > 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('Ya ingresó un Beneficiario con parentesco Padre');", True)
                Exit Sub
            End If
        End If
        'Validar doble ingreso de Madre
        If cboParentesco.SelectedValue = "4" Then
            If ContarIngresado(objDT, pID, "IDParentesco", "4", "") > 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('Ya ingresó un Beneficiario con parentesco Madre');", True)
                Exit Sub
            End If
        End If

        'Validar doble ingreso de Conyuge
        If cboParentesco.SelectedValue = "6" Then
            If ContarIngresado(objDT, pID, "IDParentesco", "6", "") > 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('Ya exite un Cónyugue ingresado');", True)
                Exit Sub
            End If
        End If

        If objDT Is Nothing Then
            objDT = CrearTablaAsegurado()
        End If
        Dim blnNuevo As Boolean = True

        If ID.Value = "0" Then
            If objDT.Rows.Count < NroBeneficiarios Then
                Dim objDR As DataRow = objDT.NewRow()
                LLenarDatos(objDR)
                objDT.Rows.Add(objDR)
            Else
                ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "M", "alert('Sólo puede registrar " & NroBeneficiarios & " Beneficiario(s).');", True)
                Exit Sub
            End If
        Else
            For Each objDR As DataRow In objDT.Rows
                If objDR("ID") = CInt(ID.Value) Then
                    LLenarDatos(objDR)
                    blnNuevo = False
                    Exit For
                End If
            Next
        End If
        LlenargvBeneficiario(objDT)
        Session("DTBeneficiario") = objDT
        LimpiarControles()
        gvBeneficiario.SelectedIndex = -1
        ID.Value = 0

    End Sub

    Private Sub LLenarDatos(ByRef objDR As DataRow)
        objDR("IDTipoDocumento") = cboTipoDoc.SelectedValue
        objDR("NroDocumento") = txtNroDocumento.Text.Trim
        objDR("IDParentesco") = cboParentesco.SelectedValue
        objDR("Parentesco") = cboParentesco.SelectedItem.Text
        objDR("Porcentaje") = txtPorcentaje.Text.Trim
        objDR("ApePaterno") = txtApePat.Text.Trim.ToUpper
        objDR("ApeMaterno") = txtApeMat.Text.Trim.ToUpper
        objDR("PrimerNom") = txtPrimerNom.Text.Trim.ToUpper
        objDR("SegundoNom") = txtSegundoNom.Text.Trim.ToUpper
        objDR("FechaNac") = txtFechaNac.Text
        objDR("Direccion") = txtDireccion.Text.Trim
    End Sub

    Protected Sub ImageButton1_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)
        Dim pID As Integer = Convert.ToInt32(e.CommandArgument)
        Dim objDT As New DataTable()
        objDT = Session("DTBeneficiario")
        ''objDT.Rows(1).Delete()
        'For Each dr As DataRow In objDT.Rows
        '    If dr("ID") = pID Then
        '        dr.Delete()
        '        objDT.Rows().Delete()
        '        'dr("Porcentaje") = "sss"
        '    End If
        'Next
        'For i As Integer = 0 To objDT.Rows.Count - 1
        '    objDT.Rows(i).Item
        'Next

        'objDT.AcceptChanges()
        LlenargvBeneficiario(objDT)
        gvBeneficiario.SelectedIndex = -1
        Session("DTBeneficiario") = objDT
    End Sub

    Public Sub LlenargvBeneficiario(ByVal DT As DataTable)
        gvBeneficiario.DataSource = DT
        gvBeneficiario.DataBind()
        CalcularAcumulado(DT)
    End Sub

    Public Sub LimpiarControles()
        txtNroDocumento.Text = ""
        'cboTipoDoc.SelectedIndex = 0
        cboParentesco.SelectedIndex = 0
        txtPorcentaje.Text = ""
        txtApePat.Text = ""
        txtApeMat.Text = ""
        txtPrimerNom.Text = ""
        txtSegundoNom.Text = ""
        txtFechaNac.Text = ""
        txtDireccion.Text = ""
        ID.Value = "0"
        btnGrabar.Text = "Agregar"
    End Sub

    Protected Sub gvBeneficiario_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        'Dim pID As Integer = Convert.ToInt32(e.RowIndex)
        Dim objDT As New DataTable()
        objDT = Session("DTBeneficiario")
        objDT.Rows(e.RowIndex).Delete()
        LlenargvBeneficiario(objDT)
        gvBeneficiario.SelectedIndex = -1
        Session("DTBeneficiario") = objDT
        LimpiarControles()
    End Sub

    Protected Sub gvBeneficiario_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBeneficiario.SelectedIndexChanged
        ID.Value = gvBeneficiario.SelectedDataKey.Item(0)
        Dim objDT As New DataTable()
        objDT = Session("DTBeneficiario")
        Dim objDR As DataRow = objDT.NewRow()
        objDR = objDT.Rows(gvBeneficiario.SelectedIndex)
        txtNroDocumento.Text = objDR("NroDocumento")
        cboTipoDoc.SelectedValue = objDR("IDTipoDocumento")
        cboParentesco.SelectedValue = objDR("IDParentesco")
        txtPorcentaje.Text = objDR("Porcentaje")
        txtApePat.Text = objDR("ApePaterno")
        txtApeMat.Text = objDR("ApeMaterno")
        txtPrimerNom.Text = objDR("PrimerNom")
        txtSegundoNom.Text = objDR("SegundoNom")
        txtFechaNac.Text = objDR("FechaNac")
        txtDireccion.Text = objDR("Direccion")
        btnGrabar.Text = "Grabar"
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        LimpiarControles()
        ID.Value = 0
        gvBeneficiario.SelectedIndex = -1
    End Sub

    Public Sub CalcularAcumulado(ByVal objDT As DataTable)
        If objDT Is Nothing Then
            lblAcumulado.Text = "0%"
            txtPorcentaje.Text = "100"
        Else
            Dim p As Decimal = 0
            For Each objDR As DataRow In objDT.Rows
                p += CDec(objDR("Porcentaje"))
            Next
            lblAcumulado.Text = p.ToString & "%"
            If objDT.Rows.Count = 0 Then
                txtPorcentaje.Text = "100"
            End If
        End If


    End Sub



    Public Function ContarIngresado(ByVal objDT As DataTable, ByVal pID As Int32, ByVal campo As String, ByVal valor1 As String, ByVal valor2 As String) As Double
        Dim p As Int32 = 0
        If objDT Is Nothing Then
            p = 0
        Else
            For Each objDR As DataRow In objDT.Rows
                If objDR("ID") <> pID Then
                    Select Case campo
                        Case "NroDocumento"
                            If objDR("IDTipoDocumento").ToString = valor1 And objDR("NroDocumento").ToString = valor2 Then
                                p += 1
                            End If
                        Case "IDParentesco"
                            If objDR("IDParentesco").ToString = valor1 Then
                                p += 1
                            End If
                    End Select
                End If
            Next
        End If
        Return p
    End Function


    Public Function CalcularPorcentaje(ByVal objDT As DataTable, ByVal pID As Int32) As Double
        Dim p As Decimal = 0
        If objDT Is Nothing Then
            p = 0
        Else
            For Each objDR As DataRow In objDT.Rows
                If objDR("ID") <> pID Then
                    p += CDec(objDR("Porcentaje"))
                End If
            Next
        End If
        Return p
    End Function

    Protected Sub btnAceptar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptar.Click
        Dim objDT As New DataTable()
        objDT = Session("DTBeneficiario")
        If CalcularPorcentaje(objDT, 0) <> 100 Then
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Mensaje", "alert('No ha completado el 100% de Beneficiarios.');", True)
            Exit Sub
        Else
            ScriptManager.RegisterStartupScript(Me.Page, MyBase.GetType, "Closex", "window.parent.cerrarModal();", True)
        End If
    End Sub
End Class