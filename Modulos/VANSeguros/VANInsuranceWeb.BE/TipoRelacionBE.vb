﻿Public Class TipoRelacionBE
    Inherits GenericEntity

    Sub New()

    End Sub

    Private _IdTipoRelacion As Integer
    Private _Descripcion As String



    Property IdTipoRelacion() As Integer
        Get
            Return _IdTipoRelacion
        End Get
        Set(ByVal value As Integer)
            _IdTipoRelacion = value
        End Set
    End Property
    Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property



End Class
