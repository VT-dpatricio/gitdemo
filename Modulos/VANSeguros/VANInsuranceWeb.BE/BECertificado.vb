﻿Public Class BECertificado
#Region "CamposCertificado"
    Private m_idcertificado As [String]
    Private m_idproducto As Int32
    Private m_numcertificado As [Decimal]
    Private m_opcion As [String]
    Private m_idfrecuencia As Int32
    Private m_idoficina As Int32
    Private m_idinformador As [String]
    Private m_idmediopago As [String]
    Private m_idestadocertificado As Int32
    Private m_montoasegurado As [Decimal]
    Private m_primabruta As [Decimal]
    Private m_iva As [Decimal]
    Private m_primatotal As [Decimal]
    Private m_primacobrar As [Decimal]
    Private m_numerocuenta As [String]
    Private m_vencimiento As DateTime
    Private m_vigencia As DateTime
    Private m_puntos As Int32
    Private m_idmotivoanulacion As Int32
    Private m_consistente As [Boolean]
    Private m_puntosanual As Int32
    Private m_idciudad As Int32
    Private m_direccion As [String]
    Private m_DirNumero As String = String.Empty
    Private m_DirPiso As String = String.Empty
    Private m_DirDpto As String = String.Empty
    Private m_DirCodPostal As String = String.Empty
    Private m_telefono As [String]
    Private m_nombre1 As [String]
    Private m_nombre2 As [String]
    Private m_apellido1 As [String]
    Private m_apellido2 As [String]
    Private m_cccliente As [String]
    Private m_idtipodocumento As [String]
    Private m_digitacion As DateTime
    Private m_incentivo As [Decimal]
    Private m_saldoincentivo As [Decimal]
    Private m_usuariocreacion As [String]
    Private m_usuariomodificacion As [String]
    Private m_fechamodificacion As DateTime
    Private m_solicitudanulacion As DateTime
    Private m_digitacionanulacion As DateTime
    Private m_efectuaranulacion As DateTime
    Private m_venta As DateTime
    Private m_finvigencia As DateTime
    Private m_idmonedaprima As [String]
    Private m_idmonedacobro As [String]
    Private m_afiliadohasta As DateTime
    Private m_idciclo As Int32
    Private m_diagenera As Int16
    Private m_mesgenera As Int16
    Private m_observacion As [String]
    Private m_nombreprovincia As [String]
    Private m_nombredistrito As [String]
    Private m_nombredepartamento As [String]
    Private m_fechanacimiento As DateTime
    Private m_idcertificadocrossselling As [String]
 
#End Region

#Region "CamposConsulta"
    Private m_nombreproducto As [String]
    Private m_codigooficina As [String]
    Private m_nombreoficina As [String]
    Private m_nombreinformador As [String]
    Private m_nombremediopago As [String]
    Private m_nombrefrecuencia As [String]
    Private m_nombretipodocumento As [String]
    Private m_nombreciudad As [String]
    Private m_nombredpto As [String]
    Private m_nombreestadocertificado As [String]
    Private m_nombremotivoanulacion As [String]
    Private m_nombremonedaprima As [String]
    Private m_nombremonedacobro As [String]
    Private m_monedacuenta As [String]
    Private m_bin As [String]
    Private m_estadoCivil As [String]
    Private m_sexo As [String]
    Private m_email As [String]
    Private m_ocupacion As [String]
#End Region

#Region "PropiedadesCertificado"
    Public Property IdCertificado() As [String]
        Get
            Return m_idcertificado
        End Get
        Set(ByVal value As [String])
            m_idcertificado = value
        End Set
    End Property

    Public Property IdCertificadoCrossSelling() As [String]
        Get
            Return m_idcertificadocrossselling
        End Get
        Set(ByVal value As [String])
            m_idcertificadocrossselling = value
        End Set
    End Property

    Public Property IdProducto() As Int32
        Get
            Return m_idproducto
        End Get
        Set(ByVal value As Int32)
            m_idproducto = value
        End Set
    End Property
    Public Property NumCertificado() As [Decimal]
        Get
            Return m_numcertificado
        End Get
        Set(ByVal value As [Decimal])
            m_numcertificado = value
        End Set
    End Property
    Public Property Opcion() As [String]
        Get
            Return m_opcion
        End Get
        Set(ByVal value As [String])
            m_opcion = value
        End Set
    End Property
    Public Property IdFrecuencia() As Int32
        Get
            Return m_idfrecuencia
        End Get
        Set(ByVal value As Int32)
            m_idfrecuencia = value
        End Set
    End Property
    Public Property IdOficina() As Int32
        Get
            Return m_idoficina
        End Get
        Set(ByVal value As Int32)
            m_idoficina = value
        End Set
    End Property
    Public Property IdInformador() As [String]
        Get
            Return m_idinformador
        End Get
        Set(ByVal value As [String])
            m_idinformador = value
        End Set
    End Property
    Public Property IdMedioPago() As [String]
        Get
            Return m_idmediopago
        End Get
        Set(ByVal value As [String])
            m_idmediopago = value
        End Set
    End Property
    Public Property IdEstadoCertificado() As Int32
        Get
            Return m_idestadocertificado
        End Get
        Set(ByVal value As Int32)
            m_idestadocertificado = value
        End Set
    End Property
    Public Property MontoAsegurado() As [Decimal]
        Get
            Return m_montoasegurado
        End Get
        Set(ByVal value As [Decimal])
            m_montoasegurado = value
        End Set
    End Property
    Public Property PrimaBruta() As [Decimal]
        Get
            Return m_primabruta
        End Get
        Set(ByVal value As [Decimal])
            m_primabruta = value
        End Set
    End Property
    Public Property Iva() As [Decimal]
        Get
            Return m_iva
        End Get
        Set(ByVal value As [Decimal])
            m_iva = value
        End Set
    End Property
    Public Property PrimaTotal() As [Decimal]
        Get
            Return m_primatotal
        End Get
        Set(ByVal value As [Decimal])
            m_primatotal = value
        End Set
    End Property
    Public Property PrimaCobrar() As [Decimal]
        Get
            Return m_primacobrar
        End Get
        Set(ByVal value As [Decimal])
            m_primacobrar = value
        End Set
    End Property
    Public Property NumeroCuenta() As [String]
        Get
            Return m_numerocuenta
        End Get
        Set(ByVal value As [String])
            m_numerocuenta = value
        End Set
    End Property
    Public Property Vencimiento() As DateTime
        Get
            Return m_vencimiento
        End Get
        Set(ByVal value As DateTime)
            m_vencimiento = value
        End Set
    End Property
    Public Property Vigencia() As DateTime
        Get
            Return m_vigencia
        End Get
        Set(ByVal value As DateTime)
            m_vigencia = value
        End Set
    End Property
    Public Property Puntos() As Int32
        Get
            Return m_puntos
        End Get
        Set(ByVal value As Int32)
            m_puntos = value
        End Set
    End Property
    Public Property IdMotivoAnulacion() As Int32
        Get
            Return m_idmotivoanulacion
        End Get
        Set(ByVal value As Int32)
            m_idmotivoanulacion = value
        End Set
    End Property
    Public Property Consistente() As [Boolean]
        Get
            Return m_consistente
        End Get
        Set(ByVal value As [Boolean])
            m_consistente = value
        End Set
    End Property
    Public Property PuntosAnual() As Int32
        Get
            Return m_puntosanual
        End Get
        Set(ByVal value As Int32)
            m_puntosanual = value
        End Set
    End Property
    Public Property IdCiudad() As Int32
        Get
            Return m_idciudad
        End Get
        Set(ByVal value As Int32)
            m_idciudad = value
        End Set
    End Property
    Public Property Direccion() As [String]
        Get
            Return m_direccion
        End Get
        Set(ByVal value As [String])
            m_direccion = value
        End Set
    End Property

    Public Property DirNumero() As String
        Get
            Return m_DirNumero
        End Get
        Set(ByVal value As String)
            m_DirNumero = value
        End Set
    End Property

    Public Property DirPiso() As String
        Get
            Return m_DirPiso
        End Get
        Set(ByVal value As String)
            m_DirPiso = value
        End Set
    End Property

    Public Property DirDpto() As String
        Get
            Return m_DirDpto
        End Get
        Set(ByVal value As String)
            m_DirDpto = value
        End Set
    End Property

    Public Property DirCodPostal() As String
        Get
            Return m_DirCodPostal
        End Get
        Set(ByVal value As String)
            m_DirCodPostal = value
        End Set
    End Property


    Public Property Telefono() As [String]
        Get
            Return m_telefono
        End Get
        Set(ByVal value As [String])
            m_telefono = value
        End Set
    End Property
    Public Property Nombre1() As [String]
        Get
            Return m_nombre1
        End Get
        Set(ByVal value As [String])
            m_nombre1 = value
        End Set
    End Property
    Public Property Nombre2() As [String]
        Get
            Return m_nombre2
        End Get
        Set(ByVal value As [String])
            m_nombre2 = value
        End Set
    End Property
    Public Property Apellido1() As [String]
        Get
            Return m_apellido1
        End Get
        Set(ByVal value As [String])
            m_apellido1 = value
        End Set
    End Property
    Public Property Apellido2() As [String]
        Get
            Return m_apellido2
        End Get
        Set(ByVal value As [String])
            m_apellido2 = value
        End Set
    End Property
    Public Property Cccliente() As [String]
        Get
            Return m_cccliente
        End Get
        Set(ByVal value As [String])
            m_cccliente = value
        End Set
    End Property
    Public Property IdTipoDocumento() As [String]
        Get
            Return m_idtipodocumento
        End Get
        Set(ByVal value As [String])
            m_idtipodocumento = value
        End Set
    End Property
    Public Property Digitacion() As DateTime
        Get
            Return m_digitacion
        End Get
        Set(ByVal value As DateTime)
            m_digitacion = value
        End Set
    End Property
    Public Property Incentivo() As [Decimal]
        Get
            Return m_incentivo
        End Get
        Set(ByVal value As [Decimal])
            m_incentivo = value
        End Set
    End Property
    Public Property SaldoIncentivo() As [Decimal]
        Get
            Return m_saldoincentivo
        End Get
        Set(ByVal value As [Decimal])
            m_saldoincentivo = value
        End Set
    End Property
    Public Property UsuarioCreacion() As [String]
        Get
            Return m_usuariocreacion
        End Get
        Set(ByVal value As [String])
            m_usuariocreacion = value
        End Set
    End Property
    Public Property UsuarioModificacion() As [String]
        Get
            Return m_usuariomodificacion
        End Get
        Set(ByVal value As [String])
            m_usuariomodificacion = value
        End Set
    End Property
    Public Property FechaModificacion() As DateTime
        Get
            Return m_fechamodificacion
        End Get
        Set(ByVal value As DateTime)
            m_fechamodificacion = value
        End Set
    End Property
    Public Property SolicitudAnulacion() As DateTime
        Get
            Return m_solicitudanulacion
        End Get
        Set(ByVal value As DateTime)
            m_solicitudanulacion = value
        End Set
    End Property
    Public Property DigitacionAnulacion() As DateTime
        Get
            Return m_digitacionanulacion
        End Get
        Set(ByVal value As DateTime)
            m_digitacionanulacion = value
        End Set
    End Property
    Public Property EfectuarAnulacion() As DateTime
        Get
            Return m_efectuaranulacion
        End Get
        Set(ByVal value As DateTime)
            m_efectuaranulacion = value
        End Set
    End Property
    Public Property Venta() As DateTime
        Get
            Return m_venta
        End Get
        Set(ByVal value As DateTime)
            m_venta = value
        End Set
    End Property
    Public Property FinVigencia() As DateTime
        Get
            Return m_finvigencia
        End Get
        Set(ByVal value As DateTime)
            m_finvigencia = value
        End Set
    End Property
    Public Property IdMonedaPrima() As [String]
        Get
            Return m_idmonedaprima
        End Get
        Set(ByVal value As [String])
            m_idmonedaprima = value
        End Set
    End Property
    Public Property IdMonedaCobro() As [String]
        Get
            Return m_idmonedacobro
        End Get
        Set(ByVal value As [String])
            m_idmonedacobro = value
        End Set
    End Property
    Public Property AfiliadoHasta() As DateTime
        Get
            Return m_afiliadohasta
        End Get
        Set(ByVal value As DateTime)
            m_afiliadohasta = value
        End Set
    End Property
    Public Property IdCiclo() As Int32
        Get
            Return m_idciclo
        End Get
        Set(ByVal value As Int32)
            m_idciclo = value
        End Set
    End Property
    Public Property DiaGenera() As Int16
        Get
            Return m_diagenera
        End Get
        Set(ByVal value As Int16)
            m_diagenera = value
        End Set
    End Property
    Public Property MesGenera() As Int16
        Get
            Return m_mesgenera
        End Get
        Set(ByVal value As Int16)
            m_mesgenera = value
        End Set
    End Property
    Public Property BIN() As [String]
        Get
            Return m_bin
        End Get
        Set(ByVal value As [String])
            m_bin = value
        End Set
    End Property

    Public Property EstadoCivil() As [String]
        Get
            Return m_estadoCivil
        End Get
        Set(ByVal value As [String])
            m_estadoCivil = value
        End Set
    End Property
    Public Property Sexo() As [String]
        Get
            Return m_sexo
        End Get
        Set(ByVal value As [String])
            m_sexo = value
        End Set
    End Property
    Public Property FechaNacimiento() As DateTime
        Get
            Return m_fechanacimiento
        End Get
        Set(ByVal value As DateTime)
            m_fechanacimiento = value
        End Set
    End Property

    Public Property Observacion() As [String]
        Get
            Return m_observacion
        End Get
        Set(ByVal value As [String])
            m_observacion = value
        End Set
    End Property
#End Region

#Region "PropiedadesConsulta"
    Public Property NombreProducto() As [String]
        Get
            Return m_nombreproducto
        End Get
        Set(ByVal value As [String])
            m_nombreproducto = value
        End Set
    End Property

    Public Property CodigoOficina() As [String]
        Get
            Return m_codigooficina
        End Get
        Set(ByVal value As [String])
            m_codigooficina = value
        End Set
    End Property

    Public Property NombreOficina() As [String]
        Get
            Return m_nombreoficina
        End Get
        Set(ByVal value As [String])
            m_nombreoficina = value
        End Set
    End Property
    Public Property NombreInformador() As [String]
        Get
            Return m_nombreinformador
        End Get
        Set(ByVal value As [String])
            m_nombreinformador = value
        End Set
    End Property
    Public Property NombreMedioPago() As [String]
        Get
            Return m_nombremediopago
        End Get
        Set(ByVal value As [String])
            m_nombremediopago = value
        End Set
    End Property
    Public Property NombreFrecuencia() As [String]
        Get
            Return m_nombrefrecuencia
        End Get
        Set(ByVal value As [String])
            m_nombrefrecuencia = value
        End Set
    End Property
    Public Property NombreTipoDocumento() As [String]
        Get
            Return m_nombretipodocumento
        End Get
        Set(ByVal value As [String])
            m_nombretipodocumento = value
        End Set
    End Property
    Public Property NombreCiudad() As [String]
        Get
            Return m_nombreciudad
        End Get
        Set(ByVal value As [String])
            m_nombreciudad = value
        End Set
    End Property
    Public Property NombreDpto() As [String]
        Get
            Return m_nombredpto
        End Get
        Set(ByVal value As [String])
            m_nombredpto = value
        End Set
    End Property
    Public Property NombreEstadoCertificado() As [String]
        Get
            Return m_nombreestadocertificado
        End Get
        Set(ByVal value As [String])
            m_nombreestadocertificado = value
        End Set
    End Property
    Public Property NombreMotivoAnulacion() As [String]
        Get
            Return m_nombremotivoanulacion
        End Get
        Set(ByVal value As [String])
            m_nombremotivoanulacion = value
        End Set
    End Property

    ''' <summary>
    ''' Nombre de la Provincia
    ''' </summary>
    Public Property NombreProvincia() As [String]
        Get
            Return m_nombreprovincia
        End Get
        Set(ByVal value As [String])
            m_nombreprovincia = value
        End Set
    End Property

    ''' <summary>
    ''' Nombre del Distrito
    ''' </summary>
    Public Property NombreDistrito() As [String]
        Get
            Return m_nombredistrito
        End Get
        Set(ByVal value As [String])
            m_nombredistrito = value
        End Set
    End Property

    ''' <summary>
    ''' Nombre del Departamento
    ''' </summary>
    Public Property NombreDepartamento() As [String]
        Get
            Return m_nombredepartamento
        End Get
        Set(ByVal value As [String])
            m_nombredepartamento = value
        End Set
    End Property

    Public Property NombreMonedaCobro() As [String]
        Get
            Return m_nombremonedacobro
        End Get
        Set(ByVal value As [String])
            m_nombremonedacobro = value
        End Set
    End Property

    Public Property NombreMonedaPrima() As [String]
        Get
            Return m_nombremonedaprima
        End Get
        Set(ByVal value As [String])
            m_nombremonedaprima = value
        End Set
    End Property

    Public Property MonedaCuenta() As [String]
        Get
            Return m_monedacuenta
        End Get
        Set(ByVal value As [String])
            m_monedacuenta = value
        End Set
    End Property
    ''' <summary>
    ''' Email del titular.
    ''' </summary>
    Public Property Email() As [String]
        Get
            Return Me.m_email
        End Get
        Set(ByVal value As [String])
            Me.m_email = value
        End Set
    End Property
    ''' <summary>
    ''' Ocupacion del titular.
    ''' </summary>
    Public Property Ocupacion() As [String]
        Get
            Return Me.m_ocupacion
        End Get
        Set(ByVal value As [String])
            Me.m_ocupacion = value
        End Set
    End Property
#End Region

End Class
