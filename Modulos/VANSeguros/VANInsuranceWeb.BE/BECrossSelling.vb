﻿Imports System.IO
Imports System.Xml.Serialization

<Serializable()> _
<System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=True), _
XmlRoot("CrossSelling")> _
Public Class BECrossSelling
    Inherits BEBase

    Private _idProducto As Integer
    Private _tipoProducto As String
    Private _nombreTipoProducto As String
    Private _nombre As String
    Private _tieneMedioPagoHabilitado As Boolean
    Private _tieneSolicitudContacto As Boolean
    Private _tieneOtroCertificado As Boolean
    Private _ventaHabilitada As Boolean
    Private _informacionAdicional As String
    Private _informacionField() As BECrossSellingInfoAdicional

    <System.Xml.Serialization.XmlArrayItemAttribute("InfoAdicional", IsNullable:=False)> _
    Public Property Informacion() As BECrossSellingInfoAdicional()
        Get
            Return Me._informacionField
        End Get
        Set(value As BECrossSellingInfoAdicional())
            Me._informacionField = value
        End Set
    End Property

    Public Property IDProducto() As Integer
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Integer)
            _idProducto = value
        End Set
    End Property

    Public Property TipoProducto() As String
        Get
            Return _tipoProducto
        End Get
        Set(ByVal value As String)
            _tipoProducto = value
        End Set
    End Property

    Public Property NombreTipoProducto() As String
        Get
            Return _nombreTipoProducto
        End Get
        Set(ByVal value As String)
            _nombreTipoProducto = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Public Property TieneMedioPagoHabilitado() As Boolean
        Get
            Return _tieneMedioPagoHabilitado
        End Get
        Set(ByVal value As Boolean)
            _tieneMedioPagoHabilitado = value
        End Set
    End Property

    Public Property TieneSolicitudContacto() As Boolean
        Get
            Return _tieneSolicitudContacto
        End Get
        Set(ByVal value As Boolean)
            _tieneSolicitudContacto = value
        End Set
    End Property

    Public Property TieneOtroCertificado() As Boolean
        Get
            Return _tieneOtroCertificado
        End Get
        Set(ByVal value As Boolean)
            _tieneOtroCertificado = value
        End Set
    End Property

    Public Property VentaHabilitada() As Boolean
        Get
            Return _ventaHabilitada
        End Get
        Set(ByVal value As Boolean)
            _ventaHabilitada = value
        End Set
    End Property

    Public Property InformacionAdicional() As String
        Get
            Return _informacionAdicional
        End Get
        Set(ByVal value As String)
            _informacionAdicional = value
        End Set
    End Property


#Region "CamposConsulta"

    Private _sIDUsuario As String
    Private _sIdsMediosDePagoCliente As List(Of BEMedioPagoCliente) = New List(Of BEMedioPagoCliente)
    Private _sIDProducto As Integer
    Private _sIDTipoDocumento As String
    Private _sNumeroDocumento As Integer
    Private _sIDMedioPagoUtilizado As String
    Private _sNumeroDeCuentaUtilizado As String
    Private _sIDMonedaCobroUtilizado As String

    Public Property SIDUsuario() As String
        Get
            Return _sIDUsuario
        End Get
        Set(ByVal value As String)
            _sIDUsuario = value
        End Set
    End Property

    Public Property SIdsMediosDePagoCliente() As List(Of BEMedioPagoCliente)
        Get
            Return _sIdsMediosDePagoCliente
        End Get
        Set(ByVal value As List(Of BEMedioPagoCliente))
            _sIdsMediosDePagoCliente = value
        End Set
    End Property

    Public Property SIDProducto() As Integer
        Get
            Return _sIDProducto
        End Get
        Set(ByVal value As Integer)
            _sIDProducto = value
        End Set
    End Property

    Public Property SIDTipoDocumento() As String
        Get
            Return _sIDTipoDocumento
        End Get
        Set(ByVal value As String)
            _sIDTipoDocumento = value
        End Set
    End Property

    Public Property SNumeroDocumento() As Integer
        Get
            Return _sNumeroDocumento
        End Get
        Set(ByVal value As Integer)
            _sNumeroDocumento = value
        End Set
    End Property

    Public Property SIDMedioPagoUtilizado() As String
        Get
            Return _sIDMedioPagoUtilizado
        End Get
        Set(ByVal value As String)
            _sIDMedioPagoUtilizado = value
        End Set
    End Property

    Public Property SNumeroDeCuentaUtilizado() As String
        Get
            Return _sNumeroDeCuentaUtilizado
        End Get
        Set(ByVal value As String)
            _sNumeroDeCuentaUtilizado = value
        End Set
    End Property

    Public Property SIDMonedaCobroUtilizado() As String
        Get
            Return _sIDMonedaCobroUtilizado
        End Get
        Set(ByVal value As String)
            _sIDMonedaCobroUtilizado = value
        End Set
    End Property

#End Region

End Class
