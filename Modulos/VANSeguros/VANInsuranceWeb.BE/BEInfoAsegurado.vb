﻿Public Class BEInfoAsegurado
#Region "Campos"
    Private m_idProducto As Int32
    Private m_idinfoAsegurado As Int32
    Private m_nombre As String
    Private m_idtipodato As String
    Private m_descripcion As String
    Private m_campo As String
#End Region

#Region "Propiedades"

    Public Property IdProducto() As Int32
        Get
            Return m_idProducto
        End Get
        Set(ByVal value As Int32)
            m_idProducto = value
        End Set
    End Property

    Public Property IdInfoAsegurado() As Int32
        Get
            Return m_idinfoAsegurado
        End Get
        Set(ByVal value As Int32)
            m_idinfoAsegurado = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return m_nombre
        End Get
        Set(ByVal value As String)
            m_nombre = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return m_descripcion
        End Get
        Set(ByVal value As String)
            m_descripcion = value
        End Set
    End Property

    Public Property IdTipoDato() As String
        Get
            Return m_idtipodato
        End Get
        Set(ByVal value As String)
            m_idtipodato = value
        End Set
    End Property

    ''' <summary>
    ''' Campo en el que se guardará el InfoProductoC (ValorString,ValorDate,ValorDecimal)
    ''' </summary>
    Public Property Campo() As String
        Get
            Return m_campo
        End Get
        Set(ByVal value As String)
            m_campo = value
        End Set
    End Property

#End Region
End Class