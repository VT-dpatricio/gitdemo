﻿Public Class ObjetoEntidadBE
    Inherits GenericEntity

    Sub New()

    End Sub

    Private _IdObjetoEntidad As Integer
    Private _IdEntidad As Integer
    Private _Objeto As String


    Property IdObjetoEntidad() As Integer
        Get
            Return _IdObjetoEntidad
        End Get
        Set(ByVal value As Integer)
            _IdObjetoEntidad = value
        End Set
    End Property
    Property IdEntidad() As Integer
        Get
            Return _IdEntidad
        End Get
        Set(ByVal value As Integer)
            _IdEntidad = value
        End Set
    End Property
    Property Objeto() As String
        Get
            Return _Objeto
        End Get
        Set(ByVal value As String)
            _Objeto = value
        End Set
    End Property

End Class
