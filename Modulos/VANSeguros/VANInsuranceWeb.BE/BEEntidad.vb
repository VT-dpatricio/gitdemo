Public Class BEEntidad
    Inherits BEBase

    Private _iDEntidad As Integer = 0
    Private _nombre As String = String.Empty
    Private _razonSocial As String = String.Empty
    Private _ruc As String = String.Empty
    Private _direccion As String = String.Empty
    Private _usuario As String = String.Empty
    Private _buscar As String = String.Empty
    Private _estado As Boolean = True

    Public Property Usuario() As String
        Get
            Return Me._usuario
        End Get
        Set(ByVal value As String)
            Me._usuario = value
        End Set
    End Property

    Public Property RazonSocial() As String
        Get
            Return Me._razonSocial
        End Get
        Set(ByVal value As String)
            Me._razonSocial = value.ToUpper
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._nombre
        End Get
        Set(ByVal value As String)
            Me._nombre = value.ToUpper
        End Set
    End Property

    Public Property RUC() As String
        Get
            Return Me._ruc
        End Get
        Set(ByVal value As String)
            Me._ruc = value
        End Set
    End Property

    Public Property Direccion() As String
        Get
            Return Me._direccion
        End Get
        Set(ByVal value As String)
            Me._direccion = value.ToUpper
        End Set
    End Property

    Public Property IDEntidad() As Integer
        Get
            Return _iDEntidad
        End Get
        Set(ByVal value As Integer)
            _iDEntidad = value
        End Set
    End Property

    Public Property Buscar() As String
        Get
            Return Me._buscar
        End Get
        Set(ByVal value As String)
            Me._buscar = value
        End Set
    End Property

    Public Property Estado() As Boolean
        Get
            Return _estado
        End Get
        Set(ByVal value As Boolean)
            _estado = value
        End Set
    End Property
End Class
