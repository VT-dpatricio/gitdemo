﻿Public Class TipoValidacionBE
    Inherits GenericEntity

    Sub New()

    End Sub

    Private _IdTipoValidacion As Integer
    Private _Descripcion As String



    Property IdTipoValidacion() As Integer
        Get
            Return _IdTipoValidacion
        End Get
        Set(ByVal value As Integer)
            _IdTipoValidacion = value
        End Set
    End Property
    Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property

End Class
