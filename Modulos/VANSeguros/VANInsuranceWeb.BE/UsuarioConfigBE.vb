﻿Public Class UsuarioConfigBE
    Inherits GenericEntity

    Private _IdUsuario As String
    Property IdUsuario() As String
        Get
            Return _IdUsuario
        End Get
        Set(ByVal value As String)
            _IdUsuario = value
        End Set
    End Property

    Private _Usuario As String
    Property Usuario() As String
        Get
            Return _Usuario
        End Get
        Set(ByVal value As String)
            _Usuario = value
        End Set
    End Property

    Private _IdAseguradora As String
    Public Property IdAseguradora() As String
        Get
            Return _IdAseguradora
        End Get
        Set(ByVal value As String)
            _IdAseguradora = value
        End Set
    End Property


    Private _Aseguradora As String
    Public Property Aseguradora() As String
        Get
            Return _Aseguradora
        End Get
        Set(ByVal value As String)
            _Aseguradora = value
        End Set
    End Property



    Private _IdProducto As String
    Public Property IdProducto() As String
        Get
            Return _IdProducto
        End Get
        Set(ByVal value As String)
            _IdProducto = value
        End Set
    End Property


    Private _Producto As String
    Public Property Producto() As String
        Get
            Return _Producto
        End Get
        Set(ByVal value As String)
            _Producto = value
        End Set
    End Property


    Private _CodBais As String
    Public Property CodBais() As String
        Get
            Return _CodBais
        End Get
        Set(ByVal value As String)
            _CodBais = value
        End Set
    End Property



    Sub New()

    End Sub


End Class
