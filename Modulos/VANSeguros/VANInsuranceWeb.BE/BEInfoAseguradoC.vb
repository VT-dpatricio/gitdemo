﻿Public Class BEInfoAseguradoC
#Region "Campos"
    Private m_idcertificado As [String]
    Private m_idinfoasegurado As Int32
    Private m_consecutivo As Int16
    Private m_valornum As [Decimal]
    Private m_valordate As DateTime
    Private m_valorstring As [String]
    Private m_nombre As [String]
    Private m_descripcion As [String]
    Private m_campo As String
#End Region

#Region "Propiedades"
    Public Property Descripcion() As [String]
        Get
            Return m_descripcion
        End Get
        Set(ByVal value As [String])
            m_descripcion = value
        End Set
    End Property
    Public Property IdCertificado() As [String]
        Get
            Return m_idcertificado
        End Get
        Set(ByVal value As [String])
            m_idcertificado = value
        End Set
    End Property
    Public Property Nombre() As [String]
        Get
            Return m_nombre
        End Get
        Set(ByVal value As [String])
            m_nombre = value
        End Set
    End Property
    Public Property IdInfoAsegurado() As Int32
        Get
            Return m_idinfoasegurado
        End Get
        Set(ByVal value As Int32)
            m_idinfoasegurado = value
        End Set
    End Property
    Public Property Consecutivo() As Int16
        Get
            Return m_consecutivo
        End Get
        Set(ByVal value As Int16)
            m_consecutivo = value
        End Set
    End Property
    Public Property ValorNum() As [Decimal]
        Get
            Return m_valornum
        End Get
        Set(ByVal value As [Decimal])
            m_valornum = value
        End Set
    End Property
    Public Property ValorDate() As DateTime
        Get
            Return m_valordate
        End Get
        Set(ByVal value As DateTime)
            m_valordate = value
        End Set
    End Property
    Public Property ValorString() As [String]
        Get
            Return m_valorstring
        End Get
        Set(ByVal value As [String])
            m_valorstring = value
        End Set
    End Property

    Public Property Campo() As String
        Get
            Return m_campo
        End Get
        Set(ByVal value As String)
            m_campo = value
        End Set
    End Property

#End Region
End Class
