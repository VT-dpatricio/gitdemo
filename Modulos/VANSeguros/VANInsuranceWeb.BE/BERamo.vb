﻿Public Class BERamo

    Inherits BEBase
    Private _idRamo As Int32
    Private _nombre As String
    Private _buscar As String

    Public Property IdRamo() As Int32
        Get
            Return _idRamo
        End Get
        Set(ByVal value As Int32)
            _idRamo = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Public Property Buscar() As String
        Get
            Return _buscar
        End Get
        Set(ByVal value As String)
            _buscar = value
        End Set
    End Property
End Class

