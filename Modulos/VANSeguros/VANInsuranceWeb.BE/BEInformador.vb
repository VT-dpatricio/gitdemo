Public Class BEInformador
    Inherits BEBase
    Private _idInformador As String
    Private _idProducto As Int32
    Private _idOficina As Int32
    Private _oficina As String
    Private _nombre As String
    Private _buscar As String

    Public Property IdInformador() As String
        Get
            Return _idInformador
        End Get
        Set(ByVal value As String)
            _idInformador = value
        End Set
    End Property

    Public Property IdProducto() As Int32
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Int32)
            _idProducto = value
        End Set
    End Property

    Public Property IdOficina() As Int32
        Get
            Return _idOficina
        End Get
        Set(ByVal value As Int32)
            _idOficina = value
        End Set
    End Property

    Public Property Oficina() As String
        Get
            Return _oficina
        End Get
        Set(ByVal value As String)
            _oficina = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Public Property Buscar() As String
        Get
            Return _buscar
        End Get
        Set(ByVal value As String)
            _buscar = value
        End Set
    End Property

End Class
