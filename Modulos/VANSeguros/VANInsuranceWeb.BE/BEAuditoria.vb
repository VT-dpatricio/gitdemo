﻿Public Class BEAuditoria
#Region "Campos"
    Private m_feccreacion As DateTime
    Private m_usrcreacion As [String]
    Private m_fecmodificacion As DateTime
    Private m_usrmodificacion As [String]
    Private m_estado As [Boolean]
#End Region

#Region "Proiedades"
    ''' <summary>
    ''' Fecha de creación del registro.
    ''' </summary>
    Public Property FecCreacion() As DateTime
        Get
            Return Me.m_feccreacion
        End Get
        Set(ByVal value As DateTime)
            Me.m_feccreacion = value
        End Set
    End Property

    ''' <summary>
    ''' Usuario de creación.
    ''' </summary>
    Public Property UsrCreacion() As [String]
        Get
            Return Me.m_usrcreacion
        End Get
        Set(ByVal value As [String])
            Me.m_usrcreacion = value
        End Set
    End Property

    ''' <summary>
    ''' Fecha de modificación.
    ''' </summary>
    Public Property FecModificacion() As DateTime
        Get
            Return Me.m_fecmodificacion
        End Get
        Set(ByVal value As DateTime)
            Me.m_fecmodificacion = value
        End Set
    End Property

    ''' <summary>
    ''' Usuario de modificación.
    ''' </summary>
    Public Property UsrModificacion() As [String]
        Get
            Return Me.m_usrmodificacion
        End Get
        Set(ByVal value As [String])
            Me.m_usrmodificacion = value
        End Set
    End Property

    ''' <summary>
    ''' Estado del registro.
    ''' </summary>
    Public Property Estado() As [Boolean]
        Get
            Return Me.m_estado
        End Get
        Set(ByVal value As [Boolean])
            Me.m_estado = value
        End Set
    End Property
#End Region
End Class
