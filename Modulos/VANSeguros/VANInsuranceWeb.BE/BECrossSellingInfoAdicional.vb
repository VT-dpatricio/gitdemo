﻿Imports System.IO
Imports System.Xml.Serialization

<Serializable()> _
<XmlRoot("InfoAdicional", IsNullable:=False)> _
Public Class BECrossSellingInfoAdicional


    Private _descripcion As String
    Private _categoria As String


    <System.Xml.Serialization.XmlAttributeAttribute("descripcion")> _
    Public Property Descripcion() As String
        Get
            Descripcion = _descripcion
        End Get
        Set(ByVal Value As String)
            _descripcion = Value
        End Set
    End Property

    <System.Xml.Serialization.XmlAttributeAttribute("categoria")> _
    Public Property Categoria() As String
        Get
            Categoria = _categoria
        End Get
        Set(ByVal Value As String)
            _categoria = Value
        End Set
    End Property
End Class
