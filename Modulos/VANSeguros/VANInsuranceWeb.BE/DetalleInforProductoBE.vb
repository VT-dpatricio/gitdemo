﻿Public Class DetalleInfoProductoBE
    Inherits GenericEntity

    Sub New()
        _TipoValorBE = New TipoValorBE()
    End Sub

    Private _IdDetalleInfoProducto As Integer
    Private _IdInfoProducto As Integer
    Private _IdTipoValor As Integer
    Private _Descripcion As String
    Private _TipoValorBE As TipoValorBE
    Private _TipoValor As String


    Public Property TipoValorBE() As TipoValorBE
        Get
            Return _TipoValorBE
        End Get
        Set(ByVal value As TipoValorBE)
            _TipoValorBE = value
        End Set
    End Property


    Public Property TipoValor() As String
        Get
            Return _TipoValor
        End Get
        Set(ByVal value As String)
            _TipoValor = value
        End Set
    End Property


    Property IdDetalleInfoProducto() As Integer
        Get
            Return _IdDetalleInfoProducto
        End Get
        Set(ByVal value As Integer)
            _IdDetalleInfoProducto = value
        End Set
    End Property
    Property IdInfoProducto() As Integer
        Get
            Return _IdInfoProducto
        End Get
        Set(ByVal value As Integer)
            _IdInfoProducto = value
        End Set
    End Property
    Property IdTipoValor() As Integer
        Get
            Return _IdTipoValor
        End Get
        Set(ByVal value As Integer)
            _IdTipoValor = value
        End Set
    End Property
    Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property

End Class
