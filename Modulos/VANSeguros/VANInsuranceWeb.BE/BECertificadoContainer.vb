﻿Public Class BECertificadoContainer
    Public Property ObjBECertificado As New BECertificado
    Public Property LstBEAsegurado As New List(Of BEAsegurado)()
    Public Property LstBEBeneficiario As New List(Of BEBeneficiario)()
    Public Property LstBEInfoProductoC As New List(Of BEInfoProductoC)()
    Public Property LstBEInfoAseguradoC As New List(Of BEInfoAseguradoC)()

    Public Property DescripcionCiudad As String
    Public Property DescripcionCiudadRiesgo As String
    Public Property DescripcionOficina As String
    Public Property DescripcionInformador As String
End Class
