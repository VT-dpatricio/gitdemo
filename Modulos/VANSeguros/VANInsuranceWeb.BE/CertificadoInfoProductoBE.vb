﻿Public Class CertificadoInfoProductoBE
    Inherits GenericEntity

    Sub New()

    End Sub

    Private _IdCertificadoInfoProducto As Integer
    Private _IdProducto As Integer
    Private _IdInfoProducto As Integer
    Private _IdCertificado As Integer
  

    Property IdCertificadoInfoProducto() As Integer
        Get
            Return _IdCertificadoInfoProducto
        End Get
        Set(ByVal value As Integer)
            _IdCertificadoInfoProducto = value
        End Set
    End Property
    Property IdProducto() As Integer
        Get
            Return _IdProducto
        End Get
        Set(ByVal value As Integer)
            _IdProducto = value
        End Set
    End Property
    Property IdInfoProducto() As Integer
        Get
            Return _IdInfoProducto
        End Get
        Set(ByVal value As Integer)
            _IdInfoProducto = value
        End Set
    End Property
    Property IdCertificado() As Integer
        Get
            Return _IdCertificado
        End Get
        Set(ByVal value As Integer)
            _IdCertificado = value
        End Set
    End Property
    


End Class
