﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports Microsoft.Practices.EnterpriseLibrary.Data

Public Class DetalleInfoProductoDA

    Dim _db As Database = Nothing

    Public Sub New()
        _db = Conexion.Instancia.Database()
    End Sub

    Function Agregar(ByVal Objeto As DetalleInfoProductoBE, Optional ByVal Transac As DbTransaction = Nothing) As String
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_DetalleInfoProducto_Agregar", Nothing, Objeto.IdInfoProducto, Objeto.IdTipoValor, Objeto.Descripcion, Objeto.Estado, Objeto.UsuarioReg, Objeto.FechaReg, Objeto.UsuarioMod, Objeto.FechaMod)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Modificar(ByVal Objeto As DetalleInfoProductoBE, Optional ByVal Transac As DbTransaction = Nothing) As Integer
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_DetalleInfoProducto_Modificar", Objeto.IdDetalleInfoProducto, Objeto.IdInfoProducto, Objeto.IdTipoValor, Objeto.Descripcion, Objeto.Estado, Objeto.UsuarioMod, Objeto.FechaMod)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Eliminar(ByVal Objeto As DetalleInfoProductoBE, Optional ByVal Transac As DbTransaction = Nothing) As Integer
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_DetalleInfoProducto_Eliminar", Objeto.IdDetalleInfoProducto, Objeto.Estado, Objeto.UsuarioMod, Objeto.FechaMod)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Eliminar_InfoProducto(ByVal Objeto As DetalleInfoProductoBE, Optional ByVal Transac As DbTransaction = Nothing) As Integer
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_DetalleInfoProducto_Eliminar_Id", Objeto.IdInfoProducto)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of DetalleInfoProductoBE)
        Dim Lista As New List(Of DetalleInfoProductoBE)

        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_DetalleInfoProducto_Todos")
                While dr.Read()
                    Lista.Add(Populate.DetalleInfoProducto_Lista(dr))
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_DetalleInfoProducto_Todos")
                While dr.Read()
                    Lista.Add(Populate.DetalleInfoProducto_Lista(dr))
                End While
            End Using
        End If
        Return Lista
    End Function

    Function Listar_Id(ByVal Objeto As DetalleInfoProductoBE, Optional ByVal Transaction As DbTransaction = Nothing) As DetalleInfoProductoBE
        Dim _DetalleInforProductoBE As New DetalleInfoProductoBE

        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_DetalleInfoProducto_Id", Objeto.IdDetalleInfoProducto)
                While dr.Read()
                    _DetalleInforProductoBE = Populate.DetalleInfoProducto_Lista(dr)
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_DetalleInfoProducto_Id", Objeto.IdDetalleInfoProducto)
                While dr.Read()
                    _DetalleInforProductoBE = Populate.DetalleInfoProducto_Lista(dr)
                End While
            End Using
        End If
        Return _DetalleInforProductoBE
    End Function

    Function Listar_IdInfoProducto(ByVal Objeto As DetalleInfoProductoBE, Optional ByVal Transaction As DbTransaction = Nothing) As List(Of DetalleInfoProductoBE)
        Dim Lista As New List(Of DetalleInfoProductoBE)

        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_DetalleInfoProducto_IdInfoProducto", Objeto.IdInfoProducto)
                While dr.Read()
                    Lista.Add(Populate.DetalleInfoProducto_Lista(dr))
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_DetalleInfoProducto_IdInfoProducto", Objeto.IdInfoProducto)
                While dr.Read()
                    Lista.Add(Populate.DetalleInfoProducto_Lista(dr))
                End While
            End Using
        End If
        Return Lista
    End Function

End Class
