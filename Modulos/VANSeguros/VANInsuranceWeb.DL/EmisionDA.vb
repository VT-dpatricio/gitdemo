﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports Microsoft.Practices.EnterpriseLibrary.Data

Public Class EmisionDA

    Dim _db As Database = Nothing

    Public Sub New()
        _db = Conexion.Instancia.Database()
    End Sub


    Function Agregar(ByVal Objeto As EmisionBE, Optional ByVal Transac As DbTransaction = Nothing) As String
        Dim resultado As String = "0"
        Dim _PersonaDA As New PersonaDA
        Dim _FormaPagoDA As New FormaPagoDA
        Dim _CertificadoDA As New CertificadoDA
        Dim _RelacionDA As New RelacionDA
        Dim _InforProducto As New CertificadoDetalleInfoProductoDA

        Dim _IdCertificado As String = ""
        Dim _IdAsegurado As String = ""

        Dim _RelacionBE As New RelacionBE

        Try

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try

                        _CertificadoDA.Agregar(Objeto.CertificadoBE, Transaction)
                        _PersonaDA.Agregar(Objeto.AseguradoBE, Transaction)
                        _RelacionDA.Agregar(_RelacionBE, Transaction)
                        _FormaPagoDA.Agregar(Objeto.FormaPagoBE, Transaction)

                        For Each Entity As InfoProductoBE In Objeto.ListInfoProducto


                        Next




                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function


End Class
