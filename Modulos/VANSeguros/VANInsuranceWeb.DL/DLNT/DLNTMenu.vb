﻿Imports VAN.InsuranceWeb.BE
'Imports DataEntry.DA
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLNTMenu
    Inherits DLBase
    Implements IDLNTDataEntry
    ' Methods
    Public Function Seleccionar() As IList Implements IDLNTDataEntry.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pEntidad As BEBase) As IList Implements IDLNTDataEntry.Seleccionar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("BW_ListarMenu", cn)
        cm.CommandType = CommandType.StoredProcedure
        Dim oBE As BEMenu = DirectCast(pEntidad, BEMenu)
        cm.Parameters.Add("@IDUsuario", SqlDbType.VarChar, 50).Value = oBE.IDUsuario
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BEMenu
                oBE.IDMenu = rd.GetInt32(rd.GetOrdinal("IDMenu"))
                oBE.IDMenuPadre = rd.GetInt32(rd.GetOrdinal("IDMenuPadre"))
                oBE.Descripcion = rd.GetString(rd.GetOrdinal("Descripcion"))
                oBE.Url = rd.GetString(rd.GetOrdinal("Url"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As IList Implements IDLNTDataEntry.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BEBase Implements IDLNTDataEntry.SeleccionarBE
        Throw New Exception("The method or operation is not implemented.")
    End Function

End Class


