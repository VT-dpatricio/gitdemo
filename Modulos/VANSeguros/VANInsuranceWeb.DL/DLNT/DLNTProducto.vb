Imports VAN.InsuranceWeb.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLNTProducto
    Inherits DLBase
    Implements IDLNTDataEntry


    Public Function Seleccionar() As System.Collections.IList Implements IDLNTDataEntry.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pEntidad As BEBase) As System.Collections.IList Implements IDLNTDataEntry.Seleccionar
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("BW_ListarProducto", cn)
        cm.CommandType = CommandType.StoredProcedure
        Dim oBE As BEProducto = DirectCast(pEntidad, BEProducto)
        cm.Parameters.Add("@IDEntidad", SqlDbType.Int).Value = oBE.IDEntidad
        cm.Parameters.Add("@Buscar", SqlDbType.VarChar, 250).Value = oBE.Buscar.ToString
        Dim lista As New ArrayList
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            Do While rd.Read
                oBE = New BEProducto
                oBE.IDProducto = rd.GetInt32(rd.GetOrdinal("IDProducto"))
                oBE.IDEntidad = rd.GetInt32(rd.GetOrdinal("IDEntidad"))
                oBE.IDAsegurador = rd.GetInt32(rd.GetOrdinal("IDAsegurador"))
                oBE.Nombre = rd.GetString(rd.GetOrdinal("Nombre"))
                oBE.TipoProducto = rd.GetString(rd.GetOrdinal("TipoProducto"))
                oBE.Entidad = rd.GetString(rd.GetOrdinal("Entidad"))
                oBE.Activo = rd.GetBoolean(rd.GetOrdinal("Activo"))
                lista.Add(oBE)
                oBE = Nothing
            Loop
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return lista
    End Function

    Public Function Seleccionar(ByVal pCodigo As Integer) As System.Collections.IList Implements IDLNTDataEntry.Seleccionar
        Throw New Exception("The method or operation is not implemented.")
    End Function

    Public Function Seleccionar(ByVal pIDEntidad As Integer, ByVal pIDUsuario As Integer) As System.Collections.IList
        Throw New Exception("The method or operation is not implemented.")
    End Function


    Public Function SeleccionarBE(ByVal pCodigo As Integer) As BEBase Implements IDLNTDataEntry.SeleccionarBE
        Dim cn As New SqlConnection(MyBase.CadenaConexion)
        Dim cm As New SqlCommand("BW_SeleccionarProducto", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@IDProducto", SqlDbType.Int).Value = pCodigo
        Dim oBE As New BEProducto
        Try
            cn.Open()
            Dim rd As SqlDataReader = cm.ExecuteReader
            If rd.Read Then
                oBE.IdProducto = rd.GetInt32(rd.GetOrdinal("IDProducto"))
                oBE.IdEntidad = rd.GetInt32(rd.GetOrdinal("IDEntidad"))
                oBE.IdAsegurador = rd.GetInt32(rd.GetOrdinal("IDAsegurador"))
                oBE.Nombre = rd.GetString(rd.GetOrdinal("Nombre"))
                oBE.TipoProducto = rd.GetString(rd.GetOrdinal("TipoProducto"))
                oBE.Entidad = rd.GetString(rd.GetOrdinal("Entidad"))
                oBE.Asegurador = rd.GetString(rd.GetOrdinal("Asegurador"))
                oBE.Activo = rd.GetBoolean(rd.GetOrdinal("Activo"))
                oBE.MaxAsegurados = rd.GetByte(rd.GetOrdinal("MaxAsegurados"))
                oBE.NacimientoRequerido = rd.GetBoolean(rd.GetOrdinal("NacimientoRequerido"))
                oBE.ValidarParentesco = rd.GetBoolean(rd.GetOrdinal("ValidarParentesco"))
                oBE.ObligaCuentaHabiente = rd.GetBoolean(rd.GetOrdinal("ObligaCuentaHabiente"))
                oBE.EdadMaxPermanencia = rd.GetByte(rd.GetOrdinal("EdadMaxPermanencia"))
                oBE.DiasAnulacion = rd.GetByte(rd.GetOrdinal("DiasAnulacion"))
                oBE.GeneraCobro = rd.GetBoolean(rd.GetOrdinal("GeneraCobro"))
                oBE.VigenciaCobro = rd.GetBoolean(rd.GetOrdinal("VigenciaCobro"))
                oBE.MaxPolizasAsegurado = rd.GetByte(rd.GetOrdinal("MaxPolizasAsegurado"))
                oBE.MaxPolizasCuentahabiente = rd.GetByte(rd.GetOrdinal("MaxPolizasCuentahabiente"))
                oBE.MaxPolizasRegalo = rd.GetByte(rd.GetOrdinal("MaxPolizasRegalo"))
                oBE.MaxCuentasTitular = rd.GetByte(rd.GetOrdinal("MaxCuentasTitular"))
                oBE.MaxPolizasAseguradoSinDNI = rd.GetByte(rd.GetOrdinal("MaxPolizasAseguradoSinDNI"))
                oBE.ValidaDireccionCer = rd.GetBoolean(rd.GetOrdinal("ValidaDireccionCer"))
                oBE.ValidaDireccionAse = rd.GetBoolean(rd.GetOrdinal("ValidaDireccionAse"))
                oBE.CuentaHabienteigualAsegurado = rd.GetBoolean(rd.GetOrdinal("CuentaHabienteigualAsegurado"))

                oBE.FrmCertificado = rd.GetString(rd.GetOrdinal("FrmCertificado"))
                oBE.FrmAsegurado = rd.GetString(rd.GetOrdinal("FrmAsegurado"))
                oBE.NroAsegurado = rd.GetInt32(rd.GetOrdinal("NroAsegurado"))
                oBE.NroBeneficiario = rd.GetInt32(rd.GetOrdinal("NroBeneficiario"))


                oBE.IdProducto = rd.GetInt32(rd.GetOrdinal("IDProducto"))
                oBE.IdEntidad = rd.GetInt32(rd.GetOrdinal("IDEntidad"))
                oBE.ReporteCertificado = rd.GetString(rd.GetOrdinal("ReporteCertificado"))
                oBE.ReporteSolicitud = rd.GetString(rd.GetOrdinal("ReporteSolicitud"))
                oBE.ReporteCondicion = rd.GetString(rd.GetOrdinal("ReporteCondicion"))
                oBE.ReporteDeclaracionJurada = rd.GetString(rd.GetOrdinal("ReporteDeclaracionJurada"))
            End If
            rd.Close()
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return oBE
    End Function
End Class
