﻿Imports VAN.InsuranceWeb.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLNTCertificado
    Inherits DLBase

    ''' <summary>
    ''' Obtiene el certificado
    ''' </summary>
    ''' <param name="pIdOficina">Codigo de la oficina</param>
    ''' <param name="pIdProducto">Numero del producto</param>
    ''' <returns>
    ''' Retorna un tipo Int32
    ''' </returns>
    Public Function ObtenerNumCertificado(ByVal pIdOficina As Int32, ByVal pIdProducto As Int32) As Int32
        Dim cn As New SqlConnection(CadenaConexion)
        Dim cm As New SqlCommand("BW_ObtenerNumCertificado", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@idOficina", SqlDbType.Int).Value = pIdOficina
        cm.Parameters.Add("@idProducto", SqlDbType.Int).Value = pIdProducto
        Dim numCertificado As Int32 = 0
        Dim oBE As New BECertificado
        Try
            cn.Open()
            numCertificado = Convert.ToInt32(cm.ExecuteScalar())
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return numCertificado
    End Function

    ''' <summary>
    ''' Obtiene el certificado
    ''' </summary>
    ''' <param name="pIdOficina">Codigo de la oficina</param>
    ''' <param name="pIdProducto">Numero del producto</param>
    ''' <param name="pNumCertificado">Numero de certificado</param>
    ''' <returns>
    ''' Retorna un tipo Int32
    ''' </returns>
    Public Function LiberarNumCertificado(ByVal pIdOficina As Int32, ByVal pIdProducto As Int32, ByVal pNumCertificado As Decimal) As Boolean
        Dim cn As New SqlConnection(CadenaConexion)
        Dim cm As New SqlCommand("BW_LiberarNumCertificado", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@idOficina", SqlDbType.Int).Value = pIdOficina
        cm.Parameters.Add("@idProducto", SqlDbType.Int).Value = pIdProducto
        cm.Parameters.Add("@numcertificado", SqlDbType.Int).Value = pNumCertificado
        Dim resp As Boolean = False
        Dim oBE As New BECertificado
        Try
            cn.Open()
            cm.ExecuteScalar()
            resp = True
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return resp
    End Function


    Public Function ValidarClienteDomicilio(ByVal pBECer As BECertificado) As String
        Dim cn As New SqlConnection(CadenaConexion)
        Dim cm As New SqlCommand("BW_ValidarClienteDomicilio", cn)
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.Add("@TipoDocumento", SqlDbType.VarChar, 3).Value = pBECer.IdTipoDocumento
        cm.Parameters.Add("@NroDocumento", SqlDbType.VarChar, 20).Value = pBECer.Cccliente
        cm.Parameters.Add("@Direccion", SqlDbType.VarChar, 250).Value = pBECer.Direccion
        cm.Parameters.Add("@DirNumero", SqlDbType.VarChar, 50).Value = pBECer.DirNumero
        cm.Parameters.Add("@DirPiso", SqlDbType.VarChar, 50).Value = pBECer.DirPiso
        cm.Parameters.Add("@DirDpto", SqlDbType.VarChar, 50).Value = pBECer.DirDpto
        cm.Parameters.Add("@DirCodPostal", SqlDbType.VarChar, 50).Value = pBECer.DirCodPostal
        cm.Parameters.Add("@IDCiudad", SqlDbType.Int).Value = pBECer.IdCiudad
        cm.Parameters.Add("@Mensaje", SqlDbType.VarChar, 300).Direction = ParameterDirection.Output
        Dim Mensaje As String = ""
        Try
            cn.Open()
            cm.ExecuteScalar()
            Mensaje = cm.Parameters("@Mensaje").Value
        Catch ex As Exception
            Throw ex
        Finally
            If (cn.State = ConnectionState.Open) Then
                cn.Close()
            End If
        End Try
        Return Mensaje
    End Function


















    ''' <summary>
    ''' Verifica la existencia de un certificado.
    ''' </summary>
    ''' <creador>
    ''' Diego Morales Rivero
    ''' </creador>
    ''' <fechacreacion>
    ''' 2010-12-10
    ''' </fechacreacion>
    ''' <param name="pIdCertificado">Codigo del certificado.</param>
    ''' <param name="pSqlCon">Objeto SqlConnection</param>
    ''' <returns>
    ''' Retorna un Entero: 1 si existe, 0 en caso contrario.
    ''' </returns>
    Public Function DAExistenciaCertificado(ByVal pIdCertificado As [String], ByVal pSqlCon As SqlConnection) As [Boolean]
        Dim Existe As [Boolean]

        Using sqlCmdExistCert As New SqlCommand("AONWebService_Certificado_Existencia", pSqlCon)
            sqlCmdExistCert.CommandType = CommandType.StoredProcedure

            Dim sqlIdCertificado As SqlParameter = sqlCmdExistCert.Parameters.Add("@idcertificado", SqlDbType.VarChar, 25)
            sqlIdCertificado.Direction = ParameterDirection.Input
            sqlIdCertificado.Value = pIdCertificado

            Existe = Convert.ToBoolean(sqlCmdExistCert.ExecuteScalar())
        End Using

        Return Existe
    End Function

    
    Public Function DALiberarNumCertificado(ByVal pIdOficina As Int32, ByVal pIdProducto As Int32, ByVal pNumCertificado As Decimal, ByVal pSqlCon As SqlConnection) As Int32
        Dim filasAfectadas As Int32

        Using sqlCmdObtCert As New SqlCommand("[dbo].[BAIS_LiberarNumCertificado]", pSqlCon)
            sqlCmdObtCert.CommandType = CommandType.StoredProcedure

            Dim sqlIdOficina As SqlParameter = sqlCmdObtCert.Parameters.Add("@idOficina", SqlDbType.Int, 25)
            sqlIdOficina.Direction = ParameterDirection.Input
            sqlIdOficina.Value = pIdOficina

            Dim sqlIdProducto As SqlParameter = sqlCmdObtCert.Parameters.Add("@idProducto", SqlDbType.Int, 25)
            sqlIdProducto.Direction = ParameterDirection.Input
            sqlIdProducto.Value = pIdProducto

            Dim sqlNumCert As SqlParameter = sqlCmdObtCert.Parameters.Add("@numcertificado", SqlDbType.[Decimal])
            sqlNumCert.Direction = ParameterDirection.Input
            sqlNumCert.Value = pNumCertificado

            filasAfectadas = sqlCmdObtCert.ExecuteNonQuery()
        End Using


        If filasAfectadas <= 0 Then
            Return -1
        Else
            Return filasAfectadas
        End If
        Return filasAfectadas
    End Function



    ''' <summary>
    ''' Permite calcular el inicio de vigencia de un certificado.
    ''' </summary>
    ''' <param name="pnIdProducto">Código de producto.</param>
    ''' <param name="pdFechaVenta">Fecha de venta del seguro</param>
    ''' <param name="pSqlCn">Objeto que permite la conexion a la BD.</param>
    ''' <returns>Objeto de tipo SqlDataReader conteniendo la fecha de vigencia.</returns>
    Public Function ObtenerIniVigencia(ByVal pnIdProducto As Int32, ByVal pdFechaVenta As DateTime, ByVal pSqlCn As SqlConnection) As SqlDataReader
        '---------------------------------------------------------------------------
        '             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-11-29
        '             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
        '            ---------------------------------------------------------------------------
        Dim sqlDr As SqlDataReader

        Using sqlCmd As New SqlCommand("[dbo].[BAIS_Certificado_ObtenerIniVigencia]", pSqlCn)
            sqlCmd.CommandType = CommandType.StoredProcedure

            Dim sqlParam1 As SqlParameter = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int)
            sqlParam1.Value = pnIdProducto

            Dim sqlParam2 As SqlParameter = sqlCmd.Parameters.Add("@fechaVenta", SqlDbType.SmallDateTime)
            sqlParam2.Value = pdFechaVenta

            sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow)
        End Using

        Return sqlDr
    End Function

    ''' <summary>
    ''' Permite calcular el fin de vigencia de un certificado.
    ''' </summary>
    ''' <param name="pnIdProducto">Código de producto.</param>
    ''' <param name="pdFechaIniVigencia">Fecha inicio de vigencia.</param>
    ''' <param name="pnPeriodo">periodo de duración del seguro.</param>
    ''' <param name="pSqlCn">Objeto que permite la conexion a la BD.</param>
    ''' <returns>Objeto de tipo SqlDataReader conteniendo la fecha de fin vigencia.</returns>
    Public Function ObtenerFinVigencia(ByVal pnIdProducto As Int32, ByVal pdFechaIniVigencia As DateTime, ByVal pnPeriodo As Int32, ByVal pSqlCn As SqlConnection) As SqlDataReader
        '---------------------------------------------------------------------------
        '             * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2011-11-29
        '             * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
        '            ---------------------------------------------------------------------------


        Dim sqlDr As SqlDataReader

        Using sqlCmd As New SqlCommand("[dbo].[BAIS_Certificado_ObtenerFinVigencia]", pSqlCn)
            sqlCmd.CommandType = CommandType.StoredProcedure

            Dim sqlParam1 As SqlParameter = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int)
            sqlParam1.Value = pnIdProducto

            Dim sqlParam2 As SqlParameter = sqlCmd.Parameters.Add("@fechaIniVigencia", SqlDbType.SmallDateTime)
            sqlParam2.Value = pdFechaIniVigencia

            Dim sqlParam3 As SqlParameter = sqlCmd.Parameters.Add("@periodo", SqlDbType.Int)
            sqlParam3.Value = pnPeriodo

            sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow)
        End Using

        Return sqlDr
    End Function

    Public Function ObtenerActivoxNroDocumento(ByVal pnIdProducto As Int32, ByVal pcIdTipoDocumento As [String], ByVal pcCcCliente As [String], ByVal pSqlCn As SqlConnection) As SqlDataReader
        Dim sqlDr As SqlDataReader

        Using sqlCmd As New SqlCommand("[dbo].[BAIS_Certificado_ObtenerActivoxNroDocumento]", pSqlCn)
            sqlCmd.CommandType = CommandType.StoredProcedure

            Dim sqlParam1 As SqlParameter = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int)
            sqlParam1.Value = pnIdProducto

            Dim sqlParam2 As SqlParameter = sqlCmd.Parameters.Add("@idTipoDocumento", SqlDbType.VarChar, 3)
            sqlParam2.Value = pcIdTipoDocumento

            Dim sqlParam3 As SqlParameter = sqlCmd.Parameters.Add("@ccCliente", SqlDbType.VarChar, 20)
            sqlParam3.Value = pcCcCliente

            sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleResult)
        End Using

        Return sqlDr
    End Function

    ''' <summary>
    ''' Permite calcular el fin de vigencia, segun el plan del producto.
    ''' </summary>
    ''' <param name="pnIdProducto">Código de producto.</param>
    ''' <param name="pdFechaInicioVig">Fecah inicio vigencia.</param>
    ''' <param name="pcOpcion">Plan del producto.</param>
    ''' <param name="pSqlCn">Objeto que permite la conexion a la BD.</param>
    ''' <returns>Objeto de tipo SqlDataReader.</returns>
    Public Function ObtenerFinVigenciaxPlan(ByVal pnIdProducto As Int32, ByVal pdFechaInicioVig As DateTime, ByVal pcOpcion As [String], ByVal pSqlCn As SqlConnection) As SqlDataReader
        '---------------------------------------------------------------------------
        '            * Usr Cre.: Gary Porras Paraguay                    Fecha Cre.: 2012-06-04
        '            * Usr Mod.: _______________________                 Fecha Mod.: ____-__-__
        '           ---------------------------------------------------------------------------


        Dim sqlDr As SqlDataReader

        Using sqlCmd As New SqlCommand("[dbo].[BAIS_Certificado_ObtenerFinVigenciaxPlan]", pSqlCn)
            sqlCmd.CommandType = CommandType.StoredProcedure

            Dim sqlParam1 As SqlParameter = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int)
            sqlParam1.Value = pnIdProducto

            Dim sqlParam2 As SqlParameter = sqlCmd.Parameters.Add("@fechaIniVigencia", SqlDbType.SmallDateTime)
            sqlParam2.Value = pdFechaInicioVig

            Dim sqlParam3 As SqlParameter = sqlCmd.Parameters.Add("@opcion", SqlDbType.VarChar, 10)
            sqlParam3.Value = pcOpcion

            sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow)
        End Using

        Return sqlDr
    End Function

    Public Function ObtenerFinVigenciaEtaria(ByVal pnIdProducto As Int32, ByVal pdFechaVenta As DateTime, ByVal pcOpcion As [String], ByVal pdFechaNacimiento As DateTime, ByVal pSqlCn As SqlConnection) As SqlDataReader
        Dim sqlDr As SqlDataReader

        Using sqlCmd As New SqlCommand("[dbo].[BAIS_Certificado_ObtenerFinVigenciaEtaria]", pSqlCn)
            sqlCmd.CommandType = CommandType.StoredProcedure

            Dim sqlParam1 As SqlParameter = sqlCmd.Parameters.Add("@idProducto", SqlDbType.Int)
            sqlParam1.Value = pnIdProducto

            Dim sqlParam2 As SqlParameter = sqlCmd.Parameters.Add("@fechaVenta", SqlDbType.SmallDateTime)
            sqlParam2.Value = pdFechaVenta

            Dim sqlParam3 As SqlParameter = sqlCmd.Parameters.Add("@Opcion", SqlDbType.VarChar, 10)
            sqlParam3.Value = pcOpcion

            Dim sqlParam4 As SqlParameter = sqlCmd.Parameters.Add("@fechaNacimiento", SqlDbType.SmallDateTime)
            sqlParam4.Value = pdFechaNacimiento

            sqlDr = sqlCmd.ExecuteReader(CommandBehavior.SingleRow)
        End Using

        Return sqlDr
    End Function

End Class
