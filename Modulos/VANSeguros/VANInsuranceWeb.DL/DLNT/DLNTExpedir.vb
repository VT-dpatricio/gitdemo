﻿Imports VAN.InsuranceWeb.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Public Class DLNTExpedir
    ''' <summary>
    ''' Método que valida la expedición de un certificado.
    ''' </summary>
    ''' <param name="pIdProducto">Codigo del producto.</param>
    ''' <param name="pNumCertFisico">Numero de certificado fisico.</param>
    ''' <param name="pCcCliente">Documento del cliente.</param>
    ''' <param name="pNumCuenta">Numero de Cuenta del Cliente.</param>
    ''' <param name="pSqlCon">Objeto SqlConnection</param>
    ''' <returns>
    ''' Retorna un enterno: El codigo de validación, caso contrario devuelve -1
    ''' </returns>
    Public Function DAValidaExpedirCertificado(ByVal pIdProducto As Int32, ByVal pNumCertFisico As [String], ByVal pIDTipoDocumento As [String], ByVal pCcCliente As [String], ByVal pIDMedioPago As [String], ByVal pNumCuenta As [String], ByVal pSqlCon As SqlConnection) As SqlDataReader
        Dim sqlDr As SqlDataReader

        Using sqlCmdExpValCert As New SqlCommand("BW_ValidaExpedirCertificado", pSqlCon)
            sqlCmdExpValCert.CommandType = CommandType.StoredProcedure

            Dim sqlIdProducto As SqlParameter = sqlCmdExpValCert.Parameters.Add("@idProducto", SqlDbType.Int)
            sqlIdProducto.Direction = ParameterDirection.Input
            sqlIdProducto.Value = pIdProducto

            Dim sqlNumFisico As SqlParameter = sqlCmdExpValCert.Parameters.Add("@numCertFisico", SqlDbType.VarChar, 20)
            sqlNumFisico.Direction = ParameterDirection.Input
            sqlNumFisico.Value = pNumCertFisico

            Dim sqlIDTipoDocumento As SqlParameter = sqlCmdExpValCert.Parameters.Add("@idtipodocumento", SqlDbType.VarChar, 3)
            sqlIDTipoDocumento.Direction = ParameterDirection.Input
            sqlIDTipoDocumento.Value = pIDTipoDocumento

            Dim sqlCcCliente As SqlParameter = sqlCmdExpValCert.Parameters.Add("@cccliente", SqlDbType.VarChar, 20)
            sqlCcCliente.Direction = ParameterDirection.Input
            sqlCcCliente.Value = pCcCliente

            Dim sqlIDMedioPago As SqlParameter = sqlCmdExpValCert.Parameters.Add("@idmediopago", SqlDbType.VarChar, 2)
            sqlIDMedioPago.Direction = ParameterDirection.Input
            sqlIDMedioPago.Value = pIDMedioPago

            Dim sqlNumCuenta As SqlParameter = sqlCmdExpValCert.Parameters.Add("@numcuenta", SqlDbType.VarChar, 20)
            sqlNumCuenta.Direction = ParameterDirection.Input
            sqlNumCuenta.Value = pNumCuenta

            Dim sqlReturnValue As SqlParameter = sqlCmdExpValCert.Parameters.Add("@ReturnValue", SqlDbType.Int)
            sqlReturnValue.Direction = ParameterDirection.ReturnValue

            sqlDr = sqlCmdExpValCert.ExecuteReader(CommandBehavior.SingleResult)

            Return sqlDr
        End Using
    End Function

    ''' <summary>
    ''' Método que valida la expedición de un asegurado.
    ''' </summary>
    ''' <param name="pIdProducto">Codigo de producto.</param>
    ''' <param name="pCcAseg">Documento del asegurado.</param>
    ''' <param name="pSqlCon">Objeto SqlConnection</param>
    ''' <returns>
    ''' Retorna un entero: El codigo de validación, caso contrario devuelve -1
    ''' </returns>
    Public Function DAValidaExpedirAsegurado(ByVal pIdProducto As Int32, ByVal pCcAseg As [String], ByVal pSqlCon As SqlConnection) As SqlDataReader
        Dim sqlDr As SqlDataReader

        Using sqlCmdExpValAseg As New SqlCommand("BW_ValidaExpedirAsegurado", pSqlCon)
            sqlCmdExpValAseg.CommandType = CommandType.StoredProcedure

            Dim sqlCcAseg As SqlParameter = sqlCmdExpValAseg.Parameters.Add("@ccAseg", SqlDbType.VarChar)
            sqlCcAseg.Direction = ParameterDirection.Input
            sqlCcAseg.Value = pCcAseg

            Dim sqlIdProducto As SqlParameter = sqlCmdExpValAseg.Parameters.Add("@idproducto", SqlDbType.Int)
            sqlIdProducto.Direction = ParameterDirection.Input
            sqlIdProducto.Value = pIdProducto

            Dim sqlReturnValue As SqlParameter = sqlCmdExpValAseg.Parameters.Add("@ReturnValue", SqlDbType.Int)
            sqlReturnValue.Direction = ParameterDirection.ReturnValue

            sqlDr = sqlCmdExpValAseg.ExecuteReader(CommandBehavior.SingleResult)
        End Using
        Return sqlDr
    End Function
End Class
