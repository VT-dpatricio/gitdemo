﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports Microsoft.Practices.EnterpriseLibrary.Data

Public Class CertificadoDetalleInfoProductoDA

    Dim _db As Database = Nothing

    Public Sub New()
        _db = Conexion.Instancia.Database()
    End Sub

    Function Agregar(ByVal Objeto As CertificadoDetalleInfoProductoBE, Optional ByVal Transac As DbTransaction = Nothing) As String
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_CertificadoDetalleInfoProducto_Agregar", Objeto.IdProducto, Objeto.IdInfoProducto, Objeto.IdDetalleInfoProducto, Objeto.IdCertificado, Objeto.Valor, Objeto.Estado, Objeto.UsuarioReg, Objeto.FechaReg, Objeto.UsuarioMod, Objeto.FechaMod)
            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Modificar(ByVal Objeto As CertificadoDetalleInfoProductoBE, Optional ByVal Transac As DbTransaction = Nothing) As Integer
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_CertificadoDetalleInfoProducto_Modificar", Objeto.IdCertificadoDetalleInfoProducto, Objeto.IdProducto, Objeto.IdInfoProducto, Objeto.IdDetalleInfoProducto, Objeto.IdCertificado, Objeto.Valor, Objeto.Estado, Objeto.UsuarioMod, Objeto.FechaMod)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Eliminar(ByVal Objeto As CertificadoDetalleInfoProductoBE, Optional ByVal Transac As DbTransaction = Nothing) As Integer
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_CertificadoDetalleInfoProducto_Eliminar", Objeto.IdCertificadoDetalleInfoProducto, Objeto.Estado, Objeto.UsuarioMod, Objeto.FechaMod)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of CertificadoBE)
        Dim Lista As New List(Of CertificadoBE)

        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_CertificadoDetalleInfoProducto_Todos")
                While dr.Read()
                    Lista.Add(Populate.Certificado_Lista(dr))
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_CertificadoDetalleInfoProducto_Todos")
                While dr.Read()
                    Lista.Add(Populate.Certificado_Lista(dr))
                End While
            End Using
        End If
        Return Lista
    End Function

    Function Listar_Id(ByVal Objeto As CertificadoDetalleInfoProductoBE, Optional ByVal Transaction As DbTransaction = Nothing) As CertificadoDetalleInfoProductoBE
        Dim Entidad As New CertificadoDetalleInfoProductoBE

        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_CertificadoDetalleInfoProducto_Id", Objeto.IdCertificadoDetalleInfoProducto)
                While dr.Read()
                    Entidad = Populate.CertificadoDetalleInfoProducto_Lista(dr)
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_CertificadoDetalleInfoProducto_Id", Objeto.IdCertificadoDetalleInfoProducto)
                While dr.Read()
                    Entidad = Populate.CertificadoDetalleInfoProducto_Lista(dr)
                End While
            End Using
        End If
        Return Entidad
    End Function
End Class
