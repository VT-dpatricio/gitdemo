﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports Microsoft.Practices.EnterpriseLibrary.Data

Public Class ProductoDA

    Dim _db As Database = Nothing
    Dim _dbBAIS As Database = Nothing

    Public Sub New()
        _db = Conexion.Instancia.Database()
        _dbBAIS = ConexionBAIS.Instancia.Database()
    End Sub

    Function AgregarT(ByVal Objeto As ProductoBE) As String
        Dim Resultado As String = ""
        Try
            Using Connection As DbConnection = _db.CreateConnection()
                Connection.Open()
                Dim Transaction As DbTransaction = Connection.BeginTransaction
                Try


                    Dim IdProductoRes = Agregar(Objeto, Transaction)
                    Resultado = IdProductoRes

                    Dim _ProductoEntidadDA As New ProductoEntidadDA
                    Dim _ProductoInfoProductoDA As New ProductoInfoProductoDA

                    For i As Integer = 0 To Objeto.ListEntidadBE.Count - 1
                        Dim _ProductoEntidadBE As New ProductoEntidadBE
                        _ProductoEntidadBE.IdProducto = IdProductoRes
                        _ProductoEntidadBE.IdEntidad = Objeto.ListEntidadBE.Item(i).IdEntidad
                        _ProductoEntidadBE.Estado = "A"
                        _ProductoEntidadBE.UsuarioReg = Objeto.UsuarioReg
                        _ProductoEntidadBE.UsuarioMod = Objeto.UsuarioMod
                        _ProductoEntidadBE.FechaReg = Objeto.FechaReg
                        _ProductoEntidadBE.FechaMod = Objeto.FechaMod
                        _ProductoEntidadDA.Agregar(_ProductoEntidadBE, Transaction)
                    Next

                    For i As Integer = 0 To Objeto.ListInfoProducto.Count - 1
                        Dim _ProductoInfoProductoBE As New ProductoInfoProductoBE
                        _ProductoInfoProductoBE.IdProducto = IdProductoRes
                        _ProductoInfoProductoBE.IdInfoProducto = Objeto.ListInfoProducto.Item(i).IdInfoProducto
                        _ProductoInfoProductoBE.Estado = "A"
                        _ProductoInfoProductoBE.UsuarioReg = Objeto.UsuarioReg
                        _ProductoInfoProductoBE.UsuarioMod = Objeto.UsuarioMod
                        _ProductoInfoProductoBE.FechaReg = Objeto.FechaReg
                        _ProductoInfoProductoBE.FechaMod = Objeto.FechaMod
                        _ProductoInfoProductoDA.Agregar(_ProductoInfoProductoBE, Transaction)
                    Next

                    Transaction.Commit()
                Catch ex As Exception
                    Transaction.Rollback()
                    Throw ex
                End Try
            End Using
        Catch ex As Exception
            Return ex.Message
        End Try
        Return Resultado
    End Function

    Function AgregarBAIS(ByVal Producto As ProductoBE, ByVal ListParentescoPermitidoBE As List(Of ParentescoPermitidoBE), ByVal ListTipoDocProductoBE As List(Of TipoDocProductoBE), ByVal ListInfoProductoBE As List(Of InfoProductoBE), ByVal ListInfoAseguradoBE As List(Of InfoAseguradoBE), ByVal ListOpcionBE As List(Of OpcionBE), ByVal ListOpcionPrimaBE As List(Of OpcionPrimaBE), ByVal ListProductoMedioPagoBE As List(Of ProductoMedioPagoBE)) As String
        Dim Resultado As String = "OK"
        Dim CodigoProducto As Integer = Producto.idProducto
        Try
            Using Connection As DbConnection = _dbBAIS.CreateConnection()
                Connection.Open()
                Dim Transaction As DbTransaction = Connection.BeginTransaction
                Try
                    Dim Command As DbCommand

                    Command = _dbBAIS.GetStoredProcCommand("BW_BAIS_Producto_Agregar", Producto.idProducto, Producto.idAsegurador, Producto.nombre, Producto.idTipoProducto, Producto.activo, Producto.maxAsegurados, Producto.nacimientoRequerido, Producto.validarParentesco, Producto.obligaCuentaHabiente, Producto.cumuloMaximo, Producto.edadMaxPermanencia, Producto.diasAnulacion, Producto.generaCobro, Producto.idEntidad, Producto.vigenciaCobro, Producto.maxPolizasAsegurado, Producto.maxPolizasCuentahabiente, Producto.maxPolizasRegalo, Producto.idReglaCobro, Producto.maxCuentasTitular, Producto.maxPolizasAseguradoSinDNI, Producto.validaDireccionCer, Producto.validaDireccionAse, Producto.cuentaHabienteigualAsegurado)
                    Resultado = _db.ExecuteNonQuery(Command, Transaction)


                    For Each Entity As ParentescoPermitidoBE In ListParentescoPermitidoBE

                        Command = _dbBAIS.GetStoredProcCommand("BW_BAIS_ParentescoPermitido_Agregar", Entity.idParentesco, CodigoProducto, Entity.maxMontoAsegurado, Entity.edadMin, Entity.edadMax)
                        Resultado = _db.ExecuteNonQuery(Command, Transaction)

                    Next


                    For Each Entity As ProductoMedioPagoBE In ListProductoMedioPagoBE

                        Command = _dbBAIS.GetStoredProcCommand("BW_BAIS_ProductoMedioPago_Agregar", CodigoProducto, Entity.idMedioPago, Entity.IdMonedaCobro, Entity.codigoComercio, Entity.cuentaRecaudadora, Entity.codigoArchivo, Entity.codigoInterno, Entity.monedaCuenta, Entity.servicio, Entity.origensolicitud)
                        Resultado = _db.ExecuteNonQuery(Command, Transaction)

                    Next


                    For Each Entity As TipoDocProductoBE In ListTipoDocProductoBE

                        Command = _dbBAIS.GetStoredProcCommand("BW_BAIS_TipoDocProducto_Agregar", CodigoProducto, Entity.idTipoDocumento)
                        Resultado = _db.ExecuteNonQuery(Command, Transaction)

                    Next

                    For Each Entity As OpcionBE In ListOpcionBE

                        Command = _dbBAIS.GetStoredProcCommand("BW_BAIS_Opcion_Agregar", CodigoProducto, Entity.opcion, Entity.activo, Entity.puntos, Entity.puntosValor)
                        Resultado = _db.ExecuteNonQuery(Command, Transaction)

                    Next


                    For Each Entity As OpcionPrimaBE In ListOpcionPrimaBE

                        Command = _dbBAIS.GetStoredProcCommand("BW_BAIS_OpcionPrima_Agregar", CodigoProducto, Entity.opcion, Entity.IdFrecuencia, Entity.prima, Entity.montoAsegurado, Entity.idMonedaPrima, IIf(Entity.edadMin = -1, Nothing, Entity.edadMin), IIf(Entity.edadMax = -1, Nothing, Entity.edadMax), Nothing, Nothing, Entity.activo)
                        Resultado = _db.ExecuteNonQuery(Command, Transaction)

                    Next


                    For Each Entity As InfoProductoBE In ListInfoProductoBE

                        Command = _dbBAIS.GetStoredProcCommand("BW_BAIS_InfoProducto_Agregar", CodigoProducto, Entity.nombre, Entity.Descripcion, Entity.idTipoDato, Entity.obligatorio, Entity.minimo, Entity.maximo, Entity.numCaracteres, Entity.regex, Entity.formato)
                        Resultado = _db.ExecuteNonQuery(Command, Transaction)

                    Next


                    For Each Entity As InfoAseguradoBE In ListInfoAseguradoBE

                        Command = _dbBAIS.GetStoredProcCommand("BW_BAIS_InfoAsegurado_Agregar", CodigoProducto, Entity.nombre, Entity.descripcion, Entity.idTipoDato, Entity.obligatorio, Entity.minimo, Entity.maximo, Entity.numCaracteres, Entity.regex, Entity.formato)
                        Resultado = _db.ExecuteNonQuery(Command, Transaction)

                    Next

                    Command = _dbBAIS.GetStoredProcCommand("BW_BAIS_PapDisponible_Agregar", Producto.idProducto, Producto.Minimo, Producto.Maximo)
                    Resultado = _db.ExecuteNonQuery(Command, Transaction)


                    Transaction.Commit()
                Catch ex As Exception
                    Transaction.Rollback()
                    Resultado = ex.Message
                    Throw ex
                End Try
            End Using
        Catch ex As Exception
            Resultado = ex.Message
            Return ex.Message
        End Try
        Return Resultado
    End Function

    Function Agregar(ByVal Objeto As ProductoBE, Optional ByVal Transac As DbTransaction = Nothing) As String
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_Producto_Agregar") ', Nothing, Objeto.Descripcion, Objeto.Abrev, Objeto.Logo, Objeto.Banner, Objeto.Reporte, Objeto.Estado, Objeto.UsuarioReg, Objeto.FechaReg, Objeto.UsuarioMod, Objeto.FechaMod)

            _db.AddOutParameter(Command, "@IdProducto", DbType.Int32, 0)
            _db.AddInParameter(Command, "Descripcion", DbType.String, Objeto.Descripcion)
            _db.AddInParameter(Command, "Abrev", DbType.String, Objeto.Abrev)
            _db.AddInParameter(Command, "Logo", DbType.String, Objeto.Logo)
            _db.AddInParameter(Command, "Banner", DbType.String, Objeto.Banner)
            _db.AddInParameter(Command, "Reporte", DbType.String, Objeto.Reporte)
            _db.AddInParameter(Command, "Estado", DbType.String, Objeto.Estado)
            _db.AddInParameter(Command, "UsuarioReg", DbType.String, Objeto.UsuarioReg)
            _db.AddInParameter(Command, "FechaReg", DbType.DateTime, Objeto.FechaReg)
            _db.AddInParameter(Command, "UsuarioMod", DbType.String, Objeto.UsuarioMod)
            _db.AddInParameter(Command, "FechaMod", DbType.DateTime, Objeto.FechaMod)


            _db.AddInParameter(Command, "vEdad", DbType.Boolean, Objeto.vEdad)
            _db.AddInParameter(Command, "vMonto", DbType.Boolean, Objeto.vMonto)




            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        resultado = Convert.ToInt32(Command.Parameters("@IdProducto").Value)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
                resultado = Convert.ToInt32(Command.Parameters("@IdProducto").Value)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function ModificarT(ByVal Objeto As ProductoBE)
        Dim Resultado As String = ""
        Try
            Using Connection As DbConnection = _db.CreateConnection()
                Connection.Open()
                Dim Transaction As DbTransaction = Connection.BeginTransaction
                Try


                    Modificar(Objeto, Transaction)
                    Resultado = Objeto.IdProducto


                    Dim _ProductoEntidadDA As New ProductoEntidadDA
                    Dim _ProductoInfoProductoDA As New ProductoInfoProductoDA

                    Dim _ProductoEntidadBE As New ProductoEntidadBE
                    _ProductoEntidadBE.IdProducto = Objeto.IdProducto

                    _ProductoEntidadDA.Eliminar_IdProducto(_ProductoEntidadBE, Transaction)
                    For i As Integer = 0 To Objeto.ListEntidadBE.Count - 1
                        _ProductoEntidadBE = New ProductoEntidadBE
                        _ProductoEntidadBE.IdProducto = Objeto.IdProducto
                        _ProductoEntidadBE.IdEntidad = Objeto.ListEntidadBE.Item(i).IdEntidad
                        _ProductoEntidadBE.Estado = "A"
                        _ProductoEntidadBE.UsuarioReg = Objeto.UsuarioMod
                        _ProductoEntidadBE.UsuarioMod = Objeto.UsuarioMod
                        _ProductoEntidadBE.FechaReg = Objeto.FechaMod
                        _ProductoEntidadBE.FechaMod = Objeto.FechaMod
                        _ProductoEntidadDA.Agregar(_ProductoEntidadBE, Transaction)
                    Next


                    Dim _ProductoInfoProductoBE As New ProductoInfoProductoBE
                    _ProductoInfoProductoBE.IdProducto = Objeto.IdProducto

                    _ProductoInfoProductoDA.Eliminar_IdProducto(_ProductoInfoProductoBE, Transaction)
                    For i As Integer = 0 To Objeto.ListInfoProducto.Count - 1
                        _ProductoInfoProductoBE = New ProductoInfoProductoBE
                        _ProductoInfoProductoBE.IdProducto = Objeto.IdProducto
                        _ProductoInfoProductoBE.IdInfoProducto = Objeto.ListInfoProducto.Item(i).IdInfoProducto
                        _ProductoInfoProductoBE.Estado = "A"
                        _ProductoInfoProductoBE.UsuarioReg = Objeto.UsuarioMod
                        _ProductoInfoProductoBE.UsuarioMod = Objeto.UsuarioMod
                        _ProductoInfoProductoBE.FechaReg = Objeto.FechaMod
                        _ProductoInfoProductoBE.FechaMod = Objeto.FechaMod
                        _ProductoInfoProductoDA.Agregar(_ProductoInfoProductoBE, Transaction)
                    Next

                    Transaction.Commit()
                Catch ex As Exception
                    Transaction.Rollback()
                    Resultado = ex.Message
                    Throw ex
                End Try
            End Using
        Catch ex As Exception
            Resultado = ex.Message
        End Try
        Return Resultado
    End Function

    Function Modificar(ByVal Objeto As ProductoBE, Optional ByVal Transac As DbTransaction = Nothing) As Integer
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_Producto_Modificar", Objeto.IdProducto, Objeto.Descripcion, Objeto.Abrev, Objeto.Logo, Objeto.Banner, Objeto.Reporte, Objeto.Estado, Objeto.UsuarioMod, Objeto.FechaMod, Objeto.vEdad, Objeto.vMonto)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function ModificarDiseno(ByVal Objeto As ProductoBE, Optional ByVal Transac As DbTransaction = Nothing) As Integer
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_Producto_Modificar", Objeto.IdProducto, Objeto.Logo, Objeto.Banner, Objeto.Reporte, Objeto.Estado, Objeto.UsuarioMod, Objeto.FechaMod)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Eliminar(ByVal Objeto As ProductoBE, Optional ByVal Transac As DbTransaction = Nothing) As Integer
        Dim resultado As String = "0"
        Dim _ProductoEntidadDA As New ProductoEntidadDA
        Dim _ProductoEntidadBE As New ProductoEntidadBE
        _ProductoEntidadBE.IdProducto = Objeto.IdProducto

        Dim _ProductoInfoProductoDA As New ProductoInfoProductoDA
        Dim _ProductoInfoProductoBE As New ProductoInfoProductoBE
        _ProductoInfoProductoBE.IdProducto = Objeto.IdProducto


        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_Producto_Eliminar", Objeto.IdProducto, Objeto.Estado, Objeto.UsuarioMod, Objeto.FechaMod)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try

                        resultado = _ProductoEntidadDA.Eliminar_IdProducto(_ProductoEntidadBE, Transaction)
                        resultado = _ProductoInfoProductoDA.Eliminar_IdProducto(_ProductoInfoProductoBE, Transaction)

                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of ProductoBE)
        Dim Lista As New List(Of ProductoBE)

        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_Producto_Todos")
                While dr.Read()
                    Lista.Add(Populate.Producto_Lista(dr))
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_Producto_Todos")
                While dr.Read()
                    Lista.Add(Populate.Producto_Lista(dr))
                End While
            End Using
        End If
        Return Lista
    End Function

    Function Listar_TodosBAIS(ByVal IdAsegurador As Integer, ByVal IdEntidad As Integer) As List(Of ProductoBE)
        Dim Lista As New List(Of ProductoBE)
        Dim Entity As New ProductoBE

        Dim cont As Integer = 0
        
        Using dr As IDataReader = _db.ExecuteReader("usp_Producto_TodosBAIS", IdAsegurador, IdEntidad)
            While dr.Read()
                Entity = Populate.Producto_ListaBDJanusBAIS(dr)
                Entity.i = cont
                Lista.Add(Entity)
                cont += 1
            End While
        End Using

        Return Lista
    End Function

    Function Listar_Id(ByVal Objeto As ProductoBE, Optional ByVal Transaction As DbTransaction = Nothing) As ProductoBE
        Dim _ProductoBE As New ProductoBE

        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_Producto_Id", Objeto.IdProducto)
                While dr.Read()
                    _ProductoBE = Populate.Producto_Lista(dr)
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_Producto_Id", Objeto.IdProducto)
                While dr.Read()
                    _ProductoBE = Populate.Producto_Lista(dr)
                End While
            End Using
        End If
        Return _ProductoBE
    End Function

    Function Listar_BDJanus(ByVal Nombre As String, Optional ByVal Transaction As DbTransaction = Nothing) As List(Of ProductoBE)
        Dim Lista As New List(Of ProductoBE)

        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_BDJanus_Producto_Listar", Nombre)
                While dr.Read()
                    Lista.Add(Populate.Producto_ListaBDJanus(dr))
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_BDJanus_Producto_Listar", Nombre)
                While dr.Read()
                    Lista.Add(Populate.Producto_ListaBDJanus(dr))
                End While
            End Using
        End If
        Return Lista
    End Function


    Function Listar_BAIS_Informador(ByVal IdProducto As Integer) As List(Of GenericoBAIS)
        Dim Lista As New List(Of GenericoBAIS)
        Using dr As IDataReader = _dbBAIS.ExecuteReader("BW_BAIS_Vendedores_Listar", IdProducto)
            While dr.Read()
                Dim entity As New GenericoBAIS
                entity.Id = dr("id")
                entity.Descripcion = dr("nombre")
                Lista.Add(entity)
            End While
        End Using
        Return Lista
    End Function

    Function Listar_BAIS_Oficinas(ByVal IdProducto As Integer) As List(Of GenericoBAIS)
        Dim Lista As New List(Of GenericoBAIS)
        Using dr As IDataReader = _dbBAIS.ExecuteReader("BW_BAIS_Oficinas_Listar", IdProducto)
            While dr.Read()
                Dim entity As New GenericoBAIS
                entity.Id = dr("id")
                entity.Descripcion = dr("nombre")
                Lista.Add(entity)
            End While
        End Using
        Return Lista
    End Function

    Function Listar_BAIS_TipoDocumento(ByVal IdProducto As Integer) As List(Of GenericoBAIS)
        Dim Lista As New List(Of GenericoBAIS)
        Using dr As IDataReader = _dbBAIS.ExecuteReader("BW_BAIS_TipoDocumento_Listar", IdProducto)
            While dr.Read()
                Dim entity As New GenericoBAIS
                entity.Id = dr("id")
                entity.Descripcion = dr("nombre")
                Lista.Add(entity)
            End While
        End Using
        Return Lista
    End Function

    Function Listar_BAIS_Plan(ByVal IdProducto As Integer) As List(Of GenericoBAIS)
        Dim Lista As New List(Of GenericoBAIS)
        Using dr As IDataReader = _dbBAIS.ExecuteReader("BW_BAIS_Opcion_Listar", IdProducto)
            While dr.Read()
                Dim entity As New GenericoBAIS
                entity.Id = dr("opcion")
                entity.Descripcion = dr("opcion")
                Lista.Add(entity)
            End While
        End Using
        Return Lista
    End Function

    Function Listar_BAIS_MonedaPrima(ByVal IdProducto As Integer, ByVal Opcion As String) As List(Of GenericoBAIS)
        Dim Lista As New List(Of GenericoBAIS)
        Using dr As IDataReader = _dbBAIS.ExecuteReader("BW_BAIS_OpcionPrima_Listar", IdProducto, Opcion)
            While dr.Read()
                Dim entity As New GenericoBAIS
                entity.Id = dr("idMonedaPrima")
                entity.Descripcion = dr("idMonedaPrima")
                Lista.Add(entity)
            End While
        End Using
        Return Lista
    End Function

    Function Listar_BAIS_MedioPago(ByVal IdProducto As Integer) As List(Of GenericoBAIS)
        Dim Lista As New List(Of GenericoBAIS)
        Using dr As IDataReader = _dbBAIS.ExecuteReader("BW_BAIS_MedioPago_Producto_Listar", IdProducto)
            While dr.Read()
                Dim entity As New GenericoBAIS
                entity.Id = dr("idMedioPago")
                entity.Descripcion = dr("nombre")
                Lista.Add(entity)
            End While
        End Using
        Return Lista
    End Function

    Function Listar_BAIS_MonedaCTA(ByVal IdProducto As Integer, ByVal Moneda As String) As List(Of GenericoBAIS)
        Dim Lista As New List(Of GenericoBAIS)
        Using dr As IDataReader = _dbBAIS.ExecuteReader("BW_BAIS_MedioPagoMonedaProducto_Listar", IdProducto, Moneda)
            While dr.Read()
                Dim entity As New GenericoBAIS
                entity.Id = dr("IdMonedaCobro")
                entity.Descripcion = dr("IdMonedaCobro")
                Lista.Add(entity)
            End While
        End Using
        Return Lista
    End Function


    Function Listar_BAIS_Frecuencia(ByVal IdProducto As Integer) As List(Of GenericoBAIS)
        Dim Lista As New List(Of GenericoBAIS)
        Using dr As IDataReader = _dbBAIS.ExecuteReader("BW_BAIS_Frecuencia_Listar", IdProducto)
            While dr.Read()
                Dim entity As New GenericoBAIS
                entity.Id = dr("IdFrecuencia")
                entity.Descripcion = dr("Nombre")
                Lista.Add(entity)
            End While
        End Using
        Return Lista
    End Function




End Class
