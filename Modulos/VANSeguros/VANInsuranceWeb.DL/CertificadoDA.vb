﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports Microsoft.Practices.EnterpriseLibrary.Data

Public Class CertificadoDA

    Dim _db As Database = Nothing
    Dim _dbBAIS As Database = Nothing

    Public Sub New()
        _db = Conexion.Instancia.Database()
        _dbBAIS = ConexionBAIS.Instancia.Database()
    End Sub

    Function Agregar(ByVal Objeto As CertificadoBE, Optional ByVal Transac As DbTransaction = Nothing) As String
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_Certificado_Agregar")

            _db.AddOutParameter(Command, "IdCertificado", DbType.Int32, 0)
            _db.AddInParameter(Command, "IdProducto", DbType.Int32, Objeto.IdProducto)
            _db.AddInParameter(Command, "IdAseguradora", DbType.Int32, Objeto.IdAseguradora)
            _db.AddInParameter(Command, "NumeroCertificado", DbType.String, Objeto.NumeroCertificado)
            _db.AddInParameter(Command, "FechaEmision", DbType.DateTime, Objeto.FechaEmision)
            _db.AddInParameter(Command, "FechaInicio", DbType.DateTime, Objeto.FechaInicio)
            _db.AddInParameter(Command, "FechaFin", DbType.DateTime, Objeto.FechaFin)
            _db.AddInParameter(Command, "PrimaNetaMensual", DbType.Decimal, Objeto.PrimaNetaMensual)
            _db.AddInParameter(Command, "PrimaBrutaMensual", DbType.Decimal, Objeto.PrimaBrutaMensual)
            _db.AddInParameter(Command, "PrimaNetaAnual", DbType.Decimal, Objeto.PrimaNetaAnual)
            _db.AddInParameter(Command, "PrimanBrutaAnual", DbType.Decimal, Objeto.PrimanBrutaAnual)
            _db.AddInParameter(Command, "Poliza", DbType.String, Objeto.Poliza)
            _db.AddInParameter(Command, "UsuarioMod", DbType.String, Objeto.Obs)
            _db.AddInParameter(Command, "UsuarioReg", DbType.String, Objeto.UsuarioReg)
            _db.AddInParameter(Command, "FechaReg", DbType.DateTime, Objeto.FechaReg)
            _db.AddInParameter(Command, "UsuarioMod", DbType.String, Objeto.UsuarioMod)
            _db.AddInParameter(Command, "FechaMod", DbType.DateTime, Objeto.FechaMod)




            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        resultado = Convert.ToInt32(Command.Parameters("@IdCertificado").Value)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
                resultado = Convert.ToInt32(Command.Parameters("@IdCertificado").Value)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Modificar(ByVal Objeto As CertificadoBE, Optional ByVal Transac As DbTransaction = Nothing) As Integer
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_Certificado_Modificar", Objeto.IdCertificado, Objeto.IdProducto, Objeto.IdAseguradora, Objeto.NumeroCertificado, Objeto.FechaEmision, Objeto.FechaInicio, Objeto.FechaFin, Objeto.PrimaNetaMensual, Objeto.PrimaBrutaMensual, Objeto.PrimaNetaAnual, Objeto.PrimanBrutaAnual, Objeto.Poliza, Objeto.Obs, Objeto.Estado, Objeto.UsuarioMod, Objeto.FechaMod)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Eliminar(ByVal Objeto As CertificadoBE, Optional ByVal Transac As DbTransaction = Nothing) As Integer
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_Certificado_Eliminar", Objeto.IdCertificado, Objeto.Estado, Objeto.UsuarioMod, Objeto.FechaMod)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of CertificadoBE)
        Dim Lista As New List(Of CertificadoBE)

        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_Certificado_Todos")
                While dr.Read()
                    Lista.Add(Populate.Certificado_Lista(dr))
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_Certificado_Todos")
                While dr.Read()
                    Lista.Add(Populate.Certificado_Lista(dr))
                End While
            End Using
        End If
        Return Lista
    End Function

    Function ConsultarPrimaCertificado(ByVal IdProducto As Integer, ByVal Opcion As String, ByVal IdFrecuancia As Integer, ByVal IdMoneda As String, ByVal Edad As Integer) As Decimal

        Dim Valor As Decimal = 0
        Try
            Using dr As IDataReader = _dbBAIS.ExecuteReader("BW_BAIS_ConsultarPrima", IdProducto, Opcion, IdFrecuancia, IdMoneda, Edad)
                While dr.Read()

                    If Not dr("Prima") Is DBNull.Value Then
                        Valor = CType(dr("Prima"), Decimal)
                    End If

                End While
            End Using
        Catch ex As Exception

        End Try


        Return Valor

    End Function

    Function Listar_Id(ByVal Objeto As CertificadoBE, Optional ByVal Transaction As DbTransaction = Nothing) As CertificadoBE
        Dim _CertificadoBE As New CertificadoBE

        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_Certificado_Id", Objeto.IdCertificado)
                While dr.Read()
                    _CertificadoBE = Populate.Certificado_Lista(dr)
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_Certificado_Id", Objeto.IdCertificado)
                While dr.Read()
                    _CertificadoBE = Populate.Certificado_Lista(dr)
                End While
            End Using
        End If
        Return _CertificadoBE
    End Function


End Class
