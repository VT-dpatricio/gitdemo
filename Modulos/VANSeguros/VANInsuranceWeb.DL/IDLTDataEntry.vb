﻿Imports VAN.InsuranceWeb.BE
Imports System
Imports System.Data.SqlClient

Public Interface IDLTDataEntry
    Function Actualizar(ByVal pEntidad As BEBase) As Boolean
    Function Eliminar(ByVal pCodigo As Integer) As Boolean
    Function Insertar(ByVal pEntidad As BEBase) As Boolean
    Function LlenarEstructura(ByVal pEntidad As BEBase, ByVal pcm As SqlCommand, ByVal TipoTransaccion As String) As SqlCommand
End Interface


