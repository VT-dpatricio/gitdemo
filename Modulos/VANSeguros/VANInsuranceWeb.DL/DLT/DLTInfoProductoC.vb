﻿Imports VAN.InsuranceWeb.BE
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class DLTInfoProductoC
    ''' <summary>
    ''' Método que inserta los infoproductos.
    ''' </summary>
    ''' <param name="pObjBEInfoProductoC">Objeto BEInfoproducto</param>
    ''' <param name="pSqlCon">Objeto SqlConnection</param>
    ''' <param name="pSqlTranx">Objeto SqlTransaction</param>
    ''' <returns></returns>
    Public Function DAInsertaInfoProductoC(ByVal pObjBEInfoProductoC As BEInfoProductoC, ByVal pSqlCon As SqlConnection, ByVal pSqlTranx As SqlTransaction) As Int32
        Dim filasAfectadas As Int32 = -1

        Using sqlCmdInsInfProdC As New SqlCommand("BW_InsertarInfoProductoC", pSqlCon)
            sqlCmdInsInfProdC.Transaction = pSqlTranx
            sqlCmdInsInfProdC.CommandType = CommandType.StoredProcedure

            Dim sqlIdCertificado As SqlParameter = sqlCmdInsInfProdC.Parameters.Add("@idCertificado", SqlDbType.VarChar, 25)
            sqlIdCertificado.Direction = ParameterDirection.Input
            sqlIdCertificado.Value = pObjBEInfoProductoC.IdCertificado

            Dim sqlValorNum As SqlParameter = sqlCmdInsInfProdC.Parameters.Add("@valorNum", SqlDbType.[Decimal])
            sqlValorNum.Direction = ParameterDirection.Input
            If pObjBEInfoProductoC.ValorNum = -1 Then
                sqlValorNum.Value = DBNull.Value
            Else
                sqlValorNum.Value = pObjBEInfoProductoC.ValorNum
            End If

            Dim sqlValorDate As SqlParameter = sqlCmdInsInfProdC.Parameters.Add("@valorDate", SqlDbType.DateTime)
            sqlValorDate.Direction = ParameterDirection.Input
            If pObjBEInfoProductoC.ValorDate = DateTime.Parse("1900-01-01") Then
                sqlValorDate.Value = DBNull.Value
            Else
                sqlValorDate.Value = pObjBEInfoProductoC.ValorDate
            End If

            Dim sqlValorString As SqlParameter = sqlCmdInsInfProdC.Parameters.Add("@valorString", SqlDbType.VarChar, 8000)
            sqlValorString.Direction = ParameterDirection.Input
            If pObjBEInfoProductoC.ValorString = [String].Empty Then
                sqlValorString.Value = DBNull.Value
            Else
                sqlValorString.Value = pObjBEInfoProductoC.ValorString
            End If

            Dim sqlIdInfoProductoC As SqlParameter = sqlCmdInsInfProdC.Parameters.Add("@idInfoproducto", SqlDbType.Int)
            sqlIdInfoProductoC.Direction = ParameterDirection.Input
            sqlIdInfoProductoC.Value = pObjBEInfoProductoC.IdInfoProducto

            filasAfectadas = sqlCmdInsInfProdC.ExecuteNonQuery()
        End Using
        If filasAfectadas <= 0 Then
            Return -1
        Else
            Return filasAfectadas
        End If
    End Function
End Class
