﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports Microsoft.Practices.EnterpriseLibrary.Data

Public Class PersonaDA

    Dim _db As Database = Nothing

    Public Sub New()
        _db = Conexion.Instancia.Database()
    End Sub

    Function Agregar(ByVal Objeto As PersonaBE, Optional ByVal Transac As DbTransaction = Nothing) As String
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_Persona_Agregar")

            _db.AddOutParameter(Command, "IdPersona", DbType.Int32, 0)
            _db.AddInParameter(Command, "TipoDocumento", DbType.String, Objeto.TipoDocumento)
            _db.AddInParameter(Command, "NumeroDocumento", DbType.String, Objeto.NumeroDocumento)
            _db.AddInParameter(Command, "Nombre", DbType.String, Objeto.Nombre)
            _db.AddInParameter(Command, "Nombre2", DbType.String, Objeto.Nombre2)
            _db.AddInParameter(Command, "ApellidoPaterno", DbType.String, Objeto.ApellidoPaterno)
            _db.AddInParameter(Command, "ApellidoMaterno", DbType.String, Objeto.ApellidoMaterno)
            _db.AddInParameter(Command, "NombreCompleto", DbType.String, Objeto.NombreCompleto)
            _db.AddInParameter(Command, "RazonSocial", DbType.String, Objeto.RazonSocial)
            _db.AddInParameter(Command, "FechaNacimiento", DbType.DateTime, Objeto.FechaNacimiento)
            _db.AddInParameter(Command, "Direccion", DbType.String, Objeto.Direccion)
            _db.AddInParameter(Command, "Telefono", DbType.String, Objeto.Telefono)
            _db.AddInParameter(Command, "Sexo", DbType.String, Objeto.Sexo)
            _db.AddInParameter(Command, "Estado", DbType.String, Objeto.Estado)
            _db.AddInParameter(Command, "UsuarioReg", DbType.String, Objeto.UsuarioReg)
            _db.AddInParameter(Command, "FechaReg", DbType.DateTime, Objeto.FechaReg)
            _db.AddInParameter(Command, "UsuarioMod", DbType.String, Objeto.UsuarioMod)
            _db.AddInParameter(Command, "FechaMod", DbType.DateTime, Objeto.FechaMod)



            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        resultado = Convert.ToInt32(Command.Parameters("@IdPersona").Value)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
                resultado = Convert.ToInt32(Command.Parameters("@IdPersona").Value)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Modificar(ByVal Objeto As PersonaBE, Optional ByVal Transac As DbTransaction = Nothing) As Integer
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_Persona_Modificar", Objeto.TipoDocumento, Objeto.NumeroDocumento, Objeto.Nombre, Objeto.Nombre2, Objeto.ApellidoPaterno, Objeto.ApellidoMaterno, Objeto.NombreCompleto, Objeto.RazonSocial, Objeto.FechaNacimiento, Objeto.Direccion, Objeto.Telefono, Objeto.Sexo, Objeto.Estado, Objeto.UsuarioMod, Objeto.FechaMod)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Eliminar(ByVal Objeto As PersonaBE, Optional ByVal Transac As DbTransaction = Nothing) As Integer
        Dim resultado As String = "0"
        Try
            Dim Command As DbCommand = _db.GetStoredProcCommand("usp_Persona_Eliminar", Objeto.IdPersona, Objeto.Estado, Objeto.UsuarioMod, Objeto.FechaMod)

            If Transac Is Nothing Then
                Using Connection As DbConnection = _db.CreateConnection()
                    Connection.Open()
                    Dim Transaction As DbTransaction = Connection.BeginTransaction
                    Try
                        resultado = _db.ExecuteNonQuery(Command, Transaction)
                        Transaction.Commit()
                    Catch ex As Exception
                        Transaction.Rollback()
                    End Try
                End Using

            Else
                resultado = _db.ExecuteNonQuery(Command, Transac)
            End If
        Catch ex As Exception
            If Not Transac Is Nothing Then
                Transac.Rollback()
            End If
            resultado = ex.Message
        End Try
        Return resultado
    End Function

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of PersonaBE)
        Dim Lista As New List(Of PersonaBE)

        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_Persona_Todos")
                While dr.Read()
                    Lista.Add(Populate.Persona_Lista(dr))
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_Persona_Todos")
                While dr.Read()
                    Lista.Add(Populate.Persona_Lista(dr))
                End While
            End Using
        End If
        Return Lista
    End Function

    Function Listar_Id(ByVal Objeto As PersonaBE, Optional ByVal Transaction As DbTransaction = Nothing) As PersonaBE
        Dim _PersonaBE As New PersonaBE

        If Not Transaction Is Nothing Then
            Using dr As IDataReader = _db.ExecuteReader(Transaction, "usp_Persona_Id", Objeto.IdPersona)
                While dr.Read()
                    _PersonaBE = Populate.Persona_Lista(dr)
                End While
            End Using
        Else
            Using dr As IDataReader = _db.ExecuteReader("usp_Persona_Id", Objeto.IdPersona)
                While dr.Read()
                    _PersonaBE = Populate.Persona_Lista(dr)
                End While
            End Using
        End If
        Return _PersonaBE
    End Function

End Class
