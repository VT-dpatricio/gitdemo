﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL

Public Class ObjetoEntidadBL

    Dim _ObjetoEntidadda As ObjetoEntidadda

    Public Sub New()
        _ObjetoEntidadda = New ObjetoEntidadda
    End Sub

    Function Agregar(ByVal Objeto As ObjetoEntidadBE) As String
        Return _ObjetoEntidadda.Agregar(Objeto, Nothing)
    End Function

    Function Modificar(ByVal Objeto As ObjetoEntidadBE) As Integer
        Return _ObjetoEntidadda.Modificar(Objeto, Nothing)
    End Function

    Function Eliminar(ByVal Objeto As ObjetoEntidadBE) As Integer
        Return _ObjetoEntidadda.Eliminar(Objeto, Nothing)
    End Function

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of ObjetoEntidadBE)
        Return _ObjetoEntidadda.Listar_Todos(Nothing)
    End Function

    Function Listar_Id(ByVal Objeto As ObjetoEntidadBE, Optional ByVal Transaction As DbTransaction = Nothing) As ObjetoEntidadBE
        Return _ObjetoEntidadda.Listar_Id(Objeto, Nothing)
    End Function

End Class
