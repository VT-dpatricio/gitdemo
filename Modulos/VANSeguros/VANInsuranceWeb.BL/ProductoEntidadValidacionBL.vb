﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL

Public Class ProductoEntidadValidacionBL

    Dim _ProductoEntidadValidacionda As ProductoEntidadValidacionda

    Public Sub New()
        _ProductoEntidadValidacionda = New ProductoEntidadValidacionda
    End Sub

    Function Agregar(ByVal Objeto As ProductoEntidadValidacionBE) As String
        Return _ProductoEntidadValidacionda.Agregar(Objeto, Nothing)
    End Function

    Function AgregarT(ByVal List As List(Of ProductoEntidadValidacionBE)) As String
        Return _ProductoEntidadValidacionda.AgregarT(List)
    End Function

    Function Modificar(ByVal Objeto As ProductoEntidadValidacionBE) As Integer
        Return _ProductoEntidadValidacionda.Modificar(Objeto, Nothing)
    End Function

    Function Eliminar(ByVal Objeto As ProductoEntidadValidacionBE) As Integer
        Return _ProductoEntidadValidacionda.Eliminar(Objeto, Nothing)
    End Function

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of ProductoEntidadValidacionBE)
        Return _ProductoEntidadValidacionda.Listar_Todos(Nothing)
    End Function

    Function Listar_Id(ByVal Objeto As ProductoEntidadValidacionBE, Optional ByVal Transaction As DbTransaction = Nothing) As ProductoEntidadValidacionBE
        Return _ProductoEntidadValidacionda.Listar_Id(Objeto, Nothing)
    End Function

    Function Listar_IdProductoIdEntidad(ByVal Objeto As ProductoEntidadValidacionBE, Optional ByVal Transaction As DbTransaction = Nothing) As List(Of EntidadDetalleBE)
        Return _ProductoEntidadValidacionda.Listar_IdProductoIdEntidad(Objeto, Nothing)
    End Function


End Class
