﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL

Public Class TipoValorBL

    Dim _TipoValorDA As TipoValorDA

    Public Sub New()
        _TipoValorDA = New TipoValorDA
    End Sub

    Function Agregar(ByVal Objeto As TipoValorBE) As String
        Return _TipoValorDA.Agregar(Objeto, Nothing)
    End Function

    Function Modificar(ByVal Objeto As TipoValorBE) As Integer
        Return _TipoValorDA.Modificar(Objeto, Nothing)
    End Function

    Function Eliminar(ByVal Objeto As TipoValorBE) As Integer
        Return _TipoValorDA.Eliminar(Objeto, Nothing)
    End Function

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of TipoValorBE)
        Return _TipoValorDA.Listar_Todos(Nothing)
    End Function

    Function Listar_Id(ByVal Objeto As TipoValorBE, Optional ByVal Transaction As DbTransaction = Nothing) As TipoValorBE
        Return _TipoValorDA.Listar_Id(Objeto, Nothing)
    End Function

End Class
