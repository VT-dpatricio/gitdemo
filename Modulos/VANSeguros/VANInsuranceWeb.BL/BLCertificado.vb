﻿Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL
Imports System
Imports System.Collections

Public Class BLCertificado

    Public Function ObtenerNumCertificado(ByVal pIdOficina As Int32, ByVal pIDProducto As Integer) As Int32
        Dim oDL As New DLNTCertificado
        Return oDL.ObtenerNumCertificado(pIdOficina, pIDProducto)
    End Function

    Public Function LiberarNumCertificado(ByVal pIdOficina As Int32, ByVal pIdProducto As Int32, ByVal pNumCertificado As Decimal) As Boolean
        Dim oDL As New DLNTCertificado
        Return oDL.LiberarNumCertificado(pIdOficina, pIdProducto, pNumCertificado)
    End Function

    Public Function ValidarClienteDomicilio(ByVal pBECer As BECertificado) As String
        Dim oDL As New DLNTCertificado
        Return oDL.ValidarClienteDomicilio(pBECer)
    End Function
End Class
