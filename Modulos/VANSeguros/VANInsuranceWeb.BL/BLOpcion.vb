Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL
Imports System
Imports System.Collections
Public Class BLOpcion
    Public Function Seleccionar(ByVal pIDProducto As Integer) As IList
        Dim oDL As New DLNTOpcion
        Return oDL.Seleccionar(pIDProducto)
    End Function

    Public Function SeleccionarOpciones(ByVal pIDProducto As Integer, ByVal pParametros As String, ByVal pCondicion As String) As IList
        Dim oDL As New DLNTOpcion
        Return oDL.SeleccionarOpciones(pIDProducto, pParametros, pCondicion)
    End Function

End Class
