﻿Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL
Imports System
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient

Public Class BLExpedir
    Inherits DLBase

    Public Function BLExpedirCertificado(ByVal pObjBECertificado As BECertificado, ByVal pLstBEAsegurado As List(Of BEAsegurado), ByVal pLstBEBeneficiario As List(Of BEBeneficiario), ByVal pLstInfoProductoC As List(Of BEInfoProductoC), ByVal pLstBEInfoAseguradoC As List(Of BEInfoAseguradoC), ByVal pNumCertFisico As [String]) As List(Of BEExpedir)
        Dim lstValidaCertificado As List(Of BEExpedir)
        Dim lstValidaAsegurado As List(Of BEExpedir)
        Dim lstBEExpedir As New List(Of BEExpedir)()
        Dim objBEExpedir As BEExpedir
        Dim exito As [Boolean] = False
        Dim sqlTranx As SqlTransaction
        Dim oCN As New DLBase

        Using sqlCon As New SqlConnection(oCN.CadenaConexion)
            sqlCon.Open()

            '****************VALIDAR LA INFORMACION DEL CERTIFICADO***************

            Dim objDAExpedir As New DLNTExpedir()
            lstValidaCertificado = ObtieneResultadoExpValideCert(pObjBECertificado.IdProducto, pNumCertFisico, pObjBECertificado.IdTipoDocumento, pObjBECertificado.Cccliente, pObjBECertificado.IdMedioPago, pObjBECertificado.NumeroCuenta, sqlCon)
            If lstValidaCertificado.Count > 0 Then
                Return lstValidaCertificado
            End If

            '****************VALIDAR LA INFORMACION DEL ASEGURADO*****************

            For Each objBEAseg As BEAsegurado In pLstBEAsegurado
                lstValidaAsegurado = ObtieneResultadoExpValideAseg(pObjBECertificado.IdProducto, objBEAseg.Ccaseg, sqlCon)
                If lstValidaAsegurado.Count > 0 Then
                    Return lstValidaAsegurado
                End If
            Next

            sqlTranx = sqlCon.BeginTransaction()
            '*********INSERCION DEL CERTIFICADO***************

            Dim objDACertificado As New DLTCertificado()
            Dim rpta As Int32 = objDACertificado.DAInsertarCertificado(pObjBECertificado, sqlCon, sqlTranx)

            If rpta <> -1 Then
                exito = True
                Dim objDAInfoProductoC As New DLTInfoProductoC()
                For Each objInfPc As BEInfoProductoC In pLstInfoProductoC
                    '*********INSERCION DE LOS INFOPRODUCTOS***************

                    rpta = objDAInfoProductoC.DAInsertaInfoProductoC(objInfPc, sqlCon, sqlTranx)
                    If rpta = -1 Then
                        exito = False
                        Exit For
                    Else
                        exito = True
                    End If
                Next

                If exito Then
                    Dim objDAAsegurado As New DLTAsegurado()
                    For Each objAseg As BEAsegurado In pLstBEAsegurado
                        '*********INSERCION DE LOS ASEGURADOS***************

                        rpta = objDAAsegurado.DAInsertarAsegurado(objAseg, sqlCon, sqlTranx)
                        If rpta = -1 Then
                            exito = False
                            Exit For
                        Else
                            exito = True
                        End If
                    Next
                    If exito Then
                        Dim objDAInfAsegC As New DLTInfoAseguradoC()
                        For Each objInfAsegC As BEInfoAseguradoC In pLstBEInfoAseguradoC
                            '*********INSERCION DE LOS INFOASEGURADOS***************

                            rpta = objDAInfAsegC.DAInsertaInfoAseguradoC(objInfAsegC, pObjBECertificado.IdProducto, sqlCon, sqlTranx)
                            If rpta = -1 Then
                                exito = False
                                Exit For
                            Else
                                exito = True
                            End If
                        Next
                        If exito Then
                            Dim objDABenef As New DLTBeneficiario()
                            For Each objBenef As BEBeneficiario In pLstBEBeneficiario
                                '*********INSERCION DE LOS BENEFICIARIOS***************
                                rpta = objDABenef.DAInsertarBeneficiario(objBenef, sqlCon, sqlTranx)

                                If rpta = -1 Then
                                    exito = False
                                    Exit For
                                Else
                                    exito = True
                                End If

                            Next
                            If exito Then
                                '*********EJECUTA PROCESO POST EXPEDIR***************
                                Dim objDAPostExpedir As New DLTPostExpedirCert()
                                rpta = objDAPostExpedir.EjecutarPostExpedir(pObjBECertificado.IdCertificado, sqlCon, sqlTranx)
                                If rpta = -1 Then
                                    exito = False
                                Else
                                    exito = True
                                End If
                                '--------------------------------------------------
                                If exito Then
                                    sqlTranx.Commit()
                                    objBEExpedir = New BEExpedir()
                                    objBEExpedir.Descripcion = "OK"
                                    objBEExpedir.Codigo = 0
                                    lstBEExpedir.Add(objBEExpedir)
                                    Return lstBEExpedir
                                Else
                                    sqlTranx.Rollback()
                                    objBEExpedir = New BEExpedir()
                                    objBEExpedir.Descripcion = "Error: Ocurrió un error al ejecutar Post-Expedir"
                                    objBEExpedir.Codigo = 1006
                                    lstBEExpedir.Add(objBEExpedir)
                                    Return lstBEExpedir
                                End If
                            Else
                                'Retorna en caso de que falle la inserción de algún Beneficiario
                                sqlTranx.Rollback()
                                objBEExpedir = New BEExpedir()
                                objBEExpedir.Descripcion = "Error: Ocurrió un error al grabar el Beneficiario"
                                objBEExpedir.Codigo = 1005
                                lstBEExpedir.Add(objBEExpedir)
                                Return lstBEExpedir
                            End If
                        Else
                            'Retorna en caso de que falle la inserción de algún InfoAsegurado
                            sqlTranx.Rollback()
                            objBEExpedir = New BEExpedir()
                            objBEExpedir.Descripcion = "Error: Ocurrió un error al grabar InfoAsegurado"
                            objBEExpedir.Codigo = 1004
                            lstBEExpedir.Add(objBEExpedir)
                            Return lstBEExpedir
                        End If
                    Else
                        'Retorna en caso de que falle la inserción de algún asegurado
                        sqlTranx.Rollback()
                        objBEExpedir = New BEExpedir()
                        objBEExpedir.Descripcion = "Error: Ocurrió un error al grabar Asegurado"
                        objBEExpedir.Codigo = 1003
                        lstBEExpedir.Add(objBEExpedir)
                        Return lstBEExpedir
                    End If
                Else
                    'Retorna en caso de que falle la inserción de algún infoproducto
                    sqlTranx.Rollback()
                    objBEExpedir = New BEExpedir()
                    objBEExpedir.Descripcion = "Error: Ocurrió un error al grabar InfoProducto"
                    objBEExpedir.Codigo = 1002
                    lstBEExpedir.Add(objBEExpedir)
                    Return lstBEExpedir
                End If
            Else
                'Retorna en caso de que falle la inserción del certificado
                sqlTranx.Rollback()
                objBEExpedir = New BEExpedir()
                objBEExpedir.Descripcion = "Error: Ocurrió un error al grabar el Certificado"
                objBEExpedir.Codigo = 1001
                lstBEExpedir.Add(objBEExpedir)
                Return lstBEExpedir
            End If
        End Using
    End Function

    Private Function ObtieneResultadoExpValideCert(ByVal pIdProducto As Int32, ByVal pNumCertFisico As [String], ByVal pIDTipoDocumento As [String], ByVal pCccliente As [String], ByVal pIDMedioPago As [String], ByVal pNumeroCuenta As [String], ByVal pSqlCon As SqlConnection) As List(Of BEExpedir)
        Dim lstBEExpedir As New List(Of BEExpedir)()
        Dim objDAExpedir As New DLNTExpedir()
        Using sqlDr As SqlDataReader = objDAExpedir.DAValidaExpedirCertificado(pIdProducto, pNumCertFisico, pIDTipoDocumento, pCccliente, pIDMedioPago, pNumeroCuenta, pSqlCon)
            If sqlDr IsNot Nothing Then
                If sqlDr.HasRows Then
                    Dim descripcionIndex As Int32 = sqlDr.GetOrdinal("descripcion")
                    Dim errorIndex As Int32 = sqlDr.GetOrdinal("error")

                    Dim objBEExpedir As BEExpedir
                    While sqlDr.Read()
                        objBEExpedir = New BEExpedir()
                        objBEExpedir.Descripcion = sqlDr.GetString(descripcionIndex)
                        objBEExpedir.Codigo = sqlDr.GetInt32(errorIndex)
                        lstBEExpedir.Add(objBEExpedir)
                    End While
                End If
            End If
        End Using
        Return lstBEExpedir
    End Function

    Private Function ObtieneResultadoExpValideAseg(ByVal pIdProducto As Int32, ByVal pCcaseg As [String], ByVal pSqlCon As SqlConnection) As List(Of BEExpedir)
        Dim lstBEExpedir As New List(Of BEExpedir)()

        Dim objDAExpedir As New DLNTExpedir()

        Using sqlDr As SqlDataReader = objDAExpedir.DAValidaExpedirAsegurado(pIdProducto, pCcaseg, pSqlCon)
            If sqlDr IsNot Nothing Then
                If sqlDr.HasRows Then
                    Dim descripcionIndex As Int32 = sqlDr.GetOrdinal("descripcion")
                    Dim errorIndex As Int32 = sqlDr.GetOrdinal("error")

                    Dim objBEExpedir As BEExpedir
                    While sqlDr.Read()
                        objBEExpedir = New BEExpedir()
                        objBEExpedir.Descripcion = sqlDr.GetString(descripcionIndex)
                        objBEExpedir.Codigo = sqlDr.GetInt32(errorIndex)
                        lstBEExpedir.Add(objBEExpedir)
                    End While
                End If
            End If
        End Using
        Return lstBEExpedir
    End Function

End Class
