Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL
Imports System
Imports System.Collections
Public Class BLOpcionPrima
    Public Function Seleccionar(ByVal pIdProducto As Int32, ByVal pOpcion As String, ByVal pIdFrecuencia As Int32, ByVal pCondicion As String, ByVal pEdad As Integer) As IList
        Dim oDL As New DLNTOpcionPrima
        Dim oBE As New BEOpcionPrima
        oBE.IdProducto = pIdProducto
        oBE.Opcion = pOpcion
        oBE.IdFrecuencia = pIdFrecuencia
        oBE.EdadMin = pEdad
        Return oDL.SeleccionarCondicion(oBE, pCondicion)
    End Function

    Public Function SeleccionarBE(ByVal pIdProducto As Int32, ByVal pOpcion As String, ByVal pIdFrecuencia As Int32, ByVal pIDMoneda As String, ByVal pEdad As Integer) As BEOpcionPrima
        Dim oDL As New DLNTOpcionPrima
        Dim oBE As New BEOpcionPrima
        oBE.IdProducto = pIdProducto
        oBE.Opcion = pOpcion
        oBE.IdFrecuencia = pIdFrecuencia
        oBE.EdadMin = pEdad
        Dim Lista As IList = oDL.SeleccionarCondicion(oBE, pIDMoneda)
        For Each oBEOP As BEOpcionPrima In Lista
            If oBE.IdProducto = oBEOP.IdProducto And oBE.Opcion = oBEOP.Opcion And oBE.IdFrecuencia = oBEOP.IdFrecuencia And oBEOP.IdMonedaPrima = pIDMoneda Then
                oBE.MontoAsegurado = oBEOP.MontoAsegurado
                oBE.Prima = oBEOP.Prima
                Exit For
            End If
        Next
        Return oBE
    End Function
End Class