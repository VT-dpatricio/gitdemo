﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL

Public Class ProductoInfoProductoBL

    Dim _ProductoInfoProductoda As ProductoInfoProductoda

    Public Sub New()
        _ProductoInfoProductoda = New ProductoInfoProductoda
    End Sub

    Function Agregar(ByVal Objeto As ProductoInfoProductoBE) As String
        Return _ProductoInfoProductoda.Agregar(Objeto, Nothing)
    End Function

    Function Modificar(ByVal Objeto As ProductoInfoProductoBE) As Integer
        Return _ProductoInfoProductoda.Modificar(Objeto, Nothing)
    End Function

    Function Eliminar(ByVal Objeto As ProductoInfoProductoBE) As Integer
        Return _ProductoInfoProductoda.Eliminar(Objeto, Nothing)
    End Function

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of ProductoInfoProductoBE)
        Return _ProductoInfoProductoda.Listar_Todos(Nothing)
    End Function

    Function Listar_Id(ByVal Objeto As ProductoInfoProductoBE, Optional ByVal Transaction As DbTransaction = Nothing) As ProductoInfoProductoBE
        Return _ProductoInfoProductoda.Listar_Id(Objeto, Nothing)
    End Function

    Function Listar_IdProducto_Check(ByVal Objeto As ProductoInfoProductoBE, Optional ByVal Transaction As DbTransaction = Nothing) As List(Of InfoProductoBE)
        Return _ProductoInfoProductoda.Listar_IdProducto_Check(Objeto, Nothing)
    End Function

    Function Listar_IdProducto_X_Check(ByVal Objeto As ProductoInfoProductoBE, Optional ByVal Transaction As DbTransaction = Nothing) As List(Of InfoProductoBE)
        Return _ProductoInfoProductoda.Listar_IdProducto_X_Check(Objeto, Nothing)
    End Function

    Function Listar_IdProducto(ByVal Objeto As ProductoInfoProductoBE, Optional ByVal Transaction As DbTransaction = Nothing) As List(Of InfoProductoBE)
        Return _ProductoInfoProductoda.Listar_IdProducto(Objeto, Nothing)
    End Function


End Class
