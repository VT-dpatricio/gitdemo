﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL

Public Class TipoRelacionBL

    Dim _TipoRelacionDA As TipoRelacionDA

    Public Sub New()
        _TipoRelacionDA = New TipoRelacionDA
    End Sub

    Function Agregar(ByVal Objeto As TipoRelacionBE) As String
        Return _TipoRelacionDA.Agregar(Objeto, Nothing)
    End Function

    Function Modificar(ByVal Objeto As TipoRelacionBE) As Integer
        Return _TipoRelacionDA.Modificar(Objeto, Nothing)
    End Function

    Function Eliminar(ByVal Objeto As TipoRelacionBE) As Integer
        Return _TipoRelacionDA.Eliminar(Objeto, Nothing)
    End Function

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of TipoRelacionBE)
        Return _TipoRelacionDA.Listar_Todos(Nothing)
    End Function

    Function Listar_Id(ByVal Objeto As TipoRelacionBE, Optional ByVal Transaction As DbTransaction = Nothing) As TipoRelacionBE
        Return _TipoRelacionDA.Listar_Id(Objeto, Nothing)
    End Function

End Class
