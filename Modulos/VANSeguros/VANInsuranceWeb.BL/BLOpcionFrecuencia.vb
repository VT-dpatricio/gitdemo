Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL
Imports System
Imports System.Collections
Public Class BLOpcionFrecuencia
    Public Function Seleccionar(ByVal pIdProducto As Int32, ByVal pOpcion As String) As IList
        Dim oDL As New DLNTOpcionFrecuencia
        Dim oBE As New BEOpcionFrecuencia
        oBE.IdProducto = pIdProducto
        oBE.Opcion = pOpcion
        Return oDL.Seleccionar(oBE)
    End Function

    Public Function SeleccionarFrecuencia(ByVal pIdProducto As Int32, ByVal pOpcion As String, ByVal pParametros As String) As IList
        Dim oDL As New DLNTOpcionFrecuencia
        Dim oBE As New BEOpcionFrecuencia
        oBE.IdProducto = pIDProducto
        oBE.Opcion = pOpcion
        Return oDL.SeleccionarFrecuencias(oBE, pParametros)
    End Function

End Class
