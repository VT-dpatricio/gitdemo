﻿Imports System.Data
Imports System.Data.Common
Imports VAN.InsuranceWeb.BE
Imports VAN.InsuranceWeb.DL

Public Class TipoValidacionBL

    Dim _TipoValorDA As TipoValidacionDA

    Public Sub New()
        _TipoValorDA = New TipoValidacionDA
    End Sub

    Function Agregar(ByVal Objeto As TipoValidacionBE) As String
        Return _TipoValorDA.Agregar(Objeto, Nothing)
    End Function

    Function Modificar(ByVal Objeto As TipoValidacionBE) As Integer
        Return _TipoValorDA.Modificar(Objeto, Nothing)
    End Function

    Function Eliminar(ByVal Objeto As TipoValidacionBE) As Integer
        Return _TipoValorDA.Eliminar(Objeto, Nothing)
    End Function

    Function Listar_Todos(Optional ByVal Transaction As DbTransaction = Nothing) As List(Of TipoValidacionBE)
        Return _TipoValorDA.Listar_Todos(Nothing)
    End Function

    Function Listar_Id(ByVal Objeto As TipoValidacionBE, Optional ByVal Transaction As DbTransaction = Nothing) As TipoValidacionBE
        Return _TipoValorDA.Listar_Id(Objeto, Nothing)
    End Function


End Class
